
from isaacRL.trainers.trainer_ac import TrainerAC
from isaacRL.models.actorcritics import ActorMVN

import torch
import torch.nn.functional as F

import time
import os, sys
import os.path as osp


class TrainerSACBase(TrainerAC):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):

        super(TrainerSACBase, self).__init__(config)

        self.alpha = torch.tensor(self.config['training']['alpha'])
        self.tau = self.config['training']['tau']

        self.alpha_pause = 0
        self.alpha_optim = None
        self.alpha_tuning = False
        if self.config['training'].get('alpha_tuning', False):
            # https://github.com/pranz24/pytorch-soft-actor-critic/blob/master/sac.py#L29
            self.target_entropy = -float(self.action_size)
            self.log_alpha = torch.log(self.alpha).clone().to(self.device)
            self.log_alpha.requires_grad = True
            self.alpha_optim = torch.optim.Adam([self.log_alpha], lr=self.lr*0.05)
            self.alpha_pause = self.config['training'].get('alpha_pause', 0)
            self.alpha_tuning = self.alpha_pause == 0

    def setup_actor(self):
        self.actor = ActorMVN(
            self.observation_size, self.action_size,
            self.config['model']['actor'],
        )
        self.actor.to(self.device)
        self.pretrained_model = self.config['model']['actor'].get('pretrained', None)
        if self.pretrained_model is not None and osp.exists(self.pretrained_model):
            self.load_pretrained(self.pretrained_model)

    def get_actions(self, state, evaluate):
        actions, _ = self.actor.get_actions(state, evaluate)
        actions = actions.detach()
        return actions

    def train_setup(self):
        if self.pretrained_model is not None and osp.exists(self.pretrained_model):
            forward = self.actor.forward
            self.actor.forward = self.actor.deterministic
        super().train_setup()
        if self.pretrained_model is not None and osp.exists(self.pretrained_model):
            self.actor.forward = forward

    def print_info(self):
        torch.set_printoptions(precision=9,sci_mode=False)
        print('alpha value:', self.alpha.item())
        torch.set_printoptions(precision=3,sci_mode=False)

    def _update_weights(self, state, action, nstate, rewards, terminals):
        # taken from: https://github.com/pranz24/pytorch-soft-actor-critic/blob/master/sac.py#L52
        with torch.no_grad():
            # Makes up second half of eq. (9)
            naction, nstate_log_pi = self.actor(nstate)
            q1_tar = self.target_critic1(nstate, naction)
            q2_tar = self.target_critic2(nstate, naction)
            # Two Q-functions to mitigate positive bias in the policy improvement step
            qvals = torch.min(q1_tar, q2_tar)
            n_qval = qvals - self.alpha * nstate_log_pi
            n_qval = rewards + (1-terminals) * self.gamma * n_qval
            n_qval = n_qval.detach()

        qval1 = self.critic1(state, action)
         # JQ = 𝔼(st,at)~D[0.5(Q1(st,at) - r(st,at) - γ(𝔼st+1~p[V(st+1)]))^2]
        q1_loss = F.mse_loss(qval1, n_qval)
        self.critic_optim1.zero_grad()
        q1_loss.backward()
        if self.clip_grad is not None:
            self.clip_grad(self.critic1.parameters())
        self.critic_optim1.step()

        qval2 = self.critic2(state, action)
        q2_loss = F.mse_loss(qval2, n_qval)
        self.critic_optim2.zero_grad()
        q2_loss.backward()
        if self.clip_grad is not None:
            self.clip_grad(self.critic2.parameters())
        self.critic_optim2.step()

        if self._wupdate_calls % self.target_update_interval == 0:
            self.target_critic1 = self.critic1.soft_update(self.tau, self.target_critic1)
            self.target_critic2 = self.critic2.soft_update(self.tau, self.target_critic2)

        pi_loss = None
        if self.actor_update and self._wupdate_calls % self.policy_update_interval == 0:
            action, log_pi = self.actor(state)

            qval1 = self.critic1(state, action)
            qval2 = self.critic2(state, action)
            qvals = torch.min(qval1, qval2)
            # Policy loss as defined in eq (12)
            # Jπ = 𝔼st∼D,εt∼N[α * logπ(f(εt;st)|st) − Q(st,f(εt;st))]
            pi_loss = ((self.alpha.detach() * log_pi) - qvals).mean()

            self.actor_optim.zero_grad()
            pi_loss.backward()
            if self.clip_grad is not None:
                self.clip_grad(self.actor.parameters())
            self.actor_optim.step()

            if self.alpha_tuning:
                alpha_loss = -(self.log_alpha * (log_pi + self.target_entropy).detach()).mean()

                self.alpha_optim.zero_grad()
                alpha_loss.backward()
                self.alpha_optim.step()

                self.alpha = self.log_alpha.exp()

        qloss = (q1_loss + q2_loss).item()
        pi_loss = pi_loss.item() if pi_loss is not None else 0.
        return qloss, pi_loss

    def load_model(self, checkpt):
        checkpoint = super().load_model(checkpt)
        if self.alpha_optim is not None and 'alpha_optim' in checkpoint:
            self.alpha_optim = checkpoint['alpha_optim']
            if 'log_alpha' in checkpoint:
                self.log_alpha = checkpoint['log_alpha']
                self.alpha = self.log_alpha.exp()
            # elif 'alpha' in checkpoint:
            #     self.log_alpha = torch.log(checkpoint['alpha']).to(self.device)
            #     self.log_alpha.requires_grad = True

    def load_pretrained(self, checkpt):
        checkpoint = torch.load(checkpt)
        self.actor.load_state_dict(checkpoint['actor'])

    def save_model(self, epoch):
        checkpoint = {}
        if self.alpha_optim is not None:
            checkpoint['log_alpha'] = self.log_alpha
            checkpoint['alpha_optim'] = self.alpha_optim
        super().save_model(epoch, checkpoint)
