
# import pickle
# import typing
# import logging

import numpy as np  # basic math and random numbers
# import src.utils.pdf as pdf
# from KDEpy import FFTKDE

import torch
import torch.nn as nn
import torch.distributions as D
from torch.distributions.mixture_same_family import MixtureSameFamily
from torch.distributions.multivariate_normal import MultivariateNormal
# from torch.distributions.normal import Normal
# from torch.distributions import Independent
# from torch.distributions.gamma import Gamma
# from torch.distributions.uniform import Uniform
# from torch.distributions.laplace import Laplace
# #from torch_utils import get_name
#
# from src.models.random_features_torch import RFF as RFFtorch
# from src.models.kernels import *
#
# from tqdm import tqdm
# from sklearn.preprocessing import StandardScaler
# from datetime import datetime
#
# from src.models.mcmc.sghmc import SGHMC
# from src.models.mcmc.sgld import SGLD
# from src.models.mcmc.losses import NegativeLogLikelihood, get_loss, to_bayesian_loss

from mdn.utils.activations import Mish


class MDNModel(nn.Module):
    """
    Mixture Density Model:
    outpus a mean with diagonal and lower left values for a covariance matrix
    and alphas for each mixture gaussian
    """
    def __init__(self, hlayer_sizes=[50, 50], nmodes=10, ind=2, outd=1, batchnorm=False):
        super(MDNModel, self).__init__()
        self.n_ins = ind
        self.n_outs = outd
        self.n_gauss = nmodes

        model = []
        hlayer_sizes = [ind] + hlayer_sizes
        for ix, layer_size in enumerate(hlayer_sizes[1:]):
            model.append(nn.Linear(hlayer_sizes[ix], layer_size))
            model.append(Mish())
            if batchnorm:
                model.append(nn.BatchNorm1d(layer_size))
        self.model = nn.Sequential( *model )
        last_size = hlayer_sizes[-1]

        self.mu = nn.Linear( last_size, self.n_gauss * self.n_outs )
        self.pi = nn.Linear( last_size, self.n_gauss )
        self.sig_diagonal = nn.Linear( last_size, self.n_gauss * self.n_outs )
        self.sigsize = 0.5 * self.n_outs * (self.n_outs - 1)
        self.sig = nn.Linear( last_size, int(self.sigsize * self.n_gauss) )

    def forward(self, x):
        z_h = self.model(x)

        mu = self.mu(z_h).reshape( -1, self.n_gauss, self.n_outs )
        alphas = torch.softmax( self.pi(z_h), -1 )
        sig_diag = torch.exp( self.sig_diagonal(z_h)
          ).reshape( -1, self.n_gauss, self.n_outs )
        # sig = self.sig(z_h
        #   ).reshape( -1, self.n_gauss, int(self.sigsize) )
        # return alphas, mu, sig, sig_diag
        return mu, sig_diag, alphas


class MVNModel(nn.Module):
    """
    MultivariateNormalModel:
    outpus a mean and stdev for a mutltivariate gaussian distribution
    """
    def __init__(self, hlayer_sizes=[50, 50, 50], ind=2, outd=2, batchnorm=False):
        super(MVNModel, self).__init__()
        self.n_ins = ind
        self.n_outs = outd
        self.hlayer_sizes = hlayer_sizes

        model = []
        hlayer_sizes = [ind] + hlayer_sizes
        for ix, layer_size in enumerate(hlayer_sizes[1:]):
            model.append(nn.Linear(hlayer_sizes[ix], layer_size))
            model.append(Mish())
            if batchnorm:
                model.append(nn.BatchNorm1d(layer_size))
        self.model = nn.Sequential( *model )
        last_size = hlayer_sizes[-1]

        self.mu = nn.Linear( last_size, self.n_outs )
        self.sig = nn.Linear( last_size, self.n_outs )

    def forward(self, x):
        z_h = self.model(x)
        mu = self.mu(z_h)
        sig = torch.exp( self.sig(z_h) )
        return mu, sig


class ClassifierModel(nn.Module):
    """
    ClassifierModel:
    outpus a classification
    """
    def __init__(self, hlayer_sizes=[50, 50, 50], ind=2, outd=2, contextlayer=False, batchnorm=False):
        super(ClassifierModel, self).__init__()
        self.n_ins = ind
        self.n_outs = outd
        self.hlayer_sizes = hlayer_sizes
        self.conlayer = None

        model = []
        # hlayer_sizes = [ind] + hlayer_sizes + [outd]
        self.inlayer = nn.Linear(ind, hlayer_sizes[0])
        self.inact = Mish()
        if contextlayer:
            self.conlayer = nn.Linear(ind, hlayer_sizes[0])
            hlayer_sizes[0] = hlayer_sizes[0]*2
        for ix, layer_size in enumerate(hlayer_sizes[1:]):
            model.append(nn.Linear(hlayer_sizes[ix], layer_size))
            model.append(Mish())
            if batchnorm:
                model.append(nn.BatchNorm1d(layer_size))
        model.append(nn.Linear(hlayer_sizes[-1], self.n_outs))
        # model.append(nn.Softmax())
        self.model = nn.Sequential( *model )

    def forward(self, x, cx=None):
        x = self.inact(self.inlayer(x))
        if self.conlayer is not None:
            cx = self.inact(self.conlayer(cx))
            x = torch.cat((x, cx), dim=1)
        return self.model(x)



class BinaryModel(ClassifierModel):
    """
    BinaryModel:
    outpus a 0 or 1
    """
    def __init__(self, hlayer_sizes=[50, 50, 50], ind=2, batchnorm=False):
        super(BinaryModel, self).__init__(hlayer_sizes, ind, 1, batchnorm)


def main():
    model = MDNModel(hlayer_sizes=[10, 10], nmodes=3, ind=4, outd=2)
    data = torch.randn(2, 4)

    alphas, mu, sig, sig_diag = model(data)

    import pdb; pdb.set_trace()


if __name__ == '__main__':
    main()
