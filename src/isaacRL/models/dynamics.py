
import time
import torch
import torch.nn as nn
from torch.distributions import Normal

from isaacRL.models.vit import vit_s16

from isaacRL.models.models import MVNModel, FCNet


class DynamicsModel(nn.Module):
    """docstring for DynamicsModel."""

    def __init__(self, statesize, action_size, cfg={}):
        super(DynamicsModel, self).__init__()
        self.statesize = statesize
        self.action_size = action_size
        self.cfg = cfg

        self.model = FCNet(
            infeats=(self.statesize + self.action_size),
            outfeats=self.statesize+1,
            layers=cfg.get('layers', [100]*3),
            activation=cfg.get('activation', 'relu'),
            batchnorm=cfg.get('batchnorm', False)
        )

    def forward(self, xdata, actions):
        feats = torch.cat((xdata['obs'], actions), dim=1)
        output = self.model(feats)
        nstate = output[:,:-1]
        reward = output[:,-1]
        return nstate, reward
