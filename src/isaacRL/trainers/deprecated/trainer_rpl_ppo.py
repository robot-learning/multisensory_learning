
from isaacRL.tasks.sim_task import SimTask
from isaacRL.utils.expert_behaviors import Expert
from isaacRL.trainers.trainer_ppo import TrainerPPO

import torch
import torch.nn as nn
import torch.nn.functional as F

import os, sys, argparse, json, random, time
import os.path as osp
import numpy as np
from tqdm import tqdm
import rospy


class TrainerRPLPPO(TrainerPPO):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        super(TrainerRPLPPO, self).__init__(config)

        self.beta = 20.0
        if 'beta' in self.config['training']:
            self.beta = self.config['training']['beta']

        if 'value_epochs' not in self.config['training']:
            self.config['training']['value_epochs'] = -1

        if 'expert_epochs' not in self.config['training']:
            self.config['training']['expert_epochs'] = -1

        if self.expert is None:
            self.expert = Expert(
              self.envir, self.config['environ']['max_env_steps'],
              self.device, open_loop=True, use_saved=self.config['training']['use_saved']
            )

    def train(self):

        interv = self.config['training']['checkpoint_interval']
        if self.config['training']['value_epochs'] > 0 and (self.start_epoch-1) < self.config['training']['value_epochs']:
            self.model.freeze_actor()
            print('freezing actor')
        # self.model.freeze_actor()

        self.model.train()
        self.envir.reset()
        self.expert.reset()
        for epoch in range(self.start_epoch, self.end_epoch):
            if rospy.is_shutdown():
                return

            rewards = []
            pbar = tqdm(total=interv, file=sys.stdout)
            for di in range(interv):
                if rospy.is_shutdown():
                    return

                start = time.time()
                rewards.append( self.play(epoch) )
                play_time = time.time() - start

                # learn from replays
                start = time.time()
                vloss, sloss = self.update_weights(epoch)
                learn_time = time.time() - start
                self.vlosses.append( vloss )
                self.slosses.append( sloss )
                reward_val = np.max(rewards) if self.sparse else rewards[-1] #np.mean(rewards[-1])
                desc = f'  R {reward_val:.2f}: loss={np.mean(self.vlosses[-(di+1):]):.3f},{np.mean(self.slosses[-(di+1):]):.3f} time={play_time:.2f},{learn_time:.2f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            if ( self.model.actor_is_frozen() and np.mean(self.vlosses[-interv:]) < self.beta and
              (epoch-1) >= self.config['training']['value_epochs'] ):
                self.model.unfreeze_actor()
                print('unfreezing actor')

            rounds = self.envir.num_envs * interv * self.acts_per_interval * epoch
            print('Epoch:', epoch)
            print('saving after {} timesteps'.format(rounds))
            reward_val = np.sum(rewards) if self.sparse else np.mean(rewards)
            print('mean rew per step:', reward_val)
            print('mean vloss:', np.mean(self.vlosses[-interv:]))
            print('mean sloss:', np.mean(self.slosses[-interv:]))
            # print('mean rewards:', self.buffer.mean_rewards())
            self.save_model(epoch)


    def play(self, playi):
        with torch.no_grad():
            state = self.envir.get_env_states()
            tot_rewards = 0.
            for step in range(self.acts_per_interval):
                if rospy.is_shutdown():
                    return tot_rewards
                base_acts, labels = self.expert.get_actions()
                state['base_action'] = base_acts
                actions, acts_logprob, values = self.model.act(state)
                if not self.model.actor_is_frozen():
                    base_acts = self.envir.add_actions(base_acts, actions)
                nstate, rewards, terminals = self.envir.step(base_acts)
                tot_rewards += rewards.mean().item()
                self.buffer.add_transitions(
                  state, actions, rewards, terminals, values, acts_logprob
                )
                self.expert.reset_indices( terminals )
                state = nstate
            state['base_action'] = base_acts #self.envir.convert_action2tensor(base_acts)
            _, _, last_values = self.model.act(state)
            self.buffer.compute_returns(last_values, self.gamma, self.lam)
            return tot_rewards/self.acts_per_interval

    @property
    def sparse(self):
        return self.envir.goalie.sparse
