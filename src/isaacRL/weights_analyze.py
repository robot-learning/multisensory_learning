
from isaacRL.train import *
import matplotlib.pyplot as plt

def main():
    opt, trainer = setup()

    if opt.checkpt is not None:
        trainer.load_model(opt.checkpt)

    model = trainer.model
    params = model.critic.parameters()

    pmin = float('inf')
    pmax = float('-inf')
    pmean = 0.
    pmag = 0.
    for par in params[::2]:
        pmin = min(pmin, par.min())
        pmax = max(pmax, par.max())
        pmean += par.mean()
        pmag += par.abs().mean()
    pmean = pmean / len(params[::2])
    pmag = pmag / len(params[::2])
    print('weights: min {}, max {}, mean {}, mag {}'.format(pmin, pmax, pmean, pmag))

    pmin = float('inf')
    pmax = float('-inf')
    pmean = 0.
    pmag = 0.
    for par in params[1::2]:
        pmin = min(pmin, par.min())
        pmax = max(pmax, par.max())
        pmean += par.mean()
        pmag += par.abs().mean()
    pmean = pmean / len(params[1::2])
    pmag = pmag / len(params[::2])
    print('weights: min {}, max {}, mean {}, mag {}'.format(pmin, pmax, pmean, pmag))

    qvals = trainer.evaluate()
    qvals = np.array(qvals)

    rospy.is_shutdown()

    print('qvals: min {}, max {}, mean {}'.format(qvals.min(), qvals.max(), qvals.mean()))

    plt.plot(trainer.qlosses, label='critic loss')
    plt.legend()
    plt.savefig('critic_loss.png')
    plt.clf()

    plt.plot(trainer.pi_losses, label='policy loss')
    plt.legend()
    plt.savefig('policy_loss.png')
    plt.clf()

    for ci in range(qvals.shape[1]):
        plt.plot(qvals[:, ci], label='qvals')
    plt.savefig('qvals.png')
    plt.clf()

    # import pdb; pdb.set_trace()

if __name__ == '__main__':
    main()
