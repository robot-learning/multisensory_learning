from isaacgym import gymtorch, gymapi

import argparse
import matplotlib.pyplot as plt

from isaacRL.train import *


def create_graph(filename, save_data, data_key='success_percents'):
    plt.plot(save_data['epochs'], save_data[data_key], 'bo-')
    if data_key == 'success_percents':
        plt.ylim([0., 1.])
    plt.savefig(filename)
    plt.clf()


def main():
    config = {'environ': {}}
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--range', type=int, nargs='+', default=None,
                    help='range of checkpoint values to look at')
    parser.add_argument('-u', '--use_reward', action='store_true',# default=False,
                    help="use reward instead of success rate")
    opt, trainer = setup(config, eval=True, parser=parser)
    config = trainer.config
    if opt.use_ros:
        import rospy
        print('using ros')
        rospy.init_node('eval_isaacgym')

    use_eval_data = True
    checkpt_path = config['training']['checkpoint_path']
    checkpt_name = config['training']['checkpoint_name']
    data_path = '_'.join(checkpt_path.split('/')[1:])
    if use_eval_data:
        filename = os.path.join(checkpt_path, data_path+'_eval_data.torch')
    else:
        filename = os.path.join(checkpt_path, data_path+'_train_data.torch')
    trainer._use_eval_data = use_eval_data

    full_times = 2
    max_steps = max(trainer.max_steps*full_times, 10)+2

    checkpt_range = (config['training']['save_best_after'], config['training']['epochs'])
    if opt.range is not None:
        checkpt_range = opt.range[:2]
        if len(opt.range) == 1:
            checkpt_range.append(config['training']['epochs'])


    intervals = (checkpt_range[1] - checkpt_range[0]) / 20 if (checkpt_range[1] - checkpt_range[0]) > 20 else 1

    if False:
        save_data = torch.load(filename)
        epochs = torch.arange(checkpt_range[0], checkpt_range[1]+1, intervals)
        epochs = epochs[epochs <= save_data['epochs']]
        save_data['epochs'] = epochs
        torch.save(save_data, filename)
        create_graph(checkpt_path, save_data)
        return

    print('Evaluation')

    epochs = []
    mean_list = []
    sucp_list = []
    succ_list = []
    runs_list = []
    checkpoints = []
    for epoch in torch.arange(checkpt_range[0], checkpt_range[1], intervals):
        epoch = int(epoch + 0.5)
        checkpt = os.path.join( checkpt_path, checkpt_name.format(epoch) )
        if not os.path.exists(checkpt):
            continue
        print('evaluating', epoch)
        trainer.load_model(checkpt)
        tot_rew, mean_rew, success, runs = trainer.evaluate(max_steps)
        suc_perc = success / runs

        print('mean rewards:', mean_rew)
        print('perc succ:', suc_perc)
        print('success:', success)
        print('runs:', runs)

        epochs.append(epoch)
        checkpoints.append(checkpt)
        mean_list.append(mean_rew)
        sucp_list.append(suc_perc)
        succ_list.append(success)
        runs_list.append(runs)
        save_data = {
            'epochs': epochs,
            'checkpoints': checkpoints,
            'mean_rewards': mean_list,
            'success_percents': sucp_list,
            'successes': succ_list,
            'runs': runs_list
        }
        torch.save(save_data, filename)

    if opt.use_reward:
        besti = np.argmax(save_data['mean_rewards'])
        print('best success:', besti, save_data['mean_rewards'][besti])
    else:
        besti = np.argmax(save_data['success_percents'])
        print('best success:', besti, save_data['success_percents'][besti])

    filename = os.path.join(checkpt_path, data_path+'_train_success.png')
    if use_eval_data:
        filename = os.path.join(checkpt_path, data_path+'_eval_success.png')

    create_graph(filename, save_data, data_key='success_percents')

    filename = filename.replace('success', 'rewards')
    create_graph(filename, save_data, data_key='mean_rewards')


if __name__ == '__main__':
    main()
