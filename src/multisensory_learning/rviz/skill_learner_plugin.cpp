#include <stdio.h>

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QString>
#include <QPushButton>

#include <multisensory_learning/rviz/skill_learner_plugin.h>
#include <multisensory_learning/SetVisualizationParams.h>
#include <std_srvs/Trigger.h>

namespace multisensory_learning
{
  SkillLearnerPlugin::SkillLearnerPlugin(QWidget* parent) : rviz::Panel(parent)
  {
    ros::Rate rate(100);
    ros::NodeHandle nh;
    std::vector<std::string> model_fns, data_fns;
    
    std::string set_vis_params_srv = "/skill_visualizer/set_visualization_params";
    std::string set_dist_srv = "/skill_visualizer/set_distributions";
    std::string get_plan_srv = "/skill_visualizer/get_plan";
    std::string show_samples_srv = "/skill_visualizer/show_action_samples";
    std::string run_sim_srv = "/skill_visualizer/run_simulator";
    
    ROS_INFO("Waiting for services...");
    ros::service::waitForService(set_vis_params_srv, 10000);
    ros::service::waitForService(set_dist_srv, 10000);
    ros::service::waitForService(get_plan_srv, 10000);
    ros::service::waitForService(show_samples_srv, 10000);
    ros::service::waitForService(run_sim_srv, 10000);
    if (!ros::service::exists(set_vis_params_srv, true))
      ROS_ERROR("Could not find set visualization params service in time");
    else if (!ros::service::exists(set_dist_srv, true))
      ROS_ERROR("Could not find set distributions service in time");
    else if (!ros::service::exists(get_plan_srv, true))
      ROS_ERROR("Could not find get plan service in time");
    else if (!ros::service::exists(show_samples_srv, true))
      ROS_ERROR("Could not find show samples service in time");
    else if (!ros::service::exists(run_sim_srv, true))
      ROS_ERROR("Could not find run simulator service in time");
    else
      ROS_INFO("Services found");
    
    set_vis_params_client_ = nh.serviceClient<multisensory_learning::SetVisualizationParams>(set_vis_params_srv);
    set_dist_client_ = nh.serviceClient<std_srvs::Trigger>(set_dist_srv);
    get_plan_client_ = nh.serviceClient<std_srvs::Trigger>(get_plan_srv);
    show_samples_client_ = nh.serviceClient<std_srvs::Trigger>(show_samples_srv);
    run_sim_client_ = nh.serviceClient<std_srvs::Trigger>(run_sim_srv);

    
    QHBoxLayout* push_slider_layout = new QHBoxLayout();
    push_slider_layout->addWidget(new QLabel("Push Height"));
    push_slider_ = new QSlider(Qt::Horizontal, this);
    // TODO will need to probably set dynamically from srvs
    push_slider_->setMinimum(0);
    push_slider_->setMaximum(20);
    push_slider_->setSingleStep(1);
    push_slider_layout->addWidget(push_slider_);

    QHBoxLayout* posx_var_slider_layout = new QHBoxLayout();
    posx_var_slider_layout->addWidget(new QLabel("Goal Pos-X Var"));
    posx_var_slider_ = new QSlider(Qt::Horizontal, this);
    // TODO will need to probably set dynamically from srvs
    posx_var_slider_->setMinimum(0);
    posx_var_slider_->setMaximum(20);
    posx_var_slider_->setSingleStep(1);
    posx_var_slider_layout->addWidget(posx_var_slider_);

    QHBoxLayout* posy_var_slider_layout = new QHBoxLayout();
    posy_var_slider_layout->addWidget(new QLabel("Goal Pos-Y Var"));
    posy_var_slider_ = new QSlider(Qt::Horizontal, this);
    // TODO will need to probably set dynamically from srvs
    posy_var_slider_->setMinimum(0);
    posy_var_slider_->setMaximum(20);
    posy_var_slider_->setSingleStep(1);
    posy_var_slider_layout->addWidget(posy_var_slider_);

    QHBoxLayout* posz_var_slider_layout = new QHBoxLayout();
    posz_var_slider_layout->addWidget(new QLabel("Goal Pos-Z Var"));
    posz_var_slider_ = new QSlider(Qt::Horizontal, this);
    // TODO will need to probably set dynamically from srvs
    posz_var_slider_->setMinimum(0);
    posz_var_slider_->setMaximum(20);
    posz_var_slider_->setSingleStep(1);
    posz_var_slider_layout->addWidget(posz_var_slider_);

    QHBoxLayout* btn_layout = new QHBoxLayout();
    QPushButton* dist_btn = new QPushButton("Set Distributions", this);
    QPushButton* plan_btn = new QPushButton("Get Plan", this);
    QPushButton* sample_btn = new QPushButton("Show Samples", this);
    QPushButton* run_sim_btn = new QPushButton("Run Simulator", this);
    btn_layout->addWidget(sample_btn);
    btn_layout->addWidget(dist_btn);
    btn_layout->addWidget(plan_btn);
    btn_layout->addWidget(run_sim_btn);
    
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addLayout(push_slider_layout);
    layout->addLayout(posx_var_slider_layout);
    layout->addLayout(posy_var_slider_layout);
    layout->addLayout(posz_var_slider_layout);
    layout->addLayout(btn_layout);
    setLayout(layout);

    connect(push_slider_, SIGNAL(valueChanged(int)), this, SLOT(handleUpdate()));
    connect(posx_var_slider_, SIGNAL(valueChanged(int)), this, SLOT(handleUpdate()));
    connect(posy_var_slider_, SIGNAL(valueChanged(int)), this, SLOT(handleUpdate()));
    connect(posz_var_slider_, SIGNAL(valueChanged(int)), this, SLOT(handleUpdate()));
    connect(dist_btn, SIGNAL(pressed()), this, SLOT(setDistributions()));
    connect(plan_btn, SIGNAL(pressed()), this, SLOT(getPlan()));
    connect(sample_btn, SIGNAL(pressed()), this, SLOT(showSamples()));
    connect(run_sim_btn, SIGNAL(pressed()), this, SLOT(runSimulator()));

    push_slider_->setValue(10);
    posx_var_slider_->setValue(0);
    posy_var_slider_->setValue(0);
    posz_var_slider_->setValue(0);
  }

  void SkillLearnerPlugin::handleUpdate()
  {
    multisensory_learning::SetVisualizationParams srv;
    srv.request.push_index = push_slider_->value();
    srv.request.goal_posx_var_index = posx_var_slider_->value();
    srv.request.goal_posy_var_index = posy_var_slider_->value();
    srv.request.goal_posz_var_index = posz_var_slider_->value(); 
    if (!set_vis_params_client_.call(srv))
      ROS_ERROR("Failed to call service to set visualization params");    
  }

  void SkillLearnerPlugin::setDistributions()
  {
    std_srvs::Trigger srv;
    if (!set_dist_client_.call(srv))
      ROS_ERROR("Failed to call service to set distributions");
    else
      ROS_INFO("Distributions set");
  }

  void SkillLearnerPlugin::getPlan()
  {
    std_srvs::Trigger srv;
    if (!get_plan_client_.call(srv))
      ROS_ERROR("Failed to call service to get plan");
    else
      ROS_INFO("Plan retrieved");
  }

  void SkillLearnerPlugin::showSamples()
  {
    std_srvs::Trigger srv;
    if (!show_samples_client_.call(srv))
      ROS_ERROR("Failed to call service to show samples");
    else
      ROS_INFO("Samples displayed");
  }

  void SkillLearnerPlugin::runSimulator()
  {
    std_srvs::Trigger srv;
    if (!run_sim_client_.call(srv))
      ROS_ERROR("Failed to call service to run simulator");
    else
      ROS_INFO("Ran simulator");
  }
  
}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(multisensory_learning::SkillLearnerPlugin, rviz::Panel)
