import sys
import torch
import torch.nn.functional as F
import numpy as np

from ll4ma_util import func_util, data_util, torch_util
from multisensory_learning.models import ImageEncoder, ImageDecoder, MLP, RSSM, MDN


def get_vector_encoder(modality_size, cfg):
    return  MLP(modality_size, cfg.vec_embedding_size, cfg.vec_enc_sizes,
                cfg.activation, cfg.vec_norm)


def get_vector_decoder(modality_size, input_size, cfg):
    # Using tanh assuming targets will be in normalized in range [-1,1]
    return MLP(input_size, modality_size, cfg.vec_dec_sizes, cfg.activation,
               cfg.vec_norm, out_activation='tanh')


def get_image_encoder(img_channels, cfg):
    return ImageEncoder(img_channels, cfg.img_embedding_size, cfg.img_enc_out_channels,
                        cfg.img_enc_kernel_sizes, cfg.img_enc_strides, cfg.img_enc_paddings,
                        cfg.activation, cfg.cnn_norm)


def get_image_decoder(img_channels, input_size, cfg):
    return ImageDecoder(img_channels, input_size, cfg.img_embedding_size, cfg.img_dec_out_channels,
                        cfg.img_dec_kernel_sizes, cfg.img_dec_strides, cfg.img_dec_paddings,
                        cfg.activation, cfg.img_out_activation, cfg.cnn_norm)


def get_goal_models(cfg):
    models = {}
    if cfg.train_regressor:
        models['regressor'] = MLP(cfg.multisensory_embedding_size, cfg.multisensory_embedding_size,
                                  cfg.regressor_hidden_sizes, cfg.activation, cfg.vec_norm)
    if cfg.train_mdn:
        models['mdn'] = MDN(cfg.multisensory_embedding_size, cfg.multisensory_embedding_size,
                            cfg.mdn_hidden_sizes, cfg.mdn_n_components, cfg.activation, cfg.vec_norm,
                            cfg.diag_only, cfg.diag_scalar)
    if cfg.train_classifier:
        models['classifier'] = MLP(cfg.multisensory_embedding_size, cfg.n_goals,
                                   cfg.classifier_hidden_sizes, cfg.activation, cfg.vec_norm)
    if cfg.checkpoint:
        checkpoint = torch.load(cfg.checkpoint)
        if 'models' not in checkpoint:
            ui_util.print_error_exit("Checkpoint does not have model weights to load")
        torch_util.load_state_dicts(models, checkpoint['models'])

    return models


def apply_goal_models(latent_in, latent_target, models, cfg):
    returns = {}
    if cfg.train_regressor:
        returns['regressor_out'] = models['regressor'](latent_in)
    if cfg.train_mdn:
        returns['mdn_out'] = models['mdn'](latent_in)
    if cfg.train_classifier:
        # TODO could condition on current state also (like MDN) but trying
        # with just the state being classified
        # classifier_in = torch.cat([latent_in, latent_target], dim=-1)
        classifier_in = latent_target
        returns['classifier_out'] = models['classifier'](classifier_in)
    return returns


def prepare_batch(batch, cfg):
    """
    Puts the batch on the correct device, handles time/batch dims, and anything 
    else needed to get data ready to apply models to. 

    Note we don't do data scaling here because for training the MSP model the 
    samples are pre-processed, but still need these steps of preparation.
    """
    torch_util.move_batch_to_device(batch, cfg.device)
    # Swap batch and time indices so they are (t, b, *channels)
    if 'obs' in batch:
        batch['obs'] = {k: v.transpose(0, 1) for k, v in batch['obs'].items()}
    if 'act' in batch:
        batch['act'] = {k: v.transpose(0, 1) for k, v in batch['act'].items()}


def process_data_in(data, modality, ranges, network_min=-1, network_max=1):
    """
    Process raw data into the network (resize images and get all modalities in same range)
    """
    if modality == 'rgb':
        data = data.transpose(0, 3, 1, 2) # (t, 3, h, w)
        data = torch.tensor(data, dtype=torch.float32)
        data = F.interpolate(data, (64, 64), mode='bilinear', align_corners=False) # (t, 3, 64, 64)
        data.div_(255) # Normalize to range [0, 1]
    elif modality == 'depth':
        data_min, data_max = ranges[modality]
        data = data_util.scale_min_max(data, data_min, data_max, 0, 1) # Assumes img outs have sigmoid
        data = torch.tensor(data, dtype=torch.float32).unsqueeze(1) # Add channel: (t, 1, h, w)
        data = F.interpolate(data, (64, 64), mode='bilinear', align_corners=False) # (t, 1, 64, 64)
    elif modality in ranges:
        data_min, data_max = ranges[modality]
        data = data_util.scale_min_max(data, data_min, data_max, network_min, network_max)
        data = torch.tensor(data, dtype=torch.float32)
    elif modality in ['ee_orientation']:
        # Orientation should already be in [0,1]
        data = torch.tensor(data, dtype=torch.float32)
    else:
        raise ValueError(f"Unknown modality for processing data into network: {modality}")
    return data


def process_data_out(data, modality, ranges, network_min=-1, network_max=1):
    """
    Process data coming out of network back to actual ranges
    """
    data = data.squeeze(dim=1) # TODO if you do batches then really need to view t,b in one dim
    if modality == 'rgb':
        data = F.interpolate(data, (512, 512), mode='bilinear', align_corners=False)
        data = data.cpu().detach().numpy()
        data *= 255
        data = data.astype(np.uint8)
        data = data.transpose(0, 2, 3, 1) # (t, h, w, 3)
    elif modality == 'depth':
        data = F.interpolate(data, (512, 512), mode='bilinear', align_corners=False)
        data = data.cpu().detach().numpy().squeeze(1) # Remove channel dim
        data = data_util.scale_min_max(data, 0, 1, *ranges[modality]) # Assumes sigmoid outs
    elif modality in ranges or '_obj_position' in modality or '_obj_velocity' in modality:
        if '_obj_position' in modality:
            modality = 'obj_position'
        if '_obj_velocity' in modality:
            modality = 'obj_velocity'
        data = data.cpu().detach().numpy().squeeze()
        data = data_util.scale_min_max(data, network_min, network_max, *ranges[modality])
    elif modality in ['ee_orientation']:
        data = data.cpu().detach().numpy().squeeze()  # Should already be in range [-1,1]
    else:
        raise ValueError(f"Unknown modality for processing data out of network: {modality}")
    return data


def process_chunk_in(batch, ranges):
    """
    Processes chunk of data (over time) to be suitable for input to the network
    """
    if 'obs' in batch:
        batch['obs'] = {k: process_data_in(v, k, ranges) for k, v in batch['obs'].items()}
    if 'act' in batch:
        batch['act'] = {k: process_data_in(v, k, ranges) for k, v in batch['act'].items()}
        
        
def process_chunk_out(data, ranges):
    return {k: process_data_out(v, k, ranges) for k, v in data.items()}


def process_time_batch(f, *tensors):
    """
    Applies a function to time-distributed inputs in a batch by "superbatching"
    them, i.e. taking input tensors of shape (time, batch, *in_channels) and input
    to function as (time * batch, *in_channels), then puts the output back in the
    form (time, batch, *out_channels). Useful for models like encoder/decoder/reward
    because they are not dependent on other timesteps (unlike transition model).
    """
    reshaped_inputs = []
    for x in tensors:
        x_size = x.size()
        time = x_size[0]
        batch = x_size[1]
        superbatch_size = time * batch
        reshaped_inputs.append(x.reshape(superbatch_size, *x_size[2:]))
    f_out = f(*reshaped_inputs)
    if isinstance(f_out, tuple):
        resized = []
        for out in f_out:
            resized.append(out.view(time, batch, *out.size()[1:]))
    else:
        resized = f_out.view(time, batch, *f_out.size()[1:])
    return resized
    
