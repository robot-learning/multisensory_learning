
from rl_games.common.algo_observer import AlgoObserver
from rl_games.algos_torch import torch_ext
import torch
import numpy as np
from typing import Callable

class RLGPUAlgoObserver(AlgoObserver):
    """Allows us to log stats from the env along with the algorithm running stats. """

    def __init__(self):
        pass

    def after_init(self, algo):
        self.algo = algo
        self.mean_final_scores = torch_ext.AverageMeter(
          1, self.algo.games_to_track
        ).to(self.algo.ppo_device)
        self.max_rew = None
        self.min_dis = None
        self.ep_infos = []
        self.direct_info = {}
        self.writer = self.algo.writer

    def process_infos(self, infos, done_indices):
        assert isinstance(infos, dict), "RLGPUAlgoObserver expects dict info"
        if isinstance(infos, dict):
            if 'episode' in infos:
                self.ep_infos.append(infos['episode'])

            if len(infos) > 0 and isinstance(infos, dict):  # allow direct logging from env
                self.direct_info = {}
                if self.max_rew is None:
                    self.max_rew = infos['rewards']
                else:
                    self.max_rew[:] += infos['rewards']
                if self.min_dis is None:
                    self.min_dis = infos['goal_dist']
                else:
                    self.min_dis[:] += infos['goal_dist']
                for k, v in infos.items():
                    # only log scalars
                    if ( isinstance(v, float) or isinstance(v, int)
                      or (isinstance(v, torch.Tensor) and len(v.shape) == 0) ):
                        self.direct_info[k] = v.item() if isinstance(v, torch.Tensor) else v
                for ind in done_indices:
                    self.mean_final_scores.update(infos['rewards'][ind])

    def after_clear_stats(self):
        self.mean_final_scores.clear()

    def after_print_stats(self, frame, epoch_num, total_time):

        if epoch_num % 100 == 0:
            outp = 'epoch:{:}'.format(epoch_num)
            outp = outp + ', time:{:.1f}'.format(total_time)
            # outp = outp + ', maxr:{:.2f}'.format(self.max_rew.mean())
            # outp = outp + ', mind:{:.2f}'.format(self.min_dis.mean())
            for k, v in self.direct_info.items():
                outp = outp + ', {}:{:.2f}'.format(k, v)
            print(outp)

        if self.ep_infos:
            for key in self.ep_infos[0]:
                    infotensor = torch.tensor([], device=self.algo.device)
                    for ep_info in self.ep_infos:
                        # handle scalar and zero dimensional tensor infos
                        if not isinstance(ep_info[key], torch.Tensor):
                            ep_info[key] = torch.Tensor([ep_info[key]])
                        if len(ep_info[key].shape) == 0:
                            ep_info[key] = ep_info[key].unsqueeze(0)
                        infotensor = torch.cat(
                          (infotensor, ep_info[key].to(self.algo.device))
                        )
                    value = torch.mean(infotensor)
                    self.writer.add_scalar('Episode/' + key, value, epoch_num)
            self.ep_infos.clear()

        for k, v in self.direct_info.items():
            self.writer.add_scalar(f'{k}/frame', v, frame)
            self.writer.add_scalar(f'{k}/iter', v, epoch_num)
            self.writer.add_scalar(f'{k}/time', v, total_time)

        if self.mean_final_scores.current_size > 0:
            mean_scores = self.mean_final_scores.get_mean()
            self.writer.add_scalar('scores/mean', mean_scores, frame)
            self.writer.add_scalar('scores/iter', mean_scores, epoch_num)
            self.writer.add_scalar('scores/time', mean_scores, total_time)
            # print('mean final rew:', mean_scores)
