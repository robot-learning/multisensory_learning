
from isaacgym import gymtorch, gymapi  # noqa: F401

import imageio

from isaacRL.train import *
from isaacRL.utils.grapher import EvalGrapher
import matplotlib.pyplot as plt

from ll4ma_util import file_util

from isaacRL.tasks.get_task import get_task

from isaacRL.utils.display_rgb import make_gif

def main():
    torch.set_printoptions(precision=3,sci_mode=False)

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str,
        default="configs/tests/rgb_01n_rpl00.yaml", help="path to config file")
    parser.add_argument("--headless", default=None,
        help="overwrite running headless or not")
    parser.add_argument("--checkpt", type=str, default=None,
        help="if continuing training from checkpoint model")
    opt = parser.parse_args()

    config = file_util.load_yaml(opt.config)
    config['environ']['headless'] = False
    config['environ']['num_envs'] = 1
    env = get_task(config['environ'])
    print(env)

    # max_steps = 400
    full_times = 1
    max_steps = max(config['environ']['max_env_steps']*full_times, 10) + 1
    images = []

    gifi = 1
    gifname = opt.config.rsplit('/', 1)[-1].rsplit('.', 1)[0]
    state = env.reset()
    for envstep in range(max_steps):
        state, rewards, dones, info = env.step()
        if True:
            rgb = env.get_rgb_img()
            # key = env.task_config['obs_objs'][0].rsplit('_emb', 1)[0]
            rgb = [img for img in rgb.values()][0][0]
            images.append(rgb)
        else:
            key = env.task_config['obs_objs'][0].rsplit('_emb', 1)[0]
            rgb = state['full_state'][key]
            images.append(rgb[0].reshape(224, 224, 3)*255)
        if envstep == 0:
            print('state keys:', state.keys())
            print('state shape:', state['obs'].shape)
            print('rewards shape:', rewards.shape)
            print('dones shape:', dones.shape)
            print('dones:', dones)
            print('rewards:', rewards)
        if (dones>0).any():
            print('dones:', dones)
            print('rewards:', rewards)
            # import pdb; pdb.set_trace()
            print('images:', len(images))
            make_gif(images, 'data/'+gifname+str(gifi)+'.gif')
            gifi += 1
            images = []
            break


if __name__ == '__main__':
    main()
