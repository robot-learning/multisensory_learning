#!/bin/bash

set -e

MINICONDA_DIR=$HOME/.miniconda3
ISAACGYM_ROOT=$HOME/source_code
# This is the version currently uploaded on our shared drive:
ISAACGYM_FILEID='1-Iz0_xQCgLaIYJdE9VB3MFSXjRVh4_Xv'


SCRIPT_PATH=$(pwd)
WS_ROOT=$SCRIPT_PATH/../../..


mkdir -p $ISAACGYM_ROOT


# Some apt installs
sudo apt install ros-melodic-moveit ros-melodic-trac-ik-kinematics-plugin python-catkin-tools



# Download Isaac Gym from our shared drive
function gdrive_download () {
  CONFIRM=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate "https://docs.google.com/uc?export=download&id=$1" -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')
  wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$CONFIRM&id=$1" -O $2
  rm -rf /tmp/cookies.txt
}
cd $ISAACGYM_ROOT
gdrive_download $ISAACGYM_FILEID isaacgym.zip
unzip isaacgym.zip
rm isaacgym.zip


# Initialize catkin workspace
cd $WS_ROOT
catkin init


# Clone internal repos
cd $WS_ROOT/src
git clone git@bitbucket.org:robot-learning/ll4ma_isaac.git
git clone git@bitbucket.org:robot-learning/ll4ma_util.git
git clone git@bitbucket.org:robot-learning/ll4ma_moveit.git


install_conda () {
    # Download and execute Miniconda installation script, creates Miniconda
    # environment in MINICONDA_DIR (variable should be set already).
    curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    bash Miniconda3-latest-Linux-x86_64.sh -b -p $MINICONDA_DIR
    rm Miniconda3-latest-Linux-x86_64.sh
    
    source $MINICONDA_DIR/etc/profile.d/conda.sh
    conda init
}


# Try to install conda if it's not installed and user wants it, otherwise exit
CONDA_INSTALLED=false
if ! type "conda" &> /dev/null ; then
    echo -e "\nCould not find 'conda'."
    while true; do
        read -p "Do you wish to install and initialize conda ('yes' or 'no')? " yn
        case $yn in
            [Yy]* ) install_conda; break;;
            [Nn]* ) echo "Cannot continue installation without conda. Exiting."; exit 1;;
            * ) echo "Please answer 'yes' or 'no'.";;
        esac
    done
    CONDA_INSTALLED=true
    source $MINICONDA_DIR/etc/profile.d/conda.sh
else
    source $MINICONDA_DIR/etc/profile.d/conda.sh
    conda deactivate
    conda deactivate
fi


cd $WS_ROOT/src/ll4ma_isaac/conda
conda update -y -n base -c defaults conda
conda env remove -n isaacgym
conda env create -f isaacgym_conda_env.yaml
conda activate isaacgym


cd $ISAACGYM_ROOT/isaacgym/python
pip install -e .


cd $WS_ROOT
catkin build
