
from isaacRL.tasks.gymtasks.pickplace_bandit_task import PickplaceBanditTask
from isaacRL.tasks import get_task
from isaacgymenvs.utils.utils import set_seed

from ll4ma_util import file_util

from isaacRL.testers.utils import *

import os, sys, argparse, json, yaml
import rospy
import torch
import numpy as np
from tqdm import tqdm

def main():
    torch.set_printoptions(precision=3,sci_mode=False)
    print('using ros')
    rospy.init_node('test_pickplace_task')

    # device = "cpu"

    config = file_util.load_yaml('configs/place_noise_presets/joint_01n_rpl10.yaml')
    # config = file_util.load_yaml('configs/tests/stack_traj.yaml')
    config['environ']['num_envs'] = 4
    config['environ']['headless'] = False
    # config['environ']['device'] = device # -1 if random
    # config['environ']['use_gpu'] = device == 'cuda'
    config['environ']['seed'] = -1 # if random
    config['environ']['seed'] = set_seed(
      config['environ']['seed'], torch_deterministic=False
    )
    config['environ']['act_range'] = config['model']['actor']['act_range']

    # envir = PickplaceBanditTask(config['environ'])
    envir = get_task(config['environ'])

    state = envir.reset()
    max_env_steps = envir._max_steps + 2
    for envstep in range(max_env_steps):
        state, reward, dones, info = envir.step()
        # if (reward>0).any():
        #     print('reward:', reward)
        #     print('dones:', dones)
        #     print('goal_dist:',info['goal_dist'])
        if (dones>0).any():
            print('reward:', reward)
            print('dones:', dones)
            print('goal_dist:',info['goal_dist'])
        # _, ee_pose = get_joints_and_ee_pose(envir, state)
        # print("EE Pose:", ee_pose[0].cpu())

if __name__ == '__main__':
    main()
