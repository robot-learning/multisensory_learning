

class Goalie(object):
    """docstring for Goalie."""

    def __init__(self, threshold, goal_shape=3, device='cpu', sparse=False):
        super(Goalie, self).__init__()
        self.threshold = threshold
        self.goal_shape = goal_shape
        self.device = device
        self.sparse = sparse

    @property
    def goal_size(self):
        return self.goal_shape

    @property
    def size(self):
        return self.goal_shape

    def goal_dist(self, state, state_keys, goal):
        raise NotImplementedError("goal_dist not implemented")

    def get_current(self, state, state_keys):
        raise NotImplementedError("get_current not implemented")

    def get_goal(self, state, state_keys):
        raise NotImplementedError("get_goal not implemented")

    def goal_reference(self, state, state_keys, goal, objects):
        for obj in objects:
            state[obj+'_goal'] = state[obj].clone()
            state[obj+'_goal'][:,:3] -= goal[:,:3]
        return state

    def reward(self, state, state_keys, goal):
        raise NotImplementedError("reward not implemented")
