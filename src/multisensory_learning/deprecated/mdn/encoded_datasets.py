
import sys, os
import pickle
import numpy as np
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset

from mdn.utils.process_observations import process_data_in
from mdn.oracles.above_object_oracle import AboveObjectOracle
from ll4ma_util import data_util

class EncodedDataset(Dataset):
    """docstring for EncodedDataset."""

    def __init__( self, file_paths, device="cpu", classify=False, context=3 ):

        super(EncodedDataset, self).__init__()
        with open(file_paths, 'r') as f:
            self.file_paths = [line.strip() for line in f]

        self._filename = None
        self._data = None
        self.classify = classify
        self.context = context

        self._preprocess()

    def _preprocess(self):

        print("\nPre-processing files to initialize data loader samples...")
        self.sample_opts = []
        total = 0
        goals = 0
        for filep in self.file_paths:
            with open(filep, 'rb') as f:
                data = np.load(f, allow_pickle=True).item()
                state, target = data['state'], data['targets']
                total += len(state)
                goals += target.sum()
                if self.classify:
                    self.sample_opts += [(filep, idx) for idx in range(self.context, state.shape[0])]
                else:
                    mintar = np.arange(len(state))[target].min()-5
                    self.sample_opts += [(filep, idx) for idx in range(self.context, mintar)]
        print('goals/total=prec | {}/{}={}'.format(
          goals, total, (float(goals)/total)) )
        print("Pre-processing complete\n")

    def __len__(self):
        return len(self.sample_opts)

    def __getitem__(self, index):

        filep, state_idx = self.sample_opts[index]
        if filep != self._filename:
            self._filename = filep
            with open(filep, 'rb') as f:
                self._data = np.load(f, allow_pickle=True).item()

        states, target = self._data['state'], self._data['targets']
        init = torch.from_numpy(states[self.context-1])
        state = torch.from_numpy(states[state_idx])
        if self.classify:
            target = torch.tensor([int(target[state_idx])], dtype=torch.float32)
        else:
            taridx = np.arange(len(target))[target]
            target = torch.from_numpy(states[np.random.choice(taridx)])
        return {
          'init': init,
          'state': state,
          'target': target
        }

    @property
    def num_files(self):
        return len(self.file_paths)

    def getfile(self, index):
        filep = self.file_paths[index]
        with open(filep, 'rb') as f:
            data = np.load(f, allow_pickle=True).item()

        states, target = data['state'], data['targets']
        state = torch.from_numpy(states)
        target = torch.tensor(target.astype(np.int), dtype=torch.float32)
        return {
          'state': state,
          'target': target
        }


if __name__ == '__main__':
    dataset = EncodedDataset( "data/train_autoenc.txt" )

    data = dataset[0]
    import pdb; pdb.set_trace()
