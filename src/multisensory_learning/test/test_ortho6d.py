#!/usr/bin/env python
import argparse

from multisensory_learning.learning.ortho6d_learner import Ortho6dConfig, Ortho6dLearner

from ll4ma_util import file_util, torch_util, func_util

import torch


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--checkpoint', type=str, required=True)
    args = parser.parse_args()

    file_util.check_path_exists(args.checkpoint, "Checkpoint file")
    checkpoint = torch.load(args.checkpoint)

    config = Ortho6dConfig(checkpoint=args.checkpoint)
    learner = Ortho6dLearner(config, set_eval=True)

    for _ in range(3):
        R_in = torch_util.random_rotation()
        batch = {'rotation': R_in.view(-1, 9).to(learner.device)}
        outputs = learner.apply_models(batch, min_max_scale=True)

        print("IN", R_in.numpy())
        print("OUT", outputs['rotation'].detach().cpu().numpy())
