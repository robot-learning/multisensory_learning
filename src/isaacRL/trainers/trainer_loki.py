
from isaacRL.trainers.trainer_dagger import TrainerDAGGER
from isaacRL.models.actorcritics import CriticBasic
from isaacRL.utils.buffer import BufferLOKI

import torch
import torch.nn as nn
import torch.nn.functional as F

import time
from tqdm import tqdm
import numpy as np
import os, sys, argparse, json
import os.path as osp
import rospy

class TrainerLOKI(TrainerDAGGER):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        config['model']['actor']['stochastic'] = True
        super(TrainerLOKI, self).__init__(config)

        self.actor.forward = self.actor.stochastic

        kwargs = {} if not hasattr(self.actor, 'rgb_model') \
            else {'ViT': self.actor.rgb_model, 'gap':self.actor.gap_dim}
        self.critic1 = CriticBasic(
            self.observation_size+self.action_size,
            config['model'].get('critic', {}),
            kwargs=kwargs
        )
        self.critic1.to(self.device)
        self.critic2 = CriticBasic(
            self.observation_size+self.action_size,
            config['model'].get('critic', {}),
            kwargs=kwargs
        )
        self.critic2.to(self.device)
        self.target_critic1 = self.critic1.copy()
        self.target_critic1.to(self.device)
        self.target_critic2 = self.critic2.copy()
        self.target_critic2.to(self.device)

        self.critic_optim1 = torch.optim.Adam(
            self.critic1.parameters(),  lr=self.lr, weight_decay=self.weight_decay
        )
        self.critic_optim2 = torch.optim.Adam(
            self.critic2.parameters(),  lr=self.lr, weight_decay=self.weight_decay
        )
        self.actor_optim = torch.optim.Adam(
            self.actor.parameters(),  lr=self.lr, weight_decay=self.weight_decay
        )

        self.gamma = self.config['training']['gamma']
        self.tau = self.config['training']['tau']
        self.target_update_interval = self.config['training'].get('target_update_interval', 1)

        self._actor_loss_type = self.config['training'].get('actor_loss', 'combined') # combined, random, separate

        self.alpha = torch.tensor(self.config['training']['alpha'])
        self.alpha_pause = 0
        self.alpha_optim = None
        self.alpha_tuning = False
        if self.config['training'].get('alpha_tuning', False):
            # https://github.com/pranz24/pytorch-soft-actor-critic/blob/master/sac.py#L29
            self.target_entropy = -float(self.action_size)
            self.log_alpha = torch.log(self.alpha).clone().to(self.device)
            self.log_alpha.requires_grad = True
            self.alpha_optim = torch.optim.Adam([self.log_alpha], lr=self.lr*0.05)
            self.alpha_pause = self.config['training'].get('alpha_pause', 0)
            self.alpha_tuning = self.alpha_pause == 0

        self.q_losses = []
        self.pi_losses = []

        buf_size = self.config['training'].get('max_buff_size', self.max_steps*self.num_envs)
        self.buffer = BufferLOKI(
            state_size=self.observation_size,
            action_size=self.action_size,
            device=self.device,
            storage_device='cpu',
            max_size=buf_size,
        )
        self.buffer._full_idx = self.envir.fullstate.indices

        if self._actor_loss_type == 'random':
            self._loss_prob = self.config['training'].get('actor_loss_prob', 0.98)
            self._loss_prob = torch.tensor(self._loss_prob)
            self._loss_prob.requires_grad = True
            self._loss_prob_optim = torch.optim.Adam([self._loss_prob], lr=0.03)
            self._loss_prob_min = self.config['training'].get('aloss_prob_min', 0.1)

    def train(self):

        if self.pretrained_model is not None and osp.exists(self.pretrained_model):
            self.load_pretrained(self.pretrained_model)

        self.runs = 0
        self.success = 0
        tot_rewards = 0.
        print('resetting')
        self.state = self.envir.reset()
        self.actor.train()
        self.expert.changing = self.config['training'].get('change_expert', False)

        for epoch in range(self.start_epoch, self.end_epoch):
            pbar = tqdm(total=self.ckpt_interval, file=sys.stdout)
            desc = f'Train Epoch {epoch}'
            pbar.set_description(desc)
            epoch_q_avg = 0.
            epoch_pi_avg = 0.
            for iter in range(self.ckpt_interval):
                if rospy.is_shutdown():
                    return

                # add to replays
                self.play(self.actions_per_interval)

                # learn from replays
                q_avg, pi_avg = 0., 0.
                for ui in range(self.updates_per_interval):
                    if rospy.is_shutdown():
                        return
                    qloss, pi_loss = self.update_weights(epoch*self.updates_per_interval+ui)
                    q_avg += qloss
                    pi_avg += pi_loss
                epoch_q_avg += q_avg / self.updates_per_interval
                epoch_pi_avg += pi_avg / self.updates_per_interval

                desc = f'Train E{epoch}: loss=q:{(epoch_q_avg/(iter+1)):.3f},pi:{(epoch_pi_avg/(iter+1)):.3f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            self.q_losses.append(epoch_q_avg/self.ckpt_interval)
            self.pi_losses.append(epoch_pi_avg/self.ckpt_interval)

            iters = (epoch*self.ckpt_interval)+self.start_epoch
            timesteps = self.num_envs * iters * self.actions_per_interval
            runs = timesteps / self.max_steps
            print('saving after:')
            print('{} epochs'.format(epoch))
            print('{} timesteps'.format(timesteps))

            print('mean rewards:', self.buffer.mean_rewards())
            print('buffer indx:', self.buffer.load_idx)
            print('buffer size:', len(self.buffer))

            if self._actor_loss_type == 'random':
                if self._loss_prob > self._env_prob_min:
                    prob_loss = ((self._loss_prob - self._env_prob_min)**2).sqrt()
                    self._loss_prob_optim.zero_grad()
                    prob_loss.backward()
                    self._loss_prob_optim.step()
                    print('loss prob:', self._loss_prob.item())

            runs = self.envir.runs()
            success = self.envir.successes()
            print('new runs:', runs - self.runs)
            print('new successes:', success - self.success)
            if hasattr(self.envir, 'trajopt_time'):
                print('avg trajopt time:', self.envir.trajopt_time())
            self.runs = runs
            self.success = success
            if epoch >= self.save_best_after:
                self.save_model(epoch)

    def play(self, num_actions=1, evaluate=False, store_images=False):
        if self.state is None:
            self.state = self.envir.reset()
        if store_images:
            self.rgb_images = []
        with torch.no_grad():
            tot_rew = 0.
            if evaluate:
                pbar = tqdm(total=num_actions, file=sys.stdout)
                desc = f'Eval Iter {0}'
                pbar.set_description(desc)
            for ai in range(num_actions):
                if rospy.is_shutdown():
                    return
                actions, _ = self.actor(self.state)
                actions = actions.detach()
                if not evaluate:
                    eactions = self.expert.get_actions(self.state)
                nstate, rewards, terminals, _ = self.envir.step(actions)
                if not evaluate:
                    self.buffer.add(
                        self.state, actions, eactions, nstate, rewards, terminals
                    )
                if store_images:
                    rgb_key = [key for key in nstate['full_state'].keys() if 'rgb' in key and 'emb' not in key]
                    if len(rgb_key) == 0:
                        rgb = self.envir.get_rgb_img()
                        rgb = [img for img in rgb.values()][0][0]
                        # self.envir.get_rgb_img()['test_camera_rgb'][0]
                        self.rgb_images.append(rgb)
                    else:
                        rgb = nstate['full_state'][rgb_key]
                        self.rgb_images.append(rgb[0].reshape(224, 224, 3)*255)
                self.state = nstate
                tot_rew += rewards.sum().item()
                if evaluate:
                    success = self.envir.successes()
                    desc = f'Eval Iter {ai}: rew={(tot_rew/((ai+1)*self.num_envs)):.3f},suc={success:d}'
                    pbar.set_description(desc)
                    pbar.update(1)
            if evaluate:
                pbar.close()
            return tot_rew

    def update_weights(self, update_i):
        state, action, eaction, nstate, rewards, terminals = self.buffer.sample(self.batch_size)

        with torch.no_grad():
            # Makes up second half of eq. (9)
            naction, nstate_log_pi = self.actor(nstate)
            q1_tar = self.target_critic1(nstate, naction)
            q2_tar = self.target_critic2(nstate, naction)
            # Two Q-functions to mitigate positive bias in the policy improvement step
            qvals = torch.min(q1_tar, q2_tar)
            n_qval = qvals - self.alpha * nstate_log_pi
            n_qval = rewards + (1-terminals) * self.gamma * n_qval
            n_qval = n_qval.detach()

        qval1 = self.critic1(state, action)
         # JQ = 𝔼(st,at)~D[0.5(Q1(st,at) - r(st,at) - γ(𝔼st+1~p[V(st+1)]))^2]
        q1_loss = F.mse_loss(qval1, n_qval)
        self.critic_optim1.zero_grad()
        q1_loss.backward()
        if self.clip_grad is not None:
            self.clip_grad(self.critic1.parameters())
        self.critic_optim1.step()

        qval2 = self.critic2(state, action)
        q2_loss = F.mse_loss(qval2, n_qval)
        self.critic_optim2.zero_grad()
        q2_loss.backward()
        if self.clip_grad is not None:
            self.clip_grad(self.critic2.parameters())
        self.critic_optim2.step()

        if update_i % self.target_update_interval == 0:
            self.target_critic1 = self.critic1.soft_update(self.tau, self.target_critic1)
            self.target_critic2 = self.critic2.soft_update(self.tau, self.target_critic2)

        action, log_pi = self.actor(state)

        # TODO add options to use combined dagger + SAC policy loss or
        # randomly choose between them or
        # do one loss and then the other

        if self._actor_loss_type == 'combined':
            dag_loss = F.mse_loss(action, eaction)

            qval1 = self.critic1(state, action)
            qval2 = self.critic2(state, action)
            qvals = torch.min(qval1, qval2)
            # Policy loss as defined in eq (12)
            # Jπ = 𝔼st∼D,εt∼N[α * logπ(f(εt;st)|st) − Q(st,f(εt;st))]
            pi_loss = ((self.alpha.detach() * log_pi) - qvals).mean()

            pi_loss = dag_loss + pi_loss

            self.actor_optim.zero_grad()
            pi_loss.backward()
            if self.clip_grad is not None:
                self.clip_grad(self.actor.parameters())
            self.actor_optim.step()

        elif self._actor_loss_type == 'random':
            if torch.rand((1,)) < self._loss_prob:
                # take a DAGGER step
                pi_loss = F.mse_loss(action, eaction)
            else:
                # take a SAC step
                qval1 = self.critic1(state, action)
                qval2 = self.critic2(state, action)
                qvals = torch.min(qval1, qval2)
                # Policy loss as defined in eq (12)
                # Jπ = 𝔼st∼D,εt∼N[α * logπ(f(εt;st)|st) − Q(st,f(εt;st))]
                pi_loss = ((self.alpha.detach() * log_pi) - qvals).mean()

            self.actor_optim.zero_grad()
            pi_loss.backward()
            if self.clip_grad is not None:
                self.clip_grad(self.actor.parameters())
            self.actor_optim.step()

        elif self._actor_loss_type == 'separate':

            dag_loss = F.mse_loss(action, eaction)

            self.actor_optim.zero_grad()
            dag_loss.backward()
            if self.clip_grad is not None:
                self.clip_grad(self.actor.parameters())
            self.actor_optim.step()

            qval1 = self.critic1(state, action)
            qval2 = self.critic2(state, action)
            qvals = torch.min(qval1, qval2)
            # Policy loss as defined in eq (12)
            # Jπ = 𝔼st∼D,εt∼N[α * logπ(f(εt;st)|st) − Q(st,f(εt;st))]
            pi_loss = ((self.alpha.detach() * log_pi) - qvals).mean()

            self.actor_optim.zero_grad()
            pi_loss.backward()
            if self.clip_grad is not None:
                self.clip_grad(self.actor.parameters())
            self.actor_optim.step()

        if self.alpha_tuning:
            alpha_loss = -(self.log_alpha * (log_pi + self.target_entropy).detach()).mean()

            self.alpha_optim.zero_grad()
            alpha_loss.backward()
            self.alpha_optim.step()

            self.alpha = self.log_alpha.exp()

        qloss = (q1_loss + q2_loss).item()
        pi_loss = pi_loss.item() if pi_loss is not None else 0.
        return qloss, pi_loss

    def load_model(self, checkpt):
        checkpoint = torch.load(checkpt)
        self.actor.load_state_dict(checkpoint['actor'])
        self.critic1.load_state_dict(checkpoint['critic1'])
        self.critic2.load_state_dict(checkpoint['critic2'])
        if self.critic_optim1 is not None:
            self.critic_optim1.load_state_dict(checkpoint['critic_optim1'])
            self.critic_optim2.load_state_dict(checkpoint['critic_optim2'])
            self.actor_optim.load_state_dict(checkpoint['actor_optim'])
        self.q_losses = checkpoint['qloss']
        self.pi_losses = checkpoint['pi_loss']
        # self.bestloss = checkpoint['bestloss']
        self.start_epoch = checkpoint['epochs']+1
        self.end_epoch += self.start_epoch
        if self.alpha_optim is not None and 'alpha_optim' in checkpoint:
            self.alpha_optim = checkpoint['alpha_optim']
            if 'log_alpha' in checkpoint:
                self.log_alpha = checkpoint['log_alpha']
                self.alpha = self.log_alpha.exp()
            # elif 'alpha' in checkpoint:
            #     self.log_alpha = torch.log(checkpoint['alpha']).to(self.device)
            #     self.log_alpha.requires_grad = True

    def load_pretrained(self, checkpt):
        checkpoint = torch.load(checkpt)
        self.actor.load_state_dict(checkpoint['actor'])

    def save_model(self, epoch):
        checkpoint = {}
        checkpoint['critic_optim1'] = self.critic_optim1.state_dict()
        checkpoint['critic_optim2'] = self.critic_optim2.state_dict()
        checkpoint['actor_optim'] = self.actor_optim.state_dict()
        checkpoint['actor'] = self.actor.state_dict()
        checkpoint['critic1'] = self.critic1.state_dict()
        checkpoint['critic2'] = self.critic2.state_dict()
        checkpoint['qloss'] = self.q_losses
        checkpoint['pi_loss'] = self.pi_losses
        # checkpoint['bestloss'] = self.bestloss
        checkpoint['epochs'] = epoch
        filename = os.path.join(
            self.config['training']['checkpoint_path'],
            self.config['training']['checkpoint_name'].format(epoch)
        )
        if self.alpha_optim is not None:
            checkpoint['log_alpha'] = self.log_alpha
            checkpoint['alpha_optim'] = self.alpha_optim
        torch.save(checkpoint, filename)
        print('saved to', filename)
