
from isaacgym import gymtorch, gymapi

from isaacgymenvs.tasks import isaacgym_task_map
from isaacRL.tasks.expert_bandit_task import ExpertBanditTask, ActionPlanners
from isaacRL.utils.utils import get_pinv
from isaacRL.experts.picknplace import PicknplaceExpert, PicknplaceStepExpert, PicknplaceController

import os, time
import numpy as np
import torch

class PickplaceBanditTask(ExpertBanditTask):
    """docstring for PickplaceBanditTask."""

    PLANNERS = ['trajopt', 'jacobian']

    def __init__(self, config):

        self._dof = 7
        self._max_steps = config['max_env_steps']
        config['allow_resets'] = False

        self._action_modif = config['task'].get('action_modifier', '')
        self._planner = config['task'].get('planner', 'trajopt')
        assert self._planner in ActionPlanners.PLANNERS
        def joint_names():
            return self.joint_names
        if self._planner == 'jacobian':
            expert = PicknplaceController(
                config['num_acts'],
                config['goal']['object'],
                same_starts=config['task'].get('same_starts', True),
                planner=self._planner,
                num_envs=config['num_envs'],
                joint_names=joint_names,
            )
            self._max_steps = expert.action_steps
        elif 'rpl' in self._action_modif:
            expert = PicknplaceStepExpert(
                config['num_acts'],
                config['goal']['object'],
                same_starts=config['task'].get('same_starts', True),
                planner=self._planner,
                num_envs=config['num_envs'],
                joint_names=joint_names,
            )
            self._max_steps = expert.action_steps
        else:
            expert = PicknplaceExpert(
                config['num_acts'],
                config['goal']['object'],
                same_starts=config['task'].get('same_starts', True),
                planner=self._planner,
                joint_names=joint_names,
            )

        super(PickplaceBanditTask, self).__init__(config, expert)
