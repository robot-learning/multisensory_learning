


def compute_rssm_predictions(models, data, config):
    init_belief = torch.zeros(config['batch_size'], config['rssm_belief_size'],
                              device=config['device'])
    init_state = torch.zeros(config['batch_size'], config['rssm_state_size'],
                             device=config['device'])
    rssm_out = models['forward_dynamics'](init_state, encoded_act, init_belief, encoded_obs)
