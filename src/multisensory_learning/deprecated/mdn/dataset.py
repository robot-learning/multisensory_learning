
import sys, os
import pickle
import numpy as np
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset

from mdn.utils.process_observations import process_data_in
from oracles.above_object_oracle import AboveObjectOracle
from multisensory_learning.util import data_util

# sys.path.append('..')
# from multisensory_learning.datasets.pickle_dataset import PickleDataset


class LatentDataset(Dataset):
    """docstring for LatentDataset."""

    def __init__( self, file_paths, context=3, sample_size=25, frame_skips=2,
      oracle=AboveObjectOracle, obs_mods={}, act_mods={}, device="cpu" ):

        super(LatentDataset, self).__init__()
        with open(file_paths, 'r') as f:
            self.file_paths = [line.strip() for line in f]
        self.context = context
        self.sample_size = sample_size
        self.time_steps = frame_skips
        self.oracle = oracle()

        self.data_ranges = {
            'joint_position': [-2*np.pi, 2*np.pi],
            'joint_velocity': [-3, 3],
            'joint_torque': [-30, 30],
            'delta_joint_position': [-0.3, 0.3],
            'depth': [-3, 0]
        }
        self.data_range_buffers = {
            'joint_position': 0.0,
            'joint_velocity': 0.5,
            'joint_torque': 5.0,
            'delta_joint_position': 0.1,
            'depth': 0.1
        }

        self._filename = None
        self._data = None
        self._attrs = None

        self.obs_modalities = obs_mods
        self.act_modalities = act_mods

        self._preprocess()

    def _preprocess(self):

        print("\nPre-processing files to initialize data loader samples...")
        chunk_size = self.sample_size*self.time_steps
        self.sample_opts = []
        for filep in tqdm(self.file_paths, file=sys.stdout):
            with open(filep, 'rb') as f:
                data, attrs = pickle.load(f)
                goals = self.oracle.generate_labels(data, 'box')
                goalidx = np.arange(len(goals))[goals]
                start_rng = goalidx.min()
                goal_rng = goalidx.max()
                end_rng = min(goal_rng, start_rng+chunk_size-3)
                start_rng = max(start_rng, chunk_size)
                for idx in range(start_rng+1, end_rng):
                    self.sample_opts.append( (filep, idx) )

                # Check min/max for data scaling
                for modality in self.data_ranges:
                    if modality == 'delta_joint_position':
                        joint_pos = data['joint_position'][::self.time_steps]
                        deltas = joint_pos[1:] - joint_pos[:-1]
                        data_min = np.min(deltas)
                        data_max = np.max(deltas)
                    else:
                        data_min = np.min(data[modality])
                        data_max = np.max(data[modality])
                    self.data_ranges[modality][0] = min(self.data_ranges[modality][0], float(data_min))
                    self.data_ranges[modality][1] = max(self.data_ranges[modality][1], float(data_max))

        # Post-processing to add pads around data ranges
        for modality, (low, high) in self.data_ranges.items():
            pad = self.data_range_buffers[modality]
            self.data_ranges[modality] = [low - pad, high + pad]

        print("Pre-processing complete\n")

    def __len__(self):
        return len(self.sample_opts)

    def __getitem__(self, index, single=False):
        chunk_size = self.sample_size*self.time_steps
        filep, end_idx = self.sample_opts[index]
        start_idx = end_idx - chunk_size
        # print('num frames:', (end_idx - start_idx))
        if self._filename != filep:
            with open(filep, 'rb') as f:
                self._data, self._attrs = pickle.load(f)
            self._filename = filep

        sample = self.get_latent_input(start_idx, end_idx)
        if single:
            return self.get_latent_data(sample)
        return sample

    def get_latent_data(self, sample):
        latent_samp = self.latent_model(sample)
        return latent_samp

    def get_latent_input(self, start_idx, end_idx):
        obs = {}
        act = {}
        for m in self.obs_modalities:
            obs[m] = process_data_in(
              self._data[m][start_idx:end_idx:self.time_steps],
              m, self.data_ranges )
        for m in self.act_modalities:
            if m == 'delta_joint_position':
                joint_pos = self._data['joint_position'][start_idx:end_idx:self.time_steps]
                if start_idx < self.time_steps:
                    first = joint_pos[0]
                else:
                    first = self._data['joint_position'][start_idx - self.time_steps]
                joint_pos = np.concatenate([np.expand_dims(first, 0), joint_pos], axis=0)
                act[m] = process_data_in(
                  joint_pos[1:] - joint_pos[:-1], m, self.data_ranges )
            else:
                act[m] = process_data_in(
                  self._data[m][start_idx:end_idx:self.time_steps],
                  m, self.data_ranges )
        return {'obs': obs, 'act': act}

    def collate_fn(self, batch):
        newbatch = {}
        for bkey in batch[0]:
            newbatch[bkey] = {}
            for key in batch[0][bkey]:
                newbatch[bkey][key] = torch.cat(
                  [batch[bi][bkey][key].unsqueeze(0) for bi in range(len(batch))], 0
                )
        return newbatch
        # latbatch = self.latent_model(newbatch)
        # return latbatch


class AutoencDataset(LatentDataset):
    """docstring for AutoencDataset."""

    def __init__(self, file_paths, context=3, sample_size=25, frame_skips=2,
      oracle=AboveObjectOracle, obs_mods=[], device="cpu"):
        super(AutoencDataset, self).__init__( file_paths, context,
          sample_size, frame_skips, oracle,
          obs_mods=obs_mods, act_mods=[], device="cpu" )

    def get_latent_input(self, start_idx, end_idx):
        sample = {}
        target = {}
        data, attrs = self._data, self._attrs
        for modality in self.obs_modalities:
            sample[modality] = self.process_data_in(data[modality][start_idx], modality)
            target[modality] = self.process_data_in(data[modality][end_idx], modality)
        return {'xdata':sample, 'targets':target}

    def process_data_in(self, data, modality):
        if modality == 'rgb':
            data = data.transpose(2, 0, 1) # (t, 3, h, w)
            data = torch.tensor(data, dtype=torch.float32).unsqueeze(0)
            data = F.interpolate(data, (64, 64), mode='bilinear', align_corners=False).squeeze(0)
            data.div_(255) # Normalize to range [0, 1]
        elif modality == 'depth':
            low, high = self.data_ranges[modality]
            data = data_util.scale_min_max(data, low, high, 0, 1)
            data = torch.tensor(data, dtype=torch.float32).unsqueeze(1) # Add channel: (t, 1, h, w)
            data = F.interpolate(data, (64, 64), mode='bilinear', align_corners=False) # (t, 1, 64, 64)
        elif modality in self.data_ranges:
            low, high = self.data_ranges[modality]
            data = data_util.scale_min_max(data, low, high, 0, 1)
            data = torch.tensor(data, dtype=torch.float32)
        else:
            raise ValueError(f"Unknown modality for processing data into network: {modality}")
        return data


def convert2latent(x, latent_model, sample_first=False, context=3):
    outputs = latent_model(x)
    rssm_out = outputs['rssm_out']
    # beliefs: torch.Tensor = None
    # prior_states: torch.Tensor = None
    # prior_means: torch.Tensor = None
    # prior_std_devs: torch.Tensor = None
    # posterior_states: torch.Tensor = None
    # posterior_means: torch.Tensor = None
    # posterior_std_devs: torch.Tensor = None
    if sample_first:
        # size(frames, batch_size, latent_size)
        samp_states = rssm_out.prior_states
        start = samp_states[context-1]
        target = samp_states[-1]
        return start, target

    samp_mus = rssm_out.prior_means
    start_mus = samp_mus[context-1]
    target_mus = samp_mus[-1]

    samp_std = rssm_out.prior_std_devs
    start_std = samp_std[context-1]
    target_std = samp_std[-1]

    start = torch.cat((start_mus, start_std), -1)
    target = torch.cat((target_mus, target_std), -1)
    return start, target


class DistrDataset(Dataset):

    """
    Expect a file path that contains the list of numpy files containing
    the latent distribution outputs given by the latent space learner.
    Loads each numpy file individually and extracts the distribution information.
    The ___ variable indicates whether the distribution is given as the data
    or is just sampled from for data.
    """

    def __init__(self, root_dir, file_path, context=3, sample_first=False, kargs={}):
        super(DistrDataset, self).__init__()

        self.root_dir = root_dir
        self.file_path = file_path
        self.sample_first = sample_first

        with open(file_path, 'r') as f:
            self.data = [line.strip() for line in f if line.strip() != '']

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        inst = self.data[index % len(self.lines)]
        inst = np.load(inst)
        mu, pi, sig, sig_diag = inst['mu'], inst['pi'], inst['sig'], inst['sig_diag']
        if self.sample_first:
            # sample from the given distribution
            asdf
        return mu, pi, sig, sig_diag

    def collate_fn(self, batch):
        paths, imgs, targets = list(zip(*batch))
        # Remove empty placeholder targets
        targets = [boxes for boxes in targets if boxes is not None]
        # Add sample index to targets
        for i, boxes in enumerate(targets):
            boxes[:, 0] = i
        targets = torch.cat(targets, 0)
        # Selects new image size every tenth batch
        if self.multiscale and self.batch_count % 10 == 0:
            self.img_size = random.choice(
                range(self.min_size, self.max_size + 1, 32) )
        # Resize images to input shape
        imgs = torch.stack([resize(img, self.img_size) for img in imgs])
        # imgs = torch.stack([img for img in imgs])
        self.batch_count += 1
        return paths, imgs, targets


def main():

    from torch.utils.data import DataLoader
    from utils.latent_models import LatentModel

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    modals = [ 'rgb', 'joint_position', 'joint_velocity', 'joint_torque' ]
    dataset = AutoencDataset( 'data/train_data2.txt', obs_mods=modals, device=device )
    datab = dataset[0]
    import pdb; pdb.set_trace()
    data_loader = DataLoader(
        dataset,
        batch_size=2,
        shuffle=True,
        num_workers=1,
        pin_memory=True,
    )
    data1 = next(iter(data_loader))
    import pdb; pdb.set_trace()


if __name__ == '__main__':
    main()
