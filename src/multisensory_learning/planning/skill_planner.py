#!/usr/bin/env python
import sys
import os.path as osp
import numpy as np
from scipy.spatial.transform import Rotation

from multisensory_learning.learning import SkillLearner, SkillLearnerConfig
from multisensory_learning.planning import SkillProblem, SkillSequence

from ll4ma_util import file_util, func_util, torch_util, ros_util, ui_util
from ll4ma_opt.solvers import CEM, MPPI
from ll4ma_opt.solvers.samplers import GaussianSampler

import torch


DEFAULT_CONFIG_DIR = osp.join(file_util.dir_of_file(__file__, 1), "config", "planning")


class SkillPlanner:

    def __init__(
            self,
            model_cps,
            default_config_dir=DEFAULT_CONFIG_DIR,
            default_config='push_plan.yaml',
            default_planner='cem',
            device='cpu',
            gym_metadata=None
    ):
        self.default_config_dir = default_config_dir
        self.config_fn = osp.join(default_config_dir, default_config)
        self.config = func_util.dict_to_dataclass(file_util.load_yaml(self.config_fn))
        self.default_planner = default_planner
        self.device = device
        
        learners = {}
        for skill_name, model_cp in model_cps.items():
            learner_config = SkillLearnerConfig(checkpoint=model_cp)
            learners[skill_name] = SkillLearner(learner_config, set_eval=True, device=device)
        self.skill_seq = SkillSequence(learners)
        self.problem = SkillProblem(self.skill_seq, self.config.problem)
        
        self._gym_metadata = gym_metadata if gym_metadata is not None else None


    @property
    def learners(self):
        return self.skill_seq.learners
        
    @property
    def nominal_learner(self):
        return self.skill_seq.nominal_learner

    @property
    def gym_metadata(self):
        if self._gym_metadata is not None:
            return self._gym_metadata
        else:
            return self.nominal_learner.checkpoint['args']['metadata']

    @property
    def target_object(self):
        return self.gym_metadata['task']['behavior']['behaviors'][0]['target_object']
    
    @property
    def mesh_resource(self):
        obj_data = self.gym_metadata['env']['objects'][self.target_object]
        urdf_path = osp.join(obj_data['asset_root'], obj_data['asset_filename'])
        mesh_path = ros_util.get_mesh_filename_from_urdf(urdf_path)
        return osp.join(obj_data['asset_root'], osp.basename(mesh_path))

    @property
    def env_markers(self):
        markers = []
        for idx, (obj_name, obj_config) in enumerate(self.gym_metadata['env']['objects'].items()):
            if not obj_config['fix_base_link']:
                continue
            color = color=obj_config['rgb_color']
            position = obj_config['position']
            if obj_config['object_type'] == 'box':
                marker = ros_util.get_marker_msg(
                    position,
                    obj_config['orientation'],
                    shape='cube',
                    scale=obj_config['extents'],
                    marker_id=idx,
                    color=color
                )
            elif obj_config['object_type'] == 'urdf':
                mesh_resource = f"{obj_config['asset_root']}/{obj_config['asset_filename']}"
                mesh_resource = mesh_resource.replace('.urdf', '.stl')
                marker = ros_util.get_marker_msg(
                    position,
                    Rotation.from_euler('XYZ', obj_config['euler'], degrees=True).as_quat(),
                    shape='mesh',
                    mesh_resource=mesh_resource,
                    marker_id=idx,
                    color=color
                )
            markers.append(marker)
        return markers

    def get_learner(self, name):
        if name not in self.learners:
            ui_util.print_warning(f"Learner not registered: {name}")
            return None
        else:
            return self.learners[name]
    
    def get_sampler(self):
        """
        Convenience function to get sampler for the sake of visualizing samples.
        """
        problem = SkillProblem(self.skill_seq, self.config.problem)
        problem.set_skeleton(['push'])
        mu = (problem.min_bounds + problem.max_bounds) / 2.
        sampler = GaussianSampler(problem, mu, start_iso_var=self.config.cem.var)
        return sampler
    
    def get_plan(
            self,
            init_distribution,
            goal_distribution,
            skeleton,
            goal_cost_type='kl_iproj',
            planner=None
    ):
        self.problem.set_skeleton(skeleton)
        self.problem.set_init_distribution(init_distribution)
        self.problem.set_goal_distribution(goal_distribution)
        self.problem.set_goal_cost_type(goal_cost_type)
    
        planner = planner if planner else self.default_planner
        if planner == 'cem':
            sampler = GaussianSampler(self.problem, start_iso_var=self.config.cem.var)
            self.planner = CEM(
                self.problem,
                sampler,
                self.config.cem.n_samples,
                self.config.cem.n_elite
            )
            max_iterations = self.config.cem.max_iterations
        elif planner == 'mppi':
            sampler = GaussianSampler(
                self.problem,
                start_iso_var=self.config.mppi.start_iso_var,
                end_iso_var=self.config.mppi.end_iso_var
            )
            self.planner = MPPI(
                self.problem,
                sampler,
                n_samples=self.config.mppi.n_samples
            )
            max_iterations = self.config.mppi.max_iterations
        else:
            raise ValueError(f"Unknown planner type: {config.planner}")

        # Start in middle of bounds
        mu0 = (self.problem.min_bounds + self.problem.max_bounds) / 2.
        result = self.planner.optimize(mu0, max_iterations=max_iterations, verbose=False)

        return result

    def result_to_skill_params(self, result):
        soln = torch.tensor(result.solution.squeeze()).unsqueeze(0).to(self.device)
        skill_params = self.skill_seq.vec_to_skill_params(soln)
        return skill_params
