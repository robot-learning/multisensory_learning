#!/bin/bash
#=======================================================================#
# This script trains the multisensory prediction model.                 #
#=======================================================================#

#SBATCH --job-name=train_msp
#SBATCH -o log-train_msp-%j.out-%N
#SBATCH -e %j.err-%N
#SBATCH --nodes=1
#SBATCH --time=3-00:00:00
#SBATCH --gres=gpu:1
#SBATCH --mem=64G

# ----------------------------------------------------------------------------------------

DATA_DIR=iiwa_pick_object_samples
CHECKPOINT_DIR=$HOME/msp_cp_contrastive
CHECKPOINT=""

CHECKPOINT_INTERVAL=10
BATCH_SIZE=256
N_EPOCHS=100
LEARNING_RATE=0.001

FORWARD_DYNAMICS=mlp

NEXT_LATENT_MSE=false
CONTRASTIVE=true


# ----------------------------------------------------------------------------------------

SLURM_USER=adam.conkey
SCRATCH_HOME=/scratch

# ----------------------------------------------------------------------------------------

SRC_ROOT=$HOME/multisensory_learning
SLURM_ROOT=$SRC_ROOT/src/multisensory_learning/slurm
SCRATCH_DIR=$SCRATCH_HOME/$SLURM_USER
DATA_DIR=$SCRATCH_DIR/$DATA_DIR

# ----------------------------------------------------------------------------------------

# Build up flags based on settings
FLAGS="--data_dir ${DATA_DIR} --checkpoint_dir ${CHECKPOINT_DIR}"
FLAGS="${FLAGS} --params {'checkpoint_interval': ${CHECKPOINT_INTERVAL, 'batch_size': ${BATCH_SIZE}, 'n_epochs': ${N_EPOCHS}, 'forward_dynamics': ${FORWARD_DYNAMICS}, 'next_latent_mse': ${NEXT_LATENT_MSE}, 'contrastive': ${CONTRASTIVE}}"

echo
echo "FLAGS: ${FLAGS}"

# ---------------------------------------------------------------------------------------

source $HOME/.bashrc

conda activate isaacgym

python -c "import slurm_helper; slurm_helper.print_timed_msg('Job Started.')"

cd $SRC_ROOT
python setup.py install

cd $SRC_ROOT/src/multisensory_learning/train
python train_multisensory_prediction.py $FLAGS

cd $SLURM_ROOT
python -c "import slurm_helper; slurm_helper.print_timed_msg('Job Ended.')"

# ---------------------------------------------------------------------------------------
