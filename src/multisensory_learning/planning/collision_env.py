import sys
import numpy as np
import torch
import time
from multiprocessing import Process, Manager

import trimesh
from trimesh.collision import CollisionManager
from trimesh.scene import Scene

from ll4ma_util import ros_util, vis_util, ui_util


class CollisionEnvironment:

    def __init__(self, colors=vis_util.random_colors(8)):
        self.scene = Scene()
        self.manager = CollisionManager()
        self.meshes = {}
        self.transforms = {}  # TODO not sure yet if necessary, but doing bookkeeping of static objects
        self.colors = [vis_util.get_color(c) if isinstance(c, str) else c for c in colors]
        self.color_idx = 0

    def add_object(
            self,
            name,
            mesh,
            transform=None,
            color=None,
            make_convex=False,
            bounding_box=False,
            add_as_collision=True
    ):
        if isinstance(mesh, str):
            if mesh.startswith("package://"):
                mesh = ros_util.resolve_ros_package_path(mesh)
            mesh = trimesh.load(mesh)  # Assuming it's the mesh filename
        if make_convex:
            mesh = trimesh.convex.convex_hull(mesh)
        if bounding_box:
            mesh = mesh.bounding_box
        if color is None:
            mesh.visual.face_colors = self.colors[self.color_idx % len(self.colors)]
            self.color_idx += 1
        else:
            mesh.visual.face_colors = color
        if add_as_collision:
            self.scene.add_geometry(mesh, geom_name=name, transform=transform)
            self.manager, _ = trimesh.collision.scene_to_collision(self.scene)
        self.meshes[name] = mesh
        self.transforms[name] = transform

    def remove_object(self, name):
        self.manager.remove_object(name)
        del self.meshes[name]
        if name in self.transforms:
            del self.transforms[name]

    def get_mesh(self, name):
        if name not in self.meshes:
            ui_util.print_warning(f"Mesh not registered: {name}")
            return None
        else:
            return self.meshes[name].copy()
            
    def show(self, tfs={}):
        """
        Visualize the environment in its nominal state.
        """
        for key, tf in tfs.items():
            if key not in self.scene.graph.nodes_geometry:
                ui_util.print_warning(f"'{key}' not in scene graph, not setting transform")
            else:
                self.scene.graph[key] = tf        
        self.scene.show()
        
    def in_collision(
            self,
            name,
            transforms,
            only_objects=[],
            n_processes=1,
            time_it=False
    ):
        if name not in self.meshes:
            raise ValueError(f"Unregistered object for collision check: {name}")
        if time_it:
            start = time.time()
        if torch.is_tensor(transforms):
            transforms = transforms.numpy()
        elif isinstance(transforms, list):
            transforms = np.array(transforms)
            
        if n_processes > 1:
            proc_manager = Manager()
            return_dict = proc_manager.dict()
            procs = []
            proc_transforms = np.array_split(transforms, n_processes)
            for proc_idx in range(n_processes):
                proc = Process(
                    target=self._check_collisions,
                    args=(name, proc_transforms[proc_idx], proc_idx, return_dict)
                )
                proc.start()
                procs.append(proc)
            for proc in procs:
                proc.join()
            results = []
            for proc_idx in range(n_processes):
                results += return_dict[proc_idx]
        else:
            results = []
            for T in transforms:
                is_collision, names = self.manager.in_collision_single(
                    mesh=self.meshes[name],
                    transform=T,
                    return_names=True
                )
                if only_objects and is_collision and not any([n in names for n in only_objects]):
                    is_collision = False
                # print("NAMES", names, "COLL", is_collision)
                results.append(is_collision)
        if time_it:
            print("TIME:", time.time() - start)
        return results

    def _check_collisions(self, name, transforms, proc_idx, return_dict):
        return_dict[proc_idx] = [
            self.manager.in_collision_single(self.meshes[name], T) for T in transforms
        ]
        

if __name__ == '__main__':
    env = CollisionEnvironment()
    m1 = trimesh.creation.box([1]*3)
    m2 = trimesh.creation.box([1]*3)
    env.add_object('m1', m1)
    env.add_object('m2', m2)

    # T = np.eye(4)
    # T[:3,3] = np.random.randn(3)
    # if env.in_collision('m2', [T])[0]:
    #     m1.visual.face_colors = [255, 0, 0, 255]
    #     m2.visual.face_colors = [255, 0, 0, 255]
    # else:
    #     m1.visual.face_colors = [0, 255, 0, 255]
    #     m2.visual.face_colors = [0, 255, 0, 255]
    # m2.apply_transform(T)        
    # trimesh.util.concatenate([m1, m2]).show()

    B = 100
    Ts = torch.eye(4).unsqueeze(0).repeat(B, 1, 1)
    Ts[:,:3,3] = torch.randn(B, 3)
    print(env.in_collision('m2', Ts))
