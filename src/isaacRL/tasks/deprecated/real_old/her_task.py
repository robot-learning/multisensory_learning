
from isaacgym import gymtorch
from isaacgym.torch_utils import tensor_clamp

from isaacRL.tasks.rl_task import RLTask
from isaacRL.tasks.her_goalies import (
  ObjectMoveHER,
  StackPlaceHER,
  StackHER
)
from isaacRL.utils.data_utils import batch_dict

from collections import defaultdict
import numpy as np
import torch


class HERTask(RLTask):
    """docstring for HERTask."""

    def __init__(self, config, device):
        self.goalie = None
        self.goals = None
        super(HERTask, self).__init__(config, device)

        self.goalie = None
        if 'maxdist' not in config['goal']:
            config['goal']['maxdist'] = 1.0
        if 'randrange' not in config['goal']:
            config['goal']['randrange'] = None
        if config['goal']['type'] == 'ObjectMove':
            self.goalie = ObjectMoveHER(
              config['goal']['object'],
              offset=config['goal']['offset'],
              threshold=config['goal']['threshold'],
              randrange=config['goal']['randrange'],
              maxdist=config['goal']['maxdist'],
              sparse=self.sparse_rewards
            )
        elif config['goal']['type'] == 'StackPlace':
            if 'release' not in config['goal']:
                config['goal']['release'] = False
            self.goalie = StackPlaceHER(
              config['goal']['object'],
              config['goal']['base_obj'],
              offset=config['goal']['offset'],
              threshold=config['goal']['threshold'],
              maxdist=config['goal']['maxdist'],
              sparse=self.sparse_rewards,
              release=config['goal']['release']
            )
        elif config['goal']['type'] == 'StackPlaceShaped':
            if 'release' not in config['goal']:
                config['goal']['release'] = False
            if 'part_ws' not in config['goal']:
                config['goal']['part_ws'] = [0.7, 1.0]
            self.goalie = StackPlaceShapedHER(
              config['goal']['object'],
              config['goal']['base_obj'],
              offset=config['goal']['offset'],
              threshold=config['goal']['threshold'],
              maxdist=config['goal']['maxdist'],
              release=config['goal']['release'],
              part_ws=config['goal']['part_ws']
            )
        elif config['goal']['type'] == 'Stack':
            self.goalie = StackHER(
              config['goal']['objects'],
              config['goal']['place_loc'],
              threshold=config['goal']['threshold'],
              randrange=config['goal']['randrange'],
              maxdist=config['goal']['maxdist'],
              sparse=self.sparse_rewards
            )
        else:
            print('unkown goal:', config['goal']['type'])

    def update_rewards(self):
        self.rewards, self.terminals = self.get_rewards(
          self.torch_state, self.torch_state['goal'] )
        return self.rewards, self.terminals

    def get_rewards(self, state, goal):
        rewards = torch.zeros_like(self.rewards)
        terminals = torch.zeros_like(self.terminals)
        for envi in range(self.num_envs):
            score, isgoal = self.goalie.reward(state, goal[envi], envi)
            rewards[envi] = float(isgoal) if self.sparse_rewards else score
            terminals[envi] = int(isgoal)
        return rewards, terminals

    def get_env_state(self, envi):
        envstate = super().get_env_state(envi)
        if self.goals is not None:
            envstate['goal'] = self.goals[envi]
        return envstate

    def reset(self):
        super().reset()
        if self.goalie is not None:
            self.goals = self.goalie.extract_goals(self.torch_state)
            for envi, state in enumerate(self.state):
                state['goal'] = self.goals[envi]
            self.torch_state = batch_dict(self.state, self.device)
        return self.torch_state

    def reset_indices(self, terminals):
        super().reset_indices(terminals)
        if self.goalie is not None:
            reset_idx = torch.arange(self.num_envs)[terminals>0]
            self.goals[reset_idx] = self.goalie.extract_goals(self.torch_state)[reset_idx]
            for envi in reset_idx:
                self.state[envi]['goal'] = self.goals[envi]
            self.torch_state = batch_dict(self.state, self.device)
        return self.torch_state
