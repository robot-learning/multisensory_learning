
import numpy as np
import os
import torch

from isaacgymenvs.utils.torch_jit_utils import *
from isaacgymenvs.tasks.base.vec_task import VecTask
from isaacgym import gymtorch
from isaacgym import gymapi


class GeneralFranka(VecTask):

    def __init__(self, cfg, sim_device, graphics_device_id, headless):
        self.cfg = cfg
        self.robot = self.cfg['robot']
        self.num_robots = 1
        self.objects = self.cfg['objects']
        obj_names = self.objects.keys()
        self.env_obj_data = [{obj_name: {} for obj_name in obj_names}
                             for _ in range(self.cfg['env']['numEnvs'])]
        self.num_objects = len(self.objects)
        self.goalie = self.cfg['goalie']
        self.additive = self.cfg['add_actions'] if 'add_actions' in self.cfg else False
        self.cartesian = self.cfg['cartesian'] if 'cartesian' in self.cfg else False

        self.max_episode_length = self.cfg["env"]["episodeLength"]

        self.action_scale = self.cfg["env"]["actionScale"]

        self.dof_vel_scale = self.cfg["env"]["dofVelocityScale"]

        self.debug_viz = self.cfg["env"]["enableDebugVis"]

        self.up_axis = "z"
        self.up_axis_idx = 2

        self.distX_offset = 0.04
        self.dt = 1/60.

        super().__init__(
          config=self.cfg, sim_device=sim_device,
          graphics_device_id=graphics_device_id, headless=headless
        )

        # get gym GPU state tensors
        actor_root_state_tensor = self.gym.acquire_actor_root_state_tensor(self.sim)
        dof_state_tensor = self.gym.acquire_dof_state_tensor(self.sim)
        rigid_body_tensor = self.gym.acquire_rigid_body_state_tensor(self.sim)

        self.gym.refresh_actor_root_state_tensor(self.sim)
        self.gym.refresh_dof_state_tensor(self.sim)
        self.gym.refresh_rigid_body_state_tensor(self.sim)

        # create some wrapper tensors for different slices
        self.franka_default_dof_pos = to_torch([1.157, -1.066, -0.155, -2.239, -1.841, 1.003, 0.469, 0.035, 0.035], device=self.device)
        self.dof_state = gymtorch.wrap_tensor(dof_state_tensor)
        self.franka_dof_state = self.dof_state.view(self.num_envs, -1, 2)[:, :self.num_franka_dofs]
        self.franka_dof_pos = self.franka_dof_state[..., 0]
        self.franka_dof_vel = self.franka_dof_state[..., 1]

        self.rigid_body_states = gymtorch.wrap_tensor(
          rigid_body_tensor
        ).view(self.num_envs, -1, 13)

        self.franka_ee = self.rigid_body_states[:, self.hand_handle]

        self.object_state = self.rigid_body_states[:, self.num_franka_bodies:]

        self.root_state_tensor = gymtorch.wrap_tensor(
          actor_root_state_tensor
        ).view(self.num_envs, -1, 13)

        self.object_roots = self.root_state_tensor[:,1:]

        self.num_dofs = self.gym.get_sim_dof_count(self.sim) // self.num_envs
        self.franka_dof_targets = torch.zeros(
          (self.num_envs, self.num_franka_dofs),
          dtype=torch.float, device=self.device
        )

        # get the indices for all objects
        self.global_indices = torch.arange(
          self.num_envs * (self.num_robots + self.num_objects),
          dtype=torch.int32, device=self.device
        ).view(self.num_envs, -1)

        self.reset(torch.arange(self.num_envs, device=self.device))

    def create_sim(self):
        self.sim_params.up_axis = gymapi.UP_AXIS_Z
        self.sim_params.gravity.x = 0
        self.sim_params.gravity.y = 0
        self.sim_params.gravity.z = -9.81
        self.sim = self.gym.create_sim(
          self.device_id, self.graphics_device_id,
          self.physics_engine, self.sim_params
        )
        self._create_ground_plane()
        self._create_envs( self.num_envs,
          self.cfg["env"]['envSpacing'], int(np.sqrt(self.num_envs))
        )

    def _create_ground_plane(self):
        plane_params = gymapi.PlaneParams()
        plane_params.normal = gymapi.Vec3(0.0, 0.0, 1.0)
        self.gym.add_ground(self.sim, plane_params)

    def _create_assets(self):
        # setup object assets
        self.num_object_bodies = 0
        self.num_object_shapes = 0
        self.num_object_dofs = 0
        for obj_name, obj_cfg in self.objects.items():
            asset_options = gymapi.AssetOptions()
            asset_options.density = obj_cfg.density
            asset_options.fix_base_link = obj_cfg.fix_base_link
            if obj_cfg.object_type == 'box':
                asset = self.gym.create_box( self.sim, obj_cfg.x_extent,
                  obj_cfg.y_extent, obj_cfg.z_extent, asset_options
                )
            elif obj_cfg.object_type == 'urdf':
                asset_options.override_com = True
                asset_options.override_inertia = True
                asset_root = ros_util.resolve_ros_package_path(obj_cfg.asset_root)
                asset = self.gym.load_asset(self.sim, asset_root, obj_cfg.asset_filename, asset_options)
            else:
                raise ValueError(f"Unknown object type for simulation: {obj_cfg.object_type}")
            obj_cfg.asset = asset
            obj_cfg.bodies = self.gym.get_asset_rigid_body_count(asset)
            obj_cfg.shapes = self.gym.get_asset_rigid_shape_count(asset)
            obj_cfg.dofs = self.gym.get_asset_dof_count(asset)
            obj_cfg.dof_props = self.gym.get_asset_dof_properties(asset)
            for i in range(obj_cfg.dofs):
                obj_cfg.dof_props['damping'][i] = 10.0

            self.num_object_bodies += obj_cfg.bodies
            self.num_object_shapes += obj_cfg.shapes
            self.num_object_dofs += obj_cfg.dofs

            obj_start_pose = gymapi.Transform()
            px = self.objects[obj_name].position_x
            py = self.objects[obj_name].position_y
            pz = self.objects[obj_name].position_z
            obj_start_pose.p = gymapi.Vec3(px, py, pz)
            self.objects[obj_name].start_pose = obj_start_pose
            self.objects[obj_name].start_noise = 0

    def _create_robot_assets(self):
        asset_root = "../../assets"
        franka_asset_file = "urdf/franka_description/robots/franka_panda.urdf"

        if "asset" in self.cfg["env"]:
            asset_root = self.cfg["env"]["asset"].get("assetRoot", asset_root)
            franka_asset_file = self.cfg["env"]["asset"].get("assetFileNameFranka", franka_asset_file)

        asset_options = gymapi.AssetOptions()
        asset_options.flip_visual_attachments = True
        asset_options.fix_base_link = True
        asset_options.collapse_fixed_joints = True
        asset_options.disable_gravity = True
        asset_options.thickness = 0.001
        asset_options.default_dof_drive_mode = gymapi.DOF_MODE_POS
        asset_options.use_mesh_materials = True
        franka_asset = self.gym.load_asset(
          self.sim, asset_root, franka_asset_file, asset_options
        )

        franka_dof_stiffness = to_torch(
          [400, 400, 400, 400, 400, 400, 400, 1.0e6, 1.0e6],
          dtype=torch.float, device=self.device
        )
        franka_dof_damping = to_torch(
          [80, 80, 80, 80, 80, 80, 80, 1.0e2, 1.0e2],
          dtype=torch.float, device=self.device
        )

        self.num_franka_bodies = self.gym.get_asset_rigid_body_count(franka_asset)
        self.num_franka_shapes = self.gym.get_asset_rigid_shape_count(franka_asset)
        self.num_franka_dofs = self.gym.get_asset_dof_count(franka_asset)

        # set franka dof properties
        franka_dof_props = self.gym.get_asset_dof_properties(franka_asset)
        self.franka_dof_lower_limits = []
        self.franka_dof_upper_limits = []
        for i in range(self.num_franka_dofs):
            franka_dof_props['driveMode'][i] = gymapi.DOF_MODE_POS
            if self.physics_engine == gymapi.SIM_PHYSX:
                franka_dof_props['stiffness'][i] = franka_dof_stiffness[i]
                franka_dof_props['damping'][i] = franka_dof_damping[i]
            else:
                franka_dof_props['stiffness'][i] = 7000.0
                franka_dof_props['damping'][i] = 50.0

            self.franka_dof_lower_limits.append(franka_dof_props['lower'][i])
            self.franka_dof_upper_limits.append(franka_dof_props['upper'][i])

        self.franka_dof_lower_limits = to_torch(
          self.franka_dof_lower_limits, device=self.device )
        self.franka_dof_upper_limits = to_torch(
          self.franka_dof_upper_limits, device=self.device )
        self.franka_dof_speed_scales = torch.ones_like(
          self.franka_dof_lower_limits )
        self.franka_dof_speed_scales[[7, 8]] = 0.1
        franka_dof_props['effort'][7] = 200
        franka_dof_props['effort'][8] = 200

        franka_start_pose = gymapi.Transform()
        franka_start_pose.p = gymapi.Vec3(0.0, 0.0, 0.4025)
        franka_start_pose.r = gymapi.Quat(0.0, 0.0, 0.0, 1.0)

        return franka_asset, franka_dof_props, franka_start_pose

    def _create_envs(self, num_envs, spacing, num_per_row):
        lower = gymapi.Vec3(-spacing, 0.0, -spacing)
        upper = gymapi.Vec3(spacing, spacing, spacing)

        # create assets
        self._create_assets()

        # load franka asset
        franka_asset, franka_dof_props, franka_start_pose = self._create_robot_assets()

        # compute aggregate size
        max_agg_bodies = self.num_franka_bodies + self.num_object_bodies
        max_agg_shapes = self.num_franka_shapes + self.num_object_shapes

        self.frankas = []
        self.envs = []
        self.object_defaults = torch.zeros(
          (self.num_envs, self.num_objects, 13), device=self.device
        )

        for envi in range(self.num_envs):
            # create env instance
            env_ptr = self.gym.create_env(
                self.sim, lower, upper, num_per_row
            )

            self.gym.begin_aggregate(
              env_ptr, max_agg_bodies, max_agg_shapes, True
            )

            franka_actor = self.gym.create_actor(
              env_ptr, franka_asset, franka_start_pose, "franka", envi, 2, 0
            )
            self.gym.set_actor_dof_properties(
              env_ptr, franka_actor, franka_dof_props
            )

            for oi, obj_name in enumerate(self.objects):
                obj_pose = self.objects[obj_name].start_pose
                obj_pose.p.x += self.objects[obj_name].start_noise * (np.random.rand() - 0.5)
                dz = 0.5 * np.random.rand()
                dy = np.random.rand() - 0.5
                obj_pose.p.y += self.objects[obj_name].start_noise * dy
                obj_pose.p.z += self.objects[obj_name].start_noise * dz
                obj_actor = self.gym.create_actor(
                  env_ptr, self.objects[obj_name].asset,
                  obj_pose, obj_name, envi, 0, 0
                )
                self.object_defaults[envi, oi] = torch.tensor( [
                  obj_pose.p.x, obj_pose.p.y, obj_pose.p.z,
                  obj_pose.r.x, obj_pose.r.y, obj_pose.r.z,
                  obj_pose.r.w, 0, 0, 0, 0, 0, 0
                ], device=self.device )

            self.gym.end_aggregate(env_ptr)

            self.envs.append(env_ptr)
            self.frankas.append(franka_actor)

        self.hand_handle = self.gym.find_actor_rigid_body_handle(env_ptr, franka_actor, "panda_link7")

    def compute_reward(self):
        goals = self.goalie.extract_goals(self.get_env_state())
        self.state['goal'] = goals
        self.rew_buf[:], self.reset_buf[:] = self.goalie.reward(self.state, goals)
        self.reset_buf = torch.where(
          self.progress_buf >= self.max_episode_length - 1,
          torch.ones_like(self.reset_buf), self.reset_buf
        )

    def compute_observations(self):

        self.gym.refresh_actor_root_state_tensor(self.sim)
        self.gym.refresh_dof_state_tensor(self.sim)
        self.gym.refresh_rigid_body_state_tensor(self.sim)

        dof_pos_scaled = (
          2.0 * (self.franka_dof_pos - self.franka_dof_lower_limits)
          / (self.franka_dof_upper_limits - self.franka_dof_lower_limits) - 1.0
        )

        obj_idx = [oi for oi, obj_name in enumerate(self.objects) if obj_name != 'table']
        self.obs_buf = torch.cat( (
          dof_pos_scaled,
          self.franka_dof_vel * self.dof_vel_scale,
          self.object_state[:,obj_idx,:3].flatten(1),
        ), dim=-1 )
        self.state = self.get_env_state()
        return self.obs_buf

    def reset(self, env_ids):
        env_ids_int32 = env_ids.to(dtype=torch.int32)

        # reset franka
        pos = tensor_clamp(
          self.franka_default_dof_pos.unsqueeze(0) + 0.25 *
          (
            torch.rand( (len(env_ids), self.num_franka_dofs),
            device=self.device ) - 0.5
          ),
          self.franka_dof_lower_limits, self.franka_dof_upper_limits
        )
        self.franka_dof_pos[env_ids, :] = pos
        self.franka_dof_vel[env_ids, :] = torch.zeros_like(
          self.franka_dof_vel[env_ids] )
        self.franka_dof_targets[env_ids, :self.num_franka_dofs] = pos

        # reset objects
        self.object_roots[env_ids, 1:] = self.object_defaults[env_ids, 1:]

        obj_indices = self.global_indices[env_ids, 2:].flatten()
        self.gym.set_actor_root_state_tensor_indexed(
          self.sim, gymtorch.unwrap_tensor(self.root_state_tensor), gymtorch.unwrap_tensor(obj_indices), len(obj_indices)
        )

        self.gym.set_dof_position_target_tensor(
          self.sim, gymtorch.unwrap_tensor(self.franka_dof_targets)
        )

        self.gym.set_dof_state_tensor(
          self.sim, gymtorch.unwrap_tensor(self.dof_state)
        )

        self.progress_buf[env_ids] = 0
        self.reset_buf[env_ids] = 0

    def pre_physics_step(self, actions):
        self.actions = actions.clone().to(self.device)
        targets = ( self.franka_dof_targets[:, :self.num_franka_dofs] +
          self.franka_dof_speed_scales * self.dt *
          self.actions * self.action_scale
        )
        self.franka_dof_targets[:, :self.num_franka_dofs] = tensor_clamp(
          targets, self.franka_dof_lower_limits, self.franka_dof_upper_limits
        )
        self.gym.set_dof_position_target_tensor(
          self.sim, gymtorch.unwrap_tensor(self.franka_dof_targets)
        )

    def post_physics_step(self):
        self.progress_buf += 1

        env_ids = self.reset_buf.nonzero(as_tuple=False).squeeze(-1)
        if len(env_ids) > 0:
            self.reset(env_ids)

        self.compute_observations()
        self.compute_reward()

    def get_env_state(self):
        """
        Get values from the environment that we can use for learning
        """
        envstate = {}
        envstate['joint_position'] = self.franka_dof_pos.clone()
        envstate['joint_velocity'] = self.franka_dof_vel.clone()
        # envstate['joint_torque'] = self.robot_dof_for.clone()
        envstate['prev_actions'] = self.actions.clone()
        envstate['ee_state'] = self.franka_ee.clone()

        obji = 0
        for obj_name, obj_cfg in self.objects.items():
            envstate[obj_name] = self.object_state[
              :, obji:(obji+obj_cfg.bodies), :
            ].clone().flatten(1)
            obji += obj_cfg.bodies
            # add noise
            # noise = torch.randn((noise.shape[0],)) * obj_cfg.noise
            # envstate[obj_name][:len(noise)] += noise
        return envstate
