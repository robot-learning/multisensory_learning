
from isaacgym import gymtorch, gymapi  # noqa: F401
from isaacRL.train import *
from isaacRL.utils.grapher import EvalGrapher
import matplotlib.pyplot as plt

from ll4ma_util import file_util

from isaacRL.tasks.gymtasks.ll4ma_rl_tasks import Ll4maRLTask

def main():
    torch.set_printoptions(precision=3,sci_mode=False)
    # config = 'configs/place_noise_presets/joint_01n_rpla12.yaml'
    config = 'configs/place_rgb/joint_01n_rpl02.yaml'
    config = file_util.load_yaml(config)
    env = Ll4maRLTask(config['environ'])
    print(env)

    max_steps = 200

    state = env.reset()
    for envstep in range(max_steps):
        state, reward, dones, info = env.step()
        if envstep == 0:
            print('state keys:', state.keys())
            print('state shape:', state['obs'].shape)
            print('reward shape:', reward.shape)
            print('dones shape:', dones.shape)


    # config = {'environ': {}}
    # opt, trainer = setup(config, eval=True)
    # config = trainer.config
    # trainer._use_eval_data = True
    # if opt.use_ros:
    #     import rospy
    #     print('using ros')
    #     rospy.init_node('test_env')
    #
    # if opt.checkpt is not None:
    #     trainer.load_model(opt.checkpt)
    #
    # full_times = 10
    # max_steps = max(trainer.max_steps*full_times, 10) + 1#+(full_times-1)
    # envir = trainer.envir
    #
    # envir.reset()
    # for envstep in range(max_steps):
    #     state, reward, dones, info = envir.step()
    #     state, state_idx = state['fullstate'], state['state_idx']
    #     if 'rgb' in state_idx and envstep == 0:
    #         rgb = state[:,state_idx['rgb']]
    #         rgb = rgb.reshape(envir.num_envs,envir.img_width,envir.img_height,3)
    #         plt.imshow(rgb[0].cpu().numpy()/255.)
    #         plt.savefig('checkpoints/rgb_test_img.png')
    #
    # if opt.use_ros:
    #     import rospy
    #     rospy.is_shutdown()


if __name__ == '__main__':
    main()
