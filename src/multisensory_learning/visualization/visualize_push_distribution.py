#!/usr/bin/env python
import os
import sys
import rospy
import numpy as np
from tqdm import tqdm

from geometry_msgs.msg import PoseStamped
from visualization_msgs.msg import MarkerArray

from ll4ma_util import file_util, ros_util, ui_util


if __name__ == '__main__':
    """
    Visualizes the distribution of object poses after push. Note this is a hacked
    script and not meant to be generally useful to anyone.
    """
    rospy.init_node('visualizer_push_distribution')

    data_dir = rospy.get_param("~data_dir")
    clear_cache = rospy.get_param("~clear_cache", False)
    high_push_only = rospy.get_param("~high_push_only", False)
    low_push_only = rospy.get_param("~low_push_only", False)
    train_mdn = rospy.get_param("~train_mdn", False)
    use_gmm = rospy.get_param("~use_gmm", False)

    file_util.check_path_exists(data_dir, "Data directory")

    mesh_resource = "package://ll4ma_isaacgym/src/ll4ma_isaacgym/assets/cleaner.stl"

    cache_filename = os.path.join(data_dir, '.push_visualization_data.pickle')

    if clear_cache:
        file_util.safe_remove_path(cache_filename)
    
    if os.path.exists(cache_filename) and not clear_cache:
        rospy.logwarn("Using cached data")
        push_data = file_util.load_pickle(cache_filename)
    else:
        push_data = {
            'start_obj_pos': [],
            'start_obj_quat': [],
            'end_obj_pos': [],
            'end_obj_quat': [],
            'push_height': []
        }

        for filename in tqdm(file_util.list_dir(data_dir, '.pickle')):
            data, attrs = file_util.load_pickle(filename)
            push_data['start_obj_pos'].append(data['objects']['cleaner']['position'][0])
            push_data['start_obj_quat'].append(data['objects']['cleaner']['orientation'][0])
            push_data['end_obj_pos'].append(data['objects']['cleaner']['position'][-1])
            push_data['end_obj_quat'].append(data['objects']['cleaner']['orientation'][-1])
            obj_height = data['objects']['cleaner']['position'][0,2]
            ee_height = data['ee_position'][-1,2]
            push_data['push_height'].append(ee_height - obj_height)
            
        file_util.save_pickle(push_data, cache_filename)


    actual = MarkerArray()
    learned = MarkerArray()
    marker_idx = 0
    for i in range(len(next(iter(push_data.values())))):
        threshold = 0.15 # TODO right now they should be either around 0.1 or 0.2
        push_height = push_data['push_height'][i]
        if high_push_only and push_height < threshold:
            continue
        if low_push_only and push_height >= threshold:
            continue

        # if push_height < threshold:
        #     print("LOW", push_height)
        # if push_height >= threshold:
        #     print("HIGH", push_height)        
            
        actual.markers.append(ros_util.get_marker_msg(push_data['start_obj_pos'][i],
                                                      orientation=push_data['start_obj_quat'][i],
                                                      shape='mesh', mesh_resource=mesh_resource,
                                                      marker_id=marker_idx, color='mediumseagreen',
                                                      alpha=0.1))
        marker_idx += 1
        actual.markers.append(ros_util.get_marker_msg(push_data['end_obj_pos'][i],
                                                      orientation=push_data['end_obj_quat'][i],
                                                      shape='mesh', mesh_resource=mesh_resource,
                                                      marker_id=marker_idx, color='dodgerblue',
                                                      alpha=0.2))
        marker_idx += 1

    actual_marker_pub = rospy.Publisher('/actual_obj_poses', MarkerArray, queue_size=1)
    learned_marker_pub = rospy.Publisher('/learned_obj_poses', MarkerArray, queue_size=1)

    rospy.loginfo("Publishing marker msgs...")
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        actual_marker_pub.publish(actual)
        if learned.markers:
            learned_marker_pub.publish(learned)
        rate.sleep()
