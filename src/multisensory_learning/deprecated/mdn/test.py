
import numpy as np

import torch
import torch.nn as nn
import torch.distributions as D
from torch.distributions.multivariate_normal import MultivariateNormal
from torch.distributions.mixture_same_family import MixtureSameFamily


# mus = torch.FloatTensor([2, 3, -2, 0])
# sig_diag = torch.FloatTensor([1, 0.5, 1.2, 0.75])
# sig = torch.FloatTensor([
#   [1, 0.1, 0.1, 0.1],
#   [0.1, 0.5, 0.1, 0.1],
#   [0.1, 0.1, 1.2, 0.1],
#   [0.1, 0.1, 0.1, 0.75]
# ])
# alphas = torch.FloatTensor([0.25, 0.15, 0.25, 0.35])
#
# mix = D.Categorical( alphas )
# comp = D.Independent( D.Normal( mus, sig ), 1 )
# gmm = MixtureSameFamily( mix, comp )
# samp = gmm.sample()
# print(samp)

bsize = 2
mixnum = 3 # the number of gaussians used to properly describe the distribution
dimsize = 4 # the dimension of the distribution

# alphas = torch.ones(mixnum,)
# mus = torch.randn(mixnum, dimsize)
# sig_diag = torch.randn(mixnum, dimsize)
# sig = torch.randn(mixnum, dimsize, dimsize)
#
# mix = D.Categorical( alphas )
# comp = D.Independent( D.Normal( mus, sig_diag ), 1 )
# gmm = MixtureSameFamily( mix, comp )
# samp = gmm.sample()
# print(samp)
# tprob = gmm.log_prob(samp)
# print('prob', tprob)
#
# test = torch.FloatTensor([0.5, 0.5, 0.5, 0.5])
# tprob = gmm.log_prob(test)
# print('prob', tprob)

# mix = D.Categorical( alphas )
# comp = D.Independent( D.Normal( mus, sig ), 1 )
# gmm = MixtureSameFamily( mix, comp )
# samp = gmm.sample()
# print(samp)

# alphas = torch.ones(bsize, mixnum,)
# mus = torch.randn(bsize, mixnum, dimsize)
# sig = torch.rand(bsize, mixnum, dimsize)
#
# mix = D.Categorical( alphas )
# comp = D.Independent( D.Normal( mus, sig ), 1 )
# gmm = MixtureSameFamily( mix, comp )
# samp = gmm.sample()
# print('sample ({}, {}): {}'.format(bsize, dimsize, samp))
#
# test = torch.randn(bsize, dimsize)
# tprob = gmm.log_prob(test)
# print('prob ({})'.format(bsize, tprob))

alphas = torch.ones(bsize, mixnum,)
mus = torch.randn(bsize, mixnum, dimsize)
sig = torch.rand(bsize, mixnum, 6)
sig_diag = torch.rand(bsize, mixnum, dimsize)

def get_distribution(alphas, mus, sig, sig_diag):
    bsize = mus.shape[0]
    mixnum = mus.shape[1]
    dimsize = mus.shape[2]
    diag_idx = np.arange(dimsize)
    tril_idx = np.tril_indices(dimsize, -1)
    sigma = torch.zeros(bsize, mixnum, dimsize, dimsize)
    sigma[:, :, tril_idx[0], tril_idx[1]] = sig
    # sigma += torch.transpose(sigma, -2, -1)
    sigma[:, :, diag_idx, diag_idx] = sig_diag

    mix = D.Categorical( alphas )
    # comp = D.Independent( D.MultivariateNormal( mus, sigma ), 1 )
    comp = D.Independent( D.MultivariateNormal( mus, scale_tril=sigma ), 0 )
    gmm = MixtureSameFamily( mix, comp )
    return gmm

gmm = get_distribution(alphas, mus, sig, sig_diag)
samp = gmm.sample()
print('sample ({}, {}): {}'.format(bsize, dimsize, samp))

targets = torch.randn(bsize, dimsize)
results = gmm.log_prob(targets)
loss = -torch.mean(results)
print('prob ({}): {}'.format(bsize, results))
print('loss: {}'.format(loss))

# alphas = torch.ones(bsize, mixnum,)
# mus = torch.randn(bsize, dimsize, mixnum)
# diag_idx = np.arange(dimsize)
# tril_idx = np.tril_indices(dimsize, -1)
# sig = torch.rand(bsize, len(tril_idx[0]), mixnum)
# sig_diag = torch.rand(bsize, dimsize, mixnum)
# sigma = torch.zeros(bsize, dimsize, dimsize, mixnum)
# sigma[:, tril_idx[0], tril_idx[1], :] = sig
# # sigma += torch.transpose(sigma, -2, -1)
# sigma[:, diag_idx, diag_idx, :] = sig_diag
#
# mix = D.Categorical( alphas )
# # comp = D.Independent( D.MultivariateNormal( mus, sigma ), 1 )
# comp = D.Independent( D.MultivariateNormal( mus, scale_tril=sigma ) )
# import pdb; pdb.set_trace()
# gmm = MixtureSameFamily( mix, comp )
# samp = gmm.sample()
# print('sample ({}, {}): {}'.format(bsize, dimsize, samp))
#
# test = torch.randn(bsize, dimsize)
# tprob = gmm.log_prob(test)
# print('prob ({}): {}'.format(bsize, tprob))
