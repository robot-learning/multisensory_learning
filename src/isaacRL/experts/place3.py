
# from isaacRL.experts.trajopt import TrajectoryOptimizer
from ll4ma_isaacgym.behaviors.planners.trajopt import TrajectoryOptimizer
from isaacRL.experts.jacobian_resolver import JacobianResolver
from ll4ma_isaacgym.behaviors.planners import jacobian_controller

from isaacgym import torch_utils
from ll4ma_util import math_util, torch_util
import moveit_interface.util as moveit_util

import torch

class Place3(TrajoptArmExpert):
    """docstring for Place3."""

    def __init__(
        self,
        action_size,
        target_object,
        same_starts=True,
        grasp_offset=None,
        target_prior=None,
        planner='trajopt',
        action_repeats=2,
        max_iterations=150,
        num_joints=7,
        joint_names=[],
        ee_link='reflex_palm_link',
        grasp_value=1,
        ungrasp_value=-1,
        grasp_neutral=0,
        cartesian_path=False
    ):
        super().__init__(
            action_size,
            grasp_offset,
            planner,
            action_repeats,
            max_iterations,
            num_joints,
            joint_names,
            ee_link,
            grasp_value,
            ungrasp_value,
            grasp_neutral,
            cartesian_path
        )
        if target_prior is None:
            target_prior = torch.cat(
                [torch.tensor([0., 0., 0.15]), self._downward_ee]
            )
        else:
            raise('Blah')
        self.target_prior = target_prior.unsqueeze(0)
        # if self.target_prior[0,3] != -0.7071067811865475:
        #     import pdb; pdb.set_trace()
        print('target_prior:', self.target_prior)

        self._step_i = 0

    def reset_setup(self):
        self.move2grasp = None
        self.lift = None

    def get_setup_actions(self, state, envis=None):
        state = state['full_state']
        num_envs = state.shape[0]
        if envis is None:
            envis = torch.arange(num_envs)
        start_joints = state[self._joint_key][:,:self.num_joints]

        actions = torch.zeros(
            (1, num_envs, self.action_size),
            device=state.device, dtype=start_joints.dtype
        )
        actions[:,:,:self.num_joints] = start_joints[-1:,:,:self.num_joints].clone()

        return actions[:,envis]

    def get_actions(self, state, target_pose, envis=None, interp=None):

        if self._step_i % 2 == 0:
            if target_pose is None:
                target_pose = state[target_object].clone()
                target_pose[:,0:3] += self.target_prior[:,0:3].to(target_pose.device)
                target_pose[:,3:7] = self.target_prior[:,3:7].to(target_pose.device)

            movetopose = self._get_moveto_actions(state, target_pose, envis, interp)

            grasp_actions = self._get_grasp_actions(movetopose[-1:], envis)

            raise_actions = self._get_raise_actions(state, target_pose, envis, interp, movetopose[-1:])

            actions = torch.cat((movetopose, grasp_actions, raise_actions), dim=0)[:,envis]

        if self._step_i % 2 == 1:
            if target_pose is None:
                target_pose = state[self._goal_key].clone()
                # target_pose[:,0:3] += self.target_prior[:,0:3].to(target_pose.device)
                target_pose[:,3:7] = self.target_prior[:,3:7].to(target_pose.device)

            movetopose = self._get_moveabove_actions(state, target_pose, envis, interp)

            lower_actions = self._get_lower_actions(state, target_pose, envis, interp, movetopose[-1:])

            ungrasp_actions = self._get_ungrasp_actions(lower_actions[-1:], envis)

            raise_actions = self._get_raise_actions(state, target_pose, envis, interp, lower_actions[-1:])

            actions = torch.cat((movetopose, lower_actions, ungrasp_actions, raise_actions), dim=0)[:,envis]

        self._step_i += 1

        return actions

    def get_setdown_actions(self, state, envis=None):
        state = state['full_state']
        num_envs = state.shape[0]
        if envis is None:
            envis = torch.arange(num_envs)
        start_joints = state[self._joint_key][:,:self.num_joints]

        actions = torch.zeros(
            (1, num_envs, self.action_size),
            device=state.device, dtype=start_joints.dtype
        )
        actions[:,:,:self.num_joints] = start_joints[-1:,:,:self.num_joints].clone()

        return actions[:,envis]

    def reset_idx(self, envis):
        self._step_i *= 0

def l2_dist(vec1, vec2):
    return ((vec1 - vec2)**2).sum(dim=1)
