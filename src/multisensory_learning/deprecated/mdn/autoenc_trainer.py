
import os, sys, argparse, json
import numpy as np
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader

from mdn.trainer import Trainer
from mdn.model import MDNModel, BinaryModel, ClassifierModel
from mdn.eval import evaluate
from mdn.encoded_datasets import EncodedDataset
from mdn.utils.loss import logprob_mix_loss
from mdn.utils.latent_models import AutencModel

from ll4ma_util import torch_util

class AutencTrainer(Trainer):
    """docstring for AutencTrainer."""

    def __init__(self, config, device, training=True):
        super(AutencTrainer, self).__init__()

        self.config = config
        self.device = device

        insize = config['latent_size']
        if not config['sample_first']:
            insize *= 2
        if config['classify']:
            outd = config['classes'] if config['criterion'] == 'focal' else 1
            self.model = ClassifierModel(
              hlayer_sizes=config['hlayers'],
              ind=insize, outd=outd,
              contextlayer=config['contextlayer'],
              batchnorm=True
            ).to(device)
        else:
            self.model = MDNModel(
              # hlayer_sizes=[insize*2, int(insize*1.5), insize*2],
                # hlayer_sizes=[int(insize*1.5), int(insize*1.0),
                # int(insize*1.5), int(insize*1.0), int(insize*1.5)],
              hlayer_sizes=config['hlayers'],
              nmodes=config['nmodes'],
              ind=insize, outd=config['latent_size'],
              batchnorm=True
            ).to(device)

        self.optim = None
        if training:
            self.optim = torch.optim.Adam(self.model.parameters())

        if config['classify']:
            if config['criterion'] == 'focal':
                from kornia.losses import focal_loss
                self.criterion = lambda x, y : focal_loss(
                  x, y.squeeze(1).type(torch.int64),
                  alpha=config['alpha'], gamma=config['gamma'],
                  reduction='mean' )
                # from kornia.losses import binary_focal_loss_with_logits
                # self.criterion = lambda x, y : binary_focal_loss_with_logits(
                #   x, y )
            else:
                self.criterion = nn.BCEWithLogitsLoss()
        else:
            self.criterion = logprob_mix_loss
        # if config['criterion'] == 'logprob':
        #     self.criterion = logprob_loss

        self.start_epoch = 0
        self.end_epoch = config['epochs']
        self.tloss = []
        self.vloss = []

        self.valloader = None
        os.makedirs(self.config['checkpoint_path'], exist_ok=True)

    def load_model(self, checkpt):
        checkpoint = torch.load(checkpt)
        self.model.load_state_dict(checkpoint['model'])
        if self.optim is not None:
            self.optim.load_state_dict(checkpoint['optimizer'])
        self.tloss = checkpoint['tloss']
        self.vloss = checkpoint['vloss']
        self.start_epoch = checkpoint['epoch']
        self.end_epoch += self.start_epoch

    def train(self):

        trainset = EncodedDataset(
          self.config['train_files'], device=self.device,
          classify=self.config['classify']
        )
        trainloader = DataLoader(
            trainset,
            batch_size=self.config['batch_size'],
            shuffle=True,
            num_workers=self.config['n_cpu'],
            pin_memory=True,
        )

        bestloss = float("inf")
        best_epoch = 0
        for epoch in range(self.start_epoch, self.end_epoch):
            print('epoch:', epoch+1)

            lossHistory = []
            self.model.train()
            pbar = tqdm(total=len(trainloader), file=sys.stdout)
            for di, xdata in enumerate(trainloader):
                context = xdata['init'].to(self.device)
                state = xdata['state'].to(self.device)
                target = xdata['target'].to(self.device)

                self.optim.zero_grad()
                distr = self.model(state, context)
                loss = self.criterion(distr, target)
                loss.backward()
                self.optim.step()

                lossHistory.append(loss.item())
                desc = f'  Iter {di}: loss={np.mean(lossHistory):.4f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            self.tloss.append( np.mean(lossHistory) )

            if epoch % self.config['evaluation_interval'] == 0:
                print('eval:')
                loss = self.evaluate()
                if self.config['classify']:
                    loss = loss[0]
                print('train loss:', self.tloss[-1])
                print('eval loss:', loss)
                self.vloss.append( loss )
                if loss < bestloss:
                    bestloss = loss
                    best_epoch = epoch
                    filen = os.path.join( self.config['checkpoint_path'],
                      'best_epoch.txt' )
                    with open(filen, 'w') as f:
                        f.write(str(epoch))

            if epoch % self.config['checkpoint_interval'] == 0:
                checkpoint = {}
                checkpoint['optimizer'] = self.optim.state_dict()
                checkpoint['model'] = self.model.state_dict()
                checkpoint['tloss'] = self.tloss
                checkpoint['vloss'] = self.vloss
                checkpoint['epoch'] = epoch
                filename = os.path.join( self.config['checkpoint_path'],
                  self.config['checkpoint_name'].format(epoch) )
                torch.save(checkpoint, filename)
                checkpoint = None

    def evaluate(self, threshold=0.5):
        self.model.eval()
        if self.valloader is None:
            valset = EncodedDataset(
              self.config['valid_files'], device=self.device,
              classify=self.config['classify']
            )
            valloader = DataLoader(
                valset,
                batch_size=self.config['eval_batch'],
                shuffle=True,
                num_workers=self.config['n_cpu'],
                pin_memory=True,
            )
            self.valloader = valloader
        else:
            valloader = self.valloader

        if self.config['classify']:
            cnt = 0
            tot = 0
        totLoss = 0.0
        with torch.no_grad():
            pbar = tqdm(total=len(valloader))
            for di, xdata in enumerate(valloader):
                context = xdata['init'].to(self.device)
                state = xdata['state'].to(self.device)
                target = xdata['target'].to(self.device)

                distr = self.model(state, context)
                if self.config['classify']:
                    if self.config['criterion'] == 'focal':
                        conf, clas = torch.softmax(distr, dim=1).max(dim=1)
                        clas[conf < threshold] = 0
                        cnt += (target.squeeze(1) == clas).sum().item()
                        tot += len(target)
                    else:
                        clas = torch.sigmoid(distr)
                        clas[clas<threshold] = 0.0
                        clas = clas.round()
                        cnt += (target == clas).sum().item()
                        tot += len(target)
                loss = self.criterion(distr, target)
                totLoss += loss.item()

                desc = f'  Iter {di}: loss={(totLoss/(di+1)):.4f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
        avgloss = totLoss / len(valloader)
        if self.config['classify']:
            acc = float(cnt) / tot
            print('cnt/tot=accuracy | {}/{}={}'.format(cnt, tot, acc))
            return avgloss, acc
        return avgloss


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str,
        default="configs/autenc.json", help="path to config file")
    parser.add_argument("-v", "--verbose", default=False,
        help="if print all info")
    parser.add_argument("--continu", type=str, default=None,
        help="if continuing training from checkpoint model")
    opt = parser.parse_args()
    # print(opt)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    config = json.load(open(opt.config))

    trainer = AutencTrainer(config, device)

    if opt.continu is not None:
        # checkpt = os.path.join( opt.continu )
        checkpt = opt.continu
        trainer.load_model(checkpt)

    trainer.train()


if __name__ == '__main__':
    main()
