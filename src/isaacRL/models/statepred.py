
import torch
import torch.nn as nn

from isaacRL.models.models import FCNet

from isaacRL.utils.pytorch3d.rotation_conversions import axis_angle_to_quaternion

class StatePredictor(nn.Module):
    """
    docstring for StatePredictor.
    Adds a normalization for quaternion pose predictions
    """

    def __init__(self, insize, cfg={}):
        super(StatePredictor, self).__init__()
        self.insize = insize
        self.cfg = cfg

        self.model = FCNet(
            infeats=insize,
            outfeats=7,  # for the pose of the predictor
            layers=cfg.get('layers', [100]*3),
            activation=cfg.get('activation', 'relu'),
            batchnorm=cfg.get('batchnorm', False),
        )
        # self.bias = torch.nn.Parameter(torch.tensor([[1., 1., 1., 1., 1., 1., 3.14]]))
        # self.bias = torch.nn.Parameter()
        # self.bias = torch.nn.Parameter(torch.tensor([[1., 1., 1., 3.14, 3.14, 3.14]]))
        # self.bias.requires_grad = False
        #self.bias = torch.tensor([[1., 1., 1., 1., 1., 1., 1.]])

    def forward(self, xdata, to_quat=False):
        pred = self.model(xdata)
        # pred = torch.cat((torch.tanh(pred[:,:6]), pred[:,6:]), dim=1)
        pred = torch.tanh(pred) #* self.bias
        if not to_quat:
            return pred
        # pos, quat = pred[:,:3], pred[:,3:7]
        # pred[:,3:7] = pred[:,3:7] / torch.norm(pred[:,3:7], p=2)
        # pred[:,3:7] = pred[:,3:7] / (pred[:,3:7]**2).sum(1).sqrt().unsqueeze(-1)
        # quat = torch.nn.functional.normalize(pred[:,3:7], p=2, dim=1)
        quat = axis_angle_to_quaternion(pred[:,3:6])
        return torch.cat((pred[:,:3], quat), dim=1)
