#!/usr/bin/env python
import os
import sys
import math
import argparse
import numpy as np
from tqdm import tqdm
from typing import List
from dataclasses import dataclass

from multisensory_learning.learning import Learner, LearnerConfig
from multisensory_learning.models import Ortho6dModel
from multisensory_learning.models.loss import matrix_log_loss

from ll4ma_util import file_util, torch_util, ui_util, math_util, func_util, data_util

import torch
from torch.utils.data import Dataset, DataLoader

"""
This implements the 6D Gram-Schmidt orthographic projection from [1]. It's
a good way of representing a full pose in a neural network that doesn't 
suffer from discontinuities in the latent space.

Note because this is just learning how to represent the rotation, you can
train on as much data as you want by just randomly generating rotations
to autoencode, and it won't overfit because you've sampled the full space.
Can train on 1 million points in ~10 minutes.

[1] "On the Continuity of Rotation Representations in Neural Networks", 
    Zhou et al., CVPR 2019, https://arxiv.org/pdf/1812.07035.pdf
"""


class Ortho6dDataset(Dataset):

    def __init__(self, n_samples=1e5):
        super().__init__()
        self.n_samples = int(n_samples)

    def __len__(self):
        return self.n_samples

    def __getitem__(self, idx):
        return {'rotation': torch_util.random_rotation().squeeze()}


@dataclass
class Ortho6dConfig(LearnerConfig):
    hidden_sizes: List[int] = func_util.lambda_field([128, 128])
    activation: str = 'relu'
    norm: str = 'layer'
    n_epochs: int = 1  # Since data is random, just set as many samples as you want
    device: str = 'cpu'  # No images, seems a little faster on CPU

    
class Ortho6dLearner(Learner):

    def __init__(self, config, n_samples=1e4, set_train=False, set_eval=False, device=None):
        self.n_samples = n_samples
        self.min_output_value = None
        self.max_output_value = None        
        super().__init__(config, set_train=set_train, set_eval=set_eval, device=device)
        
    def set_models(self):
        self.models = {
            'ortho6d': Ortho6dModel(self.config.hidden_sizes, self.config.activation,
                                    self.config.norm)
        }
        super().set_models()
        
    def get_checkpoint_filename(self, epoch, *args):
        return os.path.join(self.config.checkpoint_dir, f"ortho6d_{epoch:03d}.pt")
        
    def apply_models(self, batch, min_max_scale=False):
        R_in = batch['rotation'].view(-1, 9).float()
        ortho6d, R_out = self.models['ortho6d'](R_in)
        if min_max_scale:
            if self.min_output_value is None or self.max_output_value is None:
                raise ValueError("Missing min/max values to do min-max scaling")
            ortho6d = data_util.scale_min_max(ortho6d, self.min_output_value,
                                              self.max_output_value, -1, 1)
        outputs = {'ortho6d': ortho6d, 'rotation': R_out}
        return outputs

    def compute_loss(self, batch, outputs):
        """
        Computes matrix log between rotation matrices.
        """
        loss = matrix_log_loss(batch['rotation'], outputs['rotation'])
        loss = loss.mean()
        return loss, {'matrix_log': loss.item()}

    def get_data_loaders(self, val_size=0.1):
        train_dataset = Ortho6dDataset(self.n_samples)
        val_dataset = Ortho6dDataset(int(val_size * self.n_samples))
        train_loader = DataLoader(train_dataset, self.config.batch_size)
        val_loader = DataLoader(val_dataset, self.config.batch_size)
        return train_loader, val_loader, len(train_dataset), len(val_dataset)

    def set_checkpoint(self, filename):
        super().set_checkpoint(filename)
        if 'min_output_value' in self.checkpoint:
            self.min_output_value = self.checkpoint['min_output_value']
        if 'max_output_value' in self.checkpoint:
            self.max_output_value = self.checkpoint['max_output_value']
            
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--checkpoint_dir', type=str, required=True)
    parser.add_argument('--batch_size', type=int)
    parser.add_argument('--n_samples', type=int, default=1e7)
    parser.add_argument('--checkpoint_interval', type=int, default=1)
    args = parser.parse_args()

    config = Ortho6dConfig()
    func_util.override_dataclass_with_args(config, args)
    
    learner = Ortho6dLearner(config, args.n_samples, set_train=True)
    learner.train()
    
    # Figure out min/max values coming out of the network, this will be useful when
    # using as input to other models. Being a bit conservative about bounds by taking
    # max over any dimension and adding a buffer
    print("\nFiguring out min/max values for network outputs...")
    max_value = -sys.maxsize
    loader, _, _, _ = learner.get_data_loaders()
    for batch in tqdm(loader):
        torch_util.move_batch_to_device(batch, learner.device)
        ortho6d = learner.apply_models(batch)['ortho6d']
        max_value = max(max_value, torch.max(torch.abs(ortho6d)).item())
    cp = torch.load(learner.last_saved_checkpoint)
    cp['max_output_value'] = math.ceil(max_value) + 1 # Add buffer
    cp['min_output_value'] = -cp['max_output_value']
    torch.save(cp, learner.last_saved_checkpoint)

    ui_util.print_happy("\n  Ortho6d learning complete.\n")
    
