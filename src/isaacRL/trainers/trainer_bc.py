
from isaacRL.trainers.trainer import Trainer
from isaacRL.models.imitation_learning import DeterministicActor
from isaacRL.models.actorcritics import ActorBasic
from isaacRL.tasks import get_task
from isaacRL.utils.buffer import BufferFullstate
from isaacRL.experts.picknplace import PicknplaceStepExpert
from isaacRL.experts.dagger_expert import DaggerPicknplaceStepExpert

from isaacgymenvs.utils.utils import set_seed

import torch
import torch.nn as nn
import torch.nn.functional as F

import time
from tqdm import tqdm
from random import randint
import os, sys, argparse, json
import os.path as osp
import rospy

class TrainerBC(Trainer):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        device = "cuda" if torch.cuda.is_available() else "cpu"
        super(TrainerBC, self).__init__(config, device)

        self.max_steps = self.config['environ']['max_env_steps']
        self.randact_prob = self.config['training'].get('randact_prob', 0.)

        self.ckpt_interval = self.config['training']['checkpoint_interval']
        self.save_best_after = self.config['training'].get('save_best_after', self.ckpt_interval)
        self.actions_per_interval = self.config['training']['acts_per_interval']
        self.updates_per_interval = self.config['training']['updates_per_interval']

        self.pretrain_epochs = self.config['training'].get('pretrain_epochs', 0)
        self.pretrained_model = self.config['training'].get('pretrained_model', None)

        self.pretrain_datafile = self.config['training']['checkpoint_path'].replace('checkpoints', 'data')
        self.pretrain_datafile = self.pretrain_datafile.strip('/')
        print('pretrain data folder:', self.pretrain_datafile.rsplit('/', 1)[0])
        os.makedirs(self.pretrain_datafile.rsplit('/', 1)[0], exist_ok=True)
        self.pretrain_datafile = self.pretrain_datafile + '.torch'
        self.pretrain_datafile = self.config['training'].get(
            'pretrain_datafile', self.pretrain_datafile
        )
        print('pretrain data file:', self.pretrain_datafile)

        os.makedirs(self.config['training']['checkpoint_path'], exist_ok=True)

        self.config['environ']['seed'] = 42 # -1 if random
        self.config['environ']['seed'] = set_seed(
            self.config['environ']['seed'], torch_deterministic=False
        )
        self.config['environ']['act_range'] = self.config['model']['actor']['act_range']

        self.envir = get_task(self.config['environ'], self.device)

        self.observation_size = config['model']['observations']
        print('observ size:', self.observation_size)
        self.action_size = config['model']['actor']['actions']

        if config['model']['actor'].get('stochastic', False):
            self.actor = ActorBasic(
                self.observation_size,
                self.action_size,
                config['model']['actor'],
            )
            self.actor.forward = lambda x : self.actor.stochastic(x)[0]
        else:
            self.actor = DeterministicActor(
                self.observation_size,
                self.action_size,
                config['model']['actor']
            )

        self.actor.to(self.device)

        self.pretrained_model = self.config['model']['actor'].get('pretrained', None)
        if self.pretrained_model is not None and osp.exists(self.pretrained_model):
            self.load_pretrained(self.pretrained_model)

        self.optim = torch.optim.Adam(
            self.actor.parameters(),  lr=self.lr, weight_decay=self.weight_decay
        )

        self.losses = []

        self._use_eval_data = True

        buf_size = self.config['training'].get('max_buff_size', self.max_steps*self.num_envs)
        self.buffer = BufferFullstate(
            state_size=self.envir.fullstate.shape[1],
            action_size=self.action_size,
            full2obs=self.envir.full2obs,
            device=self.device,
            storage_device='cpu',
            max_size=buf_size,
            ee_actions=self.envir.action_type==self.envir.ActionTypes.EE_POSE
        )
        self.buffer._full_idx = self.envir.fullstate.indices
        self.state = None

        # if hasattr(self.envir, 'expert'):
        #     self.expert = self.envir.expert
        # else:
        #     self.expert = PicknplaceExpert(
        #         self.config['environ']['num_acts'],
        #         self.config['environ']['goal']['object'],
        #         same_starts=self.config['environ']['task'].get('same_starts', True),
        #         planner=self.config['environ']['task'].get('planner', 'trajopt'),
        #     )
        self.expert = DaggerPicknplaceStepExpert(
            self.config['environ']['num_acts'],
            self.config['environ']['goal']['object'],
            same_starts=self.config['environ']['task'].get('same_starts', True),
            planner=self.config['environ']['task'].get('planner', 'trajopt'),
            num_envs=self.num_envs,
        )
        # self.expert = self.envir.expert

    def generate_data(self):
        self._gather_init_data()

    def _gather_init_data(self):
        act_num = (self.buffer.max_size // self.num_envs)+1
        state = self.envir.reset()
        self.expert.reset_idx(torch.arange(self.num_envs), state)
        pterms = None
        pbar = tqdm(total=act_num, file=sys.stdout)
        desc = f'Filling Buffer'
        pbar.set_description(desc)
        for ai in range(act_num):
            if rospy.is_shutdown():
                return
            actions = self.expert.get_actions(state)  #[:, :self.expert.num_joints]
            actions = actions.detach()
            # print(actions[0])
            nstate, rewards, terminals, _ = self.envir.step(actions)
            self.buffer.add(
                state, actions, nstate, rewards, terminals
            )
            # if pterms is not None and (pterms>0).any():
            #     self.expert.reset_idx(pterms.nonzero(as_tuple=False).squeeze(-1), state)
            pterms = terminals
            state = nstate
            pbar.update(1)
            if len(self.buffer) >= self.buffer.max_size:
                break
        pbar.close()

        self.buffer.save(self.pretrain_datafile)
        print('mean rewards:', self.buffer.mean_rewards())
        success = self.envir.successes()
        print('successes:', success)
        self.success = success

    def train(self):

        if osp.exists(self.pretrain_datafile):
            self.buffer.load(self.pretrain_datafile)
        else:
            self._gather_init_data()

        self.success = 0
        tot_rewards = 0.
        print('resetting')
        self.actor.train()

        iterations = self.ckpt_interval*len(self.buffer)//self.batch_size
        num_epochs = self.end_epoch-self.start_epoch
        for epoch in range(self.start_epoch, self.end_epoch):
            mean_loss = 0.
            pbar = tqdm(total=iterations, file=sys.stdout)
            desc = f'Train Epoch {epoch}'
            pbar.set_description(desc)
            for iter in range(iterations):
                loss = self.update_weights(epoch*iterations+iter)
                mean_loss += loss
                desc = f'Train Epoch {epoch}: loss={(mean_loss/(iter+1)):.3f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            self.losses.append(mean_loss/len(self.buffer))

            if epoch >= self.save_best_after:
                self.evaluate(self.max_steps+1)
                self.actor.train()
                self.save_model(epoch)

    def evaluate(self, num_steps, store_images=False):
        if self.envir.randomize == 'preset':
            filn = self.config['environ']['randomize'].replace('train', 'eval')
            if os.path.exists(filn) and self._use_eval_data:
                self.envir.add_preset(filn)
        self.state = self.envir.reset()
        self.actor.eval()
        tot_rew = self.play(num_steps, evaluate=True, store_images=store_images)
        mean_rew = tot_rew / (num_steps*self.num_envs)
        return tot_rew, mean_rew, self.envir.successes(), self.envir.runs()

    def play(self, num_actions=1, evaluate=False, store_images=False):
        if self.state is None:
            self.state = self.envir.reset()
        if store_images:
            self.rgb_images = []
        with torch.no_grad():
            tot_rew = 0.
            if evaluate:
                pbar = tqdm(total=num_actions, file=sys.stdout)
                desc = f'Eval Iter {0}'
                pbar.set_description(desc)
            for ai in range(num_actions):
                if rospy.is_shutdown():
                    return
                actions = self.actor(self.state)
                actions = actions.detach()
                # eactions = self.expert.get_actions(self.state)
                # actions[0] = eactions[0]
                if not evaluate:
                    actions[0] = self.buffer._actions[ai*self.buffer._bsize + randint(0, 20)]
                nstate, rewards, terminals, _ = self.envir.step(actions)
                if not evaluate:
                    self.buffer.add(
                        self.state, actions, nstate, rewards, terminals
                    )
                if store_images:
                    rgb_key = [key for key in nstate['full_state'].keys() if 'rgb' in key and 'emb' not in key]
                    if len(rgb_key) == 0:
                        rgb = self.envir.get_rgb_img()
                        rgb = [img for img in rgb.values()][0][0]
                        # self.envir.get_rgb_img()['test_camera_rgb'][0]
                        self.rgb_images.append(rgb)
                    else:
                        rgb = nstate['full_state'][rgb_key]
                        self.rgb_images.append(rgb[0].reshape(224, 224, 3)*255)
                self.state = nstate
                tot_rew += rewards.sum().item()
                if evaluate:
                    success = self.envir.successes()
                    desc = f'Eval Iter {ai}: rew={(tot_rew/((ai+1)*self.num_envs)):.3f},suc={success:d}'
                    pbar.set_description(desc)
                    pbar.update(1)
            if evaluate:
                pbar.close()
            return tot_rew

    def update_weights(self, update_i):
        state, action, nstate, rewards, terminals = self.buffer.sample(self.batch_size)
        pred_action = self.actor(state)
        loss = F.mse_loss(action, pred_action)
        if torch.isnan(loss):
            import pdb; pdb.set_trace()
        self.optim.zero_grad()
        loss.backward()
        if self.clip_grad is not None:
            self.clip_grad(self.actor.parameters())
        self.optim.step()
        return loss.item()

    @property
    def num_envs(self):
        return self.envir.num_envs

    def load_pretrained(self, checkpt):
        checkpoint = torch.load(checkpt)
        self.actor.load_state_dict(checkpoint['actor'])

    def load_model(self, checkpt):
        checkpoint = torch.load(checkpt)
        self.actor.load_state_dict(checkpoint['actor'])
        if self.optim is not None:
            self.optim.load_state_dict(checkpoint['actor_optim'])
        self.losses = checkpoint['loss']
        self.start_epoch = checkpoint['epochs']+1
        self.end_epoch += self.start_epoch

    def save_model(self, epoch, name_mod=None):
        checkpoint = {}
        checkpoint['actor_optim'] = self.optim.state_dict()
        checkpoint['actor'] = self.actor.state_dict()
        checkpoint['loss'] = self.losses
        checkpoint['epochs'] = epoch
        if name_mod is None:
            name_mod = epoch
        filename = os.path.join(
            self.config['training']['checkpoint_path'],
            self.config['training']['checkpoint_name'].format(name_mod)
        )
        torch.save(checkpoint, filename)
        print('saved to', filename)
