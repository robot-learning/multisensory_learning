import sys
import torch
import torch.distributions as D

from ll4ma_util import ui_util


def mdn_loss(targets, alpha, mu, sigma_tril, is_goal=None):
    """
    Loss function for Mixture Density Network (MDN)

    Since I'm trying to train this concurrently with the other models, not every sample in a
    batch will be a positive (some will be non-goals). This allows passing in a mask to say
    which are goals and which are not, and gates the loss by zeroing out loss for any 
    negative samples.

    Args:
        targets (Tensor): Points to evaluate w.r.t. distribution (B, in_size)
        alpha (Tensor): Mixture weights (B, C)
        mu (Tensor): Component means (B, out_size, C)
        L_diag (Tensor): Component cov diagonal elements, (B, out_size, C)
        L (Tensor) : Component lower triangle values for cov (B, 0.5*out_size*(out_size-1), C)
    """
    device = alpha.device
    dtype = alpha.dtype
    batch_size, n_components = alpha.size()
    
    result = torch.zeros(batch_size, n_components, dtype=dtype).to(device)
        
    for idx in range(n_components):
        mvn = D.MultivariateNormal(loc=mu[:,:,idx], scale_tril=sigma_tril[:,:,:,idx])
        result[:,idx] = mvn.log_prob(targets) + alpha[:,idx].log()
        if is_goal is not None:
            result[:,idx] = is_goal * result[:,idx]
        
    return -torch.mean(torch.logsumexp(result, dim=1))
        
        
def matrix_log_loss(R1, R2, clip_to=0.999):
    """
    Computes the matrix log for rotation matrices. See Sec.3.2.3.3 of [1].
    Implementation similar to [2].

    Note that this only computes the angle theta since from a loss perspective
    we only need to know the scalar discrepancy and not the vector to get the
    rotations to align (e.g. as you would in control).

    We need to clip cos_theta values to be in range [-1, 1] for it to even make
    sense as a cosine value. However, using that full range produces nan values.
    So, the value clip_to keeps the values in a more conservative range to
    avoid nans.

    [1] Lynch, Kevin M., and Frank C. Park. Modern robotics, 2017
    [2] https://github.com/papagina/RotationContinuity/blob/758b0ce551c06372cab7022d4c0bdf331c89c696/sanity_test/code/tools.py#L279:L296
    """
    batch_size = R1.shape[0]
    m = torch.bmm(R1, R2.transpose(1,2))
    # See equation 3.54 in Lynch & Park
    cos_theta = 0.5 * (m[:,0,0] + m[:,1,1] + m[:,2,2] - 1.0)
    cos_theta = torch.min(cos_theta, torch.full_like(cos_theta,  clip_to))
    cos_theta = torch.max(cos_theta, torch.full_like(cos_theta, -clip_to))
    theta = torch.acos(cos_theta)
    return theta

