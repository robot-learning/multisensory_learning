
import os, sys, argparse, json
import os.path as osp

import torch
import torch.nn as nn

class Trainer:
    """docstring for Trainer."""

    def __init__(self, config, device):
        self.config = config
        self.device = device

        self.start_epoch = 1
        self.end_epoch = self.config['training']['epochs']+1
        self.batch_size = self.config['training']['batch_size']
        self.eval_batch = self.config['training'].get('eval_batch', self.batch_size)

        self.lr = self.config['training'].get('learning_rate', 0.0001)
        self.lr_scheduler = None
        self.weight_decay = self.config['training'].get('weight_decay', 0)

        self.clip_grad = None
        if 'grad_norm' in self.config['training']:
            self.clip_grad = lambda x : nn.utils.clip_grad_norm_(
                x, self.config['training']['grad_norm']
            )

    def load_model(self):
        raise NotImplementedError

    def save_model(self):
        raise NotImplementedError

    def train(self):
        raise NotImplementedError

    def evaluate(self):
        raise NotImplementedError
