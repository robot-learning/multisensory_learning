
from isaacRL.models.models import *

def get_modalnet(config, mod, input_states=1):
    lastivation = None
    if 'lastivation' in config:
        if config['lastivation'] == 'tanh':
            lastivation = torch.tanh
        elif config['lastivation'] == 'sigma':
            lastivation = torch.sigma
        else:
            print('unkown lastivation function:', config['lastivation'])
    if mod == 'rgb':
        return ConvNet( inchans=3, lastivation=lastivation )
    elif mod == 'depth':
        return ConvNet( inchans=1, lastivation=lastivation )
    else:
        # print('Unknown modality:', mod)
        if (config['infeats'] == config['outfeats']) and (len(config['layers']) == 0):
            return IdentityNet()
        return FCNet( config['infeats'], config['outfeats'],
          config['layers'], activation=config['activation'], lastivation=lastivation )

class ModalNet(nn.Module):
    """docstring for ModalNet."""

    def __init__(self, config):
        super(ModalNet, self).__init__()
        self.config = config

        self.modals = {}
        for mod in config['modalities']:
            self.modals[mod] = get_modalnet( config['modalities'][mod], mod )

    def copy(self, newnet=None):
        if newnet is None:
            newnet = ModalNet(self.config)
        for mod in self.modals:
            for new_param, param in zip(
              newnet.modals[mod].parameters(), self.modals[mod].parameters() ):
                new_param.data.copy_(param.data)
        return newnet

    def forward(self, xdata):
        ydata = {}
        for mod in xdata:
            if mod in self.modals:
                ydata[mod] = self.modals[mod](xdata[mod]).flatten(start_dim=1)
        return ydata

    def load_params(self, targ_params):
        for mod in self.modals:
            if mod in targ_params and mod == 'rgb':
                self.modals[mod].load_state_dict(targ_params[mod])

    def save_params(self):
        params = {}
        for mod in self.modals:
            params[mod] = self.modals[mod].state_dict()
        return params

    def to(self, device):
        super().to(device)
        for mod in self.modals:
            self.modals[mod].to(device)
        return self

    def train(self, setting=True):
        super().train(setting)
        for mod in self.modals:
            self.modals[mod].train(setting)
        return self

    def eval(self):
        super().eval()
        for mod in self.modals:
            self.modals[mod].eval()
        return self

    def parameters(self):
        params = []
        for mod in self.modals:
            params += [p for p in self.modals[mod].parameters()]
        return params

    def soft_update(self, tau, tarnet):
        for mod in self.modals:
            tar_params = tarnet.modals[mod].parameters()
            params = self.modals[mod].parameters()
            for target_param, param in zip( tar_params, params ):
                target_param.data.copy_(
                  target_param.data * (1.0 - tau) + param.data * tau )
        return tarnet


class PretrainModel(nn.Module):
    """docstring for PretrainModel."""

    def __init__(self, config):
        super(PretrainModel, self).__init__()
        self.config = config

        self._modals = config['modalities']
        self.modalnet = ModalNet(config)

        self.rgb_preds = {}
        self.autoencs = {}
        for mod in self._modals:
            if mod == 'rgb':
                continue
            self.rgb_preds[mod] = FCNet(
              infeats=config['modalities']['rgb']['outfeats'],
              outfeats=config['modalities'][mod]['infeats'],
              layers=[int(config['modalities']['rgb']['outfeats']*0.5)]
            )
            self.autoencs[mod] = FCNet(
              infeats=config['modalities'][mod]['outfeats'],
              outfeats=config['modalities'][mod]['infeats'],
              layers=config['modalities'][mod]['layers'][::-1]
            )

    def forward(self, xdata):
        ydata = self.modalnet(xdata)
        preds = {}
        autencs = {}
        for mod in self.rgb_preds:
            preds[mod] = self.rgb_preds[mod](ydata['rgb'])
            autencs[mod] = self.autoencs[mod](ydata[mod])
        return preds, autencs

    def to(self, device):
        super().to(device)
        self.modalnet.to(device)
        for mod in self.rgb_preds:
            self.rgb_preds[mod].to(device)
            self.autoencs[mod].to(device)
        return self

    def train(self, setting=True):
        super().train(setting)
        for mod in self.rgb_preds:
            self.rgb_preds[mod].train(setting)
            self.autoencs[mod].train(setting)
        return self

    def eval(self):
        super().eval()
        for mod in self.rgb_preds:
            self.rgb_preds[mod].eval()
            self.autoencs[mod].eval()
        return self

    def parameters(self):
        params = self.modalnet.parameters()
        for mod in self.rgb_preds:
            params += [p for p in self.rgb_preds[mod].parameters()]
            params += [p for p in self.autoencs[mod].parameters()]
        return params

    def get_rgb_params(self):
        params = [p for p in self.modalnet.modals['rgb'].parameters()]
        for mod in self.rgb_preds:
            params += [p for p in self.rgb_preds[mod].parameters()]
        return params

    def get_autenc_params(self):
        params = []
        for mod in self.modalnet.modals:
            if mod != 'rgb':
                params += [p for p in self.modalnet.modals[mod].parameters()]
        for mod in self.autoencs:
            params += [p for p in self.autoencs[mod].parameters()]
        return params

    def load_params(self, targ_params):
        for mod in self.rgb_preds:
            self.rgb_preds[mod].load_state_dict(targ_params['rgb_preds'][mod])
            self.autoencs[mod].load_state_dict(targ_params['autoencs'][mod])

    def save_params(self):
        params = {'rgb_preds': {}, 'autoencs': {}}
        for mod in self.rgb_preds:
            params['rgb_preds'][mod] = self.rgb_preds[mod].state_dict()
            params['autoencs'][mod] = self.autoencs[mod].state_dict()
        return params
