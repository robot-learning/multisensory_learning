#!/bin/bash
#==========================================================================#
# This script copies local folder to scratch space.                        #
#==========================================================================#

#SBATCH --job-name=copy_folder_to_scratch
#SBATCH -o log-copy_folder-%j.out-%N
#SBATCH -e log-copy_folder-%j.err-%N
#SBATCH --nodes=1
#SBATCH --time=12:00:00
#SBATCH --mem=5G


export FOLDER=pick_object_samples

export SLURM_USER=adam.conkey
export LOCAL_FOLDER=$HOME/$FOLDER
export SCRATCH_DIR=/scratch/$SLURM_USER
export SCRATCH_FOLDER=$SCRATCH_DIR/$FOLDER


source $HOME/.bashrc
conda activate isaacgym

python -c "import slurm_helper; slurm_helper.print_timed_msg('Copying data to scratch space...')"

echo "Before copy:"
python -c "import slurm_helper; slurm_helper.print_dir('$SCRATCH_DIR')"

python -c "import slurm_helper; slurm_helper.copy_dir('$LOCAL_FOLDER', '$SCRATCH_FOLDER')"

echo "After copy:"
python -c "import slurm_helper; slurm_helper.print_dir('$SCRATCH_DIR')"

python -c "import slurm_helper; slurm_helper.print_timed_msg('Data copied to scratch space.')"
