
class ExpertInterface():
    """docstring for ExpertInterface."""

    def __init__(self, envir, max_steps=50):
        self.envir = envir
        self.max_steps = max_steps

    @property
    def device(self):
        return self.envir.rl_device

    def get_actions(self):
        raise NotImplementedError("get_actions is not implemented in this expert")

    def reset(self):
        raise NotImplementedError("reset is not implemented in this expert")

    def reset_idx(self, idx):
        raise NotImplementedError("reset_idx is not implemented in this expert")
