#!/usr/bin/env python
import os
import sys
import argparse
import random
import h5py
import numpy as np
import matplotlib.pyplot as plt

import torch
import torch.nn.functional as F

from multisensory_learning.util import file_util, data_util, torch_util

from multisensory_learning.learners.train_autoencoder import (
    get_models, encode_observations, decode_observations
)


def process_rgb_in(data):
    data = data.transpose(2, 0, 1) # (3, h, w)
    data = torch.tensor(data, dtype=torch.float32).unsqueeze(0)
    data = F.interpolate(data, (64, 64), mode='bilinear', align_corners=False)
    data.div_(255)
    return data


def process_rgb_out(data):
    data = F.interpolate(data, (512, 512), mode='bilinear', align_corners=False)
    data = data.cpu().detach().numpy()
    data *= 255
    data = data.astype(np.uint8)
    data = data.transpose(0, 2, 3, 1).squeeze()
    return data


def process_depth_in(data, in_min=-3, in_max=0, out_min=0, out_max=1):
    data = data_util.scale_min_max(data, in_min, in_max, out_min, out_max)
    data = torch.tensor(data, dtype=torch.float32).unsqueeze(0)
    data = F.interpolate(data.unsqueeze(0), (64, 64), mode='bilinear', align_corners=False)
    return data


def process_depth_out(data, in_min=0, in_max=1, out_min=-3, out_max=0):
    data = F.interpolate(data, (512, 512), mode='bilinear', align_corners=False)
    data = data.cpu().detach().numpy().squeeze()
    data = data_util.scale_min_max(data, in_min, in_max, out_min, out_max)
    return data


def process_joint_pos_in(data, in_min=-2*np.pi, in_max=2*np.pi, out_min=-1, out_max=1):
    data = torch.from_numpy(data).unsqueeze(0)
    data = data_util.scale_min_max(data, in_min, in_max, out_min, out_max)
    return data


def process_joint_pos_out(data, in_min=-1, in_max=1, out_min=-2*np.pi, out_max=2*np.pi):
    data = data.cpu().detach().numpy().squeeze()
    data = data_util.scale_min_max(data, in_min, in_max, out_min, out_max)
    return data


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--h5_root', type=str, required=True)
    parser.add_argument('--checkpoint', type=str, required=True)
    parser.add_argument('--device', type=str, default='cuda')
    args = parser.parse_args()

    file_util.check_path_exists(args.h5_root, "H5 directory")
    file_util.check_path_exists(args.checkpoint, "Checkpoint")

    checkpoint = torch.load(args.checkpoint)
    models = get_models(checkpoint)
    torch_util.load_state_dicts(models, checkpoint)
    torch_util.move_models_to_device(models, args.device)
    torch_util.set_models_to_eval(models)

    # Pick some random files and indices to grab data to try to reconstruct
    filenames = [f for f in file_util.list_dir(args.h5_root) if f.endswith('.h5')]
    filenames = random.choices(filenames, k=16)
    orig_data = {m: [] for m in checkpoint['modalities']}
    for filename in filenames:
        with h5py.File(filename, 'r') as h5_file:
            n_steps = len(h5_file['rgb'])
            idx = random.randint(0, n_steps - 1)
            for modality in checkpoint['modalities']:
                orig_data[modality].append(h5_file[modality][idx])


    inputs = {m: [] for m in checkpoint['modalities']}
    outputs = {m: [] for m in checkpoint['modalities']}
    for i in range(16):
        batch = {}
        for modality in checkpoint['modalities']:
            data = orig_data[modality][i]
            if modality == 'rgb':
                data = process_rgb_in(data)
            elif modality == 'depth':
                data = process_depth_in(data)
            elif modality == 'joint_position':
                data = process_joint_pos_in(data)
            else:
                raise ValueError(f"Unknown modality: {modality}")
            batch[modality] = data
            inputs[modality].append(data)
        torch_util.move_batch_to_device(batch, args.device)
        encoded = encode_observations(batch, models)
        decoded = decode_observations(encoded, models)
        for modality, decode in decoded.items():
            outputs[modality].append(decode)

    processed_inputs = {m: [] for m in checkpoint['modalities']}
    processed_outputs = {m: [] for m in checkpoint['modalities']}
    for modality, data_list in inputs.items():
        for data in data_list:
            if modality == 'rgb':
                data = process_rgb_out(data)
            elif modality == 'depth':
                data = process_depth_out(data)
            elif modality == 'joint_position':
                data = process_joint_pos_out(data)
            else:
                raise ValueError(f"Unknown modality: {modality}")
            processed_inputs[modality].append(data)
    for modality, data_list in outputs.items():
        for data in data_list:
            if modality == 'rgb':
                data = process_rgb_out(data)
            elif modality == 'depth':
                data = process_depth_out(data)
            elif modality == 'joint_position':
                data = process_joint_pos_out(data)
            else:
                raise ValueError(f"Unknown modality: {modality}")
            processed_outputs[modality].append(data)

    if 'rgb' in processed_inputs:
        fig, axes = plt.subplots(4, 4)
        fig.set_size_inches(20, 10)
        axes = axes.flatten()
        gts = processed_inputs['rgb']
        preds = processed_outputs['rgb']
        for i, (gt, pred) in enumerate(zip(gts, preds)):
            img = np.concatenate([gt, pred], axis=1)
            axes[i].imshow(img)
        plt.tight_layout()
        plt.show()
    if 'depth' in processed_inputs:
        fig, axes = plt.subplots(4, 4)
        fig.set_size_inches(20, 10)
        axes = axes.flatten()
        gts = processed_inputs['depth']
        preds = processed_outputs['depth']
        for i, (gt, pred) in enumerate(zip(gts, preds)):
            img = np.concatenate([gt, pred], axis=1)
            axes[i].imshow(img)
        plt.tight_layout()
        plt.show()
    if 'joint_position' in processed_inputs:
        fig, axes = plt.subplots(4, 4)
        fig.set_size_inches(10, 10)
        axes = axes.flatten()
        gts = processed_inputs['joint_position']
        preds = processed_outputs['joint_position']
        for i, (gt, pred) in enumerate(zip(gts, preds)):
            # TODO this is kind of a silly way to plot, can switch to scatter
            axes[i].plot(gt, label='gt')
            axes[i].plot(pred, label='pred')
        plt.legend()
        plt.tight_layout()
        plt.show()
