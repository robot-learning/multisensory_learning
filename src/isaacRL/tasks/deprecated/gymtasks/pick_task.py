
from isaacgym import gymtorch, gymapi

from isaacRL.tasks.gymtasks.expert_bandit_task import ExpertBanditTask
from isaacRL.utils.utils import get_pinv
from isaacRL.experts.pick import PickExpert, PickStepExpert

import os, time
import numpy as np
import torch

class PickBanditTask(ExpertBanditTask):
    """docstring for PickBanditTask."""

    PLANNERS = ['trajopt', 'jacobian']

    def __init__(self, config):

        self._dof = 7
        self._max_steps = config['max_env_steps']
        config['allow_resets'] = False

        self._action_modif = config['task'].get('action_modifier', '')
        self._planner = config['task'].get('planner', 'trajopt')
        assert self._planner in self.ActionPlanners.PLANNERS
        if 'rpl' in self._action_modif:
            expert = PickStepExpert(
                config['num_acts'],
                config['goal']['object'],
                same_starts=config['task'].get('same_starts', True),
                planner=self._planner,
                num_envs=config['num_envs'],
            )
            self._max_steps = expert.action_steps
        else:
            expert = PickExpert(
                config['num_acts'],
                config['goal']['object'],
                same_starts=config['task'].get('same_starts', True),
                planner=self._planner,
            )

        super(PickBanditTask, self).__init__(config, expert)
