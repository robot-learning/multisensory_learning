
from isaacRL.models.dynamics import DynamicsModel

import torch
import torch.nn.functional as F
import rospy


def trainer_dynaq(trainer):

    train_class = trainer.__class__

    trainer.dyna_model = DynamicsModel(
        trainer.observation_size, trainer.action_size, trainer.config['model']['dynamics']
    )
    trainer.dyna_model.to(trainer.device)
    trainer.dyna_optim = torch.optim.Adam(
        trainer.dyna_model.parameters(), lr=trainer.lr, weight_decay=trainer.weight_decay
    )

    train_class.prev_play = train_class.play
    def play(self, num_actions=1, evaluate=False, store_images=False):
        play_retvals = self.prev_play(num_actions=1, evaluate=False, store_images=False)
        if evaluate:
            return play_retvals
        with torch.no_grad():
            state = {'obs': self.state['obs'].clone()}
            for ai in range(num_actions):
                if rospy.is_shutdown():
                    return
                actions = self.actor(state, True)
                actions = actions.detach()
                nstate, rewards = self.dyna_model(state, actions)
                nstate = {'obs': nstate}
                terminals = torch.zeros_like(rewards).to(torch.long)
                self.buffer.add(
                    state, actions, nstate, rewards, terminals
                )
                state = nstate
        return play_retvals
    train_class.play = play

    train_class.prev_update_weights = train_class._update_weights
    def _update_weights(self, state, action, nstate, rewards, terminals):
        update_retvals = self.prev_update_weights(state, action, nstate, rewards, terminals)

        pstate, preward = self.dyna_model(state, action)
        dloss = F.mse_loss(pstate, nstate['obs']) + F.mse_loss(preward.unsqueeze(1), rewards)
        self.dyna_optim.zero_grad()
        dloss.backward()
        if self.clip_grad is not None:
            self.clip_grad(self.dyna_model.parameters())
        self.dyna_optim.step()
        return update_retvals
    train_class._update_weights = _update_weights

    return trainer
