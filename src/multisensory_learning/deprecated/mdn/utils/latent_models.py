
import os, sys, argparse
import numpy as np
import cv2
import pickle
import torch
import torch.nn as nn

from ll4ma_util import torch_util, func_util
from multisensory_learning.util import msp_util


class LatentModel(nn.Module):
    """docstring for LatentModel."""

    def __init__(self, checkpoint_filename, device):
        super(LatentModel, self).__init__()

        checkpoint = torch.load(checkpoint_filename)
        config = checkpoint['args']
        models = msp_util.get_models(config)
        torch_util.load_state_dicts(models, checkpoint)
        torch_util.move_models_to_device(models, device)
        torch_util.set_models_to_eval(models)

        self.config = config
        self.cfg = func_util.dict_to_dataclass(config)
        self.models = models

    def forward(self, xdata):
        return msp_util.apply_models(self.models, xdata, self.cfg)


class AutencModel(nn.Module):
    """docstring for AutencModel."""

    def __init__(self, checkpoint_filename, device):
        super(AutencModel, self).__init__()
        from multisensory_learning.train.train_autoencoder import (
          get_models, encode_observations
        )

        checkpoint = torch.load(checkpoint_filename)
        models = get_models(checkpoint)
        torch_util.load_state_dicts(models, checkpoint)
        torch_util.move_models_to_device(models, device)
        torch_util.set_models_to_eval(models)

        self.models = models
        self.encode = encode_observations

    def forward(self, xdata):
        return self.encode(xdata, self.models)
