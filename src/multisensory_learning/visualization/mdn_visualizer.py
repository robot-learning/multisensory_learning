#!/usr/bin/env python
import os
import sys
import pickle
import argparse
import itertools
import numpy as np
import cv2
import matplotlib.pyplot as plt

import rospy
from sensor_msgs.msg import JointState, Image
from tf2_msgs.msg import TFMessage
from geometry_msgs.msg import TransformStamped, PoseStamped, PoseArray
from visualization_msgs.msg import Marker, MarkerArray
from moveit_msgs.msg import DisplayRobotState

from multisensory_learning.learning import (
    MSPLearner, GoalLearner, MSPConfig, GoalLearnerConfig, msp_util
)
from ll4ma_util import data_util, file_util, ros_util, torch_util, func_util

import torch
from torch.distributions.multivariate_normal import MultivariateNormal


# TODO need to log link and joint names in the metadata so you don't have to do this BS
joint_names = ["iiwa_joint_{}".format(i) for i in range(1,8)]
joint_names += ["reflex_preshape_joint_1", "reflex_proximal_joint_1",
                "reflex_preshape_joint_2", "reflex_proximal_joint_2",
                "reflex_proximal_joint_3"]
link_names = ["iiwa_link_{}".format(i) for i in range(8)]
link_names += ["reflex_distal_link_{}".format(i) for i in [1,2,3]]
link_names += ["reflex_distal_pad_link_{}".format(i) for i in [1,2,3]]
link_names += ["reflex_proximal_link_{}".format(i) for i in [1,2,3]]
link_names += ["reflex_proximal_pad_link_{}".format(i) for i in [1,2,3]]
link_names += ["reflex_flex_link_{}".format(i) for i in [1,2,3]]
link_names += ["reflex_swivel_link_{}".format(i) for i in [1,2]]
link_names += ["reflex_pad_link", "reflex_shell_link"]


def decode_sample(sample, msp_learner):
    dec = msp_learner.decode_obs(sample, process_out=True)
    return {k: v if v.ndim > 1 else np.expand_dims(v, 0) for k, v in dec.items()}


def get_msgs(data, color='gold', alpha=1.0, marker_id_offset=0):
    msgs = {}
    if 'arm_joint_position' in data:
        # TODO hacking with hand state
        msgs['arm_joint_position'] = ros_util.get_display_robot_msg(
            np.concatenate([data['arm_joint_position'].squeeze(), np.zeros(5)]),
            joint_names, link_names, color, alpha
        )
        
    if 'ee_position' in data and 'ee_orientation' in data:
        orientation = data['ee_orientation'].squeeze()
        orientation /= np.linalg.norm(orientation)
        msgs['ee_pose'] = ros_util.get_pose_stamped_msg(data['ee_position'].squeeze(), orientation)
    elif 'ee_position' in data:
        msgs['ee_position'] = ros_util.get_marker_msg(
            data['ee_position'].squeeze(), (0.1, 0.1, 0.1), None, 'sphere', color, alpha
        )
        
    if 'obj_position' in data:
        msgs['obj_position'] = MarkerArray()
        for marker_id, (k, v) in enumerate(data['obj_position'].items()):
            msgs['obj_position'].markers.append(ros_util.get_marker_msg(
                v.squeeze(), (0.08, 0.08, 0.08), None, 'cube', color, alpha,
                marker_id_offset + marker_id))
            
    if 'rgb' in data:
        msgs['rgb'] = ros_util.rgb_to_msg(data['rgb'])
        
    return msgs


def visualize_data(data_filename, checkpoint_filename, device, rate=1.5, component=None):
    # Create goal learner
    file_util.check_path_exists(checkpoint_filename)
    goal_config = GoalLearnerConfig(checkpoint=checkpoint_filename, device=device)
    goal_learner = GoalLearner(goal_config, set_eval=True)
    
    # Create MSP learner for encoding contextual observations
    file_util.check_path_exists(goal_config.msp_checkpoint, "MSP checkpoint file")
    msp_config = MSPConfig(checkpoint=goal_config.msp_checkpoint, device=device)
    msp_config.n_arm_joints = 7 # TODO hacked
    msp_learner = MSPLearner(msp_config, set_eval=True)

    rospy.loginfo("Loading data...")
    data, attrs = file_util.load_pickle(data_filename)
    obj_names = [k for k, v in attrs['objects'].items() if not v['fix_base_link']]
    rospy.loginfo("Data loaded")

    display_data = {'gt': [], 'means': [], 'samples': []}

    n_samples = 10
    
    for time_idx in list(range(len(data['rgb'])))[::10]:
        latent_in = msp_learner.encode_raw_obs(data, time_idx, time_idx + 1)
        alpha, mu, sigma_tril = goal_learner.apply_mdn(latent_in)

        if component is None:
            component = torch.argmax(alpha).item()
        
        mvn = MultivariateNormal(mu[:,:,component], scale_tril=sigma_tril[:,:,:,component])
        mean = mu[:,:,component].unsqueeze(0)
        dec_mean = decode_sample(mean, msp_learner)
        dec_mean['obj_position'] = {}
        for k, v in dec_mean.items():
            if '_obj_position' in k:
                dec_mean['obj_position'][k] = v
        display_data['means'].append(dec_mean)

        samples = [mvn.sample().unsqueeze(0) for _ in range(n_samples)]
        dec_samples = []
        for sample in samples:
            dec_sample = decode_sample(sample, msp_learner)
            dec_sample['obj_position'] = {}
            for k, v in dec_sample.items():
                if '_obj_position' in k:
                    dec_sample['obj_position'][k] = v
            dec_samples.append(dec_sample)
        display_data['samples'].append(dec_samples)
        
        # Observation data
        gt = {
            'obj_position': {k: data['objects'][k]['position'][time_idx] for k in obj_names},
            'rgb': data['rgb'][time_idx],
            'arm_joint_position': data['joint_position'][time_idx,:attrs['n_arm_joints']]
        }
        display_data['gt'].append(gt)
                
    rospy.loginfo("Creating ROS messages...")
    n_timesteps = len(display_data['gt'])

    msgs = {
        'gt': [get_msgs(data, 'gold', 1.) for data in display_data['gt']],
        'means': [get_msgs(data, 'dodgerblue', 1.) for data in display_data['means']],
        'samples': [[get_msgs(data[i], 'red', 0.2, i*10) for i in range(n_samples)]
                    for data in display_data['samples']]
    }

    rospy.loginfo("ROS messages created.")

    joint_pos_pub = rospy.Publisher('/joint_pos', DisplayRobotState, queue_size=1)
    joint_pos_mean_pub = rospy.Publisher('/joint_pos_mean', DisplayRobotState, queue_size=1)
    joint_pos_sample_pubs = [rospy.Publisher(f'/joint_pos_sample_{i+1}', DisplayRobotState,
                                             queue_size=1) for i in range(n_samples)]
    # ee_pos_pred_pub = rospy.Publisher('/ee_pos_pred', Marker, queue_size=1)
    ee_pose_mean_pub = rospy.Publisher('/ee_pose_mean', PoseStamped, queue_size=1)
    ee_pose_sample_pub = rospy.Publisher('/ee_pose_samples', PoseArray, queue_size=1)
    obj_pos_pub = rospy.Publisher('/obj_pos', MarkerArray, queue_size=1)
    obj_pos_mean_pub = rospy.Publisher('/obj_pos_mean', MarkerArray, queue_size=1)
    obj_pos_sample_pub = rospy.Publisher(f'/obj_pos_samples', MarkerArray, queue_size=1)
    rgb_pub = rospy.Publisher('/rgb', Image, queue_size=1)
    
    rospy.loginfo("Publishing messages...")
    rate = rospy.Rate(rate)
    while not rospy.is_shutdown():
        for i in range(n_timesteps):
            marker_id = 0
            
            gt = msgs['gt'][i]
            means = msgs['means'][i]
            samples = msgs['samples'][i]

            for k, v in gt.items():
                if k == 'arm_joint_position':
                    ros_util.publish_msg(v, joint_pos_pub)
                elif k == 'obj_position':
                    ros_util.publish_msg(v, obj_pos_pub)
                elif k == 'rgb':
                    ros_util.publish_msg(v, rgb_pub)
                else:
                    raise ValueError(f"Don't know which publisher to use for gt msg: {k}")

            for k, v in means.items():
                if k == 'arm_joint_position':
                    ros_util.publish_msg(v, joint_pos_mean_pub)
                elif k == 'obj_position':
                    ros_util.publish_msg(v, obj_pos_mean_pub)
                elif k == 'ee_pose':
                    ros_util.publish_msg(v, ee_pose_mean_pub)
                else:
                    raise ValueError(f"Don't know which publisher to use for mean msg: {k}")

            obj_pos_samples = MarkerArray()
            ee_pose_samples = PoseArray()
            ee_pose_samples.header.frame_id = 'world'
            for j, sample in enumerate(samples):
                for k, v in sample.items():
                    if k == 'arm_joint_position':
                        ros_util.publish_msg(v, joint_pos_sample_pubs[j])
                    elif k == 'obj_position':
                        obj_pos_samples.markers += v.markers
                    elif k in ['ee_pose']:
                        ee_pose_samples.poses.append(v.pose)
                    else:
                        raise ValueError(f"Don't know which publisher to use for sample msg: {k}")

            if obj_pos_samples.markers:
                ros_util.publish_msg(obj_pos_samples, obj_pos_sample_pub)
            if ee_pose_samples.poses:
                ros_util.publish_msg(ee_pose_samples, ee_pose_sample_pub)
                    
            rate.sleep()
            
        rospy.sleep(2)  # Wait a bit after each display iteration
        
        

if __name__ == '__main__':
    rospy.init_node('mdn_visualizer')

    data_filename = rospy.get_param("~data_filename")
    checkpoint_filename = rospy.get_param("~goal_checkpoint")
    use_cpu = rospy.get_param("~cpu", False)
    rate = rospy.get_param("~rate", 1.5)
    component = rospy.get_param("~component", None)
    
    if component is not None and component < 0:
        component = None
        
    file_util.check_path_exists(data_filename, "Data file")
    file_util.check_path_exists(checkpoint_filename, "Model checkpoint file")
        
    device = 'cuda' if torch.cuda.is_available() and not use_cpu else 'cpu'

    visualize_data(data_filename, checkpoint_filename, device, rate, component)
