
from isaacgym.torch_utils import tf_vector
from isaacRL.utils.utils import get_log_dist
from isaacRL.utils.pose_proposal import get_proposer
from isaacRL.utils.goalies.base_goalie import Goalie

from ll4ma_util import torch_util

import torch
import numpy as np

@torch.jit.script
def placeShapeReward(
    ee_pose, obj_pose, goal_pose, actions,
    threshold, arm_joints, release,
    obj_w, rot_w, c_w, action_penalty_scale,
    obj_dw, rot_dw
):
    # type: (Tensor, Tensor, Tensor, Tensor, float, int, bool, float, float, float, float, float, float) -> Tuple[Tensor, Tensor, Tensor]
    bsize = ee_pose.shape[0] # batch size
    terminals = torch.zeros((bsize,), device=ee_pose.device, dtype=torch.long)
    amplifyer = torch.zeros((bsize,), device=ee_pose.device, dtype=torch.long)
    x_axis = torch.tensor([[1.,0.,0.]]*bsize, device=ee_pose.device)
    z_axis = torch.tensor([[0.,0.,1.]]*bsize, device=ee_pose.device)

    obj_diff = obj_pose[:,:3] - goal_pose[:,:3]
    obj_dist = torch.norm(obj_diff, p=2, dim=-1)
    obj_reward = (1.0 / (1.0 + obj_dist**2))**2

    # align the x and z axes of the object with the goal pose
    axis1 = tf_vector(obj_pose[:,3:7], x_axis)
    axis2 = tf_vector(goal_pose[:,3:7], x_axis)
    axis3 = tf_vector(obj_pose[:,3:7], z_axis)
    axis4 = tf_vector(goal_pose[:,3:7], z_axis)

    dot1 = torch.bmm(axis1.view(bsize, 1, 3), axis2.view(bsize, 3, 1)).squeeze(-1).squeeze(-1)
    dot2 = torch.bmm(axis3.view(bsize, 1, 3), axis4.view(bsize, 3, 1)).squeeze(-1).squeeze(-1)
    rot_err = (dot1 ** 2 + dot2 ** 2).sqrt()
    rot_err2 = torch.norm(obj_pose[:,3:7] - goal_pose[:,3:7], p=2, dim=-1)
    rot_err = torch.minimum(rot_err, rot_err2)
    rot_reward = (1.0 / (1.0 + rot_err))**2

    tot_dist = obj_dist*obj_dw + rot_err*rot_dw

    # TODO: add finger grasp reward? (to learn to grasp correctly)

    terms = tot_dist < threshold
    # TODO: add release option for stacking task
    # if release:
    #     terms = torch.and(terms, grasp_released)
    terminals[terms] = 1
    amplifyer[tot_dist < (threshold*2)] = 1

    rewards = obj_w * obj_reward + rot_w * rot_reward
    rewards[amplifyer == 1] *= 2 # c_w*0.2
    rewards[terminals == 1] *= 2000 # c_w*200

    if action_penalty_scale > 0:
        action_penalty = torch.sum(actions[:,:arm_joints] ** 2, dim=-1)
        rewards = rewards - action_penalty_scale * action_penalty

    return rewards, terminals, tot_dist
    # return rewards, terminals, (ee_dist, rot_err)

@torch.jit.script
def placeMultiSparseReward(
    ee_pose, objects, goal_pose, stand_bounds, actions,
    threshold, arm_joints, action_penalty_scale, release, obj_w, rot_w
):
    # type: (Tensor, Tensor, Tensor, Tensor, float, int, float, bool, float, float) -> Tuple[Tensor, Tensor, Tensor]

    assert objects.shape == goal_pose.shape

    bsize = objects.shape[0]
    objects = objects.reshape(bsize, -1, 7)
    num_obj = objects.shape[1]
    objects = objects.reshape(-1, 7)
    goal_pose = goal_pose.reshape(-1, 7)

    z_axis = torch.tensor([[0.,0.,1.]]*bsize, device=ee_pose.device)

    # align the z axis of the object with the goal pose | should be standing up
    obj_z = tf_vector(obj_pose[:,3:7], z_axis)
    goal_z = tf_vector(goal_pose[:,3:7], z_axis)
    rot_err = torch.bmm(obj_z.view(bsize, 1, 3), goal_z.view(bsize, 3, 1))
    rot_err = rot_err.reshape(bsize, num_obj)

    z_diff = (obj_pose[:,3:4] - goal_pose[:,3:4]).abs() < 0.1
    xmax = goal_pose[:, 0] + (stand_bounds[:, 0] / 2)
    xmin = goal_pose[:, 0] - (stand_bounds[:, 0] / 2)
    x_in = torch.logical_and(objects[:, 0] < xmax, objects[:, 0] > xmin)
    ymax = goal_pose[:, 1] + (stand_bounds[:, 1] / 2)
    ymin = goal_pose[:, 1] - (stand_bounds[:, 1] / 2)
    y_in = torch.logical_and(objects[:, 0] < ymax, objects[:, 0] > ymin)

    in_range_xy = torch.logical_and(x_in, y_in)
    in_range = torch.logical_and(in_range_xy, z_diff)
    in_range = in_range.reshape(bsize, num_obj).to(object.dtype)

    obj_placed = in_range.sum(1)

    obj_placed /= num_obj

    dist = obj_placed + rot_err

    # for sparse
    obj_place = torch.floor(obj_placed)
    terminals = torch.logical_and(obj_placed, rot_err < rot_w)
    rewards = terminals.clone().to(object.dtype)

    if action_penalty_scale > 0:
        action_penalty = torch.sum(actions[:,:arm_joints] ** 2, dim=-1)
        rewards = rewards - action_penalty_scale * action_penalty
    return rewards, terminals, dist


class PlaceObject(Goalie):
    """
    Goalie for learning the PlaceObject task:
    rewards small distance of object to goal,
    small distance of fingers to block?
    """

    def __init__( self,
            obj_name, position_range, orientation_range=None,
            stack_object=None, release=False,
            device='cpu', threshold=0.01, sparse=False,
            obj_w=1., rot_w=1., c_w=10., act_penal=0.001,
        ):
        goal_shape = 7
        super(PlaceObject, self).__init__(threshold, goal_shape, device, sparse)

        self.obj_name = obj_name
        self.release = release
        assert position_range is not None
        self.stack_object = stack_object
        self.ori_type = 'same' # options: random, same, axis+range/4 valued list
        if self.stack_object is not None:
            self.stack_offset = torch.tensor(position_range, device=self.device)
        else:
            position_range = torch.tensor(position_range, device=self.device)
            if len(position_range.shape) == 1:
                self.position_mins = position_range
                self.position_range = torch.zeros_like(position_range)
            else:
                self.position_mins = position_range[:,0]
                self.position_range = position_range[:,1] - self.position_mins
            if orientation_range is not None:
                self.ori_type = orientation_range
                if isinstance(self.ori_type, list):
                    self.ori_type = 'range'
                    orientation_range = torch.tensor(orientation_range, device=self.device)
                    self.ori_axis = orientation_range[:3]
                    self.ori_mins = orientation_range[3]
                    self.ori_range = orientation_range[4] - self.ori_mins

        self.obj_w = obj_w
        self.rot_w = rot_w
        self.c_w = c_w
        self.act_penal = act_penal

    def goal_dist(self, state, state_keys, goal):
        return state[:,state_keys[self.obj_name]][:,:self.goal_shape] - goal

    def get_current(self, state, state_keys):
        objects = torch.cat([state[:,state_keys[obj_name]][:,:self.goal_shape].clone() for obj_name in self.object_names], dim=1)
        return objects

    def get_goal(self, state, state_keys, noisy=False):
        goals = torch.cat([state[:,state_keys[obj_name]][:,:self.goal_shape].clone() for obj_name in self.object_names], dim=1)
        print(goals.shape)
        goals = goals.reshape(state.shape[0], -1, self.goal_shape)
        print(goals.shape)
        goals[:, :, :3] = state[:, state_keys[self.stack_object]][:, :3].reshape(state.shape[0], 1, 3)
        goals[:, :, :3] = goals[:, :, :3] + self.stack_offset[:3]
        goals = goals.reshape(state.shape[0], -1)
        print(goals.shape)
        import pdb; pdb.set_trace()
        return goals

    def reward(self, state, state_keys, goal):
        ee_pose = state[:,state_keys['ee_state']][:,:self.goal_shape]
        arm_joints = state[0,state_keys['arm_joints']]
        objects = torch.cat([state[:,state_keys[obj_name.replace('_noisy', '')]][:,:self.goal_shape].clone() for obj_name in self.object_names], dim=1)
        if self.sparse:
            return placeSparseReward(
                ee_pose, objects, goal, state[:,state_keys['prev_action']],
                self.threshold, arm_joints, self.act_penal, self.release,
                self.obj_w, self.rot_w
            )
        return placeShapeReward(
            ee_pose, obj_pose, goal, state[:,state_keys['prev_action']],
            self.threshold, arm_joints, self.release,
            self.obj_w, self.rot_w, self.c_w, self.act_penal, 1., 1.
        )

class PlaceObject_Factory(object):
    """docstring for PlaceObject_Factory."""

    def build(self,config):
        return PlaceObject(
          config['object'],
          config.get('prange', [0., 0., 0.1]),
          config.get('orange', None),
          config.get('stack_object', None),
          config.get('release', False),
          device=config.get('device', 'cpu'),
          threshold=config.get('threshold', 0.01),
          sparse=True,
          obj_w=config.get('obj_w', 1.),
          rot_w=config.get('rot_w', 1.),
          act_penal=config.get('act_penal', 0.01),
        )

class PlaceObjectShaped_Factory(object):
    """docstring for PlaceObjectShaped_Factory."""

    def build(self,config):
        return PlaceObject(
          config['object'],
          config.get('prange', [0., 0., 0.1]),
          config.get('orange', None),
          config.get('stack_object', None),
          config.get('release', False),
          device=config.get('device', 'cpu'),
          threshold=config.get('threshold', 0.01),
          sparse=False,
          obj_w=config.get('obj_w', 1.),
          rot_w=config.get('rot_w', 1.),
          c_w=config.get('c_w', 10.),
          act_penal=config.get('act_penal', 0.01),
        )
