
from isaacgym import gymtorch, gymapi
from isaacRL.train import *
from isaacRL.utils.grapher import EvalGrapher
from isaacRL.utils.display_rgb import make_gif
import matplotlib.pyplot as plt

def main():
    config = {'environ': {}}
    opt, trainer = setup(config, eval=True)
    config = trainer.config
    trainer._use_eval_data = True
    if opt.use_ros:
        import rospy
        print('using ros')
        rospy.init_node('eval_isaacgym')

    if opt.checkpt is not None:
        trainer.load_model(opt.checkpt)

    if config['model']['model_type'] == 'statepred':
        loss = trainer.evaluate()
        print('evaluation loss:', loss)
    else:
        full_times = 5
        max_steps = max(trainer.max_steps*full_times, 10) + 1#+(full_times-1)
        store_images = True  # not opt.headless
        tot_rew, mean_rew, successes, runs = trainer.evaluate(max_steps, store_images=store_images)
        if store_images and hasattr(trainer, 'rgb_images'):
            rgb_images = trainer.rgb_images
            gifname = opt.config.rsplit('/', 1)[-1].rsplit('.', 1)[0]
            chkpt_num = opt.checkpt.rsplit('_', 1)[-1].rsplit('.', 1)[0]
            print('rgb_images:', len(rgb_images))
            make_gif(rgb_images, 'data/'+gifname+'_chkpt'+chkpt_num+'.gif')

        print('total rewards:', tot_rew)
        print('mean rew per step:', mean_rew)
        print('successes:', successes)
        print('runs:', runs)
        print('percent:', successes/runs)
        if hasattr(trainer.envir, 'trajopt_time'):
            print('avg trajopt time:', trainer.envir.trajopt_time())
        if hasattr(trainer.envir, '_setdown_time'):
            print('setdown time:',trainer.envir._setdown_time)

        if opt.use_ros:
            import rospy
            rospy.is_shutdown()


if __name__ == '__main__':
    main()


    """
    Fix bug:

    Physics Engine: PhysX
    Physics Device: cpu
    GPU Pipeline: disabled
    JAC torch.Size([128, 32, 6, 12])
    Eval Iter 400: rew=0.003,suc=143: 100%|███████| 401/401 [01:19<00:00,  5.01it/s]
    total rewards: 143.0
    mean rew per step: 0.002786003740648379
    successes: 143
    runs: 128
    avg trajopt time: 30.7822242975235
    setdown time: 30.85825252532959
    (ll4ma) iain@iclee:~/isaac_ws/src/multisensory_learning/src/isaacRL$ python evaluate.py --config configs/picknplace/joint_rpla03.yaml

    """
