
from isaacgym import gymtorch
from isaacgym.torch_utils import tensor_clamp

from isaacRL.tasks.her_task import HERTask
from isaacRL.utils.data_utils import batch_dict

from collections import defaultdict
import numpy as np
import torch


class RPLTask(HERTask):
    """docstring for RPLTask."""

    def __init__(self, config, device):
        super(RPLTask, self).__init__(config, device)

    def post_physics_step(self, calc_rewards=False):
        """
        Perform operations after a simulator step has been made.
        """
        self.update_env_states()
        if calc_rewards:
            self.update_rewards()
