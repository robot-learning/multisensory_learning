
from isaacRL.tasks.sim_task import SimTask
from isaacRL.tasks.gym_tasks import GymTask

from isaacRL.trainers.trainer import Trainer

import torch, os, rospy


class TrainerRL(Trainer):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        device = "cuda" if torch.cuda.is_available() else "cpu"
        super(TrainerRL, self).__init__(config, device)

        self.ckpt_interval = self.config['training']['checkpoint_interval']
        self.acts_per_interval = self.config['training']['acts_per_interval']
        self.updates_per_interval = self.config['training']['updates_per_interval']
        os.makedirs(self.config['training']['checkpoint_path'], exist_ok=True)

        state_feats = 0
        for mod in config['model']['modalities']:
            if mod == 'rgb':
                state_feats += config['model']['modalities'][mod]['outfeats']
            else:
                state_feats += config['model']['modalities'][mod]
        config['environ']['num_obs'] = state_feats
        # config['environ']['actions'] = config['model']['actor']['actions']
        config['environ']['act_range'] = config['model']['actor']['act_range']

        if self.config['environ']['type'] == 'arm':
            self.envir = SimTask(self.config['environ'], self.device)
        else:
            self.envir = GymTask(self.config['environ'])

        self.expert = None
