
from isaacgymenvs.utils.torch_jit_utils import *
import torch
from isaacRL.utils.pytorch3d import se3_log_map, quaternion_to_matrix

def freeze_net(network):
    for param in network.parameters():
        param.requires_grad = False
    return True

def unfreeze_net(network):
    for param in network.parameters():
        param.requires_grad = True
    return True

# @torch.jit.script
# def get_eulers(q):
#     # type: (Tensor) -> Tensor
#     roll, pitch, yaw = get_euler_xyz(q)
#     roll, pitch, yaw = roll.unsqueeze(1), pitch.unsqueeze(1), yaw.unsqueeze(1)
#     eulers = torch.cat((roll, pitch, yaw), dim=1)
#     return eulers

@torch.jit.script
def tnormalize(tensor, ntype=2):
    # type: (Tensor, float) -> Tensor
    return tensor / torch.linalg.norm(tensor, ntype, dim=1)

@torch.jit.script
def tnorm(tensor, ntype=2):
    # type: (Tensor, float) -> Tensor
    return torch.linalg.norm(tensor, ntype, dim=1)

@torch.jit.script
def quatpose2matrix(pose):
    # type: (Tensor) -> Tensor
    transf = torch.zeros((pose.shape[0], 4, 4), dtype=pose.dtype, device=pose.device)
    transf[:,:-1,:-1] = quaternion_to_matrix(pose[:,3:7])
    transf[:,:-1,-1] = pose[:,:3]
    transf[:,-1,-1] += 1
    return transf

@torch.jit.script
def inv_transf(tran):
    # type: (Tensor) -> Tensor
    invt = tran.clone().transpose(1,2)
    invt[:,:-1,-1:] = -1*invt[:,:-1,:-1] @ invt[:,-1,:-1].unsqueeze(-1)
    invt[:,-1,:-1] *= 0.
    return invt

@torch.jit.script
def get_pinv(mat, inv_threshold):
    # type: (Tensor, float) -> Tensor
    umat,smat,vmat = torch.svd(mat)
    smat = 1 / torch.clamp(smat, min=inv_threshold)
    return vmat @ torch.diag_embed(smat) @ umat.transpose(1,2)

def ttrace(mat):
    return mat[:,0,0] + mat[:,1,1] + mat[:,2,2]

# @torch.jit.script
def get_log_diff(pose, goal):
    # type: (Tensor, Tensor) -> Tensor
    # you could also use quat_apply to combine pose and goal:
    # ipose = inv_quat(pose)
    # npose = quat_apply(ipose, goal)
    # result = torch.zeros((pose.shape[0], 6), device=pose.device, dtype=pose.dtype)
    # result[:,:3] = goal[:,:3] - pose[:,:3]
    # result[:,3:] += 0.001
    # return result

    result = torch.zeros((pose.shape[0], 6), device=pose.device, dtype=pose.dtype)
    result[:,:3] = goal[:,:3] - pose[:,:3]
    geul = torch.stack(get_euler_xyz(goal[:,3:]),1)
    peul = torch.stack(get_euler_xyz(pose[:,3:]),1)
    result[:,3:] = geul[:,:3] - peul[:,:3]
    return result

    trans = quatpose2matrix(pose)
    g_trans = quatpose2matrix(goal)
    invtrans = inv_transf(trans)
    # invtrans = get_pinv(trans)
    ntrans = invtrans @ g_trans
    # ntrans[:,:-1,:-1] = ntrans[:,:-1,:-1].transpose(1,2)
    result = se3_log_map(ntrans.transpose(1,2))
    return result

    trans = quaternion_to_matrix(pose[:,3:])
    g_trans = quaternion_to_matrix(goal[:,3:])
    invtrans = trans.transpose(1,2)
    ntrans = torch.bmm(invtrans, g_trans)
    ftrans = torch.zeros((trans.shape[0], 4, 4), device=trans.device)
    ftrans[:,:-1,:-1] = ntrans[:,:,:]
    ftrans[:,-1,-1] += 1
    result = se3_log_map(ftrans)
    return result

# @torch.jit.script
def get_log_dist(pose, goal):
    # type: (Tensor, Tensor) -> Tensor
    diff = get_log_diff(pose, goal)
    dist = tnorm(diff[:,:3], 2) + tnorm(diff[:,3:], 1)
    # dist = tnorm(pose[:,:3] - goal[:,:3], 2) + tnorm(diff[:,3:], 1)
    # dist = tnorm(pose[:,:3] - goal[:,:3], 2)
    return dist

"""
Taken from https://github.com/vitchyr/rlkit/blob/master/rlkit/exploration_strategies/ou_strategy.py
"""
class OUNoise(object):
    def __init__(self, action_space, mu=0.0, theta=0.15, max_sigma=0.3, min_sigma=0.3, decay_period=100000, episodic=False, device='cpu'):
        self.device         = device
        self.mu             = mu
        self.theta          = theta
        self.sigma          = max_sigma
        self.max_sigma      = max_sigma
        self.min_sigma      = min_sigma
        self.decay_period   = decay_period
        self.episodic = episodic
        self.action_dim     = action_space[0]
        act_range = torch.tensor(action_space[1]).to(self.device)
        if len(act_range.shape) == 1:
            self.low = act_range[0]
            self.high = act_range[1]
        else:
            assert act_range.shape[0] == self.action_dim[1]
            self.low = act_range[:,0]
            self.high = act_range[:,1]
        self.reset()

    def reset(self):
        self.state = torch.ones(self.action_dim).to(self.device) * self.mu

    def evolve_state(self, index=None):
        x  = self.state
        dx = self.theta * (self.mu - x) + self.sigma * torch.randn(self.action_dim).to(self.device)
        if index is None:
            self.state = x + dx
        else:
            self.state[index] = x[index] + dx[index]
        return self.state

    def get_action(self, action, t=0):
        if not self.episodic:
            self.evolve_state()
        ou_state = self.state
        self.sigma = (
          self.max_sigma -
          (self.max_sigma - self.min_sigma) * min(1.0, t / self.decay_period)
        )
        action = action + ou_state
        return torch.clip(action, self.low, self.high)
