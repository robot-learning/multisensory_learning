
from isaacRL.trainers.trainer import Trainer
from isaacRL.tasks import get_task
from isaacRL.models.statepred import StatePredictor
from isaacRL.utils.statepred_dataset import StatePredDataset
from isaacRL.experts.picknplace import PicknplaceExpert

import torch
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torch.utils.data.dataloader import default_collate

from tqdm import tqdm
import os, sys
import os.path as osp
import rospy

class TrainerStatePred(Trainer):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        device = "cuda" if torch.cuda.is_available() else "cpu"
        super(TrainerStatePred, self).__init__(config, device)

        self.save_best_after = self.config['training'].get('save_best_after', 1)

        os.makedirs(self.config['training']['checkpoint_path'], exist_ok=True)

        self.model = StatePredictor(self.config['model']['insize'], self.config['model'])
        self.model.to(self.device)

        self.optim = torch.optim.Adam(
            self.model.parameters(),  lr=self.lr, weight_decay=self.weight_decay
        )

        self.loss = []
        self.eval_loss = []

        self.train_set = None
        self.eval_set = None

    def generate_data(self, datafile, eval=False):
        # self.config['environ']['headless'] = False
        # self.config['environ']['num_envs'] = min(4, self.config['environ']['num_envs'])
        envir = get_task(self.config['environ'], self.device)
        if hasattr(envir, 'expert'):
            expert = envir.expert
        else:
            expert = PicknplaceExpert(
                self.config['environ']['num_acts'],
                self.config['environ']['goal']['object'],
                same_starts=self.config['environ']['task'].get('same_starts', True),
                planner=self.config['environ']['task'].get('planner', 'trajopt'),
            )
        data_size = self.config['training']['data_size']
        if eval:
            data_size = data_size // 15
        state = envir.reset()
        dataset = torch.zeros((data_size, state['fullstate'].shape[1]))
        data_idx = state['state_idx']
        iters = data_size // envir.num_envs
        cur_idx = 0

        if osp.exists(datafile):
            data = torch.load(datafile)
            cur_idx = data['fill_idx']
            dataset = data['states']

        next_idx = min(cur_idx+envir.num_envs, data_size)
        dataset[cur_idx:next_idx] = state['fullstate'][:(next_idx-cur_idx)]
        cur_idx = next_idx
        pbar = tqdm(total=(iters-cur_idx//envir.num_envs), file=sys.stdout)
        desc = f'Generating {datafile}'
        pbar.set_description(desc)
        for i in range(cur_idx//envir.num_envs, iters):
            if rospy.is_shutdown():
                return
            actions = expert.get_actions(state)
            state, _, _, _ = envir.step(actions)
            next_idx = min(cur_idx+envir.num_envs, data_size)
            dataset[cur_idx:next_idx] = state['fullstate'][:(next_idx-cur_idx)]
            cur_idx = next_idx
            if i % 1000 == 0:
                data = {
                    'states': dataset,
                    'indices': data_idx,
                    'full': False,
                    'fill_idx': cur_idx
                }
                torch.save(data, datafile)
            pbar.update(1)
            if cur_idx >= data_size:
                break
        pbar.close()

        data = {
            'states': dataset,
            'indices': data_idx,
            'full': True,
            'fill_idx': cur_idx
        }
        torch.save(data, datafile)
        print('Saved Data')
        del envir
        del expert

    def train(self):
        if self.train_set is None:
            if not osp.exists(self.config['data']['train']):
                self.generate_data(self.config['data']['train'])
            elif torch.load(self.config['data']['train'])['fill_idx'] < self.config['training']['data_size']:
                self.generate_data(self.config['data']['train'])
            self.train_set = StatePredDataset(
                self.config['data']['train'],
                self.config['data']['input_keys'],
                self.config['data']['pred_keys'],
                device=self.device
            )
            print(self.train_set[0][1])
            self.train_set = DataLoader(
                self.train_set,
                batch_size=self.config['training']['batch_size'],
                shuffle=True,
                num_workers=self.config['training']['n_cpu'],
                pin_memory=True,
                # collate_fn=trainset.collate_fn
                # collate_fn=lambda x: tuple(x_.to(self.device) for x_ in default_collate(x))
            )

        self.model.train()
        num_epochs = self.end_epoch-self.start_epoch
        for epoch in range(self.start_epoch, self.end_epoch):
            mean_loss = 0.
            pbar = tqdm(total=len(self.train_set), file=sys.stdout)
            desc = f'Train Epoch {epoch}'
            pbar.set_description(desc)
            for iter, (observ, state) in enumerate(self.train_set):
                pred_state = self.model(observ.to(self.device))
                loss = F.mse_loss(state.to(self.device), pred_state)
                if torch.isnan(loss):
                    import pdb; pdb.set_trace()
                self.optim.zero_grad()
                loss.backward()

                for name, param in self.model.named_parameters():
                    if torch.isnan(param.grad).any()  or torch.isinf(param.grad).any():
                        print("nan gradient found")
                        import pdb; pdb.set_trace()

                if self.clip_grad is not None:
                    self.clip_grad(self.model.parameters())
                self.optim.step()
                mean_loss += loss.item()

                desc = f'Train Epoch {epoch}: loss={(mean_loss/(iter+1)):.3f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            self.loss.append(mean_loss/len(self.train_set))

            if epoch >= self.save_best_after and epoch % self.eval_batch == 0:
                self.evaluate()
                self.model.train()
                self.save_model(epoch)

    def evaluate(self):
        if self.eval_set is None:
            if not osp.exists(self.config['data']['eval']):
                self.generate_data(self.config['data']['eval'], eval=True)
            elif torch.load(self.config['data']['eval'])['fill_idx'] < (self.config['training']['data_size'] // 15):
                self.generate_data(self.config['data']['eval'], eval=True)
            self.eval_set = StatePredDataset(
                self.config['data']['eval'],
                self.config['data']['input_keys'],
                self.config['data']['pred_keys'],
                device=self.device
            )
            self.eval_set = DataLoader(
                self.eval_set,
                batch_size=self.config['training']['batch_size'],
                shuffle=True,
                num_workers=self.config['training']['n_cpu'],
                pin_memory=True,
                # collate_fn=trainset.collate_fn
            )
        pbar = tqdm(total=len(self.eval_set), file=sys.stdout)
        desc = f'Eval'
        pbar.set_description(desc)
        self.model.eval()
        mean_loss = 0.
        diff = torch.zeros((7,))
        with torch.no_grad():
            for iter, (observ, state) in enumerate(self.eval_set):
                pred_state = self.model(observ.to(self.device))
                loss = F.mse_loss(state.to(self.device), pred_state)
                if iter == (len(self.eval_set)-1):
                    diff = (state.to(self.device) - pred_state).abs()
                if torch.isnan(loss):
                    import pdb; pdb.set_trace()
                for name, param in self.model.named_parameters():
                    if torch.isnan(param).any()  or torch.isinf(param).any():
                        print("nan parameter found:", name)
                mean_loss += loss.item()
                desc = f'Eval: loss={(mean_loss/(iter+1)):.3f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            self.eval_loss.append(mean_loss/len(self.eval_set))
        print(diff.mean(0))
        print(diff.std(0))
        return mean_loss

    def load_model(self, checkpt):
        checkpoint = torch.load(checkpt)
        self.model.load_state_dict(checkpoint['model'])
        if self.optim is not None:
            self.optim.load_state_dict(checkpoint['optim'])
        self.loss = checkpoint['loss']
        self.eval_loss = checkpoint['eval_loss']
        self.start_epoch = checkpoint['epochs']+1
        self.end_epoch += self.start_epoch

    def save_model(self, epoch):
        checkpoint = {}
        checkpoint['model'] = self.model.state_dict()
        checkpoint['optim'] = self.optim.state_dict()
        checkpoint['loss'] = self.loss
        checkpoint['eval_loss'] = self.eval_loss
        checkpoint['epochs'] = epoch
        filename = osp.join(
            self.config['training']['checkpoint_path'],
            self.config['training']['checkpoint_name'].format(epoch)
        )
        torch.save(checkpoint, filename)
        print('saved to', filename)
