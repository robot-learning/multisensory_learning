
from isaacgym import gymtorch, gymapi
from isaacRL.train import *
from isaacRL.utils.grapher import EvalGrapher
from isaacRL.utils.display_rgb import make_gif
import matplotlib.pyplot as plt

def main():
    config = {'environ': {'num_envs': 1}}
    opt, trainer = setup(config, eval=True)
    config = trainer.config
    if opt.use_ros:
        import rospy
        print('using ros')
        rospy.init_node('test her buffer')

    if opt.checkpt is not None:
        trainer.load_model(opt.checkpt)

    full_times = 2
    max_steps = trainer.max_steps*full_times + 1
    print('max_steps:', max_steps)
    print('playing')
    trainer.play(max_steps)
    print('played')
    buffer = trainer.buffer

    print('len:', len(buffer))
    print('len idx:', buffer.load_idx)
    assert len(buffer) == buffer.load_idx
    assert len(buffer) == ((max_steps-1)*(full_times+1))+(full_times+1)
    print('goals:', buffer._goals[:buffer.goal_idx])
    print('state shape:', buffer._states[0].shape)

    # Check that the buffer size is correct
    # Check that the first and second entries in the buffer are different (not overwriting)
    # Check that when the run is done that the buffer gets an extra version of the run with the goal added
    # Check that the reward for the added HER run is correct (shouldn't be the same as before)

    state0, action0, nstate0, reward0, terminal0 = buffer[0]
    state1, action1, nstate1, reward1, terminal1 = buffer[1]
    print('state0:', state0['obs'])
    print('state1:', state1['obs'])
    assert not (state0['obs'] == state1['obs']).all()

    print('state 0 steps:', buffer[0][0]['obs'])
    print('state max steps:', buffer[trainer.max_steps][0]['obs'])
    assert not (buffer[0][0]['obs'] == buffer[trainer.max_steps][0]['obs']).all()

    print('state 0 steps:', buffer[0][0])
    print('state last step:', buffer[trainer.max_steps-1][0])
    print('state 0 steps:', buffer[trainer.max_steps][0])
    print('state last step:', buffer[trainer.max_steps*2-1][0])

    print('reward 0 steps:', buffer[0][3])
    print('reward last step:', buffer[trainer.max_steps-1][3])
    print('reward 0 steps:', buffer[trainer.max_steps][3])
    print('reward last step:', buffer[trainer.max_steps*2-1][3])

    print('run 2:')

    start_idx = trainer.max_steps*2
    end_idx = start_idx + trainer.max_steps - 1
    print('state 0 steps:', buffer[trainer.max_steps*2][0])
    print('state last step:', buffer[trainer.max_steps*3-1][0])
    print('state 0 steps:', buffer[trainer.max_steps*3][0])
    print('state last step:', buffer[trainer.max_steps*4-1][0])

    print('reward 0 steps:', buffer[trainer.max_steps*2][3])
    print('reward last step:', buffer[trainer.max_steps*3-1][3])
    print('reward 0 steps:', buffer[trainer.max_steps*3][3])
    print('reward last step:', buffer[trainer.max_steps*4-1][3])

    import pdb; pdb.set_trace()


    if opt.use_ros:
        import rospy
        rospy.is_shutdown()


if __name__ == '__main__':
    main()


    """
    Fix bug:

    Physics Engine: PhysX
    Physics Device: cpu
    GPU Pipeline: disabled
    JAC torch.Size([128, 32, 6, 12])
    Eval Iter 400: rew=0.003,suc=143: 100%|███████| 401/401 [01:19<00:00,  5.01it/s]
    total rewards: 143.0
    mean rew per step: 0.002786003740648379
    successes: 143
    runs: 128
    avg trajopt time: 30.7822242975235
    setdown time: 30.85825252532959
    (ll4ma) iain@iclee:~/isaac_ws/src/multisensory_learning/src/isaacRL$ python evaluate.py --config configs/picknplace/joint_rpla03.yaml

    """
