
import matplotlib.pyplot as plt
import torch
import os.path as osp
import imageio

def make_gif(images, gifname):
    with imageio.get_writer(gifname, mode='I') as writer:
        for image in images:
            writer.append_data(image.to(torch.uint8).cpu().numpy())

def main():
    infile = osp.expanduser('~/data/isaac_sims/rl_runs/iiwa_rpl_rgb_objectmove1.torch')
    buff = torch.load(infile)
    goal0 = buff['goals'][0]
    samsies = (buff['goals'] == goal0).all(1)
    limit = 10000
    idx = torch.arange(limit)[samsies[:limit]]
    images = buff['states']['rgb'][idx]
    # idx = torch.arange(0, 650, 16)
    # idx = torch.arange(0, 100, 16)
    idx = torch.arange(0, len(images), 2)
    # for ii in idx:
    #     image = images[ii].permute(1, 2, 0)
    #     plt.imshow(image.numpy())
    #     plt.show()

    print('frames:', len(idx))
    make_gif(images, 'mygif.gif')
    # with imageio.get_writer('mygif.gif', mode='I') as writer:
    #     for ii in idx:
    #         image = (images[ii].permute(1, 2, 0)*255).to(torch.uint8)
    #         writer.append_data(image.numpy())


if __name__ == '__main__':
    main()
