
from isaacgym import gymtorch
from isaacgym.torch_utils import tensor_clamp

from isaacRL.tasks.task import Task
from isaacRL.utils.data_utils import batch_dict

from collections import defaultdict
import numpy as np
import torch


class RLTask(Task):
    """
    implements the pre_physics_step and post_physics_step functions,
    along with variables universally used by RL tasks.  Still requires
    implementation of update_rewards with will vary between different
    RL tasks, such as SAC and HER.
    """

    def __init__(self, config, device):
        super(RLTask, self).__init__(config, device)

        self.action_scale = 7.5
        self.rewards = torch.zeros(self.num_envs).to(device)
        self.terminals = torch.zeros(self.num_envs, dtype=torch.long).to(device)
        self.sparse_rewards = self.config['goal']['sparse_rewards']

        self.act_shape = self.config['action_shape']
        self.act_range = self.config['action_range']
        self.open_thresh = self.act_range[0]/2
        self.close_thresh = self.act_range[1]/2

    @property
    def dt(self):
        return self.simulator.config.sim.dt

    def update_rewards(self):
        raise NotImplementedError("update_rewards not implemented")

    def get_env_states(self):
        self.update_env_states()
        return self.torch_state

    def step_all(self, actions=None):

        self.step(actions)

        rews = self.rewards.clone()
        terms = self.terminals.clone()
        # self.reset_indices(self.terminals)
        return self.torch_state, rews, terms

    def pre_physics_step(self, actions):
        """
        Perform operations prior to a simulator step.
        Primarily computes actions.
        """
        if actions is not None:
            if torch.is_tensor(actions):
                actions = self.convert_tensor2action(
                  actions, self.simulator.actions )
            for envi, action in enumerate(actions):
                if action is not None:
                    self.simulator.apply_action(action, envi)
                    self.env_steps[envi] += 1

    def post_physics_step(self, calc_rewards=False):
        """
        Perform operations after a simulator step has been made. Handles rendering
        the visualizer and caching data in memory if data collection is active.
        """

        self.update_env_states()
        if calc_rewards:
            self.update_rewards()
            self.check_resets()
            self.reset_indices(self.terminals)

    def check_resets(self):
        self.terminals[self.env_steps > self.max_env_steps] = 1

    def convert_tensor2action(self, tenacts, actions=None):
        if actions is None:
            actions = [
              RobotAction( self.simulator.robot.arm.config, self.simulator.robot.end_effector.config )
                for _ in range(self.num_envs)
            ]
        for envi in range(self.num_envs):
            jpos = tenacts[envi, :-1].cpu().numpy()
            # jpos *= self.dt * self.action_scale
            jpos += actions[envi].get_arm_joint_position()
            actions[envi].set_arm_joint_position(jpos)
            if tenacts[envi, -1] < self.open_thresh:
                actions[envi].set_ee_discrete('open')
            elif tenacts[envi, -1] > self.close_thresh:
                actions[envi].set_ee_discrete('close')
        return actions

    def convert_action2tensor(self, base_acts):
        actions = torch.zeros((len(base_acts), self.act_shape))
        idx = [i for i in range(len(base_acts)) if base_acts[i] is not None]
        if len(idx) < 1:
            return actions
        for envi in range(self.num_envs):
            if base_acts[envi] is not None:
                curr = base_acts[envi].get_state_tensor( self.act_range )
                prev = self.simulator.actions[envi].get_state_tensor(
                  self.act_range
                )
                actions[envi] = curr - prev
                actions[envi,-1] = curr[-1]
        return actions
