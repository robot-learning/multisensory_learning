
import torch

from ll4ma_isaacgym.behaviors import behavior_util

from isaacRL.experts.picknplace import PicknplaceStepExpert


class DaggerPicknplaceStepExpert(PicknplaceStepExpert):
    """docstring for DaggerPicknplaceStepExpert."""

    def __init__(
        self,
        action_size,
        target_object,
        same_starts=True,
        grasp_offset=None,
        target_prior=None,
        planner='trajopt',
        num_envs=1,
        action_repeats=1,
        max_iterations=50,
        num_joints=7,
        joint_names=[],
        ee_link='reflex_palm_link',
        cartesian_path=False,
        changing=False
    ):
        super(DaggerPicknplaceStepExpert, self).__init__(
            action_size,
            target_object,
            same_starts,
            grasp_offset,
            target_prior,
            planner,
            num_envs,
            action_repeats,
            max_iterations,
            num_joints,
            joint_names,
            ee_link,
            False
        )
        self.__cartesian_path = cartesian_path
        self.changing = changing
        self.action_cnt = torch.zeros_like(self.action_idx)
        self.end_steps = 5  # add extra steps for target position
        self.action_cnt_max = self.action_steps + self.end_steps + 1
        print('expert max steps:', self.action_cnt_max)
        # self.trajopt.ee_trajectory = self.changing

    def get_actions(self, state, target_pose=None, envis=None, no_interp=False, take_step=True):

        if self.actions.device != state['full_state'].device:
            self.actions = self.actions.to(state['full_state'].device)

        if not self.changing:
            if (self.action_idx == 0).any():
                moveabove_idx = self.env_idx[self.action_idx == 0]
                moveaboveactions = self._get_moveaboveactions(
                    state, target_pose, moveabove_idx, interp=None
                ).transpose(1, 0)
                if self.__cartesian_path:
                    temp = moveaboveactions[:,:,:self.num_joints].reshape(-1, self.num_joints)
                    temp = self.trajopt.fk_solution(temp)
                    moveaboveactions[:,:,:self.num_joints] = temp.reshape(
                        moveaboveactions.shape[0], moveaboveactions.shape[1], self.num_joints
                    )
                self.actions[moveabove_idx] = moveaboveactions.to(state['full_state'].device)
        else:
            self._back_get_actions = self._get_actions
            self._get_actions = self._get_actions_custom
            moveabove_idx = self.action_idx < self.max_length
            steps_left = (1 + self.max_length - self.action_idx)
            # moveabove_idx = moveabove_idx and steps_left % self.timesteps == 0
            moveabove_idx = torch.logical_and(
                moveabove_idx,
                steps_left % self.timesteps == 0
            )
            if moveabove_idx.any():
            # if (self.action_idx == 0).any():
                interp = steps_left // self.timesteps
                # moveaboveactions = self._get_moveaboveactions(
                #     state, target_pose, moveabove_idx, interp=interp
                # ).transpose(1, 0)
                num_envs = state['full_state'].shape[0]
                if envis is None:
                    envis = self.env_idx
                start_joints = state['full_state'][self._joint_key][:,:self.num_joints]
                start_pose = state['full_state'][self._ee_key][:,:self.num_joints].clone()
                object_state = state['full_state'][self.target_object]
                if target_pose is None:
                    target_pose = self.get_target_pose(state['full_state'])
                target_pose = target_pose.clone()
                target_pose[:,2] += self.set_offset[:,2].to(state['full_state'].device)
                # print('interp:', interp)
                moveaboveactions, info = self._get_actions(
                    start_joints,
                    start_pose,
                    target_pose,
                    state,
                    num_envs,
                    envis[moveabove_idx],
                    repeats=1,
                    interp=interp
                )
                nex_idx = self.timesteps
                for ii, idx in enumerate(envis[moveabove_idx]):
                    maa = moveaboveactions[ii].to(state['full_state'].device)
                    maa[:, -1] = self.grasp_value
                    if self.action_idx[idx] == 0:
                        self.actions[idx] = maa[:]
                    elif self.action_idx[idx] == self.max_length[idx] - 1:
                        self.actions[idx, self.action_idx[idx]] = maa[-1]
                    else:
                        self.actions[idx, self.action_idx[idx]] = maa[nex_idx]

        actions = self.actions[self.env_idx, self.action_idx]
        control_idx = torch.logical_and(
            self.action_idx == self.max_length,
            self.action_cnt >= self.max_length + self.end_steps
        )
        if control_idx.any():
            num_envs = state['full_state'].shape[0]
            start_pose = state['full_state'][self._ee_key][:, :self.num_joints].clone()
            jacobian = state['full_state'][self._jac_key].reshape(num_envs,6,self.num_joints)
            if target_pose is None:
                target_pose = self.get_target_pose(state['full_state'])
            control, _ = self._get_controls(
                None,
                start_pose[control_idx].clone(),
                target_pose[control_idx].clone(),
                jacobian[control_idx].clone(),
                None,
                None,
            )
            actions[control_idx, :self.num_joints] = (
                control + state['full_state'][self._joint_key][:, :self.num_joints]
            )
        if take_step:
            self.action_idx = torch.minimum(self.action_idx+1, self.max_length)

        # actions = super().get_actions(state, target_pose, envis, no_interp, take_step)
        if take_step:
            self.action_cnt += 1
        self.action_idx[self.action_cnt == self.action_cnt_max] *= 0
        self.action_cnt[self.action_cnt == self.action_cnt_max] *= 0
        return actions[:, :self.num_joints]

    def _get_actions_custom(
        self,
        start_joints,
        start_pose,
        target_pose,
        obj_state,
        num_envs,
        envis,
        identical=False,
        repeats=1,
        interp=None
    ):
        if interp is None:
            interp = self.interp_steps
        actions = []
        infos = []
        for ei in envis:
            if not identical or ei == 0:
                trajectory, info = self.trajopt.get_trajectory(
                    start_joints[ei].cpu(),
                    target_pose[ei].cpu(),
                    obj_state,
                    interp=interp if isinstance(interp, int) else interp[ei],
                    repeats=repeats,
                    goal_ee_type='pose'
                )
                infos.append(info)
            trajectory = trajectory.to(start_joints.device).to(start_joints.dtype)
            trajectory = torch.cat(
                (trajectory, torch.zeros_like(trajectory[:,:1])+self.grasp_neutral),
                dim=1
            )
            actions.append(trajectory.to(start_joints.device).to(start_joints.dtype))
        return actions, infos

def auto_interp(trajectory, start_joints, max_actions):
    interp_traj = []
    # traj_diff = trajectory[1:] - trajectory[:-1]
    # val, idx = traj_diff.abs().max(1)
    # step_size = max_actions[idx] / val
    # interp_step = torch.lerp(trajectory[:-1], trajectory[1:], step_size.unsqueeze(1))
    prev_joints = start_joints
    for idx in range(len(trajectory)):
        joints = trajectory[idx]
        jdiff = joints - prev_joints
        if (jdiff.abs() > max_actions).any():
            val, idx = jdiff.abs().max(0)
            step_size = max_actions[idx] / val
            steps = torch.arange(0, 1, step_size.item()).unsqueeze(1).to(jdiff.device)
            bet_traj = torch.lerp(prev_joints, joints, steps)
            interp_traj.append(bet_traj)
            prev_joints = trajectory[idx]
        # else: dont update the prev joints
        # once the current start joints are too close to any given joint positions in the trajectory we can ignore that one and move on to the next one
        # eventually will arrive at a point where the distance between the current joints and the last joints is close enough to just have the last joints as the action
    interp_traj.append(trajectory[-1:])
    return torch.cat(interp_traj, dim=0)


class DaggerMoveitPlacer(DaggerPicknplaceStepExpert):
    """docstring for DaggerMoveitPlacer."""

    def __init__(
        self,
        action_size,
        target_object,
        behavior_states,
        same_starts=True,
        grasp_offset=None,
        target_prior=None,
        planner='trajopt',
        num_envs=1,
        action_repeats=1,
        max_iterations=50,
        num_joints=7,
        joint_names=[],
        ee_link='reflex_palm_link',
        changing=False
    ):
        super(DaggerMoveitPlacer, self).__init__(
            action_size,
            target_object,
            same_starts,
            grasp_offset,
            target_prior,
            planner,
            num_envs,
            action_repeats,
            max_iterations,
            num_joints,
            joint_names,
            ee_link,
            changing
        )
        self.behavior_states = behavior_states
        self._traj_type = 'position' # position, velocity, acceleration, cartesian

    def _update_behavior_states(self, state, envis=None):
        if envis is None:
            envis = torch.arange(len(state))
        for env_idx in envis:
            self.behavior_states[env_idx].joint_position = \
                state['joint_position'][env_idx].unsqueeze(-1)
            self.behavior_states[env_idx].joint_velocity = \
                state['joint_velocity'][env_idx].unsqueeze(-1)
            self.behavior_states[env_idx].joint_torque = \
                state['joint_torque'][env_idx].unsqueeze(-1)

            self.behavior_states[env_idx].timestep = self.timesteps

            self.behavior_states[env_idx].prev_action = \
                state["prev_action"][env_idx]

            self.behavior_states[env_idx].ee_state = state["ee_state"][env_idx]

            for obj_name in self.behavior_states[env_idx].objects:
                self.behavior_states[env_idx].object_states[obj_name] = \
                    state[obj_name][env_idx]

    def _get_actions(
        self,
        start_joints,
        start_pose,
        target_pose,
        state,
        num_envs,
        envis,
        identical=False,
        repeats=1,
        interp=None
    ):
        self._update_behavior_states(state, envis)
        # Setup getting trajectories from moveit
        beh_state = self.behavior_states
        ee_link = beh_state[0].ee_link
        joint_names = beh_state[0].joint_names
        frame = 'world'
        max_vel_factor = 0.3
        max_acc_factor = 0.1
        max_plan_attempts = 2
        planning_time = 1.0
        cartesian_path = False
        min_cartesian_pct = 1.0
        collisionfree_objs = [self.target_object]
        attached_objects = {}

        maxlen = 0
        trajectories = []
        for env_idx in envis:
            plan, success = behavior_util.get_plan(
                target_pose[env_idx],
                beh_state[env_idx],
                ee_link,
                frame,
                collisionfree_objs,
                max_vel_factor,
                max_acc_factor,
                max_plan_attempts,
                planning_time,
                cartesian_path,
                min_cartesian_pct,
                attached_objects,
                # touch_links=self.robot.end_effector.get_touch_links(),
            )
            traj = []
            if plan is not None and success:
                traj = [point.positions for point in plan.trajectory.joint_trajectory.points]
                if self._traj_type == 'cartesian':
                    traj, suc = behavior_util.get_fk(traj, joint_names, ee_link)
                    traj = [point.positions for point in traj.points]
                elif self._traj_type == 'velocities':
                    traj = [point.velocities for point in plan.trajectory.joint_trajectory.points]
            trajectories.append(traj)
            maxlen = max(maxlen, len(traj))
            print('traj len:', len(traj))
        actions = torch.zeros((len(envis), maxlen, self.num_joints))
        for i in range(len(envis)):
            traj = trajectories[i]
            if len(traj) > 0:
                traj = torch.tensor(traj)
                actions[i,:len(traj)] = traj
                actions[i,len(traj):] = traj[-1].unsqueeze(0)
            else:
                actions[i,:] = start_joints[envis[i]]
        # print(actions.shape)
        # import pdb; pdb.set_trace()
        return actions, {}
