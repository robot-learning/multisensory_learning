
import os, random

# base_dir = '/media/iain/iains_drive/isaacsim_data'
base_dir = '/home/iain/data/isaac_sims/pickles'
picls = os.listdir(base_dir)
random.shuffle(picls)

trains = picls[:int(0.8*len(picls))]
with open('train_pickles.txt', 'w') as f:
    for fil in trains:
        f.write(os.path.join(base_dir, fil))
        f.write('\n')

valids = picls[int(0.8*len(picls)):]
with open('valid_pickles.txt', 'w') as f:
    for fil in valids:
        f.write(os.path.join(base_dir, fil))
        f.write('\n')
