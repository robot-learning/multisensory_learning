
from isaacgym import gymutil, gymtorch, gymapi
import torch
from isaacgym.torch_utils import *


#####################################################################
###=========================jit functions=========================###
#####################################################################


@torch.jit.script
def compute_stack_reward(
    reset_buf, progress_buf, actions, block1_pos,
    robot_grasp_pos, block1_grasp_pos,
    robot_grasp_rot, block1_grasp_rot,
    robot_lfinger_pos, robot_rfinger_pos,
    gripper_forward_axis, block1_inward_axis,
    gripper_x_axis, block1_x_axis,
    num_envs, dist_reward_scale,
    rot_reward_scale, around_handle_reward_scale, open_reward_scale,
    finger_dist_reward_scale, action_penalty_scale, max_episode_length
):
    # type: (Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, int, float, float, float, float, float, float, float) -> Tuple[Tensor, Tensor]

    # distance from hand to block1
    dist = torch.norm(robot_grasp_pos - block1_grasp_pos, p=2, dim=-1)
    dist_reward = 1.0 / (1.0 + dist ** 2)
    dist_reward *= dist_reward
    dist_reward = torch.where(dist <= 0.02, dist_reward * 2, dist_reward)

    axis1 = tf_vector(robot_grasp_rot, gripper_forward_axis)
    axis2 = tf_vector(block1_grasp_rot, block1_inward_axis)
    axis3 = tf_vector(robot_grasp_rot, gripper_x_axis)
    axis4 = tf_vector(block1_grasp_rot, block1_x_axis)

    dot1 = torch.bmm(axis1.view(num_envs, 1, 3), axis2.view(num_envs, 3, 1)).squeeze(-1).squeeze(-1)  # alignment of forward axis for gripper
    dot2 = torch.bmm(axis3.view(num_envs, 1, 3), axis4.view(num_envs, 3, 1)).squeeze(-1).squeeze(-1)  # alignment of x axis for gripper
    # reward for matching the orientation of the hand to the block (fingers wrapped)
    rot_reward = 0.5 * (torch.sign(dot1) * dot1 ** 2 )#+ torch.sign(dot2) * dot2 ** 2)

    # bonus if left finger is left of the block handle and right right
    around_handle_reward = torch.zeros_like(rot_reward)
    around_handle_reward = torch.where(
      robot_lfinger_pos[:, 1] > block1_grasp_pos[:, 1],
      torch.where(
        robot_rfinger_pos[:, 1] < block1_grasp_pos[:, 1],
        around_handle_reward + 0.5, around_handle_reward
      ),
      around_handle_reward
    )
    # reward for distance of each finger from the block
    finger_dist_reward = torch.zeros_like(rot_reward)
    lfinger_dist = torch.abs(robot_lfinger_pos[:, 2] - block1_grasp_pos[:, 2])
    rfinger_dist = torch.abs(robot_rfinger_pos[:, 2] - block1_grasp_pos[:, 2])
    finger_dist_reward = torch.where(
      robot_lfinger_pos[:, 2] > block1_grasp_pos[:, 2],
      torch.where(
        robot_rfinger_pos[:, 2] < block1_grasp_pos[:, 2],
        (0.04 - lfinger_dist) + (0.04 - rfinger_dist),
        finger_dist_reward
      ),
      finger_dist_reward
    )

    # regularization on the actions (summed for each environment)
    action_penalty = torch.sum(actions ** 2, dim=-1)

    # how high block1 is (using the z position of block1)
    open_reward = block1_pos[:, 2] * around_handle_reward + block1_pos[:, 2]

    rewards = (
      dist_reward_scale * dist_reward
      + rot_reward_scale * rot_reward
      + around_handle_reward_scale * around_handle_reward
      + open_reward_scale * open_reward
      + finger_dist_reward_scale * finger_dist_reward
      - action_penalty_scale * action_penalty
    )

    # reset if drawer is open or max length reached
    reset_buf = torch.where(block1_pos[:, 2] > 0.3, torch.ones_like(reset_buf), reset_buf)
    reset_buf = torch.where(
      progress_buf >= max_episode_length - 1,
      torch.ones_like(reset_buf),
      reset_buf
    )

    return rewards, reset_buf

@torch.jit.script
def compute_cabinet_reward(
    reset_buf, progress_buf, actions, cabinet_dof_pos,
    robot_grasp_pos, drawer_grasp_pos, robot_grasp_rot, drawer_grasp_rot,
    robot_lfinger_pos, robot_rfinger_pos,
    gripper_forward_axis, drawer_inward_axis, gripper_up_axis, drawer_up_axis,
    num_envs, dist_reward_scale, rot_reward_scale, around_handle_reward_scale, open_reward_scale,
    finger_dist_reward_scale, action_penalty_scale, distX_offset, max_episode_length
):
    # type: (Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, int, float, float, float, float, float, float, float, float) -> Tuple[Tensor, Tensor]

    # distance from hand to the drawer
    d = torch.norm(robot_grasp_pos - drawer_grasp_pos, p=2, dim=-1)
    dist_reward = 1.0 / (1.0 + d ** 2)
    dist_reward *= dist_reward
    dist_reward = torch.where(d <= 0.02, dist_reward * 2, dist_reward)

    axis1 = tf_vector(robot_grasp_rot, gripper_forward_axis)
    axis2 = tf_vector(drawer_grasp_rot, drawer_inward_axis)
    axis3 = tf_vector(robot_grasp_rot, gripper_up_axis)
    axis4 = tf_vector(drawer_grasp_rot, drawer_up_axis)

    dot1 = torch.bmm(axis1.view(num_envs, 1, 3), axis2.view(num_envs, 3, 1)).squeeze(-1).squeeze(-1)  # alignment of forward axis for gripper
    dot2 = torch.bmm(axis3.view(num_envs, 1, 3), axis4.view(num_envs, 3, 1)).squeeze(-1).squeeze(-1)  # alignment of up axis for gripper
    # reward for matching the orientation of the hand to the drawer (fingers wrapped)
    rot_reward = 0.5 * (torch.sign(dot1) * dot1 ** 2 + torch.sign(dot2) * dot2 ** 2)

    # bonus if left finger is above the drawer handle and right below
    around_handle_reward = torch.zeros_like(rot_reward)
    around_handle_reward = torch.where(robot_lfinger_pos[:, 2] > drawer_grasp_pos[:, 2],
                                       torch.where(robot_rfinger_pos[:, 2] < drawer_grasp_pos[:, 2],
                                                   around_handle_reward + 0.5, around_handle_reward), around_handle_reward)
    # reward for distance of each finger from the drawer
    finger_dist_reward = torch.zeros_like(rot_reward)
    lfinger_dist = torch.abs(robot_lfinger_pos[:, 2] - drawer_grasp_pos[:, 2])
    rfinger_dist = torch.abs(robot_rfinger_pos[:, 2] - drawer_grasp_pos[:, 2])
    finger_dist_reward = torch.where(robot_lfinger_pos[:, 2] > drawer_grasp_pos[:, 2],
                                     torch.where(robot_rfinger_pos[:, 2] < drawer_grasp_pos[:, 2],
                                                 (0.04 - lfinger_dist) + (0.04 - rfinger_dist), finger_dist_reward), finger_dist_reward)

    # regularization on the actions (summed for each environment)
    action_penalty = torch.sum(actions ** 2, dim=-1)

    # how far the cabinet has been opened out
    open_reward = cabinet_dof_pos[:, 3] * around_handle_reward + cabinet_dof_pos[:, 3]  # drawer_top_joint

    rewards = dist_reward_scale * dist_reward + rot_reward_scale * rot_reward \
        + around_handle_reward_scale * around_handle_reward + open_reward_scale * open_reward \
        + finger_dist_reward_scale * finger_dist_reward - action_penalty_scale * action_penalty

    # bonus for opening drawer properly
    rewards = torch.where(cabinet_dof_pos[:, 3] > 0.01, rewards + 0.5, rewards)
    rewards = torch.where(cabinet_dof_pos[:, 3] > 0.2, rewards + around_handle_reward, rewards)
    rewards = torch.where(cabinet_dof_pos[:, 3] > 0.39, rewards + (2.0 * around_handle_reward), rewards)

    # prevent bad style in opening drawer
    rewards = torch.where(robot_lfinger_pos[:, 0] < drawer_grasp_pos[:, 0] - distX_offset,
                          torch.ones_like(rewards) * -1, rewards)
    rewards = torch.where(robot_rfinger_pos[:, 0] < drawer_grasp_pos[:, 0] - distX_offset,
                          torch.ones_like(rewards) * -1, rewards)

    # reset if drawer is open or max length reached
    reset_buf = torch.where(cabinet_dof_pos[:, 3] > 0.39, torch.ones_like(reset_buf), reset_buf)
    reset_buf = torch.where(progress_buf >= max_episode_length - 1, torch.ones_like(reset_buf), reset_buf)

    return rewards, reset_buf


@torch.jit.script
def compute_grasp_transforms(hand_rot, hand_pos, robot_local_grasp_rot, robot_local_grasp_pos,
                             drawer_rot, drawer_pos, drawer_local_grasp_rot, drawer_local_grasp_pos
                             ):
    # type: (Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor) -> Tuple[Tensor, Tensor, Tensor, Tensor]

    global_robot_rot, global_robot_pos = tf_combine(
        hand_rot, hand_pos, robot_local_grasp_rot, robot_local_grasp_pos)
    global_drawer_rot, global_drawer_pos = tf_combine(
        drawer_rot, drawer_pos, drawer_local_grasp_rot, drawer_local_grasp_pos)

    return global_robot_rot, global_robot_pos, global_drawer_rot, global_drawer_pos

def test_rewards():
    num_envs = 1
    device = "cuda" if torch.cuda.is_available() else "cpu"
    rew_buf = torch.zeros((num_envs,), device=device)
    reset_buf = torch.zeros((num_envs,), device=device)
    rew_buf[:], reset_buf[:] = compute_stack_reward(
      reset_buf, progress_buf, actions, block1_states,
      robot_grasp_pos, block_grasp_pos, robot_grasp_rot,
      block_grasp_rot, robot_lfinger_pos, robot_rfinger_pos,
      gripper_forward_axis, block_inward_axis, gripper_x_axis,
      block_x_axis, num_envs, dist_reward_scale,
      rot_reward_scale, around_handle_reward_scale,
      open_reward_scale, finger_dist_reward_scale,
      action_penalty_scale, max_episode_length
    )
    import pdb; pdb.set_trace()

if __name__ == '__main__':
    test_rewards()
