
import sys, os
import os.path as osp
import pickle
import numpy as np
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset

from utils.process_observations import process_data_in
from oracles.above_object_oracle import AboveObjectOracle
from multisensory_learning.util import data_util, torch_util
from utils.latent_models import AutencModel


def process_data_in(data, modality, data_ranges):
    if modality == 'rgb':
        data = data.transpose(0, 3, 1, 2) # (t, 3, h, w)
        data = torch.tensor(data, dtype=torch.float32)
        data = F.interpolate(data, (64, 64), mode='bilinear', align_corners=False)
        data.div_(255) # Normalize to range [0, 1]
    elif modality == 'depth':
        low, high = data_ranges[modality]
        data = data_util.scale_min_max(data, low, high, 0, 1)
        data = torch.tensor(data, dtype=torch.float32).unsqueeze(1) # Add channel: (t, 1, h, w)
        data = F.interpolate(data, (64, 64), mode='bilinear', align_corners=False) # (t, 1, 64, 64)
    elif modality in data_ranges:
        low, high = data_ranges[modality]
        data = data_util.scale_min_max(data, low, high, 0, 1)
        data = torch.tensor(data, dtype=torch.float32)
    else:
        raise ValueError(f"Unknown modality for processing data into network: {modality}")
    return data


latent_checkpt = '../multisensory_learning/checkpoints/autoencoder_epoch_021.pt'
root = '/home/iain/data/isaac_sims/pickles/'
savedir = '/home/iain/data/isaac_sims/autenc_states/'
batch_size = 8
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
oracle = AboveObjectOracle()
# obs_mods = [ 'rgb', 'joint_position', 'joint_velocity', 'joint_torque' ]
obs_mods = [ 'rgb', 'joint_position' ]

latent_model = AutencModel(latent_checkpt, device)

data_ranges = {
    'joint_position': [-2*np.pi, 2*np.pi],
    'joint_velocity': [-3, 3],
    'joint_torque': [-30, 30],
    'delta_joint_position': [-0.3, 0.3],
    'depth': [-3, 0]
}

with torch.no_grad():
    fileps = os.listdir(root)
    for filep in tqdm(fileps):
        filepath = osp.join(root, filep)
        savepath = (savedir, filep)
        with open(filepath, 'rb') as f:
            data, attrs = pickle.load(f)
            goals = oracle.generate_labels(data, 'box')

            for modality in data_ranges:
                if modality == 'delta_joint_position':
                    joint_pos = data['joint_position']
                    deltas = joint_pos[1:] - joint_pos[:-1]
                    data_min = np.min(deltas)
                    data_max = np.max(deltas)
                else:
                    data_min = np.min(data[modality])
                    data_max = np.max(data[modality])
                data_ranges[modality][0] = min(data_ranges[modality][0], float(data_min))
                data_ranges[modality][1] = max(data_ranges[modality][1], float(data_max))

            xdata = {}
            for m in obs_mods:
                xdata[m] = process_data_in( data[m], m, data_ranges )

            # for xdata in
            torch_util.move_batch_to_device(xdata, device)
            ldata = latent_model(xdata)
        savedat = {'state': ldata.cpu().numpy(), 'targets': goals}
        savepath = osp.join(savedir, filep)
        np.save(savepath, savedat)
        # with open(savepath, 'w') as f:
        #     pickle.dump( savedat, f )
