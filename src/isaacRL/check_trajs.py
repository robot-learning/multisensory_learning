
import torch, os, argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("filepath", type=str, nargs=1, help="path to save file")
    opt = parser.parse_args()
    saves = torch.load(opt.filepath[0])
    print('start states shape:',saves['start_states'].shape)
    print('potentials shape:',saves['potentials'].shape)
    print('num_potentials:', saves['num_potentials'])
    print('nonzeros:', (saves['start_states'].sum(dim=-1) > 0).sum())

if __name__ == '__main__':
    main()
