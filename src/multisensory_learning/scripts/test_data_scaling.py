#!/usr/bin/env python3
import os
import h5py
import numpy as np
import argparse
import matplotlib.pyplot as plt

from multisensory_learning.common import get_dataset_metadata
from multisensory_learning.util import file_util, ui_util


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--h5_filename', type=str, required=True,
                        help="Absolute path to H5 data file.")
    args = parser.parse_args()
    h5_root = os.path.dirname(os.path.dirname(args.h5_filename))
    h5_stats_filename = os.path.join(h5_root, "h5_statistics.yaml")
    
    file_util.check_path_exists(args.h5_filename)
    file_util.check_path_exists(h5_stats_filename)

    metadata = get_dataset_metadata('isaac', h5_stats_filename)
    
    with h5py.File(args.h5_filename, 'r') as h5_file:
        joint_pos = np.array(h5_file[metadata.get_h5_key('joint_positions')])
        joint_vel = np.array(h5_file[metadata.get_h5_key('joint_velocities')])
        rgb = np.array(h5_file[metadata.get_h5_key('rgb')])
        depth = np.array(h5_file[metadata.get_h5_key('depth')])
        gripper_joint_pos = np.array(h5_file[metadata.get_h5_key('gripper_joint_positions')])
        gripper_joint_vel = np.array(h5_file[metadata.get_h5_key('gripper_joint_velocities')])
        # ee_pos = np.array(h5_file[metadata.get_h5_key('ee_position')])
        delta_joint_pos = joint_pos[1:,:] - joint_pos[:-1,:]

        joint_pos_in = metadata.scale_data_in(joint_pos, 'joint_positions')
        joint_vel_in = metadata.scale_data_in(joint_vel, 'joint_velocities')
        rgb_in = metadata.scale_data_in(rgb, 'rgb')
        depth_in = metadata.scale_data_in(depth, 'depth')
        gripper_joint_pos_in = metadata.scale_data_in(gripper_joint_pos, 'gripper_joint_positions')
        gripper_joint_vel_in = metadata.scale_data_in(gripper_joint_vel, 'gripper_joint_velocities')
        # ee_pos_in = metadata.scale_data_in(ee_pos, 'ee_position')
        delta_joint_pos_in = metadata.scale_data_in(delta_joint_pos, 'delta_joint_positions')

        joint_pos_out = metadata.scale_data_out(joint_pos_in, 'joint_positions')
        joint_vel_out = metadata.scale_data_out(joint_vel_in, 'joint_velocities')
        rgb_out = metadata.scale_data_out(rgb_in, 'rgb')
        depth_out = metadata.scale_data_out(depth_in, 'depth')
        gripper_joint_pos_out = metadata.scale_data_out(gripper_joint_pos_in,
                                                        'gripper_joint_positions')
        gripper_joint_vel_out = metadata.scale_data_out(gripper_joint_vel_in,
                                                        'gripper_joint_velocities')
        # ee_pos_out = metadata.scale_data_out(ee_pos_in, 'ee_position')
        delta_joint_pos_out = metadata.scale_data_out(delta_joint_pos_in, 'delta_joint_positions')

        np.testing.assert_array_almost_equal(joint_pos, joint_pos_out)
        np.testing.assert_array_almost_equal(joint_vel, joint_vel_out)
        np.testing.assert_array_almost_equal(rgb, rgb_out)
        np.testing.assert_array_almost_equal(depth, depth_out)
        np.testing.assert_array_almost_equal(gripper_joint_pos, gripper_joint_pos_out)
        np.testing.assert_array_almost_equal(gripper_joint_vel, gripper_joint_vel_out)
        # np.testing.assert_array_almost_equal(ee_pos, ee_pos_out)
        np.testing.assert_array_almost_equal(delta_joint_pos, delta_joint_pos_out)

        ui_util.print_happy("\nEverything passed :)\n")

        fig, axes = plt.subplots(1, 2)
        axes[0].plot(joint_pos)
        axes[1].plot(joint_pos_in)
        plt.title('Joint Positions')
        plt.show()

        fig, axes = plt.subplots(1, 2)
        axes[0].plot(joint_vel)
        axes[1].plot(joint_vel_in)
        plt.title('Joint Velocities')
        plt.show()

        fig, axes = plt.subplots(1, 2)
        axes[0].plot(gripper_joint_pos)
        axes[1].plot(gripper_joint_pos_in)
        plt.title('Gripper Joint Positions')
        plt.show()

        fig, axes = plt.subplots(1, 2)
        axes[0].plot(gripper_joint_vel)
        axes[1].plot(gripper_joint_vel_in)
        plt.title('Gripper Joint Velocities')
        plt.show()

        # fig, axes = plt.subplots(1, 2)
        # axes[0].plot(ee_pos)
        # axes[1].plot(ee_pos_in)
        # plt.title('EE Position')
        # plt.show()
        
        fig, axes = plt.subplots(1, 2)
        axes[0].plot(delta_joint_pos)
        axes[1].plot(delta_joint_pos_in)
        plt.title('Delta Joint Positions')
        plt.show()

        
        
        

