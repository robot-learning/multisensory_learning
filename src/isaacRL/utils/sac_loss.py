
import torch
import torch.nn.functional as F

def critic_loss(qvals, n_qval):
    if len(qvals) != 2:
        return F.mse_loss(qvals, n_qval)
    q1, q2 = qvals
    # JQ = 𝔼(st,at)~D[0.5(Q1(st,at) - r(st,at) - γ(𝔼st+1~p[V(st+1)]))^2]
    q1_loss = F.mse_loss(q1, n_qval)
    # JQ = 𝔼(st,at)~D[0.5(Q1(st,at) - r(st,at) - γ(𝔼st+1~p[V(st+1)]))^2]
    q2_loss = F.mse_loss(q2, n_qval)
    return q1_loss + q2_loss


def actor_loss(log_pi, qvals, alpha=0.1):
    if len(qvals) == 2:
        q1, q2 = qvals
        qvals = torch.min(q1, q2)
    # Jπ = 𝔼st∼D,εt∼N[α * logπ(f(εt;st)|st) − Q(st,f(εt;st))]
    return ((alpha * log_pi) - qvals).mean()

def contrastive_loss(targs, preds):
    loss = 0.0
    for mod in targs:
        if mod in preds:
            loss += F.mse_loss(targs[mod], preds[mod])
    return loss
