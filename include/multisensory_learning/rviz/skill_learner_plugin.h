#ifndef SKILL_LEARNER_PLUGIN_H
#define SKILL_LEARNER_PLUGIN_H

#include <QSlider>
#include <QComboBox>

# include <ros/ros.h>
# include <rviz/panel.h>


namespace multisensory_learning
{

  class SkillLearnerPlugin: public rviz::Panel
  {
    // This class uses Qt slots and is a subclass of QObject, so it needs
    // the Q_OBJECT macro.
    Q_OBJECT
  public:
    SkillLearnerPlugin(QWidget* parent = 0);

  public Q_SLOTS:
    void handleUpdate();
    void setDistributions();
    void getPlan();
    void showSamples();
    void runSimulator();
    
  protected:
    ros::NodeHandle nh_;
    ros::ServiceClient set_vis_params_client_;
    ros::ServiceClient set_dist_client_;
    ros::ServiceClient get_plan_client_;
    ros::ServiceClient show_samples_client_;
    ros::ServiceClient run_sim_client_;

    QComboBox* model_combo_;
    QComboBox* data_combo_;
    QSlider* push_slider_;
    QSlider* posx_var_slider_;
    QSlider* posy_var_slider_;
    QSlider* posz_var_slider_;
    
  };

}

#endif // SKILL_LEARNER_PLUGIN_H
