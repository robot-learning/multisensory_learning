
from isaacRL.utils.data_utils import batch_dict
from isaacRL.utils.data_utils import process_data

import random
import torch
from torch.utils.data.sampler import BatchSampler, SequentialSampler, SubsetRandomSampler
import numpy as np

class BufferBasic:
    """docstring for BufferBasic."""

    def __init__(self, obs_size, action_size, device='cpu', storage_device='cpu', max_size=5000, rgb_size=None):
        self.obs_size = obs_size
        self.action_size = action_size
        self.max_size = max_size
        self.device = device
        self.rgb_size = rgb_size
        self._bsize = 0
        self.storage_device = storage_device
        self._batch_idx = None

        self._setup_values()

    def _setup_values(self):
        self._states = None
        self.last_mean = 0.
        self.last_sum = 0.
        self.load_idx = 0
        self.reset()

    def reset(self):
        self.load_idx = 0
        self._maxed_out = False
        self._states = torch.zeros((self.max_size, self.obs_size), device=self.storage_device)
        self._nstates = torch.zeros((self.max_size, self.obs_size), device=self.storage_device)
        self._actions = torch.zeros((self.max_size, self.action_size), device=self.storage_device)
        self._rewards = torch.zeros((self.max_size,1), device=self.storage_device)
        self._terminals = torch.zeros((self.max_size,1), device=self.storage_device, dtype=torch.long)
        if self.rgb_size is not None:
            # TODO rgbs not saved in save and load functions
            # (also probably not used anywhere currently)
            self._rgbs = torch.zeros((self.max_size, self.rgb_size), device=self.storage_device)
            self._nrgbs = torch.zeros((self.max_size, self.rgb_size), device=self.storage_device)

    def __len__(self):
        return self.load_idx if not self._maxed_out else self.max_size

    def __getitem__(self, idx):
        state = self._states[idx]
        action = self._actions[idx]
        nstate = self._nstates[idx]
        reward = self._rewards[idx]
        terminal = self._terminals[idx]
        state = {'obs':state}
        nstate = {'obs':nstate}
        if self.rgb_size is not None:
            state['rgb'] = self._rgbs[idx]
            nstate['rgb'] = self._nrgbs[idx]
        return state, action, nstate, reward, terminal

    def add(self, state, action, nstate, reward, terminal, indices=None):
        if self._states is None:
            self.reset()
        # if indices is None:
        #     indices = torch.arange(state['obs'].shape[0])
        # bsize = indices.shape[0]
        bsize = state['obs'].shape[0]
        assert (
            action.shape[0] == bsize and
            state['obs'].shape[0] == bsize and
            nstate['obs'].shape[0] == bsize and
            terminal.shape[0] == bsize
        )
        self._bsize = bsize
        load_idx = torch.arange(self.load_idx,self.load_idx+bsize) % self.max_size
        self._states[load_idx] = state['obs'].clone().to(self._states.device)
        self._nstates[load_idx] = nstate['obs'].clone().to(self._nstates.device)
        self._actions[load_idx] = action.clone().to(self._actions.device)
        self._rewards[load_idx] = reward.unsqueeze(1).clone().to(self._rewards.device)
        self._terminals[load_idx] = terminal.unsqueeze(1).clone().to(self._terminals.device)
        if self.rgb_size is not None:
            self._rgbs[load_idx] = state['rgb'].clone().to(self._rgbs.device)
            self._nrgbs[load_idx] = nstate['rgb'].clone().to(self._nrgbs.device)
        if not self._maxed_out and (self.load_idx+bsize) >= self.max_size:
            self._maxed_out = True
        self.load_idx = (self.load_idx+bsize) % self.max_size

    def sample(self, bsize=10):
        batch_idx = torch.randint(0, len(self), (bsize,))
        self._batch_idx = batch_idx
        state = self._states[batch_idx].to(self.device)
        action = self._actions[batch_idx].to(self.device)
        nstate = self._nstates[batch_idx].to(self.device)
        reward = self._rewards[batch_idx].to(self.device)
        terminal = self._terminals[batch_idx].to(self.device)
        state = {'obs':state}
        nstate = {'obs':nstate}
        if self.rgb_size is not None:
            state['rgb'] = self._rgbs[batch_idx].to(self.device)
            nstate['rgb'] = self._nrgbs[batch_idx].to(self.device)
        return state, action, nstate, reward, terminal

    def mean_rewards(self):
        return self._rewards[:len(self)].mean().item()

    def save(self, filepath, checkpoint={}):
        checkpoint['states'] = self._states.cpu()
        checkpoint['nstates'] = self._nstates.cpu()
        checkpoint['actions'] = self._actions.cpu()
        checkpoint['rewards'] = self._rewards.cpu()
        checkpoint['terminals'] = self._terminals.cpu()
        checkpoint['load_idx'] = self.load_idx
        checkpoint['maxed_out'] = self._maxed_out
        checkpoint['max_size'] = self.max_size
        checkpoint['batch_size'] = self._bsize
        torch.save(checkpoint, filepath)
        print('saved buffer to', filepath)

    def load(self, filepath, full2obs=None):
        checkpoint = torch.load(filepath)
        ssize = min(checkpoint['states'].shape[0], self.max_size)
        if full2obs is not None:
            _states = full2obs(checkpoint['states'].to(self.device))
            self._states[:ssize] = _states[:ssize]
            _nstates = full2obs(checkpoint['nstates'].to(self.device))
            self._nstates[:ssize] = _nstates[:ssize]
        else:
            self._states = checkpoint['states'].to(self.device)
            self._nstates = checkpoint['nstates'].to(self.device)
        self._actions[:ssize] = checkpoint['actions'].to(self.device)[:ssize]
        self._rewards[:ssize] = checkpoint['rewards'].to(self.device)[:ssize]
        self._terminals[:ssize] = checkpoint['terminals'].to(self.device)[:ssize]
        self.load_idx = checkpoint['load_idx']
        self._maxed_out = checkpoint['maxed_out']
        self._bsize = checkpoint['batch_size']
        max_size = checkpoint['max_size']
        self.max_size = max(self.max_size, max_size)
        if self._maxed_out and self.max_size > max_size:
            self.load_idx = max_size
            self._maxed_out = True
        print('loaded buffer data')
        return checkpoint


class BufferFullstate(BufferBasic):
    """docstring for BufferFullstate."""

    def __init__(self, state_size, action_size, full2obs=None, device='cpu', storage_device='cpu', max_size=5000, rgb_size=None, ee_actions=False):
        super(BufferFullstate, self).__init__(state_size, action_size, device, storage_device, max_size, rgb_size)
        self.full2obs = full2obs
        self._use_ee_actions = ee_actions
        if self.full2obs is None:
            self.full2obs = lambda x : x

    def __getitem__(self, idx):
        state, action, nstate, reward, terminal = super().__getitem__(idx)
        state['obs'] = self.full2obs(state['obs'])
        nstate['obs'] = self.full2obs(nstate['obs'])
        if self._use_ee_actions:
            action = self._states[self._full_idx['ee_state']]
        return state, action, nstate, reward, terminal

    def sample(self, bsize=10):
        state, action, nstate, reward, terminal = super().sample(bsize)
        state['obs'] = self.full2obs(state['obs'])
        nstate['obs'] = self.full2obs(nstate['obs'])
        if self._use_ee_actions:
            action = self._states[self._full_idx['ee_state']]
        return state, action, nstate, reward, terminal

    def add(self, state, action, nstate, reward, terminal, indices=None):
        if not hasattr(self, '_full_idx') or self._full_idx is None:
            self._full_idx = state['state_idx']
        state = {'obs': state['fullstate']}
        nstate = {'obs': nstate['fullstate']}
        super().add(state, action, nstate, reward, terminal, indices)

    def save(self, filepath, checkpoint={}):
        checkpoint['fullstate_idx'] = self._full_idx
        super().save(filepath, checkpoint)

    def load(self, filepath):
        checkpoint = super().load(filepath)
        self._full_idx = checkpoint.get('fullstate_idx', self._full_idx)


class BufferHER(BufferBasic):
    """
    Same as BufferBasic except it requires information about the goals
    """

    def __init__(self, obs_size, action_size, state_size, state_idx, full2obs, goalie, num_envs, max_steps, device='cpu', storage_device='cpu', max_size=5000, max_goals=100, goal_key='goal'):
        self.state_size = state_size
        self.state_idx = state_idx
        self.goalie = goalie
        self.full2obs = full2obs
        self.num_envs = num_envs
        self.num_envs_range = torch.arange(self.num_envs, dtype=torch.long)
        self.max_steps = max_steps+1
        self.max_goals = max_goals
        self._goal_key = goal_key
        self._did_reset_full_idx = False
        super(BufferHER, self).__init__(obs_size, action_size, device, storage_device, max_size)

    def _setup_values(self):
        super()._setup_values()

    def reset(self):
        super().reset()
        self._goals = torch.zeros((self.max_goals, self.goalie.size), device='cpu')
        self._full_states = torch.zeros((self.num_envs, self.max_steps, self.state_size), device='cpu')
        self._full_nstates = torch.zeros((self.num_envs, self.max_steps, self.state_size), device='cpu')
        self._full_actions = torch.zeros((self.num_envs, self.max_steps, self.action_size), device='cpu')
        self.full_idx = torch.zeros((self.num_envs,), dtype=torch.long)
        self.goal_idx = 0
        self._maxed_goals = False

    def add(self, state, action, nstate, reward, terminal):
        super().add(state, action, nstate, reward, terminal)
        ostate = state
        oaction = action
        onstate = nstate
        oreward = reward
        oterminal = terminal

        self._full_states[self.num_envs_range, self.full_idx,:] = state['fullstate'].cpu()
        self._full_nstates[self.num_envs_range, self.full_idx,:] = nstate['fullstate'].cpu()
        self._full_actions[self.num_envs_range, self.full_idx,:] = action.cpu()
        self.full_idx += 1

        # add terminal goals and end states to the list of goals
        if (terminal > 0).any():
            last_goals = terminal > 0
            last_indices = self.num_envs_range[last_goals]

            goals = state['fullstate'][last_goals][:,self.state_idx[self._goal_key]]
            self.add_goals(goals)

            goals = state['fullstate'][last_goals][:,self.state_idx['stategoal']]
            self.add_goals(goals)

            # grab only the states that are terminating
            state = self._full_states[last_goals].to(self.device)
            nstate = self._full_nstates[last_goals].to(self.device)
            action = self._full_actions[last_goals].to(self.device)

            self.add_her_states(state, action, nstate, goals, last_indices)

            # max_idx = self.goal_idx if not self._maxed_goals else self.max_goals
            # goal_idx = torch.randint(0, max_idx, (state.shape[0],))
            # goals = self._goals[goal_idx,:].to(self.device)
            goal_idx = torch.randint(2, self.max_steps, (state.shape[0],))
            goal_idx = torch.minimum(goal_idx, self.full_idx[last_indices])
            goals = state[last_indices,goal_idx][:,self.state_idx['stategoal']]
            self.add_her_states(state, action, nstate, goals, last_indices)

            self.full_idx[last_goals] *= 0
        # # if the list of goals is not empty, add transitions that contain random selections of goals
        # if self.goal_idx > 0:
        #     bsize = state.shape[0]
        #     max_idx = self.goal_idx if not self._maxed_goals else self.max_goals
        #     goal_idx = torch.randint(0, max_idx, (bsize,))
        #     goals = self._goals[goal_idx,:]
        #
        #     state[:,-self.goal_size:] = goals
        #     nstate[:,-self.goal_size:] = goals
        #     reward[:], terminal[:], _ = self.reward(goals)
        #
        #     super().add(state, action, nstate, reward, terminal)

    def add_her_states(self, state, action, nstate, goals, last_indices):
        # update the goals to be the stategoals at the terminal state
        state[:,:,self.state_idx[self._goal_key]] = goals.unsqueeze(1)
        nstate[:,:,self.state_idx[self._goal_key]] = goals.unsqueeze(1)
        # reshape the states so they are just a list of states
        state = state.reshape(-1, state.shape[-1])
        nstate = nstate.reshape(-1, state.shape[-1])
        action = action.reshape(-1, action.shape[-1])
        # remove the excess steps for each state
        keepidx = torch.cat([torch.arange(self.max_steps)[:self.full_idx[li]] + (i*self.max_steps) for i, li in enumerate(last_indices)])
        state = state[keepidx]
        nstate = nstate[keepidx]
        action = action[keepidx]
        # get the goals from each of these states
        goals = state[:,self.state_idx[self._goal_key]]
        # update the rewards with these state/goals
        reward, terminal, _ = self.goalie.reward(state, self.state_idx, goals)
        # convert the states to dictionaries with the observation instead of fullstate
        stated = {'obs': self.full2obs(state)}
        nstated = {'obs': self.full2obs(nstate)}
        if stated['obs'].shape[1] != self.obs_size:
            extra = self.obs_size - stated['obs'].shape[1]
            stated['obs'] = torch.cat([stated['obs'], state[:,-extra:]], dim=-1)
            nstated['obs'] = torch.cat([nstated['obs'], nstate[:,-extra:]], dim=-1)
        state = stated
        nstate = nstated
        bsize = reward.shape[0]
        assert (
            action.shape[0] == bsize and
            state['obs'].shape[0] == bsize and
            nstate['obs'].shape[0] == bsize and
            terminal.shape[0] == bsize
        )
        super().add(state, action, nstate, reward, terminal)

    def add_goals(self, goals):
        bsize = goals.shape[0]
        goal_idx = torch.arange(self.goal_idx,self.goal_idx+bsize) % self.max_goals
        self._goals[goal_idx] = goals[:].cpu()
        if not self._maxed_goals and (self.goal_idx+bsize) >= self.max_goals:
            self._maxed_goals = True
        self.goal_idx = (self.goal_idx+bsize) % self.max_goals

    def reset_full_idx(self):
        self.full_idx *= 0


class BufferLOKI(BufferBasic):
    """docstring for BufferLOKI."""

    def __init__(self, state_size, action_size, device='cpu', storage_device='cpu', max_size=5000, rgb_size=None):
        super(BufferLOKI, self).__init__(
            state_size, action_size, device, storage_device, max_size, rgb_size
        )

    def reset(self):
        super().reset()
        self._expert_actions = torch.zeros(
            (self.max_size, self.action_size), device=self.storage_device
        )

    def __getitem__(self, idx):
        state, action, nstate, reward, terminal = super().__getitem__(idx)
        eaction = self._expert_actions[idx]
        return state, action, eaction, nstate, reward, terminal

    def sample(self, bsize=10):
        state, action, nstate, reward, terminal = super().sample(bsize)
        eaction = self._expert_actions[self._batch_idx].to(self.device)
        return state, action, eaction, nstate, reward, terminal

    def add(self, state, action, eaction, nstate, reward, terminal, indices=None):
        if self._states is None:
            self.reset()
        bsize = state['obs'].shape[0]
        load_idx = torch.arange(self.load_idx,self.load_idx+bsize) % self.max_size
        self._expert_actions[load_idx] = eaction.clone().to(self._expert_actions.device)
        super().add(state, action, nstate, reward, terminal, indices)

    def save(self, filepath, checkpoint={}):
        checkpoint['expert_actions'] = self._expert_actions
        super().save(filepath, checkpoint)

    def load(self, filepath):
        checkpoint = super().load(filepath)
        self._expert_actions = checkpoint.get('expert_actions', self._expert_actions)
