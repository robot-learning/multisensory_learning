
from isaacgym.torch_utils import tf_vector
from isaacRL.utils.utils import get_log_dist
from isaacRL.utils.pose_proposal import get_proposer
from isaacRL.utils.goalies.base_goalie import Goalie

from ll4ma_util import torch_util

import torch
import numpy as np

@torch.jit.script
def move2ShapeReward(
    ee_pose, goal_pose, actions,
    ee_w, rot_w, c_w, alpha,
    action_penalty_scale, threshold,
    arm_joints
):
    # type: (Tensor, Tensor, Tensor, float, float, float, float, float, float, int) -> Tuple[Tensor, Tensor, Tensor]
    bsize = ee_pose.shape[0] # batch size
    terminals = torch.zeros((bsize,), device=ee_pose.device, dtype=torch.long)
    amplifyer = torch.zeros((bsize,), device=ee_pose.device, dtype=torch.long)
    x_axis = torch.tensor([[1.,0.,0.]]*bsize, device=ee_pose.device)
    z_axis = torch.tensor([[0.,0.,1.]]*bsize, device=ee_pose.device)

    ee_diff = ee_pose[:,:3] - goal_pose[:,:3]
    ee_dist = torch.norm(ee_diff, p=2, dim=-1)
    ee_reward = (1.0 / (1.0 + ee_dist ** 2))**2

    axis1 = tf_vector(ee_pose[:,3:7], x_axis)
    axis2 = tf_vector(goal_pose[:,3:7], x_axis)
    axis3 = tf_vector(ee_pose[:,3:7], z_axis)
    axis4 = tf_vector(goal_pose[:,3:7], z_axis)

    dot1 = torch.bmm(axis1.view(bsize, 1, 3), axis2.view(bsize, 3, 1)).squeeze(-1).squeeze(-1)  # alignment of forward axis for gripper
    dot2 = torch.bmm(axis3.view(bsize, 1, 3), axis4.view(bsize, 3, 1)).squeeze(-1).squeeze(-1)  # alignment of up axis for gripper
    # rot_err = (dot1 ** 2 + dot2 ** 2).sqrt()
    rot_err = dot1.abs()
    rot_reward = (1.0 / (1.0 + rot_err))**2

    # TODO: add rotation error correctly
    tot_dist = ee_dist + rot_err #*0.5

    # rewards = ee_w * ee_reward
    action_penalty = torch.sum(actions[:,:arm_joints] ** 2, dim=-1)

    terminals[tot_dist < threshold] = 1
    amplifyer[tot_dist < (threshold*2)] = 1

    rewards = ee_w * ee_reward + rot_w * rot_reward
    rewards[amplifyer == 1] *= 2 # c_w*0.2
    rewards[terminals == 1] *= 2000 # c_w*200
    rewards = rewards - action_penalty_scale * action_penalty

    return rewards, terminals, tot_dist
    # return rewards, terminals, (tot_dist, ee_dist, rot_err)

@torch.jit.script
def move2SparseReward(
    ee_pose, goal_pose, actions, threshold, arm_joints
):
    # type: (Tensor, Tensor, Tensor, float, int) -> Tuple[Tensor, Tensor, Tensor]
    rewards, terminals, dist = move2ShapeReward(
        ee_pose, goal_pose, actions, 0., 0., 0., 0., 0., threshold, arm_joints
    )
    return terminals.to(rewards.dtype), terminals, dist


class Reach2Pose(Goalie):
    """docstring for Reach2Pose."""

    def __init__( self,
            position_range=None, orientation_range=None, joint_range=None,
            device='cpu', threshold=0.01, sparse=False,
            ee_w=1., rot_w=1., c_w=10., alpha=1., act_penal=0.001,
        ):
        goal_shape = 7
        super(Reach2Pose, self).__init__(threshold, goal_shape, device, sparse)
        assert (position_range is not None and orientation_range is not None) or joint_range is not None
        if position_range is not None:
            position_range = torch.tensor(position_range, device=self.device)
            self.position_mins = position_range[:,0]
            self.position_range = position_range[:,1] - self.position_mins
        if orientation_range is not None:
            orientation_range = torch.tensor(orientation_range, device=self.device)
            self.orientation_mins = orientation_range[:,0]
            self.orientation_range = orientation_range[:,1] - self.orientation_mins
        self.use_joints = joint_range is not None
        if self.use_joints:
            joint_range = torch.tensor(joint_range, device=self.device)
            self.joint_mins = joint_range[:,0]
            self.joint_range = joint_range[:,1] - self.joint_mins

        self.ee_w = ee_w
        self.rot_w = rot_w
        self.alpha = alpha
        self.c_w = c_w
        self.act_penal = act_penal

    def goal_dist(self, state, goal):
        if self.use_joints:
            return state['joints'] - goal
        return state['ee_state'][:,:self.goal_shape] - goal

    def get_current(self, state):
        return None

    def get_goal(self, state):
        ee_state = state['ee_state']
        if self.use_joints:
            goals = torch.rand(
                (ee_state.shape[0], self.joint_range.shape[0]),
                dtype=ee_state.dtype, device=ee_state.device
            )
            goals = goals * self.joint_range + self.joint_mins
        else:
            goals = torch.rand_like(ee_state)
            goals[:,:3] = goals[:,:3] * self.position_range + self.position_mins
            # goals[3:6] = goals[3:6] * self.orientation_range + self.orientation_mins
            # goals[3:] = euler2quat(goals[3:6])
            goals[:,3:] = torch_util.random_quat(goals.shape[0], goals.device)
        return goals

    def reward(self, state, goal):
        ee_pose = state['ee_state'][:,:self.goal_shape]
        arm_joints = state['arm_joints']
        if self.sparse:
            return move2SparseReward(
                ee_pose, goal, state['prev_action'], self.threshold, arm_joints
            )
        return move2ShapeReward(
            ee_pose, goal, state['prev_action'],
            self.ee_w, self.rot_w, self.c_w, self.alpha,
            self.act_penal, self.threshold, arm_joints
        )

class Reach2Pose_Factory(object):
    """docstring for Reach2Pose_Factory."""

    def build(self,config):
        return Reach2Pose(
            config.get('prange', None),
            config.get('orange', None),
            config.get('jrange', None),
            device=config.get('device', 'cpu'),
            threshold=config.get('threshold', 0.01),
            sparse=True,
        )

class Reach2PoseShaped_Factory(object):
    """docstring for Reach2PoseShaped_Factory."""

    def build(self,config):
        return Reach2Pose(
            config.get('prange', None),
            config.get('orange', None),
            config.get('jrange', None),
            device=config.get('device', 'cpu'),
            threshold=config.get('threshold', 0.01),
            sparse=False,
            ee_w=config.get('ee_w', 1.),
            rot_w=config.get('rot_w', 1.),
            c_w=config.get('c_w', 10.),
            alpha=config.get('alpha', 1.),
            act_penal=config.get('act_penal', 0.01),
        )


if __name__ == '__main__':
    device = 'cuda:0'
    goalie = Reach2PoseShaped_Factory().build({
      'prange': [[0.1, 0.2], [0.2, 0.3], [0.3, 0.4]],
      'orange': [[0.1, 0.2], [0.2, 0.3], [0.3, 0.4]],
      'device': device,
    })
    state = {
      'ee_state': torch.tensor([
        [0.25, 0.1, 0.7, 0.0, 1.0, 0.0, 0.0],
        [0.35, 0.1, 0.6, 0.0, 0.0, 1.0, 0.0]
      ], device=device),
      'prev_action': torch.rand((2,7),device=device)*0.1
    }
    print('state:', state['ee_state'])
    state['goals'] = goalie.get_goal(state)
    print('goals:', state['goals'])
    rewards, terminals, ee_dist = goalie.reward(state, state['goals'])
    print('rewards:', rewards)
    print('terminals:', terminals)
    print('ee_dist:', ee_dist)
