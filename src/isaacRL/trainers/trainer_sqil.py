
from isaacRL.trainers.trainer_sacbase import TrainerSACBase
from isaacRL.utils.buffer import BufferBasic, BufferHER, BufferFullstate

import torch
import torch.nn.functional as F

import time
from tqdm import tqdm
import os.path as osp


class TrainerSQIL(TrainerSACBase):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        super(TrainerSQIL, self).__init__(config)

        self.pretrain_true_rewards = config.get('pretrain_true_rewards', False)
        self._is_pretrain = False

        self.expert_buffer = self.buffer

        buf_size = self.config['training'].get('max_buff_size', self.max_steps*self.num_envs)
        if self.config['training'].get('her', False):
            max_goals = self.config['training'].get('max_goals', (2*self.num_envs))
            max_goals = max(max_goals, (2*self.num_envs))
            self.buffer = BufferHER(
                self.observation_size,
                self.action_size,
                self.envir.full_state_size,
                self.envir.state_idx,
                self.envir.full2obs,
                self.envir.goalie,
                self.num_envs,
                self.max_steps,
                self.device,
                buf_size,
                max_goals,
                [key for key in self.envir.obs_keys if 'goal' in key][0]
            )
        else:
            self.buffer = BufferBasic(
                self.observation_size,
                self.action_size,
                device=self.device,
                storage_device=self.device,
                max_size=buf_size,
            )

    def update_weights(self, update_i):
        # batch_time = time.time()
        if self._is_pretrain:
            state, action, nstate, rewards, terminals = self.buffer.sample(self.batch_size)
        else:
            batch_size = self.batch_size//2
            state, action, nstate, rewards, terminals = self.buffer.sample(batch_size)
            estate, eaction, enstate, erewards, eterminals = self.expert_buffer.sample(batch_size)
            # Do the expert actions look similar to the policy actions?
            # print(torch.mean(action, dim=0))
            # print(torch.std(action, dim=0))
            # import pdb; pdb.set_trace()
            state = {'obs': torch.cat((state['obs'], estate['obs']))}
            nstate = {'obs': torch.cat((nstate['obs'], enstate['obs']))}
            action = torch.cat((action, eaction))
            terminals = torch.cat((terminals, eterminals))
            rewards[:] = torch.ones_like(rewards)
            erewards[:] = torch.zeros_like(erewards)
            rewards = torch.cat((rewards, erewards))
        # print('batch time:', time.time() - batch_time)
        return self._update_weights(update_i, state, action, nstate, rewards, terminals)
