#!/usr/bin/env python
import os
import sys
import numpy as np
import argparse
import matplotlib.pyplot as plt
from shapely.geometry import LineString, Point
from time import time
from tqdm import tqdm

from ll4ma_util import file_util, ui_util

from multisensory_learning.planning.planners import util as plan_util
from multisensory_learning.models import MDN
from multisensory_learning.models.loss import mdn_loss

import torch
from torch.utils.data import Dataset, DataLoader
from torch.distributions import MultivariateNormal, kl_divergence
from torch.optim import Adam


class PushDataset(Dataset):

    def __init__(self, samples):
        self.samples = samples

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        return self.samples[idx]
    

def get_rand_points(n_points, env):
    b = env['buffer']
    min_x = min([min([p[0] for p in wall]) for wall in env['walls']]) + b
    max_x = max([max([p[0] for p in wall]) for wall in env['walls']]) - b
    min_y = min([min([p[1] for p in wall]) for wall in env['walls']]) + b
    max_y = max([max([p[1] for p in wall]) for wall in env['walls']]) - b

    points = []
    while len(points) < n_points:
        x = np.random.rand() * (max_x - min_x) + min_x
        y = np.random.rand() * (max_y - min_y) + min_y
        in_collision = False
        for wall in env['walls']:
            wall_min_x = min([p[0] for p in wall])
            wall_max_x = max([p[0] for p in wall])
            wall_min_y = min([p[1] for p in wall])
            wall_max_y = max([p[1] for p in wall])
            if (x > wall_min_x - b and x < wall_max_x + b and
                y > wall_min_y - b and y < wall_max_y + b):
                in_collision = True
                break
        if not in_collision:
            points.append(np.array([x, y]))

    return np.array(points)


def limit_at_wall(starts, ends, wall):
    (x1, y1), (x2, y2) = wall
    x3 = starts[:,0]
    y3 = starts[:,1]
    x4 = ends[:,0]
    y4 = ends[:,1]
    D = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
    t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / D
    u = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / D

    t_mask = torch.logical_and(t.ge(0.0), t.le(1.0))
    u_mask = torch.logical_and(u.ge(0.0), u.le(1.0))
    mask = torch.logical_and(t_mask, u_mask)

    pts = ends.clone()
    pts[mask,0] = x1 + t[mask] * (x2 - x1)
    pts[mask,1] = y1 + t[mask] * (y2 - y1)

    return pts
    


def get_pushed_distribution(start_mean, start_std, direct, distance, env, max_std=0., n_samples=100):
    # Assume deterministic pushing of mean
    end_mean = start_mean + distance * direct
    # Scale std by push distance so it disperses more the farther you push
    end_std = start_std + distance * max_std

    starts = torch.tensor(start_mean).repeat(n_samples, 1)
    ends = torch.tensor(end_mean) + end_std * torch.randn((n_samples, 2))
    for wall in env['walls']:
        ends = limit_at_wall(starts, ends, wall)

    return ends
    
    # pts = []
    # for _ in range(n_samples):
    #     pt = end_mean + end_std * np.random.normal(size=2)
    #     # Limit motion so it can't pass through walls
    #     for wall in env['walls']:
    #         line1 = LineString([start_mean, pt + env['buffer']])
    #         line2 = LineString(wall)
    #         intersect = line1.intersection(line2)    
    #         if isinstance(intersect, Point):
    #             pt = np.array([intersect.x, intersect.y]) - direct * env['buffer']
    #     pts.append(pt)
    # return pts

    # end_mean = torch.tensor(np.mean(pts, axis=0))
    # end_cov = torch.tensor(np.cov(np.array(pts).T)) # I don't know how to do cov in torch
    # # Make sure it's not singular by adding a small diagonal
    # end_cov = torch.add(end_cov, 1e-8 * torch.eye(2))

    # return end_mean, end_cov


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--n_starts', type=int, default=1000)
    parser.add_argument('--n_push_per_start', type=int, default=100)
    parser.add_argument('--n_goal_samples', type=int, default=20)
    parser.add_argument('--min_push_dist', type=float, default=0.05)
    parser.add_argument('--max_push_dist', type=float, default=0.7)
    parser.add_argument('--min_std', type=float, default=0.001)
    parser.add_argument('--max_std', type=float, default=0.02)
    parser.add_argument('--batch_size', type=int, default=512)
    parser.add_argument('--learning_rate', type=float, default=1e-3)
    parser.add_argument('--n_epochs', type=int, default=300)
    parser.add_argument('--checkpoint_dir', type=str)
    parser.add_argument('--checkpoint_interval', type=int, default=10)
    parser.add_argument('--debug_push', action='store_true')
    parser.add_argument('--debug_push_start', type=float, nargs='+', default=[0.5, 0.2])
    parser.add_argument('--debug_push_end', type=float, nargs='+', default=[0.8, 0.6])
    parser.add_argument('--debug_push_dist', type=float, default=0.5)
    parser.add_argument('--debug_push_std', type=float, default=0.01)
    parser.add_argument('--debug_samples', action='store_true')
    args = parser.parse_args()

    if not args.debug_push and not args.debug_samples and not args.checkpoint_dir:
        ui_util.print_error_exit("\n  Must specify checkpoint_dir\n")

    if args.checkpoint_dir:
        file_util.safe_create_dir(args.checkpoint_dir)
    
    env = {
        'walls': [
            [(0, 0.5), (0.5, 0.5)],
            [(0, 0), (0, 1)],
            [(0, 1), (1, 1)],
            [(1, 1), (1, 0)],
            [(1, 0), (0, 0)]
        ],
        'buffer': 0.2,
        'min_distance': args.min_push_dist,
        'max_distance': args.max_push_dist
    }

    if args.debug_push:    
        boundary_args = {'color': 'black', 'lw': 5}
         
        fig, ax = plt.subplots(1, 1)
        fig.set_size_inches(10, 10)

        for p1, p2 in env['walls']:
            # Plot seems to assume different order than is intuitive to me
            ax.plot([p1[0], p2[0]], [p1[1], p2[1]], **boundary_args)

        start_mean = np.array(args.debug_push_start)
        start_std = args.debug_push_std
        end = np.array(args.debug_push_end)
        diff = end - start_mean
        direct = diff / np.linalg.norm(diff)
        
        ax.arrow(*start_mean, end[0] - start_mean[0], end[1] - start_mean[1], width=0.009, fc='yellow')

        pts = get_pushed_distribution(start_mean, start_std, direct, args.debug_push_dist,
                                      env, args.max_std, n_samples=100)
        for i in range(len(pts)):
            ax.scatter(*pts[i], color='r', alpha=0.5)        

        start_mean = torch.tensor(start_mean)
        start_cov = torch.tensor([[start_std**2, 0], [0, start_std**2]]).double()
        start_mvn = MultivariateNormal(start_mean, start_cov)
        start_samples = start_mvn.sample((100,))
        for i in range(len(start_samples)):
            ax.scatter(*start_samples[i].numpy(), color='g', alpha=0.5)
            
        plt.tight_layout()
        plt.show()
        sys.exit()


    min_d = env['min_distance']
    max_d = env['max_distance']

    start_means = get_rand_points(args.n_starts, env)
    start_means = np.repeat(start_means, args.n_push_per_start, axis=0)
    ends = get_rand_points(len(start_means), env)
    diffs = ends - start_means
    directs = diffs / np.expand_dims(np.linalg.norm(diffs, axis=-1), -1)
    dists = np.expand_dims(np.random.rand(len(start_means)) * (max_d - min_d) + min_d, -1)

    start = time()
    print("Generating samples...")
    samples = []
    for i in tqdm(range(len(start_means))):
        start_mean = start_means[i]
        start_std = np.random.uniform() * (args.max_std - args.min_std) + args.min_std
        start_cov = np.array([start_std**2, start_std**2])

        direct = directs[i]
        dist = dists[i]

        pts = get_pushed_distribution(start_mean, start_std, direct, dist.item(), env,
                                      args.max_std, n_samples=args.n_goal_samples)
        for pt in pts:
            samples.append((start_mean, start_cov, pt, direct, dist))

        # print("START MEAN", start_mean)
        # print("START COV", start_cov)
        # print("END MEAN", end_mean)
        # print("END COV", end_cov)
        # print("DIRECT", direct)
        # print("DIST", dist)
        # sys.exit()
        
    print(f"Samples generated in {time() - start:.2f} seconds")

    if args.debug_samples:
        print("Plotting samples...")
        boundary_args = {'color': 'b', 'lw': 5}
         
        fig, ax = plt.subplots(1, 1)
        fig.set_size_inches(10, 10)

        for p1, p2 in env['walls']:
            # Plot seems to assume different order than is intuitive to me
            ax.plot([p1[0], p2[0]], [p1[1], p2[1]], **boundary_args)

        for start_mean, start_cov, end, direct, dist in samples:
            ax.scatter(*start_mean, color='g', alpha=0.5)
            ax.scatter(*end, color='r', alpha=0.5)
            ax.plot([start_mean[0], end[0]], [start_mean[1], end[1]], color='y')

        plt.tight_layout()
        plt.show()
        sys.exit()

    dataset = PushDataset(samples)
    loader = DataLoader(dataset, args.batch_size, shuffle=True, num_workers=0)
    
    # In dim=7 is 2 start mean, 2 start cov, 2 push direct, 1 push dist
    mdn = MDN(7, 2, [128, 128], 1, 'relu', 'layer').double()
    mdn.train()
    optim = Adam(mdn.parameters(), lr=args.learning_rate)
    
    for epoch in range(1, args.n_epochs + 1):
        losses = []
        pbar = tqdm(total=len(dataset), file=sys.stdout)
        for start_mean, start_cov, end, direct, dist in loader:            
            mdn_in = torch.cat([start_mean, start_cov, direct, dist], dim=-1)
            mdn_out = mdn(mdn_in)

            # TODO I realize now, at this point you're not even really training an MDN according
            # to the MDN loss because the training data includes target distributions which are
            # the outputs of the skills. Seems you could either make the dataset include the
            # input dists and the target points and use the MDN loss, or fit the dists from
            # the training data (as I'm currently doing) and include dists as targets for learning
            # which you can then use a KL divergence loss for. In short, you need to use KL loss
            # for the current setup since you already accounted for the distributions in the
            # training data. This I think is just in virtue of being able to generate target dists
            # analytically for synthetic data. On the real robot you'll probably only have point
            # estimates of where it ends up. But that being said, one way to get a distribution of
            # the skills is to keep spawning the object in the same spot and repeatedly apply the
            # skill to see the resulting distribution, so it seems you could either estimate a
            # distribution from the data and learn a generative model of params with KL div or
            # just do the MDN loss using the point samples.
            
            loss = mdn_loss(end, *mdn_out)

            
            # mdn_mvn = MultivariateNormal(mu[:,:,0], scale_tril=sigma_tril[:,:,:,0])
            # target_mvn = MultivariateNormal(end_mean, torch.diag_embed(end_cov, dim1=-2, dim2=-1))
            # loss = kl_divergence(target_mvn, mdn_mvn).mean() # TODO check projection
            
            optim.zero_grad()
            loss.backward()
            optim.step()
            losses.append(loss.item())

            pbar.set_description(f"  E{epoch}-loss={np.mean(losses):.4f}")
            pbar.update(args.batch_size)
        pbar.close()

        if epoch % args.checkpoint_interval == 0:
            checkpoint = {'mdn': mdn.state_dict(), 'env': env}
            checkpoint_fn = os.path.join(args.checkpoint_dir, f"mdn_{epoch:04d}.pt")
            torch.save(checkpoint, checkpoint_fn)
            

    
