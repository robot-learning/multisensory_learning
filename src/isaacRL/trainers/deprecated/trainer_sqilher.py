
from isaacRL.tasks.her_task import HERTask
from isaacRL.tasks.expert_behaviors import Expert
from isaacRL.models.actorcritics import SoftActorCritic
from isaacRL.trainers.trainer_her import TrainerHER
from isaacRL.utils.buffer import BufferHER

import os, sys, argparse, json, random
import os.path as osp
import numpy as np
from tqdm import tqdm
import rospy

import torch
import torch.nn as nn
import torch.nn.functional as F


class TrainerSQILHER(TrainerHER):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        super(TrainerSQILHER, self).__init__(config)
        self.expert = Expert(self.envir)

    def play(self, playi):
        with torch.no_grad():
            self.model.eval()
            state = self.envir.reset()
            trans = []
            tot_rewards = 0.
            tot_terms = 0.000001
            pbar = tqdm(total=self.max_steps, file=sys.stdout)
            desc = f'  Play Epoch {playi}'
            pbar.set_description(desc)
            for step in range(self.max_steps):
                if rospy.is_shutdown():
                    return
                if random.uniform(0,1) < self.randact_prob:
                    actions = self.model.policy.random_action(
                      self.n_envs, self.device )
                else:
                    actions, _ = self.model.policy(
                      self.buffer.prep_state(state, self.device)
                    )
                actions = actions.detach()
                nstate, rewards, terminals = self.envir.step_all(actions)
                # rewards = torch.zeros_like(rewards)
                # terminals = torch.zeros_like(terminals)
                rewards *= 0
                terminals *= 0
                tot_rewards += rewards.sum().item()
                tot_terms += terminals.sum().item()
                self.buffer.add_transitions(
                  state, actions, nstate, rewards, terminals, state['goal']
                )
                if step == 0:
                    self.buffer.add_goals(state['goal'])
                trans.append( (state, actions, nstate) )
                if terminals.sum() > 0:
                    state = self.envir.reset_indices(terminals)
                else:
                    state = nstate
                desc = f'  Play R: {(tot_rewards):.1f} {(tot_terms):.0f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            # rewards = torch.zeros_like(rewards)
            # terminals = torch.zeros_like(terminals)
            rewards *= 0
            terminals *= 0
            fin_goals = self.envir.goalie.extract_current(state)
            self.buffer.add_goals(fin_goals)
            for state, actions, nstate in trans:
                self.buffer.add_transitions(
                  state, actions, nstate, rewards, terminals, fin_goals
                )
                # save the transitions with other goals
                for _ in range(self.goal_samples):
                    # sample other goals
                    goals = self.buffer.sample_goals(actions.shape[0])
                    # get the rewards for these other goals
                    self.buffer.add_transitions(
                      state, actions, nstate, rewards, terminals, goals
                    )

    def expert_demos(self, load_experts=True):
        trans = []
        tot_rewards = 0.
        tot_terms = 0.000001
        state = self.envir.reset()
        self.expert.reset()
        pbar = tqdm(total=self.max_steps, file=sys.stdout)
        desc = f'  Expert'
        pbar.set_description(desc)
        for step in range(self.max_steps):
            if rospy.is_shutdown():
                return

            base_acts, exp_resets = self.expert.get_actions()
            actions = torch.zeros((len(base_acts), self.action_space))
            for ai, action in enumerate(base_acts):
                if action is not None:
                    actions[ai] = action.get_state_tensor()
            actions[:, -1] = actions[:,-1] * 2 - 1
            nstate, rewards, terminals = self.envir.step_all(base_acts)
            rewards = torch.ones_like(rewards)
            # do or operation on terminals with exp_resets
            terminals[exp_resets] = 1
            # TODO: make whether to ignore terminal condition from
            # goal/rewards optional
            terminals[[not exp for exp in exp_resets]] = 0
            tot_rewards += rewards.sum().item()
            tot_terms += terminals.sum().item()
            self.buffer.add_transitions(
              state, actions, nstate, rewards, terminals, state['goal']
            )
            if step == 0:
                self.buffer.add_goals(state['goal'])
            trans.append( (state, actions, nstate) )
            state = nstate
            if all(exp_resets):
                break
            desc = f'  Expert R: {(tot_rewards):.1f} {(tot_terms):.0f}'
            pbar.set_description(desc)
            pbar.update(1)
        pbar.close()
        fin_goals = self.envir.goalie.extract_current(state)
        self.buffer.add_goals(fin_goals)
        for ti, (state, actions, nstate) in enumerate(trans):
            rewards, terminals = self.envir.get_rewards(state, fin_goals)
            rewards = torch.ones_like(rewards)
            # terminals = torch.zeros_like(terminals)
            # if ti == (len(trans)-1):
            #     terminals = torch.ones_like(terminals)
            self.buffer.add_transitions(
              state, actions, nstate, rewards, terminals, fin_goals
            )
            # save the transitions with other goals
            for _ in range(self.goal_samples):
                # sample other goals
                goals = self.buffer.sample_goals(actions.shape[0])
                # get the rewards for these other goals
                rewards, terminals = self.envir.get_rewards(state, goals)
                rewards = torch.ones_like(rewards)
                self.buffer.add_transitions(
                  state, actions, nstate, rewards, terminals, goals
                )
