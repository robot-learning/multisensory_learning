
from tasks.expert_task import ExpertTask
from isaacgymenvs.utils.utils import set_seed

import torch
from ll4ma_util import file_util
import os, sys, argparse, json, yaml
import numpy as np
from tqdm import tqdm

def setup():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str,
        default="configs/a2c/m2o_optim01.yaml", help="path to config file")
    parser.add_argument("-v", "--verbose", default=False,
        help="if print all info")
    parser.add_argument("--checkpt", type=str, default=None,
        help="if continuing training from checkpoint model")
    opt = parser.parse_args()
    # print(opt)

    config = file_util.load_yaml(opt.config)

    return opt, config

def main():
    opt, config = setup()
    device = "cpu"
    device = "cuda" if torch.cuda.is_available() else "cpu"

    config['environ']['device'] = device # -1 if random
    config['environ']['seed'] = 42 # -1 if random
    config['environ']['seed'] = set_seed(
      config['environ']['seed'], torch_deterministic=False
    )
    config['environ']['act_range'] = config['model']['actor']['act_range']
    # state_feats = 0
    # for mod in config['model']['modalities']:
    #     if mod == 'rgb':
    #         state_feats += config['model']['modalities'][mod]['outfeats']
    #     else:
    #         state_feats += config['model']['modalities'][mod]
    # config['environ']['num_obs'] = state_feats
    # config['environ']['act_range'] = config['model']['actor']['act_range']

    max_steps = config['environ']['max_env_steps']

    if 'expert' not in config['environ']:
        config['environ']['expert'] = {
          'type': 'Optimizer',
          'max_steps': max_steps,
          'step_size': (0.009, 0.0005),
          'end_threshold': 0.07
        }

    envir = ExpertTask(config['environ'])

    print('resetting behaviors')
    tot_rewards = 0.
    tot_terms = 0.000001
    state = envir.reset()
    max_steps += 1
    pbar = tqdm(total=max_steps, file=sys.stdout)
    desc = f'  Expert'
    pbar.set_description(desc)
    print('envir:', envir.get_robot_ee())
    for step in range(max_steps):

        nstate, rewards, done, _ = envir.step()
        if done.any():
            print('rewards:', rewards)
            print('envir:', envir.get_robot_ee())
            import pdb; pdb.set_trace()
        desc = f'  Expert R: {(rewards.mean().item()):.1f}'
        pbar.set_description(desc)
        pbar.update(1)
    pbar.close()
    print('lowest loss:', envir.expert.lowest_loss)
    print('lowest state:', envir.expert.lowest_state)
    print('envir:', envir.get_robot_ee())
    print('rewards:', rewards)


if __name__ == '__main__':
    main()
