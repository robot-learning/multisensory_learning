

from isaacRL.tasks.pickplace_bandit_task import PickplaceBanditTask
from isaacRL.tasks import get_task
from isaacgymenvs.utils.utils import set_seed

from ll4ma_util import file_util

from isaacRL.testers.utils import *

import os, sys, argparse, json, yaml
import rospy
import torch
import numpy as np
from tqdm import tqdm

def main():
    torch.set_printoptions(precision=3,sci_mode=False)

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str,
        default="configs/place_noise_presets/joint_02n_rpl01.yaml", help="path to config file")
    parser.add_argument("--eval", action='store_true',
        help="if using ros")
    opt = parser.parse_args()

    # device = "cpu"

    # config = file_util.load_yaml('configs/picknplace/joint_rpl01.yaml')
    config = file_util.load_yaml(opt.config)

    econfig = config['environ']
    # config = file_util.load_yaml('configs/tests/stack_traj.yaml')
    econfig['num_envs'] = 100
    econfig['headless'] = True
    # econfig['device'] = device # -1 if random
    # econfig['use_gpu'] = device == 'cuda'
    econfig['seed'] = -1 # if random
    econfig['seed'] = set_seed(
        econfig['seed'], torch_deterministic=False
    )
    econfig['act_range'] = config['model']['actor']['act_range']

    # preset_path = 'checkpoints/preset_2block_01n_eval.torch'
    preset_path = econfig['randomize']#.replace('train', 'eval')
    if opt.eval:
        preset_path = preset_path.replace('train', 'eval')

    econfig['randomize'] = 'inf' # preset_path

    preset_state_num = 5000
    trajs_per_state = 5
    cur_fill = 0
    actions_fill = torch.zeros((preset_state_num,), dtype=torch.long)

    envir = get_task(econfig)

    preset = None
    if os.path.exists(preset_path) and True:
        preset = torch.load(preset_path)
        cur_fill = preset['cur_fill']
        actions_fill = preset['actions_fill']

    if cur_fill >= preset_state_num:
        print('filled states')

    if all(actions_fill >= trajs_per_state):
        print('finished filling actions')
        return

    if cur_fill < preset_state_num:
        # get state so I can reuse it to initialize
        preset_states, setup_actions, actions, noise = envir.get_preset_states()

        new_fill = min(preset_state_num-cur_fill, preset_states.shape[0])

        preset_inits = preset['states'] if preset is not None \
            else torch.zeros((preset_state_num, *preset_states.shape[1:]))
        preset_inits[cur_fill:cur_fill+new_fill] = preset_states[:new_fill]

        setup_actions = setup_actions.transpose(0,1)
        preset_setups = preset['setups'] if preset is not None \
            else torch.zeros((preset_state_num, *setup_actions.shape[1:]))
        preset_setups[cur_fill:cur_fill+new_fill] = setup_actions[:new_fill]

        preset_action = preset['actions'] if preset is not None \
            else torch.zeros((preset_state_num, trajs_per_state, *actions.shape[1:]))
        preset_action[cur_fill:cur_fill+new_fill][:,0] = actions[:new_fill]

        preset_noise = preset['noise'] if preset is not None \
            else torch.zeros((preset_state_num, trajs_per_state, *noise.shape[1:]))
        preset_noise[cur_fill:cur_fill+new_fill][:,0] = noise[:new_fill]

        actions_fill[cur_fill:cur_fill+new_fill] += 1

        preset = {
            'states': preset_inits,
            'noise': preset_noise,
            'setups': preset_setups,
            'actions': preset_action,
            'cur_fill': cur_fill+new_fill,
            'actions_fill': actions_fill
        }
        torch.save(preset, preset_path)

    for _ in range(4):
        # reset the environment several times and save the trajectories every time
        envir.randomize = 'preset'
        envir.full_presets = preset
        envir.preset_states = preset['states']
        envir.num_presets = preset['cur_fill']
        leftover = (actions_fill<trajs_per_state).sum()
        envir.preset_idx = torch.arange(envir.num_envs)
        envir.preset_idx[:leftover] = (
            torch.arange(preset_state_num)[actions_fill<trajs_per_state][:envir.num_envs]
        )
        envir.reset()
        trajs = envir.expert.actions.clone()
        preset_idx = envir.preset_idx
        actions_fill[preset_idx] = torch.minimum(
            actions_fill[preset_idx], torch.tensor([trajs_per_state-1]*len(preset_idx))
        )
        action_idx = actions_fill[preset_idx]
        preset['actions'][preset_idx,action_idx] = trajs.to(preset['actions'].device)[:]
        preset['noise'][preset_idx,action_idx] = envir.object_noises.to(preset['noise'].device)[:]
        actions_fill[preset_idx] += 1
        preset['actions_fill'] = actions_fill
        torch.save(preset, preset_path)

        print(preset['states'].shape)
        print(preset['setups'].shape)
        print(preset['actions'].shape)
        print(preset['cur_fill'])
        print(preset['actions_fill'][torch.arange(0,preset_state_num,econfig['num_envs'])])

    print('saved to', preset_path)


if __name__ == '__main__':
    main()
