
from isaacgym import gymtorch, gymapi  # noqa: F401

from isaacRL.tasks.ll4ma_rl_tasks import Ll4maRLTask
from isaacRL.utils.utils import get_pinv
from ll4ma_isaacgym.behaviors.planners import jacobian_controller

import os, time
import numpy as np
import torch


class ExpertBanditTask(Ll4maRLTask):
    """docstring for ExpertBanditTask."""

    class ActionTypes:
        ACTION_TYPES = ['target_pose', 'ee_offset', 'joint_position', 'joint_velocity', 'ee_pose']
        TARGET_POSE = ACTION_TYPES[0]  # plans to a target pose
        EE_OFFSET = ACTION_TYPES[1]  # a controller moves according to the positional and rotational error
        JOINT_POSITION = ACTION_TYPES[2]  # a controller moves to the joint positions
        JOINT_VELOCITY = ACTION_TYPES[3]  # a controller achieves the joint velocities
        EE_POSE = ACTION_TYPES[4]  # a controller moves to the pose


    class ActionPlanners:
        PLANNERS = ['trajopt', 'jacobian']
        TRAJOPT = PLANNERS[0]
        JACOBIAN = PLANNERS[1]

    def __init__(self, config, expert, max_steps=None):

        self._dof = 7
        self._max_bandit_steps = config['max_env_steps'] if max_steps is None else max_steps
        config['allow_resets'] = False

        self.steps_per_action = config.get('steps_per_action', 1)

        self._action_type = config['task'].get('action_type', 'target_pose')
        assert self._action_type in self.ActionTypes.ACTION_TYPES

        self._action_modif = config['task'].get('action_modifier', '')
        self._planner = config['task'].get('planner', 'trajopt')
        assert self._planner in self.ActionPlanners.PLANNERS

        # TODO: implement modification to allow the actor to decide when to drop the object and end the run
        self._actor_grasp_control = False

        self.expert = expert
        config['max_env_steps'] = self.expert.setup_steps + (self._max_bandit_steps * self.steps_per_action) + self.expert.setdown_steps + 2

        if self._action_type in [self.ActionTypes.JOINT_POSITION, self.ActionTypes.JOINT_VELOCITY]:
            if 'rpl' in self._action_modif:
                config['additive_actions'] = False
            config['jacobian_control'] = False

            if self._action_type == self.ActionTypes.JOINT_VELOCITY:
                config['vecolity_control'] = True
                print('doing correct?')
                import pdb; pdb.set_trace()  # noqa: E702
        elif self._action_type == self.ActionTypes.EE_OFFSET:
            if 'rpl' in self._action_modif:
                config['additive_actions'] = False
                config['jacobian_control'] = False
            else:
                config['additive_actions'] = True
                config['jacobian_control'] = True
        elif self._action_type == self.ActionTypes.EE_POSE:
            config['additive_actions'] = False
            config['jacobian_control'] = True
        elif self._action_type == self.ActionTypes.TARGET_POSE:
            config['additive_actions'] = False
            config['jacobian_control'] = False
            # set the max_env_steps to be a little over the number of trajectory steps
            config['max_env_steps'] = self.expert.setup_steps + self.expert.action_steps + self.expert.setdown_steps + 2
        else:
            raise NotImplementedError("Action type {} has not been implemented fully".format(self._action_type))

        super(ExpertBanditTask, self).__init__(config)

        self.expert.grasp_value = self.robot.end_effector.config.discretes.close
        self.expert.ungrasp_value = self.robot.end_effector.config.discretes.open
        self.expert.grasp_neutral = self.robot.end_effector.config.discretes.neutral

        self._preset_setup = None
        self._preset_actions = None
        self._preset_noise = None
        if self.full_preset_states is not None:
            self._preset_setup = self.full_preset_states['setups'].to(self.rl_device)
            if self._planner == 'trajopt':
                self._preset_actions = self.full_preset_states['actions'].to(self.rl_device)
            if 'noise' in self.full_preset_states:
                self._preset_noise = self.full_preset_states['noise'].to(self.rl_device)

        self.resets = torch.zeros_like(self.reset_buf)
        self.steps_i = torch.zeros_like(self.progress_buf)
        self._success = 0
        self._runs = 0
        self._tot_resets = 0
        self._trajopt_time = 0
        self._setdown_time = 0

        if self._action_modif == 'rpla':
            self.fullstate.add_key('expert_actions', self.action_size)

        # self._use_setdown_act = self.action_size > self.num_joints
        self.setdown_threshold = 0.75

    def action_type():
        doc = "The action_type property."
        def fget(self):
            return self._action_type
        def fset(self, value):
            self._action_type = value
            if self._action_type in [self.ActionTypes.JOINT_POSITION, self.ActionTypes.JOINT_VELOCITY]:
                if 'rpl' in self._action_modif:
                    self.additive = False
                self.jacobian_control = False
                if self._action_type == self.ActionTypes.JOINT_VELOCITY:
                    self.vecolity_control = True
                    print('doing correct?')
                    import pdb; pdb.set_trace()  # noqa: E702
            elif self._action_type == self.ActionTypes.EE_OFFSET:
                if 'rpl' in self._action_modif:
                    self.additive = False
                    self.jacobian_control = False
                else:
                    self.additive = True
                    self.jacobian_control = True
            elif self._action_type == self.ActionTypes.EE_POSE:
                self.additive = False
                self.jacobian_control = True
            elif self._action_type == self.ActionTypes.TARGET_POSE:
                self.additive = False
                self.jacobian_control = False
                # set the max_env_steps to be a little over the number of trajectory steps
                config['max_env_steps'] = self.expert.setup_steps + self.expert.action_steps + self.expert.setdown_steps + 2
            else:
                raise NotImplementedError("Action type {} has not been implemented fully".format(self._action_type))
        def fdel(self):
            del self._action_type
        return locals()
    action_type = property(**action_type())

    def trajopt_time(self):
        return self._trajopt_time/self._tot_resets

    def step(self, actions=None):
        """
        adds the given action to the expert action
        """
        # TODO: if the action is meaningless because the env is about to be reset, don't run this? Save some time
        if not self.resets.all():
            if actions == None:
                actions = self.zero_actions()
            actions = self.modify_action(actions)

            if self._action_type == self.ActionTypes.TARGET_POSE:
                self._trajopt_step(actions)
            else:
                for _ in range(self.steps_per_action):
                    self._base_steps(actions.clone())

        self.steps_i += 1

        if (self.resets > 0).any():
            self.reset_idx(self.resets.nonzero(as_tuple=False).squeeze(-1))

        if (self.steps_i >= self._max_bandit_steps).any():
            self.update_resets(self.steps_i >= self._max_bandit_steps)

        if (self.resets > 0).any():
            self.setdown_actions(self.resets.nonzero(as_tuple=False).squeeze(-1))

        state = self.get_state()
        rewards = self.get_reward()
        if (self.resets > 0).any():
            self._success += (rewards>0.9).sum().item()
            self._runs += (self.resets > 0).sum().item()
        dones = self.get_dones()
        info = self.get_info()
        return state, rewards, dones, info

    def _base_steps(self, actions):
        if self._actor_grasp_control:
            self.steps_i[actions[:,-1] >= self.setdown_threshold] = self._max_bandit_steps
        if actions.shape[1] < self.num_acts:
            actions = torch.cat([actions, torch.zeros_like(actions[:,0:1])], dim=1)
            actions[:,-1] = self.expert.grasp_value
        if 'rpl' in self._action_modif:
            eactions = self.expert.get_actions(self.get_state())
            if self._action_type == self.ActionTypes.EE_OFFSET:
                actions = jacobian_controller.get_action_from_error(
                    actions[:, 0:6], self.fullstate['jacobian']
                )
            actions = self.add_actions(eactions, actions)
        super().step(actions)

    def add_actions(self, bactions, actions):
        ajnts = self.robot.arm.num_joints()
        bactions[:,:ajnts] += self.scale_action(actions[:,:ajnts])
        # TODO: Maybe need to do something special when adding the end effector actions
        # bactions[:,ajnts:] += actions[:,ajnts:]
        return bactions

    def _trajopt_step(self, actions):
        eactions = self.expert.get_actions(
            self.get_state(), target_pose=actions
        ).to(self.rl_device)
        self.expert_action_steps(eactions)

    def setdown_actions(self, env_ids):
        env_ids = env_ids.cpu()
        self.goal_updates += 1
        state = self.get_state()
        start = time.time()
        setdown_actions = self.expert.get_setdown_actions(state, env_ids).to(self.rl_device)
        self._setdown_time += time.time() - start
        if setdown_actions.shape[1] != self.num_envs:
            full_setdown_actions = torch.stack([self.stationary_actions()]*setdown_actions.shape[0])
            full_setdown_actions[:,env_ids] = setdown_actions[:]
            setdown_actions = full_setdown_actions

        self.expert_action_steps(setdown_actions)

    def reset(self):
        """
        resets the expert too
        """
        self._success = 0
        self._runs = 0
        self._tot_resets = 0
        self._trajopt_time = 0
        self._setdown_time = 0
        return self.reset_idx(torch.arange(self.num_envs))

    def reset_idx(self, env_ids, envreset=True):
        """
        resets the expert at each env too
        """
        env_ids = env_ids.cpu()
        if envreset:
            super().reset_idx(env_ids)
            for _ in range(self.reset_wait_steps):
                super().step()

        if self._preset_actions is not None:
            self._preset_act_idx = \
                torch.randint(0, self._preset_actions.shape[1], (len(env_ids),))
        if self._preset_noise is not None:
            if self._preset_actions is None:
                self._preset_act_idx = \
                    torch.randint(0, self._preset_noise.shape[1], (len(env_ids),))
            self.object_noises[env_ids] = \
                self._preset_noise[self.preset_idx, self._preset_act_idx]

        self.goal_updates += 1
        state = self.get_state()
        if 'rpl' not in self._action_modif:
            start = time.time()
        if self._preset_setup is not None:
            setup_actions = self._preset_setup[self.preset_idx].transpose(0,1)
        else:
            setup_actions = self.expert.get_setup_actions(state, env_ids).to(self.rl_device)
        if 'rpl' not in self._action_modif:
            self._trajopt_time += time.time() - start
        if setup_actions.shape[1] != self.num_envs:
            full_setup_actions = torch.stack([self.stationary_actions()]*setup_actions.shape[0])
            full_setup_actions[:,env_ids] = setup_actions[:]
            setup_actions = full_setup_actions

        self.expert_action_steps(setup_actions)

        state = self.get_state()
        if 'rpl' in self._action_modif:
            start = time.time()
            if self._preset_actions is not None:
                actions = self._preset_actions[self.preset_idx, self._preset_act_idx]
                self.expert.set_actions(env_ids, actions)
            else:
                self.expert.reset_idx(env_ids, state)
            self._trajopt_time += time.time() - start

        self.steps_i[env_ids] *= 0
        self.resets[env_ids] *= 0
        self._tot_resets += 1
        if self._tot_resets % 10 == 0:
            self.expert.reset_setup()

        return state

    def expert_action_steps(self, actions):
        additive = self.additive
        self.additive = False
        jacobian = self.jacobian_control
        self.jacobian_control = False
        for action in actions:
            super().step(action)
        self.additive = additive
        self.jacobian_control = jacobian

    def update_resets(self, resets):
        self.resets[:] = torch.where(
            resets,
            torch.ones_like(self.resets),
            self.resets
        )

    def zero_actions(self, state=None):
        zeros = None
        if self._action_type == self.ActionTypes.TARGET_POSE:
            zeros = torch.zeros((self.num_envs, self._dof), device=self.rl_device)
            if state is not None:
                ee_pose = state['fullstate'][:,state['state_idx']['ee_state']]
                zeros[:,3:7] = ee_pose[:,3:7].clone()
        else:
            zeros = super().zero_actions()
        zeros[:,-1] = self.discrete_grips.close
        return zeros

    def modify_action(self, actions):
        if self._action_type in [self.ActionTypes.TARGET_POSE]:
            actions[:,3:7] = torch.nn.functional.normalize(actions[:,3:7])
        return actions

    def get_state(self):
        self.update_states()
        obs_dict = {}
        obs_dict['obs'] = self.full2obs(self.fullstate.data).clone()
        obs_dict['full_state'] = self.fullstate.clone()
        obs_dict['fullstate'] = self.fullstate.data.clone()
        obs_dict['state_idx'] = self.fullstate.indices
        # if self.store_rgb:
        #     obs_dict['rgb'] = self.fullstate['rgb']
        # if self.store_depth:
        #     obs_dict['depth'] = self.fullstate['depth']
        if self._action_modif == 'rpla':
            raise NotImplementedError("rpla not setup")
            # add next action to the state
            next_eactions = self.expert.get_actions(self.get_state(), take_step=False)
            next_eactions[:,:7] = self.scale_joint_position(next_eactions[:,:7])
            obs_dict['fullstate'] = torch.cat(
                [obs_dict['fullstate'], next_eactions.to(self.rl_device)], dim=1
            )
        return obs_dict

    def full2obs(self, state):
        obs = super().full2obs(state)
        if self._action_modif == 'rpla':
            if state.shape[1] == self.full_state_size:
                next_eactions = state[:,self.state_idx['expert_actions']]
            else:
                next_eactions = self.expert.get_actions(self.get_state(), take_step=False)
                next_eactions[:,:7] = self.scale_joint_position(next_eactions[:,:7])
            obs = torch.cat([obs, next_eactions.to(self.rl_device)], dim=1)
        return obs

    def get_reward(self):
        if not (self.resets > 0).any():
            return torch.zeros_like(self.rew_buf)
        self.compute_reward()
        reward = self.rew_buf.clone()
        reward[self.resets == 0] *= 0.
        return reward

    def _add_preset_state(self, preset):
        super()._add_preset_state(preset)
        self._preset_setup = self.full_preset_states['setups'].to(self.rl_device)
        if self._planner == 'trajopt':
            self._preset_actions = self.full_preset_states['actions'].to(self.rl_device)
        if 'noise' in self.full_preset_states:
            self._preset_noise = self.full_preset_states['noise'].to(self.rl_device)

    def get_preset_states(self):
        env_ids = torch.arange(self.num_envs)
        randomize = self.randomize
        same_starts = self.expert.same_starts
        # self.expert.same_starts = False
        self.randomize = 'inf'

        super().reset_idx(env_ids)
        super().step()
        self.goal_updates += 1
        state = self.get_state()
        preset_state = super().get_preset_states()
        noise = self.object_noises.clone()

        # add setup trajectory
        setup_actions = self.expert.get_setup_actions(state, env_ids)

        # get act trajectory
        self.expert_action_steps(setup_actions)

        state = self.get_state()
        assert 'rpl' in self._action_modif
        self.expert.reset_idx(env_ids, state)

        actions = self.expert.actions

        self.expert.same_starts = same_starts
        self.randomize = randomize
        return preset_state, setup_actions, actions, noise

    def get_dones(self):
        return self.resets.clone()

    def get_info(self):
        return self.extras

    def successes(self):
        return self._success

    def runs(self):
        return self._runs

    @property
    def max_steps(self):
        return self._max_bandit_steps

    @property
    def full_state_size(self):
        siz = self.fullstate.shape[1]
        if self._action_modif == 'rpla':
            siz += self.action_size #self.config['num_acts']
        return siz
