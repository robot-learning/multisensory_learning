#!/usr/bin/env python3
"""
This script resizes images in H5 files to the desired size while copying over all other data.

The use case is data is often recorded at a high resolution for nice visualization but input
to network needs to be smaller and the H5 files are too large for practical loading during
learning.
"""
import os
import sys
import h5py
import argparse
import numpy as np
from tqdm import tqdm
from glob import glob

from multisensory_learning.util import file_util, ui_util, data_util


def resize_h5_images(nominal_h5_root, resized_h5_root, img_size):
    print(f"\nCopying data to new H5 and resizing images to (h, w) = ({img_size}, {img_size})...\n")
    tasks = os.listdir(nominal_h5_root)
    for task in tasks:
        nominal_task_path = os.path.join(nominal_h5_root, task)
        resized_task_path = os.path.join(resized_h5_root, task)
        os.makedirs(resized_task_path)
        filenames = sorted(os.listdir(nominal_task_path))
        for filename in tqdm(filenames, desc=task):
            nominal_filename = os.path.join(nominal_task_path, filename)
            resized_filename = os.path.join(resized_task_path, filename)
            nominal_h5 = h5py.File(nominal_filename, 'r')
            resized_h5 = h5py.File(resized_filename, 'w')
            for key, data in nominal_h5.items():
                if key == 'rgb':
                    resized_h5[key] = data_util.resize_images(data, (img_size, img_size, 3))
                elif key == 'depth':
                    resized_h5[key] = data_util.resize_images(data, (img_size, img_size))
                elif key in ['instance_segmentation', 'semantic_segmentation']:
                    resized_h5[key] = data_util.resize_images(data, (img_size, img_size))
                    for attr_key, attr in data.attrs.items():
                        resized_h5[key].attrs[attr_key] = np.copy(attr)
                else:
                    nominal_h5.copy(key, resized_h5)
            for attr_key, attr in nominal_h5.attrs.items():
                resized_h5.attrs[attr_key] = np.copy(attr)
    
    ui_util.print_happy(f"\nSamples created successfully here: {resized_h5_root}\n")
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--nominal_h5_root', type=str, required=True,
                        help="Absolute path to root directory of H5 files to be resized.")
    parser.add_argument('-r', '--resized_h5_root', type=str, required=True,
                        help="Absolute path to root directory where resized H5 files will be stored.")
    # TODO can easily change to arbitrary height/width but I don't have that use case right now
    parser.add_argument('-s', '--image_size', type=int, required=True,
                        help="Size to resize image to (used for height and width)")
    args = parser.parse_args()

    file_util.check_path_exists(args.nominal_h5_root, "H5 directory")
    file_util.safe_create_dir(args.resized_h5_root)

    resize_h5_images(args.nominal_h5_root, args.resized_h5_root, args.image_size)
