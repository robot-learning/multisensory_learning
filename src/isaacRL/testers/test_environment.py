
from isaacgym import gymtorch, gymapi  # noqa: F401
from isaacRL.train import *
from isaacRL.utils.grapher import EvalGrapher
import matplotlib.pyplot as plt

from ll4ma_util import file_util

from isaacRL.tasks.gymtasks.config import EnvironmentConfig
from isaacRL.tasks.gymtasks.environment import Environment

def main():
    config = 'configs/new_env_configs/iiwa_block.yaml'
    config = EnvironmentConfig(config_filename=config)
    env = Environment(config)
    print(env)

    max_steps = 1000

    env.reset()
    for envstep in range(max_steps):
        # state, reward, dones, info = env.step()
        state = env.step()


    # config = {'environ': {}}
    # opt, trainer = setup(config, eval=True)
    # config = trainer.config
    # trainer._use_eval_data = True
    # if opt.use_ros:
    #     import rospy
    #     print('using ros')
    #     rospy.init_node('test_env')
    #
    # if opt.checkpt is not None:
    #     trainer.load_model(opt.checkpt)
    #
    # full_times = 10
    # max_steps = max(trainer.max_steps*full_times, 10) + 1#+(full_times-1)
    # envir = trainer.envir
    #
    # envir.reset()
    # for envstep in range(max_steps):
    #     state, reward, dones, info = envir.step()
    #     state, state_idx = state['fullstate'], state['state_idx']
    #     if 'rgb' in state_idx and envstep == 0:
    #         rgb = state[:,state_idx['rgb']]
    #         rgb = rgb.reshape(envir.num_envs,envir.img_width,envir.img_height,3)
    #         plt.imshow(rgb[0].cpu().numpy()/255.)
    #         plt.savefig('checkpoints/rgb_test_img.png')
    #
    # if opt.use_ros:
    #     import rospy
    #     rospy.is_shutdown()


if __name__ == '__main__':
    main()
