
import torch

def run_steps(envir, actions, max_steps=100):
    if len(actions.shape) == 3:
        max_steps = actions.shape[0]
    else:
        actions = torch.stack([actions]*max_steps)
    for step in range(max_steps):
        state, rewards, done, info = envir.step(actions[step])
    return state

def joint2actions(envir, joints):
    if len(joints.shape) == 2:
        actions = torch.zeros((joints.shape[0], 8), dtype=envir.actions.dtype, device=joints.device)
        actions[:,:7] += joints
        actions[:,-1] += 0.5
        actions = torch.stack([actions]*envir.num_envs).transpose(0,1)
    else:
        actions = torch.zeros((envir.num_envs, 8), dtype=envir.actions.dtype, device=joints.device)
        actions[:,:7] += joints
        actions[:,-1] += 0.5
    return actions

def get_joints_and_ee_pose(envir, state):
    state_idx = state['state_idx']
    state = state['fullstate']
    joints = state[:,state_idx['joint_position']][:,:7].clone()
    ee_state = state[:,state_idx['ee_state']][:,:7].clone()
    return joints, ee_state

def optim_ik(envir, target_quat, target_qs=None):
    from ll4ma_opt.solvers import GaussNewtonMethod # PenaltyMethod, AugmentedLagranMethod
    from ll4ma_opt.problems import IiwaIK
    from ll4ma_util import math_util
    target_rot = math_util.pose_to_homogeneous(target_quat[:3], target_quat[3:7])
    params = {'alpha': 1, 'rho': 0.5}
    FP_PRECISION = 1e-5
    problem = IiwaIK(
      desired_pose=target_rot,
      error_type='pose',
      use_collision=False,
      collision_env=(
        1, 0.1, (torch.tensor([0.3, 0., 0.99]), torch.tensor([0.3, 0., 0.99]))
      ),
      soft_limits=False
    )
    # solver = PenaltyMethod(problem, 'gauss newton', params, FP_PRECISION=FP_PRECISION)
    solver = GaussNewtonMethod(problem, params['alpha'], params['rho'], FP_PRECISION=FP_PRECISION)
    if target_qs is not None:
        problem.initial_solution = target_qs.unsqueeze(-1).cpu().numpy()
    result = solver.optimize(problem.initial_solution, max_iterations=50)
    return torch.tensor(result.iterates[-1].squeeze())

def moveit_ik(envir, target_quat):
    from moveit_interface import util as moveit_util
    ik_resp, _ = moveit_util.get_ik([target_quat], 'reflex_palm_link')
    solfound = ik_resp.solution_found[0]
    if not solfound:
        print('Solution Not Found')
        return torch.zeros((7,))
    soln_joints = torch.tensor(ik_resp.solutions[0].position)
    return soln_joints

def optim_traj():
    set_start_joints
