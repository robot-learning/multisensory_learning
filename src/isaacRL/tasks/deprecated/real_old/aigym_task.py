
from isaacRL.tasks.sim_task import SimTask

import gym
import numpy as np

class AIGymTask(gym.GoalEnv):
    """
    This class acts as a wrapper around the SimTask class so outside functions
    can interact with Isaacgym the same way as OpenAI Gym.
    """

    def __init__(self, config):
        # super(AIGymTask, self).__init__()
        self.config = config
        self.device = "cuda" if torch.cuda.is_available() else "cpu"
        self.simtask = SimTask(config, self.device)

        state_feats = 0
        self.feat_mods = {}
        for mod in config['modalities']:
            if mod == 'rgb':
                state_feats += config['modalities'][mod]['outfeats']
            else:
                self.feat_mods[mod] = config['modalities'][mod]
                state_feats += self.feat_mods[mod]

        self.action_space = gym.spaces.Box(
          self.config['act_range'][0],
          self.config['act_range'][1],
          shape=( self.config['actions'], ),
          dtype="float32"
        )
        self.observation_space = gym.spaces.Dict(
          dict(
            desired_goal=gym.spaces.Box(
              -np.inf, np.inf, shape=(self.simtask.goalie.goal_shape,), dtype="float32"
            ),
            achieved_goal=gym.spaces.Box(
              -np.inf, np.inf, shape=(self.simtask.goalie.goal_shape,), dtype="float32"
            ),
            observation=gym.spaces.Box(
              -np.inf, np.inf, shape=(state_feats,), dtype="float32"
            ),
          )
        )
        # self.reward_range = (0, 1) # auto set to (-inf, inf)

    def _get_obs(self):
        state = self.simtask.state
        obs = {}
        obs['achieved_goal'] = self.simtask.goalie.extract_current(state).cpu().squeeze(0).numpy()
        obs['desired_goal'] = state['goal'].clone().cpu().squeeze(0).numpy()
        feats = [
          state[key].flatten(1)[:,:self.feat_mods[key]]
            for key in state if key in self.feat_mods
        ]
        feats = torch.cat( feats, 1 )
        obs['observation'] = feats.clone().cpu().squeeze(0).numpy()
        return obs

    def step(self, action):
        action = torch.tensor(action).unsqueeze(0)
        _, reward, done = self.simtask.step(action)
        obs = self._get_obs()
        return obs, reward.cpu().item(), done.cpu().item()==1, None

    def reset(self):
        self.simtask.reset()

    def render(self):
        pass

    def close(self):
        pass

    def seed(self):
        pass

if __name__ == '__main__':

    from ll4ma_util import file_util
    import torch
    import os.path as osp

    # env = gym.make('CartPole-v1')
    # print(env.action_space)
    # print(env.observation_space)
    #
    # obs = env.reset()
    # action = torch.zeros((1,2))
    # for i in range(1000):
    #     action = env.action_space.sample()
    #     obs, reward, done, info = env.step(action)
    #     env.render()
    #     if done:
    #         obs = env.reset()
    #
    # env.close()
    #
    # print('action:', action)
    # print('finished gym test')

    config = file_util.load_yaml( osp.expanduser(
      '~/isaac_ws/src/multisensory_learning/src/isaacRL/configs/rpl_ppo/ppo_iiwastack1.yaml'
    ) )

    config['environ']['modalities'] = config['model']['modalities']
    config['environ']['actions'] = config['model']['actor']['actions']
    config['environ']['num_acts'] = config['model']['actor']['actions']
    config['environ']['act_range'] = config['model']['actor']['act_range']
    config['environ']['num_envs'] = 1

    env = AIGymTask(config['environ'])

    obs = env.reset()
    for i in range(100):
        action = env.action_space.sample()
        obs, reward, done, info = env.step(action)
        env.render()
        if done:
            obs = env.reset()
