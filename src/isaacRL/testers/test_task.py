
from isaacgym import gymtorch, gymapi  # noqa: F401
from isaacRL.train import *
from isaacRL.tasks.get_task import get_task

from ll4ma_util import file_util


def main():
    torch.set_printoptions(precision=3,sci_mode=False)

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str,
        default="configs/tests/joint_01n_rpl00.yaml", help="path to config file")
    parser.add_argument("--headless", default=None,
        help="overwrite running headless or not")
    parser.add_argument("--checkpt", type=str, default=None,
        help="if continuing training from checkpoint model")
    opt = parser.parse_args()

    config = file_util.load_yaml(opt.config)
    config['environ']['headless'] = False
    config['environ']['num_envs'] = 4
    env = get_task(config['environ'])
    print(env)

    max_steps = 1200

    state = env.reset()
    for envstep in range(max_steps):
        state, rewards, dones, info = env.step()
        if envstep == 0:
            print('state keys:', state.keys())
            print('state shape:', state['obs'].shape)
            print('full state keys:', state['full_state'].keys())
            print('rewards shape:', rewards.shape)
            print('dones shape:', dones.shape)
            print('dones:', dones)
            print('rewards:', rewards)
        if (dones>0).any():
            print('dones:', dones)
            print('rewards:', rewards)
            # import pdb; pdb.set_trace()


if __name__ == '__main__':
    main()
