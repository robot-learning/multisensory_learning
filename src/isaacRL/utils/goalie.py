
from .goalies import *

goalie_dict = {
  'Reach2Pose': Reach2Pose_Factory(),
  'Reach2PoseShaped': Reach2PoseShaped_Factory(),
  'Move2Object': Move2Object_Factory(),
  'Move2ObjectShaped': Move2ObjectShaped_Factory(),
  'PlaceObject': PlaceObject_Factory(),
  'PlaceObjectShaped': PlaceObjectShaped_Factory(),
  'LiftObject': LiftObject_Factory(),
  'LiftObjectShaped': LiftObjectShaped_Factory(),
}
def select_goalie(config,device='cpu'):
    if config['type'] not in goalie_dict:
        print('unkown goal:', config['type'])
        return None
    config['device'] = device
    return goalie_dict[config['type']].build(config)
