#!/usr/bin/env python
import os
import os.path as osp
import sys
import ast
import argparse
import random
import numpy as np
from tqdm import tqdm
from typing import List, Dict
from dataclasses import dataclass

from multisensory_learning.learning import (
    PickleDataset,
    Learner, LearnerConfig,
    Ortho6dConfig, Ortho6dLearner
)
from multisensory_learning.models import MLP, MDN
from multisensory_learning.models.loss import matrix_log_loss, mdn_loss
from multisensory_learning.util import learn_util

from ll4ma_util import file_util, torch_util, ui_util, func_util, math_util, ros_util
from distribution_planning.distributions.torch_distributions import GaussianMixtureModel

import torch


SKILLS = [
    "push",
    "pick_place",
    "drop"
]
OBJECTS = [
    "cleaner",
    "mug",
    "skillet",
    "blue_block",
    "red_block",
    "yellow_block",
    "double_blue_block",
    "block_10cm"
]


class SkillLearnerDataset(PickleDataset):

    def __getitem__(self, idx):
        data, attrs = file_util.load_pickle(self.filenames[idx])
        if len(attrs['behavior_params']) != 1:
            raise ValueError(f"Expected only one top-level behavior, got these: "
                             f"{attrs['behavior_params'].keys()}")
        skill = list(attrs['behavior_params'].keys())[0]
        if skill not in ['push_object', 'pick_place']:
            raise ValueError(f"Unknown skill type for creating sample: {skill}")
        params = list(attrs['behavior_params'].values())[0]
        target_obj = params['target_object']

        # Determine index for skill start
        if skill == 'push_object':
            start_idx = list(data['behavior']).index('push_object:push')
        elif skill == 'pick_place':
            start_idx = list(data['behavior']).index('pick_place:pick:grasp')
        
        # Read out data
        obj_pos_init = np.array(data['objects'][target_obj]['position'][0])
        obj_quat_init = np.array(data['objects'][target_obj]['orientation'][0])
        obj_pos_end = np.array(data['objects'][target_obj]['position'][-1])
        obj_quat_end = np.array(data['objects'][target_obj]['orientation'][-1])
        ee_pos_start = np.array(data['ee_position'][start_idx])
        ee_quat_start = np.array(data['ee_orientation'][start_idx])
        ee_pos_end = np.array(data['ee_position'][-1])
        ee_quat_end = np.array(data['ee_orientation'][-1])

        # Create object and EE poses (as homogeneous TFs)
        w_T_obj_init = math_util.pose_to_homogeneous(obj_pos_init, obj_quat_init)
        w_T_obj_end = math_util.pose_to_homogeneous(obj_pos_end, obj_quat_end)
        w_T_ee_start = math_util.pose_to_homogeneous(ee_pos_start, ee_quat_start)
        w_T_ee_end = math_util.pose_to_homogeneous(ee_pos_end, ee_quat_end)        

        # Get the inverse of the object pose so we can transform into object frame
        obj_init_T_w = math_util.homogeneous_inverse(w_T_obj_init)
        
        # Compute delta obj pose (for targets in loss)
        obj_init_T_obj_end = obj_init_T_w @ w_T_obj_end

        # Transforming EE poses into obj frame (for model inputs)
        obj_T_ee_start = obj_init_T_w @ w_T_ee_start
        obj_T_ee_end = obj_init_T_w @ w_T_ee_end
        
        sample = {
            # Model inputs:
            'ee_start_pos_in_obj' : math_util.homogeneous_to_position(obj_T_ee_start),
            'ee_start_rot_in_obj' : math_util.homogeneous_to_rotation(obj_T_ee_start).flatten(),
            'ee_end_pos_in_obj'   : math_util.homogeneous_to_position(obj_T_ee_end),
            'ee_end_rot_in_obj'   : math_util.homogeneous_to_rotation(obj_T_ee_end).flatten(), 
            # Loss inputs
            'obj_pos_delta_in_obj': math_util.homogeneous_to_position(obj_init_T_obj_end),
            'obj_rot_delta_in_obj': math_util.homogeneous_to_rotation(obj_init_T_obj_end).flatten()
        }
        return sample
    

@dataclass
class SkillLearnerConfig(LearnerConfig):
    data_root: str = ''
    hidden_sizes: List[int] = func_util.lambda_field([256, 256])
    activation: str = 'relu'
    norm: str = 'layer'
    n_mdn_components: int = 2
    ortho6d_checkpoint: str = ''
    skill: str = ''
    object: str = ''
    learner_type: str = 'mdn'
    input_type: str = 'point'
    metadata: Dict = None

    def __post_init__(self):
        if self.skill and self.skill not in ['push', 'pick_place']:
            raise ValueError(f"Invalid skill type: {self.skill}")
        if self.learner_type not in ['mdn', 'regressor-ml', 'regressor-latent']:
            raise ValueError(f"Invalid learner type: {self.learner_type}")

    @property
    def object_config(self):
        return self.metadata['env']['objects'][self.object]
        
    @property
    def object_urdf_filename(self):
        return osp.join(self.object_config['asset_root'], self.object_config['asset_filename'])

    @property
    def object_mesh_filename(self):
        return ros_util.get_mesh_filename_from_urdf(self.object_urdf_filename)

    @property
    def ee_mesh_filename(self):
        # TODO hacking this for now
        return "package://multisensory_learning/src/multisensory_learning/assets/reflex.stl"
        

class SkillLearner(Learner):

    def __init__(
            self,
            config,
            train_filenames=[],
            val_filenames=[],
            set_train=False,
            set_eval=False,
            device=None
    ):
        super().__init__(config, train_filenames, val_filenames, set_train, set_eval, device)

        if train_filenames:
            # Get the metadata to save in the checkpoint
            metadata_fn = osp.join(osp.dirname(train_filenames[0]), 'metadata.yaml')
            self.config.metadata = file_util.load_yaml(metadata_fn)
        
        ortho6d_config = Ortho6dConfig(checkpoint=self.config.ortho6d_checkpoint)
        self.ortho6d_learner = Ortho6dLearner(ortho6d_config, set_eval=True, device=self.device)

    @property
    def skill(self):
        return self.config.skill
        
    def get_input_size(self):
        if self.skill == 'push':
            # EE start pos (3D) and ortho (6D), EE end pos (3D) all in obj frame
            in_size = 12
        elif self.skill == 'pick_place':
            # EE start pos (3D) and ortho (6D), EE end pos (3D) and orhto (6D) all in obj frame
            in_size = 18
        else:
            raise ValueError(f"Unknown skill type: {self.skill}")
        return in_size

    def get_output_size(self):
        if self.skill in ['push', 'pick_place']:
            out_size = 9 # 3D obj delta pos and 6D ortho obj delta rot
        else:
            raise ValueError(f"Unknown skill type: {self.skill}")
        return out_size

    def get_sample_bounds(self):
        if self.skill == 'push':
            # TODO these should probably be set more intelligently based on
            # the training data, I just manually checked these in rviz
            min_bounds = np.array([
                 0.2,   # Start x pos in world
                -0.8,   # Start y pos in world
                 0.819, # Start z pos in world
                 0.2,   # End x pos in world
                -0.8    # End y pos in world
            ])
            max_bounds = np.array([
                 0.9,   # Start x pos in world
                 0.8,   # Start y pos in world
                 0.955, # Start z pos in world
                 0.9,   # End x pos in world
                 0.8    # End y pos in world
            ])
        else:
            raise ValueError(f"Unknown skill type: {self.skill}")
        return min_bounds, max_bounds

    def sample_to_model_input(self, sample, w_T_obj=None, pos_ortho6d=None):
        if w_T_obj is None and pos_ortho6d is None:
            raise ValueError("Must specify either w_T_obj or pos_ortho6d")
        elif pos_ortho6d is not None:
            w_T_obj = learn_util.vec_to_homogeneous(pos_ortho6d)

        if self.skill == 'push':
            inputs = self._push_sample_to_model_input(sample, w_T_obj)
        elif self.skill == 'pick_place':
            inputs = self._pick_place_sample_to_model_input(sample, w_T_obj)
        else:
            raise ValueError(f"Unknown skill type: {self.skill}")
        return inputs

    def get_sample_size(self):
        min_bounds, _ = self.get_sample_bounds()
        return len(min_bounds)
        
    def set_models(self):
        in_size = self.get_input_size()
        out_size = self.get_output_size()
        
        if self.config.learner_type == 'mdn':
            self.models = {
                'mdn': MDN(
                    in_size,
                    out_size,
                    self.config.hidden_sizes,
                    self.config.n_mdn_components,
                    self.config.activation,
                    self.config.norm
                )
            }
        elif 'regressor' in self.config.learner_type:
            self.models = {
                'regressor': MLP(
                    in_size,
                    out_size,
                    self.config.hidden_sizes,
                    self.config.activation,
                    self.config.norm
                )
            }
        else:
            raise ValueError(f"Unknown learner type: {self.config.learner_type}")
        super().set_models()
        
    def get_checkpoint_filename(self, epoch, *args):
        return osp.join(self.config.checkpoint_dir, f"checkpoint_{epoch:03d}.pt")
            
    def apply_models(self, batch):
        if isinstance(batch, dict):
            torch_util.move_batch_to_device(batch, self.device)
            inputs = [
                batch['ee_start_pos_in_obj'],
                learn_util.rot_to_ortho6d(batch['ee_start_rot_in_obj'], self.ortho6d_learner),
                batch['ee_end_pos_in_obj']
            ]
            if self.skill == 'pick_place':
                inputs.append(
                    learn_util.rot_to_ortho6d(batch['ee_end_rot_in_obj'], self.ortho6d_learner)
                )
            inputs = torch.cat(inputs, dim=-1).float()
        elif torch.is_tensor(batch):
            # Assume inputs are already good to go into model
            inputs = batch.to(self.device).float()
        else:
            raise ValueError(f"Unknown type for batch: {type(batch)}")
            
        outputs = {}
        if self.config.learner_type == 'mdn':                  
            outputs['mdn'] = self.models['mdn'](inputs)
        elif 'regressor' in self.config.learner_type:
            outputs['regressor'] = self.models['regressor'](inputs)
        else:
            raise ValueError(f"Unknown learner type: {self.config.learner_type}")
        if isinstance(batch, dict) and 'obj_rot_delta_in_obj' in batch:
            outputs['obj_ortho6d_delta_in_obj'] = learn_util.rot_to_ortho6d(
                batch['obj_rot_delta_in_obj'],
                self.ortho6d_learner
            )
        return outputs
    
    def compute_loss(self, batch, outputs):
        # Note doing delta predictions
        target_pos_delta = batch['obj_pos_delta_in_obj']
        target_rot_delta = batch['obj_rot_delta_in_obj'].view(-1, 3, 3).float()
        target_ortho6d_delta = outputs['obj_ortho6d_delta_in_obj']
        if self.config.learner_type == 'mdn':
            target = torch.cat([target_pos_delta, target_ortho6d_delta], dim=-1)
            alpha, mu, sigma = outputs['mdn']        
            loss = mdn_loss(target, *outputs['mdn'])
            loss_dict = {'mdn': loss.item()}
        elif 'regressor' in self.config.learner_type:
            pred_pos_delta = outputs['regressor'][:,:3]
            pos_loss = (target_pos_delta - pred_pos_delta).square().sum(dim=-1).mean()
            if self.config.learner_type == 'regressor-ml':
                pred_rot_delta = torch_util.ortho6d_to_rotation(outputs['regressor'][:,3:])
                rot_loss = matrix_log_loss(target_rot_delta, pred_rot_delta).mean()
            elif self.config.learner_type == 'regressor-latent':
                pred_ortho6d_delta = outputs['regressor'][:,3:]
                rot_loss = (target_ortho6d_delta - pred_ortho6d_delta).square().sum(dim=-1).mean()
            else:
                raise ValueError(f"Unknown learner type: {self.config.learner_type}")
            loss = pos_loss + rot_loss
            loss_dict = {'pos': pos_loss.item(), 'rot': rot_loss.item()}
        else:
            raise ValueError(f"Unknown learner type: {self.config.learner_type}")
        return loss, loss_dict

    def propagate_distribution(
            self,
            params,
            samples=None,
            distribution=None,
            n_input_samples=100,
            n_samples_per_output=None,  # Will be resolved to better default if not specified
            fit_gmm=False,
            propagation_type='samples',
            filter_to_topk=-1
    ):
        """
        Expecting either samples or distribution as input, in latter case
        n_input_samples will be generated from distribution (or other options
        when set for distribution, e.g. propagate mean).
        """
        if samples is None and distribution is None:
            raise ValueError("Must specify either samples or distribution")
        if samples is not None and distribution is not None:
            raise ValueError("Must specify only one of samples or distribution")
        if distribution is not None:
            samples = distribution.sample(n_input_samples)
        if not torch.is_tensor(params):
            params = torch.tensor(params)
        if samples is not None and not torch.is_tensor(samples):
            samples = torch.tensor(samples)

        if propagation_type == 'samples':
            # ui_util.print_warning("Propagating samples")
            if n_samples_per_output is None:
                n_samples_per_output = 1
            B = samples.size(0)
            pos_ortho6d = samples
        elif propagation_type == 'mean':
            # ui_util.print_warning("Propagating mean")
            if n_samples_per_output is None:
                n_samples_per_output = 100
            B = 1
            pos_ortho6d = distribution.get_mean()
            if not torch.is_tensor(pos_ortho6d):
                pos_ortho6d = torch.tensor(pos_ortho6d)
            if pos_ortho6d.dim() == 1:
                pos_ortho6d = pos_ortho6d.unsqueeze(0)
        else:
            raise ValueError(f"Unknown propagation type: {propagation_type}")
        
        if params.dim() == 1:
            params = torch_util.make_batch(params, B)
        inputs = self.sample_to_model_input(params, pos_ortho6d=pos_ortho6d)

        N = n_samples_per_output
        
        alpha, mu, sigma_tril = self.apply_models(inputs)['mdn']
        gmm = GaussianMixtureModel(alpha, mu, sigma_tril=sigma_tril)
        delta_samples, mix_idxs = gmm.sample(N)        
        samples = learn_util.compose_vecs(
            pos_ortho6d.repeat(N, 1),
            delta_samples.view(N * B, -1),
            self.ortho6d_learner
        )
        
        if fit_gmm or filter_to_topk > 0:
            gmm = GaussianMixtureModel()
            gmm.fit(samples.view(N * B, 1, -1), self.config.n_mdn_components)
        else:
            gmm = None
            
        if filter_to_topk > 0:
            # TODO trying this out, evaluate samples under fit GMM and reject low-probability ones.
            pdfs = gmm.log_pdf(samples.unsqueeze(1)).squeeze()
            # min_pdf = pdfs.min().item()
            # max_pdf = pdfs.max().item()
            topk, topk_idxs = pdfs.topk(filter_to_topk, largest=True)
            samples = samples[topk_idxs]
    
        return samples, mix_idxs, gmm, inputs

    def _push_sample_to_model_input(self, sample, w_T_obj):
        start_xyz = sample[:,:3]
        end_xyz = start_xyz.clone()
        end_xyz[:,:2] = sample[:,3:]  # Take end xy only and use z from start pos

        push = end_xyz - start_xyz
        push_direction = torch.nn.functional.normalize(push, dim=-1)
        w_R_ee = torch_util.construct_rotation_matrix(  # This is same as push pose gen
            x=push_direction,
            y=torch_util.make_batch(torch.tensor([0., 0., 1.]), sample.size(0)).double(),
        )
        
        w_T_ee_start = torch_util.pose_to_homogeneous(start_xyz, R=w_R_ee)
        w_T_ee_end = torch_util.pose_to_homogeneous(end_xyz, R=w_R_ee)

        obj_T_w = math_util.homogeneous_inverse(w_T_obj)
        obj_T_ee_start = torch.bmm(obj_T_w, w_T_ee_start)
        obj_T_ee_end = torch.bmm(obj_T_w, w_T_ee_end)
        
        inputs = {
            'ee_start_pos_in_obj': math_util.homogeneous_to_position(obj_T_ee_start),
            'ee_start_rot_in_obj': math_util.homogeneous_to_rotation(obj_T_ee_start).flatten(1),
            'ee_end_pos_in_obj': math_util.homogeneous_to_position(obj_T_ee_end)
        }
        return inputs

    def _pick_place_sample_to_model_input(self, sample, w_T_obj):

        # TODO for now this might be hacked just test model inputs given full EE start
        # and end pose, will have to think more if there's a better parameterization
        # for planning that you wouldn't have to do this

        start_pose, end_pose = sample.chunk(2, dim=-1)
        start_pos = start_pose[:,:3]
        w_R_ee_start = start_pose[:,3:].view(-1, 3, 3)
        w_T_ee_start = torch_util.pose_to_homogeneous(start_pos, R=w_R_ee_start)
        end_pos = end_pose[:,:3]
        w_R_ee_end = end_pose[:,3:].view(-1, 3, 3)
        w_T_ee_end = torch_util.pose_to_homogeneous(end_pos, R=w_R_ee_end)

        obj_T_w = math_util.homogeneous_inverse(w_T_obj)
        obj_T_ee_start = torch.bmm(obj_T_w, w_T_ee_start)
        obj_T_ee_end = torch.bmm(obj_T_w, w_T_ee_end)
        
        inputs = {
            'ee_start_pos_in_obj': math_util.homogeneous_to_position(obj_T_ee_start),
            'ee_start_rot_in_obj': math_util.homogeneous_to_rotation(obj_T_ee_start).flatten(1),
            'ee_end_pos_in_obj': math_util.homogeneous_to_position(obj_T_ee_end),
            'ee_end_rot_in_obj': math_util.homogeneous_to_rotation(obj_T_ee_end).flatten(1)
        }
        return inputs

        
def print_config(config):
    print(f"\n{'='*20} CONFIG {'='*20}\n")
    ui_util.print_dict(vars(config), '    ')
    print("\n")

        
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--skill', type=str, required=True, choices=SKILLS)
    parser.add_argument('-o', '--object', type=str, required=True, choices=OBJECTS)
    parser.add_argument('-r', '--data_root', type=str, default=None)
    parser.add_argument('--checkpoint_root', type=str, default=None)
    parser.add_argument('--checkpoint_aux', type=str, default='',
                        help="Auxiliary string to append to checkpoint dir")
    parser.add_argument('--ortho6d_checkpoint', type=str, default=None)
    parser.add_argument('--batch_size', type=int, default=128)
    parser.add_argument('--n_epochs', type=int, default=100)
    parser.add_argument('--checkpoint_interval', type=int, default=10)
    parser.add_argument('--learner_type', type=str, default='mdn',
                        choices=['mdn', 'regressor-ml', 'regressor-latent'])
    parser.add_argument('--input_type', type=str, default='point',
                        choices=['point', 'gaussian'])
    parser.add_argument('--n_mdn_components', type=int)
    parser.add_argument('--hidden_sizes', type=int, nargs='+')
    parser.add_argument('--clear_cache', action='store_true')
    parser.add_argument('--show_config', action='store_true')
    args = parser.parse_args()

    if args.data_root is None:
        args.data_root = os.environ.get("DATA_ROOT")
    if args.data_root is None:
        ui_util.print_error_exit("\n  Need to specify data root either as environment "
                                 "variable DATA_ROOT or as script input with --data_root "
                                 "(-d) flag\n")
    file_util.check_path_exists(args.data_root)
    if args.checkpoint_root is None:
        args.checkpoint_root = os.environ.get("CHECKPOINT_ROOT")
    if args.checkpoint_root is None:
        ui_util.print_error_exit("\n  Need to specify checkpoint root either as environment "
                                 "variable CHECKPOINT_ROOT or as script put with "
                                 "--checkpoint_root flag\n")

    sample_dirs = [d for d in file_util.list_dir(args.data_root, exclude_files=True)
                   if f"{args.skill}_{args.object}" in d and "_samples" in d]

    if len(sample_dirs) == 0:
        ui_util.print_error_exit("\n  Sample directories not found. Make sure you run "
                                 "collect_skill_data.py with the --create_samples flag first.")
    sample_fns = [f for d in sample_dirs for f in file_util.list_dir(d, '.pickle')]
    
    if args.ortho6d_checkpoint is None:
        args.ortho6d_checkpoint = osp.join(args.checkpoint_root, "ortho6d_cp", "ortho6d_001.pt")
    file_util.check_path_exists(args.ortho6d_checkpoint, "Ortho6d checkpoint file")
    
    learner_config = SkillLearnerConfig()
    func_util.override_dataclass_with_args(learner_config, args)
    # Build up the checkpoint directory name
    cp_dir = f"{args.skill}_{args.object}_cp"
    cp_dir += f"_{learner_config.learner_type}"
    if learner_config.learner_type == 'mdn':
        cp_dir += f"_comp-{learner_config.n_mdn_components}"
    cp_dir += '_' + '-'.join(str(e) for e in learner_config.hidden_sizes)
    if args.checkpoint_aux:
        cp_dir += args.checkpoint_aux
    learner_config.checkpoint_dir = osp.join(args.checkpoint_root, cp_dir)
    file_util.safe_create_dir(learner_config.checkpoint_dir)
    learner_config.train_sample_dirs = sample_dirs
    print_config(learner_config)
    if args.show_config:
        sys.exit()

    learner = SkillLearner(learner_config, sample_fns, set_train=True)
    learner.train()

    ui_util.print_happy("\n  Skill learner training complete.\n")
