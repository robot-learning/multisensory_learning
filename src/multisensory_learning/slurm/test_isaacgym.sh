#!/bin/bash
#SBATCH --job-name=test_isaacgym
#SBATCH -o log-%j.out-%N
#SBATCH -e log-%j.err-%N
#SBATCH --nodes=1
#SBATCH --time=3-00:00:00
#SBATCH --gres=gpu:1
#SBATCH --mem=32G

RLGPU_DIR=$HOME/isaacgym/python/rlgpu
TASK=FrankaCabinet


source $HOME/.bashrc
conda activate isaacgym
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/miniconda3/envs/isaacgym/lib:$HOME/isaacgym/python/isaacgym/_bindings/linux-x86_64

cd $RLGPU_DIR
python train.py --task $TASK --headless
