
from isaacRL.utils.goalies.r2p_goalie import *
from isaacRL.utils.goalies.base_goalie import Goalie

from ll4ma_util import math_util

class Move2Object(Goalie):
    """docstring for Move2Object."""

    def __init__( self, obj_name, offset, device='cpu', threshold=0.01, ee_w=3., rot_w=0., alpha=1., c_w=1000., act_penal=0.01, sparse=False ):
        goal_shape = 7
        super(Move2Object, self).__init__(threshold, goal_shape, device, sparse)

        self.obj_name = obj_name
        self._offset = offset if torch.is_tensor(offset) else torch.tensor(offset, device=self.device)
        self.ee_w = ee_w
        self.rot_w = rot_w
        self.alpha = alpha
        self.c_w = c_w
        self.act_penal = act_penal

        x_axis, y_axis, z_axis = torch.eye(3, device=self.device)
        rot = torch.stack([-z_axis, -y_axis, torch.cross(-z_axis, -y_axis)])
        self.facedown_pose = math_util.rotation_to_quat(rot)

        self.x_axis = torch.tensor([1.,0.,0.], device=self.device)
        self.z_axis = torch.tensor([0.,1.,0.], device=self.device)

    def goal_dist(self, state, state_keys, goal):
        return state[:,state_keys['ee_state']][:,:self.goal_shape] - goal

    def get_current(self, state, state_keys):
        return state[:,state_keys['ee_state']][:,:self.goal_shape].clone()

    def get_goal(self, state, state_keys, noisy=False):
        obj_name = self.obj_name.replace('_noisy', '') \
            if not noisy else self.obj_name
        goal = state[:,state_keys[obj_name]][:,:self.goal_shape].clone()
        goal[:,:3] +=  self._offset[:3]
        goal[:,3:] = self.facedown_pose[:].clone()
        return goal

    def reward(self, state, state_keys, goal):
        ee_pose = state[:,state_keys['ee_state']][:,:self.goal_shape]
        arm_joints = state[0,state_keys['arm_joints']]
        if self.sparse:
            return move2SparseReward(
                ee_pose, goal, state[:,state_keys['prev_action']],
                self.threshold, arm_joints
            )
        return move2ShapeReward(
            ee_pose, goal, state[:,state_keys['prev_action']],
            self.ee_w, self.rot_w, self.c_w, self.alpha,
            self.act_penal, self.threshold, arm_joints
        )

class Move2Object_Factory(object):
    """docstring for Move2Object_Factory."""

    def build(self,config):
        return Move2Object(
            config['object'],
            config['offset'],
            device=config.get('device', 'cpu'),
            threshold=config.get('threshold', 0.01),
            sparse=True
        )

class Move2ObjectShaped_Factory(object):
    """docstring for Move2ObjectShaped_Factory."""

    def build(self,config):
        return Move2Object(
            config['object'],
            config['offset'],
            device=config.get('device', 'cpu'),
            threshold=config.get('threshold', 0.01),
            sparse=False,
            ee_w=config.get('ee_w', 1.),
            rot_w=config.get('rot_w', 1.),
            c_w=config.get('c_w', 10.),
            alpha=config.get('alpha', 10.),
            act_penal=config.get('act_penal', 0.001),
        )
