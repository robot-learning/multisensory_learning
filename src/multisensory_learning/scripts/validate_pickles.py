#!/usr/bin/env python
import sys
import pickle
import argparse
from tqdm import tqdm

from multisensory_learning.util import file_util, ui_util


if __name__ == '__main__':
    """
    Having some problems with decoding data for whatever reason, this is a simple script
    to see if things are getting corrupted in the torch loading.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--pickle_dir', type=str, required=True)
    args = parser.parse_args()

    file_util.check_path_exists(args.pickle_dir)

    for filename in tqdm(file_util.list_dir(args.pickle_dir, '.pickle')):
        try:
            with open(filename, 'rb') as f:
                data = pickle.load(f)
        except UnicodeDecodeError as e:
            ui_util.print_error(f"File {filename} is corrupted: {e}")
            sys.exit(1)

    ui_util.print_happy("\n  Everything seems ok!\n")
