#!/usr/bin/env python
import argparse
import torch
from torch.distributions import Normal, MultivariateNormal
from torch.distributions.kl import kl_divergence
from time import time

# Script testing relative timing of different Gaussian KL divergences on cpu/gpu for planning.
# Should definitely be using Normal for diagonal covariances. Seems computing KL on proper
# multivariates is wayyy more time consuming (couple orders of magnitude)

parser = argparse.ArgumentParser()
parser.add_argument('--horizon', type=int, default=10)
parser.add_argument('--n_candidates', type=int, default=1000)
parser.add_argument('--size', type=int, default=256)
parser.add_argument('--cpu', action='store_true')
parser.add_argument('--mvn', action='store_true')
args = parser.parse_args()

device = torch.device('cpu' if args.cpu else 'cuda')

torch.manual_seed(1)

mu1 = torch.zeros(args.horizon, args.n_candidates, args.size, device=device)
sigma1 = torch.full_like(mu1, 0.1, device=device)

mu2 = torch.zeros_like(mu1, device=device)
sigma2 = torch.full_like(mu1, 0.5, device=device)


if args.mvn:
    p1 = MultivariateNormal(mu1[0], torch.diag_embed(sigma1[0].square(), dim1=-2, dim2=-1))
else:
    p1 = Normal(mu2[0], sigma2[0])

start = time()
for t in range(args.horizon):
    if args.mvn:
        p2 = MultivariateNormal(mu2[t], torch.diag_embed(sigma2[t].square(), dim1=-2, dim2=-1))
        kl_mvn = kl_divergence(p1, p2)
        kl_mvn = kl_divergence(p1, p2)
    else:
        p2 = Normal(mu2[t], sigma2[t])
        kl = kl_divergence(p1, p2).sum(dim=-1)
        kl = kl_divergence(p1, p2).sum(dim=-1)
print(time() - start)



