
from isaacRL.tasks.gym_task import GymTask
from isaacgymenvs.utils.utils import set_seed

from ll4ma_util import file_util

import os, sys, argparse, json, yaml
import rospy
import torch
import numpy as np
from tqdm import tqdm

from isaacRL.experts.jacobian_resolver import JacobianResolver
from isaacRL.testers.utils import *

def main():
    torch.set_printoptions(precision=3,sci_mode=False)

    device = "cpu"

    config = file_util.load_yaml('configs/tests/stack_traj.yaml')
    config['environ']['num_envs'] = 2
    config['environ']['headless'] = False
    config['environ']['device'] = device # -1 if random
    config['environ']['use_gpu'] = device == 'cuda'
    config['environ']['seed'] = -1 # if random
    config['environ']['seed'] = set_seed(
      config['environ']['seed'], torch_deterministic=False
    )
    config['environ']['act_range'] = config['model']['actor']['act_range']
    config['environ']['additive_actions'] = False
    config['environ']['allow_resets'] = False
    config['environ']['allow_state_updates'] = True
    config['environ']['max_env_steps'] = 1000
    max_env_steps = config['environ']['max_env_steps']

    print('using ros')
    rospy.init_node('test_moveit_ik')

    envir = GymTask(config['environ'])

    num_joints = 7
    expert = JacobianResolver(
        max_vel=envir.max_vel[:num_joints]
    )

    state = envir.reset()
    ee_pose = state['fullstate'][:,state['state_idx']['ee_state'][:,:7]]
    target_pose = ee_pose.clone()
    target_pose[:,0] += 0.5
    target_pose[:,1] -= 0.2
    print('target:', target_pose)

    for step in range(max_env_steps):
        if rospy.is_shutdown():
            return

        joints = state['fullstate'][:,state['state_idx']['joint_position'][:,:num_joints]]
        jacobian = state['fullstate'][:,state['state_idx']['jacobian'][:,:num_joints]]
        ee_pose = state['fullstate'][:,state['state_idx']['ee_state'][:,:7]]
        action = expert.get_action(joints, target_pose, jacobian, ee_pose)
        state, reward, dones, info = envir.step(action)

        if step % 10 == 0:
            print(state['fullstate'][0,state['state_idx']['ee_state']])
            print(state['fullstate'][0,state['state_idx']['joint_position']])
            import pdb; pdb.set_trace()

if __name__ == '__main__':
    main()
