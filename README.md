# Multisensory Learning

Repository for paper on multisensory learning. Includes code for training different models and managing datasets.

## Setup

Instead of trying to keep this up to date, I will wait until paper is submitted and then add detailed instructions for how to do everything. There is a setup script to install the conda env [`setup/setup_conda_env.sh`](https://github.com/adamconkey/multisensory_learning/blob/master/setup/setup_conda_env.sh).