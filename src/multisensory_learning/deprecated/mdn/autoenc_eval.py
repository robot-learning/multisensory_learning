
import os.path as osp
import matplotlib.pyplot as plt
from mdn.autoenc_trainer import *

def order_eval(trainer, threshold=0.5, outdir='data'):
    outfile = osp.join(outdir, 'order_test.txt')
    trainer.model.eval()
    valset = EncodedDataset(
      trainer.config['valid_files'], device=trainer.device,
      classify=trainer.config['classify']
    )

    if trainer.config['classify']:
        cnt = 0
        tot = 0
    totLoss = 0.0
    noclas = 0
    confids = []
    clases = []
    targs = []
    maxl = 0
    with open(outfile, 'w') as f:
        f.write('')
    with torch.no_grad():
        pbar = tqdm(total=valset.num_files)
        for fi in range(valset.num_files):
            xdata = valset.getfile(fi)
            state = xdata['state'].to(trainer.device)
            target = xdata['target']
            distr = trainer.model(state).cpu()

            if trainer.config['criterion'] == 'focal':
                distr = torch.softmax(distr, dim=1)
                conf, clas = distr.max(dim=1)
                distr = distr[:,1]
                clas[conf < threshold] = 0
                cnt += (target == clas).sum().item()
                tot += len(target)
            else:
                distr = torch.sigmoid(distr).squeeze(1)
                clas = distr.clone()
                clas[distr<threshold] = 0.0
                clas = clas.round()
                cnt += (target == clas).sum().item()
                tot += len(target)

            target = target.numpy().astype(np.int)
            clas = clas.numpy().astype(np.int)
            distr = distr.numpy()

            tidx = np.where(target==1)[0]
            cidx = np.where(clas==1)[0]
            if clas.sum() < 1:
                cmin = tidx[0]
                cmax = tidx[-1]
            else:
                cmin = min(tidx[0], cidx[0])
                cmax = max(tidx[-1], cidx[-1])+1
            maxl = max(maxl, cmax-cmin)

            confids.append( distr[cmin:cmax] )
            clases.append( clas[cmin:cmax] )
            targs.append( target[cmin:cmax] )

            with open(outfile, 'a+') as f:
                f.write('conf: ')
                f.write( ','.join([str(c) for c in distr[cmin:cmax]]) )
                f.write('\n')
                f.write('pred: ')
                f.write( ','.join([str(c) for c in clas[cmin:cmax]]) )
                f.write('\n')
                f.write('targ: ')
                f.write( ','.join([str(c) for c in target[cmin:cmax]]) )
                f.write('\n')
                f.write('\n')

            desc = f'  Iter {fi}: acc={(float(cnt)/tot):.4f}'
            pbar.set_description(desc)
            pbar.update(1)
        pbar.close()
    # img = np.zeros((len(clases)*2, maxl))
    # for ci in range(len(clases)):
    #     clas = clases[ci]
    #     targ = targs[ci]
    #     ci = ci*2
    #     img[ci][:len(clas)] += clas
    #     img[ci+1][:len(targ)] += targ
    # plt.imshow(img)
    # plt.savefig(outfile.replace('.txt', '.png'))
    """
    4 options:
      0. Target is 0 and pred is 0 (true negative)  | purple
      1. Target is 0 and pred is 1 (false positive) | blue
      2. Target is 1 and pred is 0 (false negative) | green
      3. Target is 1 and pred is 1 (true positive)  | yellow
    """
    # width = 2
    # maxcolor = 4
    # img = np.zeros((len(clases)*width, maxl))
    # for ci in range(len(clases)):
    #     ti = ci*2
    #     clas = clases[ci]
    #     targ = targs[ci]
    #     diff = (clas-targ)
    #     for wi in range(width):
    #         # img[ti+wi][:len(clas)] += diff
    #         img[ti+wi][:len(clas)][targ==0] += clas[targ==0]
    #         img[ti+wi][:len(clas)][targ==1] += clas[targ==1] + 2
    # img = img / maxcolor
    # plt.imshow(img)
    # outfile = osp.join(outdir, 'order_test.png')
    # plt.savefig(outfile)

    """
    4 Rows:
      0. ground truth
      1. thresholded probability
      2. Preds v Truths (with color scheme from above)
      3. Confidences
    """
    width = 4
    maxcolor = 4
    # img = np.zeros((len(clases)*width, maxl))
    fig, axes = plt.subplots(min(5,len(clases)), sharex=False, gridspec_kw={'hspace': 1})
    for ci in range(len(axes)):
        img = np.zeros((4, maxl))
        clas = clases[ci]
        conf = confids[ci]
        targ = targs[ci]
        img[0][:len(targ)] += targ
        img[1][:len(conf)] += clas
        img[3][:len(conf)] += conf

        img[2][:len(clas)][targ==0] += clas[targ==0]
        img[2][:len(clas)][targ==1] += clas[targ==1] + 2
        img[2] /= maxcolor

        axes[ci].imshow(img)
        axes[ci].set_aspect(6)

    # img = img / maxcolor
    # plt.imshow(img)
    outfile = osp.join(outdir, 'order_info.png')
    plt.savefig(outfile)

    print('no classifications for {} files'.format(noclas))
    return float(cnt)/tot

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str,
        default="configs/autenc.json", help="path to config file")
    parser.add_argument("-v", "--verbose", default=False,
        help="if print all info")
    parser.add_argument("--checkpt", type=str, default=None,
        help="if continuing training from checkpoint model")
    parser.add_argument("--task", type=str, default='simple',
        help="way to eval: [simple, ordered, presacc]")
    parser.add_argument("--thresh", type=float, default=0.5,
        help="Threshold for confidence")
    parser.add_argument("--outdir", type=str, default="data",
        help="directory to store output evaluation data")
    opt = parser.parse_args()
    # print(opt)

    os.makedirs(opt.outdir, exist_ok=True)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    config = json.load(open(opt.config))

    trainer = AutencTrainer(config, device, training=False)

    trainer.load_model(opt.checkpt)
    trainer.model.eval()

    threshold = opt.thresh

    if opt.task == 'simple':

        res = trainer.evaluate(threshold)
        if config['classify']:
            loss, acc = res
    elif opt.task == 'ordered':
        # prints the predicted goals and targets in order for comparison
        order_eval(trainer, threshold, opt.outdir)
    elif opt.task == 'presacc':
        # creates a graph of precision vs accuracy
        threshs = np.arange(0.5, 1.0, 0.05)
        print(threshs)
        losses = [trainer.evaluate(threshold=thresh)[1] for thresh in threshs]
        plt.plot(threshs, losses)
        plt.xlabel('precision')
        plt.ylabel('accuracy')
        plt.savefig(osp.join(opt.outdir, 'latent_presicion_acc.png'))

if __name__ == '__main__':
    main()
