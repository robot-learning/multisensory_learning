from isaacRL.utils.data_utils import PickleDataset
from isaacRL.models.pretrain_model import PretrainModel
from isaacRL.trainers.trainer import Trainer
from isaacRL.utils.sac_loss import contrastive_loss

import os, sys, argparse, json
import numpy as np
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader


class TrainerModals(Trainer):
    """
    Holder of models and data for training and evaluating modality
    feature extraction method.
    """

    def __init__(self, config):
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        super(TrainerModals, self).__init__(config, device)

        self.start_epoch = 0
        self.end_epoch = self.config['training']['epochs']
        self.batch_size = self.config['training']['batch_size']
        self.eval_batch = self.config['training']['eval_batch']

        self.lr = self.config['training']['learning_rate']
        os.makedirs(self.config['training']['checkpoint_path'], exist_ok=True)
        # self.ckpt_interval = self.config['training']['checkpoint_interval']
        # self.eval_interval = self.config['training']['evaluation_interval']
        # self.ckpt_path = self.config['training']['checkpoint_path']
        # self.ckpt_name = self.config['training']['checkpoint_name']

        self.model = PretrainModel(config['model']).to(device)
        self.modalities = [key for key in self.config['model']['modalities']]

        self.rgb_optim = torch.optim.Adam(self.model.get_rgb_params(), lr=self.lr)
        self.autenc_optim = torch.optim.Adam(self.model.get_autenc_params(), lr=self.lr)

        self.criterion = contrastive_loss

        self.tloss = []
        self.vloss = []
        self.bestloss = float("inf")

        self.validloader = None

    def load_model(self, checkpt):
        checkpoint = torch.load(checkpt)
        # self.model.modalnet.load_state_dict(checkpoint['model'])
        self.model.load_params(checkpoint['prednet'])
        self.model.modalnet.load_params(checkpoint['modelnet'])
        if self.rgb_optim is not None:
            self.rgb_optim.load_state_dict(checkpoint['rgb_optim'])
            self.autenc_optim.load_state_dict(checkpoint['autenc_optim'])
        self.tloss = checkpoint['tloss']
        self.vloss = checkpoint['vloss']
        self.bestloss = checkpoint['bestloss']
        self.start_epoch = checkpoint['epochs']+1
        self.end_epoch += self.start_epoch

    def save_model(self, epoch):
        checkpoint = {}
        checkpoint['rgb_optim'] = self.rgb_optim.state_dict()
        checkpoint['autenc_optim'] = self.autenc_optim.state_dict()
        checkpoint['prednet'] = self.model.save_params()
        checkpoint['modelnet'] = self.model.modalnet.save_params()
        # checkpoint['prednet'] = self.model.state_dict()
        checkpoint['tloss'] = self.tloss
        checkpoint['vloss'] = self.vloss
        checkpoint['bestloss'] = self.bestloss
        checkpoint['epochs'] = epoch
        filename = os.path.join( self.config['training']['checkpoint_path'],
          self.config['training']['checkpoint_name'].format(epoch) )
        torch.save(checkpoint, filename)

    def train(self):

        trainset = PickleDataset(
          self.config['data']['train'], self.modalities, self.device,
          batch_size=self.config['training']['batch_size']
        )
        # trainloader = DataLoader(
        #     trainset,
        #     batch_size=self.config['training']['batch_size'],
        #     shuffle=True,
        #     num_workers=self.config['training']['n_cpu'],
        #     pin_memory=True,
        #     collate_fn=trainset.collate_fn
        # )

        for epoch in range(self.start_epoch, self.end_epoch):

            print('epoch:', epoch)

            lossHistory = []
            rgb_losses = []
            autenc_losses = []
            self.model.train()
            pbar = tqdm(total=len(trainset), file=sys.stdout)
            iorder = np.arange(len(trainset))
            np.random.shuffle(iorder)
            # for di, xdata in enumerate(trainset):
            for di in range(len(trainset)):
                xdata = trainset[iorder[di]]
                for key in xdata:
                    xdata[key] = xdata[key].to(self.device)

                self.rgb_optim.zero_grad()
                self.autenc_optim.zero_grad()
                preds, autencs = self.model(xdata)
                rgb_loss = self.criterion(xdata, preds)
                rgb_loss.backward()
                self.rgb_optim.step()
                autenc_loss = self.criterion(xdata, autencs)
                autenc_loss.backward()
                self.autenc_optim.step()

                totloss = np.mean([rgb_loss.item(), autenc_loss.item()])

                lossHistory.append(totloss)
                rgb_losses.append(rgb_loss.item())
                autenc_losses.append(autenc_loss.item())
                desc = f'  Iter {di}: loss={np.mean(rgb_losses):.4f},{np.mean(autenc_losses):.4f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            self.tloss.append(
              (np.mean(lossHistory), np.mean(rgb_losses), np.mean(autenc_losses))
            )

            if epoch % self.config['training']['evaluation_interval'] == 0:
                print('eval:')
                loss = self.evaluate()
                trainloss = self.tloss[-1]
                print('train loss:', trainloss[0])
                print('  rgb:', trainloss[1])
                print('  aut:', trainloss[2])
                print('eval loss:', loss[0])
                print('  rgb:', loss[1])
                print('  aut:', loss[2])
                self.vloss.append( loss )
                if loss[0] < self.bestloss:
                    self.bestloss = loss[0]
                    best_epoch = epoch
                    filen = os.path.join( self.config['training']['checkpoint_path'],
                      'best_epoch.txt' )
                    with open(filen, 'w') as f:
                        f.write(str(epoch))

            if epoch % self.config['training']['checkpoint_interval'] == 0:
                self.save_model(epoch)

    def evaluate(self):
        if self.validloader is None:
            validset = PickleDataset(
              self.config['data']['valid'], self.modalities, self.device,
              batch_size=self.config['training']['eval_batch']
            )
            self.validloader = validset
            # self.validloader = DataLoader(
            #     validset,
            #     batch_size=self.config['training']['eval_batch'],
            #     shuffle=False,
            #     num_workers=self.config['training']['n_cpu'],
            #     pin_memory=True,
            #     collate_fn=validset.collate_fn
            # )

        lossHistory = []
        rgb_losses = []
        autenc_losses = []
        self.model.eval()
        with torch.no_grad():
            pbar = tqdm(total=len(self.validloader), file=sys.stdout)
            iorder = np.arange(len(self.validloader))
            np.random.shuffle(iorder)
            for di in range(len(self.validloader)):
                xdata = self.validloader[iorder[di]]
                for key in xdata:
                    xdata[key] = xdata[key].to(self.device)

                preds, autencs = self.model(xdata)
                rgb_loss = self.criterion(xdata, preds)
                autenc_loss = self.criterion(xdata, autencs)

                totloss = np.mean([rgb_loss.item(), autenc_loss.item()])

                lossHistory.append(totloss)
                rgb_losses.append(rgb_loss.item())
                autenc_losses.append(autenc_loss.item())
                desc = f'  Iter {di}: loss={np.mean(rgb_losses):.4f},{np.mean(autenc_losses):.4f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
        return np.mean(lossHistory), np.mean(rgb_losses), np.mean(autenc_losses)
