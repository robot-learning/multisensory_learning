#!/usr/bin/env python
import os
import sys
import argparse
import numpy as np
import matplotlib.pyplot as plt

from ll4ma_util import file_util

from multisensory_learning.planning import CEMPlanner
from multisensory_learning.planning import Push2DProblem, CEMConfig
from multisensory_learning.models import MDN

import torch
from torch.distributions import MultivariateNormal


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--checkpoint', type=str, required=True)
    parser.add_argument('--start', type=float, nargs='+', default=[0.2, 0.2])
    parser.add_argument('--goal', type=float, nargs='+', default=[0.2, 0.8])
    parser.add_argument('--n_skills', type=int, default=1)
    parser.add_argument('--n_cem_samples', type=int, default=1000)
    parser.add_argument('--n_cem_elite', type=int, default=10)
    parser.add_argument('--n_cem_iters', type=int, default=10)
    # These are just for debug mode:
    parser.add_argument('--debug_model', action='store_true')
    parser.add_argument('--debug_push_dist', type=float, default=0.1)
    parser.add_argument('--debug_n_samples', type=int, default=100)
    parser.add_argument('--debug_end', type=float, nargs='+', default=[0.5, 0.2])

    
    args = parser.parse_args()

    file_util.check_path_exists(args.checkpoint)
    checkpoint = torch.load(args.checkpoint)

    mdn = MDN(7, 2, [128, 128], 1, 'relu', 'layer').double()
    mdn.load_state_dict(checkpoint['mdn'])
    mdn.eval()

    env = {
        'walls': [
            [(0, 0.5), (0.5, 0.5)],
            [(0, 0), (0, 1)],
            [(0, 1), (1, 1)],
            [(1, 1), (1, 0)],
            [(1, 0), (0, 0)]
        ],
        'buffer': 0.01,
        'min_distance': 0.05,
        'max_distance': 0.5
    }

    if args.debug_model:
        boundary_args = {'color': 'black', 'lw': 5}
        fig, ax = plt.subplots(1, 1)
        fig.set_size_inches(10, 10)

        for p1, p2 in env['walls']:
            # Plot seems to assume different order than is intuitive to me
            ax.plot([p1[0], p2[0]], [p1[1], p2[1]], **boundary_args)
        
        start_mean = np.array(args.start)
        start_std = 0.01
        start_cov = np.array([start_std**2, start_std**2])
        end = np.array(args.debug_end)
        diff = end - start_mean
        direct = diff / np.linalg.norm(diff)
        dist = np.array([args.debug_push_dist])
    
        mdn_input = torch.cat([torch.from_numpy(start_mean).unsqueeze(0),
                               torch.from_numpy(start_cov).flatten().unsqueeze(0),
                               torch.from_numpy(direct).unsqueeze(0),
                               torch.from_numpy(dist).unsqueeze(0)], dim=-1)
        alpha, mu, sigma_tril = mdn(mdn_input)
        mvn = MultivariateNormal(mu[:,:,0], scale_tril=sigma_tril[:,:,:,0])
        samples = mvn.sample((args.debug_n_samples,)).squeeze(1)

        ax.scatter(*start_mean, color='limegreen', s=200, zorder=100)
        for i in range(args.debug_n_samples):
            sample = samples[i].numpy()
            ax.scatter(*sample, color='blue', alpha=0.5)

        start_mvn = MultivariateNormal(torch.from_numpy(start_mean),
                                       torch.diag_embed(torch.tensor(start_cov)))
        start_samples = start_mvn.sample((100,))
        for start_sample in start_samples:
            ax.scatter(*start_sample, color='green', alpha=0.5)

        plt.tight_layout()
        plt.show()
        sys.exit()


    cem_config = CEMConfig()
    cem_config.horizon = args.n_skills
    cem_config.n_candidates = args.n_cem_samples
    cem_config.n_elite = args.n_cem_elite
    cem_config.max_iters = args.n_cem_iters

    with torch.no_grad():
        goal = torch.from_numpy(np.array(args.goal)).to(cem_config.device)
        goal_mu = goal.repeat(args.n_cem_samples, 1)
        # TODO just hard-coding cov for a moment
        goal_cov = torch.eye(2).double() * 0.001
        goal_cov = goal_cov.repeat(args.n_cem_samples, 1, 1).to(cem_config.device)
        goal_mvn = MultivariateNormal(goal_mu, goal_cov)

        problem = Push2DProblem(goal_mvn, mdn)
        planner = CEMPlanner(problem, cem_config)

        start_std = 0.01 # TODO hard-coding
        start_mu = torch.tensor(args.start).unsqueeze(0)
        start_cov = torch.tensor([start_std**2, start_std**2]).unsqueeze(0)
        current = torch.cat([start_mu, start_cov], dim=-1)
        current = current.to(cem_config.device)
        
        act = planner.get_action(current.clone(), return_full_horizon=True)
        # Normalize push direction in action
        act[:,:2] /= torch.linalg.norm(act[:,:2], dim=-1).unsqueeze(-1)
    
        boundary_args = {'color': 'black', 'lw': 5}
        fig, ax = plt.subplots(1, 1)
        fig.set_size_inches(10, 10)
        
        for p1, p2 in env['walls']:
            # Plot seems to assume different order than is intuitive to me
            ax.plot([p1[0], p2[0]], [p1[1], p2[1]], **boundary_args)
                
        ax.scatter(*np.array(args.start), color='limegreen', s=200, zorder=100)
        ax.scatter(*np.array(args.goal), color='red', s=200, zorder=100)

        # goal_samples = goal_mvn.sample((100,))[:,0,:]
        # for goal_sample in goal_samples:
        #     ax.scatter(*goal_sample.cpu(), color='green')

        
        colors = ['blue', 'green', 'red', 'black']
        
        for i in range(len(act)):
            mdn_in = torch.cat([current, act[i].unsqueeze(0)], dim=-1)
            alpha, mu, sigma_tril = mdn(mdn_in)
            mu = mu[:,:,0]
            sigma = sigma_tril[:,:,:,0]
            mvn = MultivariateNormal(mu, scale_tril=sigma)
            samples = mvn.sample((100,)).squeeze(1)
            for j in range(len(samples)):
                sample = samples[j].cpu().numpy()
                ax.scatter(*sample, color=colors[i], alpha=0.5)
            current = torch.cat([mu, torch.diagonal(sigma, dim1=-2, dim2=-1)], dim=-1)
    
    plt.tight_layout()
    plt.show()
        
