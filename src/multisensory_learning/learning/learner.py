#!/usr/bin/env python
import os.path as osp
import sys
import argparse
import numpy as np
from tqdm import tqdm
from dataclasses import dataclass
from typing import List

from multisensory_learning.learning import PickleDataset

from ll4ma_util import torch_util, func_util

import torch
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.tensorboard import SummaryWriter
from torch.optim import Adam
from torch.optim.lr_scheduler import ReduceLROnPlateau


@dataclass
class LearnerConfig(func_util.Config):
    train_sample_dirs: List[str] = None
    val_sample_dir: str = ''
    checkpoint_dir: str = ''
    checkpoint: str = ''
    batch_size: int = 64
    device: str = 'cuda'
    learning_rate: float = 1e-3
    adam_eps: float = 1e-4
    grad_clip_norm: float = 100
    n_epochs: int = 100
    checkpoint_interval: int = 10
    loss_print_interval: int = 10  # Number of batches to process between terminal updates

    def __post_init__(self):
        if self.checkpoint:
            checkpoint = torch.load(self.checkpoint)
            self.from_dict(checkpoint['args'])

    def from_dict(self, dict_):
        # We skip these from dict loading since we want to take values that get passed in
        # on construction, i.e. we're typically loading from dict from a loaded torch
        # checkpoint, and we don't want to take the old CP and device values
        if 'checkpoint' in dict_:
            del dict_['checkpoint']
        if 'device' in dict_:
            del dict_['device']
        super().from_dict(dict_)


class Learner:

    def __init__(
            self,
            config,
            train_filenames=[],
            val_filenames=[],
            set_train=False,
            set_eval=False,
            device=None
    ):
        if set_train and set_eval:
            raise ValueError("Cannot set learner to both train and eval modes simultaneously")
        
        self.config = config
        self.train_filenames = train_filenames
        self.val_filenames = val_filenames
        
        self.device = self.config.device if device is None else device
        self.last_saved_checkpoint = ''
        
        self.checkpoint = None
        if self.config.checkpoint:
            self.set_checkpoint(self.config.checkpoint)

        self.models = None
        self.set_models()
        
        if set_train:
            self.set_train()
        if set_eval:
            self.set_eval()

    def set_models(self):
        if self.models is None:
            raise ValueError("No models have been created yet")
        if self.checkpoint:
            torch_util.load_state_dicts(self.models, self.checkpoint['models'])
        torch_util.move_models_to_device(self.models, self.device)

    def get_params(self):
        return torch_util.accumulate_parameters(self.models)

    def get_batch_size(self, batch):
        return next(iter(batch.values())).size(0)
    
    def get_checkpoint_filename(self, epoch, episode):
        raise NotImplementedError()

    def get_data_loaders(self, val_size=0.1, drop_last_batch=False, dataset_type=PickleDataset):
        if self.val_filenames:
            train_dataset = dataset_type(self.train_filenames)
            val_dataset = dataset_type(self.val_filenames)
            train_loader = DataLoader(train_dataset, self.config.batch_size, shuffle=True,
                                      num_workers=1, pin_memory=True, drop_last=drop_last_batch)
            val_loader = DataLoader(val_dataset, self.config.batch_size, num_workers=1,
                                    pin_memory=True, drop_last=drop_last_batch)
            n_train = len(self.train_filenames)
            n_val = len(self.val_filenames)
        else:
            # Make a validation set from the training data
            dataset = dataset_type(self.train_filenames)
            dataset_size = len(dataset)
            indices = list(range(dataset_size))
            split = int(np.floor(val_size * dataset_size))
            np.random.shuffle(indices)
            train_indices, val_indices = indices[split:], indices[:split]
            train_sampler = SubsetRandomSampler(train_indices)
            val_sampler = SubsetRandomSampler(val_indices)
            train_loader = DataLoader(dataset, self.config.batch_size, sampler=train_sampler,
                                      num_workers=1, pin_memory=True)
            val_loader = DataLoader(dataset, self.config.batch_size, sampler=val_sampler,
                                    num_workers=1, pin_memory=True)
            n_train = len(train_indices)
            n_val = len(val_indices)
        return train_loader, val_loader, n_train, n_val
        
    def apply_models(self, batch):
        raise NotImplementedError()

    def compute_loss(self, batch, outputs):
        raise NotImplementedError()
        
    def train(self, episode=None, val_size=0.1):
        self.params = self.get_params()
        self.optim = Adam(self.params, lr=self.config.learning_rate, eps=self.config.adam_eps)
        scheduler = ReduceLROnPlateau(self.optim, min_lr=1e-5, threshold=1e-5,
                                      cooldown=10, verbose=True)

        self.writer = SummaryWriter(log_dir=osp.join(self.config.checkpoint_dir, "tensorboard"))

        train_loader, val_loader, n_train, n_val = self.get_data_loaders(val_size)

        for epoch in range(1, self.config.n_epochs + 1):
            train_losses = self.compute_train_loss(train_loader, n_train, epoch)
            val_losses = self.compute_validation_loss(val_loader, n_val, epoch)
            
            self.update_tensorboard(train_losses, val_losses, epoch)

            if epoch % self.config.checkpoint_interval == 0:
                self.save_checkpoint(epoch, episode)

            total_val_loss = sum([np.mean(v) for v in val_losses.values()])
            scheduler.step(total_val_loss)
                
    def set_eval(self):
        torch_util.set_models_to_eval(self.models)

    def set_train(self):
        torch_util.set_models_to_train(self.models)

    def update_tensorboard(self, train_losses, val_losses, epoch):
        for k in train_losses.keys():
            self.writer.add_scalar(f'Loss/{k}/Train', train_losses[k], epoch)
            self.writer.add_scalar(f'Loss/{k}/Validation', np.mean(val_losses[k]), epoch)

    def set_checkpoint(self, filename):
        self.checkpoint = torch.load(filename)
        func_util.override_dataclass_with_args(self.config, self.checkpoint['args'])
        self.config.checkpoint = filename
            
    def save_checkpoint(self, epoch, episode=None):
        checkpoint = {
            'args': self.config.to_dict(),
            'optimizer': self.optim.state_dict(),
            'models': {k: v.state_dict() for k, v in self.models.items()}
        }
        checkpoint_filename = self.get_checkpoint_filename(epoch, episode)
        torch.save(checkpoint, checkpoint_filename)
        self.last_saved_checkpoint = checkpoint_filename

    def compute_train_loss(self, train_loader, n_samples, epoch):
        self.set_train()
        running_losses = {}
        epoch_losses = {}
        pbar_update_size = 0
        n_batches = 0
        total_batches = len(train_loader)

        pbar = tqdm(total=n_samples, file=sys.stdout)
        for batch_idx, batch in enumerate(train_loader):
            batch_size = self.get_batch_size(batch)
            torch_util.move_batch_to_device(batch, self.config.device)

            outputs = self.apply_models(batch)
            loss, loss_dict = self.compute_loss(batch, outputs)

            self.optim.zero_grad()
            loss.backward()
            torch.nn.utils.clip_grad_norm_(self.params, self.config.grad_clip_norm, norm_type=2)
            self.optim.step()
            
            for k, v in loss_dict.items():
                if k not in running_losses:
                    running_losses[k] = 0.
                running_losses[k] += v

            pbar_update_size += batch_size
            n_batches += 1

            if (batch_idx % self.config.loss_print_interval == self.config.loss_print_interval - 1
                or batch_idx == total_batches - 1):
                # Print the loss averaged over the sliding window and reset the values
                desc = f'  E{epoch}-T:'
                for k in running_losses.keys():
                    desc += f' {k}={running_losses[k]/n_batches:.6f},'
                    # Ignore last batch for reporting epoch loss since it may be very small
                    if batch_idx != total_batches - 1:
                        epoch_losses[k] = running_losses[k] / n_batches
                    running_losses[k] = 0.
                if desc.endswith(','):
                    desc = desc[:-1]
                    
                pbar.set_description(desc)
                pbar.update(pbar_update_size)
                pbar_update_size = 0
                n_batches = 0
        pbar.close()
        
        return epoch_losses
        
    def compute_validation_loss(self, val_loader, n_samples, epoch):
        val_losses = {}
        self.set_eval()

        pbar = tqdm(total=n_samples, file=sys.stdout)
        with torch.no_grad():
            for batch in val_loader:
                batch_size = self.get_batch_size(batch)
                torch_util.move_batch_to_device(batch, self.config.device)

                outputs = self.apply_models(batch)
                loss, loss_dict = self.compute_loss(batch, outputs)
                
                desc = f'  E{epoch}-V:'
                for k, v in loss_dict.items():
                    if k not in val_losses:
                        val_losses[k] = []
                    val_losses[k].append(v)
                    desc += f' {k}={np.mean(val_losses[k]):.6f},'
                if desc.endswith(','):
                    desc = desc[:-1]
                pbar.set_description(desc)
                pbar.update(batch_size)
        pbar.close()
                
        return val_losses
                
