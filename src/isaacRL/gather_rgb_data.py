
from isaacgym import gymtorch, gymapi
from isaacRL.train import *
from isaacRL.utils.grapher import EvalGrapher
import matplotlib.pyplot as plt

def main():
    config = {'environ': {}}
    opt, trainer = setup(config, eval=True)
    config = trainer.config
    trainer._use_eval_data = True
    if opt.use_ros:
        import rospy
        print('using ros')
        rospy.init_node('gather_rgb')

    if opt.checkpt is not None:
        trainer.load_model(opt.checkpt)

    full_times = 100
    max_steps = max(trainer.max_steps*full_times, 10) + 1#+(full_times-1)
    envir = trainer.envir

    filename = 'b_rgb_data/replay_{}.torch'
    save_config = {
        'state_idx': {},
        'states': [],
        # 'rgb': [],
        # 'depth': []
    }
    num_envs = trainer.envir.num_envs
    full_states = torch.zeros(
        (num_envs, trainer.envir.max_steps, trainer.envir.full_state_size),
        device='cpu'
    )
    env_idx = torch.arange(num_envs)
    full_idx = torch.zeros((num_envs,), dtype=torch.long)

    runi = 0
    envir.reset()
    for envstep in range(max_steps):
        state, reward, dones, info = envir.step()
        state, state_idx = state['fullstate'], state['state_idx']
        if envstep == 0:
            save_config['state_idx'] = state_idx
        full_states[env_idx,full_idx] = state.clone().cpu()
        full_idx += 1
        for idx in env_idx[dones>0]:
            save_config['states'] = full_states[idx,:full_idx[idx]]
            torch.save(save_config, filename.format(runi))
            runi += 1
        full_idx[dones>0] *= 0

        # rgb = state[:,state_idx['rgb']].reshape(
        #     num_envs,envir.img_width,envir.img_height,3
        # ).cpu().clone()
        # save_config['rgb'] += [img for img in rgb]
        # if 'depth' in state_idx:
        #     depth = state[:,state_idx['depth']].reshape(
        #         num_envs,envir.img_width,envir.img_height,3
        #     ).cpu().clone()
        #     save_config['depth'] += [img for img in depth]
        # torch.save(save_config, filename.format(runi))
        # state, state_idx = state['fullstate'], state['state_idx']
        # if 'rgb' in state_idx and envstep == 0:
        #     rgb = state[:,state_idx['rgb']]
        #     rgb = rgb.reshape(envir.num_envs,envir.img_width,envir.img_height,3)
        #     plt.imshow(rgb[0].cpu().numpy()/255.)
        #     plt.savefig('checkpoints/rgb_test_img.png')



    if opt.use_ros:
        import rospy
        rospy.is_shutdown()


if __name__ == '__main__':
    main()
