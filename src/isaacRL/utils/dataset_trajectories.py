
import os
import os.path as osp
import torch


class TrajectoryDataset:
    """docstring for TrajectoryDataset."""

    def __init__(self, folder_path, data_keys=['rgb'], label_keys=['ee_state'], train=True, preload=False, output_as_img=False, resize_img=False):
        self._folder_path = folder_path
        self._data_keys = data_keys
        self._label_keys = label_keys
        self._train = train
        self._preload = preload
        self._output_as_img = output_as_img
        self._resize_img = resize_img

        self._file_list = [
            osp.join(self._folder_path,filename)
                for filename in os.listdir(self._folder_path)
        ]
        example_file = torch.load(self._file_list[0])

        self._states_per_file = example_file['states'].shape[0]
        self._state_size = example_file['states'].shape[1]
        self._state_idx = example_file['state_idx']
        self._state_key_idx = torch.cat([
            self._state_idx[key] for key in self._data_keys
        ])
        if len(self._label_keys) > 1:
            self._label_key_idx = torch.cat([
                self._state_idx[key] for key in self._label_keys
            ])

        if self._preload:
            loaded_files = [
                torch.load(file) for file in self._file_list
            ]
            self._loaded_data = torch.stack([file['state'] for file in loaded_files])

    def __len__(self):
        return len(self._file_list)*self._states_per_file

    @property
    def state_size(self):
        return self._state_size

    def __getitem__(self, idx):
        file_idx = idx // self._states_per_file
        state_idx = idx % self._states_per_file
        if self._preload:
            data = self._loaded_data[file_idx,state_idx][self._state_key_idx]
        else:
            _file = torch.load(self._file_list[file_idx])
            data = _file['states'][state_idx][self._state_key_idx]
        return data


if __name__ == '__main__':
    dataset = TrajectoryDataset('a_rgb_data')
    sample = dataset[0]
    print(sample.shape)
