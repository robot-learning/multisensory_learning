
from isaacgym import gymtorch, gymapi

import numpy as np
import os
import torch
import rospy

from isaacRL.utils.utils import get_pinv
from isaacRL.models.actorcritics import get_rgb_network
from isaacgymenvs.utils.torch_jit_utils import *
from isaacgymenvs.tasks.base.vec_task import VecTask
from ll4ma_util import ros_util
from ll4ma_isaacgym.msg import IsaacGymState
from geometry_msgs.msg import Pose, TransformStamped
from tf2_msgs.msg import TFMessage
from sensor_msgs.msg import JointState

class GeneralIiwa(VecTask):

    def __init__(self,
            cfg,
            rl_device,
            sim_device,
            graphics_device_id,
            headless,
            virtual_screen_capture=False,
            force_render=True,
        ):
        self.checker = False
        self.num_steps = 0
        self.cfg = cfg
        self.randomize = self.cfg.get('randomize',0)
        self.robot = self.cfg['robot']
        self.num_robots = 1
        self.objects = self.cfg['objects']
        obj_names = self.objects.keys()
        self.env_obj_data = [{obj_name: {} for obj_name in obj_names}
                             for _ in range(self.cfg['env']['numEnvs'])]
        self.num_objects = len(self.objects)
        self.object_noise = 0.1
        self.obs_keys = self.cfg.get('obs_objs', ['ee_state', 'goal'])
        # list of obs keys that should have noise
        self.noise_keys = self.cfg.get('noisy_objs', {})
        # assert all([True if key in self.obs_keys else False for key in self.noise_keys])
        self.goalie = self.cfg['goalie']

        self.use_rgb = self.cfg.get('use_rgb', False)
        self.use_depth = self.cfg.get('use_depth', False)
        self.use_rgb_emb = self.cfg.get('use_rgb_emb', False)
        self.use_depth_emb = self.cfg.get('use_depth_emb', False)

        self.img_width = self.cfg.get('img_width', 64)
        self.img_height = self.cfg.get('img_height', 64)

        self.depth_min = self.cfg.get('depth_min', -3.)

        if self.use_rgb:
            self.rgb_net = self.cfg.get('rgb_net', None)
            if self.rgb_net is not None:
                self.rgb_net, self.rgb_emb_size = get_rgb_network()
                self.rgb_net.to(self.rl_device)

        if self.use_depth:
            self.depth_net = self.cfg.get('depth_net', None)
            if self.depth_net is not None:
                self.depth_net, self.depth_emb_size = get_depth_network()
                self.depth_net.to(self.rl_device)

        self.allow_resets = self.cfg.get('allow_resets', True)
        self.allow_state_updates = self.cfg.get('allow_state_updates', True)

        self.additive = self.cfg.get('additive_actions', True)
        self.cartesian = self.cfg.get('cartesian_control', False)
        self.vecolity_control = self.cfg.get('vecolity_control', False)
        self.inv_threshold = 0.00001

        self.ee_name = self.cfg.get('ee_name',
            self.robot.end_effector.get_link()#.replace('palm', 'pad')
        )
        print('end effector:', self.ee_name)

        self.max_episode_length = self.cfg['max_env_steps']

        # self.action_scale = self.cfg["env"]["actionScale"]
        self.action_scale = self.cfg.get('action_scale', self.cfg["env"]["actionScale"])

        self.dof_vel_scale = self.cfg["env"]["dofVelocityScale"]

        self.debug_viz = self.cfg["env"]["enableDebugVis"]

        self.up_axis = "z"
        self.up_axis_idx = 2

        self.distX_offset = 0.04
        self.dt = 1/60.

        self.ros_pub = None
        _publish_info = self.cfg['publish_ros'] if 'publish_ros' in self.cfg else False
        if _publish_info:
            self.ros_pub = rospy.Publisher('/geniiwa_state', IsaacGymState, queue_size=1)
            try:
                self.ros_pub.publish(IsaacGymState())
            except Exception as e:
                print('not publishing')
                self.ros_pub = None

            self.ros_pub_pose = rospy.Publisher('/geniiwa_ee_pose', Pose, queue_size=1)
            try:
                self.ros_pub_pose.publish(Pose())
                print('pose publishing')
            except Exception as e:
                print('not publishing')
                self.ros_pub_pose = None

        # self.cfg['rl_device'] = sim_device
        super().__init__(
            config=self.cfg,
            rl_device=rl_device,
            sim_device=sim_device,
            graphics_device_id=graphics_device_id,
            headless=headless,
            virtual_screen_capture=virtual_screen_capture,
            force_render=force_render,
        )

        self.full_presets = None
        if isinstance(self.randomize, str) and self.randomize != 'inf':
            self.add_preset(self.randomize)

        self.reset_num = 0
        self.setup_values()
        self.reset_idx(torch.arange(self.num_envs, device=self.device))
        self.goal_done_num = 0
        self.steps_i = 0

    def add_preset(self, preset):
        assert '.torch' in preset
        self.full_presets = torch.load(preset)
        self._preset_states = self.full_presets['states'].to(self.device)
        self._num_presets = self.full_presets['cur_fill']
        self._use_preset = False
        self._preset_idx = np.random.choice(
            np.arange(self._num_presets), size=(self.num_envs,), replace=False
        )
        self.randomize = 'preset'

    def runs(self):
        return self.reset_num

    def successes(self):
        return self.goal_done_num

    def refresh(self, refresh_state=True):
        self.gym.refresh_actor_root_state_tensor(self.sim)
        self.gym.refresh_dof_state_tensor(self.sim)
        self.gym.refresh_dof_force_tensor(self.sim)
        self.gym.refresh_rigid_body_state_tensor(self.sim)
        self.gym.refresh_jacobian_tensors(self.sim)
        if refresh_state:
            self.update_env_states((self.goal_updates>0).any())

    def reset_idx(self, env_ids):
        pos = self.robot_default_dof_pos.unsqueeze(0).clone()
        # pos = tensor_clamp(
        #   self.robot_default_dof_pos.unsqueeze(0) + 0.25 *
        #   (torch.rand((len(env_ids), self.num_robot_dofs), device=self.device) - 0.5),
        #   self.robot_dof_lower_limits, self.robot_dof_upper_limits
        # )

        # pos[:,self.n_arm_joints:] *= 0.
        self.robot_dof_pos[env_ids, :] = pos
        self.robot_dof_vel[env_ids, :] = torch.zeros_like(
            self.robot_dof_vel[env_ids]
        )
        self.robot_dof_targets[env_ids, :] = pos

        # reset blocks
        # importantly ignoring the table: roots[env_ids, 2:] only looks at the root_states of objects after the robot and table (assuming the table is first, which it should be or else a lot of things break). When selecting the global_indices, you also want to only grab the indices of objects after the robot and table.
        roots = self.root_state_tensor#.clone()
        if self.randomize == 0:
            object_resets = self.object_defaults[env_ids, 1:]
        elif self.randomize == 'inf':
            object_resets = torch.zeros_like(self.object_defaults[env_ids, 1:])
            object_resets[:] += self.object_bias[1:,:]
            object_resets[:] += self.object_ranges[1:,:] * torch.rand_like(object_resets)
        elif self.randomize == 'preset':
            if not self._use_preset:
                self._preset_idx = np.random.choice(np.arange(self._num_presets), size=env_ids.shape, replace=False)
            object_resets = self._preset_states[self._preset_idx]
            self._use_preset = False
        elif self.randomize > 0:
            object_resets = torch.zeros_like(self.object_defaults[env_ids, 1:])
            object_resets[:] += self.object_bias[1:,:]
            num_secs = self.randomize
            rangvals = torch.linspace(0,1,num_secs).to(self.device)
            idx = torch.randint(high=num_secs,size=(len(env_ids)*13,))
            rands = rangvals[idx].reshape(-1,1,13)
            object_resets[:] += self.object_ranges[1:,:] * rands
        else:
            print('Unknown randomization value:', self.randomize)
        self.object_resets[env_ids] = object_resets.clone()
        roots[env_ids, 2:] = object_resets

        # reset the object noise if episodic
        for ni, nkey in enumerate(self.noise_keys):
            if self.noise_keys[nkey].get('type', 'episodic') == 'episodic':
                self.object_noises[env_ids, ni, :] = torch.randn_like(
                    self.object_noises[env_ids, ni, :]
                )[:] * self.noise_keys[nkey]['noise']

        multi_env_ids_int32 = self.global_indices[env_ids, 2:].flatten()
        # self.gym.set_actor_root_state_tensor(
        #   self.sim, gymtorch.unwrap_tensor(roots.clone()),
        # )
        self.gym.set_actor_root_state_tensor_indexed(
            self.sim, gymtorch.unwrap_tensor(roots),
            gymtorch.unwrap_tensor(multi_env_ids_int32), len(multi_env_ids_int32)
        )

        multi_env_ids_int32 = self.global_indices[env_ids, :1].flatten()
        self.gym.set_dof_position_target_tensor_indexed(
            self.sim, gymtorch.unwrap_tensor(self.robot_dof_targets),
            gymtorch.unwrap_tensor(multi_env_ids_int32), len(multi_env_ids_int32)
        )
        self.gym.set_dof_state_tensor_indexed(
            self.sim, gymtorch.unwrap_tensor(self.dof_state),
            gymtorch.unwrap_tensor(multi_env_ids_int32), len(multi_env_ids_int32)
        )

        self.gym.simulate(self.sim)

        self.reset_num += self.reset_buf.sum().item()
        self.progress_buf[env_ids] *= 0
        self.reset_buf[env_ids] *= 0
        self.goal_dones[env_ids] *= 0
        self.goal_updates[env_ids] += 1

    def get_preset_states(self):
        state = [
            self.object_roots[:,obji:obji+1].clone()
                for obji in range(1, self.object_defaults.shape[1])
        ]
        state = torch.cat(state, dim=1)
        return state

    def compute_observations(self):

        self.obs_buf[:] = self.full2obs(self.fullstate)

        self.obs_dict['obs'] = self.obs_buf.to(self.rl_device)
        self.obs_dict['fullstate'] = self.fullstate
        self.obs_dict['state_idx'] = self.state_idx

        # if self.use_rgb:
        #     self.obs_dict['rgb'] = self.fullstate[:,self.state_idx['rgb']]
        #
        # if self.use_depth:
        #     self.obs_dict['depth'] = self.fullstate[:,self.state_idx['depth']]

        return self.obs_buf

    def full2obs(self, fullstate):
        obs_buf = [
            fullstate[:,self.state_idx[key]] for key in self.obs_keys
        ]
        obs_buf = torch.cat(obs_buf, dim=-1)[:]
        return obs_buf

    def compute_reward(self):
        rewards, resets, dist = self.goalie.reward(self.fullstate, self.state_idx, self.goals)
        self.rew_buf[:] = rewards[:].to(self.device)
        self.goal_dones[:] = resets[:]
        self.goal_done_num += self.goal_dones.sum().item()
        # if self.goal_done_num > 1:
        #     print(self.rew_buf[self.goal_dones > 0])
        #     import pdb; pdb.set_trace()

        self.update_resets((resets > 0).to(self.device))
        self.update_resets(self.progress_buf >= self.max_episode_length)

        self.extras['goal_dist'] = dist.clone()
        self.extras['rewards'] = self.rew_buf[:].clone()
        # self.extras['gdist_mean'] = self.goal_dist[:].mean()
        self.extras['reward_mean'] = self.rew_buf[:].mean()
        self.extras['goal_dones'] = self.goal_done_num / (self.reset_num + 0.001)

    def update_resets(self, reset):
        self.reset_buf[:] = torch.where(
          reset,
          torch.ones_like(self.reset_buf),
          self.reset_buf
        )

    def step(self, *args):
        if rospy.is_shutdown():
            return
        return super().step(*args)

    def cartesian2joints(self, cacts):
        inv_jac = get_pinv(self.ee_jacobian.to(cacts.device), self.inv_threshold)
        jacts = torch.bmm(inv_jac, cacts[:,:6].unsqueeze(-1)).squeeze(-1)
        jacts = torch.cat((jacts, cacts[:,6:]), 1)
        return jacts

    def add_actions(self, base_acts, actions):
        ajnts = self.n_arm_joints
        base_acts[:,ajnts:] += actions[:,ajnts:]
        base_acts[:,ajnts:] = self.robot.end_effector.update_action_joint_pos(
          base_acts[:,ajnts:], self.robot_dof_pos[:,ajnts:]
        )
        base_acts[:,:ajnts] += actions[:,:ajnts]
        return base_acts

    def pre_physics_step(self, actions):
        ajnts = self.n_arm_joints
        self.actions[:] = actions[:]
        if self.cartesian and self.additive:
            self.actions = self.cartesian2joints( self.actions )
        self.robot_dof_targets[:,ajnts:] = self.robot.end_effector.update_action_joint_pos(
            self.actions[:,ajnts:], self.robot_dof_pos[:,ajnts:]
        )
        if self.vecolity_control:
            self.robot_dof_targets[:,:ajnts] = (
              self.actions[:,:ajnts] * self.robot_dof_max_vel[:ajnts] * 0.5
            )
            self.gym.set_dof_velocity_target_tensor(
              self.sim, gymtorch.unwrap_tensor(self.robot_dof_targets)
            )
        else:
            if self.additive:
                scaled_actions = self.scale_action(self.actions)[:,:ajnts]
                self.robot_dof_targets[:,:ajnts] = (
                  self.robot_dof_pos[:,:ajnts] + scaled_actions
                )
            else:
                self.robot_dof_targets[:,:ajnts] = actions[:,:ajnts]
            self.robot_dof_targets[:] = torch.clamp(
              self.robot_dof_targets[:],
              self.robot_dof_lower_limits, self.robot_dof_upper_limits
            )
            self.gym.set_dof_position_target_tensor(
              self.sim, gymtorch.unwrap_tensor(self.robot_dof_targets)
            )

    def scale_action(self, actions):
        actions[:,:self.n_arm_joints] = actions[:,:self.n_arm_joints] * self.dt * self.action_scale
        return actions

    def same_actions(self):
        actions = super().zero_actions()
        actions[:,:self.n_arm_joints] = self.robot_dof_targets[:,:self.n_arm_joints].clone()
        actions[:,self.n_arm_joints] = 0.5
        return actions

    def post_physics_step(self):
        self.progress_buf += 1
        self.num_steps += 1
        envis = self.reset_buf.nonzero(as_tuple=False).squeeze(-1)
        if len(envis) > 0 and self.allow_resets:
            self.reset_idx(envis)
        self.refresh(self.allow_state_updates)
        if self.allow_state_updates:
            self.compute_observations()
            self.compute_reward()
        # self.publish_info()

        # if self.num_steps % 10000 == 0:
        #     self.goal_done_num = 0
        #     self.reset_num = 0

    def get_robot_ee(self, simple=False):
        return self.rigid_body_states[:, self.ee_handle][:, 0:7]

    def get_resets(self):
        return self.reset_buf.clone().to(self.rl_device)

    def get_env_states(self):
        return self.fullstate

    def scale_joint_position(self, joint_pos):
        if joint_pos.device == self.robot_dof_lower_limits_rl.device:
            return (
                2.0 * (joint_pos - self.robot_dof_lower_limits_rl[:joint_pos.shape[1]])
                / (self.robot_dof_upper_limits_rl[:joint_pos.shape[1]] - self.robot_dof_lower_limits_rl[:joint_pos.shape[1]])
                - 1.0
            )
        return (
            2.0 * (joint_pos - self.robot_dof_lower_limits[:joint_pos.shape[1]])
            / (self.robot_dof_upper_limits[:joint_pos.shape[1]] - self.robot_dof_lower_limits[:joint_pos.shape[1]])
            - 1.0
        )

    def update_env_states(self, get_goal=False):
        """
        Get values from the environment that we can use for learning
        """

        self.fullstate[:,self.state_idx['joint_position']] = \
            self.robot_dof_pos.clone().to(self.rl_device)
        self.fullstate[:,self.state_idx['joint_velocity']] = \
            self.robot_dof_vel.clone().to(self.rl_device)
        self.fullstate[:,self.state_idx['joint_force']] = \
            self.robot_dof_for.clone().to(self.rl_device)
        self.fullstate[:,self.state_idx['joint_pos_scaled']] = \
            self.scale_joint_position(self.robot_dof_pos).to(self.rl_device)
        self.fullstate[:,self.state_idx['joint_vel_scaled']] = (
            self.robot_dof_vel  * self.dof_vel_scale
        ).to(self.rl_device)
        self.fullstate[:,self.state_idx['prev_action']] = \
            self.actions.clone().to(self.rl_device)
        self.fullstate[:,self.state_idx['ee_state']] = \
            self.get_robot_ee().to(self.rl_device)
        self.fullstate[:,self.state_idx['arm_joints']] = self.n_arm_joints
        self.fullstate[:,self.state_idx['jacobian']] = \
            self.ee_jacobian.clone().flatten(1).to(self.rl_device)

        # obji = 0
        for obji, (obj_name, obj_cfg) in enumerate(self.objects.items()):
            self.fullstate[:,self.state_idx[obj_name]] = \
                self.object_roots[:,obji].clone().flatten(1)[:,:7].to(self.rl_device)
            self.fullstate[:,self.state_idx[obj_name+'_init']] = \
                self.object_resets.clone().flatten(1)[:,:7].to(self.rl_device)

        for ni, nkey in enumerate(self.noise_keys):
            if self.noise_keys[nkey].get('type', 'episodic') == 'episodic':
                self.fullstate[:,self.state_idx[nkey+'_noisy'][:7]] = (
                    self.fullstate[:,self.state_idx[nkey][:7]]
                    + self.object_noises[:,ni,:]
                )
                self.fullstate[:,self.state_idx[nkey+'_init_noisy'][:7]] = (
                    self.fullstate[:,self.state_idx[nkey+'_init'][:7]]
                    + self.object_noises[:,ni,:]
                )
            else:
                object_noise = torch.randn_like(
                    self.fullstate[:,self.state_idx[nkey+'_noisy'][:7]]
                ) * self.noise_keys[nkey]['noise']
                self.fullstate[:,self.state_idx[nkey+'_noisy'][:7]] = (
                    self.fullstate[:,self.state_idx[nkey][:7]]
                    + object_noise
                )
                self.fullstate[:,self.state_idx[nkey+'_init_noisy'][:7]] = (
                    self.fullstate[:,self.state_idx[nkey+'_init'][:7]]
                    + object_noise
                )
        if get_goal:
            envis = self.goal_updates.nonzero(as_tuple=False).squeeze(-1)
            self.goals[envis] = self.goalie.get_goal(
                self.fullstate, self.state_idx
            )[envis]
            self.goals_noisy[envis] = self.goalie.get_goal(
                self.fullstate, self.state_idx, noisy=True
            )[envis]
            self.goal_updates *= 0
        self.fullstate[:,self.state_idx['goal']] = self.goals.clone()
        self.fullstate[:,self.state_idx['goal_noisy']] = self.goals_noisy.clone()
        self.fullstate[:,self.state_idx['stategoal']] = self.goalie.get_current(
            self.fullstate, self.state_idx
        )

        if self.use_rgb or self.use_depth:
            self.gym.render_all_camera_sensors(self.sim)
            self.gym.start_access_image_tensors(self.sim)

            if self.use_rgb:
                rgb = torch.stack(self.rgb_tensors)[...,:3].clone() / 255
                # self.fullstate[:,self.state_idx['rgb']] = rgb.flatten(1)
                rgb_emb = self.rgb_net(rgb)
                self.fullstate[:,self.state_idx['rgb_emb']] = rgb_emb

            if self.use_depth:
                for envi in range(self.num_envs):
                    depth_img = self.gym.get_camera_image_gpu_tensor(
                        self.sim, self.envs[envi], self.sensor_handle, gymapi.IMAGE_DEPTH
                    )
                    depth_img = gymtorch.wrap_tensor(depth_img)
                    self.depth_ten[envi] = depth_img.flatten()
                self.depth_ten[self.depth_ten == -torch.inf] = 0.0  # Filter no-depth values
                self.depth_ten[self.depth_ten < self.depth_min] = self.depth_min  # Clamp to min value
                self.fullstate[:,self.state_idx['depth']] = self.depth_ten.clone()

            self.gym.end_access_image_tensors(self.sim)

    def get_objects(self):
        object_states = {}
        for i, obj_name in enumerate(self.objects):
            object_states[obj_name] = self.root_state_tensor[:,i+1].clone()
        return object_states

    @property
    def joint_names(self):
        return self.robot.joint_names

    def publish_info(self):
        if self.ros_pub is not None:
            ee_state = self.fullstate['ee_state'][0]
            ee_pose = Pose()
            ee_pose.position.x = ee_state[0]
            ee_pose.position.y = ee_state[1]
            ee_pose.position.z = ee_state[2]
            ee_pose.orientation.x = ee_state[3]
            ee_pose.orientation.y = ee_state[4]
            ee_pose.orientation.z = ee_state[5]
            ee_pose.orientation.w = ee_state[6]
            self.ros_pub_pose.publish(ee_pose)
            """
            run rqt_plot /geniiwa_ee_pose/position/x:y:z in a terminal to show
            the data given by this publisher
            """
            return

            state = IsaacGymState()
            for env_idx in range(self.num_envs):
                joint_state = JointState()
                joint_state.name = self.joint_names
                joint_state.position = self.fullstate['joint_position'][env_idx].squeeze().cpu().numpy().tolist()
                joint_state.velocity = self.fullstate['joint_velocity'][env_idx].squeeze().cpu().numpy().tolist()
                joint_state.effort = self.fullstate['joint_force'][env_idx].squeeze().cpu().numpy().tolist()
                state.joint_state.append(joint_state)

                if self.robot.has_end_effector():
                    ee_state = self.fullstate['ee_state'][env_idx]
                    ee_pose = Pose()
                    ee_pose.position.x = ee_state[0]
                    ee_pose.position.y = ee_state[1]
                    ee_pose.position.z = ee_state[2]
                    ee_pose.orientation.x = ee_state[3]
                    ee_pose.orientation.y = ee_state[4]
                    ee_pose.orientation.z = ee_state[5]
                    ee_pose.orientation.w = ee_state[6]
                    state.ee_pose.append(ee_pose)

                # rgb = self._get_rgb_img(self.envs[env_idx], self.sensor_config)
                # state.rgb.append(ros_util.rgb_to_msg(rgb))
                # depth = self._get_depth_img(self.envs[env_idx], self.sensor_config)
                # state.depth.append(ros_util.depth_to_msg(depth))

                tf_env = TFMessage()
                for obj_name in self.objects:
                    obj_state = self.fullstate[obj_name][env_idx]
                    tf = TransformStamped()
                    tf.header.frame_id = 'world'
                    tf.child_frame_id = obj_name
                    tf.transform.translation.x = obj_state[0]
                    tf.transform.translation.y = obj_state[1]
                    tf.transform.translation.z = obj_state[2]
                    tf.transform.rotation.x = obj_state[3]
                    tf.transform.rotation.y = obj_state[4]
                    tf.transform.rotation.z = obj_state[5]
                    tf.transform.rotation.w = obj_state[6]
                    tf_env.transforms.append(tf)
                state.tf.append(tf_env)
            self.ros_pub.publish(state)

    def create_sim(self):
        self.seg_id_dict = {}
        self.current_seg_id = 1
        self.sim_params.up_axis = gymapi.UP_AXIS_Z
        self.sim_params.gravity.x = 0
        self.sim_params.gravity.y = 0
        self.sim_params.gravity.z = -9.81
        self.sim = super().create_sim(
            self.device_id, self.graphics_device_id,
            self.physics_engine, self.sim_params
        )
        self._create_ground_plane()
        self._create_envs( self.num_envs,
            self.cfg["env"]['envSpacing'], int(np.sqrt(self.num_envs))
        )

    def _create_ground_plane(self):
        plane_params = gymapi.PlaneParams()
        plane_params.normal = gymapi.Vec3(0.0, 0.0, 1.0)
        self.gym.add_ground(self.sim, plane_params)

    def _create_assets(self):
        # setup object assets
        self.num_object_bodies = 0
        self.num_object_shapes = 0
        self.num_object_dofs = 0
        self.obj_assets = {}
        self.obj_rb_dict = {}
        self.object_bias = torch.zeros(
          (self.num_objects, 13), device=self.device
        )
        self.object_ranges = torch.zeros(
          (self.num_objects, 13), device=self.device
        )
        for oi, (obj_name, obj_cfg) in enumerate(self.objects.items()):
            asset_options = gymapi.AssetOptions()
            if obj_cfg.density is not None:
                asset_options.density = obj_cfg.density
            asset_options.fix_base_link = obj_cfg.fix_base_link
            if obj_cfg.object_type == 'box':
                asset = self.gym.create_box( self.sim, obj_cfg.extents[0],
                  obj_cfg.extents[1], obj_cfg.extents[2], asset_options
                )
            elif obj_cfg.object_type == 'urdf':
                asset_options.override_com = True
                asset_options.override_inertia = True
                # asset_root = os.path.expanduser(obj_cfg.asset_root)
                asset_root = ros_util.resolve_ros_package_path(obj_cfg.asset_root)
                asset = self.gym.load_asset(self.sim, asset_root, obj_cfg.asset_filename, asset_options)
            else:
                raise ValueError(f"Unknown object type for simulation: {obj_cfg.object_type}")
            self.obj_assets[obj_name] = asset
            self.obj_rb_dict[obj_name] = self.gym.get_asset_rigid_body_dict(asset)
            obj_cfg.bodies = self.gym.get_asset_rigid_body_count(asset)
            obj_cfg.shapes = self.gym.get_asset_rigid_shape_count(asset)
            obj_cfg.dofs = self.gym.get_asset_dof_count(asset)
            obj_cfg.dof_props = self.gym.get_asset_dof_properties(asset)
            for i in range(obj_cfg.dofs):
                obj_cfg.dof_props['damping'][i] = 10.0

            self.num_object_bodies += obj_cfg.bodies
            self.num_object_shapes += obj_cfg.shapes
            self.num_object_dofs += obj_cfg.dofs

            for i in range(len(obj_cfg.position)):
                self.object_bias[oi,i] = obj_cfg.position[i]
            if self.randomize != 0:
                if obj_cfg.position_ranges is not None:
                    pranges = obj_cfg.position_ranges
                    for i in range(len(pranges)):
                        self.object_ranges[oi,i] = (pranges[i][1] - pranges[i][0])
                        self.object_bias[oi,i] = pranges[i][0]
                elif obj_name != 'table':
                    self.object_bias[oi,:3] -= self.object_noise
                    self.object_ranges[oi,:3] += self.object_noise*2
            if obj_cfg.orientation is None:
                self.object_bias[oi,6] = 1
            else:
                for i in range(4):
                    self.object_bias[oi,i+3] = obj_cfg.orientation[i]
            # pose = self._get_start_pose(obj_cfg)
            # self.object_defaults[:,oi,:7] = pose

    def _create_robot_assets(self):

        asset_options = gymapi.AssetOptions()
        asset_options.armature = self.robot.config.armature
        asset_options.fix_base_link = self.robot.config.fix_base_link
        asset_options.disable_gravity = self.robot.config.disable_gravity
        asset_options.flip_visual_attachments = self.robot.config.flip_visual_attachments
        robot_asset = self.gym.load_asset(
          self.sim,
          self.cfg['sim_cfg'].sim.asset_root,
          self.robot.config.asset_filename,
          asset_options
        )

        self.robot_rb_dict = self.gym.get_asset_rigid_body_dict(robot_asset)
        self.num_robot_bodies = self.gym.get_asset_rigid_body_count(robot_asset)
        self.num_robot_shapes = self.gym.get_asset_rigid_shape_count(robot_asset)
        self.num_robot_dofs = self.gym.get_asset_dof_count(robot_asset)

        # set robot dof properties
        robot_dof_props = self.gym.get_asset_dof_properties(robot_asset)
        robot_dof_props["driveMode"].fill(gymapi.DOF_MODE_POS)

        self.robot_dof_lower_limits = torch.tensor(
          robot_dof_props['lower'][:self.num_robot_dofs], device=self.device
        )
        self.robot_dof_lower_limits_rl = self.robot_dof_lower_limits.to(self.rl_device)
        self.robot_dof_upper_limits = torch.tensor(
          robot_dof_props['upper'][:self.num_robot_dofs], device=self.device
        )
        self.robot_dof_upper_limits_rl = self.robot_dof_upper_limits.to(self.rl_device)
        self.robot_dof_max_vel = torch.tensor(
          robot_dof_props['velocity'][:self.num_robot_dofs], device=self.device
        ) # radians per second

        self.n_arm_joints = self.robot.arm.num_joints()
        n_ee_joints = self.robot.end_effector.num_joints()
        robot_dof_props["stiffness"][:self.n_arm_joints] = self.robot.arm.config.stiffness
        robot_dof_props["damping"][:self.n_arm_joints] = self.robot.arm.config.damping
        if self.robot.config.end_effector:
            robot_dof_props["stiffness"][self.n_arm_joints:] = self.robot.end_effector.config.stiffness
            robot_dof_props["damping"][self.n_arm_joints:] = self.robot.end_effector.config.damping

        self.robot_dof_speed_scales = torch.ones_like(
          self.robot_dof_lower_limits
        ) * 7.5 #* 0.3 # max velocity value
        self.robot_dof_speed_scales[self.n_arm_joints:] = 0.1
        robot_dof_props['effort'][self.n_arm_joints:] = 200

        robot_start_pose = gymapi.Transform()
        robot_start_pose.p = gymapi.Vec3(*self.robot.config.position)
        robot_start_pose.r = gymapi.Quat(*self.robot.config.orientation)

        return robot_asset, robot_dof_props, robot_start_pose

    def random_obj_color(self, rgb_color=None, form='tensor'):
        cr = np.random.uniform(0, 1) if rgb_color is None else rgb_color[0]
        cg = np.random.uniform(0, 1) if rgb_color is None else rgb_color[1]
        cb = np.random.uniform(0, 1) if rgb_color is None else rgb_color[2]
        return [cr, cg, cb]

    def _create_envs(self, num_envs, spacing, num_per_row):
        lower = gymapi.Vec3(-spacing, 0.0, -spacing)
        upper = gymapi.Vec3(spacing, spacing, spacing)

        # create assets
        self._create_assets()

        # load robot asset
        robot_asset, robot_dof_props, robot_start_pose = self._create_robot_assets()

        # compute aggregate size
        max_agg_bodies = self.num_robot_bodies + self.num_object_bodies
        max_agg_shapes = self.num_robot_shapes + self.num_object_shapes

        self.robots = []
        self.envs = []
        self.object_defaults = torch.zeros(
          (self.num_envs, self.num_objects, 13), device=self.device
        )
        self.object_defaults[:,:] += self.object_bias[:]
        self.object_defaults[:] += self.object_ranges[:] * 0.5
        self.object_noises = torch.zeros(
            (self.num_envs, len(self.noise_keys), 7), device=self.rl_device
        )

        self.rgb_tensors = []

        for envi in range(self.num_envs):
            # create env instance
            env_ptr = self.gym.create_env(
              self.sim, lower, upper, num_per_row
            )

            self.gym.begin_aggregate(
              env_ptr, max_agg_bodies, max_agg_shapes, True
            )

            robot_actor = self.gym.create_actor(
              env_ptr, robot_asset, robot_start_pose, self.robot.get_name(), envi, 2, 0
            )
            self.gym.enable_actor_dof_force_sensors(env_ptr, robot_actor)
            self.gym.set_actor_dof_properties(
              env_ptr, robot_actor, robot_dof_props
            )
            if envi == 0:
                self.robot.joint_names = self.gym.get_actor_dof_names(
                  env_ptr, robot_actor
                )

            # create object actors
            for oi, (obj_name, obj_cfg) in enumerate(self.objects.items()):
                if obj_name not in self.seg_id_dict:
                    self.seg_id_dict[obj_name] = self.current_seg_id
                    self.current_seg_id += 1
                obj_pose = gymapi.Transform()
                obj_pose.p = gymapi.Vec3(*self.object_defaults[envi,oi,:3])
                obj_pose.r = gymapi.Quat(*self.object_defaults[envi,oi,3:7])
                obj_actor = self.gym.create_actor(
                  env_ptr,
                  self.obj_assets[obj_name],
                  obj_pose,
                  obj_name,
                  envi,
                  0,
                  self.seg_id_dict[obj_name]
                )
                if obj_cfg.dofs > 0:
                    self.gym.set_actor_dof_properties(
                      env_ptr, obj_actor, obj_cfg.dof_props
                    )
                if obj_cfg.set_color:
                    color = self.random_obj_color(obj_cfg.rgb_color)
                    self.gym.set_rigid_body_color(
                        env_ptr, obj_actor, 0, gymapi.MESH_VISUAL_AND_COLLISION,
                        gymapi.Vec3(*color)
                    )
                    self.env_obj_data[envi][obj_name]['color'] = color

            self.robot_link_names = []
            for link_name, rb_idx in self.robot_rb_dict.items():
                # Semantic segmentation IDs
                if link_name not in self.seg_id_dict:
                    self.seg_id_dict[link_name] = self.current_seg_id
                    self.current_seg_id += 1
                self.gym.set_rigid_body_segmentation_id(
                  env_ptr, robot_actor, rb_idx, self.seg_id_dict[link_name]
                )
                self.robot.add_rb_index(link_name, rb_idx) # Track RB indices (e.g. for contact info)
                self.robot_link_names.append(link_name)

                self.robot.joint_names = self.gym.get_actor_dof_names(env_ptr, robot_actor)

            if self.robot.has_end_effector():
                ee_link = self.robot.end_effector.get_link()
                ee_idx = self.gym.find_actor_rigid_body_index(
                  env_ptr, robot_actor, ee_link, gymapi.DOMAIN_SIM
                )
                self.robot.end_effector.add_rb_index(ee_idx)

            self.gym.end_aggregate(env_ptr)

            if self.use_rgb or self.use_depth:
                sensor_props = gymapi.CameraProperties()
                sensor_props.enable_tensors = True
                sensor_props.width = self.img_width
                sensor_props.height = self.img_height
                sensor_props.horizontal_fov = 120
                sensor_handle = self.gym.create_camera_sensor(env_ptr, sensor_props)
                # front camera
                # sensor_origin = gymapi.Vec3(*[1.1, 0.0, 1.3])
                # sensor_target = gymapi.Vec3(*[0.0, 0.0, 0.4])
                # back camera
                sensor_origin = gymapi.Vec3(*[-.01, 0.6, 1.15])
                sensor_target = gymapi.Vec3(*[1.0, -.5, 0.45])
                self.gym.set_camera_location(sensor_handle, env_ptr, sensor_origin, sensor_target)
                self.sensor_handle = sensor_handle

                # Camera tensor
                rgb_tensor = self.gym.get_camera_image_gpu_tensor(
                    self.sim, env_ptr, sensor_handle, gymapi.IMAGE_COLOR
                )
                rgb_tensor_th = gymtorch.wrap_tensor(rgb_tensor)
                self.rgb_tensors.append(rgb_tensor_th)

            self.envs.append(env_ptr)
            self.robots.append(robot_actor)

        self.ee_handle = self.gym.find_actor_rigid_body_handle(
          env_ptr, robot_actor, self.ee_name
        )

    def set_viewer(self):
        """Create the viewer."""

        self.enable_viewer_sync = True
        self.viewer = None

        # if running with a viewer, set up keyboard shortcuts and camera
        if self.headless == False:
            # subscribe to keyboard shortcuts
            self.viewer = self.gym.create_viewer(
                self.sim, gymapi.CameraProperties())
            self.gym.subscribe_viewer_keyboard_event(
                self.viewer, gymapi.KEY_ESCAPE, "QUIT")
            self.gym.subscribe_viewer_keyboard_event(
                self.viewer, gymapi.KEY_V, "toggle_viewer_sync")

            # set the camera position based on up axis
            sim_params = self.gym.get_sim_params(self.sim)
            if sim_params.up_axis == gymapi.UP_AXIS_Z:
                cam_pos = gymapi.Vec3(4, 3, 2)
                cam_target = gymapi.Vec3(-4, -3, 0)
            else:
                cam_pos = gymapi.Vec3(20.0, 3.0, 25.0)
                cam_target = gymapi.Vec3(10.0, 0.0, 15.0)

            # Point viewer camera at middle env
            look_at_env = self.envs[0]
            if self.num_envs > 10:
                n_per_row = int(np.sqrt(self.num_envs))
                look_at_env = self.envs[self.num_envs // 2 + n_per_row // 2]
            self.gym.viewer_camera_look_at(
              self.viewer, look_at_env, cam_pos, cam_target
            )

    def setup_values(self):
        self.goal_dones = torch.zeros_like(self.reset_buf)
        self.goal_updates = torch.zeros_like(self.reset_buf)
        # self.goal_dist = torch.zeros_like(self.rew_buf)

        # get gym GPU state tensors
        actor_root_state_tensor = self.gym.acquire_actor_root_state_tensor(self.sim)
        dof_state_tensor = self.gym.acquire_dof_state_tensor(self.sim)
        _dof_force = self.gym.acquire_dof_force_tensor(self.sim)
        rigid_body_tensor = self.gym.acquire_rigid_body_state_tensor(self.sim)
        jacobians = self.gym.acquire_jacobian_tensor(self.sim, self.robot.get_name())

        self.refresh(False)

        # create some wrapper tensors for different slices
        self.robot_default_dof_pos = torch.zeros(self.num_robot_dofs, device=self.device)
        self.robot_default_dof_pos[:self.n_arm_joints] = \
            torch.tensor(self.robot.arm.get_default_joint_position())
        self.robot_default_dof_pos[self.n_arm_joints:] = \
            torch.tensor(self.robot.end_effector.get_default_joint_position())

        self.dof_state = gymtorch.wrap_tensor(dof_state_tensor)
        self.robot_dof_state = \
            self.dof_state.view(self.num_envs, -1, 2)[:, :self.num_robot_dofs]
        self.robot_dof_pos = self.robot_dof_state[..., 0]
        self.robot_dof_vel = self.robot_dof_state[..., 1]
        self.robot_dof_for = gymtorch.wrap_tensor(
            _dof_force
        ).view(self.num_envs, -1)

        self.rigid_body_states = gymtorch.wrap_tensor( rigid_body_tensor )
        self.root_state_tensor = gymtorch.wrap_tensor(
            actor_root_state_tensor
        ).view(self.num_envs, -1, 13)

        self.rigid_body_states = self.rigid_body_states.view(self.num_envs, -1, 13)

        self.object_state = self.rigid_body_states[:, self.num_robot_bodies:]
        self.object_roots = self.root_state_tensor[:,1:]

        self.num_dofs = self.gym.get_sim_dof_count(self.sim) // self.num_envs
        self.robot_dof_targets = torch.zeros(
            (self.num_envs, self.num_robot_dofs),
            dtype=torch.float, device=self.device
        )

        jacobians = gymtorch.wrap_tensor( jacobians )
        self.ee_jacobian = jacobians[:,self.ee_handle-1,:,:self.n_arm_joints]
        print("JAC", self.ee_jacobian.shape)

        self.actions = torch.zeros(
            (self.num_envs, self.num_acts), device=self.device
        )
        self.action_speed_scales = torch.ones_like(self.actions)
        if 'act_scale' in self.cfg:
            self.action_speed_scales *= torch.tensor(self.cfg['act_scale'], device=self.device)

        self.goals = torch.zeros( (self.num_envs, self.goalie.goal_size),
            dtype=torch.float, device=self.rl_device
        )
        self.goals_noisy = self.goals.clone()

        self.setup_fullstate()

        self.object_resets = self.root_state_tensor[:,2:].clone()

        # get the indices for all objects
        self.global_indices = torch.arange(
            self.num_envs * (self.num_robots + self.num_objects),
            dtype=torch.int32, device=self.device
        ).view(self.num_envs, -1)

    def setup_fullstate(self):
        self.state_idx = {}
        min_idx = 0
        max_idx = min_idx + self.robot_dof_pos.shape[1]
        self.state_idx['joint_position'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + self.robot_dof_vel.shape[1]
        self.state_idx['joint_velocity'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + self.robot_dof_for.shape[1]
        self.state_idx['joint_force'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + self.robot_dof_pos.shape[1]
        self.state_idx['joint_pos_scaled'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + self.robot_dof_vel.shape[1]
        self.state_idx['joint_vel_scaled'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + self.actions.shape[1]
        self.state_idx['prev_action'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + self.get_robot_ee().shape[1]
        self.state_idx['ee_state'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + 1
        self.state_idx['arm_joints'] = torch.arange(min_idx, max_idx)


        min_idx = max_idx
        # 6 * 7 = 42
        max_idx = min_idx + self.ee_jacobian.shape[1] * self.n_arm_joints
        self.state_idx['jacobian'] = torch.arange(min_idx, max_idx)

        space_dof = 7
        for obji, (obj_name, obj_cfg) in enumerate(self.objects.items()):
            min_idx = max_idx
            max_idx = min_idx + space_dof
            self.state_idx[obj_name+'_init'] = torch.arange(min_idx, max_idx)
            min_idx = max_idx
            max_idx = min_idx + space_dof
            self.state_idx[obj_name+'_noisy'] = torch.arange(min_idx, max_idx)
            min_idx = max_idx
            max_idx = min_idx + space_dof
            self.state_idx[obj_name+'_init_noisy'] = torch.arange(min_idx, max_idx)
            min_idx = max_idx
            max_idx = min_idx + space_dof
            self.state_idx[obj_name] = torch.arange(min_idx, max_idx)

        min_idx = max_idx
        max_idx = min_idx + self.goalie.goal_size
        self.state_idx['goal'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + self.goalie.goal_size
        self.state_idx['goal_noisy'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + self.goalie.goal_size
        self.state_idx['stategoal'] = torch.arange(min_idx, max_idx)

        if self.use_rgb:
            min_idx = max_idx
            # max_idx = min_idx + (self.img_width * self.img_height * 3)
            # self.state_idx['rgb'] = torch.arange(min_idx, max_idx)
            max_idx = min_idx + self.rgb_emb_size
            self.state_idx['rgb_emb'] = torch.arange(min_idx, max_idx)

        if self.use_depth:
            min_idx = max_idx
            # max_idx = min_idx + (self.img_width * self.img_height)
            # self.state_idx['depth'] = torch.arange(min_idx, max_idx)
            max_idx = min_idx + self.depth_emb_size
            self.state_idx['depth_emb'] = torch.arange(min_idx, max_idx)

        self.fullstate = torch.zeros((self.num_envs, max_idx), device=self.rl_device)
