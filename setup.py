from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup


setup_args = generate_distutils_setup(
    packages=[
        'mdn',
        'isaacRL',
        'multisensory_learning',
        'multisensory_learning.datasets',
        'multisensory_learning.learning',
        'multisensory_learning.models',
        'multisensory_learning.planning',
        'multisensory_learning.visualization',
    ],
    package_dir={'': 'src'}
)

setup(**setup_args)
