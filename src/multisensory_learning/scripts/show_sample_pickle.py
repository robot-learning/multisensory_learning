#!/usr/bin/env python
import pickle
import argparse
import numpy as np
import matplotlib.pyplot as plt

from ll4ma_isaac.util import file_util


def print_data(data, indent=''):
    if not isinstance(data, dict):
        raise ValueError("Something is wrong, was expecting dictionary as input")
    for k, v in data.items():
        if isinstance(v, np.ndarray):
            print(indent + k, v.shape)
        elif isinstance(v, dict):
            print(indent + k)
            print_data(v, indent + '    ')
        else:
            raise ValueError(f"Unknown datatype: {type(v)}")


def print_attrs(attrs, indent=''):
    if not isinstance(attrs, dict):
        raise ValueError("Something is wrong, was expecting dictionary as input")
    for k, v in attrs.items():
        if isinstance(v, dict):
            print(indent + k)
            print_attrs(v, indent + '    ')
        else:
        # elif isinstance(v, str) or isinstance(v, list):
            print(indent + k, v)
        # else:
        #     raise ValueError(f"Unknown datatype: {type(v)}")
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--pickle', type=str, required=True)
    args = parser.parse_args()

    file_util.check_path_exists(args.pickle, "Pickle file")

    with open(args.pickle, 'rb') as f:
        data = pickle.load(f)

    print("=================== DATA ===================")
    print_data(data)
