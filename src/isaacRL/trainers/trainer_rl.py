
from isaacRL.trainers.trainer import Trainer
from isaacRL.tasks import get_task

from isaacgymenvs.utils.utils import set_seed

import torch
import torch.nn as nn
import torch.nn.functional as F

import time
from tqdm import tqdm
import numpy as np
import os, sys, argparse, json
import os.path as osp
import rospy

class TrainerRL(Trainer):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        device = "cuda" if torch.cuda.is_available() else "cpu"
        super(TrainerRL, self).__init__(config, device)

        self.envir = get_task(self.config['environ'], self.device)
        self.expert = None

    @property
    def num_envs(self):
        return self.envir.num_envs

    def gather_init_data(self):
        data_file = self.config['training']['checkpoint_path'].replace('checkpoints', 'data')
        os.makedirs(data_file, exist_ok=True)
        data_file = data_file.strip('/')
        data_file = data_file + '.torch'

        act_num = (self.buffer.max_size // self.num_envs)+1
        state = self.envir.reset()

        if os.path.exists(data_file):
            self.buffer.load(data_file)
            return

        for ai in range(act_num):
            if rospy.is_shutdown():
                return
            if self.expert is not None:
                actions = self.expert.get_actions(state)
            else:
                actions, _ = self.actor(state)
            actions = actions.detach()
            nstate, rewards, terminals, _ = self.envir.step(actions)
            self.buffer.add(
                state, actions, nstate, rewards, terminals
            )
            state = nstate
            tot_rew += rewards.sum().item()

        self.buffer.save(data_file)
        return tot_rew

    def pretrain(self):
        self.gather_init_data()
        pbar = tqdm(total=self.pretrain_steps, file=sys.stdout)
        desc = f'Pretrain Step {0}'
        pbar.set_description(desc)
        q_avg = 0.
        pi_avg = 0.
        for stepi in range(self.pretrain_steps):
            if rospy.is_shutdown():
                return
            qloss, pi_loss = self.update_weights(stepi)
            q_avg += qloss
            pi_avg += pi_loss
            desc = f'Pretrain Step {stepi}: loss=q:{(q_avg/(stepi+1)):.3f},pi:{(pi_avg/(stepi+1)):.3f}'
            pbar.set_description(desc)
            pbar.update(1)
        pbar.close()

        self.save_model('pre_train')

    @property
    def max_steps(self):
        return self.envir.max_steps
