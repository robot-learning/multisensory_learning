
from isaacRL.tasks.sim_task import SimTask
from isaacRL.tasks.gym_tasks import GymTask

from isaacRL.trainers.trainer import Trainer
from isaacRL.models.ppo import OPPO
from rl_pytorch.ppo import RolloutStorage

import torch
import torch.nn as nn
import torch.nn.functional as F

import os, sys, argparse, json, random
import time
import os.path as osp
import numpy as np
from tqdm import tqdm
import rospy


class TrainerPPOP(Trainer):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        super(TrainerPPOP, self).__init__(config, device)
        torch.manual_seed(10)
        self.max_steps = self.config['environ']['max_env_steps']

        self.ckpt_interval = self.config['training']['checkpoint_interval']
        self.acts_per_interval = self.config['training']['acts_per_interval']
        self.updates_per_interval = self.config['training']['updates_per_interval']
        os.makedirs(self.config['training']['checkpoint_path'], exist_ok=True)

        self.clip_grad = None
        if 'grad_norm' in self.config['training']:
            self.clip_grad = lambda x : nn.utils.clip_grad_norm_(
              x, self.config['training']['grad_norm']
            )
        self.clip_range = 0.1
        if 'clip_range' in self.config['training']:
            self.clip_range = self.config['training']['clip_range']
        self.value_loss_coef = self.config['training']['value_loss_coef']
        self.entropy_coef = self.config['training']['ent_coef']
        self.gamma = self.config['training']['gamma']
        self.lam = self.config['training']['lam']

        self.use_clipped_value_loss = False

        self.vlosses = []
        self.slosses = []
        self.qstart = 0
        if 'qstart' in self.config['training']:
            self.qstart = self.config['training']['qstart']

        self.expert_interval = -1
        if 'expert_interval' in self.config['training']:
            self.expert_interval = self.config['training']['expert_interval']

        if self.config['environ']['type'] == 'arm':
            self.envir = SACTask(self.config['environ'], self.device)
        else:
            self.envir = GymTask(self.config['environ'], self.device)

        print( 'obs: {}, state: {}, act: {}'.format( self.envir.obs_space.shape,
          self.envir.state_space.shape, self.envir.act_space.shape ) )
        self.buffer = RolloutStorage(
          self.n_envs, self.acts_per_interval, self.envir.obs_space.shape,
          self.envir.state_space.shape, self.envir.act_space.shape, self.device
        )
        self.num_mini_batches = self.config['training']['num_mini_batches']

        self.model = OPPO( self.config['model'],
          self.envir.obs_space.shape, self.envir.act_space.shape
        ).to(self.device)

        weight_decay = 0 if 'weight_decay' not in self.config['training'] \
          else self.config['training']['weight_decay']
        self.optimizer = torch.optim.Adam( self.model.parameters(),
          lr=self.lr, weight_decay=weight_decay )

    def train(self):

        if self.config['training']['expert_demos'] > 0:
            self.expert_demos(self.config['training']['load_experts'])

        interv = self.config['training']['checkpoint_interval']

        self.model.train()
        self.envir.reset()
        for epoch in range(self.start_epoch, self.end_epoch):
            if rospy.is_shutdown():
                return

            rewards = []
            pbar = tqdm(total=interv, file=sys.stdout)
            for di in range(interv):
                if rospy.is_shutdown():
                    return

                start = time.time()
                if (epoch-1) >= self.config['training']['expert_epochs']:
                    rewards.append( self.play(epoch) )
                play_time = time.time() - start

                # learn from replays
                start = time.time()
                vloss, sloss = self.update_weights(epoch)
                learn_time = time.time() - start
                self.buffer.clear()
                self.vlosses.append( vloss )
                self.slosses.append( sloss )
                desc = f'  R {np.mean(rewards):.3f}: loss={vloss:.3f} time={play_time:.2f},{learn_time:.2f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()

            rounds = self.envir.num_envs * interv  * self.acts_per_interval * epoch
            print('saving after {} timesteps'.format(rounds))
            print('mean rew per step:', np.mean(rewards))
            self.save_model(epoch)


    def play(self, playi):
        with torch.no_grad():
            state = self.envir.get_env_states()
            tot_rewards = 0.
            for step in range(self.acts_per_interval):
                if rospy.is_shutdown():
                    return tot_rewards
                current_obs = state['dynamics'].to(self.device)
                current_states = state['state'].to(self.device)
                actions, actions_log_prob, values, mu, sigma = self.model.act(
                  current_obs, current_states
                )
                nstate, rews, dones = self.envir.step(actions)
                tot_rewards += rews.mean().item()
                self.buffer.add_transitions(
                  current_obs, current_states, actions, rews, dones, values,
                  actions_log_prob, mu, sigma
                )
                state = nstate
            current_obs = state['dynamics'].to(self.device)
            current_states = state['state'].to(self.device)
            _, _, last_values, _, _ = self.model.act(
              current_obs, current_states
            )
            self.buffer.compute_returns(last_values, self.gamma, self.lam)
            return tot_rewards/self.acts_per_interval


    def update_weights(self, update_i):
        mean_value_loss = 0
        mean_surrogate_loss = 0
        unvisited = True
        batch = self.buffer.mini_batch_generator(self.num_mini_batches)
        for epoch in range(self.updates_per_interval):
            if rospy.is_shutdown():
                return
            for indices in batch:
                if rospy.is_shutdown():
                    return

                obs_batch = self.buffer.observations.view(-1, *self.buffer.observations.size()[2:])[indices]
                states_batch = None
                actions_batch = self.buffer.actions.view(-1, self.buffer.actions.size(-1))[indices]
                target_values_batch = self.buffer.values.view(-1, 1)[indices]
                returns_batch = self.buffer.returns.view(-1, 1)[indices]
                old_actions_log_prob_batch = self.buffer.actions_log_prob.view(-1, 1)[indices]
                advantages_batch = self.buffer.advantages.view(-1, 1)[indices]
                old_mu_batch = self.buffer.mu.view(-1, self.buffer.actions.size(-1))[indices]
                old_sigma_batch = self.buffer.sigma.view(-1, self.buffer.actions.size(-1))[indices]

                actions_log_prob_batch, entropy_batch, value_batch, mu_batch, sigma_batch = self.model.evaluate(obs_batch,
                                                                                                                       states_batch,
                                                                                                                       actions_batch)

                # KL
                # if self.desired_kl != None and self.schedule == 'adaptive':
                #
                #     kl = torch.sum(
                #         sigma_batch - old_sigma_batch + (torch.square(old_sigma_batch.exp()) + torch.square(old_mu_batch - mu_batch)) / (2.0 * torch.square(sigma_batch.exp())) - 0.5, axis=-1)
                #     kl_mean = torch.mean(kl)
                #
                #     if kl_mean > self.desired_kl * 2.0:
                #         self.step_size = max(1e-5, self.step_size / 1.5)
                #     elif kl_mean < self.desired_kl / 2.0 and kl_mean > 0.0:
                #         self.step_size = min(1e-2, self.step_size * 1.5)
                #
                #     for param_group in self.optimizer.param_groups:
                #         param_group['lr'] = self.step_size

                # Surrogate loss
                ratio = torch.exp(actions_log_prob_batch - torch.squeeze(old_actions_log_prob_batch))
                surrogate = -torch.squeeze(advantages_batch) * ratio
                surrogate_clipped = -torch.squeeze(advantages_batch) * torch.clamp(ratio, 1.0 - self.clip_range,
                                                                                   1.0 + self.clip_range)
                surrogate_loss = torch.max(surrogate, surrogate_clipped).mean()

                # Value function loss
                if self.use_clipped_value_loss:
                    value_clipped = target_values_batch + (value_batch - target_values_batch).clamp(-self.clip_range,
                                                                                                    self.clip_range)
                    value_losses = (value_batch - returns_batch).pow(2)
                    value_losses_clipped = (value_clipped - returns_batch).pow(2)
                    value_loss = torch.max(value_losses, value_losses_clipped).mean()
                else:
                    value_loss = (returns_batch - value_batch).pow(2).mean()

                loss = surrogate_loss + self.value_loss_coef * value_loss - self.entropy_coef * entropy_batch.mean()
                if unvisited:
                    unvisited = False
                    print('value loss:', value_loss)

                # Gradient step
                self.optimizer.zero_grad()
                loss.backward()
                self.clip_grad(self.model.parameters())
                self.optimizer.step()

                mean_value_loss += value_loss.item()
                mean_surrogate_loss += surrogate_loss.item()
        num_updates = self.updates_per_interval * self.num_mini_batches
        mean_value_loss /= num_updates
        mean_surrogate_loss /= num_updates
        return mean_value_loss, mean_surrogate_loss

    @property
    def n_envs(self):
        return self.envir.num_envs

    def load_model(self, checkpt):
        checkpoint = torch.load(checkpt)
        self.model.load_state_dict(checkpoint['model'])
        if self.optimizer is not None:
            self.optimizer.load_state_dict(checkpoint['optimizer'])
        self.vlosses = checkpoint['vloss']
        self.slosses = checkpoint['sloss']
        # self.bestloss = checkpoint['bestloss']
        self.start_epoch = checkpoint['epochs']+1
        self.end_epoch += self.start_epoch

    def save_model(self, epoch):
        checkpoint = {}
        checkpoint['optimizer'] = self.optimizer.state_dict()
        checkpoint['model'] = self.model.state_dict()
        checkpoint['vloss'] = self.vlosses
        checkpoint['sloss'] = self.slosses
        # checkpoint['bestloss'] = self.bestloss
        checkpoint['epochs'] = epoch
        filename = os.path.join( self.config['training']['checkpoint_path'],
          self.config['training']['checkpoint_name'].format(epoch) )
        torch.save(checkpoint, filename)
