
from isaacRL.tasks.gym_task import GymTask
from isaacgymenvs.utils.utils import set_seed

from ll4ma_util import file_util

import os, sys, argparse, json, yaml
import rospy
import torch
import numpy as np
from tqdm import tqdm

from isaacRL.experts.picknplace import PicknplaceExpert
from isaacRL.testers.utils import *

def main():

    device = "cpu"

    # config = file_util.load_yaml('configs/tests/stack_traj.yaml')
    config = file_util.load_yaml('configs/picknplace/sac01.yaml')
    config['environ']['num_envs'] = 4
    config['environ']['headless'] = False
    config['environ']['device'] = device # -1 if random
    config['environ']['use_gpu'] = device == 'cuda'
    config['environ']['seed'] = -1 # if random
    config['environ']['seed'] = set_seed(
      config['environ']['seed'], torch_deterministic=False
    )
    config['environ']['act_range'] = config['model']['actor']['act_range']
    config['environ']['additive_actions'] = False
    config['environ']['allow_resets'] = False
    config['environ']['allow_state_updates'] = True
    config['environ']['max_env_steps'] = 1000
    max_env_steps = config['environ']['max_env_steps']

    print('using ros')
    rospy.init_node('test_moveit_ik')

    envir = GymTask(config['environ'])

    expert = PicknplaceExpert(
        action_size=config['environ']['num_acts'],
        target_object=config['environ']['goal']['object']
    )

    torch.set_printoptions(precision=3,sci_mode=False)

    for step in range(max_env_steps):
        if rospy.is_shutdown():
            return
        state = envir.reset()
        actions = expert.get_setup_actions(state)
        for action in actions:
            state, reward, dones, info = envir.step(action)
        if (reward>0).any():
            print('reward:', reward)
            print('dones:', dones)
        if (dones>0).any():
            print('reward:', reward)
            print('dones:', dones)
        # print(state['fullstate'][0,state['state_idx']['ee_state']])
        # print(state['fullstate'][0,state['state_idx']['joint_position']])
        actions = expert.get_actions(state)
        for action in actions:
            state, reward, dones, info = envir.step(action)
        if (reward>0).any():
            print('reward:', reward)
            print('dones:', dones)
        if (dones>0).any():
            print('reward:', reward)
            print('dones:', dones)
        # print(state['fullstate'][0,state['state_idx']['ee_state']])
        # print(state['fullstate'][0,state['state_idx']['joint_position']])
        # import pdb; pdb.set_trace()

if __name__ == '__main__':
    main()
