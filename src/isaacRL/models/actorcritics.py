
import time
import torch
import torch.nn as nn
from torch.distributions import Normal

from isaacRL.models.vit import vit_s16

from isaacRL.models.models import MVNModel, FCNet


class ActorCriticBase(nn.Module):
    def soft_update(self, tau, tarnet):
        for target_param, param in zip(tarnet.parameters(), self.parameters()):
            target_param.data.copy_(target_param.data * (1.0 - tau) + param.data * tau)
        return tarnet


class ActorBasic(ActorCriticBase):

    def __init__(self, insize, action_size, cfg={}, zero_start=False):
        super(ActorBasic, self).__init__()
        self.insize = insize
        self.action_size = action_size
        self.cfg = cfg

        self.action_scale = self.cfg.get('action_scale', 1.0)
        self.action_bias = self.cfg.get('action_bias', 0.0)
        self.epsilon = self.cfg.get('epsilon', 1e-5)

        self.model = FCNet(
            infeats=self.insize,
            outfeats=self.action_size,
            layers=self.cfg.get('layers', [100]*3),
            activation=self.cfg.get('activation', 'relu'),
            batchnorm=self.cfg.get('batchnorm', False)
        )

        self.noise_sig = self.cfg.get('noise_sig', 0.2)
        self.noise_min = self.cfg.get('noise_min', -0.5)
        self.noise_max = self.cfg.get('noise_max', 0.5)
        self.noise_clip = False

        self.forward = self.get_action

    def add_noise(self, action):
        noise = torch.randn_like(action) * self.noise_sig
        if self.noise_clip:
            noise = torch.clamp(noise, self.noise_min, self.noise_max)
        else:
            noise = noise * 0.5
        action = torch.clamp(action + noise, -1, 1)
        return action

    def copy(self, newnet=None):
        if newnet is None:
            newnet = ActorBasic(self.insize, self.action_size, self.cfg)
        for new_param, param in zip(newnet.parameters(), self.parameters()):
            new_param.data.copy_(param.data)
        return newnet

    def update_noise(self):
        pass

    def get_action(self, xdata, add_noise=True):
        action = self.model(xdata['obs'])
        # action = torch.tanh(action)
        action = torch.clamp(action, -1, 1)
        if add_noise:
            action = self.add_noise(action)
        return action

    def get_random_action(self, xdata, add_noise=True):
        obs = xdata['obs']
        action = torch.randn(
            (obs.shape[0], self.action_size), device=obs.device
        ) * self.noise_sig * 2
        action = torch.clamp(action, -1, 1)
        return action


class ActorMVN(ActorCriticBase):
    """docstring for ActorMVN."""

    def __init__(self, insize, action_size, cfg={}, zero_start=False):
        super(ActorMVN, self).__init__()
        self.insize = insize
        self.action_size = action_size
        self.cfg = cfg

        self.action_scale = cfg.get('action_scale', 1.0)
        self.action_bias = cfg.get('action_bias', 0.0)
        self.epsilon = cfg.get('epsilon', 1e-5)

        self.model = MVNModel(
            infeats=self.insize,
            outfeats=action_size,
            layers=cfg.get('layers', [100]*3),
            activation=cfg.get('activation', 'relu'),
            log_range=cfg.get('log_range', [-20, 2]),
            batchnorm=cfg.get('batchnorm', False)
        )

    def deterministic(self, xdata, kwargs={}):
        return self.get_actions(xdata, deterministic=True)

    def stochastic(self, xdata, kwargs={}):
        return self.get_actions(xdata, deterministic=False)

    def basic(self, xdata, kwargs={}):
        return self.model(xdata['obs'])

    def forward(self, xdata, kwargs={}):
        return self.stochastic(xdata)

    def get_actions(self, xdata, deterministic=False):
        mu, std = self.model(xdata['obs'])
        # std = std * 0.001
        normal = Normal(mu, std)
        # for reparameterization trick (mean + std * N(0,1))
        x_t = mu if deterministic else normal.rsample()
        y_t = torch.tanh(x_t)
        # y_t = x_t
        action = y_t * self.action_scale + self.action_bias
        log_prob = normal.log_prob(x_t)
        # Enforcing Action Bound
        log_prob -= torch.log(self.action_scale * (1 - y_t.pow(2)) + self.epsilon)
        # log_prob -= torch.log((1 - y_t.pow(2)) + self.epsilon)
        log_prob = log_prob.sum(1, keepdim=True)
        return action, log_prob


class CriticBasic(ActorCriticBase):
    """docstring for CriticBasic."""

    def __init__(self, insize, cfg={}, zero_start=False, kwargs={}):
        super(CriticBasic, self).__init__()
        self.insize = insize
        self.cfg = cfg

        self.model = FCNet(
            infeats=self.insize,
            outfeats=1,
            layers=cfg.get('layers', [100]*3),
            activation=cfg.get('activation', 'relu'),
            batchnorm=cfg.get('batchnorm', False)
        )

    def copy(self, newnet=None):
        if newnet is None:
            newnet = CriticBasic(self.insize, self.cfg)
        for new_param, param in zip(newnet.parameters(), self.parameters()):
            new_param.data.copy_(param.data)
        return newnet

    def forward(self, xdata, actions):
        feats = torch.cat((xdata['obs'], actions), dim=1)
        output = self.model(feats)
        return output


class ActorRGB(ActorMVN):
    """docstring for ActorRGB."""

    def __init__(self, insize, action_size, cfg={}, zero_start=False, img_size=(224, 224)):
        rgb_model, gap_dim = get_rgb_network()
        emb_dim = 128
        super(ActorRGB, self).__init__(insize+emb_dim, action_size, cfg, zero_start)
        self.insize = insize
        self.gap_dim = gap_dim
        self.rgb_model = rgb_model
        self.rgb_model.freeze()
        self.emb_dim = emb_dim
        self.img_size = img_size
        self.gap_layer = nn.Linear(gap_dim, emb_dim)

    def forward(self, xdata, print_time=False):
        xrgb = xdata['rgb']
        if xrgb.shape[1] > self.img_size[0]:
            xrgb = xrgb.reshape(-1, *self.img_size, 3)
            xrgb = xrgb.transpose(1, 3)
        if not print_time:
            rgb_feats = self.gap_layer( self.rgb_model(xrgb) )
            xfeats = torch.cat([rgb_feats, xdata['obs']], dim=1)
            return super().forward({'obs': xfeats})

        start_time = time.time()
        rgb_feats = self.rgb_model(xrgb)
        print('ViT:', time.time() - start_time)
        start_time = time.time()
        rgb_feats = self.gap_layer( rgb_feats )
        print('Gap layer:', time.time() - start_time)
        start_time = time.time()
        xfeats = torch.cat([rgb_feats, xdata['obs']], dim=1)
        outvals = super().forward({'obs': xfeats})
        print('Actor layer:', time.time() - start_time)
        return outvals


class CriticRGB(CriticBasic):
    """docstring for CriticRGB."""

    def __init__(self, insize, cfg={}, zero_start=False, img_size=(224, 224), kwargs={}):
        if 'ViT' in kwargs:
            rgb_model, gap_dim = kwargs['ViT'], kwargs['gap']
        else:
            rgb_model, gap_dim = get_rgb_network()
        emb_dim = 128
        super(CriticRGB, self).__init__(insize+emb_dim, cfg, zero_start)
        self.insize = insize
        self.gap_dim = gap_dim
        self.rgb_model = rgb_model
        if 'ViT' not in kwargs:
            self.rgb_model.freeze()
        self.emb_dim = emb_dim
        self.img_size = img_size
        self.gap_layer = nn.Linear(gap_dim, emb_dim)

    def forward(self, xdata, actions):
        xrgb = xdata['rgb']
        if xrgb.shape[1] > self.img_size[0]:
            xrgb = xrgb.reshape(-1, *self.img_size, 3)
            xrgb = xrgb.transpose(1, 3)
        rgb_feats = self.rgb_model(xrgb)
        rgb_feats = self.gap_layer( rgb_feats )
        xfeats = torch.cat([rgb_feats, xdata['obs']], dim=1)
        outvals = super().forward({'obs': xfeats}, actions)
        return outvals

    def copy(self, newnet=None):
        if newnet is None:
            newnet = CriticRGB(
                self.insize, self.cfg, img_size=self.img_size,
                kwargs={'ViT': self.rgb_model, 'gap':self.gap_dim}
            )
        for new_param, param in zip(newnet.parameters(), self.parameters()):
            new_param.data.copy_(param.data)
        return newnet


def get_rgb_network():
    rgb_model, gap_dim = vit_s16('pretrained/mae_pretrain_hoi_vit_small.pth')
    rgb_model.freeze()
    return rgb_model, gap_dim
