
from isaacgym.torch_utils import tf_vector
from ll4ma_isaac.util import task_util
from isaacRL.utils.utils import get_log_dist
from isaacRL.utils.pose_proposal import get_proposer
import torch
import numpy as np

from isaacRL.utils.goalies.base_goalie import Goalie

# @torch.jit.script
def liftShapeReward(
    ee_pose, obj_pose, grasp_pose, goal_pose, actions,
    ee_w, obj_w, rot_w, alpha, c_w,
    action_penalty_scale, threshold
):
    # type: (Tensor, Tensor, Tensor, Tensor, Tensor, float, float, float, float, float, float, float) -> Tuple[Tensor, Tensor, Tensor]
    bsize = ee_pose.shape[0] # batch size
    terminals = torch.zeros((bsize,), device=ee_pose.device, dtype=torch.long)
    amplifyer = torch.zeros((bsize,), device=ee_pose.device, dtype=torch.long)
    x_axis = torch.tensor([[1.,0.,0.]]*bsize, device=ee_pose.device)
    z_axis = torch.tensor([[0.,0.,1.]]*bsize, device=ee_pose.device)

    ee_diff = ee_pose[:,:3] - grasp_pose[:,:3]
    ee_dist = torch.norm(ee_diff, p=2, dim=-1)
    ee_reward = (1.0 / (1.0 + ee_dist ** 2))**2

    axis1 = tf_vector(ee_pose[:,3:7], x_axis)
    axis2 = -z_axis

    dot1 = torch.bmm(axis1.view(bsize, 1, 3), axis2.view(bsize, 3, 1)).squeeze(-1).squeeze(-1)  # alignment of forward axis for gripper
    rot_err = dot1.abs()
    rot_reward = (1.0 / (1.0 + rot_err))**2

    grasp_dist = ee_dist + rot_err

    # reward for getting object closer to goal pose
    obj_diff = obj_pose[:,:3] - goal_pose[:,:3]
    obj_dist = torch.norm(obj_diff, p=2, dim=-1)
    obj_reward = (1.0 / (1.0 + obj_dist ** 2))**2

    # TODO: add finger grasp reward

    action_penalty = torch.sum(actions ** 2, dim=-1)

    terminals[obj_dist < threshold] = 1
    amplifyer[obj_dist < (threshold*2)] = 1

    rewards = ee_w * ee_reward + rot_w * rot_reward + obj_w * obj_reward
    rewards[amplifyer == 1] *= c_w*0.2
    rewards[terminals == 1] *= c_w
    rewards = rewards - action_penalty_scale * action_penalty

    # return rewards, terminals, tot_dist
    return rewards, terminals, (ee_dist, rot_err)

# @torch.jit.script
def liftSparseReward(
    ee_pose, obj_pose, goal_pose, actions, threshold, arm_joints, action_penalty_scale
):
    # type: (Tensor, Tensor, Tensor, Tensor, float, float, float) -> Tuple[Tensor, Tensor, Tensor]
    rewards, terminals, ee_dist = liftShapeReward(
        ee_pose, obj_pose, goal_pose, actions, 0., 0., 0., 0., 0., 0., threshold
    )
    rewards = terminals.to(rewards.dtype)
    if action_penalty_scale > 0:
        action_penalty = torch.sum(actions[:,:arm_joints] ** 2, dim=-1)
        rewards = rewards - action_penalty_scale * action_penalty
    return rewards, terminals, dist


class LiftObject(Goalie):
    """docstring for LiftObject."""

    def __init__( self,
            obj_name, lift_offset=0.1,
            device='cpu', threshold=0.01, sparse=False,
            ee_w=1., obj_w=1., rot_w=1., alpha=1., c_w=10., act_penal=0.001
        ):
        goal_shape = 7
        super(LiftObject, self).__init__(threshold, goal_shape, device, sparse)
        self.obj_name = obj_name
        self._offset = offset if torch.is_tensor(offset) else torch.tensor(offset, device=self.device)
        self._lift_offset = lift_offset
        self.sparse = sparse
        self.ee_w = ee_w
        self.obj_w = obj_w
        self.rot_w = rot_w
        self.alpha = alpha
        self.c_w = c_w
        self.act_penal = act_penal

    def goal_dist(self, state, state_keys, goal):
        return state[:,state_keys[self.obj_name]][:,:self.goal_shape] - goal

    def get_current(self, state, state_keys):
        return state[:,state_keys[self.obj_name]][:,:self.goal_shape].clone()

    def get_goal(self, state, state_keys, noisy=False):
        obj_name = self.obj_name.replace('_noisy', '') \
            if not noisy else self.obj_name
        goals = state[:,state_keys[obj_name]][:,:self.goal_shape].clone()
        goals[:,:3] += self._lift_offset[:3]
        return goals

    def reward(self, state, state_keys, goal):
        obj_name = self.obj_name.replace('_noisy', '')
        ee_pose = state[:,state_keys['ee_state']][:,:self.goal_shape]
        arm_joints = state[0,state_keys['arm_joints']]
        obj_pose = state[:,state_keys[obj_name]][:,:self.goal_shape]
        if self.sparse:
            return liftSparseReward(
                ee_pose, obj_pose, goal, self.threshold
            )
        return liftShapeReward(
            ee_pose, obj_pose, goal, state['prev_action'],
            self.ee_w, self.obj_w, self.rot_w, self.alpha, self.c_w, self.act_penal, self.threshold
        )

class LiftObject_Factory(object):
    """docstring for LiftObject_Factory."""

    def build(self,config):
        return LiftObject(
          config['object'],
          config['lift_offset'],
          device=config.get('device', 'cpu'),
          threshold=config.get('threshold', 0.01),
          sparse=True
        )

class LiftObjectShaped_Factory(object):
    """docstring for LiftObjectShaped_Factory."""

    def build(self,config):
        return LiftObject(
          config['object'],
          config['grasp_offset'],
          config['lift_offset'],
          device=config.get('device', 'cpu'),
          threshold=config.get('threshold', 0.01),
          sparse=False,
          ee_w=config.get('ee_w', 1.),
          obj_w=config.get('obj_w', 1.),
          rot_w=config.get('rot_w', 1.),
          alpha=config.get('alpha', 1.),
          c_w=config.get('c_w', 10.),
          act_penal=config.get('act_penal', 0.001),
        )
