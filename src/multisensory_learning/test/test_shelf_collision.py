#!/usr/bin/env python
import os
import sys
import numpy as np
import trimesh
from trimesh.scene import Scene
from trimesh.collision import CollisionManager

from ll4ma_robots_description.scene_generators import ShelfGenerator
from ll4ma_util import ros_util, vis_util


if __name__ == '__main__':

    mesh_fn = os.path.join(ros_util.get_path('ll4ma_isaacgym'),
                           "src", "ll4ma_isaacgym", "assets", "cleaner.stl")
    
    shelf = ShelfGenerator.create_mesh(
        width=0.5,
        height=1.0,
        depth=0.5,
        thickness=0.02,
        n_shelves=1,
        exclude_bottom_wall=True,
        return_component_meshes=True
    )

    obj = trimesh.load(mesh_fn)
    # obj = trimesh.creation.box([0.1]*3)

    obj_T = np.array([
        [1, 0, 0, 0.25],
        [0, 1, 0, 0.3],
        [0, 0, 1, 0.7],
        [0, 0, 0, 1]
    ])

    scene = Scene()
    for i, m in enumerate(shelf):
        scene.add_geometry(
            m,
            f'shelf_{i}',
            transform=np.array([
                [1, 0, 0, 0],
                [0, 1, 0, 0],
                [0, 0, 1, 0.753/2. + 1./2.],
                [0, 0, 0, 1]
            ])
        )
    scene.set_camera(angles=(np.pi/2. - 0.5, 0.0, np.pi/2.), distance=2.)
    scene.camera_transform[2,3] = 1.6
    
    manager, _ = trimesh.collision.scene_to_collision(scene)
    in_collision, names = manager.in_collision_single(
        mesh=obj,
        transform=obj_T,
        return_names=True
    )
    print("IN COLLISION", in_collision, "NAMES", names)
    
    obj.visual.face_colors = vis_util.get_color('red' if in_collision else 'green')
    scene.add_geometry(obj, 'object', transform=obj_T)
    scene.show()
