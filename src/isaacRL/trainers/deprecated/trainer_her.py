
from isaacRL.tasks.her_task import HERTask
from isaacRL.tasks.expert_behaviors import Expert
from isaacRL.trainers.trainer_goal import TrainerGoal

import os, sys, argparse, json, random
import os.path as osp
import numpy as np
from tqdm import tqdm
import rospy

import torch
import torch.nn as nn
import torch.nn.functional as F


class TrainerHER(TrainerGoal):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        super().__init__(config)
        self.envir = HERTask(self.config['environ'], self.device)

    def train(self):

        if self.config['training']['expert_demos'] > 0:
            self.expert_demos(self.config['training']['load_experts'])

        for epoch in range(self.start_epoch, self.end_epoch):

            if rospy.is_shutdown():
                return

            if self.expert_interval > 0 and (epoch % self.expert_interval) == 0:
                self.expert_demos(False)

            if (epoch-1) >= self.config['training']['expert_epochs']:
                self.play(epoch)

            # learn from replays
            qlosses, pi_losses = 0., 0.
            pbar = tqdm(total=self.updates_per_interval, file=sys.stdout)
            desc = f'  Learn Epoch {epoch}'
            pbar.set_description(desc)
            for ui in range(self.updates_per_interval):
                if rospy.is_shutdown():
                    return
                qloss, pi_loss = self.update_weights(ui)
                self.qlosses.append(qloss)
                self.pi_losses.append(pi_loss)
                desc = f'  Learn Epoch {epoch}: loss={np.mean(self.qlosses):.4f},{np.mean(self.pi_losses):.4f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()

            if epoch%self.ckpt_interval == 0:
                rounds = self.envir.num_envs * self.max_steps * epoch
                print('saving after {} timesteps'.format(rounds))
                mean_rew = tot_rewards / (self.envir.num_envs * self.max_steps)
                print('mean rew per step:', mean_rew)
                print('mean rewards:', self.buffer.mean_rewards())
                self.save_model(epoch)
            if epoch%self.eval_interval == 0:
                self.evaluate()


    def play(self, playi):
        with torch.no_grad():
            self.model.eval()
            state = self.envir.reset()
            trans = []
            tot_rewards = 0.
            tot_terms = 0.000001
            pbar = tqdm(total=self.max_steps, file=sys.stdout)
            desc = f'  Play Epoch {playi}'
            pbar.set_description(desc)
            for step in range(self.max_steps):
                if rospy.is_shutdown():
                    return
                if random.uniform(0,1) < self.randact_prob:
                    actions = self.model.policy.random_action(
                      self.n_envs, self.device )
                else:
                    actions, _ = self.model.policy(
                      self.buffer.prep_state(state, self.device)
                    )
                actions = actions.detach()
                nstate, rewards, terminals = self.envir.step_all(actions)
                tot_rewards += rewards.sum().item()
                tot_terms += terminals.sum().item()
                self.buffer.add_transitions(
                  state, actions, nstate, rewards, terminals, state['goal']
                )
                if step == 0:
                    self.buffer.add_goals(state['goal'])
                trans.append( (state, actions, nstate) )
                if terminals.sum() > 0:
                    state = self.envir.reset_indices(terminals)
                else:
                    state = nstate
                desc = f'  Play R: {(tot_rewards):.1f} {(tot_terms):.0f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()

            fin_goals = self.envir.goalie.extract_current(state)
            self.buffer.add_goals(fin_goals)
            for state, actions, nstate in trans:
                rewards, terminals = self.envir.get_rewards(state, fin_goals)
                self.buffer.add_transitions(
                  state, actions, nstate, rewards, terminals, fin_goals
                )
                # save the transitions with other goals
                for _ in range(self.goal_samples):
                    # sample other goals
                    goals = self.buffer.sample_goals(actions.shape[0])
                    # get the rewards for these other goals
                    rewards, terminals = self.envir.get_rewards(state, goals)
                    self.buffer.add_transitions(
                      state, actions, nstate, rewards, terminals, goals
                    )

    def expert_demos(self, load_experts=True):
        demos = 0

        if load_experts:
            self.buffer.load(
              osp.join(osp.expanduser(self.config['environ']['sim']['data_root']),
                self.config['environ']['sim']['data_prefix'])
            )
            print('loaded experts')
            return

        expert = Expert(self.envir)
        while demos < self.config['training']['expert_demos']:
            print('resetting behaviors')
            tot_rewards = 0.
            tot_terms = 0.000001
            trans = []
            state = self.envir.reset()
            expert.reset()
            pbar = tqdm(total=self.max_steps, file=sys.stdout)
            desc = f'  Expert'
            pbar.set_description(desc)
            for step in range(self.max_steps):
                if rospy.is_shutdown():
                    return

                base_acts, exp_resets = expert.get_actions()
                actions = torch.zeros((len(base_acts), self.action_space))
                for ai, action in enumerate(base_acts):
                    if action is not None:
                        actions[ai] = action.get_state_tensor()
                actions[:, -1] = actions[:,-1] * 2 - 1
                nstate, rewards, terminals = self.envir.step_all(base_acts)
                # do or operation on terminals with exp_resets
                terminals[exp_resets] = 1
                # TODO: make whether to ignore terminal condition from
                # goal/rewards optional
                terminals[[not exp for exp in exp_resets]] = 0
                tot_rewards += rewards.sum().item()
                tot_terms += terminals.sum().item()
                self.buffer.add_transitions(
                  state, actions, nstate, rewards, terminals, state['goal']
                )
                if step == 0:
                    self.buffer.add_goals(state['goal'])
                trans.append( (state, actions, nstate) )
                state = nstate
                if all(exp_resets):
                    break
                desc = f'  Expert R: {(tot_rewards):.1f} {(tot_terms):.0f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            fin_goals = self.envir.goalie.extract_current(state)
            self.buffer.add_goals(fin_goals)
            for state, actions, nstate in trans:
                rewards, terminals = self.envir.get_rewards(state, fin_goals)
                self.buffer.add_transitions(
                  state, actions, nstate, rewards, terminals, fin_goals
                )
                # save the transitions with other goals
                for _ in range(self.goal_samples):
                    # sample other goals
                    goals = self.buffer.sample_goals(actions.shape[0])
                    # get the rewards for these other goals
                    rewards, terminals = self.envir.get_rewards(state, goals)
                    self.buffer.add_transitions(
                      state, actions, nstate, rewards, terminals, goals
                    )
            demos += self.envir.num_envs
        # print('got experts')
        # print('total rewards:', self.buffer._buff['rewards'].sum())
        # print('total transitions:', len(self.buffer))
        # self.buffer.save(
        #   osp.join(osp.expanduser(self.config['environ']['sim']['data_root']),
        #     self.config['environ']['sim']['data_prefix'])
        # )
