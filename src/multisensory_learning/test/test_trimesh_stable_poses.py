#!/usr/bin/env python

import trimesh
import numpy as np

from ll4ma_util import vis_util


if __name__ == '__main__':

    mesh_fn = '/home/adam/catkin_ws/src/ll4ma_isaac/ll4ma_isaacgym/src/ll4ma_isaacgym/assets/cleaner.stl'


    
    m = trimesh.load(mesh_fn)
    m = trimesh.convex.convex_hull(m)

    n_poses = 3  # I know there's 3 for cleaner
    tfs, probs = m.compute_stable_poses()
    tfs = tfs[:n_poses]
    probs = probs[:n_poses]
    probs /= np.linalg.norm(probs)  # For coloring purposes
    colors = vis_util.random_colors(len(tfs), alpha=0.7)
        
    meshes = []
    for i, tf in enumerate(tfs):
        mesh = trimesh.load(mesh_fn)
        mesh.apply_transform(tf)
        color = colors[i]
        # color[-1] = probs[i]
        mesh.visual.face_colors = color

        meshes.append(mesh)
    trimesh.util.concatenate(meshes).show()
    
