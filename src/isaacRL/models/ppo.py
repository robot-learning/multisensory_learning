from isaacRL.models.pretrain_model import *

import os, sys
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Normal, MultivariateNormal

class OPPO(nn.Module):
    """docstring for OPPO."""

    def __init__(self, config, obs_shape, act_shape):
        super(OPPO, self).__init__()
        self.config = config

        # Policy
        self.actor = FCNet(
          infeats=obs_shape[0], outfeats=act_shape[0],
          layers=config['actor']['layers'],
          activation=config['actor']['activation']
        )

        # Value function
        self.critic = FCNet(
          infeats=obs_shape[0], outfeats=1,
          layers=config['critic']['layers'],
          activation=config['critic']['activation']
        )

        # Action noise
        if 'log_std' not in config['actor']:
            config['actor']['log_std'] = 1.0
        self.log_std = nn.Parameter(
          np.log(config['actor']['log_std']) * torch.ones(*act_shape)
        )

        # Initialize the weights like in stable baselines
        init_weights( self.actor.model,
          [np.sqrt(2), np.sqrt(2), np.sqrt(2), 0.01]
        )
        init_weights( self.critic.model,
          [np.sqrt(2), np.sqrt(2), np.sqrt(2), 1.0]
        )

    def forward(self):
        raise NotImplementedError

    def act(self, observations, states):
        actions_mean = self.actor(observations)

        covariance = torch.diag(self.log_std.exp() * self.log_std.exp())
        distribution = MultivariateNormal(actions_mean, scale_tril=covariance)

        actions = distribution.sample()
        actions_log_prob = distribution.log_prob(actions)

        value = self.critic(observations)

        return actions.detach(), actions_log_prob.detach(), value.detach(), actions_mean.detach(), self.log_std.repeat(actions_mean.shape[0], 1).detach()

    def act_inference(self, observations):
        return self.actor(observations)

    def evaluate(self, observations, states, actions):
        actions_mean = self.actor(observations)

        covariance = torch.diag(self.log_std.exp() * self.log_std.exp())
        distribution = MultivariateNormal(actions_mean, scale_tril=covariance)

        actions_log_prob = distribution.log_prob(actions)
        entropy = distribution.entropy()

        value = self.critic(observations)

        return actions_log_prob, entropy, value, actions_mean, self.log_std.repeat(actions_mean.shape[0], 1)


class PPO(nn.Module):
    """docstring for PPO."""

    def __init__(self, config, obs_shape, act_shape):
        super(PPO, self).__init__()
        self.config = config
        config['actor']['actions'] = act_shape

        # Policy
        self.policy = PPOActor(config)

        # Critic
        self.critic = PPOCritic(config)

        # Initialize the weights like in stable baselines
        # init_weights( self.policy.model.model,
        #   [np.sqrt(2), np.sqrt(2), np.sqrt(2), 0.01]
        # )
        # init_weights( self.critic.model.model,
        #   [np.sqrt(2), np.sqrt(2), np.sqrt(2), 1.0]
        # )

        self._actor_is_frozen = False

    def freeze_actor(self):
        for param in self.policy.parameters():
            param.requires_grad = False
        self._actor_is_frozen = True

    def unfreeze_actor(self):
        for param in self.policy.parameters():
            param.requires_grad = True
        self._actor_is_frozen = False

    def actor_is_frozen(self):
        return self._actor_is_frozen

    def act(self, observations):
        actions, actions_log_prob = self.policy.act(observations)

        value = self.critic(observations)

        return actions.detach(), actions_log_prob.detach(), value.detach()

    def act_inference(self, observations):
        obs = observations
        if not torch.is_tensor(observations):
            obs = observations['dynamics']
        return self.policy(observations)

    def evaluate(self, observations, actions):

        actions_log_prob, entropy = self.policy.evaluate(observations, actions)

        value = self.critic(observations)

        return actions_log_prob, entropy, value


class PPOActor(nn.Module):
    """docstring for PPOActor."""

    def __init__(self, config):
        super(PPOActor, self).__init__()
        self.config = config
        self.actions = config['actor']['actions']
        # [-3.051551103591919, 3.739427089691162]
        self.act_range = config['actor']['act_range']
        self.action_scale = (self.act_range[1] - self.act_range[0])/2
        self.action_bias = self.act_range[0] + self.action_scale
        self.epsilon = 1e-5

        self.rgb_net = None
        if 'rgb' in config['modalities']:
            self.rgb_net = get_modalnet( config['modalities']['rgb'], 'rgb' )
            if 'pretrained' in config and config['pretrained'] is not None:
                checkpoint = torch.load(config['pretrained'])
                self.rgb_net.load_state_dict(checkpoint['rgb'])

        self.state_feats = 0
        self.feat_mods = {}
        for mod in config['modalities']:
            if mod == 'rgb':
                self.state_feats += config['modalities'][mod]['outfeats']
            else:
                self.feat_mods[mod] = config['modalities'][mod]
                self.state_feats += self.feat_mods[mod]

        init_std = 0.1 if 'init_std' not in config['actor'] else config['actor']['init_std']
        # self.model = MVNModel( infeats=self.state_feats,
        #   outfeats=self.actions[0], layers=config['actor']['layers'],
        #   activation=config['actor']['activation'], log_range=[-20, np.log(init_std)]
        # )
        #
        # torch.nn.init.orthogonal_(self.model.mu.weight, gain=0.001)
        # self.model.mu.bias.data *= 0.01
        # torch.nn.init.orthogonal_(self.model.sig.weight, gain=np.log(init_std))
        # self.model.sig.bias.data -= 1.

        self.model = FCNet( infeats=self.state_feats,
          outfeats=self.actions[0], layers=config['actor']['layers'],
          activation=config['actor']['activation']
        )
        init_weights( self.model.model,
          [np.sqrt(2)]*len(config['actor']['layers']) + [0.001]
        )
        self.log_std = nn.Parameter(
          np.log(init_std) * torch.ones(*self.actions)
        )

    def forward(self, xdata):
        feats = [
          xdata[key].flatten(1)[:,:self.feat_mods[key]]
            for key in xdata if key in self.feat_mods
        ]
        feats = torch.cat( feats, 1 )

        if 'rgb' in xdata and self.rgb_net is not None:
            xrgb = self.rgb_net(xdata['rgb'])
            feats = torch.cat( (feats, xrgb.flatten(1)), 1 )
        actions = self.model(feats)
        # actions, log_std = self.model(feats)
        # actions = torch.tanh(actions)
        # actions = actions * self.action_scale + self.action_bias
        return actions, self.log_std
        # return actions, log_std

    def act(self, xdata):
        actions_mean, log_std = self(xdata)

        covariance = torch.diag(log_std.exp() * log_std.exp())
        distribution = MultivariateNormal(actions_mean, scale_tril=covariance)

        actions = distribution.sample()
        # actions = torch.clamp(actions, min=self.act_range[0], max=self.act_range[1])
        actions_log_prob = distribution.log_prob(actions)

        # normal = Normal(actions_mean, log_std)
        # actions = normal.rsample()  # for reparameterization trick (mean + std * N(0,1))
        # # y_t = torch.tanh(x_t)
        # # actions = torch.tanh(actions) * self.action_scale + self.action_bias
        # log_prob = normal.log_prob(actions)
        # # Enforcing Action Bound
        # # log_prob -= torch.log(self.action_scale * (1 - actions.pow(2)) + self.epsilon)
        # actions_log_prob = log_prob.sum(1)

        return actions, actions_log_prob

    def evaluate(self, xdata, actions):
        actions_mean, log_std = self(xdata)

        covariance = torch.diag(log_std.exp() * log_std.exp())
        distribution = MultivariateNormal(actions_mean, scale_tril=covariance)
        actions_log_prob = distribution.log_prob(actions)
        entropy = distribution.entropy()

        # normal = Normal(actions_mean, log_std)
        # log_prob = normal.log_prob(actions)
        # # Enforcing Action Bound
        # # log_prob -= torch.log(self.action_scale * (1 - actions.pow(2)) + self.epsilon)
        # actions_log_prob = log_prob.sum(1)
        # entropy = normal.entropy().mean(1)

        return actions_log_prob, entropy


class PPOCritic(nn.Module):
    """docstring for PPOCritic."""

    def __init__(self, config):
        super(PPOCritic, self).__init__()
        self.config = config
        actions = config['actor']['actions']

        self.rgb_net = None
        if 'rgb' in config['modalities']:
            self.rgb_net = get_modalnet( config['modalities']['rgb'], 'rgb' )
            if 'pretrained' in config and config['pretrained'] is not None:
                checkpoint = torch.load(config['pretrained'])
                self.rgb_net.load_state_dict(checkpoint['rgb'])

        self.state_feats = 0
        self.feat_mods = {}
        for mod in config['modalities']:
            if mod == 'rgb':
                self.state_feats += config['modalities'][mod]['outfeats']
            else:
                self.feat_mods[mod] = config['modalities'][mod]
                self.state_feats += self.feat_mods[mod]

        self.model = FCNet(
          infeats=self.state_feats, outfeats=1,
          layers=config['critic']['layers'],
          activation=config['critic']['activation']
        )

        init_weights( self.model.model,
          [np.sqrt(2)]*len(config['critic']['layers']) + [0.001]
        )

    def forward(self, xdata):
        feats = [
          xdata[key].flatten(1)[:,:self.feat_mods[key]]
            for key in xdata if key in self.feat_mods
        ]
        feats = torch.cat( feats, 1 )
        if 'rgb' in xdata and self.rgb_net is not None:
            xrgb = self.rgb_net(xdata['rgb'])
            feats = torch.cat( (feats, xrgb.flatten(1)), 1 )
        output = self.model(feats)
        return output


def init_weights(sequential, scales):
    [torch.nn.init.orthogonal_(module.weight, gain=scales[idx]) for idx, module in
     enumerate(mod for mod in sequential if isinstance(mod, nn.Linear))]
