import sys
import rospy
import os.path as osp
import numpy as np

import torch
from torch.distributions import MultivariateNormal

from ll4ma_opt.problems import Problem
from ll4ma_util import file_util, torch_util, math_util
from distribution_planning.distributions import kl_divergence

from multisensory_learning.models.loss import matrix_log_loss
from multisensory_learning.planning.collision_env import CollisionEnvironment


VALID_GOAL_COSTS = ['mean_pose_error', 'latent_l2', 'kl_iproj', 'kl_mproj', 'ce_iproj']


class SkillProblem(Problem):

    def __init__(self, skill_seq, config):
        self.min_bounds = None
        self.max_bounds = None
        
        super().__init__()
        self.skill_seq = skill_seq
        self.config = config
        self.device = skill_seq.device
        self.set_goal_cost_type(config.goal_cost_type)

        self.p0 = None  # Init distribution
        self.pG = None  # Goal distribution
        
        # Setup collision envs:
        self.collision_envs = {}
        for name, learner in self.skill_seq.learners.items():
            collision_env = CollisionEnvironment()
            collision_env.add_object(
                learner.config.object,
                learner.config.object_mesh_filename,
                bounding_box=True
            )
            collision_env.add_object(
                'ee',
                learner.config.ee_mesh_filename,
                bounding_box=True
            )
            self.collision_envs[name] = collision_env
        
    def set_goal_cost_type(self, cost):
        if cost not in VALID_GOAL_COSTS:
            rospy.logerr(f"Cost '{cost}' not one of valid costs: {VALID_GOAL_COSTS}")
            return False
        else:
            self.goal_cost_type = cost
            self.goal_weight = getattr(self.config, cost).goal_weight
            self.ee_collision_weight = getattr(self.config, cost).ee_collision_weight
            self.ee_distance_weight = getattr(self.config, cost).ee_distance_weight
            return True

    def set_skeleton(self, skeleton):
        if not self.skill_seq.set_skeleton(skeleton):
            return False
        self.min_bounds, self.max_bounds = self.skill_seq.get_min_max_bounds()
        return True

    def set_init_distribution(self, p0):
        self.p0 = p0
        
    def set_goal_distribution(self, pG):
        self.pG = pG
        
    def batch_cost(self, seq_params):
        rollout = self.skill_seq.rollout(
            self.p0,
            params_vec=seq_params.to(self.device),
            n_samples_to_propagate=self.config.n_samples_to_propagate
        )

        if seq_params.size(0) == 1:
            return torch.zeros(1)  # This is a hack to avoid cost computation at end of CEM

        cost = torch.zeros(rollout.batch_size).double()
        
        goal_cost = self._goal_cost(rollout)
        cost += goal_cost * self.goal_weight
        if self.config.use_ee_collision_cost:
            collision_cost = self._ee_collision_cost(rollout)
            cost += collision_cost * self.ee_collision_weight
        if self.config.use_ee_distance_cost:
            ee_distance_cost = self._ee_distance_cost(rollout)
            cost += ee_distance_cost * self.ee_distance_weight

        print("GOAL", goal_cost * self.goal_weight)
        if self.config.use_ee_collision_cost:
            print("COLLISION", collision_cost)
        if self.config.use_ee_distance_cost:
            print("DISTANCE", ee_distance_cost)
        
        return cost

    def _goal_cost(self, rollout):
        B = rollout.batch_size
        if self.goal_cost_type == 'mean_pose_error':
            cost = torch.zeros(B).double()
            goal_mean = self.pG.get_mean()            
            goal_pos = goal_mean[:,:3]
            goal_rot = torch_util.ortho6d_to_rotation(goal_mean[:,3:])
            mean = rollout.pT.get_mean()
            pos = mean[:,:3]
            rot = torch_util.ortho6d_to_rotation(mean[:,3:])
            cost += (goal_pos - pos).square().sum(dim=-1)
            cost += matrix_log_loss(goal_rot, rot)
        elif self.goal_cost_type == 'latent_l2':
            raise NotImplementedError("TODO need to update for new rollout")
            cost = torch.zeros(B).double()
            goal = torch.tensor(self.pG.get_mean()).unsqueeze(0).repeat(B, 1).float()
            ortho6d = self.skill_seq.nominal_learner.rot_to_ortho6d(node.rotation.flatten(1))
            pred = torch.cat([node.position, ortho6d], dim=-1)
            cost = (goal - pred).square().sum(dim=-1)


        elif self.goal_cost_type == 'ce_iproj':
            # TODO want to add an option here to compute a population-based CE just
            # from the samples you have from the rollout. That seems like it should
            # work and since you already have the samples from the rollout it's
            # motivated in a sense, because there's still no closed form soln for GMM

            samples = rollout.nodes[-1].samples_reshaped
            N, B, D = samples.size()
            pT = rollout.pT
            pG = self.pG
            cost = torch.sum(pT.pdf(samples) * -pG.log_pdf(samples), dim=0)
            
        elif self.goal_cost_type == 'kl_iproj':

            # # TODO I think this isn't working because of the sigma points, seems the SPs
            # # don't make sense for distributions over this parameterization with the
            # # ortho6d. I'm not sure why, since you can sample from the distribution and
            # # all samples look okay, but then as soon as you try the sigma points you
            # # can get some points that completely flip in rotation, and positions start
            # # varying in weird ways. I think it's mainly that the covariance is coupled
            # # in weird ways? 
            # cost = kl_divergence(rollout.pT, self.pG)

            # TODO this is a hack to work around the above problem, iterating over
            # goal distribution components and finding the min-cost between that
            # and the max-weighted component of the rollout. The idea is be greedy
            # for the most likely outcome
            rollout_component = rollout.pT.get_max_component()
            cost = torch.zeros(B, self.pG.n_components)
            for idx in range(self.pG.n_components):
                cost[:,idx] = kl_divergence(rollout_component, self.pG.get_component(idx))
            cost, _ = cost.min(dim=-1)
            
        elif self.goal_cost_type == 'kl_mproj':
            rollout_component = rollout.pT.get_max_component()
            goal_component = self.pG.get_max_component()
            cost = kl_divergence(goal_component, rollout_component)
        else:
            raise ValueError(f"Unknown goal cost type: {self.goal_cost_type}")
        return cost
            
    def _ee_collision_cost(self, rollout):
        """
        Collision cost for preventing EE from intersecting object mesh.
        """
        B = rollout.batch_size
        N = rollout.n_samples_to_propagate
        collision_cost = torch.zeros(B).double()

        for step_idx, node in enumerate(rollout.nodes):
            obj_T_ee = torch_util.pose_to_homogeneous(
                node.model_inputs['ee_start_pos_in_obj'],
                R=node.model_inputs['ee_start_rot_in_obj'].reshape(-1, 3, 3)
            )
            collision_cost += torch.tensor(
                self.collision_envs[node.skill].in_collision('ee', obj_T_ee)
            ).double().view(N, B).sum(dim=0)  # Batch cost is sum cost over samples
        return collision_cost

    def _ee_distance_cost(self, rollout):
        """
        Distance cost to prevent EE from straying too far from object initially.
        """
        B = rollout.batch_size
        N = rollout.n_samples_to_propagate
        ee_dist_cost = torch.zeros(N * B)
        for step_idx, node in enumerate(rollout.nodes):
            ee_dist_cost += node.model_inputs['ee_start_pos_in_obj'].norm(dim=-1)
        ee_dist_cost = ee_dist_cost.view(N, B).sum(dim=0)  # Batch cost is sum cost over samples
        return ee_dist_cost
                
    def size(self):
        if self.min_bounds is None:
            return 1  # This bogus but bounds will get overridden once skeleton is set
        else:
            return self.min_bounds.shape[0]

    def _get_height_offset_range(self):
        metadata_fn = osp.join(
            self.learner.checkpoint['args']['train_sample_dirs'][0],
            "metadata.yaml"
        )
        metadata = file_util.load_yaml(metadata_fn)
        # TODO probably want to make more robust:
        offset_range = metadata['task']['behavior']['behaviors'][0]['push_height_offset_range']
        return np.array([offset_range[0]]), np.array([offset_range[1]])


if __name__ == '__main__':
    from ll4ma_util import file_util
    from multisensory_learning.learning import SkillLearnerConfig, SkillLearner
    
    learner_cp_fn = '/media/data_haro/push_cleaner_cp_mdn_comp-2_256-256/checkpoint_100.pt'
    file_util.check_path_exists(learner_cp_fn)
    
    learner_config = SkillLearnerConfig(checkpoint=learner_cp_fn)
    learners = {'push': SkillLearner(learner_config, set_eval=True)}
    problem = SkillProblem(learners)

    B = 10
    problem.set_plan_skeleton(['push', 'push', 'push'])
    params1 = torch.full((B,11), 1.)
    params2 = torch.full((B,11), 2.)
    params3 = torch.full((B,11), 3.)
    params = torch.cat([params1, params2, params3], dim=-1)
    skeleton_params = problem.get_skeleton_params(params)
    for ps in skeleton_params:
        ps = ps.numpy()
        print("\n", ps.shape, "\n", ps)
    
    print("\nGOOD\n")
