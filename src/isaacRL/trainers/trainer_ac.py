
from isaacRL.trainers.trainer_rl import TrainerRL
from isaacRL.models.actorcritics import CriticBasic
from isaacRL.utils.buffer import BufferBasic, BufferHER, BufferFullstate

from isaacgymenvs.utils.utils import set_seed

import torch
import torch.nn.functional as F

import time
from tqdm import tqdm
import numpy as np
import os, sys
import os.path as osp
import rospy


class TrainerAC(TrainerRL):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):

        config['environ']['seed'] = 42 # -1 if random
        config['environ']['seed'] = set_seed(
            config['environ']['seed'], torch_deterministic=False
        )
        config['environ']['act_range'] = config['model']['actor']['act_range']

        super(TrainerAC, self).__init__(config)

        self.gamma = self.config['training']['gamma']
        self.tau = self.config['training']['tau']
        self.randact_prob = self.config['training'].get('randact_prob', 0.)

        self.target_update_interval = self.config['training'].get('target_update_interval', 1)
        self.policy_update_interval = self.config['training'].get('policy_update_interval', 2)
        self.ckpt_interval = self.config['training']['checkpoint_interval']
        self.save_best_after = self.config['training'].get('save_best_after', self.ckpt_interval)
        self.actions_per_interval = self.config['training']['acts_per_interval']
        self.updates_per_interval = self.config['training']['updates_per_interval']

        self.pretrain_zeros_epochs = self.config['training'].get('pretrain_zeros', 0)
        self.pretrain_zeros = self.pretrain_zeros_epochs > 0

        os.makedirs(self.config['training']['checkpoint_path'], exist_ok=True)

        self.observation_size = config['model']['observations']
        print('observ size:', self.observation_size)
        self.action_size = config['model']['actor']['actions']

        self.setup_actor()

        self.actor_update = True
        self.pretrain_critic = self.config['training'].get('pretrain_critic', 0)

        kwargs = {} if not hasattr(self.actor, 'rgb_model') \
            else {'ViT': self.actor.rgb_model, 'gap':self.actor.gap_dim}
        self.critic1 = CriticBasic(
            self.observation_size+self.action_size,
            config['model'].get('critic', {}),
            kwargs=kwargs
        )
        self.critic1.to(self.device)
        self.critic2 = CriticBasic(
            self.observation_size+self.action_size,
            config['model'].get('critic', {}),
            kwargs=kwargs
        )
        self.critic2.to(self.device)
        self.target_critic1 = self.critic1.copy()
        self.target_critic1.to(self.device)
        self.target_critic2 = self.critic2.copy()
        self.target_critic2.to(self.device)

        self.critic_optim1 = torch.optim.Adam(
            self.critic1.parameters(),  lr=self.lr, weight_decay=self.weight_decay
        )
        self.critic_optim2 = torch.optim.Adam(
            self.critic2.parameters(),  lr=self.lr, weight_decay=self.weight_decay
        )
        self.actor_optim = torch.optim.Adam(
            self.actor.parameters(),  lr=self.lr, weight_decay=self.weight_decay
        )

        self.q_losses = []
        self.pi_losses = []

        self._use_eval_data = True

        rgb_size = None #if not self.envir.use_rgb else \
            # (self.envir.img_width*self.envir.img_height*3)

        buf_size = self.config['training'].get('max_buff_size', self.max_steps*self.num_envs)

        self.pretrain_datafile = self.config['training'].get(
            'pretrain_datafile', None
        )
        if self.config['training'].get('her', False):
            max_goals = self.config['training'].get('max_goals', (2*self.num_envs))
            max_goals = max(max_goals, (2*self.num_envs))
            self.buffer = BufferHER(
                self.observation_size,
                self.action_size,
                self.envir.full_state_size,
                self.envir.state_idx,
                self.envir.full2obs,
                self.envir.goalie,
                self.num_envs,
                self.max_steps,
                device=self.device,
                storage_device=self.device,
                max_size=buf_size,
                max_goals=max_goals,
                goal_key=[key for key in self.envir.obs_keys if 'goal' in key][0]
            )
        else:
            self.buffer = BufferBasic(
                self.observation_size,
                self.action_size,
                device=self.device,
                storage_device=self.device,
                max_size=buf_size,
                rgb_size=rgb_size
            )

        self.state = None

        self._wupdate_calls = 0

        self._test_time = 0
        self._test_iter = 0

    def train_setup(self):
        # fill minimal replay buffer
        minsize = (self.batch_size//self.num_envs)+1
        minsize = max(minsize, min(self.max_steps, self.buffer.max_size//self.num_envs)+1)
        minsize = max(minsize, self.config['training'].get('init_buf_steps', 0))
        if self.pretrain_datafile is not None:
            print('pretrain data file:', self.pretrain_datafile)
            self.buffer.load(self.pretrain_datafile, self.envir.full2obs)
            print('buffer idx:', self.buffer.load_idx)
            print('buffer len:', len(self.buffer))
        else:
            pbar = tqdm(total=minsize, file=sys.stdout)
            desc = f'Filling buffer'
            pbar.set_description(desc)
            for iter in range(minsize):
                if rospy.is_shutdown():
                    return
                self.play(1)
                pbar.update(1)
            pbar.close()

    def train(self):

        self.runs = 0.
        self.success = 0
        tot_rewards = 0.
        print('resetting')
        self.actor.train()
        self.critic1.train()
        self.critic2.train()

        self.train_setup()

        print('mean rewards:', self.buffer.mean_rewards())
        runs = self.envir.runs()
        success = self.envir.successes()
        print('runs:', runs)
        print('successes:', success)

        self.state = self.envir.reset()
        if isinstance(self.buffer, BufferHER):
            self.buffer.reset_full_idx()

        if self.pretrain_critic > 0:
            self.actor_update = False

        self._wupdate_calls = max( 0,
            self.start_epoch * self.ckpt_interval * self.updates_per_interval - 1
        )

        num_epochs = self.end_epoch-self.start_epoch
        tot_iters = num_epochs * self.ckpt_interval
        for epoch in range(self.start_epoch, self.end_epoch):
            pbar = tqdm(total=self.ckpt_interval, file=sys.stdout)
            desc = f'Train E{epoch}'
            pbar.set_description(desc)
            tot_play_time = 0.
            tot_learn_time = 0.
            tot_time = 0.
            epoch_q_avg = 0.
            epoch_pi_avg = 0.
            for iter in range(self.ckpt_interval):
                start = time.time()
                if rospy.is_shutdown():
                    return

                play_time = time.time()
                # add to replays
                self.play(self.actions_per_interval)
                tot_play_time += time.time() - play_time
                # learn from replays
                q_avg, pi_avg = 0., 0.
                for ui in range(self.updates_per_interval):
                    learn_time = time.time()
                    if rospy.is_shutdown():
                        return
                    qloss, pi_loss = self.update_weights()
                    q_avg += qloss
                    pi_avg += pi_loss
                    tot_learn_time += time.time() - learn_time
                epoch_q_avg += q_avg / self.updates_per_interval
                epoch_pi_avg += pi_avg / self.updates_per_interval

                desc = f'Train E{epoch}: loss=q:{(epoch_q_avg/(iter+1)):.3f},pi:{(epoch_pi_avg/(iter+1)):.3f}'
                pbar.set_description(desc)
                pbar.update(1)
                tot_time += time.time() - start
            pbar.close()
            self.q_losses.append(epoch_q_avg/self.ckpt_interval)
            self.pi_losses.append(epoch_pi_avg/self.ckpt_interval)

            if self.pretrain_zeros and epoch >= self.pretrain_zeros_epochs:
                self.pretrain_zeros = False

            if self.pretrain_critic > 0 and epoch >= self.pretrain_critic:
                self.pretrain_critic = 0
                self.actor_update = True

            iters = (epoch*self.ckpt_interval)+self.start_epoch
            timesteps = self.num_envs * iters * self.actions_per_interval
            runs = timesteps / self.max_steps
            print('saving after:')
            print('{} epochs'.format(epoch))
            print('{} timesteps'.format(timesteps))

            print('mean rewards:', self.buffer.mean_rewards())
            print('buffer indx:', self.buffer.load_idx)
            print('buffer size:', len(self.buffer))

            self.print_info()

            runs = self.envir.runs()
            success = self.envir.successes()
            print('new runs:', runs - self.runs)
            print('new successes:', success - self.success)
            if hasattr(self.envir, 'trajopt_time'):
                print('avg trajopt time:', self.envir.trajopt_time())
            self.runs = runs
            self.success = success
            if epoch >= self.save_best_after:
                self.save_model(epoch)

    def print_info(self):
        pass

    def evaluate(self, num_steps, store_images=False):
        if self.envir.randomize == 'preset':
            filn = self.config['environ']['randomize'].replace('train', 'eval')
            if os.path.exists(filn) and self._use_eval_data:
                self.envir.add_preset(filn)
        self.state = self.envir.reset()
        self.actor.eval()
        self.critic1.eval()
        self.critic2.eval()
        tot_rew = self.play(num_steps, evaluate=True, store_images=store_images)
        mean_rew = tot_rew / (num_steps*self.num_envs)
        return tot_rew, mean_rew, self.envir.successes(), self.envir.runs()

    def get_actions(self, state, evaluate):
        pass

    def play(self, num_actions=1, evaluate=False, store_images=False):
        if self.state is None:
            self.state = self.envir.reset()
        if store_images:
            self.rgb_images = []
        with torch.no_grad():
            tot_rew = 0.
            if evaluate:
                pbar = tqdm(total=num_actions, file=sys.stdout)
                desc = f'Eval Iter {0}'
                pbar.set_description(desc)
            for ai in range(num_actions):
                if rospy.is_shutdown():
                    return
                actions = self.get_actions(self.state, evaluate)
                if self.pretrain_zeros:
                    actions *= 0.
                nstate, rewards, terminals, _ = self.envir.step(actions)
                if not evaluate:
                    self.buffer.add(
                        self.state, actions, nstate, rewards, terminals
                    )
                if store_images and 'full_state' in nstate:
                    rgb_key = [key for key in nstate['full_state'].keys() if 'rgb' in key and 'emb' not in key]
                    if len(rgb_key) == 0:
                        rgb = self.envir.get_rgb_img()
                        rgb = [img for img in rgb.values()][0][0]
                        # self.envir.get_rgb_img()['test_camera_rgb'][0]
                        self.rgb_images.append(rgb)
                    else:
                        rgb = nstate['full_state'][rgb_key]
                        self.rgb_images.append(rgb[0].reshape(224, 224, 3)*255)
                self.state = nstate
                tot_rew += rewards.sum().item()
                if evaluate:
                    success = self.envir.successes()
                    desc = f'Eval Iter {ai}: rew={(tot_rew/((ai+1)*self.num_envs)):.3f},suc={success:d}'
                    pbar.set_description(desc)
                    pbar.update(1)
            if evaluate:
                pbar.close()
            return tot_rew

    def update_weights(self):
        self._wupdate_calls += 1
        state, action, nstate, rewards, terminals = self.buffer.sample(self.batch_size)
        return self._update_weights(state, action, nstate, rewards, terminals)

    def load_model(self, checkpt):
        checkpoint = torch.load(checkpt)
        self.actor.load_state_dict(checkpoint['actor'])
        self.critic1.load_state_dict(checkpoint['critic1'])
        self.critic2.load_state_dict(checkpoint['critic2'])
        if self.critic_optim1 is not None:
            self.critic_optim1.load_state_dict(checkpoint['critic_optim1'])
            self.critic_optim2.load_state_dict(checkpoint['critic_optim2'])
            self.actor_optim.load_state_dict(checkpoint['actor_optim'])
        self.q_losses = checkpoint['qloss']
        self.pi_losses = checkpoint['pi_loss']
        # self.bestloss = checkpoint['bestloss']
        self.start_epoch = checkpoint['epochs']+1
        self.end_epoch += self.start_epoch
        return checkpoint

    def load_pretrained(self, checkpt):
        checkpoint = torch.load(checkpt)
        self.actor.load_state_dict(checkpoint['actor'])

    def save_model(self, epoch, checkpoint={}):
        checkpoint['critic_optim1'] = self.critic_optim1.state_dict()
        checkpoint['critic_optim2'] = self.critic_optim2.state_dict()
        checkpoint['actor_optim'] = self.actor_optim.state_dict()
        checkpoint['actor'] = self.actor.state_dict()
        checkpoint['critic1'] = self.critic1.state_dict()
        checkpoint['critic2'] = self.critic2.state_dict()
        checkpoint['qloss'] = self.q_losses
        checkpoint['pi_loss'] = self.pi_losses
        # checkpoint['bestloss'] = self.bestloss
        checkpoint['epochs'] = epoch
        filename = os.path.join(
            self.config['training']['checkpoint_path'],
            self.config['training']['checkpoint_name'].format(epoch)
        )
        torch.save(checkpoint, filename)
        print('saved to', filename)
