import os
import sys
import os.path as osp
import numpy as np
import torch
import random
from sklearn.mixture import GaussianMixture
from scipy.spatial.transform import Rotation

import trimesh
from trimesh.scene import Scene

from multisensory_learning.planning.collision_env import CollisionEnvironment
from multisensory_learning.learning import Ortho6dConfig, Ortho6dLearner
from multisensory_learning.util import learn_util

from ll4ma_util import ros_util, math_util, torch_util, vis_util, file_util, ui_util


class GoalDistributionGenerator:

    def __init__(
            self,
            ortho6d_checkpoint,
            obj_mesh_fn,
            display_mesh_fn=None,
            obj_name='object',
            make_convex=True,
            obj_color=vis_util.get_color('white'),
            args={}
    ):
        self.args = args
        self.collision_env = CollisionEnvironment()
        self.obj_name = obj_name
        self.collision_env.add_object(
            obj_name,
            obj_mesh_fn,
            make_convex=make_convex
        )
        self.display_mesh_fn = display_mesh_fn if display_mesh_fn else obj_mesh_fn
        self.display_mesh = trimesh.load(self.display_mesh_fn)
        self.obj_color = obj_color + [1.0]
        self.trans_obj_color = obj_color + [0.2]
        
        ortho6d_config = Ortho6dConfig(checkpoint=ortho6d_checkpoint)
        self.ortho6d_learner = Ortho6dLearner(ortho6d_config, set_eval=True, device='cpu')
        
    def add_env_object(
            self,
            name,
            mesh,
            transform=None,
            color=None,
            make_convex=False,
            bounding_box=False,
            add_as_collision=True,
    ):
        self.collision_env.add_object(
            name,
            mesh,
            transform,
            color,
            make_convex,
            bounding_box,
            add_as_collision
        )

    def show_env(self, tfs={}):
        self.collision_env.show(tfs)

    def get_scene(
            self,
            raise_view=1.6,
            look_down=0.5,
            distance=2.,
    ):
        scene = self.collision_env.scene.copy()
        # Setup the camera to have a good view
        scene.set_camera(
            angles=(np.pi/2. - look_down, 0.0, np.pi/2.),
            distance=distance
        )
        scene.camera_transform[2,3] = raise_view
        return scene
        
    def create_distribution(
            self,
            shelf,
            nominal_T=np.eye(4),
            n_support_surfaces=1,
            variance=0.01,
            n_samples=100,
            reject_collisions=[],
            show=False,
            save_dir='',
            n_gmm_components=2
    ):
        obj_mesh = self.collision_env.get_mesh(self.obj_name)
        ui_util.print_no_newline("Computing stable poses...")
        stable_poses, probs = obj_mesh.compute_stable_poses()
        ui_util.print_happy("DONE")
        stable_poses = stable_poses[:n_support_surfaces]

        nominal_T = torch_util.make_batch(nominal_T, n_samples).double()

        # Getting a padded bounding box for the shelf for containment checking
        shelf_bb = shelf.bounding_box
        bb_extents, _ = trimesh.bounds.to_extents(shelf_bb.bounds)
        bb_extents += 0.1  # TODO parameterize and tune
        shelf_bb = trimesh.creation.box(bb_extents)
        shelf_bb.apply_transform(self.collision_env.transforms['shelf_part_1'])  # All the same
        
        # Iterate over stable poses and generate samples for each one
        T_in_collision = []
        T_no_collision = []
        T_contained = []
        T_no_contained = []
        scene = self.get_scene()
        ui_util.print_no_newline("Generating collision-free samples...")
        for pose_idx, T_stable in enumerate(stable_poses):
            # Generate Gaussian-distributed random poses for this stable pose
            T_stable = torch_util.make_batch(T_stable, n_samples).double()
            pos, rot = math_util.gaussian_random_planar_poses(
                pos_std=[variance, variance, 0.0],
                n_samples=n_samples
            )
            T_delta = torch_util.pose_to_homogeneous(
                torch.tensor(pos).double(),
                R=torch.tensor(rot).double()
            ).double()

            # This is a little non-intuitive, but I think interpret it as first put the
            # object in its nominal pose, then randomly perturb it in the plane, then
            # put the object in its stable pose.
            T_sample = torch.bmm(torch.bmm(nominal_T, T_delta), T_stable)

            # Reject on collision with specified objects              
            in_collision = self.collision_env.in_collision(
                name=self.obj_name,
                transforms=T_sample,
                only_objects=reject_collisions
            )
            no_collision = torch.logical_not(torch.tensor(in_collision))
            T_in_collision.append(T_sample[in_collision])
            T_no_collision.append(T_sample[no_collision])

            # Find samples contained in bounding box of shelf
            T_contained_samples = []
            T_no_contained_samples = []
            for T in T_sample[no_collision]:
                mesh = self.collision_env.get_mesh(self.obj_name)
                mesh.apply_transform(T)
                # if all(shelf_bb.contains(mesh.vertices)):
                if all(shelf_bb.contains(mesh.bounding_box.vertices)):
                    T_contained_samples.append(T)
                else:
                    T_no_contained_samples.append(T)
            # It can happen that no samples are left after filtering (e.g. all are in
            # collision), so we'll just add null if there's nothing to append
            if T_contained_samples:
                T_contained.append(torch.stack(T_contained_samples))
            else:
                T_contained.append(None)
            if T_no_contained_samples:
                T_no_contained.append(torch.stack(T_no_contained_samples))
            else:
                T_no_contained.append(None)
        ui_util.print_happy("DONE")

        # Find component means and precisions to initialize the GMM with
        means = []
        precs = []
        weights = []
        for T_component in T_contained:
            if T_component is None:
                continue  # Skip ones for which all samples were filtered out
            vec_samples = learn_util.homogeneous_to_vec(T_component, self.ortho6d_learner).numpy()
            mean = np.mean(vec_samples, axis=0)
            cov = np.cov(vec_samples.T) + np.eye(len(mean)) * 1e-8  # Make PSD
            prec = np.linalg.inv(cov)
            means.append(mean)
            precs.append(prec)
            weights.append(float(len(vec_samples)))
        weights = np.array(weights)
        weights /= weights.sum()

        if all([T is None for T in T_contained]):
            ui_util.print_error_exit("\n  No samples are left for fitting GMM. They were "
                                     "likely all filtered out due to collisions or "
                                     "non-containment.\n")
        T_samples = torch.cat([T for T in T_contained if T is not None])

        ui_util.print_no_newline(f"Fitting {n_gmm_components}-component GMM to "
                                 f"{len(T_samples)} samples...")
        fit_samples = learn_util.homogeneous_to_vec(T_samples, self.ortho6d_learner)
        gmm = GaussianMixture(n_components=len(means))
        gmm.weights_init = weights
        gmm.means_init = np.array(means)
        gmm.precisions_init = np.array(precs)
        gmm.fit(fit_samples)
        ui_util.print_happy("DONE")
    
        if save_dir:
            save_data = {
                'weights': gmm.weights_,
                'means': gmm.means_,
                'covariances': gmm.covariances_,
                'args': vars(self.args)
            }
            save_fn = osp.join(save_dir, "gmm_params.pickle")
            file_util.save_pickle(save_data, save_fn)
            ui_util.print_info(f"    GMM params saved: {save_fn}\n")
        if show or save_dir:
            # self._vis_env(show, save_dir)
            # self._vis_all_samples(T_in_collision, T_no_collision, show, save_dir)
            # self._vis_in_collision_samples(T_in_collision, T_no_collision, show, save_dir)
            # self._vis_no_collision_samples(T_no_collision, T_in_collision, show, save_dir)
            # self._vis_contained_samples(
            #     T_contained,
            #     T_no_contained,
            #     T_in_collision,
            #     show,
            #     save_dir
            # )
            # self._vis_no_contained_samples(
            #     T_no_contained,
            #     T_contained,
            #     T_in_collision,
            #     show,
            #     save_dir
            # )
            self._vis_gmm_samples(gmm, show, save_dir)
        return gmm
             
    def _add_mesh_to_scene(self, scene, mesh, transform, color):
        m = mesh.copy()
        m.visual.face_colors = color
        scene.add_geometry(m, transform=transform)

    def _vis_env(self, show=False, save_dir=None):
        scene = self.get_scene()
        save_fn = osp.join(save_dir, 'environment.png') if save_dir else None
        self._render_scene(scene, show, save_fn)
        
    def _vis_all_samples(
            self,
            T_in_collision=None,
            T_no_collision=None,
            show=False,
            save_dir=None,
            color=None
    ):
        ui_util.print_no_newline("Rendering all samples...")
        scene = self.get_scene()
        color = color if color is not None else self.obj_color
        mesh = self.display_mesh.copy()
        n_support_surfaces = len(T_in_collision)
        for i in range(n_support_surfaces):
            if T_in_collision is not None:
                for T in T_in_collision[i]:
                    self._add_mesh_to_scene(scene, mesh, T, color)
            if T_no_collision is not None:
                for T in T_no_collision[i]:
                    self._add_mesh_to_scene(scene, mesh, T, color)
        save_fn = osp.join(save_dir, 'samples_all.png') if save_dir else None
        self._render_scene(scene, show, save_fn)
        
    def _vis_in_collision_samples(
            self,
            T_in_collision,
            T_no_collision,
            show=False,
            save_dir=None,
            in_collision_color=vis_util.get_color('tomato'),
            other_color=None
    ):
        ui_util.print_no_newline("Rendering in-collision samples...")
        scene = self.get_scene()
        mesh = self.display_mesh.copy()
        other_color = other_color if other_color is not None else self.trans_obj_color
        n_support_surfaces = len(T_in_collision)
        for i in range(n_support_surfaces):
            for T in T_in_collision[i]:
                self._add_mesh_to_scene(scene, mesh, T, in_collision_color)
            # for T in T_no_collision[i]:
            #     self._add_mesh_to_scene(scene, mesh, T, other_color)
        save_fn = osp.join(save_dir, 'samples_in_collision.png') if save_dir else None
        self._render_scene(scene, show, save_fn)

    def _vis_no_collision_samples(
            self,
            T_no_collision,
            T_in_collision,
            show=False,
            save_dir=None,
            no_collision_color=vis_util.get_color('gold'),
            other_color=None
    ):
        ui_util.print_no_newline("Rendering no-collision samples...")
        scene = self.get_scene()
        mesh = self.display_mesh.copy()
        other_color = other_color if other_color is not None else self.trans_obj_color
        n_support_surfaces = len(T_in_collision)
        for i in range(n_support_surfaces):
            for T in T_no_collision[i]:
                self._add_mesh_to_scene(scene, mesh, T, no_collision_color)
            # for T in T_in_collision[i]:
            #     self._add_mesh_to_scene(scene, mesh, T, other_color)
        save_fn = osp.join(save_dir, 'samples_no_collision.png') if save_dir else None
        self._render_scene(scene, show, save_fn)

    def _vis_contained_samples(
            self,
            T_contained,
            T_no_contained,
            T_in_collision,
            show=False,
            save_dir=None,
            contained_color=vis_util.get_color('dodgerblue'),
            other_color=None
    ):
        ui_util.print_no_newline("Rendering contained samples...")
        scene = self.get_scene()
        mesh = self.display_mesh.copy()
        other_color = other_color if other_color is not None else self.trans_obj_color
        n_support_surfaces = len(T_in_collision)
        for i in range(n_support_surfaces):
            if T_contained[i] is None:
                continue  # All samples were filtered out
            for T in T_contained[i]:
                self._add_mesh_to_scene(scene, mesh, T, contained_color)
            # for T in T_no_contained[i]:
            #     self._add_mesh_to_scene(scene, mesh, T, other_color)
            # for T in T_in_collision[i]:
            #     self._add_mesh_to_scene(scene, mesh, T, other_color)
        save_fn = osp.join(save_dir, 'samples_contained.png') if save_dir else None
        self._render_scene(scene, show, save_fn)

    def _vis_no_contained_samples(
            self,
            T_no_contained,
            T_contained,
            T_in_collision,
            show=False,
            save_dir=None,
            no_contained_color=vis_util.get_color('mediumpurple', 1.0),
            other_color=None
    ):
        ui_util.print_no_newline("Rendering non-contained samples...")
        scene = self.get_scene()
        mesh = self.display_mesh.copy()
        other_color = other_color if other_color is not None else self.trans_obj_color
        n_support_surfaces = len(T_in_collision)
        for i in range(n_support_surfaces):
            for T in T_no_contained[i]:
                self._add_mesh_to_scene(scene, mesh, T, no_contained_color)
            # for T in T_contained[i]:
            #     self._add_mesh_to_scene(scene, mesh, T, other_color)
            # for T in T_in_collision[i]:
            #     self._add_mesh_to_scene(scene, mesh, T, other_color)
        save_fn = osp.join(save_dir, 'samples_no_contained.png') if save_dir else None
        self._render_scene(scene, show, save_fn)

    def _vis_gmm_samples(
            self,
            gmm,
            show=False,
            save_dir=None,
            n_gmm_samples=100
    ):
        colors = vis_util.random_colors(gmm.n_components, palette="colorblind", alpha=0.6)
        mesh = self.display_mesh.copy()

        # Visualize all samples
        ui_util.print_no_newline("Rendering all GMM samples...")
        scene = self.get_scene()
        gmm_samples, components = gmm.sample(n_gmm_samples)
        T_gmm_samples = learn_util.vec_to_homogeneous(torch.tensor(gmm_samples)).numpy()
        for i, T in enumerate(T_gmm_samples):
            self._add_mesh_to_scene(scene, mesh, T, colors[components[i]])
        save_fn = osp.join(save_dir, 'gmm_all.png') if save_dir else None
        self._render_scene(scene, show, save_fn)

        # Visualize samples from each component
        for i in range(gmm.n_components):
            ui_util.print_no_newline(f"Rendering GMM component {i+1} of {gmm.n_components}...")
            scene = self.get_scene()
            T_component_samples = T_gmm_samples[components == i]
            T_other_samples = T_gmm_samples[components != i]
            for T in T_component_samples:
                self._add_mesh_to_scene(scene, mesh, T, colors[i])
            # for T in T_other_samples:
            #     self._add_mesh_to_scene(scene, mesh, T, self.trans_obj_color)
            save_fn = osp.join(save_dir, f'gmm_comp-{i+1}.png') if save_dir else None
            self._render_scene(scene, show, save_fn)
        
    def _render_scene(self, scene, show=False, save_fn=None):
        if show:
            scene.show(smooth=False)  # smooth=False speeds up rendering
        ui_util.print_happy("DONE")
        if save_fn:
            with open(save_fn, 'wb') as f:
                f.write(scene.save_image(smooth=False))  # smooth=False speeds up rendering
            ui_util.print_info(f"    Image saved: {save_fn}\n")


if __name__ == '__main__':
    import argparse
    from ll4ma_robots_description.scene_generators import ShelfGenerator

    parser = argparse.ArgumentParser()
    # MODEL
    parser.add_argument('--checkpoint_root', type=str, default=None)
    parser.add_argument('-c', '--ortho6d_checkpoint', type=str, default=None)
    # OBJECT
    parser.add_argument(
        '--obj_mesh', type=str,
        default=osp.join(ros_util.get_path('ll4ma_isaacgym'),
                         "src", "ll4ma_isaacgym", "assets", "cleaner.stl")
    )
    parser.add_argument('--n_support_surfaces', type=int, default=3)
    # SHELF
    parser.add_argument('--shelf_W', type=float, default=0.5)
    parser.add_argument('--shelf_H', type=float, default=0.7)
    parser.add_argument('--shelf_D', type=float, default=0.5)
    parser.add_argument('--n_shelves', type=int, default=1)
    parser.add_argument('--shelf_alpha', type=float, default=1.)
    # TABLE
    parser.add_argument('--table_W', type=float, default=2.4384)
    parser.add_argument('--table_H', type=float, default=0.753)
    parser.add_argument('--table_D', type=float, default=1.2192)
    # DISTRIBUTION
    parser.add_argument('--variance', type=float, default=0.2)
    parser.add_argument('--n_samples', type=int, default=100,
                        help="Note this is number of samples per support surface")
    parser.add_argument('--n_gmm_components', type=int, default=3)
    parser.add_argument('--target_shelf', type=int, default=0)
    # MISC
    parser.add_argument('--seed', type=int, default=None)
    parser.add_argument('--show', action='store_true')
    parser.add_argument('--save_dir', type=str, default=None)
    args = parser.parse_args()


    if args.checkpoint_root is None:
        args.checkpoint_root = os.environ.get("CHECKPOINT_ROOT")
    if args.checkpoint_root is None:
        ui_util.print_error_exit("\n  Need to specify checkpoint root either as environment "
                                 "variable CHECKPOINT_ROOT or as script put with "
                                 "--checkpoint_root flag\n")
    if args.ortho6d_checkpoint is None:
        args.ortho6d_checkpoint = osp.join(args.checkpoint_root, "ortho6d_cp", "ortho6d_001.pt")    
    file_util.check_path_exists(args.ortho6d_checkpoint, "Ortho6d checkpoint file")
    

    if args.save_dir:
        args.save_dir = osp.expanduser(args.save_dir)
        file_util.create_dir(args.save_dir)

    if args.seed is not None:
        random.seed(args.seed)
        np.random.seed(args.seed)
        torch.manual_seed(args.seed)

    shelf, shelf_meshes = ShelfGenerator.create_mesh(
        args.shelf_W,
        args.shelf_H,
        args.shelf_D,
        thickness=0.02,
        n_shelves=args.n_shelves,
        exclude_bottom_wall=True,
        return_component_meshes=True
    )
    table = trimesh.creation.box((args.table_D, args.table_W, args.table_H))

    goal_gen = GoalDistributionGenerator(
        args.ortho6d_checkpoint,
        obj_mesh_fn=args.obj_mesh,
        args=args
    )
    goal_gen.add_env_object(
        "table",
        table,
        transform=np.array([
            [1, 0, 0,  0.7991],
            [0, 1, 0, -0.1892],
            [0, 0, 1, args.table_H/2.],
            [0, 0, 0, 1]
        ]),
        color=vis_util.get_color('darkslategrey'),
    )
    shelf_part_names = []
    shelf_tf = np.eye(4)
    shelf_tf[:3,3] = [0.7, -0.5, args.table_H + args.shelf_H/2.]
    shelf_tf[:3,:3] = Rotation.from_euler('Z', 90, degrees=True).as_matrix()
    for i, m in enumerate(shelf_meshes):
        part_name = f"shelf_part_{i}"
        goal_gen.add_env_object(
            part_name,
            m,
            transform=shelf_tf,
            color=vis_util.get_color('tan', alpha=args.shelf_alpha)
        )
        shelf_part_names.append(part_name)

    H_buffer = 0.001  # Add mm buffer for collision check, stable poses are touching surface
    diff = args.shelf_H / (args.n_shelves + 1) + H_buffer
    target_height = args.table_H + args.target_shelf * diff
    goal_gen.create_distribution(
        shelf,
        nominal_T=np.array([
            [1, 0, 0,  0.7],
            [0, 1, 0, -0.5],
            [0, 0, 1, target_height],
            [0, 0, 0, 1]
        ]),
        n_support_surfaces=args.n_support_surfaces,
        variance=args.variance,
        n_samples=args.n_samples,
        reject_collisions=shelf_part_names,
        show=args.show,
        save_dir=args.save_dir,
        n_gmm_components=args.n_gmm_components
    )
