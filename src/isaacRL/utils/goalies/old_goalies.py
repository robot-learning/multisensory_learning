class ObjectMoveHER(HERGoalie):
    """docstring for ObjectMoveHER."""

    def __init__( self, obj_name, proposer, include_rot=False, threshold=0.05,
            part_ws=[3.0,1.0,0.01], sparse=False ):
        super(ObjectMoveHER, self).__init__(threshold)
        self.obj_name = obj_name
        self.goal_shape = 3 if not include_rot else 7
        self.proposer = proposer
        self.sparse = sparse
        self.block_w = part_ws[0]
        self.ee_w = part_ws[1]
        self.act_penal = part_ws[2]

    def extract_current(self, state):
        return state[self.obj_name][:, :self.goal_shape].clone()

    def extract_goal(self, state):
        # ssize = state[self.obj_name].shape[0]
        # offset = self.offset.clone()
        # offset = torch.stack([offset]*ssize)
        # goal = state[self.obj_name][:,:3] \
        #           + self.proposer.propose(state)
        return self.proposer.propose(state)

    def reward(self, state, goal):
        obj_pose = state[self.obj_name][:,:self.goal_shape]
        ee_pose = state['ee_state'][:,:self.goal_shape]
        actions = state['prev_action']
        if self.sparse:
            return moveObjectSparseReward(
              obj_pose, goal, self.include_rot, self.threshold
            )
        return moveObjectShapeReward(
          obj_pose, goal, ee_pose, actions,
          self.block_w, self.ee_w, self.act_penal, self.threshold
        )

# @torch.jit.script
def moveObjectShapeReward(
    obj_pose, goal_pose, ee_pose, actions,
    obj_w, ee_w, action_penalty_scale, threshold
):
    # type: (Tensor, Tensor, Tensor, Tensor, float, float, float, float) -> Tuple[Tensor, Tensor]
    shape0 = obj_pose.shape[0]

    # distance from obj to goal
    obj_dist = torch.norm(obj_pose[:,:3] - goal_pose[:,:3], p=2, dim=-1)
    obj_reward = ( 1.0 / (1.0 + obj_dist ** 2) )**2
    obj_reward = torch.where(obj_dist <= threshold, obj_reward * 2, obj_reward)

    # distance from end effector to obj
    ee_dist = torch.norm(obj_pose[:,:3] - ee_pose[:,:3], p=2, dim=-1)
    ee_reward = ( 1.0 / (1.0 + ee_dist ** 2) )**2
    ee_reward = torch.where(ee_dist <= threshold, ee_reward * 2, ee_reward)

    # ee_forward_axis = torch.tensor([0, 0, 1], device=ee_pose.device)
    # ee_up_axis = torch.tensor([0, 1, 0], device=ee_pose.device)
    # # object grab from top
    # object_grab_axis = torch.tensor([0, 0, -1], device=ee_pose.device)
    # object_up_axis = torch.tensor([0, 0, 1], device=ee_pose.device)
    # axis1 = quat_apply(franka_grasp_rot, ee_forward_axis)
    # axis2 = quat_apply(drawer_grasp_rot, object_grab_axis)
    # axis3 = quat_apply(franka_grasp_rot, ee_up_axis)
    # axis4 = quat_apply(drawer_grasp_rot, object_up_axis)
    #
    # dot1 = torch.bmm(axis1.view(shape0, 1, 3), axis2.view(shape0, 3, 1)).squeeze(-1).squeeze(-1)  # alignment of forward axis for end effector
    # dot2 = torch.bmm(axis3.view(shape0, 1, 3), axis4.view(shape0, 3, 1)).squeeze(-1).squeeze(-1)  # alignment of up axis for end effector
    # # reward for matching the orientation of the hand to the drawer (fingers wrapped)
    # rot_reward = 0.5 * (torch.sign(dot1) * dot1 ** 2 + torch.sign(dot2) * dot2 ** 2)

    action_penalty = torch.sum(actions ** 2, dim=-1)

    rewards = (
      obj_w * obj_reward + ee_w * ee_reward
      # + rot_w * obj_rot + rot_w * ee_rot
      - action_penalty_scale * action_penalty
    )

    terms = torch.where(
      obj_dist < threshold, torch.ones((shape0,), device=obj_pose.device),
      torch.zeros((shape0,), device=obj_dist.device)
    )

    return rewards, terms, obj_dist

# @torch.jit.script
def moveObjectSparseReward(
    block_pose, goal_pose, include_rot, threshold
):
    # type: (Tensor, Tensor, bool, float) -> Tuple[Tensor, Tensor]

    shape0 = block_pose.shape[0]
    rewards = torch.ones((shape0,), device=block_pose.device)

    # distance from block to goal
    block_dist = torch.norm(block_pose - goal_pose, p=2, dim=-1)

    rewards = torch.where(
      block_dist < threshold,
      torch.ones((shape0,), device=block_pose.device),
      torch.zeros((shape0,), device=block_dist.device)
    )

    return rewards, rewards.clone()


class HandoffHER(object):
    """docstring for HandoffHER."""

    def __init__(self, obj_name, offset, threshold=0.01, part_ws=[1.5,3.0,0.01], sparse=False):
        super(HandoffHER, self).__init__()
        self.obj_name = obj_name
        self.offset = offset if torch.is_tensor(offset) else torch.tensor(offset)
        self.threshold = threshold
        self.goal_shape = 7
        self.ee_w = part_ws[0]
        self.obj_w = part_ws[1]
        self.act_penal = part_ws[-1]
        self.sparse = sparse

    def goal_dist(self, state, goal):
        return state[self.obj_name][:,:self.goal_shape] - goal

    def extract_current(self, state):
        return state[self.obj_name][:,:self.goal_shape].clone()

    def extract_goal(self, state):
        ssize = state[self.obj_name].shape[0]
        device = state[self.obj_name].device
        goal = state['surgeon_hand'][:,:self.goal_shape].clone()
        offset = torch.stack([self.offset.clone()]*ssize).to(device)
        offset[:,:3] = quat_apply(goal[:,3:], offset[:,:3])
        goal[:,:offset.shape[1]] += offset
        if offset.shape[1] < self.goal_shape:
            # quat = quat_from_euler_xyz(
            #   *torch.tensor([[0., 0., -1.]], device=device).T
            # )
            quat = torch.zeros(4, device=device)
            quat[-1] = 1.
            goal[:,offset.shape[1]:] += quat
        return goal

    def reward(self, state, goal):
        ee_pose = state['ee_state'][:,:self.goal_shape]
        obj_pose = state[self.obj_name][:,:self.goal_shape]
        actions = state['prev_action']
        if self.sparse:
            return moveObjectSparseReward(
              ee_pose, obj_pose, hand, goal, self.threshold
            )
        return moveObjectShapeReward(
          obj_pose, goal, ee_pose, actions,
          self.obj_w, self.ee_w, self.act_penal, self.threshold
        )


class Move2PoseHER(HERGoalie):
    """docstring for Move2PoseHER."""

    def __init__( self, goal_ranges, threshold=0.01, ee_w=3., rot_w=0., alpha=1., c_w=1000., act_penal=0.01, sparse=False ):
        super(Move2PoseHER, self).__init__(threshold)
        self.goal_shape = 7
        self.sparse = sparse
        self.ee_w = ee_w
        self.rot_w = rot_w
        self.alpha = alpha
        self.c_w = c_w
        self.act_penal = act_penal
        self.goal_ranges = goal_ranges

    def goal_dist(self, state, goal):
        return state['ee_state'][:,:self.goal_shape] - goal

    def extract_current(self, state):
        return state['ee_state'][:,:self.goal_shape].clone()

    def extract_goal(self, state):
        ee_pose = state['ee_state'][:,:self.goal_shape]
        self.goal_ranges = self.goal_ranges.to(ee_pose.device)
        goal_range = self.goal_ranges[:,1] - self.goal_ranges[:,0]
        goal_low = self.goal_ranges[:,0]
        goal = torch.rand((*ee_pose.shape), device=ee_pose.device)
        goal = goal * goal_range + goal_low
        return goal

    def reward(self, state, goal):
        ee_pose = state['ee_state'][:,:self.goal_shape]
        actions = state['prev_action']
        if self.sparse:
            return move2SparseReward(
              ee_pose, goal, self.threshold
            )
        return move2ShapeReward(
          ee_pose, goal, actions, self.ee_w, self.rot_w, self.act_penal, self.threshold
        )
