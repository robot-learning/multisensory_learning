#!/usr/bin/env python
import os
import sys
import argparse
import pickle
import random
import itertools
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
from tqdm import tqdm

from ll4ma_isaacgym.oracles import AboveObjectOracle, PrePushOracle, PushOracle
from ll4ma_util import file_util, ui_util
from multisensory_learning.learning import MSPConfig, MSPLearner, msp_util

import torch


class GoalSampleCreator:

    def __init__(self, msp_checkpoint, oracles, debug=False):
        msp_config = MSPConfig(checkpoint=msp_checkpoint)
        self.msp_learner = MSPLearner(msp_config, set_eval=True)
        # self.msp_learner.models['multisensory_encoder'].train()  # TODO trying sampling from encoder
        self.oracles = oracles
        self.debug = debug
        
    def create_samples(self, filenames, target_dir, batch_size=128, max_samples_per_file=1000,
                       max_files_per_dir=1e6, goal_downsample=3, input_downsample=5):
        existing = [f for f in file_util.list_dir(os.path.dirname(target_dir)) if target_dir in f]
        if len(existing) > 0:
            delete = ui_util.query_yes_no(f"\nDelete these existing directories? {existing}")
            if delete:
                for f in existing:
                    file_util.remove_dir(f)
            else:
                ui_util.print_info_exit("Exiting. No data was deleted.\n\n")

        file_util.safe_create_dir(target_dir)
        
        if self.debug:
            filenames = filenames[:1]
        
        self.sample_idx = 1
        self.target_dir = target_dir
        # Target dir name will change with idx appended if too many files are saved to one
        # dir, apparently the filesystem can't handle too many files in one dir 
        target_dir_base = target_dir
        target_dir_idx = 1
        
        metadata = {'msp_checkpoint': self.msp_learner.config.checkpoint}

        # Get test file to see how many goals there are (for now assuming same objs in each
        # file and that oracle will define one goal per obj)
        data, attrs = file_util.load_pickle(filenames[0])
        obj_names = [k for k, v in attrs['objects'].items() if not v['fix_base_link']]
        self.label_names = ['not_goal']
        for oracle in self.oracles:
            for obj_name in obj_names:
                self.label_names.append(f"{oracle.get_name()}_{obj_name}")
        
        counts = {k: 0 for k in self.label_names}


        # TODO want this to generate samples for all oracles/obj combos, need to figure
        # out inputs to satisfy sub-goals I think
        
        
        pos_sample_idxs = []  # Init as empty in case no goals are found
        neg_sample_idxs = []
        for file_idx, filename in enumerate(tqdm(filenames, desc="  Process Goal Samples")):
            data, attrs = file_util.load_pickle(filename)
            n_steps = len(next(iter(v for v in data.values() if isinstance(v, np.ndarray))))
            all_idxs = list(range(n_steps))
            
            labels = np.zeros(n_steps, dtype=np.int64)
            for oracle in oracles:
                for i, obj_name in enumerate(obj_names):
                    obj_i_labels = oracle.generate_labels(data, obj_name)
                    labels[obj_i_labels] = self.label_names.index(f"{oracle.get_name()}_{obj_name}")

            goal_idxs = np.where(labels > 0)[0].tolist()            
            nongoal_idxs = [i for i in all_idxs if i not in goal_idxs]    
            in_idxs = deepcopy(all_idxs) # TODO for now taking any idx
    
            # Downsample so it's not too redundant
            goal_idxs = goal_idxs[::goal_downsample]
            nongoal_idxs = nongoal_idxs[::goal_downsample]
            in_idxs = in_idxs[::input_downsample]
            
            pos_sample_idxs = list(itertools.product(in_idxs, goal_idxs))
            neg_sample_idxs = list(itertools.product(in_idxs, nongoal_idxs))
                
            if len(pos_sample_idxs) > max_samples_per_file:
                pos_sample_idxs = random.sample(pos_sample_idxs, max_samples_per_file)
            if len(neg_sample_idxs) > max_samples_per_file:
                neg_sample_idxs = random.sample(neg_sample_idxs, max_samples_per_file)
    
            # if self.debug:
            #     for in_idx in in_idxs:
            #         plt.imshow(data['rgb'][in_idx])
            #         plt.title("INPUT")
            #         plt.show()
            #     for goal_idx in goal_idxs:
            #         plt.imshow(data['rgb'][goal_idx])
            #         plt.title("GOAL")
            #         plt.show()
    
                # TODO probably this should better handled, maybe even down to the data logging?
            if 'arm_joint_position' in self.msp_learner.config.enc_obs_modalities:
                data['arm_joint_position'] = data['joint_position'][:,:attrs['n_arm_joints']]

            with torch.no_grad():

                # TODO can probably batch this better, doing individual samples so that we always
                # get a new sample in the latent space (if VAE is active). Can for example process
                # these pairs in batch to accumulate all their inputs and pass through network,
                # it will still give different samples just process all at once
                
                # Save positive goal samples
                for (in_idx, goal_idx) in pos_sample_idxs:
                    # Use different sample for every input (from VAE)
                    latent_in = self.msp_learner.encode_raw_obs(data, in_idx, in_idx+1)
                    latent_goal = self.msp_learner.encode_raw_obs(data, goal_idx, goal_idx+1)
                    self._save_sample(latent_in.squeeze(), latent_goal.squeeze(), labels[goal_idx])
                    counts[self.label_names[labels[goal_idx]]] += 1
                # Save negative samples
                for (in_idx, nongoal_idx) in neg_sample_idxs:
                    latent_in = self.msp_learner.encode_raw_obs(data, in_idx, in_idx+1)
                    latent_nongoal = self.msp_learner.encode_raw_obs(data, nongoal_idx, nongoal_idx+1)
                    self._save_sample(latent_in.squeeze(), latent_nongoal.squeeze(), 0)
                    counts['not_goal'] += 1
    
                # if self.debug:
                #     np.set_printoptions(precision=4)
                #     latent_in = torch.from_numpy(latent_in[in_idx]).view(1, 1, -1)
                #     latent_in = latent_in.to(self.msp_learner.device)
                #     latent_goal = latent_goal.unsqueeze(0)
                #     dec_in = self.msp_learner.decode_obs(latent_in, True)
                #     dec_goal = self.msp_learner.decode_obs(latent_goal, True)
                #     print("IN JOINT", data['joint_position'][in_idx,:attrs['n_arm_joints']])
                #     print("DEC IN JOINT", dec_in['arm_joint_position'])
                #     print("GOAL JOINT", data['joint_position'][goal_idx,:attrs['n_arm_joints']])
                #     print("DEC GOAL JOINT", dec_goal['arm_joint_position'])
                #     print("IN OBJ", data['objects']['block']['position'][in_idx])
                #     print("DEC IN OBJ", dec_in['block_obj_position'])
                #     print("GOAL OBJ", data['objects']['block']['position'][goal_idx])
                #     print("DEC GOAL OBJ", dec_goal['block_obj_position'])
                #     # sys.exit()                    
    
            if len(file_util.list_dir(self.target_dir)) >= max_files_per_dir:
                self.target_dir = f"{target_dir_base}_{target_dir_idx}"
                os.makedirs(self.target_dir)
                target_dir_idx += 1

        # TODO need to track number of samples for each goal in multi-goal setting
        metadata['goal_counts'] = counts
        metadata['multisensory_embedding_size'] = self.msp_learner.config.multisensory_embedding_size
        file_util.save_yaml(metadata, os.path.join(target_dir_base, "metadata.yaml"))

    def _save_sample(self, latent_in, latent_target, label):
        sample = {
            'latent_in': latent_in,
            'latent_target': latent_target,
            'goal_label': label
        }
        fn = os.path.join(self.target_dir, f"{self.label_names[label]}_{self.sample_idx:09d}.pickle")
        file_util.save_pickle(sample, fn)
        self.sample_idx += 1

        
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--msp_checkpoint', type=str, required=True,
                        help="Absolute path to MSP model checkpoint")
    parser.add_argument('--data_root', type=str, required=True,
                        help="Absolute path to directory containing demo instances (pickle files)")
    parser.add_argument('--target_dir', type=str, required=True,
                        help="Absolute path to directory where processed samples will be saved")
    parser.add_argument('--oracles', type=str, nargs='+', required=True,
                        choices=['above_object', 'prepush', 'push'])
    parser.add_argument('--batch_size', type=int, default=128)
    parser.add_argument('--max_samples_per_file', type=int, default=1000)
    parser.add_argument('--max_files_per_dir', type=int, default=1e6)
    parser.add_argument('--goal_downsample', type=int, default=3)
    parser.add_argument('--input_downsample', type=int, default=5)
    parser.add_argument('--debug', action='store_true')
    args = parser.parse_args()

    oracles = []
    for oracle in args.oracles:
        if oracle == 'above_object':
            oracles.append(AboveObjectOracle())
        elif oracle == 'prepush':
            oracles.append(PrePushOracle())
        elif oracle == 'push':
            oracles.append(PushOracle())
        else:
            ui_util.print_error_exit(f"\n  Unknown oracle type: {oracle}\n")

    filenames = file_util.list_dir(args.data_root, '.pickle')
        
    sample_creator = GoalSampleCreator(args.msp_checkpoint, oracles, args.debug)
    sample_creator.create_samples(filenames, args.target_dir, args.batch_size,
                                  args.max_samples_per_file, args.max_files_per_dir,
                                  args.goal_downsample, args.input_downsample)

    ui_util.print_happy("\n  Processing goal samples complete.\n")
