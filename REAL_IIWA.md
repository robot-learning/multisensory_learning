# Running on Real iiwa

1. [:eyes: Perception PC] In a new terminal, start `roscore`.
2. [:robot: Robot PC] In a new terminal, start robot controller `roslaunch iiwa_control joint_position_control`
3. [:robot: Robot PC] If using Reflex hand, in a new terminal, `roslaunch reflex left_reflex.launch`
4. [:muscle: Pendant] Start ROS control on the pendant:
    - **Applications > ROSPositionControl**
	- Play button (Green arrow on left of pendant) 
	- Select `reflex` in dialog that comes up
	- Select `Enable ROS Control`
5. [:eyes: Perception PC] In a new terminal, `roslaunch multisensory_learning real_iiwa.launch`. Verify the robot appears in rviz in the same joint configuration as the real robot.
6. [:eyes: Perception PC] In a new terminal, `rosrun multisensory_learning real_iiwa.py`. These variants may be useful:
    - `rosrun multisensory_learning real_iiwa.py --test_hand` (closes and opens the hand only to verify it's working)
	- `rosrun multisensory_learning real_iiwa.py --test_perception` (shows where the object perception believes object is)
    - `rosrun multisensory_learning real_iiwa.py --reset` (move robot back to home position)
	- `rosrun multisensory_learning real_iiwa.py --no_go_home` (run without going to home position (e.g. the robot's already there))