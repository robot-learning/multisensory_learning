#!/usr/bin/env python
import isaacgym # To satisfy import order
import os
import sys
import argparse
import rospy
import tf
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseStamped

from ll4ma_isaacgym.core import SessionConfig, EnvironmentState
from ll4ma_isaacgym.robots import Robot
from ll4ma_isaacgym.behaviors import Behavior, behavior_util

from ll4ma_util import file_util, ui_util, ros_util, math_util
from moveit_interface.iiwa_planner import IiwaPlanner
from reflex import ReflexGraspInterface
from trajectory_msgs.msg import JointTrajectory

import torch


DEFAULT_TASK_CONFIG_DIR = os.path.join(ros_util.get_path("multisensory_learning"),
                                       "src", "multisensory_learning", "config", "isaacgym")


class IiwaBehaviorInterface:
    def __init__(
        self, session_config, rate=100, use_reflex=True
    ):
        self.session_config = session_config
        self.use_reflex = use_reflex

        self.rate = rospy.Rate(rate)
        self.tf_listener = tf.TransformListener()
        self.cmd_pub = rospy.Publisher("/iiwa/joint_cmd", JointState, queue_size=1)
        rospy.Subscriber("/joint_states", JointState, self.joint_state_cb)

        self.joint_state = None
        self.obj_state = None

        self.robot_config = next(iter(session_config.env.robots.values()))
        self.robot = Robot(self.robot_config)

        self.state = EnvironmentState()
        self.state.dt = 1.0 / rate
        self.state.objects = self.session_config.env.objects

        self.state.object_states["object_table"] = torch.tensor(
            [
                self.session_config.env.objects["object_table"].position[0],
                self.session_config.env.objects["object_table"].position[1],
                self.session_config.env.objects["object_table"].position[2],
                self.session_config.env.objects["object_table"].orientation[0],
                self.session_config.env.objects["object_table"].orientation[1],
                self.session_config.env.objects["object_table"].orientation[2],
                self.session_config.env.objects["object_table"].orientation[3],
            ]
        )

        self.iiwa = IiwaPlanner(rate)
        if use_reflex:
            self.reflex = ReflexGraspInterface()

    def joint_state_cb(self, msg):
        self.joint_state = msg

    def lookup_tf(self, from_frame, to_frame):
        trans = rot = None
        rate = rospy.Rate(100)
        while not rospy.is_shutdown() and trans is None:
            try:
                trans, rot = self.tf_listener.lookupTransform(from_frame, to_frame, rospy.Time())
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
                rospy.logwarn(e)
            rate.sleep()
        return trans, rot

    def wait_for_joint_state(self, timeout=30):
        if self.joint_state is None:
            rospy.loginfo("Waiting for joint state...")
            start = rospy.get_time()
            while (
                not rospy.is_shutdown()
                and rospy.get_time() - start < timeout
                and self.joint_state is None
            ):
                self.rate.sleep()
            if self.joint_state is not None:
                rospy.loginfo("Joint state received")
            else:
                rospy.logerr("Joint state unknown")
                return False
        return True

    def update_env_state(self, update_obj_state=True):
        # TODO should these short-circuit?
        success = self.update_joint_state()
        success = success and self.update_ee_state()
        if update_obj_state:
            success = success and self.update_obj_state()
        return success

    def update_joint_state(self):
        if not self.wait_for_joint_state():
            return False
        self.state.joint_position = torch.tensor(self.joint_state.position).unsqueeze(-1)
        self.state.joint_velocity = torch.tensor(self.joint_state.velocity).unsqueeze(-1)
        self.state.joint_torque = torch.tensor(self.joint_state.effort).unsqueeze(-1)
        self.state.joint_names = self.joint_state.name
        # TODO make this less hacked
        self.state.prev_action = torch.tensor(list(self.joint_state.position[:7]) + [0])
        return True

    def update_ee_state(self):
        rospy.loginfo("Getting EE pose...")
        trans, rot = self.lookup_tf("/world", "/reflex_palm_link")
        if trans is None or rot is None:
            rospy.logerr("Could not update EE pose")
            return False
        else:
            self.state.ee_state = torch.cat([torch.tensor(trans), torch.tensor(rot)])
            rospy.loginfo("EE pose found!")
            return True

    def update_obj_state(self, verify=True):
        rospy.loginfo("Estimating object pose...")
        success = self._update_obj_pose()
        if verify:
            while not ui_util.query_yes_no("Object pose look okay?", default="yes"):
                success = self._update_obj_pose()
        return success

    def _update_obj_pose(self):
        # TODO fix this hack
        mesh_resource = "package://ll4ma_isaacgym/src/ll4ma_isaacgym/assets/cleaner.stl"
        # TODO this should depend on current active behavior
        target_obj = self.session_config.task.behavior.behaviors[0].target_object

        trans, rot = self.lookup_tf("world", "bleach")   # TODO hard-coded
        if trans is None or rot is None:
            rospy.logwarn("Could not update object pose")
            return False
        else:
            self.state.object_states[target_obj] = torch.cat([
                torch.tensor(trans),
                torch.tensor(rot)
            ])

            # Display the object mesh in the estimated pose as sanity check
            pose_stmp = PoseStamped()
            pose_stmp.header.frame_id = "world"
            pose_stmp.pose.position.x = trans[0]
            pose_stmp.pose.position.y = trans[1]
            pose_stmp.pose.position.z = trans[2]
            pose_stmp.pose.orientation.x = rot[0]
            pose_stmp.pose.orientation.y = rot[1]
            pose_stmp.pose.orientation.z = rot[2]
            pose_stmp.pose.orientation.w = rot[3]
            ros_util.display_rviz_mesh(mesh_resource, pose_stmp, color="orangered")
            
            rospy.loginfo("Object pose found!")
            return True

    def command_trajectory(
        self,
        traj,
        rate=100,
        at_goal_tolerance=0.01,
        wait_secs_for_at_goal=0,
        wait_secs_after_commanded=0,
        preview=True,
    ):
        self.iiwa.set_rate(rate)
        self.iiwa.command_trajectory(
            traj,
            False,
            at_goal_tolerance,
            wait_secs_for_at_goal,
            wait_secs_after_commanded,
            preview,
        )

    def reset(self, go_home=True):
        rospy.loginfo("Resetting environment...")
        self.behavior = Behavior(
            session_config.task.behavior,
            self.robot,
            session_config.env,
            None,
            session_config.device,
            open_loop=True,
        )
        if self.use_reflex:
            self.reflex.open_hand()
        if go_home:
            self.go_home()
        rospy.loginfo("Environment reset")

    def go_home(self, update_obj_state=False, preview=True):
        objects = {}
        self.update_env_state(update_obj_state)
        if update_obj_state:
            objects = behavior_util.get_moveit_objects_from_state(self.state)
        traj = self.iiwa.get_plan("home", objects, vel_scaling=0.2)
        if traj is not None:
            self.command_trajectory(traj, preview=preview)
        else:
            rospy.logerr("Could not go home, got empty plan trajectory")

    def run_behavior(self, preview=True):
        if not self.update_env_state():
            rospy.logerr("State could not be fully updated")
            return False

        if not self.behavior.set_policy(self.state):
            rospy.logerr("Could not get behavior trajectories")
            sys.exit()

        actions = self._flatten_ros_actions(self.behavior.get_ros_actions())
        for action in actions:
            if isinstance(action, JointTrajectory):
                self.command_trajectory(action, preview=preview)
            elif isinstance(action, str):
                if action == "close":
                    if not self.use_reflex:
                        rospy.logerr(
                            "Cannot execute 'close' behavior, must set "
                            "use_reflex to true on class creation"
                        )
                        sys.exit()

                    # TODO should have these on config
                    close_velocity = 1.0
                    max_close_position = [2.7] * 3
                    use_tactile_stops = False
                    use_velocity_stops = True
                    tighten_increment = 0.3

                    self.reflex.grasp(
                        close_velocity,
                        max_close_position,
                        use_tactile_stops,
                        use_velocity_stops,
                        tighten_increment,
                    )
                elif action == "open":
                    if not self.use_reflex:
                        rospy.logerr(
                            "Cannot execute 'open' behavior, must set "
                            "use_reflex to true on class creation"
                        )
                        sys.exit()
                    self.reflex.open_hand()
                else:
                    rospy.logerr(f"Unknown action string: {action}")
            else:
                rospy.logerr(f"Unknown action type: {type(action)}")
                sys.exit()

    def _flatten_ros_actions(self, traj):
        actions = []
        for k, v in traj.items():
            if isinstance(v, dict):
                actions += self._flatten_ros_actions(v)
            else:
                actions.append(v)
        return actions


if __name__ == "__main__":
    rospy.init_node("run_iiwa_behaviors", anonymous=True)

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--config", type=str,
        default="push_cleaner_upright.yaml",
        help="Filename of YAML config for simulation (relative to --config_dir)",
    )
    parser.add_argument(
        "--config_dir", type=str,
        default=DEFAULT_TASK_CONFIG_DIR,
        help="Absolute path to root directory where sim YAML configs are",
    )
    parser.add_argument("--device", type=str, default="cpu")
    parser.add_argument("--rate", type=float, default=100.0)
    parser.add_argument("--use_reflex", action="store_true")
    parser.add_argument("--no_use_reflex", dest="use_reflex", action="store_false")
    parser.add_argument("--test_hand", action="store_true")
    parser.add_argument("--test_perception", action="store_true")
    parser.add_argument(
        "--reset", action="store_true", help="Set true to reset environment and exit"
    )
    parser.add_argument(
        "--go_home", action="store_true", help="Move robot to home configuration on reset"
    )
    parser.add_argument(
        "--no_go_home", dest="go_home", action="store_false",
        help="Leave robot in current configuration on reset",
    )
    parser.set_defaults(use_reflex=True)
    parser.set_defaults(go_home=True)
    args = parser.parse_args(rospy.myargv(argv=sys.argv)[1:])

    config_filename = os.path.join(args.config_dir, args.config)
    file_util.check_path_exists(config_filename, "Config file")
    session_config = SessionConfig(config_filename=config_filename)
    session_config.device = args.device

    interface = IiwaBehaviorInterface(session_config, args.rate, args.use_reflex)

    if args.test_hand:
        if not args.use_reflex:
            rospy.logwarn("Reflex hand not active, must set --use_reflex")
            sys.exit()
        interface.reflex.test()
        rospy.loginfo("Hand test complete, exiting.")
        sys.exit()
    if args.test_perception:
        if not interface.update_obj_state():
            rospy.logerr("Could not estimate object state")
        rospy.loginfo("Perception test complete, exiting.")
        sys.exit()
    if args.reset:
        interface.reset()
        rospy.loginfo("Environment reset complete, exiting.")
        sys.exit()

    # We need to make sure fake joint states aren't being published when executing
    if rospy.get_param("/fake_joint_state", True):
        rospy.logerr(
            "Cannot execute behavior. Either fake joint states are being "
            "published, or could not verify they are not. Make sure "
            "fake_joint_state is set to false for the real_iiwa.launch file."
        )
        sys.exit()

    interface.reset(args.go_home)
    interface.run_behavior(preview=True)
    rospy.loginfo("Behavior execution complete.")
