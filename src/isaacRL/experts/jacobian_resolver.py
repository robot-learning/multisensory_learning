
import torch

from ll4ma_util import ros_util, torch_util
import moveit_interface.util as moveit_util

class JacobianResolver:
    """docstring for JacobianResolver."""

    def __init__(self, num_joints, joint_names, ee_link, velocity):
        self.num_joints = num_joints
        self.joint_names = joint_names
        self.ee_link = ee_link
        self.velocity = velocity

    def get_trajectory(
        self,
        start_joints,
        start_pose,
        target_pose,
        num_steps=50,
        obj_state=None,
        dt=1/60.
    ):
        plan = self.get_jacobian_pinv(
            start_joints,
            start_pose,
            target_pose,
            num_steps,
            obj_state,
            dt
        )
        if plan is None:
            return None, {}
        traj = [p.positions for p in plan.trajectory.joint_trajectory.points]
        traj = torch.tensor(traj)
        return traj, {}

    def get_jacobian_pinv(
        self,
        start_joints,
        start_pose,
        target_pose,
        num_steps=50,
        obj_state=None,
        dt=1/60.
    ):
        dist = start_pose[:3] - target_pose[:3]
        velocity = dist / (num_steps * dt)
        start_pose = ros_util.array_to_pose(start_pose.cpu().numpy())
        target_pose = ros_util.array_to_pose(target_pose.cpu().numpy())
        joint_state = ros_util.get_joint_state_msg(
            start_joints[:self.num_joints].cpu().numpy().squeeze(),
            joint_names=self.joint_names(),
        )
        plan, success = moveit_util.get_jac_pinv_plan(
            joint_state,
            start_pose,
            target_pose,
            self.ee_link,
            velocity=self.velocity,
            n_steps=num_steps
        )
        if plan is not None and not plan.success:
            plan = None
        return plan

    # def get_action(self, joints, target_pose, jacobian, ee_pose=None):
    #     if ee_pose is None:
    #         pose_err = target_pose
    #     else:
    #         pos_diff = target_pose[:,:3] - ee_pose[:,:3]
    #         quat_diff = torch_util.quat_mul(
    #             target_pose[:,3:7], torch_util.quat_conjugate(ee_pose[:,3:7])
    #         )
    #         pose_err = torch.cat((
    #             pos_diff,
    #             torch_util.quat_to_axisangle(quat_diff)[0]
    #         ), dim=-1)
    #     jac_pinv = torch_util.get_pinv(jacobian)
    #
    #     action = torch.bmm((jac_pinv, pose_err), dim=0)
    #     if (action > self.max_vel).any():
    #         too_fast = action > self.max_vel
    #         norms = torch.nn.functional.normalize(actions[too_fast.any(dim=-1)])
    #         action[too_fast.any(dim=-1)] = norms * self.max_vel
    #     return action
