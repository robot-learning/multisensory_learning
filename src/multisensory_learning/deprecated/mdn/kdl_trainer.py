
import os, sys, argparse, json
import numpy as np
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader

from trainer import Trainer
from model import MVNModel
from eval import evaluate
from dataset import LatentDataset, convert2latent
from utils.loss import kldiv_loss, logprob_loss
from utils.latent_models import LatentModel


class DistrTrainer(Trainer):
    """docstring for DistrTrainer."""

    def __init__(self, config, device, training=True):
        super(DistrTrainer, self).__init__()
        self.config = config
        self.device = device

        self.latent_model = LatentModel(config['latent_chkpt'], device)
        self.latent_model.config['batch_size'] = config['batch_size']

        insize = config['latent_size']
        if not config['sample_first']:
            insize *= 2
        self.model = MVNModel(
          hlayer_sizes=[insize*2, int(insize*1.5), insize*2],
          ind=insize, outd=config['latent_size']
        ).to(device)

        self.optim = None
        if training:
            self.optim = torch.optim.Adam(self.model.parameters())

        self.criterion = kldiv_loss
        if config['criterion'] == 'logprob':
            self.criterion = logprob_loss

        self.start_epoch = 0
        self.end_epoch = config['epochs']
        self.tloss = []
        self.vloss = []

        self.valloader = None

    def converter(self, x, batch_size=None):
        if x['obs']['rgb'].shape[0] != self.config['batch_size']:
            batch_size = x['obs']['rgb'].shape[0]
            self.latent_model.config['batch_size'] = batch_size
        latent = convert2latent( x, self.latent_model,
          self.config['sample_first'], self.config['context'] )
        if batch_size is not None:
            self.latent_model.config['batch_size'] = self.config['batch_size']
        return latent

    def load_model(self, checkpt):
        checkpoint = torch.load(checkpt)
        self.model.load_state_dict(checkpoint['model'])
        if self.optim is not None:
            self.optim.load_state_dict(checkpoint['optimizer'])
        self.tloss = checkpoint['tloss']
        self.vloss = checkpoint['vloss']
        self.start_epoch = checkpoint['epoch']
        self.end_epoch += self.start_epoch

    def train(self):

        trainset = LatentDataset( self.config['train_files'], device=self.device,
          obs_mods=self.latent_model.config['obs_modalities'],
          act_mods=self.latent_model.config['act_modalities']
        )
        trainloader = DataLoader(
            trainset,
            batch_size=self.config['batch_size'],
            shuffle=True,
            num_workers=self.config['n_cpu'],
            pin_memory=True,
        )

        bestloss = float("inf")
        best_epoch = 0
        for epoch in range(self.start_epoch, self.end_epoch):
            print('epoch:', epoch+1)

            lossHistory = []
            self.model.train()
            pbar = tqdm(total=len(trainloader), file=sys.stdout)
            for di, xdata in enumerate(trainloader):
                torch_util.move_batch_to_device(xdata, self.device)
                xdata, targets = self.converter(xdata)

                self.optim.zero_grad()
                mus, sig = self.model(xdata)
                loss = self.criterion(mus, sig, targets)
                loss.backward()
                self.optim.step()

                xdata = None
                targets = None
                lossHistory.append(loss.item())
                desc = f'  Iter {di}: loss={np.mean(lossHistory):.4f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            self.tloss.append( np.mean(lossHistory) )

            if epoch % self.config['evaluation_interval'] == 0:
                print('eval:')
                self.model.eval()
                loss = self.evaluate()
                print('eval loss:', loss)
                self.vloss.append( loss )
                if loss < bestloss:
                    bestloss = loss
                    best_epoch = epoch
                    filen = os.path.join( self.config['checkpoint_path'],
                      'best_epoch.txt' )
                    with open(filen, 'w') as f:
                        f.write(str(epoch))

            if epoch % self.config['checkpoint_interval'] == 0:
                checkpoint = {}
                checkpoint['optimizer'] = self.optim.state_dict()
                checkpoint['model'] = self.model.state_dict()
                checkpoint['tloss'] = self.tloss
                checkpoint['vloss'] = self.vloss
                checkpoint['epoch'] = epoch
                filename = os.path.join( self.config['checkpoint_path'],
                  self.config['checkpoint_name'].format(epoch) )
                torch.save(checkpoint, filename)
                checkpoint = None

    def evaluate(self):

        if self.valloader is None:
            valset = LatentDataset( self.config['valid_files'], device=self.device,
              obs_mods=self.latent_model.config['obs_modalities'],
              act_mods=self.latent_model.config['act_modalities']
            )
            valloader = DataLoader(
                valset,
                batch_size=self.config['batch_size'],
                shuffle=True,
                num_workers=self.config['batch_size'],
                pin_memory=True,
            )
            self.valloader = valloader
        else:
            valloader = self.valloader

        totLoss = 0.0
        with torch.no_grad():
            pbar = tqdm(total=len(valloader))
            for di, xdata in enumerate(valloader):
                torch_util.move_batch_to_device(xdata, self.device)
                xdata, targets = self.converter(xdata, xdata['obs']['rgb'].shape[0])
                mus, sig = self.model(xdata)
                loss = self.criterion(mus, sig, targets)
                totLoss += loss.cpu().item()
                desc = f'  Iter {di}: loss={(totLoss/(di+1)):.4f}'
                xdata = None
                targets = None
                mus = None
                sig = None
                loss = None
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
        return totLoss / len(valloader)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str,
        default="configs/base.json", help="path to config file")
    parser.add_argument("-v", "--verbose", default=False,
        help="if print all info")
    parser.add_argument("--continu", type=str, default=None,
        help="if continuing training from checkpoint model")
    opt = parser.parse_args()
    # print(opt)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    config = json.load(open(opt.config))

    trainer = DistrTrainer(config, device)

    if opt.continu is not None:
        checkpt = os.path.join( opt.continu )
        trainer.load_model(checkpt)

    trainer.train()


if __name__ == '__main__':
    main()
