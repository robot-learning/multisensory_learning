
from isaacRL.tasks.sim_task import SimTask
from isaacRL.tasks.gym_tasks import GymTask

from isaacRL.trainers.trainer_rl import TrainerRL
from isaacRL.models.ppo import PPO, OPPO
from isaacRL.utils.buffer import BufferRollout

import torch
import torch.nn as nn
import torch.nn.functional as F

import os, sys, argparse, json, random
import time
import os.path as osp
import numpy as np
from tqdm import tqdm
import rospy


class TrainerPPO(TrainerRL):
    """
    Holder of models and data for training and evaluating PPO
    Reinforcement Learning method.
    """

    def __init__(self, config):
        super(TrainerPPO, self).__init__(config)

        self.clip_grad = None
        if 'grad_norm' in self.config['training']:
            self.clip_grad = lambda x : nn.utils.clip_grad_norm_(
              x, self.config['training']['grad_norm']
            )
        self.clip_range = 0.1
        if 'clip_range' in self.config['training']:
            self.clip_range = self.config['training']['clip_range']
        self.value_loss_coef = self.config['training']['value_loss_coef']
        self.entropy_coef = self.config['training']['ent_coef']
        self.gamma = self.config['training']['gamma']
        self.lam = self.config['training']['lam']

        self.vlosses = []
        self.slosses = []

        self.buffer = BufferRollout(
          # self.config['training']['max_buff_size'],
          self.acts_per_interval,
          self.num_envs, self.batch_size,
          batch_num=self.config['training']['num_mini_batches']
        )
        self.buffer.device = self.device
        print('buffer size:', self.buffer.num_envs * self.config['training']['num_mini_batches'])

        obs_shape = (config['environ']['num_obs'],)
        self.action_space = (config['model']['actor']['actions'],)
        try:
            obs_shape = self.envir.obs_space.shape
            self.action_space = self.envir.act_space.shape
        except Exception as e:
            pass

        self.model = PPO(
          self.config['model'], obs_shape, self.action_space
        ).to(self.device)
        print(self.model)

        self.optimizer = torch.optim.Adam(
          self.model.parameters(), lr=self.lr, weight_decay=self.weight_decay
        )

        if 'expert_demos' not in self.config['training']:
            self.config['training']['expert_demos'] = 0
        if 'expert_epochs' not in self.config['training']:
            self.config['training']['expert_epochs'] = 0
        self.expert = None

    def train(self):

        if self.config['training']['expert_demos'] > 0:
            self.expert_demos(self.config['training']['load_experts'])

        interv = self.config['training']['checkpoint_interval']

        self.model.train()
        self.envir.reset()
        for epoch in range(self.start_epoch, self.end_epoch):
            if rospy.is_shutdown():
                return

            rewards = []
            pbar = tqdm(total=interv, file=sys.stdout)
            for di in range(interv):
                if rospy.is_shutdown():
                    return

                start = time.time()
                if (epoch-1) >= self.config['training']['expert_epochs']:
                    rewards.append( self.play(epoch) )
                play_time = time.time() - start

                # learn from replays
                start = time.time()
                vloss, sloss = self.update_weights(epoch)
                learn_time = time.time() - start
                self.vlosses.append( vloss )
                self.slosses.append( sloss )
                desc = f'  R {np.mean(rewards):.3f}: loss={np.mean(self.vlosses[-(di+1):]):.3f},{np.mean(self.slosses[-(di+1):]):.3f}'# time={play_time:.2f},{learn_time:.2f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            print('epoch:', epoch)
            # rounds = interv * self.acts_per_interval * epoch
            # print('envsteps {}'.format(self.envir.num_envs * rounds))
            # print('actsteps {}'.format(rounds))
            # rounds = interv * self.updates_per_interval * epoch
            # print('updates {}'.format(rounds))
            print('mean rew per step:', np.mean(rewards))
            print('mean rewards:', self.buffer.mean_rewards())
            print('mean vloss:', np.mean(self.vlosses[-interv:]))
            print('mean sloss:', np.mean(self.slosses[-interv:]))
            self.save_model(epoch)


    def play(self, playi):
        with torch.no_grad():
            state = self.envir.get_env_states()
            tot_rewards = 0.
            for step in range(self.acts_per_interval):
                if rospy.is_shutdown():
                    return tot_rewards
                actions, acts_logprob, values = self.model.act(state)
                nstate, rewards, terminals = self.envir.step(actions)
                tot_rewards += rewards.mean().item()
                # if tot_rewards < 0:
                #     import pdb; pdb.set_trace()
                self.buffer.add_transitions(
                  state, actions, rewards, terminals, values, acts_logprob
                )
                state = nstate
            actions, acts_logprob, last_values = self.model.act(state)
            self.buffer.compute_returns(last_values, self.gamma, self.lam)
            return tot_rewards/self.acts_per_interval


    def update_weights(self, update_i):

        mean_value_loss = 0
        mean_surrogate_loss = 0
        unvisited = True
        for epoch in range(self.updates_per_interval):
            if rospy.is_shutdown():
                return
            for batch in self.buffer:
                if rospy.is_shutdown():
                    return

                obs_batch, actions_batch, target_values_batch, returns_batch, old_actions_log_prob_batch, advantages_batch = batch

                actions_log_prob_batch, entropy_batch, value_batch = self.model.evaluate(
                  self.buffer.prep_state(obs_batch, self.device),
                  actions_batch.to(self.device)
                )

                # Surrogate loss
                ratio = torch.exp(
                  actions_log_prob_batch - torch.squeeze(old_actions_log_prob_batch)
                )
                surrogate = -torch.squeeze(advantages_batch) * ratio
                surrogate_clipped = -torch.squeeze(advantages_batch) * \
                 torch.clamp(ratio, 1.0 - self.clip_range, 1.0 + self.clip_range)
                surrogate_loss = torch.max(surrogate, surrogate_clipped).mean()

                # Value function loss
                value_loss = (returns_batch - value_batch).pow(2).mean()

                loss = surrogate_loss + self.value_loss_coef * value_loss - self.entropy_coef * entropy_batch.mean()
                # if unvisited:
                #     unvisited = False
                #     print('value loss:', value_loss)
                #     print('surrogate loss:', surrogate_loss)

                # Gradient step
                self.optimizer.zero_grad()
                loss.backward()
                if self.clip_grad is not None:
                    self.clip_grad(self.model.parameters())
                self.optimizer.step()

                mean_value_loss += value_loss.item()
                mean_surrogate_loss += surrogate_loss.item()
        num_updates = (
          self.updates_per_interval *
          self.config['training']['num_mini_batches']
        )
        mean_value_loss /= num_updates
        mean_surrogate_loss /= num_updates
        return mean_value_loss, mean_surrogate_loss

    @property
    def num_envs(self):
        return self.envir.num_envs

    def load_model(self, checkpt):
        checkpoint = torch.load(checkpt)
        self.model.load_state_dict(checkpoint['model'])
        if self.optimizer is not None:
            self.optimizer.load_state_dict(checkpoint['optimizer'])
        self.vlosses = checkpoint['vloss']
        self.slosses = checkpoint['sloss']
        # self.bestloss = checkpoint['bestloss']
        self.start_epoch = checkpoint['epochs']+1
        self.end_epoch += self.start_epoch

    def save_model(self, epoch):
        checkpoint = {}
        checkpoint['optimizer'] = self.optimizer.state_dict()
        checkpoint['model'] = self.model.state_dict()
        checkpoint['vloss'] = self.vlosses
        checkpoint['sloss'] = self.slosses
        # checkpoint['bestloss'] = self.bestloss
        checkpoint['epochs'] = epoch
        filename = osp.join(
          self.config['training']['checkpoint_path'],
          self.config['training']['checkpoint_name'].format(epoch)
        )
        torch.save(checkpoint, filename)

    def evaluate(self, steps=None):
        if steps is None:
            steps = self.config['training']['checkpoint_interval']
        self.model.eval()
        self.envir.reset()
        rewards = []
        pbar = tqdm(total=steps, file=sys.stdout)
        for di in range(steps):
            if rospy.is_shutdown():
                break

            start = time.time()
            rewards.append( self.play(di) )
            play_time = time.time() - start

            desc = f'  R {rewards[-1]:.3f}'
            pbar.set_description(desc)
            pbar.update(1)
        pbar.close()
        return rewards
