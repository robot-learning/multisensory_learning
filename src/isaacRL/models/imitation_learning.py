
import torch

from isaacRL.models.models import FCNet


class DeterministicActor(torch.nn.Module):
    """docstring for DeterministicActor."""

    def __init__(self, insize, action_size, cfg={}, zero_start=False):
        super(DeterministicActor, self).__init__()
        self.insize = insize
        self.action_size = action_size
        self.cfg = cfg

        _scale = cfg.get('action_scale', 1.0)
        _bias = cfg.get('action_bias', 0.0)
        self.epsilon = cfg.get('epsilon', 1e-5)

        self.action_scale = torch.nn.Parameter(torch.ones((1,action_size))*_scale)
        self.action_scale.requires_grad = False
        self.action_bias = torch.nn.Parameter(torch.ones((1,action_size))*_scale)
        self.action_bias.requires_grad = False

        self.model = FCNet(
            infeats=self.insize,
            outfeats=action_size,
            layers=cfg.get('layers', [100]*3),
            activation=cfg.get('activation', 'relu'),
            batchnorm=cfg.get('batchnorm', False)
        )

    def forward(self, xdata):
        act = self.model(xdata['obs'])
        action = torch.tanh(act) * self.action_scale + self.action_bias
        return action
