#!/usr/bin/env python

import rospy

from multisensory_learning.srv import (
    VisualizeTrajectorySamples, VisualizeTrajectorySamplesResponse
)


class TrajectorySampleVisualizer:

    def __init__(self):

        self.rate = rospy.Rate(10)

        self.publishers = []
        self.msgs = []
        self.robot_states = []
        self.n_steps = 0
        
        
        rospy.Service("/visualization/visualize_trajectory_samples",
                      VisualizeTrajectorySamples, self._visualize_samples_srv)

    def run(self):
        while not rospy.is_shutdown():
            if self.publishers:
                for t in range(self.n_steps):
                    for i, publisher in enumerate(self.publishers):
                        msgs[i].state = self.robot_states[i][t]
                        publisher.publish(msgs[i])
                    self.rate.sleep()
            self.rate.sleep()

    def _visualize_samples_srv(self, req):
        if len(req.samples) != len(values):
            rospy.logerr("Number of samples and values must be equal")
            return VisualizeTrajectorySamplesResponse(False)
        
        self.publishers = []
        self.msgs = []
        self.robot_states = []
        self.n_steps = len(samples[0].joint_trajectory.points)
        for i, (value, sample) in enumerate(sorted(zip(values, samples))):
            pub = rospy.Publisher(f"/visualization/trajectory_sample_{i+1}",
                                  DisplayRobotState, queue_size=1)
            self.publishers.append(pub)
            msg = DisplayRobotState()
            # TODO need to set colors based on scaling between min/max over a color gradient

            sample_robot_states = []
            for point in sample.joint_trajectory.points:
                robot_state = RobotState()
                robot_state.joint_state.position = point.positions
                sample_robot_states.append(robot_state)
            
            self.robot_states.append(sample_robot_states)

            
            
        


        return VisualizeTrajectorySamplesResponse(True)


        
    
    
if __name__ == '__main__':
    rospy.init_node('trajectory_sample_visualizer')
    visualizer = TrajectorySampleVisualizer()
    visualizer.run()
