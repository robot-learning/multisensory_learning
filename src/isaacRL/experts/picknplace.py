
# from isaacRL.experts.trajopt import TrajectoryOptimizer
from ll4ma_isaacgym.behaviors.planners.trajopt import TrajectoryOptimizer
from isaacRL.experts.jacobian_resolver import JacobianResolver
from ll4ma_isaacgym.behaviors.planners import jacobian_controller

from isaacgym import torch_utils
from ll4ma_util import math_util, torch_util
import moveit_interface.util as moveit_util

import torch

class PicknplaceExpert:
    """docstring for PicknplaceExpert."""

    def __init__(
        self,
        action_size,
        target_object,
        same_starts=True,
        grasp_offset=None,
        target_prior=None,
        planner='trajopt',
        action_repeats=2,
        max_iterations=150,
        num_joints=7,
        joint_names=[],
        ee_link='reflex_palm_link',
        grasp_value=1,
        ungrasp_value=-1,
        grasp_neutral=0,
        cartesian_path=False
    ):
        self.target_object = target_object
        self.same_starts = same_starts
        self.planner = planner
        self.action_repeats = action_repeats
        self.cartesian_path = cartesian_path
        # self._downward_ee = torch.tensor([-0.080, -0.629,  0.084, -0.769]) # torch.tensor([-0.764, -0.013, 0.645, -0.034]) # torch.tensor([0.60909081,  0.36338773, -0.60599518,  0.36017716])
        # x_axis, y_axis, z_axis = torch.eye(3)
        # rot = torch.stack([-z_axis, -y_axis, torch.cross(-z_axis, -y_axis)])
        # self._downward_ee = math_util.rotation_to_quat(rot)
        self._downward_ee = torch.tensor([0.07469838112592697, -0.6329322457313538, -0.042158689349889755, -0.7694410681724548])
        if grasp_offset is None:
            grasp_offset = torch.cat(
                [torch.tensor([0., 0., 0.1]), self._downward_ee]
            )
        self.grasp_offset = grasp_offset.unsqueeze(0)
        self.lift_offset = torch.tensor([0., 0., 0.2]).unsqueeze(0)
        self.set_offset = torch.tensor([0., 0., 0.1]).unsqueeze(0)
        if target_prior is None:
            target_prior = torch.cat(
                [torch.tensor([0., 0., 0.15]), self._downward_ee]
            )
        else:
            raise('Blah')
        self.target_prior = target_prior.unsqueeze(0)

        self.num_joints = num_joints
        self.ee_link = ee_link
        self.action_size = self.num_joints + 1

        self.timesteps = 10
        if self.cartesian_path:
            self.total_steps = self.timesteps
            self.interp_steps = 1
        else:
            self.total_steps = 140
            self.interp_steps = self.total_steps // self.timesteps
        self.max_iterations = max_iterations
        self.trajopt = TrajectoryOptimizer(
            timesteps=self.timesteps,
            alpha=0.5,
            rho=0.5,
            FP_PRECISION=1e-8,
            max_iterations=self.max_iterations,
            ee_link=self.ee_link,
            ee_trajectory=self.cartesian_path
        )
        self.trajopt.init_noise = 0.03
        self.temp_verbose = False
        # self.trajopt.verbose = True
        # self.error_type = 'position'
        self.error_type = 'pose'
        self.trajopt.error_type = self.error_type

        self.grasp_steps = 80
        self.ungrasp_steps = 80
        self.lift_steps = 80 # int(self.total_steps * 3 / 4) + 1

        if self.same_starts:
            self.setup_steps = 1 + self.grasp_steps + self.lift_steps
        else:
            self.setup_steps = self.total_steps + self.grasp_steps + self.lift_steps
        self.action_steps = self.total_steps + 30 + 0
        self.setdown_steps = self.ungrasp_steps + self.lift_steps

        self.jac_move_steps = 60
        self.jac_lift_steps = 50

        self.reset_setup()
        self.grasp_value = grasp_value
        self.ungrasp_value = ungrasp_value
        self.grasp_neutral = grasp_neutral

        self._goal_key = 'goal_noisy'
        self._ee_key = 'ee_state'
        self._joint_key = 'joint_position'
        self._jac_key = 'jacobian'

    def reset_setup(self):
        self.move2grasp = None
        self.lift = None

    def get_setup_actions(self, state, envis=None):
        state = state['full_state']
        num_envs = state.shape[0]
        if envis is None:
            envis = torch.arange(num_envs)
        start_joints = state[self._joint_key][:,:self.num_joints]
        start_pose = state[self._ee_key][:,:self.num_joints]
        object_state = state[self.target_object]
        jacobian = state[self._jac_key].reshape(num_envs,6,self.num_joints)

        # move 2 grasping pose
        target_pose = object_state.clone()
        target_pose[:,0:3] += self.grasp_offset[:,0:3].to(state.device)
        if self.lift is None:
            self.grasp_offset = self.grasp_offset.to(state.device)
            self.grasp_offset[:,3:7] = start_pose[0,3:7]
            self.target_prior[:,3:7] = start_pose[0,3:7]
        target_pose[:,3:7] = self.grasp_offset[:,3:7]
        no_traj = False
        move2grasp = None
        if (self.same_starts and self.move2grasp is None) or not self.same_starts:
            # check to see if the current ee pose is close enough to the target to not plan
            dists = l2_dist(target_pose[:,:3], start_pose[:,:3])[0]
            no_traj = dists < 0.1
            if no_traj:
                move2grasp = torch.zeros(
                    (1, num_envs, self.action_size),
                    device=start_joints.device, dtype=start_joints.dtype
                )
                move2grasp[0,:,:self.num_joints] = start_joints[:,:self.num_joints]
        if self.same_starts:
            if move2grasp is not None:
                self.move2grasp = move2grasp
            else:
                if self.temp_verbose:
                    temp = self.trajopt.verbose
                    self.trajopt.verbose = True
                self.trajopt.max_iterations = 500
                self.move2grasp, info = self._get_actions(
                    start_joints,
                    start_pose,
                    target_pose,
                    state.data,
                    num_envs,
                    envis,
                    identical=True
                )
                if self.temp_verbose:
                    self.trajopt.verbose = temp
                self.trajopt.max_iterations = self.max_iterations
                move2grasp = self.move2grasp.clone()
        elif move2grasp is None:
            move2grasp, _ = self._get_actions(
                start_joints,
                start_pose,
                target_pose,
                state.data,
                num_envs,
                envis
            )

        # grasp object
        grasp = torch.zeros(
            (self.grasp_steps, num_envs, self.action_size),
            device=state.device, dtype=start_joints.dtype
        )
        grasp[:,:,:self.num_joints] = move2grasp[-1:,:,:self.num_joints]
        grasp[:,:,-1] = self.grasp_value

        # lift object
        target_pose[:,0:3] += self.lift_offset[:,0:3].to(state.device)
        if self.same_starts:
            if self.lift is None: # or self._lift_info['cost'] > 1.5:
                if self.temp_verbose:
                    temp = self.trajopt.verbose
                    self.trajopt.verbose = True
                self.trajopt.max_iterations = 500
                self.lift, self._lift_info = self._get_actions(
                    move2grasp[-1,:,:self.num_joints],
                    start_pose,
                    target_pose,
                    state.data,
                    num_envs,
                    envis,
                    identical=True,
                    repeats=5,
                    interp=(self.lift_steps//self.timesteps)
                )
                if self.temp_verbose:
                    self.trajopt.verbose = temp
                self.trajopt.max_iterations = self.max_iterations
                self.lift[:,:,-1] = self.grasp_value
            lift = self.lift.clone()
        else:
            if no_traj:
                lift_idx = envis
                lift = move2grasp[-1,:,:self.num_joints].clone()
                acts, _ = self._get_controls(
                    move2grasp[-1,:,:self.num_joints],
                    start_pose[lift_idx],
                    target_pose[lift_idx],
                    jacobian[lift_idx],
                    state.data,
                    lift_idx,
                )
                lift += acts
                lift = torch.cat([lift, torch.zeros_like(start_joints[:,0:1])+self.grasp_value], dim=1)
                lift = torch.stack([lift]*self.jac_lift_steps)
            else:
                lift, _ = self._get_actions(
                    move2grasp[-1,:,:self.num_joints],
                    start_pose,
                    target_pose,
                    state.data,
                    num_envs,
                    envis,
                    repeats=self.action_repeats,
                    interp=(self.lift_steps//self.timesteps)
                )
                lift[:,:,-1] = self.grasp_value
        return torch.cat((move2grasp, grasp, lift), dim=0)[:,envis]

    def get_target_pose(self, state):
        target_pose = state[self._goal_key].clone()
        target_pose[:,0:3] += self.target_prior[:,0:3].to(target_pose.device)
        target_pose[:,3:7] = self.target_prior[:,3:7].to(target_pose.device)
        return target_pose

    def _get_moveaboveactions(self, state, target_pose=None, envis=None, interp=None):
        state = state['full_state']
        num_envs = state.shape[0]
        if envis is None:
            envis = torch.arange(num_envs)
        start_joints = state[self._joint_key][:,:self.num_joints]
        start_pose = state[self._ee_key][:,:self.num_joints].clone()
        object_state = state[self.target_object]

        # move 2 target pose
        if target_pose is None:
            target_pose = self.get_target_pose(state)
        target_pose = target_pose.clone()
        target_pose[:,2] += self.set_offset[:,2].to(state.device)
        moveabovepose, info = self._get_actions(
            start_joints,
            start_pose,
            target_pose,
            state,
            num_envs,
            envis,
            repeats=self.action_repeats,
            interp=interp
        )
        moveabovepose[:,:,-1] = self.grasp_value
        return moveabovepose

    def _get_moveactions(self, state, start_joints, target_pose=None, envis=None, no_interp=False):
        state = state['full_state']
        num_envs = state.shape[0]
        if envis is None:
            envis = torch.arange(num_envs)
        start_pose = state[self._ee_key][:,:self.num_joints].clone()
        object_state = state[self.target_object]

        # move 2 target pose
        if target_pose is None:
            target_pose = self.get_target_pose(state)
        interp = (self.lift_steps//self.timesteps) if not no_interp else 1
        move2pose, _ = self._get_actions(
            start_joints,
            start_pose,
            target_pose,
            state,
            num_envs,
            envis,
            repeats=self.action_repeats,
            interp=interp
        )
        move2pose[:,:,-1] = self.grasp_value
        return move2pose

    def get_actions(self, state, target_pose=None, envis=None, interp=None):
        num_envs = state['full_state'].shape[0]
        if envis is None:
            envis = torch.arange(num_envs)

        moveabovepose = self._get_moveaboveactions(state, target_pose, envis, interp)

        move2pose = self._get_moveactions(state, moveabovepose[-1,:,:self.num_joints], target_pose, envis, interp)

        return torch.cat((moveabovepose, move2pose), dim=0)[:,envis]

    def get_setdown_actions(self, state, envis=None):
        state = state['full_state']
        num_envs = state.shape[0]
        if envis is None:
            envis = torch.arange(num_envs)
        start_joints = state[self._joint_key][:,:self.num_joints]
        start_pose = state[self._ee_key][:,:self.num_joints].clone()
        jacobian = state[self._jac_key].reshape(num_envs,6,self.num_joints)

        # ungrasp object
        ungrasp = start_joints.clone()
        ungrasp = torch.cat([ungrasp, torch.zeros_like(ungrasp[:,0:1])], dim=1)
        ungrasp[:,-1] = self.ungrasp_value
        ungrasp = torch.stack([ungrasp]*self.ungrasp_steps)

        target_pose = state[self._ee_key][:,:self.num_joints].clone()
        goal = state[self._goal_key].clone()
        moveabove = (target_pose[envis,:3] - goal[envis,:3]).abs().sum(dim=1) < 0.2
        if not moveabove.any():
            return ungrasp[:,envis]

        lift_idx = envis[moveabove.cpu()]
        movelift = torch.cat(
            [start_joints.clone(), torch.zeros_like(start_joints[:,0:1])], dim=1
        )
        movelift = torch.stack([movelift]*self.jac_lift_steps)
        movelift[:,:,-1] = self.ungrasp_value
        # TODO: only plan for the environments that need it
        target_pose[:,2] += self.set_offset[:,2].to(state.device)
        acts, _ = self._get_controls(
            start_joints[lift_idx],
            start_pose[lift_idx],
            target_pose[lift_idx],
            jacobian[lift_idx],
            state,
            lift_idx,
        )
        movelift[:,lift_idx,:-1] += torch.stack([acts]*self.jac_lift_steps)

        return torch.cat((ungrasp, movelift), dim=0)[:,envis]

    def _get_actions(
        self,
        start_joints,
        start_pose,
        target_pose,
        obj_state,
        num_envs,
        envis,
        identical=False,
        repeats=1,
        interp=None
    ):
        if interp is None:
            interp = self.interp_steps
        if self.cartesian_path:
            interp = 1
        actions = torch.zeros(
            (interp*self.timesteps, num_envs, self.action_size),
            device=start_joints.device, dtype=start_joints.dtype
        )
        actions[:,:,-1] = self.grasp_neutral
        infos = []
        err_types = ['pose' for _ in range(len(target_pose))]
        if self.error_type == 'position':
            ik_resp, success = moveit_util.get_ik(
                target_pose.cpu().numpy(), self.ee_link, 5
            )
            sols = [
                torch.tensor(sol.position, dtype=target_pose.dtype, device=target_pose.device)
                    if ik_resp.solution_found[si] else target_pose[si]
                        for si, sol in enumerate(ik_resp.solutions)
            ]
            err_types = [
                'position'
                    if ik_resp.solution_found[si] else 'pose'
                        for si, sol in enumerate(ik_resp.solutions)
            ]
            target_pose = torch.stack(sols)
        for ei in envis:
            if not identical or ei == 0:
                trajectory, info = self.trajopt.get_trajectory(
                    start_joints[ei].cpu(),
                    target_pose[ei].cpu(),
                    obj_state,
                    interp=interp,
                    repeats=repeats,
                    goal_ee_type=err_types[ei]
                )
                infos.append(info)
            actions[:,ei,:self.num_joints] = trajectory[:,:].to(actions.device)
        if identical:
            return actions, info
        return actions, infos
        # if interp is None:
        #     interp = self.interp_steps
        # err_types = ['pose' for _ in range(len(target_pose))]
        # actions, infos = [], []
        # for ei in envis:
        #     if not identical or ei == 0:
        #         trajectory, info = self.trajopt.get_trajectory(
        #             start_joints[ei].cpu(),
        #             target_pose[ei].cpu(),
        #             obj_state,
        #             interp=interp,
        #             repeats=repeats,
        #             goal_ee_type=err_types[ei]
        #         )
        #         infos.append(info)
        #     actions.append(trajectory[:,:])
        # actions = torch.stack(actions)
        # actions = torch.cat((actions, torch.zeros((actions.shape[0], actions.shape[1], 1))), dim=-1)
        # actions[:,:,-1] = self.grasp_neutral
        # actions = actions.to(device=start_joints.device).to(dtype=start_joints.dtype)
        # return actions, infos

    def _get_controls(
        self,
        start_joints,
        start_pose,
        target_pose,
        jacobian,
        obj_state,
        envis,
        identical=False,
    ):
        return jacobian_controller.get_action(start_pose, target_pose, jacobian), {}

    def reset_idx(self, envis):
        pass

def l2_dist(vec1, vec2):
    return ((vec1 - vec2)**2).sum(dim=1)

class PicknplaceStepExpert(PicknplaceExpert):
    """docstring for PicknplaceStepExpert."""

    def __init__(
        self,
        action_size,
        target_object,
        same_starts=True,
        grasp_offset=None,
        target_prior=None,
        planner='trajopt',
        num_envs=1,
        action_repeats=1,
        max_iterations=50,
        num_joints=7,
        joint_names=[],
        ee_link='reflex_palm_link',
        cartesian_path=False
    ):
        super(PicknplaceStepExpert, self).__init__(
            action_size,
            target_object,
            same_starts,
            grasp_offset,
            target_prior,
            planner,
            action_repeats,
            max_iterations,
            num_joints,
            joint_names,
            ee_link,
            cartesian_path=cartesian_path
        )

        self.num_envs = num_envs
        self.action_idx = torch.zeros((self.num_envs,), dtype=torch.long)
        self.max_length = torch.tensor([self.total_steps-1]*self.num_envs, dtype=torch.long)
        self.actions = torch.zeros((self.num_envs,self.total_steps,self.action_size))
        self.env_idx = torch.arange(self.num_envs)

    def set_actions(self, envis, actions):
        if actions.device != self.actions.device:
            self.actions = self.actions.to(actions.device)
        self.actions[envis] = actions
        self.action_idx[envis] *= 0

    def get_actions(self, state, target_pose=None, envis=None, no_interp=False, take_step=True):
        if self.actions.device != state['full_state'].device:
            self.actions = self.actions.to(state['full_state'].device)
        if (self.action_idx == 0).any():
            moveabove_idx = self.env_idx[self.action_idx == 0]
            moveaboveactions = self._get_moveaboveactions(
                state, target_pose, moveabove_idx, interp=None
            ).transpose(1, 0)
            self.actions[moveabove_idx] = moveaboveactions.to(state['full_state'].device)
        actions = self.actions[self.env_idx,self.action_idx]
        if (self.action_idx == self.max_length).any():
            num_envs = state['full_state'].shape[0]
            start_pose = state['full_state'][self._ee_key][:, :self.num_joints].clone()
            jacobian = state['full_state'][self._jac_key].reshape(num_envs,6,self.num_joints)
            if target_pose is None:
                target_pose = self.get_target_pose(state['full_state'])
            control, _ = self._get_controls(
                None,
                start_pose[self.action_idx == self.max_length].clone(),
                target_pose[self.action_idx == self.max_length].clone(),
                jacobian[self.action_idx == self.max_length].clone(),
                None,
                None,
            )
            actions[self.action_idx == self.max_length, :self.num_joints] = (
                control + state['full_state'][self._joint_key][:, :self.num_joints]
            )
        if take_step:
            self.action_idx = torch.minimum(self.action_idx+1, self.max_length)
        return actions

    def reset_idx(self, envis, state, target_pose=None):
        self.action_idx[envis] *= 0

class PicknplaceTrajStepExpert(PicknplaceExpert):
    """docstring for PicknplaceTrajStepExpert."""

    def __init__(
        self,
        action_size,
        target_object,
        same_starts=True,
        grasp_offset=None,
        target_prior=None,
        planner='trajopt',
        num_envs=1,
        action_repeats=1,
        max_iterations=50,
        num_joints=7,
        joint_names=[],
        ee_link='reflex_palm_link',
        cartesian_path=False
    ):
        super(PicknplaceTrajStepExpert, self).__init__(
            action_size,
            target_object,
            same_starts,
            grasp_offset,
            target_prior,
            planner,
            action_repeats,
            max_iterations,
            num_joints,
            joint_names,
            ee_link,
            cartesian_path
        )

        self.num_envs = num_envs
        self.action_idx = torch.zeros((self.num_envs,), dtype=torch.long)
        self.max_length = torch.tensor([self.action_steps-1]*self.num_envs, dtype=torch.long)
        self.actions = torch.zeros((self.num_envs,self.action_steps,self.action_size))
        self.env_idx = torch.arange(self.num_envs)

    def set_actions(self, envis, actions):
        if actions.device != self.actions.device:
            self.actions = self.actions.to(actions.device)
        self.actions[envis] = actions
        self.action_idx[envis] *= 0

    def get_actions(self, take_step=True):
        actions = self.actions[self.env_idx,self.action_idx]
        if take_step:
            self.action_idx = torch.minimum(self.action_idx+1, self.max_length)
        return actions

    def reset_idx(self, envis, state, target_pose=None):
        actions = super().get_actions(state, target_pose, envis)
        if actions.device != self.actions.device:
            self.actions = self.actions.to(actions.device)
        self.actions[envis] = actions.transpose(0,1)
        self.action_idx[envis] *= 0

class PicknplaceController(PicknplaceTrajStepExpert):
    """docstring for PicknplaceController."""

    def __init__(
        self,
        action_size,
        target_object,
        same_starts=True,
        grasp_offset=None,
        target_prior=None,
        planner='trajopt',
        num_envs=1,
        action_repeats=1,
        max_iterations=50,
        num_joints=7,
        joint_names=[],
        ee_link='reflex_palm_link',
    ):
        super(PicknplaceController, self).__init__(
            action_size,
            target_object,
            same_starts,
            grasp_offset,
            target_prior,
            planner,
            num_envs,
            action_repeats,
            max_iterations,
            num_joints,
            joint_names,
            ee_link
        )
        self.actions = torch.zeros((self.num_envs,self.action_size))
        self.action_steps = self.jac_move_steps + self.jac_lift_steps

    def get_actions(self, state, target_pose=None, envis=None, take_step=True):
        state = state['full_state']
        num_envs = state.shape[0]
        if envis is None:
            envis = torch.arange(num_envs)
        start_joints = state[self._joint_key][:,:self.num_joints]
        start_pose = state[self._ee_key][:,:self.num_joints].clone()
        object_state = state[self.target_object]
        jacobian = state[self._jac_key].reshape(num_envs,6,self.num_joints)

        # move 2 target pose
        if target_pose is None:
            target_pose = state[self._goal_key].clone()
            target_pose[:,0:3] += self.target_prior[:,0:3]
            target_pose[:,3:7] = self.target_prior[:,3:7]

        actions = self.actions
        actions[:,:self.num_joints] = start_joints[:,:self.num_joints]
        actions[envis,-1] = self.grasp_value

        moveabove_idx = envis[self.action_idx[envis] < self.jac_move_steps]
        move2pose_idx = envis[self.action_idx[envis] >= self.jac_move_steps]

        if len(moveabove_idx) > 0:
            target_pose[:,2] += self.set_offset[:,2]
            acts, info = self._get_controls(
                start_joints[moveabove_idx],
                start_pose[moveabove_idx],
                target_pose[moveabove_idx],
                jacobian[moveabove_idx],
                state,
                moveabove_idx,
            )
            actions[moveabove_idx,:self.num_joints] += acts
            target_pose[:,2] -= self.set_offset[:,2]
        if len(move2pose_idx) > 0:
            acts, _ = self._get_controls(
                start_joints[move2pose_idx],
                start_pose[move2pose_idx],
                target_pose[move2pose_idx],
                jacobian[move2pose_idx],
                state,
                move2pose_idx,
            )
            actions[move2pose_idx,:self.num_joints] += acts

        if take_step:
            self.action_idx[envis] = torch.minimum(
                self.action_idx[envis]+1, self.max_length[envis]
            )
        return actions[envis]

    def reset_idx(self, envis, state, target_pose=None):
        self.action_idx[envis] *= 0
        if state['fullstate'].device != self.target_prior.device:
            self.target_prior = self.target_prior.to(state['fullstate'].device)
        if state['fullstate'].device != self.lift_offset.device:
            self.lift_offset = self.lift_offset.to(state['fullstate'].device)
        if state['fullstate'].device != self.set_offset.device:
            self.set_offset = self.set_offset.to(state['fullstate'].device)
        if state['fullstate'].device != self.actions.device:
            self.actions = self.actions.to(state['fullstate'].device)
