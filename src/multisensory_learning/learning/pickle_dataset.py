import sys
import torch
import numpy as np

from ll4ma_util import file_util


class PickleDataset(torch.utils.data.Dataset):
    """
    Loads samples using pickle (one sample per file) where it's assumed 
    the sample is a (possibly nested) dictionary of numpy arrays.
    """

    def __init__(self, filenames):
        super().__init__()
        self.filenames = filenames

    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, idx):
        filename = self.filenames[idx]
        sample = file_util.load_pickle(filename)
        self._tensorize(sample)        
        return sample

    def _tensorize(self, instance):
        if isinstance(instance, dict):
            for k, v in instance.items():
                instance[k] = self._tensorize(v)
            return instance
        elif isinstance(instance, tuple):
            return tuple([self._tensorize(v) for v in instance])
        elif isinstance(instance, list):
            return [self._tensorize(v) for v in instance]
        elif isinstance(instance, np.ndarray):
            return torch.from_numpy(instance)
        else:
            # Catch-all is just return the same thing
            return instance
