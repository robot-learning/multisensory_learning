#!/usr/bin/env python
import os
import os.path as osp
import argparse
import subprocess
import rospy
from tqdm import tqdm

from multisensory_learning.learning import SkillLearnerDataset, SKILLS, OBJECTS

from ll4ma_util import file_util, ui_util, ros_util

CONFIG_DIR = osp.join(
    ros_util.get_path("multisensory_learning"),
    "src",
    "multisensory_learning",
    "config",
    "isaacgym"
)


def run_nominal_data_collection(config_fn, data_dir, n_envs, n_demos):
    try:
        subprocess.call([
            'rosrun', 'll4ma_isaacgym', 'run_isaacgym.py',
            '--config_dir', CONFIG_DIR,
            '--config', config_fn,
            '--data_root', data_dir,
            '--n_envs', str(n_envs),
            '--n_demos', str(n_demos),
            '--log_only_success',
            '--log_only_behavior_intervals',
            '--headless'
        ])
    except KeyboardInterrupt:
        sys.exit()


def run_perturbed_data_collection(nominal_data_dir, behavior_init, n_perturbations):
    try:
        subprocess.call([
            'rosrun', 'multisensory_learning', 'collect_perturbed_data.py',
            '--nominal_data_dir', nominal_data_dir,
            '--behavior_init', behavior_init,
            '--n_perturbations', str(n_perturbations),
            '--headless'
        ])
    except KeyboardInterrupt:
        sys.exit()


def run_create_samples(data_root, skill, obj, regenerate=False):
    sample_dir = osp.join(data_root, f"{skill}_{obj}_samples")
    if regenerate:
        file_util.force_create_dir(sample_dir)
    else:
        if not file_util.safe_create_dir(sample_dir, False):
            return  # This means user wants to keep those samples
    data_dirs = [d for d in file_util.list_dir(data_root, exclude_files=True)
                 if f"{skill}_{obj}_data" in d]
    if len(data_dirs) == 0:
        ui_util.print_warning(f"No data found for skill {skill.upper()} and object "
                              f"{obj.upper()}. Skipping sample creation.")
        return
    # Copy over metadata
    file_util.copy_file(
        osp.join(data_dirs[0], 'metadata.yaml'),
        osp.join(sample_dir, 'metadata.yaml')
    )
    fns = [f for data_dir in data_dirs for f in file_util.list_dir(data_dir, '.pickle')]
    dataset = SkillLearnerDataset(fns)
    for i, sample in enumerate(tqdm(dataset, desc=f"{skill}-{obj}")):
        filename = osp.join(sample_dir, f"sample_{i+1:06d}.pickle")
        file_util.save_pickle(sample, filename)


if __name__ == '__main__':
    """
    This script combines nominal and perturbed data collection for skills.
    """
    rospy.init_node('collect_skill_data')
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--skills', type=str, nargs='+', choices=SKILLS,
                        help="List of skills to operate on")
    parser.add_argument('-o', '--objects', type=str, nargs='+', choices=OBJECTS,
                        help="List of objects to operate on")
    parser.add_argument('--data_root', type=str, default=None,
                        help="Absolute path to data root directory (containing sample dirs)")
    parser.add_argument('--n_demos', type=int, default=1000,
                        help="Number of demos per config")
    parser.add_argument('--n_envs', type=int, default=8,
                        help="Number of envs in Isaac Gym")
    parser.add_argument('--n_perturbations', type=int, default=20,
                        help="Number of perturbation instances to generate per nominal sample")
    parser.add_argument('--n_noops', type=int, default=5,
                        help="Number of no-op instances to collect (only for --noop mode)")
    parser.add_argument('--nominal', action='store_true',
                        help="Enable nominal data collection")
    parser.add_argument('--perturbed', action='store_true',
                        help="Enable perturbed data collection (perturb nominal instances)")
    parser.add_argument('--create_samples', action='store_true',
                        help="Create samples for learning from collected data")
    parser.add_argument('--run_all', action='store_true',
                        help="Run every mode sequentially")
    parser.add_argument('--all_skills', action='store_true',
                        help="Run active mode for all discovered skills")
    parser.add_argument('--all_objects', action='store_true',
                        help="Run active mode for all discovered objects")
    parser.add_argument('--regenerate_samples', action='store_true',
                        help="Regenerate samples for learning (will delete old samples)")
    args = parser.parse_args()

    if not (args.nominal or args.perturbed or args.create_samples or args.run_all):
        ui_util.print_error_exit("\n  Must specify at least one of these options:\n"
                                 "    --nominal\n"
                                 "    --perturbed\n"
                                 "    --create_samples\n"
                                 "    --run_all\n")
    if not (args.skills or args.all_skills):
        ui_util.print_error_exit("\n  Must specify either a list of skills to the "
                                 "--skill (-s) option or enable --all_skills\n")
    if not (args.objects or args.all_objects):
        ui_util.print_error_exit("\n  Must specify either a list of obects to the "
                                 "--objects (-o) option or enable --all_objects\n")
    
    if args.data_root is not None:
        data_root = args.data_root
    else:
        data_root = os.environ.get("DATA_ROOT")
        if data_root is None:
            ui_util.print_error_exit(
                "\n  Need to specify data root either as environment variable "
                "DATA_ROOT or as script input with --data_root (-d) flag\n"
            )
    if not osp.exists(data_root):
        ui_util.print_error_exit(f"\n  Data root does not exist: {data_root}\n")

    if args.all_skills:
        ui_util.print_warning("Running for all skills")
    if args.all_objects:
        ui_util.print_warning("Running for all objects")
        
    skills = args.skills if args.skills else SKILLS
    objects = args.objects if args.objects else OBJECTS
    for skill in skills:
        for obj in objects:
            config_fns = [f for f in file_util.list_dir(CONFIG_DIR)
                          if f"{skill}_{obj}" in f]
            for config_fn in config_fns:
                basename = osp.basename(config_fn)
                prefix = f"{skill}_{obj}"
                nominal_data_dir = basename.replace('.yaml', '')
                nominal_data_dir = nominal_data_dir.replace(prefix, f"{prefix}_data")
                nominal_data_dir = osp.join(data_root, nominal_data_dir)
            
                if args.nominal or args.run_all:
                    ui_util.print_info(
                        f"\n  Running nominal data collection for config: {basename}"
                    )
                    run_nominal_data_collection(
                        config_fn,
                        nominal_data_dir,
                        args.n_envs,
                        args.n_demos
                    )
                    ui_util.print_happy(
                        f"\n  Nominal data collection for config '{basename}' complete"
                    )
            
                if args.perturbed or args.run_all:
                    ui_util.print_info(
                        f"\n  Running perturbed data collection for config: {basename}"
                    )
                    if skill == 'push':
                        behavior_init = 'push_object:push'
                    elif skill == 'pick_place':
                        behavior_init = 'pick_place:pick:grasp'
                    run_perturbed_data_collection(
                        nominal_data_dir,
                        behavior_init,
                        args.n_perturbations
                    )
                    ui_util.print_happy(
                        f"\n  Perturbed data collection for config '{basename}' complete"
                    )
            
            if args.create_samples or args.run_all:
                ui_util.print_info(f"\n  Creating samples for skill {skill.upper()} and "
                                   f"object {obj.upper()}")
                run_create_samples(data_root, skill, obj, args.regenerate_samples)
                ui_util.print_happy(f"\n  Samples created for skill {skill.upper()} and "
                                    f"object {obj.upper()}")
