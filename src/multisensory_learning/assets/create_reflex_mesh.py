#!/usr/bin/env python
import trimesh
from urdfpy import URDF

from ll4ma_util import math_util


if __name__ == '__main__':
    robot = URDF.load('reflex.urdf')
    mesh = trimesh.util.concatenate(
        [m.apply_transform(T) for m, T in robot.visual_trimesh_fk().items()]
    )
    
    for link, T in robot.link_fk().items():
        if link.name == 'reflex1_palm_link':
            mesh.apply_transform(math_util.homogeneous_inverse(T))
            break
        
    # mesh.show()
    mesh.export('reflex.stl')
    
