# Adam's Commands

This is for Adam to manage commands he might need to copy/paste into a terminal

## Isaac Gym
Generate the shelf mesh/urdf:
```bash
python shelf_generator.py --width 0.5 --depth 0.5 --height 0.7 --n_shelves 1 --exclude_bottom_wall --save_dir ~/catkin_ws/src/ll4ma_isaac/ll4ma_isaacgym/src/ll4ma_isaacgym/assets
```

Run shelf env:
```bash
rosrun ll4ma_isaacgym run_isaacgym.py --config_dir /home/adam/catkin_ws/src/multisensory_learning/src/multisensory_learning/config/isaacgym --config push_cleaner_shelf.yaml
```


## Data Collection
They will all be of this form, changing the flag `-s` for skill and `-o` for object, for example:
```bash
rosrun multisensory_learning collect_skill_data.py -s push -o cleaner
```
This script is a gateway to multiple modes including `--nominal` (collect original data in Isaac Gym), `--perturbed` (for each nominal instance, create an instance in Isaac Gym where object pose is perturbed, and execute nominal action on the env), and `--create_samples` (generate samples for learning). There is also a `--run_all` flag to do all of these sequentially. 

### No-op Data
It's been necessary to collect data for "no-ops", i.e. actions that result in no change of state. This is done with this script (with example flags I've been using for push cleaner):
```bash
rosrun multisensory_learning collect_noop_data.py -s push -o cleaner
```
Note I did this separately from the other data collection script because it was easier to leave standalone than spend the time integrating with my other script.

### Transfer data between machines
Can sync the data drive to the shared drive with this command:
```bash
rclone sync /media/adam/data_adam ll4ma_drive:Datasets/adam_skill_data --exclude "/*_samples/**" --progress --bwlimit8.2M
```

---


## Model Training

### Train Ortho6d model
```bash
rosrun multisensory_learning ortho6d_learner.py --checkpoint_dir $CHECKPOINT_ROOT/ortho6d_cp
```

### Train Skill Models


#### Regressor Matrix Log Loss
```bash
rosrun multisensory_learning skill_learner.py -s push -o cleaner --learner_type regressor-ml
```

#### Regressor Latent L2 Loss
```bash
rosrun multisensory_learning skill_learner.py -s push -o cleaner --learner_type regressor-latent
```

#### MDN
```bash
rosrun multisensory_learning skill_learner.py -s push -o cleaner --learner_type mdn --n_mdn_components 2
```

---


## Visualization


### Visualize Skill Models

Note `roscore` should be started before this:
```bash
mon launch multisensory_learning visualize_skill_learner.launch data_dir:=/media/data_haro/push_cleaner_data_perturbed
```