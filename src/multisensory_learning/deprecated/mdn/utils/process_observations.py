
import os, sys
import numpy as np
import torch
import torch.nn.functional as F

# sys.path.append('..')
from ll4ma_util import data_util

def process_data_in(data, modality, data_ranges):
    if modality == 'rgb':
        data = data.transpose(0, 3, 1, 2) # (t, 3, h, w)
        data = torch.tensor(data, dtype=torch.float32)
        data = F.interpolate(data, (64, 64), mode='bilinear', align_corners=False) # (t, 3, 64, 64)
        data.div_(255) # Normalize to range [0, 1]
    elif modality == 'depth':
        low, high = data_ranges[modality]
        data = data_util.scale_min_max(data, low, high, 0, 1)
        data = torch.tensor(data, dtype=torch.float32).unsqueeze(1) # Add channel: (t, 1, h, w)
        data = F.interpolate(data, (64, 64), mode='bilinear', align_corners=False) # (t, 1, 64, 64)
    elif modality in data_ranges:
        low, high = data_ranges[modality]
        data = data_util.scale_min_max(data, low, high, 0, 1)
        data = torch.tensor(data, dtype=torch.float32)
    else:
        raise ValueError(f"Unknown modality for processing data into network: {modality}")
    return data

def process_data_out(data, modality, data_ranges):
    data = data.squeeze(dim=1) # TODO if you do batches then really need to view t,b in one dim
    if modality == 'rgb':
        data = F.interpolate(data, (512, 512), mode='bilinear', align_corners=False)
        data = data.cpu().detach().numpy()
        data *= 255
        data = data.astype(np.uint8)
        data = data.transpose(0, 2, 3, 1) # (t, h, w, 3)
    elif modality == 'depth':
        data = F.interpolate(data, (512, 512), mode='bilinear', align_corners=False)
        data = data.cpu().detach().numpy().squeeze(1) # Remove channel dim
        data = data_util.scale_min_max(data, 0, 1, *data_ranges[modality])
    elif modality in data_ranges:
        data = data.cpu().detach().numpy().squeeze()
        data = data_util.scale_min_max(data, 0, 1, *data_ranges[modality])
    else:
        raise ValueError(f"Unknown modality for processing data out of network: {modality}")
    return data
