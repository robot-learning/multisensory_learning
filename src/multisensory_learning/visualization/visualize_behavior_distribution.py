#!/usr/bin/env python
import os
import sys
import rospy
import numpy as np
from tqdm import tqdm

from geometry_msgs.msg import PoseStamped
from visualization_msgs.msg import MarkerArray

from ll4ma_util import file_util, ros_util, ui_util
from multisensory_learning.models import MDN
from multisensory_learning.models.loss import mdn_loss

import torch
from torch.utils.data import Dataset, DataLoader
from torch.distributions import MultivariateNormal
from torch.optim import Adam

            
if __name__ == '__main__':
    """
    """
    rospy.init_node('visualizer_behavior_distribution')

    data_dir = rospy.get_param("~data_dir")
    clear_cache = rospy.get_param("~clear_cache", False)
    iterate = rospy.get_param("~iterate", False)
    rate = rospy.get_param("~rate", 1)
    
    file_util.check_path_exists(data_dir, "Data directory")
    
    metadata_filename = os.path.join(data_dir, "metadata.yaml")
    file_util.check_path_exists(metadata_filename, "Data collection metadata file")
    metadata = file_util.load_yaml(metadata_filename)
    
    mesh_resource = "package://ll4ma_isaacgym/src/ll4ma_isaacgym/assets/cleaner.stl"

    cache_filename = os.path.join(data_dir, '.behavior_visualization_data.pickle')

    if clear_cache:
        file_util.safe_remove_path(cache_filename)
    
    if os.path.exists(cache_filename) and not clear_cache:
        rospy.logwarn("Using cached data")
        behavior_data = file_util.load_pickle(cache_filename)
    else:
        behavior_data = {
            'start_obj_pos': [],
            'start_obj_quat': [],
            'end_obj_pos': [],
            'end_obj_quat': [],
            'target_obj_pos': [],
            'target_obj_quat': [],
            'end_ee_pos': [],
            'end_ee_quat': [],
            'label': [],
        }

        for filename in tqdm(sorted(file_util.list_dir(data_dir, '.pickle'))):
            data, attrs = file_util.load_pickle(filename)
            # TODO hacked, need to make more generic
            try:
                place_params = attrs['behavior_params']['pick_place']['behaviors']['place']
            except KeyError:
                place_params = attrs['behavior_params']['push_cleaner']
            target_obj = place_params['target_object']
            behavior_data['start_obj_pos'].append(data['objects'][target_obj]['position'][0])
            behavior_data['start_obj_quat'].append(data['objects'][target_obj]['orientation'][0])
            behavior_data['end_obj_pos'].append(data['objects'][target_obj]['position'][-1])
            behavior_data['end_obj_quat'].append(data['objects'][target_obj]['orientation'][-1])
            behavior_data['target_obj_pos'].append(place_params['target_object_pose'][:3])
            behavior_data['target_obj_quat'].append(place_params['target_object_pose'][3:])
            behavior_data['end_ee_pos'].append(data['ee_position'][-1])
            behavior_data['end_ee_quat'].append(data['ee_orientation'][-1])
            behavior_data['label'].append(os.path.basename(filename).split('.')[0])
            behavior_data['target_obj'] = target_obj
            
        file_util.save_pickle(behavior_data, cache_filename)


    start_markers = []
    end_markers = []
    target_markers = []
    env_markers = []
    label_markers = []
    ee_poses = []
        
    marker_idx = 0
    for i in range(len(next(iter(behavior_data.values())))):
        # TODO hacked
        if behavior_data['target_obj'] == 'cleaner':
            start_markers.append(ros_util.get_marker_msg(behavior_data['start_obj_pos'][i],
                                                         behavior_data['start_obj_quat'][i],
                                                         shape='mesh', mesh_resource=mesh_resource,
                                                         marker_id=marker_idx, color='green',
                                                         alpha=0.8))
            
            end_markers.append(ros_util.get_marker_msg(behavior_data['end_obj_pos'][i],
                                                       behavior_data['end_obj_quat'][i],
                                                       shape='mesh', mesh_resource=mesh_resource,
                                                       marker_id=marker_idx, color='red', alpha=0.8))
            target_markers.append(ros_util.get_marker_msg(behavior_data['target_obj_pos'][i],
                                                          behavior_data['target_obj_quat'][i],
                                                          shape='mesh', mesh_resource=mesh_resource,
                                                          marker_id=marker_idx, color='yellow',
                                                          alpha=0.8))
        elif behavior_data['target_obj'] == 'block':
            start_markers.append(ros_util.get_marker_msg(behavior_data['start_obj_pos'][i],
                                                         behavior_data['start_obj_quat'][i],
                                                         scale=[0.08, 0.08, 0.08],
                                                         shape='cube', marker_id=marker_idx,
                                                         color='green', alpha=0.8))
            
            end_markers.append(ros_util.get_marker_msg(behavior_data['end_obj_pos'][i],
                                                       behavior_data['end_obj_quat'][i],
                                                       scale=[0.08, 0.08, 0.08],
                                                       shape='cube', marker_id=marker_idx,
                                                       color='red', alpha=0.8))
            target_markers.append(ros_util.get_marker_msg(behavior_data['target_obj_pos'][i],
                                                          behavior_data['target_obj_quat'][i],
                                                          scale=[0.08, 0.08, 0.08],
                                                          shape='cube', marker_id=marker_idx,
                                                          color='yellow', alpha=0.8))

        else:
            raise ValueError(f"Unknown object: {target_obj}")

        pose_stmp = PoseStamped()
        pose_stmp.header.frame_id = 'world'
        pose_stmp.pose.position.x = behavior_data['end_ee_pos'][i][0]
        pose_stmp.pose.position.y = behavior_data['end_ee_pos'][i][1]
        pose_stmp.pose.position.z = behavior_data['end_ee_pos'][i][2]
        pose_stmp.pose.orientation.x = behavior_data['end_ee_quat'][i][0]
        pose_stmp.pose.orientation.y = behavior_data['end_ee_quat'][i][1]
        pose_stmp.pose.orientation.z = behavior_data['end_ee_quat'][i][2]
        pose_stmp.pose.orientation.w = behavior_data['end_ee_quat'][i][3]
        ee_poses.append(pose_stmp)
        
        if iterate:
            label_markers.append(ros_util.get_marker_msg([0, 0, 1], shape='text', color='black',
                                                         text=behavior_data['label'][i],
                                                         scale=[.1]*3, marker_id=marker_idx))
        else:
            marker_idx += 1


    table_config = metadata['env']['objects']['table']
    env_markers.append(ros_util.get_marker_msg(table_config['position'],
                                               table_config['orientation'],
                                               shape='cube',
                                               scale=table_config['extents'],
                                               marker_id=marker_idx,
                                               color=table_config['rgb_color']))
    marker_idx += 1

        
    start_marker_pub = rospy.Publisher('/start_obj_poses', MarkerArray, queue_size=1)
    end_marker_pub = rospy.Publisher('/end_obj_poses', MarkerArray, queue_size=1)
    target_marker_pub = rospy.Publisher('/target_obj_poses', MarkerArray, queue_size=1)
    env_marker_pub = rospy.Publisher('/environment', MarkerArray, queue_size=1)
    label_pub = rospy.Publisher('/labels', MarkerArray, queue_size=1)
    ee_pose_pub = rospy.Publisher('/ee_pose', PoseStamped, queue_size=1)

    start = MarkerArray(markers=start_markers)
    end = MarkerArray(markers=end_markers)
    target = MarkerArray(markers=target_markers)
    env = MarkerArray(markers=env_markers)
    labels = MarkerArray(markers=label_markers)

    
    rospy.loginfo("Publishing marker msgs...")
    rate = rospy.Rate(rate)
    while not rospy.is_shutdown():
        if iterate:
            for i in range(len(start_markers)):
                start.markers = [start_markers[i]]
                end.markers = [end_markers[i]]
                target.markers = [target_markers[i]]
                labels.markers = [label_markers[i]]
                start_marker_pub.publish(start)
                end_marker_pub.publish(end)
                target_marker_pub.publish(target)
                label_pub.publish(labels)
                env_marker_pub.publish(env)
                ee_poses[i].header.stamp = rospy.Time.now()
                ee_pose_pub.publish(ee_poses[i])
                rate.sleep()
            break
        else:
            start_marker_pub.publish(start)
            end_marker_pub.publish(end)
            target_marker_pub.publish(target)
            env_marker_pub.publish(env)
        rate.sleep()
