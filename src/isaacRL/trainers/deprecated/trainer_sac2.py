
from isaacRL.trainers.trainer_sac import TrainerSAC

import os, sys, argparse, json, random
import os.path as osp
import numpy as np
from tqdm import tqdm
import rospy

import torch


class TrainerSAC2(TrainerSAC):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        super(TrainerSAC2, self).__init__(config)

    def train(self):

        if self.config['training']['expert_demos'] > 0:
            self.expert_demos(self.config['training']['load_experts'])

        interv = self.config['training']['checkpoint_interval']

        self.model.train()
        self.state = self.envir.reset()
        for epoch in range(self.start_epoch, self.end_epoch):
            if rospy.is_shutdown():
                return

            rewards = []
            pbar = tqdm(total=interv, file=sys.stdout)
            for di in range(interv):
                if rospy.is_shutdown():
                    return

                if (epoch-1) >= self.config['training']['expert_epochs']:
                    # act in environment
                    rewards.append( self.play(epoch) )
                    while len(self.buffer) < self.config['training']['batch_size']:
                        rewards.append( self.play(epoch) )

                # learn from replays
                qloss, pi_loss = self.update_weights(epoch)
                self.qlosses.append( qloss )
                self.pi_losses.append( pi_loss )
                desc = f'  R {np.mean(rewards):.3f}: loss={self.qlosses[-1]:.3f}, {self.pi_losses[-1]:.3f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()

            print('epoch:', epoch)
            rounds = interv * self.acts_per_interval * epoch
            print('envsteps {}'.format(self.envir.num_envs * rounds))
            print('actsteps {}'.format(rounds))
            rounds = interv * self.updates_per_interval * epoch
            print('updates {}'.format(rounds))
            print('mean rew per step:', np.mean(rewards))
            print('mean rewards:', self.buffer.mean_rewards())
            self.save_model(epoch)


    def play(self, playi):
        with torch.no_grad():
            tot_rewards = 0.
            # tot_terms = 0.000001
            state = self.state
            for step in range(self.acts_per_interval):
                if rospy.is_shutdown():
                    return tot_rewards
                if random.uniform(0,1) < self.randact_prob:
                    actions = self.model.policy.random_action(
                      self.envir.num_envs, self.device )
                else:
                    actions, _ = self.model.policy(
                      self.buffer.prep_state(state, self.device)
                    )
                actions = actions.detach()
                nstate, rewards, terminals = self.envir.step_all(actions)
                tot_rewards += rewards.mean().item()
                # tot_terms += terminals.sum().item()
                self.buffer.add_transitions(
                  state, actions, nstate, rewards, terminals
                )
                state = nstate
            self.state = state
            return tot_rewards/self.acts_per_interval #, tot_terms
