
from isaacgym import gymtorch, gymapi  # noqa: F401

import rospy
import torch

from ll4ma_isaacgym.core.config import EnvironmentConfig
from ll4ma_isaacgym.core.environment import Environment

from isaacRL.utils.goalie import select_goalie
from isaacRL.models.actorcritics import get_rgb_network


class Ll4maRLTask(Environment):
    """docstring for Ll4maRLTask."""

    def __init__(self, task_config, rl_device='cuda'):
        device = rl_device if task_config.get('use_gpu', False) else 'cpu'
        self.task_config = task_config

        self.max_env_steps = self.task_config['max_env_steps']
        self.goalie = select_goalie(self.task_config['goal'], rl_device)
        self.goal_done_num = 0

        self.num_obs = self.task_config.get('num_obs', 14)
        self.obs_keys = self.task_config.get('obs_objs', ['ee_state', 'goal'])

        self.use_rgbemb = any('rgb_emb' in key for key in self.obs_keys)
        self.use_depthemb = any('depth_emb' in key for key in self.obs_keys)
        if self.use_rgbemb:
            self.rgb_net = self.task_config.get('rgb_net', None)
            if self.rgb_net is not None:
                self.rgb_net, self.rgb_emb_size = get_rgb_network()
                self.rgb_net.to(rl_device)
                self.rgb_net.eval()
            else:
                self.use_rgbemb = False

        env_config = EnvironmentConfig(config_filename=self.task_config['env_config'])
        env_config.control_size = self.task_config['num_acts']
        env_config.action_scale = self.task_config['action_scale']
        env_config.additive_actions = self.task_config['additive_actions']
        env_config.vecolity_control = self.task_config['vecolity_control']
        env_config.jacobian_control = self.task_config['jacobian_control']
        env_config.num_envs = self.task_config.get('num_envs', env_config.num_envs)
        env_config.randomize = self.task_config.get('randomize', env_config.randomize)
        env_config.allow_resets = self.task_config.get('allow_resets', True)
        env_config.sim.device = device
        env_config.sim.use_gpu = self.task_config.get('use_gpu', env_config.sim.use_gpu)
        env_config.sim.headless = self.task_config.get('headless', env_config.sim.headless)
        env_config.sim.n_threads = self.task_config.get('num_threads')
        if 'store_rgb' in self.task_config:
            env_config.store_rgb = self.task_config['store_rgb']
        else:
            env_config.store_rgb = any(
                'rgb' in key and 'rgb_emb' not in key
                for key in self.obs_keys
            )
        if 'store_depth' in self.task_config:
            env_config.store_depth = self.task_config['store_depth']
        else:
            env_config.store_depth = any(
                'depth' in key and 'depth_emb' not in key
                for key in self.obs_keys
            )

        super(Ll4maRLTask, self).__init__(
            env_config, device, rl_device
        )
        print('sensor names:', self.sensor_tensors.keys())

        self.obs_buf = torch.zeros(
            (self.num_envs, self.num_obs), device=self.device, dtype=torch.float
        )
        self.rew_buf = torch.zeros(
            self.num_envs, device=self.device, dtype=torch.float
        )
        self.obs_dict = {}
        self.extras = {}

    def _post_physics_step(self):
        super()._post_physics_step()
        if self.allow_state_updates:
            self.compute_observations()
            self.compute_reward()

    def compute_observations(self):

        self.obs_buf[:] = self.full2obs(self.fullstate.data)

        self.obs_dict['obs'] = self.obs_buf.to(self.rl_device)
        self.obs_dict['fullstate'] = self.fullstate.data
        self.obs_dict['state_idx'] = self.fullstate.indices

        # if self.use_rgb:
        #     self.obs_dict['rgb'] = self.fullstate[:,self.state_idx['rgb']]
        #
        # if self.use_depth:
        #     self.obs_dict['depth'] = self.fullstate[:,self.state_idx['depth']]

        return self.obs_buf

    def full2obs(self, fullstate):
        obs_buf = [
            fullstate[:, self.state_idx[key]] for key in self.obs_keys
        ]
        obs_buf = torch.cat(obs_buf, dim=-1)[:]
        return obs_buf

    def _render(self, **kwargs):
        super()._render(kwargs)
        if self.use_rgbemb or self.use_depthemb:
            self.gym.fetch_results(self.sim, True)
            self.gym.step_graphics(self.sim)

    def update_states(self):
        super().update_states()
        if self.use_rgbemb or self.use_depthemb:
            self.gym.render_all_camera_sensors(self.sim)
            self.gym.start_access_image_tensors(self.sim)

            if self.use_rgbemb:
                for senkey in self.sensor_tensors:
                    if 'rgb' in senkey:
                        rgb_data = torch.stack(
                            self.sensor_tensors[senkey]
                        )[..., :3].clone()
                        rgb_data = self._prep_rgb(
                            rgb_data, self.config.sensors[senkey.rsplit('_', 1)[0]]
                        )
                        with torch.no_grad():
                            rgb_emb = self.rgb_net(rgb_data.permute(0, 3, 1, 2))
                            self.fullstate[senkey + '_emb'] = rgb_emb

            # TODO add in depth embedding
            if False and self.use_depthemb:
                for senkey in self.sensor_tensors:
                    if 'depth' in senkey:
                        depth_data = torch.stack(
                            self.sensor_tensors[senkey]
                        )[..., :3].clone() / 255
                        with torch.no_grad():
                            depth_data = self.depth_net(depth_data)
                            self.fullstate[senkey + '_emb'] = depth_data

            self.gym.end_access_image_tensors(self.sim)

        if (self.goal_updates > 0).any():
            envis = self.goal_updates.nonzero(as_tuple=False).squeeze(-1)
            self.goals[envis] = self.goalie.get_goal(
                self.fullstate.data, self.fullstate.indices
            )[envis]
            self.goals_noisy[envis] = self.goalie.get_goal(
                self.fullstate.data, self.fullstate.indices, noisy=True
            )[envis]
            self.goal_updates *= 0
        self.fullstate['goal'] = self.goals.clone()
        self.fullstate['goal_noisy'] = self.goals_noisy.clone()
        self.fullstate['stategoal'] = self.goalie.get_current(
            self.fullstate.data, self.fullstate.indices
        )
        self.fullstate['timestep'] = \
            self.progress_buf.clone().unsqueeze(1).to(self.fullstate['timestep'].dtype).to(self.rl_device)
        self.fullstate['timesine'] = \
            self.progress_buf.clone().unsqueeze(1).to(self.fullstate['timesine'].dtype).to(self.rl_device) / self.max_env_steps

    def compute_reward(self):
        rewards, resets, dist = self.goalie.reward(
            self.fullstate.data, self.fullstate.indices, self.goals
        )
        self.rew_buf[:] = rewards[:].to(self.device)
        self.goal_dones[:] = resets[:]
        self.goal_done_num += self.goal_dones.sum().item()

        self.update_reset_buf((resets > 0).to(self.device))
        self.update_reset_buf(self.progress_buf >= self.max_env_steps)

        self.extras['goal_dist'] = dist.clone()
        self.extras['rewards'] = self.rew_buf[:].clone()
        # self.extras['gdist_mean'] = self.goal_dist[:].mean()
        self.extras['reward_mean'] = self.rew_buf[:].mean()
        self.extras['goal_dones'] = self.goal_done_num / (self.reset_num + 0.001)

    def runs(self):
        return self.reset_num

    @property
    def max_steps(self):
        return self.max_env_steps

    def successes(self):
        return self.goal_done_num

    def _setup_values(self):
        self.goals = torch.zeros(
            (self.num_envs, self.goalie.goal_size),
            dtype=torch.float,
            device=self.rl_device
        )
        self.goals_noisy = self.goals.clone()
        super()._setup_values()
        self.goal_dones = torch.zeros_like(self.reset_buf)
        self.goal_updates = torch.zeros_like(self.reset_buf)

    def _setup_fullstate(self):
        super()._setup_fullstate()
        keys = ['goal', 'goal_noisy', 'stategoal', 'timestep', 'timesine']
        sizes = [self.goalie.goal_size, self.goalie.goal_size, self.goalie.goal_size, 1, 1]
        if self.use_rgbemb:
            for senkey in self.sensor_tensors:
                if 'rgb' in senkey:
                    keys.append(senkey + '_emb')
                    sizes.append(self.rgb_emb_size)
        self.fullstate.add_keys(keys, sizes)
        print('setup fullstate')

    def reset(self, *args):
        super().reset(*args)
        return self.obs_dict

    def reset_idx(self, env_ids):
        super().reset_idx(env_ids)
        self.goal_dones[env_ids] *= 0
        self.goal_updates[env_ids] += 1

    def step(self, *args):
        if rospy.is_shutdown():
            return
        super().step(*args)
        return (
            self.obs_dict,
            self.rew_buf.to(self.rl_device),
            self.reset_buf.to(self.rl_device),
            self.extras
        )
