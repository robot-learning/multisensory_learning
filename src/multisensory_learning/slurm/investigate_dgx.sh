#!/bin/bash
#==========================================================================#
# This script investigates the capabilities and installed software on DGX. #
#==========================================================================#

#SBATCH --job-name=investigate_dgx
#SBATCH -o log-%j.out-%N
#SBATCH -e log-%j.err-%N
#SBATCH --nodes=1
#SBATCH --time=1:00:00
#SBATCH --mem=100M

nvidia-smi

python -c "import slurm_helper; slurm_helper.print_dir('/scratch/adam.conkey')"

echo
