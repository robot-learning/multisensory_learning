
from isaacRL.trainers.trainer_rl import TrainerRL
from isaacRL.models.actorcritics import ActorBasic, CriticBasic, ActorRGB, CriticRGB
from isaacRL.utils.buffer import BufferBasic, BufferHER

from isaacgymenvs.utils.utils import set_seed

import torch
import torch.nn as nn
import torch.nn.functional as F

import time
from tqdm import tqdm
import numpy as np
import os, sys, argparse, json
import os.path as osp
import rospy

class TrainerDDPG(TrainerRL):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):

        config['environ']['seed'] = 42 # -1 if random
        config['environ']['seed'] = set_seed(
            config['environ']['seed'], torch_deterministic=False
        )
        config['environ']['act_range'] = config['model']['actor']['act_range']

        super(TrainerDDPG, self).__init__(config)

        self.alpha = torch.tensor(self.config['training']['alpha'])
        self.gamma = self.config['training']['gamma']
        self.tau = self.config['training']['tau']
        self.randact_prob = self.config['training'].get('randact_prob', 0.)

        self.target_update_interval = self.config['training'].get('target_update_interval', 1)
        self.ckpt_interval = self.config['training']['checkpoint_interval']
        self.save_best_after = self.config['training'].get('save_best_after', self.ckpt_interval)
        self.actions_per_interval = self.config['training']['acts_per_interval']
        self.updates_per_interval = self.config['training']['updates_per_interval']

        self.pretrain_zeros_epochs = self.config['training'].get('pretrain_zeros', 0)
        self.pretrain_zeros = self.pretrain_zeros_epochs > 0

        os.makedirs(self.config['training']['checkpoint_path'], exist_ok=True)

        self.observation_size = config['model']['observations']
        print('observ size:', self.observation_size)
        self.action_size = config['model']['actor']['actions']

        # actor_class = ActorRGB if self.envir.use_rgb else ActorBasic
        # critic_class = CriticRGB if self.envir.use_rgb else CriticBasic
        actor_class = ActorBasic
        critic_class = CriticBasic

        self.actor = actor_class(
            self.observation_size, self.action_size,
            config['model']['actor'],
            # zero_start=self.pretrain_zeros
        )
        self.actor.to(self.device)
        kwargs = {} if not hasattr(self.actor, 'rgb_model') \
            else {'ViT': self.actor.rgb_model, 'gap':self.actor.gap_dim}
        self.critic1 = critic_class(
            self.observation_size+self.action_size,
            config['model'].get('critic', {}),
            # zero_start=self.pretrain_zeros,
            kwargs=kwargs
        )
        self.critic1.to(self.device)
        self.critic2 = critic_class(
            self.observation_size+self.action_size,
            config['model'].get('critic', {}),
            # zero_start=self.pretrain_zeros,
            kwargs=kwargs
        )
        self.critic2.to(self.device)
        self.target_critic1 = self.critic1.copy()
        self.target_critic1.to(self.device)
        self.target_critic2 = self.critic2.copy()
        self.target_critic2.to(self.device)

        self.critic_optim1 = torch.optim.Adam(
            self.critic1.parameters(),  lr=self.lr, weight_decay=self.weight_decay
        )
        self.critic_optim2 = torch.optim.Adam(
            self.critic2.parameters(),  lr=self.lr, weight_decay=self.weight_decay
        )
        self.actor_optim = torch.optim.Adam(
            self.actor.parameters(),  lr=self.lr, weight_decay=self.weight_decay
        )

        if self.config['training'].get('exploration_noise', False):
            self.episodic = self.config['training'].get('episodic_noise', False)
            self.exploration_noise = OUNoise( (
                    (self.num_envs, self.action_size),
                    self.config['model']['actor']['act_range']
                ),
                max_sigma=self.config['training']['sigma'][1],
                min_sigma=self.config['training']['sigma'][0],
                episodic=self.episodic,
                device=self.device
            )

        self.q_losses = []
        self.pi_losses = []

        self._use_eval_data = True

        rgb_size = None #if not self.envir.use_rgb else \
            # (self.envir.img_width*self.envir.img_height*3)

        buf_size = self.config['training'].get('max_buff_size', self.max_steps*self.num_envs)
        # buf_size = max(buf_size, self.max_steps*self.num_envs)
        if self.config['training'].get('her', False):
            max_goals = self.config['training'].get('max_goals', (2*self.num_envs))
            max_goals = max(max_goals, (2*self.num_envs))
            self.buffer = BufferHER(
                self.observation_size,
                self.action_size,
                self.envir.full_state_size,
                self.envir.state_idx,
                self.envir.full2obs,
                self.envir.goalie,
                self.num_envs,
                self.max_steps,
                self.device,
                buf_size,
                max_goals,
                [key for key in self.envir.obs_keys if 'goal' in key][0]
            )
        else:
            self.buffer = BufferBasic(
                self.observation_size,
                self.action_size,
                self.device,
                buf_size,
                rgb_size
            )

        self.state = None

    def train_setup(self):
        # fill minimal replay buffer
        minsize = (self.batch_size//self.num_envs)+1
        minsize = max(minsize, min(self.max_steps, self.buffer.max_size//self.num_envs))
        pbar = tqdm(total=minsize, file=sys.stdout)
        desc = f'Filling buffer'
        pbar.set_description(desc)
        for iter in range(minsize):
            if rospy.is_shutdown():
                return
            self.play(1)
            pbar.update(1)
        pbar.close()

    def train(self):

        self.success = 0
        tot_rewards = 0.
        print('resetting')
        self.actor.train()
        self.critic1.train()
        self.critic2.train()

        self.train_setup()

        print('mean rewards:', self.buffer.mean_rewards())
        success = self.envir.successes()
        print('successes:', success)
        self.success = success

        self.state = self.envir.reset()
        if isinstance(self.buffer, BufferHER):
            self.buffer.reset_full_idx()

        num_epochs = self.end_epoch-self.start_epoch
        tot_iters = num_epochs * self.ckpt_interval
        self._timesteps = 0
        for epoch in range(self.start_epoch, self.end_epoch):
            pbar = tqdm(total=self.ckpt_interval, file=sys.stdout)
            desc = f'Train Epoch {epoch}'
            pbar.set_description(desc)
            for iter in range(self.ckpt_interval):
                if rospy.is_shutdown():
                    return

                # play_time = time.time()
                # add to replays
                self.play(self.actions_per_interval)
                # print('play time:', time.time() - play_time)

                # learn_time = time.time()
                # learn from replays
                q_avg, pi_avg = 0., 0.
                for ui in range(self.updates_per_interval):
                    if rospy.is_shutdown():
                        return
                    qloss, pi_loss = self.update_weights(epoch*self.updates_per_interval+ui)
                    q_avg += qloss
                    pi_avg += pi_loss
                self.q_losses.append(q_avg/self.updates_per_interval)
                self.pi_losses.append(pi_avg/self.updates_per_interval)
                # print('learn time:', time.time() - learn_time)

                desc = f'Train Epoch {epoch}: loss=q:{np.mean(self.q_losses):.3f},pi:{np.mean(self.pi_losses):.3f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()

            if self.pretrain_zeros and epoch >= self.pretrain_zeros_epochs:
                self.pretrain_zeros = False

            iters = (epoch*self.ckpt_interval)+self.start_epoch
            timesteps = self.num_envs * iters * self.actions_per_interval
            runs = timesteps / self.max_steps
            print('saving after:')
            print('{} timesteps'.format(timesteps))
            print('{} runs'.format(self.envir.runs()))
            print('{} epochs'.format(epoch))
            print('mean rewards:', self.buffer.mean_rewards())
            success = self.envir.successes()
            print('successes:', success)
            print('new successes:', (success-self.success))
            if hasattr(self.envir, 'trajopt_time'):
                print('avg trajopt time:', self.envir.trajopt_time())
            self.success = success
            if epoch >= self.save_best_after:
                self.save_model(epoch)

    def evaluate(self, num_steps, store_images=False):
        if self.envir.randomize == 'preset':
            filn = self.config['environ']['randomize'].replace('train', 'eval')
            if os.path.exists(filn) and self._use_eval_data:
                self.envir.add_preset(filn)
        self.state = self.envir.reset()
        self.actor.eval()
        self.critic1.eval()
        self.critic2.eval()
        tot_rew = self.play(num_steps, evaluate=True, store_images=store_images)
        mean_rew = tot_rew / (num_steps*self.num_envs)
        return tot_rew, mean_rew, self.envir.successes(), self.envir.runs()

    def play(self, num_actions=1, evaluate=False, store_images=False):
        if self.state is None:
            self.state = self.envir.reset()
        if store_images:
            self.rgb_images = []
        with torch.no_grad():
            tot_rew = 0.
            if evaluate:
                pbar = tqdm(total=num_actions, file=sys.stdout)
                desc = f'Eval Iter {0}'
                pbar.set_description(desc)
            for ai in range(num_actions):
                if rospy.is_shutdown():
                    return
                actions = self.actor(self.state)
                actions = actions.detach()
                if not evaluate and self.exploration_noise is not None:
                    if self.episodic:
                        self.exploration_noise.evolve_state(self.envir.reset_buf.clone())
                    actions = self.exploration_noise.get_action(actions, self._timesteps)
                nstate, rewards, terminals, _ = self.envir.step(actions)
                if not evaluate:
                    self.buffer.add(
                        self.state, actions, nstate, rewards, terminals
                    )
                if store_images:
                    rgb_key = [key for key in nstate['full_state'].keys() if 'rgb' in key and 'emb' not in key]
                    if len(rgb_key) == 0:
                        rgb = self.envir.get_rgb_img()
                        rgb = [img for img in rgb.values()][0][0]
                        # self.envir.get_rgb_img()['test_camera_rgb'][0]
                        self.rgb_images.append(rgb)
                    else:
                        rgb = nstate['full_state'][rgb_key]
                        self.rgb_images.append(rgb[0].reshape(224, 224, 3)*255)
                self.state = nstate
                tot_rew += rewards.sum().item()
                if evaluate:
                    success = self.envir.successes()
                    desc = f'Eval Iter {ai}: rew={(tot_rew/((ai+1)*self.num_envs)):.3f},suc={success:d}'
                    pbar.set_description(desc)
                    pbar.update(1)
                if not evaluate:
                    self._timesteps += 1
            if evaluate:
                pbar.close()
            return tot_rew

    def update_weights(self, update_i):
        # batch_time = time.time()
        state, action, nstate, rewards, terminals = self.buffer.sample(self.batch_size)
        # print('batch time:', time.time() - batch_time)
        return self._update_weights(update_i, state, action, nstate, rewards, terminals)

    def _update_weights(self, update_i, state, action, nstate, rewards, terminals):
        actor_time_total = 0
        critic_time_total = 0

        with torch.no_grad():
            naction = self.actor(nstate)
            q1_tar = self.target_critic1(nstate, naction)
            q2_tar = self.target_critic2(nstate, naction)
            # Two Q-functions to mitigate positive bias in the policy improvement step
            qvals = torch.min(q1_tar, q2_tar)
            n_qval = rewards + (1-terminals) * self.gamma * qvals
            n_qval = n_qval.detach()

        qval1 = self.critic1(state, action)
         # JQ = 𝔼(st,at)~D[0.5(Q1(st,at) - r(st,at) - γ(𝔼st+1~p[V(st+1)]))^2]
        q1_loss = F.mse_loss(qval1, n_qval)
        self.critic_optim1.zero_grad()
        q1_loss.backward()
        if self.clip_grad is not None:
            self.clip_grad(self.critic1.parameters())
        self.critic_optim1.step()

        qval2 = self.critic2(state, action)
        q2_loss = F.mse_loss(qval2, n_qval)
        self.critic_optim2.zero_grad()
        q2_loss.backward()
        if self.clip_grad is not None:
            self.clip_grad(self.critic2.parameters())
        self.critic_optim2.step()

        action = self.actor(state)

        qval1 = self.critic1(state, action)
        qval2 = self.critic2(state, action)
        qvals = torch.min(qval1, qval2)
        # Policy loss as defined in eq (12)
        # Jπ = 𝔼st∼D,εt∼N[α * logπ(f(εt;st)|st) − Q(st,f(εt;st))]
        pi_loss = qvals.mean()

        self.actor_optim.zero_grad()
        pi_loss.backward()
        if self.clip_grad is not None:
            self.clip_grad(self.actor.parameters())
        self.actor_optim.step()

        if update_i % self.target_update_interval == 0:
            self.target_critic1 = self.critic1.soft_update(self.tau, self.target_critic1)
            self.target_critic2 = self.critic2.soft_update(self.tau, self.target_critic2)

        qloss = (q1_loss + q2_loss).item()
        pi_loss = pi_loss.item() if pi_loss is not None else 0.
        return qloss, pi_loss

    def load_model(self, checkpt):
        checkpoint = torch.load(checkpt)
        self.actor.load_state_dict(checkpoint['actor'])
        self.critic1.load_state_dict(checkpoint['critic1'])
        self.critic2.load_state_dict(checkpoint['critic2'])
        if self.critic_optim1 is not None:
            self.critic_optim1.load_state_dict(checkpoint['critic_optim1'])
            self.critic_optim2.load_state_dict(checkpoint['critic_optim2'])
            self.actor_optim.load_state_dict(checkpoint['actor_optim'])
        self.q_losses = checkpoint['qloss']
        self.pi_losses = checkpoint['pi_loss']
        # self.bestloss = checkpoint['bestloss']
        self.start_epoch = checkpoint['epochs']+1
        self.end_epoch += self.start_epoch
        self._timesteps = checkpoint['timesteps']

    def save_model(self, epoch):
        checkpoint = {}
        checkpoint['critic_optim1'] = self.critic_optim1.state_dict()
        checkpoint['critic_optim2'] = self.critic_optim2.state_dict()
        checkpoint['actor_optim'] = self.actor_optim.state_dict()
        checkpoint['actor'] = self.actor.state_dict()
        checkpoint['critic1'] = self.critic1.state_dict()
        checkpoint['critic2'] = self.critic2.state_dict()
        checkpoint['qloss'] = self.q_losses
        checkpoint['pi_loss'] = self.pi_losses
        checkpoint['timesteps'] = self._timesteps
        # checkpoint['bestloss'] = self.bestloss
        checkpoint['epochs'] = epoch
        filename = os.path.join(
            self.config['training']['checkpoint_path'],
            self.config['training']['checkpoint_name'].format(epoch)
        )
        torch.save(checkpoint, filename)
        print('saved to', filename)
