
You need to install [Pytorch3d](https://pytorch3d.readthedocs.io/en/latest/modules/transforms.html) for some stuff to work (in addition to the requirements for the rest of the multisensory_learning package).  [Installation]((https://github.com/facebookresearch/pytorch3d/blob/main/INSTALL.md)) should be easy.    

To run isaacgym run this line first: `export LD_LIBRARY_PATH=~/miniconda3/lib/` in order to find the correct libraries.  
