import torch

class EnvState(dict):
    """docstring for EnvState."""

    def __init__(self, indices, size, num_envs, device='cpu'):
        self.indices = indices
        self.size = size
        self.state_tensor = torch.zeros((num_envs, self.size), device=device)

    @property
    def data(self):
        return self.state_tensor

    def __len__(self):
        return self.state_tensor.shape[1]

    def __getitem__(self, key):
        return self.state_tensor[:,self.indices[key]]

    def __setitem__(self, key, value):
        assert key in self.indices
        self.state_tensor[:,self.indices[key]] = value[:]

    def __str__(self):
        return str(self.state_tensor)

    def __delitem__(self):
        raise NotImplementedError()

    def __iter__(self):
        raise NotImplementedError()


if __name__ == '__main__':
    size = 7
    index = {'hello': torch.arange(size), 'bob': torch.arange(size)+size}
    envstate = EnvState(index, len(index)*size, 4)
    print(envstate)
    print(envstate['hello'])
    envstate['hello'] = torch.arange(size, dtype=torch.float)
    print(envstate['hello'])
    print(envstate)
