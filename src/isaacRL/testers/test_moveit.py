
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

from isaacRL.tasks.expert_task import ExpertTask
from isaacgymenvs.utils.utils import set_seed

import torch
from ll4ma_util import file_util
import os, sys, argparse, json, yaml
import numpy as np
from tqdm import tqdm

def setup():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str,
        default="configs/tests/m2o_moveit_test.yaml", help="path to config file")
    parser.add_argument("-v", "--verbose", default=False,
        help="if print all info")
    parser.add_argument("--head", default=False, action='store_true',
        help="overwrite running headless or not")
    parser.add_argument("--checkpt", type=str, default=None,
        help="if continuing training from checkpoint model")
    opt = parser.parse_args()
    # print(opt)

    config = file_util.load_yaml(opt.config)

    return opt, config

def main():

    opt, config = setup()
    device = "cpu"
    # device = "cuda" if torch.cuda.is_available() else "cpu"

    if not opt.head:
        config['environ']['num_envs'] = 9
        config['environ']['headless'] = False
    config['environ']['device'] = device # -1 if random
    config['environ']['use_gpu'] = device == 'cuda'
    config['environ']['seed'] = 42 # -1 if random
    config['environ']['seed'] = set_seed(
      config['environ']['seed'], torch_deterministic=False
    )
    config['environ']['act_range'] = config['model']['actor']['act_range']

    max_steps = config['environ']['max_env_steps']

    import rospy
    print('using ros')
    rospy.init_node('run_isaacgym')

    envir = ExpertTask(config['environ'])

    print('resetting behaviors')
    tot_rewards = 0.
    tot_terms = 0.000001
    state = envir.reset()
    # max_steps = int(max(envir.expert.lengths()))
    # envir.gymtask.max_episode_length = max_steps+5
    max_steps = (max_steps+1) * 2
    pbar = tqdm(total=max_steps, file=sys.stdout)
    desc = f'  Expert'
    pbar.set_description(desc)
    avg_rew = 0.
    tot_success = 0
    tot_runs = 0
    for step in range(max_steps):
        if rospy.is_shutdown():
            return
        state, rewards, done, info = envir.step()
        success = envir.successes()
        avg_rew += rewards.sum().item()
        tot_runs += (done > 0).sum().item()
        desc = f'Expert R: {(rewards.mean().item()):.2f},suc={success:d}'
        pbar.set_description(desc)
        pbar.update(1)
        # if (done > 0).any() and step > 10:
        # #     print('bottle:', state['fullstate'][:,envir.state_idx['bottle']][done>0])
        # #     print('goal:', state['fullstate'][:,envir.state_idx['goal']][done>0])
        # #     print('dist:', info['goal_dist'][done>0])
        #     print('reward:', rewards[done>0])
        #     import pdb; pdb.set_trace()
    pbar.close()
    print('tot_success:', envir.successes())
    print('tot_runs:', envir.runs())
    print('reward avg:', avg_rew/(max_steps*envir.num_envs))
    print('reward sum:', avg_rew)

    rospy.is_shutdown()


if __name__ == '__main__':
    main()
