
from ll4ma_opt.problems import IiwaKDLTraj

from isaacRL.experts.expert_interface import ExpertInterface
class OptimizerExpert(ExpertInterface):
    """docstring for OptimizerExpert."""

    def __init__(self, envir, max_steps=40):
        super(OptimizerExpert, self).__init__(envir, max_steps)

        self.sdf = GymSDF(envir)
        self.action_idx = 0

    def get_actions(self):
        return self.actions[self.action_idx], self.action_idx > (self.max_steps-1)

    def reset(self):
        problem = IiwaKDLTraj(
            desired_pose=[],
            timesteps=self.max_steps,
            use_collision=True,
            collision_env=self.sdf,
            soft_limits=softlimits
        )
        if auglag:
            solver = AugmentedLagranMethod(problem, 'gauss newton', params, FP_PRECISION=FP_PRECISION)
        else:
            solver = PenaltyMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
        result = solver.optimize(problem.initial_solution, max_iterations=max_iters)
        self.actions = result.iterates[-1].reshape(problem.timesteps,-1)

    def reset_idx(self, idx):
        problem = IiwaKDLTraj(
            desired_pose=[],
            timesteps=self.max_steps,
            use_collision=True,
            collision_env=self.sdf,
            soft_limits=softlimits
        )
        if auglag:
            solver = AugmentedLagranMethod(problem, 'gauss newton', params, FP_PRECISION=FP_PRECISION)
        else:
            solver = PenaltyMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
        result = solver.optimize(problem.initial_solution, max_iterations=max_iters)
        self.actions = result.iterates[-1].reshape(problem.timesteps,-1)


    def get_trajectory(self):
        self.update_env_states()
        if envis is None:
            envis = self.envi_idx
        for envi in envis:
            """
            I should be going through this very efficiently.  I need a function to take a state and tensorize it, keeping only the important info.  Then I need to compare a state to all the previous state and give the lowest value.  Going through with a for loop over each EnvState class from ll4ma_isaacgym is going to be too slow.
            """
            estate = self.env_states[envi]
            potidx = self.find_closest_potential(estate)
            if potidx is None:
                behavior = self._get_behavior()
                traj, labels = behavior.get_trajectory(estate)
                if traj is None:
                    # Something went wrong and the plan failed, so we want to
                    # reset the environment to try and fix it
                    print('state:', estate.object_states['block'][:3])
                    import pdb; pdb.set_trace()
                    continue
                print('plan length:', len(traj))
                plen = min(len(traj), (self.max_steps-1))
                self.potential_trajs[self.num_potents,:plen,:traj.shape[1]] = traj[:plen,:]
                self.potential_trajs[self.num_potents,plen:,:traj.shape[1]] = traj[plen-1,:]
                self.potential_lens[self.num_potents] = plen+1
                self.potential_labels[self.num_potents] = labels
                self.start_states[self.num_potents] = self.extract_state(estate)

                plan = self.potential_trajs[self.num_potents]
                potidx = self.num_potents
                self.num_potents += 1

            if potidx > self.potential_trajs.shape[0]:
                import pdb; pdb.set_trace()
            self.plans[envi] = self.potential_trajs[potidx]
            self.plan_len[envi] = self.potential_lens[potidx]
            self.labels[envi] = self.potential_labels[potidx]
        self.save_traj(self.save_file)


    def update_env_states(self):

        self.env_states = self.envir.get_states()

from ll4ma_sdf_collisions.envs import SDFInterface
class GymSDF(SDFInterface):
    """docstring for GymSDF."""

    def __init__(self, envir):
        super(GymSDF, self).__init__()
        self.envir = envir
        self.tempsol = SphereSDF(
          torch.tensor([[0.5, 0.0, 0.99]]), torch.tensor([0.1])
        )

    def query(self, pt, obj_idx=None):
        """
        Params:
          pt - (3,) array of a point to get the sdf for
          obs_idx - index of the object to get the sdf for (if None returns an array of the sdf for each object) (default is None)
        Returns:
          sdf_value - float of the sdf value for the given point and object(s)
          sdf_gradient - array of the gradients for this value
        """
        return self.tempsol.query(pt, obj_idx)
