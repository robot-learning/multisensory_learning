
from ll4ma_isaacgym.behaviors import Behavior
from ll4ma_isaacgym.core import EnvironmentState
from std_msgs.msg import ColorRGBA

import torch

class ResidualPrior(object):
    """docstring for ResidualPrior."""

    def __init__(self, envir):
        super(ResidualPrior, self).__init__()
        self.envir = envir
        self.config = self.envir.env_config
        dt = 1.0 / 60.0

        self.reset_behaviors()
        self.env_states = []  # Useful for passing info to behaviors
        for _ in range(self.envir.num_envs):
            env_state = EnvironmentState()
            env_state.dt = dt
            env_state.objects = self.config.env.objects
            env_state.object_colors = { k: ColorRGBA()
              for k in self.config.env.objects.keys() }
            env_state.joint_names = self.envir.joint_names
            self.env_states.append(env_state)
