
from isaacRL.tasks.sac_task import SACTask
from isaacRL.tasks.expert_behaviors import Expert
from isaacRL.models.actorcritics import SoftActorCritic
from isaacRL.trainers.trainer_sac import TrainerSAC
from isaacRL.utils.buffer import Buffer

import os, sys, argparse, json
import os.path as osp
import numpy as np
from tqdm import tqdm
import rospy

import torch
import torch.nn as nn
import torch.nn.functional as F


class TrainerSQIL(TrainerSAC):
    """
    Holder of models and data for training and evaluating SQIL method.
    """

    def __init__(self, config):
        super(TrainerSQIL, self).__init__(config)
        self.expert = Expert(self.envir)


    def train(self):

        interv = self.config['training']['checkpoint_interval']
        eval_iterv = self.config['training']['evaluation_interval'] // interv

        for epoch in range(self.start_epoch, self.end_epoch):

            tot_rewards = 0.0
            for di in range(interv):
                if rospy.is_shutdown():
                    return

                # Expert actions
                self.expert_demos(False)
                # Act in environment
                tot_rewards += self.play(epoch)

                # Learn from replays
                qlosses, pi_losses = 0., 0.
                pbar = tqdm(total=self.updates_per_interval, file=sys.stdout)
                for ui in range(1,self.updates_per_interval+1):
                    if rospy.is_shutdown():
                        return
                    qloss, pi_loss = self.update_weights(epoch)
                    qlosses += qloss
                    pi_losses += pi_loss
                    desc = f'  Learn loss=q{(qlosses/ui):.4f},p{(pi_losses/ui):.4f}'
                    pbar.set_description(desc)
                    pbar.update(1)
                pbar.close()
                self.qlosses.append(qlosses/self.updates_per_interval)
                self.pi_losses.append(pi_losses/self.updates_per_interval)

            print('epoch:', epoch)
            self.save_model(epoch)
            if di % eval_iterv == 0:
                self.evaluate()


    def play(self, playi):
        with torch.no_grad():
            self.model.eval()
            tot_rewards = 0.
            tot_terms = 0.000001
            state = self.envir.reset()
            pbar = tqdm(total=self.max_steps, file=sys.stdout)
            desc = f'  Play'
            pbar.set_description(desc)
            for step in range(self.max_steps):
                if rospy.is_shutdown():
                    return tot_rewards
                actions, _ = self.model.policy(
                  self.buffer.prep_state(state, self.device)
                )
                actions = actions.detach()
                nstate, rewards, terminals = self.envir.step_all(actions)
                rewards *= 0
                terminals *= 0
                self.buffer.add_transitions(
                  state, actions, nstate, rewards, terminals
                )
                tot_rewards += rewards.mean().item()
                state = nstate
                desc = f'  Play R: {(tot_rewards):.1f} {(tot_terms):.0f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            return tot_rewards


    def expert_demos(self, load_experts=True):
        tot_rewards = 0.
        tot_terms = 0.000001
        state = self.envir.reset()
        self.expert.reset()
        pbar = tqdm(total=self.max_steps, file=sys.stdout)
        desc = f'  Expert'
        pbar.set_description(desc)
        for step in range(self.max_steps):
            if rospy.is_shutdown():
                return

            base_acts, exp_resets = self.expert.get_actions()
            actions = torch.zeros((len(base_acts), self.action_space))
            for ai, action in enumerate(base_acts):
                if action is not None:
                    actions[ai] = action.get_state_tensor()
            actions[:, -1] = actions[:,-1] * 2 - 1
            nstate, rewards, terminals = self.envir.step_all(base_acts)
            rewards = torch.ones_like(rewards)
            # do or operation on terminals with exp_resets
            terminals[exp_resets] = 1
            # TODO: make whether to ignore terminal condition from
            # goal/rewards optional
            terminals[[not exp for exp in exp_resets]] = 0
            tot_rewards += rewards.sum().item()
            tot_terms += terminals.sum().item()
            self.buffer.add_transitions(
              state, actions, nstate, rewards, terminals
            )
            state = nstate
            if all(exp_resets):
                break
            desc = f'  Expert R: {(tot_rewards):.1f} {(tot_terms):.0f}'
            pbar.set_description(desc)
            pbar.update(1)
        pbar.close()
