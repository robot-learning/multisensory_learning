
from isaacRL.tasks.gym_task import GymTask
from isaacgymenvs.utils.utils import set_seed

import os, sys, argparse, json, yaml, time
import rospy
import torch
import numpy as np
from tqdm import tqdm

from ll4ma_util import file_util, math_util
from ll4ma_isaacgym.behaviors.planners.trajopt import TrajectoryOptimizer, IKOptimizer
from isaacRL.testers.utils import *

def main():
    torch.set_printoptions(precision=3,sci_mode=False)

    device = "cpu"
    device = "cuda" if torch.cuda.is_available() else "cpu"

    config = file_util.load_yaml('configs/tests/stack_traj.yaml')
    config['environ']['num_envs'] = 1
    config['environ']['headless'] = True #False
    config['environ']['device'] = device # -1 if random
    config['environ']['use_gpu'] = device == 'cuda'
    config['environ']['seed'] = 42 # -1 # if random
    config['environ']['seed'] = set_seed(
      config['environ']['seed'], torch_deterministic=False
    )
    config['environ']['act_range'] = config['model']['actor']['act_range']
    config['environ']['additive_actions'] = False
    config['environ']['allow_resets'] = False
    config['environ']['max_env_steps'] = 500
    max_env_steps = config['environ']['max_env_steps']

    envir = GymTask(config['environ'])
    state = envir.reset()

    max_iterations = 500
    # ikoptim = IKOptimizer(FP_PRECISION=1e-7, max_iterations=max_iterations)
    trajopt = TrajectoryOptimizer(timesteps=10,FP_PRECISION=1e-10,max_iterations=max_iterations)
    trajopt.init_noise = 0.03
    trajopt.ee_link = envir.robot.end_effector.get_link()
    # trajopt.verbose = True

    robot_range = (envir.gymtask.robot_dof_upper_limits[:7] - envir.gymtask.robot_dof_lower_limits[:7]).unsqueeze(0)
    robot_bias = envir.gymtask.robot_dof_lower_limits[:7].unsqueeze(0)

    move_steps = 250
    tot_trajopt_time = 0
    tot_run_time = 0
    tot_hessian_time = 0
    ten_dir_diff = 0
    ten_step_diff = 0
    tot_time = time.time()
    iterations = 20

    tensor_test = False

    filename = 'test_trajopt_joints.torch'
    try:
        save_tars = torch.load(filename)
        print('loaded joints')
    except:
        start_tarjoints = torch.rand((iterations, 7), device=robot_range.device) * robot_range + robot_bias
        end_tarjoints = torch.rand((iterations, 7), device=robot_range.device) * robot_range + robot_bias
        start_joints = torch.zeros_like(start_tarjoints)
        end_joints = torch.zeros_like(end_tarjoints)
        end_poses = torch.zeros_like(end_tarjoints)
        for iter in range(iterations):

            state = run_steps(envir, joint2actions(envir, end_tarjoints[iter]), move_steps)
            end_jnts, end_ee_pose = get_joints_and_ee_pose(envir, state)

            state = run_steps(envir, joint2actions(envir, start_tarjoints[iter]), move_steps)
            start_jnts, start_ee_pose = get_joints_and_ee_pose(envir, state)

            start_joints[iter,:] = start_jnts[:]
            end_joints[iter,:] = end_jnts[:]
            end_poses[iter,:] = end_ee_pose[:]
        save_tars = {
            'start_joints': start_joints,
            'end_joints': end_joints,
            'end_poses': end_poses
        }
        torch.save(save_tars, filename)
        print('saved joints')

    traj_cost = 0.
    start_joints = save_tars['start_joints']
    end_joints = save_tars['end_joints']
    end_poses = save_tars['end_poses']
    print('starting test')
    for iter in range(iterations):
        start_time = time.time()
        if not config['environ']['headless']:
            state = run_steps(envir, joint2actions(envir, start_joints[iter]), move_steps)
            start_jnts, start_ee_pose = get_joints_and_ee_pose(envir, state)
            print('\nstart state')
            print(start_ee_pose)
        trajectory, info = trajopt.get_trajectory(start_joints[iter], end_poses[iter], state, interp=12, repeats=1)
        traj_cost += info['cost']
        tot_hessian_time = info['hesstime']
        trajopt_time = time.time() - start_time
        if not config['environ']['headless']:
            print('target state', end_poses[iter])
            actions = joint2actions(envir, trajectory)
            state = run_steps(envir, actions)
            end_jnts, end_ee_pose = get_joints_and_ee_pose(envir, state)
            print('end state', end_ee_pose)
        run_time = time.time() - start_time

        tot_trajopt_time += trajopt_time
        tot_run_time += run_time

        print('\ntrajopt:', trajopt_time)
        print('avg cost:', traj_cost/(iter+1))
        print('run:', run_time)
        # print('info time:', info['time'])
        print('total trajopt:', tot_trajopt_time)
        print('total run:', tot_run_time)
        print('hessian_time:', tot_hessian_time)

        if tensor_test:
            # time test:
            from ll4ma_opt.solvers.tensor_gausnewton import GaussNewtonMethodTensorized
            from ll4ma_opt.solvers import GaussNewtonMethod
            problem = trajopt.problem
            solver_bas = GaussNewtonMethod(problem, trajopt.alpha, trajopt.rho, FP_PRECISION=trajopt.FP_PRECISION)
            solver_ten = GaussNewtonMethodTensorized(problem, trajopt.alpha, trajopt.rho, FP_PRECISION=trajopt.FP_PRECISION)

            print('\nTensorized')
            tot_dir_time = 0
            tot_step_time = 0
            problem._use_fast = True
            tendevice = 'cpu'
            solver_ten.set_device(tendevice)
            if not torch.is_tensor(info['iterations']):
                xiterats = torch.tensor(info['iterations'], device=tendevice)
            else:
                xiterats = info['iterations'].to(tendevice)
            for xiter in xiterats:
                start_time = time.time()
                problem.pre_compute(xiter)
                direction = solver_ten.compute_search_direction(xiter, torch.tensor([]))
                tot_dir_time += time.time() - start_time
                start_time = time.time()
                step_len = solver_ten.compute_step_length(xiter, direction)
                tot_step_time += time.time() - start_time
            print('avg dir time:', tot_dir_time/len(info['iterations']))
            print('tot dir time:', tot_dir_time)
            print('avg step time:', tot_step_time/len(info['iterations']))
            print('tot step time:', tot_step_time)
            print()
            ten_dir_time = tot_dir_time
            ten_step_time = tot_step_time

            print('\nNot Tensorized')
            tot_dir_time = 0
            tot_step_time = 0
            problem._use_fast = False
            notendevice = 'cpu'
            solver_ten.set_device(notendevice)
            if not torch.is_tensor(info['iterations']):
                xiterats = torch.tensor(info['iterations'], device=tendevice)
            else:
                xiterats = info['iterations'].to(tendevice)
            for xiter in xiterats:
                start_time = time.time()
                problem.pre_compute(xiter)
                direction = solver_bas.compute_search_direction(xiter, torch.tensor([]))
                tot_dir_time += time.time() - start_time
                start_time = time.time()
                step_len = solver_bas.compute_step_length(xiter.numpy(), direction)
                tot_step_time += time.time() - start_time
            print('avg dir time:', tot_dir_time/len(info['iterations']))
            print('tot dir time:', tot_dir_time)
            print('avg step time:', tot_step_time/len(info['iterations']))
            print('tot step time:', tot_step_time)
            print()
            noten_dir_time = tot_dir_time
            noten_step_time = tot_step_time

            ten_dir_diff += ten_dir_time-noten_dir_time
            ten_step_diff += ten_step_time-noten_step_time
            print('dir ten v noten:', ten_dir_diff/(iter+1))
            print('step ten v noten:', ten_step_diff/(iter+1))

    print('\navg trajopt:', tot_trajopt_time/iterations)
    print('avg run:', tot_run_time/iterations)
    print('avg hessian_time:', tot_hessian_time/iterations)
    print('tot time:', time.time() - tot_time)

if __name__ == '__main__':
    main()

"""
    not fast
Run 1:
total trajopt: 79.16574907302856
total run: 109.20624017715454

avg trajopt: 3.9582874536514283
avg run: 5.460312008857727
tot time: 234.646155834198

Run 2:
trajopt: 8.421456098556519
run: 9.807384729385376
total trajopt: 77.95652651786804
total run: 107.30008363723755

avg trajopt: 3.8978263258934023
avg run: 5.365004181861877
tot time: 235.87062048912048

    fast
Run 1:
trajopt: 8.289891242980957
run: 9.975979804992676
total trajopt: 76.12813472747803
total run: 106.00361752510071

avg trajopt: 3.8064067363739014
avg run: 5.300180876255036
tot time: 234.59336519241333

Run 2:
trajopt: 8.123320579528809
run: 10.119334697723389
total trajopt: 76.34885501861572
total run: 108.58694410324097

avg trajopt: 3.817442750930786
avg run: 5.429347205162048
tot time: 240.50383186340332
"""

"""
trajopt: 2.34953236579895
avg cost: 2.9390777665340275
run: 2.349534034729004
total trajopt: 23.773163557052612
total run: 23.77318501472473
hessian_time: 3.585367441177368

Tensorized
avg dir time: 0.0015712083883620985
tot dir time: 0.3126704692840576
avg step time: 0.009872177737442093
tot step time: 1.9645633697509766


Not Tensorized
avg dir time: 0.0017424887748219858
tot dir time: 0.3467552661895752
avg step time: 0.014958505055413174
tot step time: 2.9767425060272217

dir ten v noten: -0.018561160564422606
step ten v noten: -0.37781453132629395

avg trajopt: 1.1886581778526306
avg run: 1.1886592507362366
avg hessian_time: 0.17926837205886842
tot time: 72.40571618080139

------------------------------------------
------------------------------------------
------------------------------------------

trajopt: 4.297340631484985
avg cost: 2.9390582492902397
run: 4.297341585159302
total trajopt: 43.18173432350159
total run: 43.181755781173706
hessian_time: 8.252582550048828

Tensorized
avg dir time: 0.00154472355866552
tot dir time: 0.3073999881744385
avg step time: 0.009606245175078885
tot step time: 1.9116427898406982


Not Tensorized
avg dir time: 0.001681876541981146
tot dir time: 0.33469343185424805
avg step time: 0.015005289010666124
tot step time: 2.9860525131225586

dir ten v noten: -0.01999831199645996
step ten v noten: -0.3974848508834839

avg trajopt: 2.1590867161750795
avg run: 2.1590877890586855
avg hessian_time: 0.4126291275024414
tot time: 92.76296639442444
"""

"""
***No GPU
trajopt: 30.188386917114258
avg cost: 2.3038671871273317
run: 30.188387870788574
total trajopt: 365.6791784763336
total run: 365.67920112609863
hessian_time: 44.63922643661499

avg trajopt: 18.28395892381668
avg run: 18.28396005630493
avg hessian_time: 2.2319613218307497
tot time: 365.6811943054199

***No GPU run 2
trajopt: 29.268282651901245
avg cost: 2.3038671871273317
run: 29.26828384399414
total trajopt: 366.34632182121277
total run: 366.3463468551636
hessian_time: 44.427518367767334

avg trajopt: 18.31731609106064
avg run: 18.31731734275818
avg hessian_time: 2.221375918388367
tot time: 366.34832644462585

***GPU
trajopt: 56.54142355918884
avg cost: 2.3040267870770683
run: 56.54142498970032
total trajopt: 656.4518823623657
total run: 656.4519102573395
hessian_time: 95.39686512947083

avg trajopt: 32.82259411811829
avg run: 32.822595512866975
avg hessian_time: 4.769843256473541
tot time: 656.4537682533264


***torch orientation_grad no GPU
trajopt: 30.534677982330322
avg cost: 4.847641326566512
run: 30.53467893600464
total trajopt: 352.7657001018524
total run: 352.76572036743164
hessian_time: 44.44227051734924

avg trajopt: 17.63828500509262
avg run: 17.63828601837158
avg hessian_time: 2.2221135258674622
tot time: 352.76863288879395

***torch orientation_grad GPU
trajopt: 54.92347478866577
avg cost: 4.847602766534568
run: 54.923476219177246
total trajopt: 635.2897109985352
total run: 635.2897415161133
hessian_time: 95.92101073265076

avg trajopt: 31.764485549926757
avg run: 31.764487075805665
avg hessian_time: 4.796050536632538
tot time: 635.2915487289429

***torch orientation_grad no GPU jit script
trajopt: 29.65052819252014
avg cost: 4.8476412350830085
run: 29.650529146194458
total trajopt: 350.17695689201355
total run: 350.17697834968567
hessian_time: 43.747785329818726

avg trajopt: 17.508847844600677
avg run: 17.508848917484283
avg hessian_time: 2.1873892664909365
tot time: 350.17913341522217


"""


"""
********** Work PC *************
    No GPU
trajopt: 13.078534126281738
avg cost: 2.3990988255918926
run: 13.078535556793213
total trajopt: 178.18943047523499
total run: 178.18945813179016
hessian_time: 24.504011631011963

avg trajopt: 8.909471523761749
avg run: 8.909472906589508
avg hessian_time: 1.2252005815505982
tot time: 178.19151329994202

    GPU
trajopt: 13.487304925918579
avg cost: 2.3990988255918926
run: 13.487306594848633
total trajopt: 176.47997522354126
total run: 176.4800021648407
hessian_time: 24.16684603691101

avg trajopt: 8.823998761177062
avg run: 8.824000108242036
avg hessian_time: 1.2083423018455506
tot time: 176.48307919502258

    kdl_util orientation error GPU
trajopt: 3.5633835792541504
avg cost: 2.2363667426343934
run: 3.563384771347046
total trajopt: 161.3611011505127
total run: 161.36112809181213
hessian_time: 24.97252893447876

avg trajopt: 8.068055057525635
avg run: 8.068056404590607
avg hessian_time: 1.248626446723938
tot time: 161.36421489715576

    kdl_util orientation error No GPU
trajopt: 1.9977872371673584
avg cost: 2.234771014637396
run: 1.997788429260254
total trajopt: 87.80176734924316
total run: 87.80179572105408
hessian_time: 12.478453636169434

avg trajopt: 4.390088367462158
avg run: 4.3900897860527035
avg hessian_time: 0.6239226818084717
tot time: 87.80387377738953

trajopt: 2.341947555541992
avg cost: 2.2347710140153323
run: 2.341948986053467
total trajopt: 135.6595175266266
total run: 135.65954399108887
hessian_time: 13.240012645721436

avg trajopt: 6.78297587633133
avg run: 6.782977199554443
avg hessian_time: 0.6620006322860718
tot time: 135.66170263290405

trajopt: 2.00834321975708
avg cost: 2.234771014637396
run: 2.008344888687134
total trajopt: 95.89576435089111
total run: 95.89579367637634
hessian_time: 12.779592990875244

avg trajopt: 4.794788217544555
avg run: 4.794789683818817
avg hessian_time: 0.6389796495437622
tot time: 95.89799904823303
"""
