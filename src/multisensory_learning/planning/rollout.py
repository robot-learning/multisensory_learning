import torch
from dataclasses import dataclass, field
from typing import List, Dict, TypeVar

from ll4ma_util import math_util

from distribution_planning.distributions import Distribution
from distribution_planning.distributions.torch_distributions import GaussianMixtureModel


RolloutNode = TypeVar("RolloutNode")  # https://stackoverflow.com/a/64756232/3711266


@dataclass
class RolloutNode:
    samples: torch.Tensor = None

    # TODO this was the mode propagation stuff, not sure if it's still needed
    skill: str = None
    params: torch.Tensor = None
    propagation: str = ''
    ref_points: List[torch.Tensor] = field(default_factory=list)
    component: int = None
    alpha: float = None
    mu: torch.Tensor = None
    sigma_tril: torch.Tensor = None
    model_inputs: Dict[str, torch.Tensor] = field(default_factory=dict)
    model_outputs: Dict[str, torch.Tensor] = field(default_factory=dict)
    parent: RolloutNode = None
    children: List[RolloutNode] = field(default_factory=list)
    _torch_mvn: torch.distributions.MultivariateNormal = None
    
    @property
    def terminal_nodes(self):
        if self.children:
            return [t for c in self.children for t in c.terminal_nodes]
        else:
            return [self]

    @property
    def node_sequence(self):
        seq = [self]
        node = self
        while node.parent is not None:
            seq.append(node.parent)
            node = node.parent
        return reversed(seq) # Reverse so they go by increasing step idx
        
    @property
    def param_sequence(self):
        return [n.params for n in self.node_sequence]

    @property
    def alpha_sequence(self):
        return [n.alpha for n in self.node_sequence if n.alpha is not None]

    @property
    def mu_sequence(self):
        return [n.mu for n in self.node_sequence if n.mu is not None]

    @property
    def model_inputs_sequence(self):
        return [n.model_inputs for n in self.node_sequence if n.model_inputs]

    @property
    def skill_sequence(self):
        return [n.skill for n in self.node_sequence if n.skill]

    @property
    def batch_size(self):
        return self.mu.size(0)

    @property
    def torch_mvn(self):
        if self._torch_mvn is None:
            self._torch_mvn = torch.distributions.MultivariateNormal(
                loc=self.mu,
                scale_tril=self.sigma_tril
            )
        return self._torch_mvn
    
    def add_child(self, node):
        self.children.append(node)
        

@dataclass
class Rollout:
    root: RolloutNode = field(default_factory=RolloutNode)
    p0: Distribution = None
    pT: GaussianMixtureModel = None
    init_samples: torch.Tensor = None
    nodes: List[RolloutNode] = field(default_factory=list)
    
    @property
    def terminal_nodes(self):
        return self.root.terminal_nodes

    @property
    def batch_size(self):
        return self.pT.batch_size
