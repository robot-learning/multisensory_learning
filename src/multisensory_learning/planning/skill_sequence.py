import sys
import rospy
import torch
import numpy as np
from enum import Enum

from multisensory_learning.planning import RolloutNode, Rollout
from multisensory_learning.util import learn_util

from ll4ma_util import math_util, torch_util
from distribution_planning.distributions.torch_distributions import GaussianMixtureModel


class PropagationType(Enum):
    MEAN = 1
    SIGMA_POINTS = 2
    SAMPLES = 3


class SkillSequence:

    def __init__(self, learners, skeleton=[]):
        self.learners = learners
        self.skeleton = skeleton
        self.device = self.nominal_learner.device

    @property
    def n_steps(self):
        return len(self.skeleton)

    @property
    def nominal_learner(self):
        """
        Sometimes you just want an arbitrary learner, e.g. to use its ortho6d conversions.
        """
        return next(iter(self.learners.values()))

    def set_skeleton(self, skeleton):
        for name in skeleton:
            if name not in self.learners:
                rospy.logerr(f"Skill {name} in plan skeleton not known. "
                             f"Valid skills: {list(self.learners.keys())}")
                return False
        self.skeleton = skeleton        
        return True

    def rollout(
            self,
            p0,
            params_vec=None,
            params_list=None,
            skeleton=[],
            n_samples_to_propagate=100
    ):
        """
        Given a sequence of skill parameters and initial distribution, computes
        terminal distribution by sampling through the skill models over the
        planning horizon.
        """
        if skeleton:
            self.skeleton = skeleton
        if self.n_steps == 0:
            rospy.logerr("Plan skeleton was not set")
            return None, None
        if params_list is None:
            if params_vec is None:
                raise ValueError("Must specify either params_vec or params_list")
            params_list = self.vec_to_skill_params(params_vec)
            
        with torch.no_grad():
            B = params_list[0].size(0)
            
            rollout = Rollout()
            rollout.n_samples_to_propagate = n_samples_to_propagate
            rollout.p0 = p0.as_torch(batch_size=B)

            N = n_samples_to_propagate
            D = rollout.p0.loc.size(-1)

            samples = rollout.p0.sample((N,)).reshape(N * B, D)
            rollout.init_samples = samples
            
            for step_idx in range(self.n_steps):
                skill = self.skeleton[step_idx]
                learner = self.learners[skill]
                params = params_list[step_idx].repeat(N, 1)
                samples, _, _, inputs = learner.propagate_distribution(
                    params,
                    samples
                )
                node = RolloutNode()
                node.skill = skill
                node.params = params[0]  # Don't want the repeats
                node.samples = samples.clone()
                node.samples_reshaped = samples.clone().reshape(N, B, D)
                node.model_inputs = inputs
                rollout.nodes.append(node)
            gmm = GaussianMixtureModel()
            gmm.fit(samples.reshape(N, B, D), learner.config.n_mdn_components)
            rollout.pT = gmm
                
        return rollout

    def vec_to_skill_params(self, v):
        """
        Decision vars come in as one vector, this function scans through to
        parse out the params associated with each skill in the skeleton.
        """
        if self.n_steps == 0:
            rospy.logerr("Plan skeleton has not been set yet")
            return []
        params = []
        start_idx = 0
        end_idx = 0
        for name in self.skeleton:
            input_size = self.learners[name].get_sample_size()
            end_idx += input_size
            params.append(v[:,start_idx:end_idx])
            start_idx += input_size            
        return params

    def get_min_max_bounds(self):
        min_bounds = []
        max_bounds = []
        for name in self.skeleton:
            skill_min, skill_max = self.learners[name].get_sample_bounds()
            min_bounds.append(skill_min)
            max_bounds.append(skill_max)
        min_bounds = np.concatenate(min_bounds).reshape(-1, 1)
        max_bounds = np.concatenate(max_bounds).reshape(-1, 1)
        return min_bounds, max_bounds

    def _propagate_node(self, node, learner, params, propagation):
        if PropagationType[propagation] == PropagationType.MEAN:
            inputs, outputs = self._propagate_point(node.mu, learner, params)
            ref_points = node.mu.clone()  # Mean propagation will always have only 1 point
            alpha = outputs['alpha']
            # Transforming delta distribution with ref frame to have world distribution
            mu = torch.zeros_like(outputs['mu'])
            sigma_tril = torch.zeros_like(outputs['sigma_tril'])
            for comp_idx in range(mu.size(-1)):
                mu[:,:,comp_idx], sigma_tril[:,:,:,comp_idx] = self._transform_gaussian(
                    node.mu,
                    outputs['mu'][:,:,comp_idx],
                    outputs['sigma_tril'][:,:,:,comp_idx],
                    learner
                )
        elif PropagationType[propagation] == PropagationType.SIGMA_POINTS:

            # TODO I'm not sure this will actually work with the ortho6d rotations
            # in the representation, I did some tests and the sigma points don't
            # quite make sense for those dimensions (e.g. the sigma points can
            # show the mesh totally flipped upside down when it shouldn't be).
            
            raise NotImplementedError("TODO implement sigma points propagation")
        elif PropagationType[propagation] == PropagationType.SAMPLES:
            inputs, outputs, ref_points, alpha, mu, sigma_tril = self._propagate_samples(
                node,
                learner,
                params
            )
        return inputs, outputs, ref_points, alpha, mu, sigma_tril
    
    def _propagate_point(self, pos_ortho6d, learner, params):
        inputs = learner.sample_to_model_input(params, pos_ortho6d=pos_ortho6d)
        alpha, mu, sigma_tril = learner.apply_models(inputs)['mdn']
        outputs = {
            'alpha': alpha,
            'mu': mu,
            'sigma_tril': sigma_tril
        }
        return inputs, outputs

    def _propagate_sigma_points(self, node, learner, params):
        pass

    def _propagate_samples(
            self,
            node,
            learner,
            params,
            n_ref_samples=10,
            n_mdn_samples=20
    ):
        B = node.mu.size(0)
        N = n_ref_samples
        M = n_mdn_samples
        D = node.mu.size(-1)
        K = learner.config.n_mdn_components
        
        mvn = MVN(node.mu, scale_tril=node.sigma_tril)
        ref_points = mvn.sample((N,)).reshape(N * B, D)
        params = torch_util.make_batch(params, N).reshape(N * B, -1)

        inputs, outputs = self._propagate_point(ref_points, learner, params)
        # Reshape so they're more interpretable in being associated with different sample points
        for k, v in inputs.items():
            inputs[k] = v.reshape(N, B, -1)
        for k, v in outputs.items():
            outputs[k] = v.reshape(N, B, *v.size()[1:])
                
        # TODO need strategy to integrate outputs into a single GMM distribution.
        # For now trying simplest thing which is sample from all GMMs and refit
        # a GMM to the samples.

        # First generate samples from all components of all (batched) ref GMMs
        gmm_samples = torch.zeros(N, M, B, D, K)
        mix_idxs = torch.zeros(N, M, B)
        for ref_idx in range(N):
            for comp_idx in range(K):
                mvn = MVN(
                    outputs['mu'][ref_idx,:,:,comp_idx],
                    scale_tril=outputs['sigma_tril'][ref_idx,:,:,:,comp_idx]
                )
                gmm_samples[ref_idx,:,:,:,comp_idx] = mvn.sample((M,))
            mix = Categorical(outputs['alpha'][ref_idx])
            mix_idxs[ref_idx,:,:] = mix.sample((M,))

        mix_idxs = mix_idxs.unsqueeze(-1).repeat(1, 1, 1, D)

        # Collapse component dim of samples by selecting based on the sampled mixture idxs
        samples = torch.zeros(N, M, B, D)
        for comp_idx in range(K):
            # Replaces samples with the corresponding gmm_samples when comp_idx was selected:
            samples = samples.where(mix_idxs != comp_idx, gmm_samples[:,:,:,:,comp_idx])

        # Take all samples from all GMMs, and fit a new single GMM
        gmm = GMM(n_components=K)
        samples = samples.reshape(N * M, B, D)
        alphas = []
        mus = []
        sigmas = []
        for batch_idx in range(B):
            gmm.fit(samples[:,batch_idx,:].numpy())
            alphas.append(gmm.weights_)
            mus.append(gmm.means_)
            sigmas.append(gmm.covariances_)
        alpha = torch.tensor(np.array(alphas))
        mu = torch.tensor(np.array(mus).transpose(0, 2, 1))
        sigma = torch.tensor(np.array(sigmas)).reshape(B * K, D, D)
        sigma_tril = torch.linalg.cholesky(sigma).reshape(B, K, D, D).transpose(1, 3)

        # TODO Can probably keep this code, but it's potentially unnecessary based
        # on discussion with Tucker, instead of fitting GMM at each step can maybe
        # just sample all the way through and only fit a GMM at the last step


        
        
        return inputs, outputs, ref_points, alpha, mu, sigma_tril

    def _transform_gaussian(self, ref_point, mu, sigma_tril, learner, n_samples=100):
        """
        Transform delta distribution from model output into the world frame using a
        reference point. There may be a way to do this analytically but until I figure
        that out just doing sampling.
        """
        B = mu.size(0)
        N = n_samples
        D = mu.size(-1)
        w_T_ref = torch_util.make_batch(
            learn_util.vec_to_homogeneous(ref_point), N
        ).reshape(N * B, 4, 4)
        mvn = MVN(mu, scale_tril=sigma_tril)
        samples = mvn.sample((N,)).reshape(N * B, D)
        ref_T_obj = learn_util.vec_to_homogeneous(samples)
        w_T_obj = torch.bmm(w_T_ref, ref_T_obj)
        tf_samples = learn_util.homogeneous_to_vec(
            w_T_obj,
            learner.ortho6d_learner
        ).reshape(N, B, D)
        mu = tf_samples.mean(dim=0)
        cov = torch_util.batch_cov(tf_samples.transpose(0, 1))
        cov += torch.eye(D) * 1e-8  # Regularize to make PSD
        sigma_tril = torch.linalg.cholesky(cov)
        return mu, sigma_tril        
        
