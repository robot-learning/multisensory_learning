
from isaacRL.tasks.rpl_task import RPLTask
from isaacRL.tasks.expert_behaviors import Expert
from isaacRL.models.actorcritics import SoftActorCritic
from isaacRL.trainers.trainer_goal import TrainerGoal
from isaacRL.utils.buffer import BufferHER
from isaacRL.utils.sac_loss import critic_loss, actor_loss
from isaacRL.utils.data_utils import LimitArray

import os, sys, argparse, json, random
import os.path as osp
import numpy as np
from tqdm import tqdm
import rospy

import torch
import torch.nn as nn
import torch.nn.functional as F


class TrainerRPLHER(TrainerGoal):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        super().__init__(config)
        self.envir = RPLTask(self.config['environ'], 'cpu')

        self.expert = Expert(self.envir)

    def load_model(self, checkpt):
        super().load_model(checkpt)
        # self.buffer.load(
        #   osp.join(osp.expanduser(self.config['environ']['sim']['data_root']),
        #     self.config['environ']['sim']['data_prefix'])
        # )

    def save_model(self, epoch):
        super().save_model(epoch)
        # self.buffer.save(
        #   osp.join(osp.expanduser(self.config['environ']['sim']['data_root']),
        #     self.config['environ']['sim']['data_prefix'])
        # )

    def train(self):

        for epoch in range(self.start_epoch, self.end_epoch):

            if rospy.is_shutdown():
                return

            self.play(epoch)
            if ('init_buff' in self.config['training']) and \
              (epoch == self.start_epoch):
                for _ in range(self.config['training']['init_buff']-1):
                    self.play(epoch)

            # learn from replays
            qlosses, pi_losses = 0., 0.
            pbar = tqdm(total=self.updates_per_interval, file=sys.stdout)
            desc = f'  Learn Epoch {epoch}'
            pbar.set_description(desc)
            base_ui = int(self.updates_per_interval * epoch)
            for ui in range(self.updates_per_interval):
                if rospy.is_shutdown():
                    return
                qloss, pi_loss = self.update_weights(base_ui + ui)
                self.qlosses.append(qloss)
                self.pi_losses.append(pi_loss)
                desc = f'  Learn Epoch {epoch}: loss={np.mean(self.qlosses):.4f},{np.mean(self.pi_losses):.4f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()

            if epoch%self.ckpt_interval == 0:
                self.save_model(epoch)
            if epoch%self.eval_interval == 0:
                self.evaluate()


    def play(self, playi):
        with torch.no_grad():
            self.model.eval()
            test = False
            state = self.envir.reset()
            self.expert.reset()
            base_acts, exp_resets = self.expert.get_actions(self.device)
            trans = []
            tot_rewards = 0.
            tot_terms = 0.000001
            pbar = tqdm(total=self.max_steps, file=sys.stdout)
            desc = f'  Play Epoch {playi}'
            pbar.set_description(desc)
            for step in range(self.max_steps):
                if rospy.is_shutdown():
                    return

                # adding actions | TODO: add other potential changes to state space
                state['base_action'] = self.envir.convert_action2tensor(base_acts)

                if random.uniform(0,1) < self.randact_prob:
                    res_acts = self.model.policy.random_action(
                      self.n_envs, self.device )
                else:
                    res_acts, _ = self.model.policy(
                      self.buffer.prep_state(state, self.device)
                    )
                    res_acts = res_acts.detach()
                if ('one_zero' in self.config['training']
                  and self.config['training']['one_zero']):
                    res_acts[0] *= 0
                self.envir.convert_tensor2action(res_acts, base_acts)

                nstate, rewards, terminals = self.envir.step_all(base_acts)
                tot_rewards += rewards.sum().item()
                terminals[exp_resets] = 1
                # TODO: make whether to ignore terminal condition from
                # goal/rewards optional
                terminals[[not exp for exp in exp_resets]] = 0
                tot_terms += terminals.sum().item()

                base_acts, exp_resets = self.expert.get_actions(self.device)
                # adding actions | TODO: add other potential changes to state space
                base_action = torch.zeros((len(base_acts), self.action_space))
                for ai, action in enumerate(base_acts):
                    if action is not None:
                        base_action[ai] = action.get_state_tensor()
                nstate['base_action'] = base_action

                if step == 0:
                    self.buffer.add_goals(state['goal'])
                self.buffer.add_transitions(
                  state, res_acts, nstate, rewards, terminals, state['goal']
                )

                trans.append( (state, res_acts, nstate) )
                if terminals.sum() > 0:
                    goal_idx = terminals.clone()
                    idx = torch.arange(len(goal_idx))[goal_idx>0][0]
                    fin_goals = self.envir.goalie.extract_current(state)
                    self.buffer.add_goals(fin_goals[goal_idx>0])
                    for state, actions, nstate in trans:
                        rewards, terminals = self.envir.get_rewards(state, fin_goals)
                        self.buffer.add_transitions(
                          state, actions, nstate, rewards, terminals, fin_goals,
                          goal_idx
                        )
                        # save the transitions with other goals
                        for _ in range(self.goal_samples):
                            # sample other goals
                            goals = self.buffer.sample_goals(actions.shape[0])
                            # get the rewards for these other goals
                            rewards, terminals = self.envir.get_rewards(state, goals)
                            self.buffer.add_transitions(
                              state, actions, nstate, rewards, terminals, goals,
                              goal_idx
                            )

                    state = self.envir.reset_indices(goal_idx)
                    base_acts, exp_resets = self.expert.reset_indices(goal_idx)
                    self.buffer.add_goals(state['goal'][goal_idx>0])
                    if rewards.sum() > 0:
                        test = True
                else:
                    state = nstate
                desc = f'  Play rewards: {(tot_rewards):.0f} {(tot_terms):.0f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            fin_goals = self.envir.goalie.extract_current(state)
            self.buffer.add_goals(fin_goals)
            for state, actions, nstate in trans:
                rewards, terminals = self.envir.get_rewards(state, fin_goals)
                self.buffer.add_transitions(
                  state, actions, nstate, rewards, terminals, fin_goals
                )
                # save the transitions with other goals
                for _ in range(self.goal_samples):
                    # sample other goals
                    goals = self.buffer.sample_goals(actions.shape[0])
                    # get the rewards for these other goals
                    rewards, terminals = self.envir.get_rewards(state, goals)
                    self.buffer.add_transitions(
                      state, actions, nstate, rewards, terminals, goals
                    )
            # print('rewards tot:{:.0f}, terms: {:.0f}, mean:{:.3f}'.format(
            #   tot_rewards, tot_terms, tot_rewards/tot_terms ))

    def evaluate(self):
        print('evaluation')
        tot_rewards = 0.
        tot_terms = 0.000001
        qvals = []
        with torch.no_grad():
            self.model.eval()
            state = self.envir.reset()
            pbar = tqdm(total=self.max_steps, file=sys.stdout)
            desc = f'  Evaluate'
            pbar.set_description(desc)
            for step in range(self.max_steps):
                if rospy.is_shutdown():
                    return

                base_acts, exp_resets = self.expert.get_actions(self.device)
                # adding actions | later change state depending on config
                base_action = torch.zeros((len(base_acts), self.action_space))
                for ai, action in enumerate(base_acts):
                    if action is not None:
                        base_action[ai] = action.get_state_tensor()
                state['base_action'] = base_action

                res_acts, _ = self.model.policy(
                  self.buffer.prep_state(state, self.device)
                )
                res_acts = res_acts.detach()
                for ai, action in enumerate(base_acts):
                    if action is not None:
                        jpos = action.get_arm_joint_position()
                        jpos += res_acts[ai, :len(jpos)].cpu().numpy()
                        action.set_arm_joint_position( jpos )
                        if res_acts[ai, -1] > 0.5:
                            action.set_ee_discrete('close')
                        elif res_acts[ai, -1] < -0.5:
                            action.set_ee_discrete('open')
                nstate, rewards, terminals = self.envir.step_all(base_acts)

                terminals[exp_resets] = 1

                if terminals.sum() > 0:
                    state = self.envir.reset_indices(terminals)
                    self.expert.reset_indices(terminals)
                else:
                    state = nstate

                tot_rewards += rewards.sum().item()
                tot_terms += terminals.sum().item()
                desc = f'  Evaluate rewards: {(tot_rewards):.0f} {(tot_terms):.0f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
        print('rewards tot:{:.0f}, terms: {:.0f}, mean:{:.3f}'.format(
          tot_rewards, tot_terms, tot_rewards/tot_terms ))
        self.cur_state = self.envir.reset()
        print('eval done')
        return qvals
