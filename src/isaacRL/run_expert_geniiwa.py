
from isaacgym import gymtorch, gymapi

from tasks.gym_tasks import GymTask
from isaacRL.utils.expert_behaviors import Expert

import torch
from ll4ma_util import file_util
import os, sys, argparse, json, yaml
import numpy as np
from tqdm import tqdm
import rospy

def setup():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str,
        default="configs/nonarm/ppo_iiwastack1.yaml", help="path to config file")
    parser.add_argument("-v", "--verbose", default=False,
        help="if print all info")
    parser.add_argument("--checkpt", type=str, default=None,
        help="if continuing training from checkpoint model")
    parser.add_argument("--num_runs", type=int, default=1,
        help="number of environments to run")
    parser.add_argument("--no_ros", action='store_true',
        help="if not using ros")
    opt = parser.parse_args()
    # print(opt)

    if not opt.no_ros:
        print('using ros')
        rospy.init_node('run_isaacgym')

    config = file_util.load_yaml(opt.config)

    return opt, config

def main():
    opt, config = setup()
    device = "cpu"
    device = "cuda" if torch.cuda.is_available() else "cpu"

    state_feats = 0
    for mod in config['model']['modalities']:
        if mod == 'rgb':
            state_feats += config['model']['modalities'][mod]['outfeats']
        else:
            state_feats += config['model']['modalities'][mod]
    config['environ']['num_obs'] = state_feats
    # config['environ']['actions'] = config['model']['actor']['actions']
    config['environ']['act_range'] = config['model']['actor']['act_range']

    envir = GymTask(config['environ'])
    envir.set_additive(True)

    for _ in range(10):
        if not opt.no_ros:
            if rospy.is_shutdown():
                return
        envir.step(None)

    max_steps = config['environ']['max_env_steps']
    print('max steps:', max_steps)
    expert = Expert(envir, max_steps, device)

    print('resetting behaviors')
    tot_rewards = []
    good_terms, bad_terms = 0., 0.
    good_thresh = 0.
    tot_terms = 0.
    runs_per = opt.num_runs // envir.num_envs
    for ri in range(1,runs_per+1):
        state = envir.reset()
        envir.set_additive(False)
        expert.reset()
        succeeded = False
        num_steps = int(min(max_steps, expert.plan_len.max()))
        pbar = tqdm(total=num_steps, file=sys.stdout)
        desc = f'  Expert'
        pbar.set_description(desc)
        for step in range(num_steps):
            if not opt.no_ros:
                if rospy.is_shutdown():
                    return

            base_acts, _ = expert.get_actions()
            nstate, rewards, terminals = envir.step(base_acts)
            tot_rewards.append(rewards.mean().item())
            tot_terms += terminals.sum().item()
            if terminals.sum() > 0:
                succeeded = True
            state = nstate
            desc = f'  Expert R: {(tot_rewards[-1]):.3f}' #' good: {good_terms}'
            pbar.set_description(desc)
            pbar.update(1)
        pbar.close()
        if num_steps > 0:
            if succeeded:
                good_terms += envir.gymtask.successes()
            tot_terms += envir.num_envs
        print('completed {} runs'.format(tot_terms))

    print('rewards:', np.mean(tot_rewards))
    print('good ends:', good_terms)
    print('total ends:', tot_terms)

    if not opt.no_ros:
        rospy.is_shutdown()


if __name__ == '__main__':
    main()
