
from isaacRL.tasks.gym_task import GymTask
from isaacgymenvs.utils.utils import set_seed

from ll4ma_util import file_util

import os, sys, argparse, json, yaml
import rospy
import torch
import numpy as np
from tqdm import tqdm

# from isaacRL.experts.trajopt import TrajectoryOptimizer, IKOptimizer
from isaacRL.testers.utils import *

def main():

    device = "cpu"

    config = file_util.load_yaml('configs/tests/stack_traj.yaml')
    config['environ']['num_envs'] = 1
    config['environ']['headless'] = False
    config['environ']['device'] = device # -1 if random
    config['environ']['use_gpu'] = device == 'cuda'
    config['environ']['seed'] = -1 # if random
    config['environ']['seed'] = set_seed(
      config['environ']['seed'], torch_deterministic=False
    )
    config['environ']['act_range'] = config['model']['actor']['act_range']
    config['environ']['additive_actions'] = False
    config['environ']['allow_resets'] = False
    config['environ']['max_env_steps'] = 500

    import rospy
    print('using ros')
    rospy.init_node('test_joints')

    envir = GymTask(config['environ'])
    state = envir.reset()

    np.set_printoptions(precision=3)

    joints = torch.zeros((1,8))
    joints[0,:7] = state['fullstate'][:,state['state_idx']['joint_position']][0,:7]
    state = run_steps(envir, joints, 100)
    obs_joints, obs_ee_pose = get_joints_and_ee_pose(envir, state)
    obs_joints = obs_joints[0].cpu().numpy()
    obs_ee_pose = obs_ee_pose[0].cpu().numpy()
    print("EE Pose:", obs_ee_pose)
    print("Joints:", obs_joints)

    if True:
        for i in range(10):
            changes = input('joint change:')
            changes = changes.split(' ')
            if changes[0] == 'a':
                joints[:,:len(changes)-1] += torch.tensor([float(c) for c in changes[1:]])
            elif changes[0] == 'f':
                joints[:,:len(changes)-1] = torch.tensor([float(c) for c in changes[1:]])
            elif changes[0].isnumeric():
                joints[:,int(changes[0])] += float(changes[1])
            state = run_steps(envir, joints, 100)
            obs_joints, obs_ee_pose = get_joints_and_ee_pose(envir, state)
            obs_joints = obs_joints[0].cpu().numpy()
            obs_ee_pose = obs_ee_pose[0].cpu().numpy()
            print("EE Pose:", obs_ee_pose)
            print("Joints:", obs_joints)
    else:
        urdf = '~/isaac_ws/src/ll4ma_robots_description/urdf/iiwa/static/iiwa_reflex_static.urdf'
        urdf = os.path.expanduser(urdf)
        links = [
          'iiwa_link_1', 'iiwa_link_2', 'iiwa_link_3', 'iiwa_link_4',
          'iiwa_link_5', 'iiwa_link_6', 'iiwa_link_7', 'reflex_palm_link'
        ]
        from ll4ma_sdf_collisions.robots import Moveitbot
        mbot = Moveitbot(urdf, links, 'world')

        from ll4ma_sdf_collisions.robots import KDLbot
        kbot = KDLbot(urdf, links, 'world')
        change =  0.5
        for i in range(6,-1,-1):
            joints[0,i] += change
            run_steps(envir, joints, 100)
            start_joints, start_ee_pose = get_joints_and_ee_pose(envir, state)
            start_joints = start_joints[0].cpu().numpy()
            start_ee_pose = start_ee_pose[0].cpu().numpy()
            print('Start Joints:', start_joints)
            print('Start EE Pose:', start_ee_pose)
            # input('wait...')
            joints[0,i] -= change*2
            run_steps(envir, joints, 100)
            start_joints, start_ee_pose = get_joints_and_ee_pose(envir, state)
            start_joints = start_joints[0].cpu().numpy()
            start_ee_pose = start_ee_pose[0].cpu().numpy()
            print('Start Joints:', start_joints)
            print('Start EE Pose:', start_ee_pose)
            # input('wait...')

    rospy.is_shutdown()





def get_ik_actions(envir):
    from ll4ma_util import math_util
    # target_pose = envir.state[:,envir.gymtask.state_idx['bottle']].cpu().numpy()
    # target_pose[:,2] += 0.2
    # target_pose = torch.tensor(
    #    [[ 1.,  0., 0.,  0.62],
    #     [ 0.,  1., 0.,  0.20],
    #     [ 0.,  0., 1.,  0.87],
    #     [ 0.,  0.,  0.,  1.]]
    # )
    target_quat = torch.tensor([[0.5, -0.3,  1.1, 0.60909081,  0.36338773, -0.60599518,  0.36017716]]*envir.num_envs)
    # target_rot = math_util.quat_to_rotation(target_quat[0])
    target_rot = math_util.pose_to_homogeneous(target_quat[0,:3], target_quat[0,3:])

    from ll4ma_opt.solvers import PenaltyMethod, AugmentedLagranMethod
    from ll4ma_opt.problems import IiwaIK
    params = {'alpha': 1, 'rho': 0.5}
    FP_PRECISION = 1e-5
    problem = IiwaIK(
      desired_pose=target_rot,
      error_type='pose',
      use_collision=False,
      collision_env=(
        1, 0.1, (torch.tensor([0.3, 0., 0.99]), torch.tensor([0.3, 0., 0.99]))
      ),
      soft_limits=False
    )
    solver = PenaltyMethod(problem, 'gauss newton', params, FP_PRECISION=FP_PRECISION)
    result = solver.optimize(problem.initial_solution, max_iterations=50)
    joints = torch.tensor(result.iterates[-1].squeeze())
    opt_ik = joints = torch.stack([joints]*2)

    from moveit_interface import util as moveit_util
    ee_link = envir.gymtask.robot.end_effector.get_link()
    ikresp, _ = moveit_util.get_ik(target_quat.numpy(), ee_link, 5)
    moveit_ik = torch.tensor([ik.position for i, ik in enumerate(ikresp.solutions) if ikresp.solution_found[i]])[2:]
    print(moveit_ik[0])

    actions = torch.zeros((envir.num_envs, 8), dtype=envir.actions.dtype, device=device)
    actions[:2,:-1] += opt_ik[:]
    actions[2:,:-1] += moveit_ik[:]
    # actions[:,:-1] += joints[:]
    actions[:,-1] += 0.5
    return torch.stack([actions]*envir.max_steps)


def get_trajectory(envir, state):
    target = torch.tensor([-1.2159,  1.0083,  1.7099, -1.1944,  2.0726, -1.8789, -0.9745])
    num_joints = 7
    from ll4ma_opt.solvers import PenaltyMethod, AugmentedLagranMethod
    from ll4ma_opt.problems import IiwaKDLTraj
    from ll4ma_util import math_util
    params = {'alpha': 1, 'rho': 0.5}
    FP_PRECISION = 1e-5
    state_idx = state['state_idx']
    start_joints = state['fullstate'][:,state_idx['joint_position']][0,:7].cpu()
    problem = IiwaKDLTraj(
      desired_pose=target,
      timesteps=20,
      start_joints=start_joints,
      error_type='position',
      use_collision=False,
      collision_env=(
        1, 0.1, (torch.tensor([0.3, 0., 0.99]), torch.tensor([0.3, 0., 0.99]))
      ),
      soft_limits=False
    )
    solver = PenaltyMethod(problem, 'gauss newton', params, FP_PRECISION=FP_PRECISION)
    initial_solution = torch.zeros((*problem.initial_solution.shape)).reshape(-1,num_joints)
    initial_solution += start_joints
    result = solver.optimize(initial_solution.reshape(-1,1).numpy(), max_iterations=25)
    joints = torch.tensor(result.iterates[-1].squeeze()).reshape(-1,num_joints)
    joints = joint_interpolation(joints)

    actions = torch.zeros((envir.max_steps, envir.num_envs, num_joints+1), dtype=envir.actions.dtype)
    joints = torch.stack([joints]*envir.num_envs).transpose(0,1)
    actions[:joints.shape[0],:,:num_joints] += joints
    actions[joints.shape[0]:,:,:num_joints] += joints[-1,:,:]
    actions[:,:,-1] += 0.5
    return actions.to(envir.device)

def joint_interpolation(joints):
    interps = torch.zeros((joints.shape[0]*2, joints.shape[1]))
    interps[::2,:] += joints[:]
    interps[1:-1:2,:] += (joints[1:] + joints[:-1])/2
    interps[-1,:] += joints[-1]
    return interps

if __name__ == '__main__':
    main()
