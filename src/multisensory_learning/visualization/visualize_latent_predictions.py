#!/usr/bin/env python
import os
import sys
import pickle
import argparse
import itertools
import numpy as np
import cv2
import matplotlib.pyplot as plt

import rospy
from sensor_msgs.msg import JointState, Image
from tf2_msgs.msg import TFMessage
from geometry_msgs.msg import TransformStamped, PoseStamped
from visualization_msgs.msg import Marker, MarkerArray
from moveit_msgs.msg import DisplayRobotState

from multisensory_learning.learning import MSPConfig, MSPLearner, msp_util
from ll4ma_util import data_util, file_util, ros_util, torch_util, func_util

import torch
from torch.utils.data import DataLoader


def visualize_data(data_filename, checkpoint_filename, device, rate=30,
                   predict_steps=20, one_step=False, auto_enc=False, idx=None):
    msp_config = MSPConfig(checkpoint=checkpoint_filename, device=device)
    msp_config.n_arm_joints = 7 # TODO need to get from elsewhere
    msp_learner = MSPLearner(msp_config, set_eval=True)

    data, attrs = file_util.load_pickle(data_filename)
    idx = 1 if idx is None else idx

    with torch.no_grad():
        latents = msp_learner.encode_raw_obs(data, subsample=msp_config.time_subsample).unsqueeze(1)
        act = msp_learner.encode_raw_act(data, subsample=msp_config.time_subsample).unsqueeze(1)
        
        if one_step:
            rospy.logwarn("Showing one-step predictions")
        
            if msp_learner.config.forward_dynamics == 'rssm':
                # TODO need to update this
                raise NotImplementedError("Need to implement for RSSM")
                
                # latents = outputs['rssm_out'].posterior_states # TODO can look at others
                # pred_decoded = msp_learner.decode_obs(latents, True)
            else:
                next_latents = msp_learner.predict_next_latent(latents[:-1], act[1:])
                pred_decoded = msp_learner.decode_obs(next_latents, process_out=True)

                next_latents = next_latents.squeeze(1)
                latents = latents.squeeze(1)[1:] # Drop first since that's only input for forward dyn
                
                error = (latents - next_latents).square().sum(-1)
                error = error.cpu().numpy()
                plt.plot(error)
                plt.show()
        elif auto_enc:
            rospy.logwarn("Showing autoencoding")
            pred_decoded = msp_learner.decode_obs(latents, process_out=True)
        else:
            rospy.logwarn("Showing multi-step predictions")
            if msp_learner.config.forward_dynamics == 'rssm':

                # TODO need to update this
                raise NotImplementedError("Need to implement for RSSM")
                
                
                # Take first belief and posterior state and loop through prior pathway
                # to get sequence of predicted latent state that we can decode from.
        
                hist = 10  # TODO maybe try longer history
                b_0 = outputs['rssm_out'].beliefs[hist-1]
                s_0 = outputs['rssm_out'].posterior_states[hist-1]
                act = outputs['act'][hist:] # (t, b, latent_act_size)
                rssm_out = models['forward_dynamics'](s_0, act, b_0)
                latents = rssm_out.prior_states
                pred_decoded = msp_learner.decode_obs(latents, True)
                # Add in initial decode from posterior
                for k, v in decoded.items():
                    first_decode = np.expand_dims(decoded[k][0], 0)
                    pred_decoded[k] = np.concatenate([first_decode, pred_decoded[k]], axis=0)
            else:
                # Start with latent 1 back since data is (act_t, o_tp1)
                next_latents = [latents[idx - 1].unsqueeze(0)]
                for i in range(predict_steps):
                    latent_i = next_latents[-1]
                    act_i = act[idx + i].unsqueeze(0)
                    next_latent = msp_learner.predict_next_latent(latent_i, act_i)
                    next_latents.append(next_latent)
                    
                next_latents = torch.cat(next_latents, dim=0)
                pred_decoded = msp_learner.decode_obs(next_latents, process_out=True)

                next_latents = next_latents.squeeze(1)[1:] # Exclude first that was only inp to FD
                latents = latents.squeeze(1)[idx:idx + predict_steps]

                error = (latents - next_latents).square().sum(-1)
                error = error.cpu().numpy()
                plt.plot(error)
                plt.show()
                

                    
    display_data = {}

    # Create obs data so it has right subsampling
    obs_data = msp_learner.get_subsampled_data(data, subsample=msp_config.time_subsample)
    
    for obj_name in msp_config.obj_names:
        obs_data[f'{obj_name}_obj_position'] = obs_data['objects'][obj_name]['position']
    
    for k, v in pred_decoded.items():
        display_data[f'{k}_pred'] = v if v.ndim > 1 else np.expand_dims(v, 0)

        # Skip first timestep in displaying since we want to show next-step predictions,
        # and the first step would have been used as initial obs to get predictions
        if k == 'arm_joint_position':
            if one_step:
                display_data[k] = obs_data['joint_position'][1:,:msp_config.n_arm_joints]
            elif auto_enc:
                display_data[k] = obs_data['joint_position'][:,:msp_config.n_arm_joints]
            else:
                display_data[k] = obs_data['joint_position'][:predict_steps+1,:msp_config.n_arm_joints]
                
        else:
            if one_step:
                display_data[k] = obs_data[k][1:]
            elif auto_enc:
                display_data[k] = obs_data[k]
            else:
                display_data[k] = obs_data[k][:predict_steps+1]
            

        # diff = display_data[k] - display_data[f'{k}_pred']
        # plt.plot(display_data[k], color='red')
        # plt.plot(display_data[f'{k}_pred'], color='blue')
        # # plt.plot(diff)
        # plt.show()

    for k, v in display_data.items():
        print(k, v.shape)
    
    rospy.loginfo("Creating ROS messages...")
    n_timesteps = len(next(iter(display_data.values())))

    # TODO need to log link and joint names in the metadata so you don't have to do this BS
    joint_names = ["iiwa_joint_{}".format(i) for i in range(1,8)]
    joint_names += ["reflex_preshape_joint_1", "reflex_proximal_joint_1",
                    "reflex_preshape_joint_2", "reflex_proximal_joint_2",
                    "reflex_proximal_joint_3"]
    link_names = ["iiwa_link_{}".format(i) for i in range(8)]
    link_names += ["reflex_distal_link_{}".format(i) for i in [1,2,3]]
    link_names += ["reflex_distal_pad_link_{}".format(i) for i in [1,2,3]]
    link_names += ["reflex_proximal_link_{}".format(i) for i in [1,2,3]]
    link_names += ["reflex_proximal_pad_link_{}".format(i) for i in [1,2,3]]
    link_names += ["reflex_flex_link_{}".format(i) for i in [1,2,3]]
    link_names += ["reflex_swivel_link_{}".format(i) for i in [1,2]]
    link_names += ["reflex_pad_link", "reflex_shell_link"]

    msgs = {k: [] for k in display_data.keys()}
    msgs['obj_pos'] = []
    msgs['obj_pos_pred'] = []
    if 'ee_position' in msgs and 'ee_orientation' in msgs:
        msgs['ee_pose'] = []
        msgs['ee_pose_pred'] = []
        del msgs['ee_position']
        del msgs['ee_position_pred']
        del msgs['ee_orientation']
        del msgs['ee_orientation_pred']
    
    for i in range(n_timesteps):
        if 'rgb' in msgs:
            msgs['rgb'].append(ros_util.rgb_to_msg(display_data['rgb'][i].squeeze()))
            msgs['rgb_pred'].append(ros_util.rgb_to_msg(display_data['rgb_pred'][i]))
            
        if 'arm_joint_position' in msgs:
            # TODO for now only doing arm, and hacked for iiwa-reflex
            joint_pos = np.concatenate([display_data['arm_joint_position'][i],
                                        np.zeros(5)])
            joint_pos_pred = np.concatenate([display_data['arm_joint_position_pred'][i],
                                             np.zeros(5)])
            msgs['arm_joint_position'].append(ros_util.get_display_robot_msg(
                joint_pos, joint_names, link_names, "mediumseagreen"))
            msgs['arm_joint_position_pred'].append(ros_util.get_display_robot_msg(
                joint_pos_pred, joint_names, link_names, "gold"))
            
        if 'ee_pose' in msgs:
            msgs['ee_pose'].append(ros_util.get_pose_stamped_msg(
                display_data['ee_position'][i], display_data['ee_orientation'][i]))
            orientation_pred = display_data['ee_orientation_pred'][i]
            orientation_pred /= np.linalg.norm(orientation_pred)  # Normalize to valid quaternion
            msgs['ee_pose_pred'].append(ros_util.get_pose_stamped_msg(
                display_data['ee_position_pred'][i], orientation_pred))
        elif 'ee_position' in msgs:
            msgs['ee_position'].append(ros_util.get_marker_msg(
                display_data['ee_position'][i], (0.1, 0.1, 0.1), None, 'sphere',
                'mediumseagreen', 0.6))
            msgs['ee_position_pred'].append(ros_util.get_marker_msg(
                display_data['ee_position_pred'][i], (0.1, 0.1, 0.1), None, 'sphere', 'red'))

        obj_pos = MarkerArray()
        obj_pos_pred = MarkerArray()
        for j, key in enumerate([k for k in msgs.keys() if '_obj_position' in k and 'pred' not in k]):
            obj_pos.markers.append(ros_util.get_marker_msg(
                display_data[key][i], (0.045, 0.045, 0.045), None, 'cube', 'mediumseagreen', 0.6, j))
            obj_pos_pred.markers.append(ros_util.get_marker_msg(
                display_data[f'{key}_pred'][i], (0.045, 0.045, 0.045), None, 'cube', 'gold', 1.0, j))
        msgs['obj_pos'].append(obj_pos)
        msgs['obj_pos_pred'].append(obj_pos_pred)
            
    rospy.loginfo("ROS messages created.")

    # Not all of these will necessarily be used if there's not data to publish for them
    rgb_pub = rospy.Publisher('/rgb', Image, queue_size=1)
    rgb_pred_pub = rospy.Publisher('/rgb_pred', Image, queue_size=1)
    joint_pos_pub = rospy.Publisher('/joint_pos', DisplayRobotState, queue_size=1)
    joint_pos_pred_pub = rospy.Publisher('/joint_pos_pred', DisplayRobotState, queue_size=1)
    ee_pose_pub = rospy.Publisher('/ee_pose', PoseStamped, queue_size=1)
    ee_pose_pred_pub = rospy.Publisher('ee_pose_pred', PoseStamped, queue_size=1)
    ee_pos_pub = rospy.Publisher('/ee_pos', Marker, queue_size=1)
    ee_pos_pred_pub = rospy.Publisher('/ee_pos_pred', Marker, queue_size=1)
    obj_pos_pub = rospy.Publisher('/obj_pos', MarkerArray, queue_size=1)
    obj_pos_pred_pub = rospy.Publisher('/obj_pos_pred', MarkerArray, queue_size=1)
    
    rospy.loginfo("Publishing messages...")
    rate = rospy.Rate(rate)
    while not rospy.is_shutdown():
        for i in range(n_timesteps):
            if 'rgb' in msgs:
                ros_util.publish_msg(msgs['rgb'][i], rgb_pub)
                ros_util.publish_msg(msgs['rgb_pred'][i], rgb_pred_pub)
            if 'arm_joint_position' in msgs:
                ros_util.publish_msg(msgs['arm_joint_position'][i], joint_pos_pub)
                ros_util.publish_msg(msgs['arm_joint_position_pred'][i], joint_pos_pred_pub)
            if 'ee_pose' in msgs:
                ros_util.publish_msg(msgs['ee_pose'][i], ee_pose_pub)
                ros_util.publish_msg(msgs['ee_pose_pred'][i], ee_pose_pred_pub)
            if 'ee_position' in msgs:
                ros_util.publish_msg(msgs['ee_position'][i], ee_pos_pub)
                ros_util.publish_msg(msgs['ee_position_pred'][i], ee_pos_pred_pub)
            if 'obj_pos' in msgs:
                ros_util.publish_msg(msgs['obj_pos'][i], obj_pos_pub)
            if 'obj_pos_pred' in msgs:
                ros_util.publish_msg(msgs['obj_pos_pred'][i], obj_pos_pred_pub)
            rate.sleep()
        rospy.sleep(2)
        

if __name__ == '__main__':
    rospy.init_node('visualize_latent_predictions')

    data_filename = rospy.get_param("~data_filename")
    checkpoint_filename = rospy.get_param("~checkpoint")
    use_cpu = rospy.get_param("~cpu", False)
    rate = rospy.get_param("~rate", 30)
    predict_steps = rospy.get_param("~predict_steps", None)
    one_step = rospy.get_param("~one_step", False)
    auto_enc = rospy.get_param("~auto_enc", False)
    idx = rospy.get_param("~idx", None)
    idx = None if idx is not None and idx < 0 else idx
        
    file_util.check_path_exists(data_filename, "Data file")
    file_util.check_path_exists(checkpoint_filename, "Model checkpoint file")
        
    device = 'cuda' if torch.cuda.is_available() and not use_cpu else 'cpu'

    visualize_data(data_filename, checkpoint_filename, device, rate, predict_steps,
                   one_step, auto_enc, idx)
