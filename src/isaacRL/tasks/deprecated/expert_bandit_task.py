
from isaacgym import gymtorch, gymapi

from isaacgymenvs.tasks import isaacgym_task_map
from isaacRL.tasks.gym_task import GymTask
from isaacRL.utils.utils import get_pinv

import os, time
import numpy as np
import torch

class ActionTypes:
    ACTION_TYPES = ['target_pose', 'ee_offset', 'ee_velocity', 'joint_position', 'joint_velocity']
    TARGET_POSE = ACTION_TYPES[0]
    EE_OFFSET = ACTION_TYPES[1]
    EE_VELOCITY = ACTION_TYPES[2]
    JOINT_POSITION = ACTION_TYPES[3]
    JOINT_VELOCITY = ACTION_TYPES[4]
class ActionPlanners:
    PLANNERS = ['trajopt', 'jacobian']
    TRAJOPT = PLANNERS[0]
    JACOBIAN = PLANNERS[1]


class ExpertBanditTask(GymTask):
    """docstring for ExpertBanditTask."""

    PLANNERS = ['trajopt', 'jacobian']

    def __init__(self, config, expert, max_steps=None):

        self._dof = 7
        self._max_steps = config['max_env_steps'] if max_steps is None else max_steps
        config['allow_resets'] = False

        self.steps_per_action = config.get('steps_per_action', 1)

        self._action_type = config['task'].get('action_type', 'target_pose')
        assert self._action_type in ActionTypes.ACTION_TYPES

        self._action_modif = config['task'].get('action_modifier', '')
        self._planner = config['task'].get('planner', 'trajopt')
        assert self._planner in ActionPlanners.PLANNERS

        self.expert = expert

        if self._action_type == ActionTypes.JOINT_POSITION or self._action_type == ActionTypes.JOINT_VELOCITY:
            config['additive_actions'] = False if 'rpl' in self._action_modif else True
            config['max_env_steps'] = self.expert.setup_steps + self._max_steps + self.expert.setdown_steps + 2
            if self._action_type == ActionTypes.JOINT_VELOCITY:
                config['vecolity_control'] = True
                print('doing correct?')
                import pdb; pdb.set_trace()
        elif self._action_type == ActionTypes.TARGET_POSE:
            config['additive_actions'] = False
            # set the max_env_steps to be a little over the number of trajectory steps
            config['max_env_steps'] = self.expert.setup_steps + self.expert.action_steps + self.expert.setdown_steps + 2

        super(ExpertBanditTask, self).__init__(config)

        self._preset_setup = None
        self._preset_actions = None
        self._preset_noise = None
        if self.gymtask.full_presets is not None:
            self._preset_setup = self.gymtask.full_presets['setups'].to(self.rl_device)
            if self._planner == 'trajopt':
                self._preset_actions = self.gymtask.full_presets['actions'].to(self.rl_device)
            if 'noise' in self.gymtask.full_presets:
                self._preset_noise = self.gymtask.full_presets['noise'].to(self.device)

        self.resets = torch.zeros_like(self.reset_buf)
        self.steps_i = torch.zeros_like(self.gymtask.progress_buf)
        self._success = 0
        self._runs = 0
        self._tot_resets = 0
        self._trajopt_time = 0
        self._setdown_time = 0

        self._state_idx = self.gymtask.state_idx
        if self._action_modif == 'rpla':
            min_idx = self.gymtask.fullstate.shape[-1]
            max_idx = min_idx + self.action_size
            self._state_idx['expert_actions'] = torch.arange(min_idx, max_idx)

        # self._use_setdown_act = self.action_size > self.num_joints
        self.setdown_threshold = 0.75


    def trajopt_time(self):
        return self._trajopt_time/self._tot_resets

    def step(self, actions=None):
        """
        adds the given action to the expert action
        """
        # TODO: if the action is meaningless because the env is about to be reset, don't run this? Save some time
        if not self.resets.all():
            if actions == None:
                actions = self.zero_actions()
            actions = self.scale_action(actions)

            if self._action_type == ActionTypes.TARGET_POSE:
                self._trajopt_step(actions)
            elif self._action_type in [ActionTypes.JOINT_POSITION, ActionTypes.JOINT_VELOCITY]:
                for _ in range(self.steps_per_action):
                    self._base_steps(actions.clone())
            elif self._action_type in [ActionTypes.EE_OFFSET, ActionTypes.EE_VELOCITY]:
                self._jacobian_steps(actions)

        if (self.resets > 0).any():
            self.reset_idx(self.resets.nonzero(as_tuple=False).squeeze(-1))

        if (self.steps_i >= self._max_steps).any():
            self.update_resets(self.steps_i >= self._max_steps)

        if (self.resets > 0).any():
            self.setdown_actions(self.resets.nonzero(as_tuple=False).squeeze(-1))

        state = self.get_state()
        rewards = self.get_reward()
        if (self.resets > 0).any():
            self._success += (rewards>0.9).sum().item()
            self._runs += (self.resets > 0).sum().item()
        dones = self.get_dones()
        info = self.get_info()
        return state, rewards, dones, info

    def _base_steps(self, actions):
        if actions.shape[1] == self.num_acts:
            self.steps_i[actions[:,-1] >= self.setdown_threshold] = self._max_steps
        elif actions.shape[1] < self.num_acts:
            actions = torch.cat([actions, torch.zeros_like(actions[:,0:1])], dim=1)
            actions[:,-1] = self.expert.grasp_value
        else:
            print('action size is bigger than expected')
            import pdb; pdb.set_trace()
        if 'rpl' in self._action_modif:
            eactions = self.expert.get_actions() if self._planner == 'trajopt' \
                else self.expert.get_actions(self.get_state())
            actions = self.add_actions(eactions, actions)
        self.gymtask.step(actions)
        self.steps_i += 1

    def add_actions(self, bactions, actions):
        ajnts = self.robot.arm.num_joints()
        bactions[:,:ajnts] += self.gymtask.scale_action(actions[:,:ajnts])
        # TODO: Maybe need to do something special when adding the end effector actions
        # bactions[:,ajnts:] += actions[:,ajnts:]
        return bactions

    def _trajopt_step(self, actions):
        eactions = self.expert.get_actions(self.get_state(), actions).to(self.rl_device)
        for action in eactions:
            state, reward, dones, info = self.gymtask.step(action)
        self.steps_i += 1

    def _jacobian_steps(self, actions):
        raise NotImplementedError('Jacobian Steps are not implemented')
        self.steps_i += 1

    def setdown_actions(self, env_ids):
        state = self.get_state(True)
        _state_idx = state['state_idx']
        _state = state['fullstate']
        start = time.time()
        setdown_actions = self.expert.get_setdown_actions(state, env_ids).to(self.rl_device)
        self._setdown_time += time.time() - start
        if setdown_actions.shape[1] != self.num_envs:
            full_setdown_actions = torch.stack([self.same_actions()]*setdown_actions.shape[0])
            full_setdown_actions[:,env_ids] = setdown_actions[:]
            setdown_actions = full_setdown_actions

        temp = self.gymtask.additive
        self.gymtask.additive = False
        for action in setdown_actions:
            self.gymtask.step(action)
        self.gymtask.additive = temp

    def reset(self):
        """
        resets the expert too
        """
        self._success = 0
        self._runs = 0
        self._tot_resets = 0
        self._trajopt_time = 0
        self._setdown_time = 0
        return self.reset_idx(torch.arange(self.num_envs))

    def reset_idx(self, env_ids, envreset=True):
        """
        resets the expert at each env too
        """
        if envreset:
            self.gymtask.reset_idx(env_ids)
            self.gymtask.step(self.same_actions())
        if self._preset_actions is not None:
            self._preset_act_idx = \
                torch.randint(0, self._preset_actions.shape[1], (len(env_ids),))
        if self._preset_noise is not None:
            if self._preset_actions is None:
                self._preset_act_idx = \
                    torch.randint(0, self._preset_noise.shape[1], (len(env_ids),))
            self.gymtask.object_noises[env_ids] = \
                self._preset_noise[self.preset_idx, self._preset_act_idx]

        state = self.get_state(True)
        if 'rpl' not in self._action_modif:
            start = time.time()
        if self._preset_setup is not None:
            setup_actions = self._preset_setup[self.preset_idx].transpose(0,1)
        else:
            setup_actions = self.expert.get_setup_actions(state, env_ids).to(self.rl_device)
        if 'rpl' not in self._action_modif:
            self._trajopt_time += time.time() - start
        if setup_actions.shape[1] != self.num_envs:
            full_setup_actions = torch.stack([self.same_actions()]*setup_actions.shape[0])
            full_setup_actions[:,env_ids] = setup_actions[:]
            setup_actions = full_setup_actions

        temp = self.gymtask.additive
        self.gymtask.additive = False
        for action in setup_actions:
            self.gymtask.step(action)
        self.gymtask.additive = temp

        state = self.get_state()
        if 'rpl' in self._action_modif:
            start = time.time()
            if self._preset_actions is not None:
                actions = self._preset_actions[self.preset_idx, self._preset_act_idx]
                self.expert.set_actions(env_ids, actions)
            else:
                self.expert.reset_idx(env_ids, state)
            self._trajopt_time += time.time() - start

        self.steps_i[env_ids] *= 0
        self.resets[env_ids] *= 0
        self._tot_resets += 1
        if self._tot_resets % 10 == 0:
            self.expert.reset_setup()

        return state

    def update_resets(self, resets):
        self.resets[:] = torch.where(
          resets,
          torch.ones_like(self.resets),
          self.resets
        )

    def zero_actions(self, state=None):
        zeros = None
        if self._action_type == ActionTypes.TARGET_POSE:
            zeros = torch.zeros((self.num_envs, self._dof), device=self.rl_device)
            if state is not None:
                ee_pose = state['fullstate'][:,state['state_idx']['ee_state']]
                zeros[:,3:7] = ee_pose[:,3:7].clone()
            else:
                zeros[:,-1] = 1.
        elif self._action_type == ActionTypes.EE_OFFSET:
            zeros = torch.zeros((self.num_envs, self._dof), device=self.rl_device)
            zeros[:,-1] = 1.
        elif self._action_type == ActionTypes.EE_VELOCITY:
            zeros = torch.zeros((self.num_envs, self._dof-1), device=self.rl_device)
        else:
            zeros = super().zero_actions()
        return zeros

    def scale_action(self, actions):
        if self._action_type in [ActionTypes.EE_OFFSET, ActionTypes.TARGET_POSE]:
            state = self.get_state()
            state_idx = state['state_idx']
            state = state['fullstate']
            actions[:,:3] *= 0.5
            actions[:,3:7] = torch.nn.functional.normalize(actions[:,3:7])
            if self._action_type == ActionTypes.EE_OFFSET:
                ee = state[:,state_idx['ee_state']].clone()
                actions = qpose_mul(ee, actions)
            elif self._action_type == ActionTypes.TARGET_POSE:
                goal = state[:,state_idx['goal']].clone()
                # actions = qpose_mul(goal, actions)
                actions[:,:3] += goal[:,:3] + torch.stack(
                    [self.expert.target_prior[0,:3]]*goal.shape[0]
                ).to(goal.device)
                # actions[:,3:7] = quat_mul(self.expert.target_prior[:,3:7], actions[:,3:7])
        return actions

    def get_state(self, update_goals=False):
        self.gymtask.update_env_states(update_goals)
        obs_dict = {}
        obs_dict['obs'] = self.full2obs(self.gymtask.fullstate).clone()
        obs_dict['fullstate'] = self.gymtask.fullstate.clone()
        obs_dict['state_idx'] = self._state_idx
        if self.gymtask.use_rgb:
            obs_dict['rgb'] = self.gymtask.fullstate[:,self.state_idx['rgb']]
        if self.gymtask.use_depth:
            obs_dict['depth'] = self.gymtask.fullstate[:,self.state_idx['depth']]
        if self._action_modif == 'rpla':
            # add next action to the state
            next_eactions = self.expert.get_actions(False)
            next_eactions[:,:7] = self.gymtask.scale_joint_position(next_eactions[:,:7])
            obs_dict['fullstate'] = torch.cat(
                [obs_dict['fullstate'], next_eactions.to(self.rl_device)], dim=1
            )
        return obs_dict

    def full2obs(self, state):
        obs = super().full2obs(state)
        if self._action_modif == 'rpla':
            if state.shape[1] == self.full_state_size:
                next_eactions = state[:,self._state_idx['expert_actions']]
            else:
                next_eactions = self.expert.get_actions(False)
                next_eactions[:,:7] = self.gymtask.scale_joint_position(next_eactions[:,:7])
            obs = torch.cat([obs, next_eactions.to(self.rl_device)], dim=1)
        return obs

    def get_reward(self):
        if not (self.resets > 0).any():
            return self.gymtask.rew_buf.clone()*0.
        self.gymtask.compute_reward()
        reward = self.gymtask.rew_buf.clone()
        reward[self.resets == 0] *= 0.
        return reward

    def add_preset(self, preset):
        self.gymtask.add_preset(preset)
        self._preset_setup = self.gymtask.full_presets['setups'].to(self.rl_device)
        if self._planner == 'trajopt':
            self._preset_actions = self.gymtask.full_presets['actions'].to(self.rl_device)
        if 'noise' in self.gymtask.full_presets:
            self._preset_noise = self.gymtask.full_presets['noise'].to(self.device)

    def get_preset_states(self):
        env_ids = torch.arange(self.num_envs)
        randomize = self.gymtask.randomize
        same_starts = self.expert.same_starts
        # self.expert.same_starts = False
        self.gymtask.randomize = 'inf'

        self.gymtask.reset_idx(env_ids)
        self.gymtask.step(self.same_actions())
        state = self.get_state(True)
        preset_state = self.gymtask.get_preset_states()
        noise = self.gymtask.object_noises.clone()

        # add setup trajectory
        setup_actions = self.expert.get_setup_actions(state, env_ids)

        # get act trajectory
        temp = self.gymtask.additive
        self.gymtask.additive = False
        for action in setup_actions:
            self.gymtask.step(action)
        self.gymtask.additive = temp

        state = self.get_state()
        assert 'rpl' in self._action_modif
        self.expert.reset_idx(env_ids, state)

        actions = self.expert.actions

        self.expert.same_starts = same_starts
        self.gymtask.randomize = randomize
        return preset_state, setup_actions, actions, noise

    def get_dones(self):
        return self.resets.clone()

    def get_info(self):
        return self.gymtask.extras

    def get_objects(self):
        return self.gymtask.get_objects()

    def successes(self):
        return self._success

    def runs(self):
        return self._runs

    @property
    def full_state_size(self):
        siz = self.gymtask.fullstate.shape[1]
        if self._action_modif == 'rpla':
            siz += self.action_size #self.config['num_acts']
        return siz

    @property
    def robot(self):
        return self.gymtask.robot

    @property
    def joint_names(self):
        return self.gymtask.joint_names

    @property
    def ee_name(self):
        return self.gymtask.ee_name

    @property
    def preset_idx(self):
        return self.gymtask._preset_idx

    @property
    def randomize(self):
        return self.gymtask.randomize
