#!/usr/bin/env python
import os
import sys
import argparse
import pickle
import numpy as np
from tqdm import tqdm

import torch
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
from torch.optim import Adam
from torch.optim.lr_scheduler import StepLR

from multisensory_learning.models import ImageEncoder, ImageDecoder, MLP
from multisensory_learning.util import file_util, data_util, torch_util

# from multisensory_learning.datasets.multisensory_prediction_dataset import MultisensoryPredictionDataset
from multisensory_learning.datasets.autoenc_dataset import AutoencoderDataset


def get_models(config):

    models = {}
    combined_embedding_size = 0
    for modality in config['modalities']:
        if modality in ['rgb', 'depth']:
            img_channels = 3 if modality == 'rgb' else 1
            encoder = ImageEncoder(img_channels, config['img_embedding_size'],
                                   config['img_enc_out_channels'], config['img_enc_kernel_sizes'],
                                   config['img_enc_strides'], config['img_enc_paddings'],
                                   config['activation'], config['cnn_batch_norm'])
            decoder = ImageDecoder(img_channels, config['multisensory_embedding_size'],
                                   config['img_embedding_size'], config['img_dec_out_channels'],
                                   config['img_dec_kernel_sizes'], config['img_dec_strides'],
                                   config['img_dec_paddings'], config['activation'],
                                   config['img_out_activation'], config['cnn_batch_norm'])
            combined_embedding_size += config['img_embedding_size']
        elif modality in ['joint_position']:
            # TODO need to get from elsewhere
            modality_size = 9
            enc_activations = [config['activation']] * len(config['vec_enc_sizes'])
            enc_layer_norms = [config['vec_layer_norm']] * len(config['vec_enc_sizes'])
            encoder = MLP(modality_size, config['vec_enc_sizes'],
                          enc_activations, enc_layer_norms)
            dec_sizes = config['vec_dec_sizes'][::]
            dec_activations = [config['activation']] * len(dec_sizes)
            dec_layer_norms = [config['activation']] * len(dec_sizes)
            # Add last layer
            dec_sizes.append(modality_size)
            dec_activations.append('tanh') # TODO can do switch on out activation like img channels
            dec_layer_norms.append(False)
            decoder = MLP(config['multisensory_embedding_size'], dec_sizes,
                          dec_activations, dec_layer_norms)
            combined_embedding_size += config['vec_enc_sizes'][-1]
        else:
            raise ValueError(f"Unknown modality: {modality}")
        models[f'{modality}_encoder'] = encoder
        models[f'{modality}_decoder'] = decoder

    # Add multisensory models if there are multiple modalities
    if len(config['modalities']) > 1:
        sizes = config['multisensory_hidden_sizes'][::]
        sizes.append(config['multisensory_embedding_size'])
        activations = [config['activation']] * len(sizes)
        layer_norms = [config['vec_layer_norm']] * len(sizes)
        # TODO do you want activation/LN on last layer?
        activations[-1] = None
        layer_norms[-1] = False
        models['multisensory_encoder'] = MLP(combined_embedding_size, sizes, activations,
                                             layer_norms)
    return models


def encode_observations(batch, models):
    encoded = []
    for modality, data in batch.items():
        encoded.append(models[f'{modality}_encoder'](data))
    if len(batch) > 1:
        encoded = models['multisensory_encoder'](torch.cat(encoded, dim=-1))
    else:
        encoded = encoded[0]
    return encoded


def decode_observations(batch, models):
    decoded = {}
    for model_name, model in models.items():
        if 'decoder' not in model_name:
            continue
        modality = model_name.replace("_decoder", "")
        decoded[modality] = model(batch)
    return decoded


def compute_loss(batch, decoded):
    loss = 0
    for modality, ground_truth in batch.items():
        modality_loss = F.mse_loss(decoded[modality], ground_truth, reduction='none')
        sum_dims = list(range(modality_loss.dim()))[1:]  # Skip batch dim
        modality_loss = modality_loss.sum(dim=sum_dims)
        loss += modality_loss
    loss = loss.mean(dim=0)  # Mean over batch
    return loss


def main():

    parser = argparse.ArgumentParser()
    # Learning ----------------------------------------------------------------------------------
    parser.add_argument('--trainpath', type=str, default='../mdn/data/train_data.txt')
    parser.add_argument('--validpath', type=str, default='../mdn/data/val_data.txt')
    parser.add_argument('--checkpoint_dir', type=str, default='checkpoints')
    parser.add_argument('--batch_size', type=int, default=50)
    parser.add_argument('--device', type=str, default='cuda')
    parser.add_argument('--learning_rate', type=float, default=1e-4)
    parser.add_argument('--lr_step', type=int, default=200)
    parser.add_argument('--adam_eps', type=float, default=1e-4)
    parser.add_argument('--n_epochs', type=int, default=500)
    parser.add_argument('--validate_interval', type=int, default=3)
    parser.add_argument('--checkpoint_interval', type=int, default=3)
    parser.add_argument('--continu', type=str, default=None)
    # Model -------------------------------------------------------------------------------------
    parser.add_argument( '--modalities', type=str, nargs='+',
      default=["rgb", "joint_position"] )
    parser.add_argument('--activation', type=str, default='prelu')
    parser.add_argument('--cnn_bn', dest='cnn_batch_norm', action='store_true')
    parser.add_argument('--no_cnn_bn', dest='cnn_batch_norm', action='store_false')
    parser.add_argument('--vec_ln', dest='vec_layer_norm', action='store_true')
    parser.add_argument('--no_vec_ln', dest='vec_layer_norm', action='store_false')
    parser.add_argument('--img_out_activation', type=str, default='sigmoid')
    parser.add_argument('--img_embedding_size', type=int, default=128)
    parser.add_argument('--img_enc_out_channels', type=int, nargs='+', default=[32, 64, 128, 256])
    parser.add_argument('--img_enc_kernel_sizes', type=int, nargs='+', default=[4]*4)
    parser.add_argument('--img_enc_strides', type=int, nargs='+', default=[2]*4)
    parser.add_argument('--img_enc_paddings', type=int, nargs='+', default=[0]*4)
    parser.add_argument('--img_dec_out_channels', type=int, nargs='+', default=[128, 64, 32])
    parser.add_argument('--img_dec_kernel_sizes', type=int, nargs='+', default=[5, 5, 6, 6])
    parser.add_argument('--img_dec_strides', type=int, nargs='+', default=[2]*4)
    parser.add_argument('--img_dec_paddings', type=int, nargs='+', default=[0]*4)
    parser.add_argument('--vec_enc_sizes', type=int, nargs='+', default=[16, 16])
    parser.add_argument('--vec_dec_sizes', type=int, nargs='+', default=[16, 16])
    parser.add_argument('--multisensory_embedding_size', type=int, default=128)
    parser.add_argument('--multisensory_hidden_sizes', type=int, nargs='+', default=[256])
    # Defaults ----------------------------------------------------------------------------------
    parser.set_defaults(cnn_batch_norm=True, vec_layer_norm=True)

    args = parser.parse_args()

    os.makedirs(args.checkpoint_dir, exist_ok=True)

    trainloss = []
    valloss = []

    if args.continu is not None:
        checkpoint = torch.load(args.continu)
        models = get_models(checkpoint)
        torch_util.load_state_dicts(models, checkpoint)
        trainloss = checkpoint['trainloss']
        valloss = checkpoint['valloss']
        params = torch_util.accumulate_parameters(models)
        optimizer = Adam(params, lr=args.learning_rate, eps=args.adam_eps)
        optimizer.load_state_dict(checkpoint['optimizer'])
    else:
        models = get_models(vars(args))
        params = torch_util.accumulate_parameters(models)
        optimizer = Adam(params, lr=args.learning_rate, eps=args.adam_eps)
    torch_util.move_models_to_device(models, args.device)
    torch_util.set_models_to_train(models)
    scheduler = StepLR(optimizer, step_size=args.lr_step)

    with open(args.trainpath, 'r') as f:
        filenames = [line.strip() for line in f][:2]
    trainset = AutoencoderDataset(filenames, args.modalities)
    trainloader = DataLoader( trainset, args.batch_size, shuffle=True,
      num_workers=2, pin_memory=True )

    with open(args.validpath, 'r') as f:
        filenames = [line.strip() for line in f]
    valset = AutoencoderDataset(filenames, args.modalities)
    valloader = DataLoader( valset, args.batch_size, shuffle=True,
      num_workers=2, pin_memory=True )

    for epoch in range(1, args.n_epochs + 1):
        pbar = tqdm(total=len(trainloader.dataset), file=sys.stdout)
        losses = []
        for bi, batch in enumerate(trainloader):
            torch_util.move_batch_to_device(batch, args.device)

            encoded = encode_observations(batch, models)
            decoded = decode_observations(encoded, models)

            loss = compute_loss(batch, decoded)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            losses.append(loss.item())
            desc = f'  Epoch {epoch}: loss={np.mean(losses):.4f}'
            pbar.set_description(desc)
            pbar.update(args.batch_size)
        pbar.close()
        trainloss.append( np.mean(losses) )

        if epoch % args.validate_interval == 0:
            pbar = tqdm(total=len(valloader.dataset), file=sys.stdout)
            with torch.no_grad():
                valos = 0.0
                for bi, batch in enumerate(valloader):
                    torch_util.move_batch_to_device(batch, args.device)

                    encoded = encode_observations(batch, models)
                    decoded = decode_observations(encoded, models)

                    loss = compute_loss(batch, decoded)
                    valos += loss.item()

                    desc = f'  Validation {epoch}: loss={(valos/(bi+1)):.4f}'
                    pbar.set_description(desc)
                    pbar.update(args.batch_size)
                valloss.append( valos )
            pbar.close()

            print('train loss:', trainloss[-1])
            print('valloss:', valloss[-1])

        if epoch % args.checkpoint_interval == 0:
            checkpoint = vars(args)
            checkpoint['trainloss'] = trainloss
            checkpoint['valloss'] = valloss
            checkpoint['optimizer'] = optimizer.state_dict()
            for k, v in models.items():
                checkpoint[k] = v.state_dict()
            filename = os.path.join(args.checkpoint_dir, f'autoencoder_epoch_{epoch:03d}.pt')
            torch.save(checkpoint, filename)

        scheduler.step()

if __name__ == '__main__':
    main()
