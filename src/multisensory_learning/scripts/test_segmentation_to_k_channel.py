import h5py
import numpy as np
import argparse
import matplotlib.pyplot as plt

from multisensory_learning.util import data_util, file_util


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--h5_file', type=str, required=True)
    args = parser.parse_args()

    file_util.check_path_exists(args.h5_file, "H5 file")

    with h5py.File(args.h5_file, 'r') as h5_file:
        semantic_seg = np.array(h5_file['semantic_segmentation'][:2])
        seg_ids = h5_file['semantic_segmentation'].attrs['ids']
        
    colors = [np.random.random(3) for _ in seg_ids]
    seg_rgb = data_util.segmentation_to_rgb(semantic_seg[0], seg_ids, colors)
    plt.imshow(seg_rgb)
    plt.show()

    k_channel = data_util.segmentation_to_k_channel(semantic_seg, seg_ids)
    for i in range(k_channel.shape[0]):
        for j in range(k_channel.shape[1]):
            plt.imshow(k_channel[i,j,:,:])
            plt.show()
        
