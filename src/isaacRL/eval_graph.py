
from isaacRL.full_eval import *

def main():
    # config = {'environ': {}}
    # opt, trainer = setup(config, eval=True)
    # config = trainer.config
    # if opt.use_ros:
    #     import rospy
    #     print('using ros')
    #     rospy.init_node('eval_isaacgym')
    #
    # use_eval_data = True
    # checkpt_path = config['training']['checkpoint_path']
    # checkpt_name = config['training']['checkpoint_name']
    # if use_eval_data:
    #     filename = os.path.join(checkpt_path, 'eval_data.torch')
    # else:
    #     filename = os.path.join(checkpt_path, 'train_data.torch')
    # save_data = torch.load(filename)
    #
    # besti = np.argmax(save_data['success_percents'])
    # print('best success:', besti, save_data['success_percents'][besti])
    #
    # filename = os.path.join(checkpt_path, 'train_success.png')
    # if use_eval_data:
    #     filename = os.path.join(checkpt_path, 'eval_success.png')
    # create_graph(filename, save_data)

    files = ['checkpoints/sac/franka01/eval_data.torch', 'checkpoints/sac/franka10/eval_data.torch', 'checkpoints/sac/franka05/eval_data.torch', 'checkpoints/sac/franka06/eval_data.torch', 'checkpoints/sac/franka07/eval_data.torch', 'checkpoints/sac/franka08/eval_data.torch', 'checkpoints/sac/franka09/eval_data.torch']
    for filename in files:
        data = torch.load(filename)
        graph_name = filename.replace('eval_data.torch', 'eval_rewards.png')
        create_graph(graph_name, data, data_key='mean_rewards')


if __name__ == '__main__':
    main()
