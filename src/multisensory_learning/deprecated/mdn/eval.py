
from tqdm import tqdm

from mdn.utils.loss import kldiv_loss

def evaluate(model, dataset, tolatent):
    totLoss = 0.0
    pbar = tqdm(total=len(dataset))
    for di, xdata in enumerate(dataset):
        xdata, targets = tolatent(xdata, xdata['obs']['rgb'].shape[0])
        mus, sig = model(xdata)
        loss = kldiv_loss(mus, sig, targets)
        totLoss += loss
        desc = f'  Iter {di}: loss={(totLoss/(di+1)):.4f}'
        xdata = None
        targets = None
        mus = None
        sig = None
        loss = None
        pbar.set_description(desc)
        pbar.update(1)
    pbar.close()
    return totLoss / len(dataset)
