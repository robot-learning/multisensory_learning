#!/usr/bin/env python
import os
import sys
import argparse
import pickle
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

from multisensory_learning.learning import (
    DEFAULT_ENC_OBS_MODALITIES,
    DEFAULT_DEC_OBS_MODALITIES,
    DEFAULT_ACT_MODALITIES,
    DEFAULT_DATA_RANGES,
    MSPLearner, MSPConfig
)

from ll4ma_util import file_util, ui_util


class MSPSampleCreator:

    def __init__(self, enc_obs_modalities, dec_obs_modalities, act_modalities,
                 chunk_size, time_subsample, sample_subsample,
                 data_ranges=DEFAULT_DATA_RANGES, debug=False):
        self.enc_obs_modalities = enc_obs_modalities
        self.dec_obs_modalities = dec_obs_modalities
        self.act_modalities = act_modalities
        self.chunk_size = chunk_size
        self.time_subsample = time_subsample
        self.sample_subsample = sample_subsample
        self.data_ranges = data_ranges
        self.debug = debug
        
    def create_samples(self, data_root, target_dir, batch_size=64):
        data_collection_metadata_filename = os.path.join(data_root, "metadata.yaml")
        
        file_util.check_path_exists(data_root, "Data directory")
        file_util.check_path_exists(data_collection_metadata_filename, "Data metadata YAML file")
        file_util.safe_create_dir(target_dir)    
    
        filenames = file_util.list_dir(data_root, '.pickle')
        if self.debug:
            ui_util.print_warning("\n  DEBUG mode\n")
            filenames = filenames[:1]

        data_collection_metadata = file_util.load_yaml(data_collection_metadata_filename)

        msp_config = MSPConfig()
        msp_config.enc_obs_modalities = self.enc_obs_modalities
        msp_config.dec_obs_modalities = self.dec_obs_modalities
        msp_config.act_modalities = self.act_modalities
        msp_config.chunk_size = self.chunk_size
        msp_config.time_subsample = self.time_subsample
        msp_config.obj_names = [n for n in data_collection_metadata['env']['objects'].keys()
                                if not data_collection_metadata['env']['objects'][n]['fix_base_link']]
        # TODO need to figure out how to best get this info from data, should probably
        # be saved in metadata?
        msp_config.act_size = 7
        msp_config.n_arm_joints = 7
        msp_learner = MSPLearner(msp_config)
        
        metadata = {
            'act_modalities': self.act_modalities,
            'enc_obs_modalities': self.enc_obs_modalities,
            'dec_obs_modalities': self.dec_obs_modalities,
            'chunk_size': self.chunk_size,
            'time_subsample': self.time_subsample,
            'data_ranges': self.data_ranges,
            'data_collection': data_collection_metadata,
            'obj_names': msp_config.obj_names
        }
                
        sample_idx = 1
        subsample = self.time_subsample
        for filename in tqdm(filenames, desc="  Process samples"):
            data, attrs = file_util.load_pickle(filename)
            n_steps = len(data['rgb']) # TODO make more robust
            max_start_idx = n_steps - (self.chunk_size * self.time_subsample) - 1
            start_idxs = list(range(max_start_idx))[::self.sample_subsample]

            for start_idx in start_idxs:
                end_idx = start_idx + (self.time_subsample * self.chunk_size)
                sample = {
                    'obs': msp_learner.create_obs_from_data(data, start_idx, end_idx, subsample),
                    'act': msp_learner.create_act_from_data(data, start_idx, end_idx, subsample)
                }
                filename = os.path.join(target_dir, f"sample_{sample_idx:07d}.pickle")
                file_util.save_pickle(sample, filename)
                sample_idx += 1

                if self.debug:
                    joints = sample['obs']['arm_joint_position']
                    # delta = joints[1:] - joints[:-1]
                    act = sample['act']['delta_arm_joint_position']
                    # act = sample['act']['arm_joint_position']
                    summed = act + joints

                    joints = joints[1:]
                    
                    idx = 1
                    # plt.plot(delta[:,idx], label='delta')
                    plt.plot(joints, label='obs', color='r')
                    # plt.plot(act, label='act', color='b')
                    plt.plot(summed, label='act', color='b')
                    # plt.legend()
                    plt.show()
                    
                    sys.exit()

        # Save some metadata
        metadata['n_arm_joints'] = attrs['n_arm_joints']
        metadata['n_ee_joints'] = attrs['n_ee_joints']
        metadata['n_joints'] = metadata['n_arm_joints'] + metadata['n_ee_joints']
        metadata['act_size'] = 0  # Save action size (all samples are identical)
        for v in sample['act'].values():
            metadata['act_size'] += v.shape[-1]
        file_util.save_yaml(metadata, os.path.join(target_dir, 'metadata.yaml'))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_root', type=str, required=True,
                        help="Absolute path to directory containing demo instances (pickle files)")
    parser.add_argument('--target_dir', type=str, required=True,
                        help="Absolute path to directory where processed samples will be saved")
    parser.add_argument('--enc_obs_modalities', type=str, nargs='+',
                        default=DEFAULT_ENC_OBS_MODALITIES,
                        help="List of observation modalities to encode")
    parser.add_argument('--dec_obs_modalities', type=str, nargs='+',
                        default=DEFAULT_DEC_OBS_MODALITIES,
                        help="List of observation modalities to decode")
    parser.add_argument('--act_modalities', type=str, nargs='+',
                        default=DEFAULT_ACT_MODALITIES,
                        help="List of action modalities")
    parser.add_argument('--chunk_size', type=int, default=25,
                        help="Number of timesteps in each sample")
    parser.add_argument('--time_subsample', type=int, default=5,
                        help="Sub-sampling rate of data")
    parser.add_argument('--sample_subsample', type=int, default=1,
                        help=("Sub-sampling rate of consecutive samples (i.e. between the "
                              "start indices of temporal chunks of data)"))
    parser.add_argument('--batch_size', type=int, default=64,
                        help="Batch size to use for processing samples")
    parser.add_argument('--debug', action='store_true',
                        help="Use debug mode")
    args = parser.parse_args()

    print("\n  Encode Observation Modalities:")
    for m in args.enc_obs_modalities:
        print(f"      {m}")
    print("  Decode Observation Modalities:")
    for m in args.dec_obs_modalities:
        print(f"      {m}")
    print("  Action Modalities:")
    for m in args.act_modalities:
        print(f"      {m}")
    print(f"  Chunk Size: {args.chunk_size}")
    print(f"  Time Subsample: {args.time_subsample}")
    print(f"  Data Source: {args.data_root}")
    print(f"  Save Directory: {args.target_dir}")
    print()


    sample_creator = MSPSampleCreator(args.enc_obs_modalities, args.dec_obs_modalities,
                                      args.act_modalities, args.chunk_size, args.time_subsample,
                                      args.sample_subsample, debug=args.debug)
    sample_creator.create_samples(args.data_root, args.target_dir, args.batch_size)

    ui_util.print_happy("\n  Sample creation complete\n")
