#!/usr/bin/env python
import os
import sys
import ast
import argparse
import numpy as np
from tqdm import tqdm
from copy import deepcopy
from dataclasses import dataclass, field
from typing import List, Dict

from multisensory_learning.learning import msp_util, Learner, LearnerConfig
from multisensory_learning.models import MLP, RSSM, MDN
from ll4ma_util import data_util, file_util, ui_util, torch_util, func_util

import torch
import torch.nn.functional as F
from torch.distributions import Normal
from torch.distributions.kl import kl_divergence


# TODO need to bring back these default, but simplifying to kinematic for now
# DEFAULT_ENC_OBS_MODALITIES = ['rgb', 'depth', 'joint_position', 'joint_velocity', 'joint_torque']
# DEFAULT_DEC_OBS_MODALITIES = ['joint_position', 'joint_velocity', 'joint_torque',
#                               'ee_position', 'ee_velocity', 'obj_position', 'obj_velocity']
DEFAULT_ENC_OBS_MODALITIES = ['rgb', 'depth', 'arm_joint_position']
DEFAULT_DEC_OBS_MODALITIES = ['arm_joint_position', 'ee_position', 'obj_position']
DEFAULT_ACT_MODALITIES = ['delta_arm_joint_position']
DEFAULT_DATA_RANGES = {
    'delta_joint_position': [-2.*np.pi, 2*np.pi],
    'delta_arm_joint_position': [-2.*np.pi, 2*np.pi],
    'depth': [-4.0, 0.0],
    'ee_position': [-10.0, 10.0],
    'ee_velocity': [-20.0, 20.0],
    'joint_position': [-2.*np.pi, 2*np.pi],
    'arm_joint_position': [-2.*np.pi, 2*np.pi],
    'joint_torque': [-200.0, 200.0],
    'joint_velocity': [-5.0, 5.0],
    'arm_joint_velocity': [-5.0, 5.0],
    'obj_position': [-10.0, 10.0],
    'obj_velocity': [-20.0, 20.0]
}


@dataclass
class MSPConfig(LearnerConfig):
    enc_obs_modalities: List[str] = func_util.lambda_field(DEFAULT_ENC_OBS_MODALITIES)
    dec_obs_modalities: List[str] = func_util.lambda_field(DEFAULT_DEC_OBS_MODALITIES)
    act_modalities: List[str] = func_util.lambda_field(DEFAULT_ACT_MODALITIES)
    # MODEL ---------------------------
    activation: str = 'relu'
    cnn_norm: str = None # 'layer2d'
    vec_norm: str = 'layer'
    img_embedding_size: int = 128
    img_enc_out_channels: List[int] = func_util.lambda_field([32, 64, 128, 256])
    img_enc_kernel_sizes: List[int] = func_util.lambda_field([4]*4)
    img_enc_strides: List[int] = func_util.lambda_field([2]*4)
    img_enc_paddings: List[int] = func_util.lambda_field([0]*4)
    img_dec_out_channels: List[int] = func_util.lambda_field([128, 64, 32])
    img_dec_kernel_sizes: List[int] = func_util.lambda_field([5, 5, 6, 6])
    img_dec_strides: List[int] = func_util.lambda_field([2]*4)
    img_dec_paddings: List[int] = func_util.lambda_field([0]*4)
    img_out_activation: str = 'sigmoid'
    vec_embedding_size: int = 32
    vec_enc_sizes: List[int] = func_util.lambda_field([32, 32])
    vec_dec_sizes: List[int] = func_util.lambda_field([32, 32])
    multisensory_embedding_size: int = 64
    multisensory_hidden_sizes: List[int] = func_util.lambda_field([256, 256])
    forward_dynamics: str = 'mlp'
    rssm_belief_size: int = 64
    rssm_state_size: int = 64
    rssm_hidden_size: int = 128
    rssm_min_std_dev: int = 0.1
    rssm_free_nats: int = 3
    fd_mlp_sizes: List[int] = func_util.lambda_field([128, 128, 128])
    vae: bool = True
    # OPTIONS -----------------------------
    decode: bool = True
    next_latent_mse: bool = True
    contrastive: bool = False
    encode_act: bool = False
    latent_delta: bool = True
    diagnostic: bool = False
    # DATA ----------------------------------
    # TODO these should all come from metadata
    data_ranges: Dict[str, List[float]] = func_util.lambda_field(DEFAULT_DATA_RANGES)
    obj_names: List[str] = field(default_factory=list)
    act_size: int = None
    n_joints: int = None
    n_arm_joints: int = None
    n_ee_joints: int = None
    chunk_size: int = 25
    time_subsample: int = 5


class MSPLearner(Learner):

    def __init__(self, config, train_filenames=[], val_filenames=[],
                 set_train=False, set_eval=False):
        super().__init__(config, train_filenames, val_filenames, set_train, set_eval)
        # Allowed deviation in KL divergence (only used for RSSM)
        self.free_nats = torch.full((1, ), config.rssm_free_nats,
                                    dtype=torch.float32, device=config.device)

    def get_params(self):
        if self.config.diagnostic:
            self.train_models = {k: v for k, v in self.models.items() if '_obs_decoder' in k}
            fixed_models = {k: v for k, v in self.models.items() if '_obs_decoder' not in k}
            torch_util.set_models_to_train(self.train_models)
            torch_util.set_models_to_eval(fixed_models)
            params = torch_util.accumulate_parameters(self.train_models)
        else:
            torch_util.set_models_to_train(self.models)
            params = torch_util.accumulate_parameters(self.models)
        return params

    def get_batch_size(self, batch):
        return next(iter(batch['obs'].values())).size(0)

    def get_checkpoint_filename(self, epoch, episode=None):
        if episode is not None:
            filename = os.path.join(self.config.checkpoint_dir,
                                    f'msp_episode_{episode:03d}_epoch_{epoch:03d}.pt')
        else:
            filename = os.path.join(self.config.checkpoint_dir, f'msp_epoch_{epoch:03d}.pt')
        return filename

    def apply_models(self, batch):
        returns = {}
        using_rssm = self.config.forward_dynamics == 'rssm'
        
        msp_util.prepare_batch(batch, self.config)
        
        latent = self.encode_obs(batch['obs']) # x_tp1...x_Tp1
        act = self.encode_act(batch['act'])
                            
        # Passing back anything that might be useful on the other end
        returns['obs'] = batch['obs']
        returns['act'] = batch['act']
        returns['latent'] = latent

        if self.config.forward_dynamics is not None:
            if self.config.encode_act:
                returns['encoded_act'] = act

            # Current latent states will be x_tp1...x_Tp1 and act is a_t...a_T since data is
            # recorded as the action applied in this timestep that results in the observation
            # at the next timestep (apply action, step physics, record data).
            if using_rssm:
                raise NotImplementedError("Need to adjust data input for RSSM training")
            else:
                returns['next_latent'] = self.predict_next_latent(latent[:-1], act[1:])
    
        if self.config.decode:        
            dec_in = rssm_out.posterior_states if using_rssm else latent
            returns['dec_obs'] = self.decode_obs(dec_in)
    
        return returns
        
    def set_train(self):
        if self.config.diagnostic:
            torch_util.set_models_to_train(self.train_models)
        else:
            torch_util.set_models_to_train(self.models)

    def create_obs_from_data(self, data, start_idx=0, end_idx=None, subsample=1, process_in=True):
        obs = {}
        ranges = self.config.data_ranges
        for m in self.config.enc_obs_modalities:
            if m in ['obj_position', 'obj_velocity']:
                for obj_name in self.config.obj_names:
                    obj_data = data['objects'][obj_name]
                    data_key = m.replace('obj_', '')
                    obs_key = f'{obj_name}_{m}'
                    obs[obs_key] = obj_data[data_key][start_idx:end_idx:subsample]
                    if process_in:
                        obs[obs_key] = msp_util.process_data_in(obs[obs_key], m, ranges)
            elif m in ['arm_joint_position', 'arm_joint_velocity']:
                base_modality = m.replace('arm_', '')
                joint_data = data[base_modality]
                obs[m] = joint_data[start_idx:end_idx:subsample,:self.config.n_arm_joints]
                if process_in:
                    obs[m] = msp_util.process_data_in(obs[m], m, ranges)
            else:
                obs[m] = data[m][start_idx:end_idx:subsample]
                if process_in:
                    obs[m] = msp_util.process_data_in(obs[m], m, ranges)
        return obs

    def create_act_from_data(self, data, start_idx=0, end_idx=None, subsample=1, process_in=True):
        act = {}
        ranges = self.config.data_ranges
        for m in self.config.act_modalities:
            # TODO trying to debug the action modalities
            joint_data = data['joint_position']
            # joint_data = data['action']['joint_position']
            if m in ['delta_joint_position', 'delta_arm_joint_position']:
                # joint_data = data['action']['joint_position']
                joint_pos = joint_data[start_idx:end_idx:subsample]
                if m == 'delta_arm_joint_position':
                    joint_pos = joint_pos[:,:self.config.n_arm_joints]
                if start_idx < subsample:
                    first = joint_pos[0]
                else:
                    first = joint_data[start_idx - subsample]
                    if m == 'delta_arm_joint_position':
                        first = first[:self.config.n_arm_joints]
                joint_pos = np.concatenate([np.expand_dims(first, 0), joint_pos], axis=0)
                act[m] = joint_pos[1:] - joint_pos[:-1]
            elif m == 'arm_joint_position':
                act[m] = joint_data[start_idx:end_idx:subsample,:self.config.n_arm_joints]
            else:
                act[m] = data['action'][m][start_idx:end_idx:subsample]
            if process_in:
                act[m] = msp_util.process_data_in(act[m], m, ranges)
        return act

    def get_subsampled_data(self, data, start_idx=0, end_idx=None, subsample=1):
        sub = {}
        for k, v in data.items():
            if isinstance(v, dict):
                sub[k] = self.get_subsampled_data(v, start_idx, end_idx, subsample)
            else:
                sub[k] = data[k][start_idx:end_idx:subsample]
        return sub
            
    def encode_raw_obs(self, data, start_idx=0, end_idx=None, subsample=1, batch_size=128):
        """
        This is meant for encoding directly from obs data loaded from file that was
        recorded in the simulator. Useful for processing data with an already
        trained model.
        """
        obs = self.create_obs_from_data(data, start_idx, end_idx, subsample)        
        torch_util.move_batch_to_device(obs, self.device)
        obs = {k: torch.split(v, batch_size) for k, v in obs.items()}
        batches = [{k: v[i].unsqueeze(0) for k, v in obs.items()}
                   for i in range(len(next(iter(obs.values()))))]
        latents = torch.cat([self.encode_obs(b).squeeze(0) for b in batches])
        return latents

    def encode_raw_act(self, data, start_idx=0, end_idx=None, subsample=1, batch_size=128):
        """
        Encode/process action directly from raw data, this gets it to the form
        you can directly concat with an encoded obs to do forward prediction.
        """
        act = self.create_act_from_data(data, start_idx, end_idx, subsample)
        torch_util.move_batch_to_device(act, self.device)
        act = {k: torch.split(v, batch_size) for k, v in act.items()}
        batches = [{k: v[i].unsqueeze(0) for k, v in act.items()}
                   for i in range(len(next(iter(act.values()))))]
        act = torch.cat([self.encode_act(b).squeeze(0) for b in batches])
        return act

    def encode_obs(self, obs):
        """
        Assumes data is already processed for input to the network.
        """
        # Encode each modality individually
        enc = []
        for m in self.config.enc_obs_modalities:
            if m == 'obj_position':
                for obj_name in self.config.obj_names:
                    modality_name = f'{obj_name}_{m}'
                    model = self.models[f'{modality_name}_encoder']
                    enc.append(msp_util.process_time_batch(model, obs[modality_name]))
            else:
                enc.append(msp_util.process_time_batch(self.models[f'{m}_obs_encoder'], obs[m]))
                
        # Apply multisensory encoder if there's more than one observation modality
        if len(self.config.enc_obs_modalities) > 1:
            if 'multisensory_encoder' not in self.models:
                raise ValueError(f"Multisensory encoder model not found")
            enc = self.models['multisensory_encoder'](torch.cat(enc, dim=-1))
        else:
            enc = enc[0]
            
        return enc

    def encode_act(self, act):
        """
        Encode action modalities. A learned encoder model is used if option is activated,
        otherwise this simply process the action modality data for input to the network
        by concatenating them if there's more than one, or taking the one modality data
        if there is only one action modality.
        """
        if self.config.encode_act:
            enc = []
            for modality in self.config.act_modalities:
                model_name = f'{modality}_act_encoder'
                if model_name not in self.models:
                    raise ValueError(f"No encoder for action modality '{modality}'")
                if modality not in act:
                    raise ValueError(f"No data for encoding action modality '{modality}'")
                enc.append(msp_util.process_time_batch(self.models[model_name], act[modality]))
            enc = torch.cat(enc, dim=-1) if len(enc) > 0 else enc[0]
        elif len(self.config.act_modalities) > 1:
            enc = torch.cat([act[m] for m in self.config.act_modalities])
        else:
            enc = next(iter(act.values()))
        return enc
    
    def decode_obs(self, latents, process_out=False):
        """
        Decodes observation data from the latent space using the different modality
        decoder models. 

        Assumes latents are size (time, batch, *channels).
        """
        dec = {}
        for modality in self.config.dec_obs_modalities:
            if modality in ['obj_position', 'obj_velocity']:
                for obj_name in self.config.obj_names:
                    model_name = f'{obj_name}_{modality}_decoder'
                    dec_obj_data = msp_util.process_time_batch(self.models[model_name], latents)
                    dec[f'{obj_name}_{modality}'] = dec_obj_data
            else:
                model_name = f'{modality}_obs_decoder'
                if model_name not in self.models:
                    raise ValueError(f"No decoder for observation modality '{modality}'")
                dec[modality] = msp_util.process_time_batch(self.models[model_name], latents)
        if process_out:
            dec = msp_util.process_chunk_out(dec, self.config.data_ranges)
        return dec

    def predict_next_latent(self, latent, act):
        """
        latent (time, batch, embedding_size)
        act (time, batch, act_size)

        This will assume latents are x_tp1...x_Tp1 and act are a_t...a_T since data are
        recorded action applied to result in this observation (apply action, step physics,
        record data). RSSM assumes prev act and current obs as input (since its posterior
        prediction is of the current state) while other models assume current act current
        obs. So need to handle shifting here.
        """
        
        if self.config.forward_dynamics == 'rssm':
            # TODO this hasn't been updated yet
            raise NotImplementedError()
            
            # Batch size can be different on last batch than the size set
            batch_size = next(iter(batch['obs'].values())).size(1)
            b0 = torch.zeros(batch_size, self.config.rssm_belief_size, device=self.device)
            s0 = torch.zeros(batch_size, self.config.rssm_state_size, device=self.device)
            rssm_out = models['forward_dynamics'](s0, act, b0, encoded_obs)
            next_latent = rssm_out.posterior_states
            returns['rssm_out'] = rssm_out
        else:
            inputs = torch.cat([latent, act], dim=-1)
            # Need to do superbatch processing here since, even though it's acting over two
            # timesteps the input/output is independent (i.e. you're not looping back outputs).
            # Will want to adjust this though when you add horizon curriculum
            if self.config.latent_delta:
                delta = msp_util.process_time_batch(self.models['forward_dynamics'], inputs)
                next_latent = latent + delta
            else:
                next_latent = msp_util.process_time_batch(self.models['forward_dynamics'], inputs)
            return next_latent  # x_tp2...x_Tp1
    
    def get_forward_predictions(self, init_latent, act):
        latents = [init_latent]
        act = self.encode_act(act)
        n_steps = len(act)
        
        for i in range(n_steps):

            fd_inputs = torch.cat([latents[i], act[i]], dim=-1)
            if self.config.latent_delta:
                delta = self.models['forward_dynamics'](fd_inputs)
                next_latent = latents[-1] + delta
            else:
                next_latent = self.models['forward_dynamics'](fd_inputs)
            latents.append(next_latent)
            
        return torch.stack(latents)

    def compute_loss(self, batch, outputs):
        """
        latent x_tp1...x_Tp1
        next_latent x_tp2...x_Tp1
        """
        loss = 0
        loss_dict = {}
        if self.config.decode:
            dec_loss = self._compute_decode_loss(outputs['obs'], outputs['dec_obs'])
            loss += dec_loss
            loss_dict['dec'] = dec_loss.item()

        if self.config.forward_dynamics is not None:        
            if self.config.forward_dynamics == 'rssm':
                kl_loss = self._compute_kl_loss(outputs['rssm_out'])
                loss += kl_loss
                loss_dict['kl'] = kl_loss.item()
                
            if self.config.contrastive:
                contrastive_loss = self._compute_contrastive_loss(outputs['next_latent'],
                                                                  outputs['latent'][1:])
                loss += contrastive_loss
                loss_dict['x_contrast'] = contrastive_loss.item()
            elif self.config.next_latent_mse:
                next_latent_mse_loss = self._compute_next_latent_mse_loss(outputs['next_latent'],
                                                                          outputs['latent'][1:])
                loss += next_latent_mse_loss
                loss_dict['x_mse'] = next_latent_mse_loss.item()
                
        return loss, loss_dict 
    
    def _compute_decode_loss(self, targets, decoded):
        dec_loss = 0
        for modality, pred in decoded.items():
            target = targets[modality]
            modality_loss = F.mse_loss(pred, target, reduction='none')
            sum_dims = list(range(modality_loss.dim()))[2:] # Exclude time/batch dims
            modality_loss = modality_loss.sum(dim=sum_dims) # Sum over channel dims
            dec_loss += modality_loss
        dec_loss = dec_loss.mean(dim=(0, 1)) # Mean over time/batch dims
        return dec_loss
    
    def _compute_kl_loss(self, rssm_out):
        """
        Computes KL loss associated with RSSM model.
        """
        # Compute KL loss (again sum final dims and average over batch and time)
        prior_state = Normal(rssm_out.prior_means, rssm_out.prior_std_devs)
        posterior_state = Normal(rssm_out.posterior_means, rssm_out.posterior_std_devs)
        kl_div = kl_divergence(posterior_state, prior_state).sum(dim=2)
        kl_loss = torch.max(kl_div, self.free_nats).mean(dim=(0, 1))
        return kl_loss
    
    def _compute_next_latent_mse_loss(self, latent_pred, latent_actual):
        assert latent_pred.shape == latent_actual.shape
        latent_loss = F.mse_loss(latent_pred, latent_actual)
        return latent_loss
    
    def _compute_contrastive_loss(self, pred, actual):
        """
        Contrastive loss based on CFM paper. This implementation differs slightly to use only
        the predicted and pos (CFM used input current encoded instead of next encoded for
        negative distance computation, which is unnecessary). The functions here were also
        re-written to be more readable, but were confirmed to be approximately equivalent AND
        they actually run an order of magnitude faster (0.0004 seconds vs. 0.002 seconds).
        See CFM for comparison:
            https://github.com/wilson1yan/contrastive-forward-model/blob/master/cfm/train_cfm.py#L34
        """
        assert pred.shape == actual.shape
    
        contrastive_loss = 0
        # Need to sum over timesteps, note you DON'T want to just superbatch the time and batch
        # dimensions because then you're enforcing consecutive timesteps to be pushed away in
        # the representation space, and they should be more similar than random other steps
        for i in range(pred.size(0)):
            # Negative distances, which is actual versus all others in batch, computed
            # for every sample in batch (so result is size b x b)
            neg_dists = -torch.pow(torch.cdist(pred[i], actual[i]), 2)
            # Set to minus infinity entries when comparing with self - zeroed out in softmax
            neg_dists.fill_diagonal_(float('-inf'))
    
            # Positive distances, actual next versus predicted next (size b x 1)
            pos_dists = -torch.pow(torch.norm(actual[i] - pred[i], p=2, dim=1), 2).unsqueeze(1)
    
            dists = torch.cat([neg_dists, pos_dists], dim=1) # b x b+1
            dists = F.log_softmax(dists, dim=1) # b x b+1
    
            contrastive_loss += -dists[:, -1].mean() # Get last column which is the true pos sample
        return contrastive_loss

    def get_models(self):
        models = {}
            
        # Observation modality encoders
        combined_embedding_size = 0
        for modality in self.config.enc_obs_modalities:
            key = f'{modality}_obs_encoder'
            if modality in ['rgb', 'depth']:
                img_channels = 3 if modality == 'rgb' else 1
                models[key] = msp_util.get_image_encoder(img_channels, self.config)
                combined_embedding_size += self.config.img_embedding_size
            elif modality in ['joint_position', 'joint_velocity', 'joint_torque']:
                models[key] = msp_util.get_vector_encoder(self.config.n_joints, self.config)
                combined_embedding_size += self.config.vec_embedding_size
            # TODO hacking
            elif modality in ['arm_joint_position', 'arm_joint_velocity']:
                models[key] = msp_util.get_vector_encoder(7, self.config)
                combined_embedding_size += self.config.vec_embedding_size
            elif modality in ['ee_position', 'ee_velocity']:
                in_size = 3 if modality == 'ee_position' else 6
                models[key] = msp_util.get_vector_encoder(in_size, self.config)
                combined_embedding_size += self.config.vec_embedding_size
            elif modality in ['ee_orientation']:
                models[key] = msp_util.get_vector_encoder(4, self.config)
                combined_embedding_size += self.config.vec_embedding_size
            elif modality in ['obj_position', 'obj_velocity']:
                in_size = 3 if modality == 'obj_position' else 6
                for obj_name in self.config.obj_names:
                    encoder = msp_util.get_vector_encoder(in_size, self.config)
                    models[f'{obj_name}_{modality}_encoder'] = encoder
                    combined_embedding_size += self.config.vec_embedding_size
            else:
                raise ValueError(f"Unknown encode observation modality: {modality}")
    
        # Add multisensory models if there are multiple modalities
        if len(self.config.enc_obs_modalities) > 1:
            models['multisensory_encoder'] = MLP(combined_embedding_size,
                                                 self.config.multisensory_embedding_size,
                                                 self.config.multisensory_hidden_sizes,
                                                 self.config.activation,
                                                 self.config.vec_norm,
                                                 vae=self.config.vae)
            
        if self.config.forward_dynamics == 'rssm':
            dec_input_size = self.config.rssm_state_size
        elif len(self.config.enc_obs_modalities) > 1:
            dec_input_size = self.config.multisensory_embedding_size
        else:
            dec_input_size = combined_embedding_size  # When there is one input obs modality
            
        # Observation modality decoders
        for modality in self.config.dec_obs_modalities:
            key = f'{modality}_obs_decoder'
            if modality in ['rgb', 'depth']:
                img_channels = 3 if modality == 'rgb' else 1
                models[key] = msp_util.get_image_decoder(img_channels, dec_input_size, self.config)
            elif modality in ['joint_position', 'joint_velocity', 'joint_torque']:
                models[key] = msp_util.get_vector_decoder(self.config.n_joints, dec_input_size,
                                                          self.config)
            # TODO hacking
            elif modality in ['arm_joint_position', 'arm_joint_velocity']:
                models[key] = msp_util.get_vector_decoder(7, dec_input_size, self.config)
            elif modality in ['ee_position', 'ee_velocity']:
                out_size = 3 if modality == 'ee_position' else 6
                models[key] = msp_util.get_vector_decoder(out_size, dec_input_size, self.config)
            elif modality in ['ee_orientation']:
                models[key] = msp_util.get_vector_decoder(4, dec_input_size, self.config)
            elif modality in ['obj_position', 'obj_velocity']:
                out_size = 3 if modality == 'obj_position' else 6
                for obj_name in self.config.obj_names:
                    decoder = msp_util.get_vector_decoder(out_size, dec_input_size, self.config)
                    models[f'{obj_name}_{modality}_decoder'] = decoder
            else:
                raise ValueError(f"Unknown decode observation modality: {modality}")
            
        # Action modality encoders
        if self.config.encode_act:
            for modality in self.config.act_modalities:
                key = f'{modality}_act_encoder'
                if modality in ['joint_position', 'delta_joint_position']:
                    models[key] = msp_util.get_vector_encoder(self.config.n_joints, self.config)
                elif modality in ['arm_joint_position', 'delta_arm_joint_position']:
                    # TODO this is hard-coded for 7-DOF
                    models[key] = msp_util.get_vector_encoder(7, self.config)
                else:
                    raise ValueError(f"Unknown action modality: {modality}")
    
        # TODO this won't correctly account for multiple action modalities if encoding actions,
        # will need to add a multi-action encoder (or concat features of actions)
        act_size = self.config.vec_enc_sizes[-1] if self.config.encode_act else self.config.act_size
        
        # Forward dynamics model
        if self.config.forward_dynamics is not None:
            if self.config.forward_dynamics == 'rssm':
                models['forward_dynamics'] = RSSM(self.config.rssm_belief_size,
                                                  self.config.rssm_state_size,
                                                  act_size,
                                                  self.config.rssm_hidden_size,
                                                  self.config.multisensory_embedding_size,
                                                  self.config.activation,
                                                  self.config.rssm_min_std_dev)
            elif self.config.forward_dynamics == 'mlp':
                # Input size is concatenated representations for obs and act
                models['forward_dynamics'] = MLP(dec_input_size + act_size,
                                                 dec_input_size,
                                                 self.config.fd_mlp_sizes,
                                                 self.config.activation,
                                                 self.config.vec_norm)
            else:
                raise ValueError(f"Unknown forward dynamics function: {self.config.forward_dynamics}")
        
        if self.config.checkpoint:
            checkpoint = torch.load(self.config.checkpoint)
            if 'models' not in checkpoint:
                ui_util.print_error_exit("Checkpoint does not have model weights to load")
            torch_util.load_state_dicts(models, checkpoint['models'])
        
        return models

    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--train_sample_dir', type=str, required=True)
    parser.add_argument('--val_sample_dir', type=str,
                        help=("Optional separate directory containing validation samples. "
                              "If not provided, will make validation samples from training data. "
                              "It's better to use samples from completely different trajs."))
    parser.add_argument('--checkpoint_dir', type=str, required=True)
    parser.add_argument('--batch_size', type=int)
    parser.add_argument('--n_epochs', type=int)
    parser.add_argument('--checkpoint_interval', type=int)
    parser.add_argument('--params', type=str, default='{}',
                        help="Catch-all for other options to be set on GoalLearnerConfig")
    args = parser.parse_args()

    file_util.check_path_exists(args.train_sample_dir, "Train sample directory")
    metadata_filename = os.path.join(args.train_sample_dir, "metadata.yaml")
    file_util.check_path_exists(metadata_filename)
    metadata = file_util.load_yaml(metadata_filename)
    if args.val_sample_dir:
        file_util.check_path_exists(args.val_sample_dir, "Validation sample directory")
    
    config = MSPConfig()
    func_util.override_dataclass_with_args(config, args) # Take anything set on args
    config.from_dict(ast.literal_eval(args.params))

    if not config.decode and not config.contrastive:
        ui_util.print_error_exit("\n  No loss to compute\n")
    # TODO will add support for RSSM + contrastive
    if config.forward_dynamics == 'rssm' and config.contrastive:
        ui_util.print_error_exit("\n  Contrastive loss not yet supported for RSSM\n")
    if config.diagnostic and not config.checkpoint:
        ui_util.print_error_exit("\n  Must provide checkpoint to train diagnostic models\n")
    if config.decode and not config.next_latent_mse and not config.contrastive \
       and config.forward_dynamics is not None and config.forward_dynamics != 'rssm':
        ui_util.print_error_exit("\n  Must enable --next_latent_mse or --contrastive "
                                 "or use RSSM forward dynamics with decode loss\n")
    if config.next_latent_mse and config.contrastive:
        ui_util.print_error_exit("\n  MSE and contrastive losses on latent states cannot "
                                 "be active simultaneously\n")

    file_util.safe_create_dir(config.checkpoint_dir)

    if config.diagnostic:
        ui_util.print_warning("\n  Training diagnostic models\n")

    metadata = file_util.load_yaml(metadata_filename)
    config.from_dict(metadata)

    # Print config to catch any mistakes in settings
    def print_config(data, indent=''):
        if not isinstance(data, dict):
            raise ValueError("Something is wrong, was expecting dictionary as input")
        for k, v in data.items():
            if isinstance(v, dict):
                print(indent + k)
                print_config(v, indent + '    ')
            else:
                print(f"{indent + k}: {v}")
            
    print("\n========================== CONFIG ==========================")
    config_dict = deepcopy(vars(config))
    if config.forward_dynamics != 'rssm':
        for k in [key for key in config_dict.keys() if 'rssm' in key]:
            del config_dict[k]
    if config.forward_dynamics != 'mlp':
        del config_dict['fd_mlp_sizes']
    if config.forward_dynamics is None:
        del config_dict['latent_delta']
        del config_dict['next_latent_mse']
        del config_dict['contrastive']
    if ('rgb' not in config.enc_obs_modalities and 'rgb' not in config.dec_obs_modalities and
        'depth' not in config.enc_obs_modalities and 'depth' not in config.dec_obs_modalities):
        for k in [key for key in config_dict.keys() if 'img_' in key]:
            del config_dict[k]
        del config_dict['cnn_norm']
        
    print_config(config_dict, '  ')

    train_filenames = file_util.list_dir(config.train_sample_dir, '.pickle')
    val_filenames = file_util.list_dir(config.val_sample_dir, '.pickle')
    msp = MSPLearner(config, train_filenames, val_filenames)
    print(f"\nTraining for {config.n_epochs} epochs (checkpoint every {config.checkpoint_interval})...")
    msp.train()
