
from isaacgym import gymapi
from isaacgym import gymtorch
from rlgpu.utils.torch_jit_utils import *

import torch
import numpy as np
import math, os

device = 'cuda'
num_envs = 4
num_per_row = int(math.sqrt(num_envs))
spacing = 1.5
lower = gymapi.Vec3(-spacing, 0.0, -spacing)
upper = gymapi.Vec3(spacing, spacing, spacing)

start_position_noise = 0.0
start_rotation_noise = 0.0

gym = gymapi.acquire_gym()

physics_engine = gymapi.SIM_PHYSX

sim_params = gymapi.SimParams()
sim_params.dt = 1./60.
sim_params.substeps = 2
sim_params.use_gpu_pipeline = True

sim_params.physx.solver_type = 1
sim_params.physx.num_position_iterations = 8
sim_params.physx.num_velocity_iterations = 1
sim_params.physx.num_threads = 4
sim_params.physx.use_gpu = True
# sim_params.physx.num_subscenes = 0
# sim_params.physx.max_gpu_contact_pairs = 8 * 1024 * 1024
sim_params.physx.rest_offset = 0.0
sim_params.physx.contact_offset = 0.001
sim_params.physx.friction_offset_threshold = 0.001
sim_params.physx.friction_correlation_distance = 0.0005

sim_params.up_axis = gymapi.UP_AXIS_Z
sim_params.gravity.x = 0
sim_params.gravity.y = 0
sim_params.gravity.z = -9.81
sim = gym.create_sim(
  0, 0, physics_engine, sim_params
)
if sim is None:
    raise Exception("Failed to create sim")
print('sim created')

viewer = gym.create_viewer(sim, gymapi.CameraProperties())
if viewer is None:
    raise Exception("Failed to create viewer")
print('veiwer created')

plane_params = gymapi.PlaneParams()
plane_params.normal = gymapi.Vec3(0.0, 0.0, 1.0)
gym.add_ground(sim, plane_params)

asset_root = os.path.expanduser(
  "~/isaac_ws/src/isaacgym/assets"
)
franka_asset_file = "urdf/franka_description/robots/franka_panda.urdf"

block_size = [0.08, 0.08, 0.08]

# create block assets
asset_options = gymapi.AssetOptions()
asset_options.density = 2000.0
asset_options.fix_base_link = False
block1_asset = gym.create_box(
  sim, block_size[0], block_size[1], block_size[2], asset_options
)
block2_asset = gym.create_box(
  sim, block_size[0], block_size[1], block_size[2], asset_options
)
asset_options.fix_base_link = True
table_asset = gym.create_box(
  sim, 0.9, 0.9, 0.4, asset_options
)

# load franka asset
asset_options = gymapi.AssetOptions()
asset_options.flip_visual_attachments = True
asset_options.fix_base_link = True
asset_options.collapse_fixed_joints = True
asset_options.disable_gravity = True
asset_options.thickness = 0.001
asset_options.default_dof_drive_mode = gymapi.DOF_MODE_POS
asset_options.use_mesh_materials = True
franka_asset = gym.load_asset(
  sim, asset_root, franka_asset_file, asset_options
)

franka_dof_stiffness = to_torch(
  [400, 400, 400, 400, 400, 400, 400, 1.0e6, 1.0e6],
  dtype=torch.float, device=device
)
franka_dof_damping = to_torch(
  [80, 80, 80, 80, 80, 80, 80, 1.0e2, 1.0e2],
  dtype=torch.float, device=device
)

num_franka_bodies = gym.get_asset_rigid_body_count(franka_asset)
num_franka_dofs = gym.get_asset_dof_count(franka_asset)
num_block1_bodies = gym.get_asset_rigid_body_count(block1_asset)
num_block2_bodies = gym.get_asset_rigid_body_count(block2_asset)

# set franka dof properties
franka_dof_props = gym.get_asset_dof_properties(franka_asset)
franka_dof_lower_limits = []
franka_dof_upper_limits = []
for i in range(num_franka_dofs):
    franka_dof_props['driveMode'][i] = gymapi.DOF_MODE_POS
    if physics_engine == gymapi.SIM_PHYSX:
        franka_dof_props['stiffness'][i] = franka_dof_stiffness[i]
        franka_dof_props['damping'][i] = franka_dof_damping[i]
    else:
        franka_dof_props['stiffness'][i] = 7000.0
        franka_dof_props['damping'][i] = 50.0

    franka_dof_lower_limits.append(franka_dof_props['lower'][i])
    franka_dof_upper_limits.append(franka_dof_props['upper'][i])

franka_dof_lower_limits = to_torch(
  franka_dof_lower_limits, device=device )
franka_dof_upper_limits = to_torch(
  franka_dof_upper_limits, device=device )
franka_dof_speed_scales = torch.ones_like(
  franka_dof_lower_limits )
franka_dof_speed_scales[[7, 8]] = 0.1
franka_dof_props['effort'][7] = 200
franka_dof_props['effort'][8] = 200

franka_start_pose = gymapi.Transform()
franka_start_pose.p = gymapi.Vec3(0.0, 0.0, 0.4025)
franka_start_pose.r = gymapi.Quat(0.0, 0.0, 0.0, 1.0)

table_start_pose = gymapi.Transform()
table_start_pose.p = gymapi.Vec3(0.3, 0.0, 0.2)
block1_start_pose = gymapi.Transform()
block1_start_pose.p = gymapi.Vec3(0.55, -0.25, 0.54)
# block1_start_pose.p = gymapi.Vec3(0.0, 0.0, 0.4025)
block2_start_pose = gymapi.Transform()
block2_start_pose.p = gymapi.Vec3(0.55, 0.25, 0.54)

# compute aggregate size
num_franka_bodies = gym.get_asset_rigid_body_count(franka_asset)
num_franka_shapes = gym.get_asset_rigid_shape_count(franka_asset)
num_table_bodies = gym.get_asset_rigid_body_count(table_asset)
num_table_shapes = gym.get_asset_rigid_shape_count(table_asset)
num_block1_bodies = gym.get_asset_rigid_body_count(block1_asset)
num_block1_shapes = gym.get_asset_rigid_shape_count(block1_asset)
num_block2_bodies = gym.get_asset_rigid_body_count(block2_asset)
num_block2_shapes = gym.get_asset_rigid_shape_count(block2_asset)
max_agg_bodies = num_franka_bodies + num_block1_bodies + num_block2_bodies + num_table_bodies
max_agg_shapes = num_franka_shapes + num_block1_shapes + num_block2_shapes + num_table_shapes
print('assets created')

frankas = []
block1s = []
default_block1_poses = []
block2s = []
default_block2_poses = []
envs = []

for i in range(num_envs):
    # create env instance
    env_ptr = gym.create_env(
        sim, lower, upper, num_per_row
    )

    gym.begin_aggregate(
      env_ptr, max_agg_bodies, max_agg_shapes, True
    )

    # franka_actor = gym.create_actor(
    #   env_ptr, franka_asset, franka_start_pose, "franka", i, 2, 0
    # )
    # gym.set_actor_dof_properties(
    #   env_ptr, franka_actor, franka_dof_props
    # )

    table_actor = gym.create_actor(
      env_ptr, table_asset, table_start_pose, "table", i, 0, 0
    )

    block1_pose = block1_start_pose
    block1_pose.p.x += start_position_noise * (np.random.rand() - 0.5)
    dz = 0.5 * np.random.rand()
    dy = np.random.rand() - 0.5
    block1_pose.p.y += start_position_noise * dy
    block1_pose.p.z += start_position_noise * dz
    block1_actor = gym.create_actor(
      env_ptr, block1_asset, block1_pose, "block1", i, 0, 0
    )
    default_block1_poses.append( [
      block1_pose.p.x, block1_pose.p.y, block1_pose.p.z,
      block1_pose.r.x, block1_pose.r.y, block1_pose.r.z,
      block1_pose.r.w, 0, 0, 0, 0, 0, 0
    ] )

    block2_pose = block2_start_pose
    block2_pose.p.x += start_position_noise * (np.random.rand() - 0.5)
    dz = 0.5 * np.random.rand()
    dy = np.random.rand() - 0.5
    block2_pose.p.y += start_position_noise * dy
    block2_pose.p.z += start_position_noise * dz
    block2_actor = gym.create_actor(
      env_ptr, block2_asset, block2_pose, "block2", i, 0, 0
    )
    default_block2_poses.append( [
      block2_pose.p.x, block2_pose.p.y, block2_pose.p.z,
      block2_pose.r.x, block2_pose.r.y, block2_pose.r.z,
      block2_pose.r.w, 0, 0, 0, 0, 0, 0
    ] )

    franka_actor = gym.create_actor(
      env_ptr, franka_asset, franka_start_pose, "franka", i, 0, 0
    )
    gym.set_actor_dof_properties(
      env_ptr, franka_actor, franka_dof_props
    )

    gym.end_aggregate(env_ptr)

    envs.append(env_ptr)
    frankas.append(franka_actor)
    block1s.append(block1_actor)
    block2s.append(block2_actor)
default_block1_poses = to_torch(
  default_block1_poses, device=device, dtype=torch.float
).view( num_envs, 13 )
default_block2_poses = to_torch(
  default_block2_poses, device=device, dtype=torch.float
).view( num_envs, 13 )
print('actors created')

cam_pos = gymapi.Vec3(4, 3, 2)
cam_target = gymapi.Vec3(-4, -3, 0)
middle_env = envs[num_envs // 2 + num_per_row // 2]
gym.viewer_camera_look_at(viewer, middle_env, cam_pos, cam_target)

gym.prepare_sim(sim)
print('sim prepped')

# setup_variables()
#   get gym GPU state tensors
actor_root_state_tensor = gym.acquire_actor_root_state_tensor(sim)
dof_state_tensor = gym.acquire_dof_state_tensor(sim)
rigid_body_tensor = gym.acquire_rigid_body_state_tensor(sim)

gym.refresh_actor_root_state_tensor(sim)
gym.refresh_dof_state_tensor(sim)
gym.refresh_rigid_body_state_tensor(sim)

franka_default_dof_pos = to_torch([1.157, -1.066, -0.155, -2.239, -1.841, 1.003, 0.469, 0.035, 0.035], device=device)
dof_state = gymtorch.wrap_tensor(dof_state_tensor)
franka_dof_state = dof_state.view(num_envs, -1, 2)[:, :num_franka_dofs]
franka_dof_pos = franka_dof_state[..., 0]
franka_dof_vel = franka_dof_state[..., 1]

rigid_body_states = gymtorch.wrap_tensor(rigid_body_tensor).view(num_envs, -1, 13)
num_bodies = rigid_body_states.shape[1]

block1_num = num_franka_bodies + num_table_bodies
block2_num = num_franka_bodies + num_table_bodies + num_block1_bodies
block1_state = rigid_body_states[:, block1_num]
block2_state = rigid_body_states[:, block2_num]

root_state_tensor = gymtorch.wrap_tensor(actor_root_state_tensor).view(num_envs, -1, 13)
print('robot pose:', root_state_tensor[:, -1, :3])
print('block1 pose:', root_state_tensor[:, 1, :3])
print('block2 pose:', root_state_tensor[:, 2, :3])

block1_rstates = root_state_tensor[:,1]
block2_rstates = root_state_tensor[:,2]

num_dofs = gym.get_sim_dof_count(sim) // num_envs
franka_dof_targets = torch.zeros((num_envs, num_dofs), dtype=torch.float, device=device)

print('tensors created')

# reset()
env_ids = torch.arange(num_envs, device=device)
# reset franka
pos = tensor_clamp(
  franka_default_dof_pos.unsqueeze(0) + 0.25 *
  (
    torch.rand( (len(env_ids), num_franka_dofs),
    device=device ) - 0.5
  ),
  franka_dof_lower_limits, franka_dof_upper_limits
)
franka_dof_pos[env_ids, :] = pos
franka_dof_vel[env_ids, :] = torch.zeros_like(
  franka_dof_vel[env_ids] )
franka_dof_targets[env_ids, :num_franka_dofs] = pos

# reset blocks
block1_rstates[env_ids] = default_block1_poses[env_ids]
block2_rstates[env_ids] = default_block2_poses[env_ids]
gym.set_actor_root_state_tensor( sim, gymtorch.unwrap_tensor(root_state_tensor) )

gym.set_dof_position_target_tensor(
  sim, gymtorch.unwrap_tensor(franka_dof_targets)
)

gym.set_dof_state_tensor(
  sim, gymtorch.unwrap_tensor(dof_state)
)

print('sim reset')

# initial_steps()
for i in range(500):
    # step()
    gym.set_dof_position_target_tensor(
      sim, gymtorch.unwrap_tensor(franka_dof_targets)
    )

    # step the physics
    gym.simulate(sim)
    gym.fetch_results(sim, True)

    # step graphics
    gym.step_graphics(sim)
    gym.draw_viewer(viewer, sim, False)
    gym.sync_frame_time(sim)

    # refresh tensors
    gym.refresh_actor_root_state_tensor(sim)
    gym.refresh_dof_state_tensor(sim)
    gym.refresh_rigid_body_state_tensor(sim)

print('initial steps taken')

# cleanup
gym.destroy_viewer(viewer)
gym.destroy_sim(sim)
print('cleaned')
