
from rlgpu.tasks.base.vec_task import VecTaskPython
from ll4ma_util import file_util
from ll4ma_isaacgym.core import SessionConfig, RobotAction#, GeneralIiwa
from isaacRL.tasks.gymtasks.general_iiwa import GeneralIiwa
from ll4ma_isaacgym.robots import Robot
from isaacRL.tasks.her_goalies import select_goalie
from ll4ma_isaacgym.core import EnvironmentState
from std_msgs.msg import ColorRGBA

from isaacgym import gymtorch, gymapi

import os
import numpy as np
import torch

class IiwaTask(object):
    """docstring for IiwaTask."""

    def __init__(self, config, device):
        super(IiwaTask, self).__init__()
        self.config = config
        self.device = device

        sim_params = gymapi.SimParams()
        sim_params.dt = 1./60.
        sim_params.substeps = 2
        sim_params.num_client_threads = config['subscenes']

        sim_params.use_gpu_pipeline = False
        sim_params.physx.use_gpu = False
        sim_params.physx.solver_type = 1
        sim_params.physx.num_position_iterations = 8
        sim_params.physx.num_velocity_iterations = 1
        sim_params.physx.rest_offset = 0.0
        sim_params.physx.contact_offset = 0.001
        sim_params.physx.friction_offset_threshold = 0.001
        sim_params.physx.friction_correlation_distance = 0.0005
        sim_params.physx.num_subscenes = config['subscenes']
        sim_params.physx.max_gpu_contact_pairs = 8 * 1024 * 1024
        sim_params.physx.num_threads = config['num_threads']

        sim_device = device
        # if 'use_gpu' not in self.config:
        #     self.config['use_gpu'] = torch.cuda.is_available()
        # if torch.cuda.is_available() and self.config['use_gpu']:
        if device == 'cuda':
            sim_params.use_gpu_pipeline = True
            sim_params.physx.use_gpu = True

        env_configf = os.path.expanduser(config['env_config'])
        self.env_config = file_util.load_yaml(env_configf)
        self.init_wait_steps = config['init_wait_steps']
        self.env_config["env"]["asset"]["assetRoot"] = os.path.expanduser(
          "~/isaac_ws/src/isaacgym/assets"
        )
        if 'num_envs' in self.config:
            self.env_config['env']['numEnvs'] = self.config['num_envs']
        if 'max_env_steps' in self.config:
            self.env_config["env"]["episodeLength"] = self.config['max_env_steps']
        self.sim_cfg = SessionConfig(config_filename=env_configf)
        self.env_config['sim_cfg'] = self.sim_cfg
        self.env_config['robot'] = Robot(
          next( iter( self.sim_cfg.env.robots.values() ) )
        )
        self.env_config['objects'] = self.sim_cfg.env.objects
        self.env_config['goalie'] = select_goalie(config)

        self.act_shape = self.config['actions']
        self.act_range = self.config['act_range']
        self.open_thresh = self.act_range[0]/2
        self.close_thresh = self.act_range[1]/2
        self.env_config['env']['numObservations'] = self.config['num_obs']
        self.env_config['env']['numActions'] = self.act_shape

        self.task = GeneralIiwa(
            cfg=self.env_config,
            sim_params=sim_params,
            physics_engine=gymapi.SIM_PHYSX,
            device_type=sim_device,
            device_id=-1 if self.config['headless'] else 0,
            headless=self.config['headless']
        )
        self.task.apply_restrictions = False

        self.prev_actions = self.task.robot_dof_targets.clone()

        self.env_states = []
        for _ in range(self.num_envs):
            env_state = EnvironmentState()
            env_state.dt = self.task.dt
            env_state.objects = self.task.objects
            env_state.object_colors = {
              k: ColorRGBA() for k in self.task.objects.keys()
            }
            env_state.joint_names = self.task.robot.joint_names
            env_state.n_arm_joints = self.task.robot.arm.num_joints()
            env_state.n_ee_joints = self.task.robot.end_effector.num_joints()
            self.env_states.append(env_state)

    def update_env_states(self):
        retval = self.task.update_env_states()
        for env_idx in range(self.num_envs):
            self.env_states[env_idx].joint_position = self.task.state['joint_position'][env_idx,:].unsqueeze(-1)
            self.env_states[env_idx].joint_velocity = self.task.state['joint_velocity'][env_idx,:].unsqueeze(-1)
            self.env_states[env_idx].joint_torque = self.task.state['joint_torque'][env_idx,:]

            self.env_states[env_idx].timestep = self.task.progress_buf.max()

            self.env_states[env_idx].prev_action = self.task.state['prev_action'][env_idx]

            if self.robot.has_end_effector():
                # ee_idx = self.robot.end_effector.get_rb_index(env_idx)
                # self.env_states[env_idx].ee_state = self.rb_states[ee_idx, :]
                self.env_states[env_idx].ee_state = self.task.state['ee_state'][env_idx]

            # self.env_states[env_idx].jacobian = self.jacobian[env_idx,:,:,:]

            for obj_name, obj_cfg in self.task.objects.items():
                self.env_states[env_idx].object_states[obj_name] = self.task.state[obj_name][env_idx]
                if obj_cfg.set_color:
                    obj_color = self.task.env_obj_data[env_idx][obj_name]['color']
                    self.env_states[env_idx].object_colors[obj_name].r = obj_color[0]
                    self.env_states[env_idx].object_colors[obj_name].g = obj_color[1]
                    self.env_states[env_idx].object_colors[obj_name].b = obj_color[2]
                    self.env_states[env_idx].object_colors[obj_name].a = 1
        return retval

    def get_env_states(self):
        return self.latest_state

    def get_sim_states(self):
        return self.env_states

    def step(self, actions=None):
        if actions is None:
            actions = self.actions.clone()
            if not self.task.additive:
                actions[:,:-1] = self.task.robot_dof_targets[:,:(actions.shape[-1]-1)]

        self.task.step(actions)
        next_obs, rews, dones = (
          self.task.obs_buf.to(self.device),
          self.task.rew_buf.to(self.device),
          self.reset_buf.to(self.device)
        )

        nstate = {
          'dynamics': next_obs
        }
        self.latest_state = nstate
        return nstate, rews, dones

    def reset(self, envis=None):
        if envis is None:
            envis = torch.arange(self.num_envs, device=self.device)
        self.task.reset(envis)
        self.task.step(
          torch.zeros_like(self.task.actions, device=self.device)
        )
        self.latest_state = {
          'dynamics': self.task.obs_buf
        }
        return self.latest_state

    def add_actions(self, base_acts, actions):
        ret_acts = base_acts.clone()
        ret_acts[:,:-1] += actions[:,:-1]
        ret_acts[actions[:,-1] < self.open_thresh,-1] = self.task.robot.end_effector.config.discrete_range[0]
        ret_acts[actions[:,-1] > self.close_thresh,-1] = self.task.robot.end_effector.config.discrete_range[1]
        # if (actions[:,-1] > self.close_thresh).any():
        #     import pdb; pdb.set_trace()
        return ret_acts

    @property
    def robot(self):
        return self.task.robot

    @property
    def objects(self):
        return self.task.objects

    @property
    def state(self):
        return self.task.state

    @property
    def obs_space(self):
        return self.task.num_obs

    @property
    def state_space(self):
        return self.task.num_states

    @property
    def act_space(self):
        return self.task.num_actions

    @property
    def num_envs(self):
        return self.task.num_envs

    @property
    def actions(self):
        return self.task.actions

    @property
    def reset_buf(self):
        return self.task.reset_buf
