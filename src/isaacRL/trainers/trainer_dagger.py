
from isaacRL.trainers.trainer_bc import TrainerBC
from isaacRL.models.imitation_learning import DeterministicActor
from isaacRL.tasks import get_task
from isaacRL.utils.buffer import BufferFullstate
from isaacRL.experts.picknplace import PicknplaceStepExpert
from isaacRL.experts.dagger_expert import DaggerPicknplaceStepExpert

from isaacgymenvs.utils.utils import set_seed

import torch
import torch.nn as nn
import torch.nn.functional as F

import time
from tqdm import tqdm
import numpy as np
import os, sys, argparse, json
import os.path as osp
import rospy

class TrainerDAGGER(TrainerBC):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        super(TrainerDAGGER, self).__init__(config)

        self._env_probs = self.config['training'].get('expert_a_probs', 0.98)
        self._env_probs = torch.tensor(self._env_probs)
        # self._env_probs = torch.ones((self.num_envs,)) - 0.02
        self._env_updates = self.config['training'].get('a_prob_updates', True)
        self._env_prob_min = self.config['training'].get('a_prob_min', 0.1)
        self._env_prob_rate = 0.03

    def train(self):

        if osp.exists(self.pretrain_datafile):
            self.buffer.load(self.pretrain_datafile)
            print('buffer idx:', self.buffer.load_idx)
            print('buffer len:', len(self.buffer))
        else:
            self._gather_init_data()

        if self.pretrained_model is not None and osp.exists(self.pretrained_model):
            self.load_model(self.pretrained_model)

        self.runs = 0
        self.success = 0
        tot_rewards = 0.
        print('resetting')
        self.state = self.envir.reset()
        self.actor.train()
        self.expert.changing = True

        for epoch in range(self.start_epoch, self.end_epoch):
            pbar = tqdm(total=self.ckpt_interval, file=sys.stdout)
            desc = f'Train Epoch {epoch}'
            pbar.set_description(desc)
            epoch_loss = 0.
            for iter in range(self.ckpt_interval):
                if rospy.is_shutdown():
                    return

                # add to replays
                self.play(self.actions_per_interval)

                # learn from replays
                loss_avg = 0.
                for ui in range(self.updates_per_interval):
                    if rospy.is_shutdown():
                        return
                    loss = self.update_weights(epoch*self.updates_per_interval+ui)
                    loss_avg += loss
                epoch_loss += loss_avg / self.updates_per_interval

                desc = f'Train Epoch {epoch}: loss={(epoch_loss/(iter+1)):.3f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            self.losses.append(epoch_loss/self.ckpt_interval)

            iters = (epoch*self.ckpt_interval)+self.start_epoch
            timesteps = self.num_envs * iters * self.actions_per_interval
            runs = timesteps / self.max_steps
            print('saving after:')
            print('{} epochs'.format(epoch))
            print('{} timesteps'.format(timesteps))

            print('mean rewards:', self.buffer.mean_rewards())
            print('buffer indx:', self.buffer.load_idx)
            print('buffer size:', len(self.buffer))

            runs = self.envir.runs()
            success = self.envir.successes()
            print('new runs:', runs - self.runs)
            print('new successes:', success - self.success)
            if hasattr(self.envir, 'trajopt_time'):
                print('avg trajopt time:', self.envir.trajopt_time())
            self.runs = runs
            self.success = success
            if self._env_updates and (self._env_probs > self._env_prob_min).all():
                self._env_probs = torch.maximum(
                    self._env_probs - min((epoch*self._env_prob_rate)**2, self._env_prob_rate),
                    torch.tensor(self._env_prob_min)
                )
            print('env probs:', self._env_probs)
            if epoch >= self.save_best_after:
                self.save_model(epoch)

    def play(self, num_actions=1, evaluate=False, store_images=False):
        if self.state is None:
            self.state = self.envir.reset()
        if store_images:
            self.rgb_images = []
        with torch.no_grad():
            tot_rew = 0.
            if evaluate:
                pbar = tqdm(total=num_actions, file=sys.stdout)
                desc = f'Eval Iter {0}'
                pbar.set_description(desc)
            for ai in range(num_actions):
                if rospy.is_shutdown():
                    return
                actions = self.actor(self.state)
                actions = actions.detach()
                if not evaluate:
                    eactions = self.expert.get_actions(self.state)
                    # TODO: randomly select expert actions to do instead of the policy actions
                    e_idx = torch.arange(self.num_envs)[torch.rand((self.num_envs,)) < self._env_probs]
                    if len(e_idx) > 0:
                        actions[e_idx] = eactions[e_idx]
                nstate, rewards, terminals, _ = self.envir.step(actions)
                if not evaluate:
                    self.buffer.add(
                        self.state, eactions, nstate, rewards, terminals
                    )
                if store_images:
                    rgb_key = [key for key in nstate['full_state'].keys() if 'rgb' in key and 'emb' not in key]
                    if len(rgb_key) == 0:
                        rgb = self.envir.get_rgb_img()
                        rgb = [img for img in rgb.values()][0][0]
                        # self.envir.get_rgb_img()['test_camera_rgb'][0]
                        self.rgb_images.append(rgb)
                    else:
                        rgb = nstate['full_state'][rgb_key]
                        self.rgb_images.append(rgb[0].reshape(224, 224, 3)*255)
                self.state = nstate
                tot_rew += rewards.sum().item()
                if evaluate:
                    success = self.envir.successes()
                    desc = f'Eval Iter {ai}: rew={(tot_rew/((ai+1)*self.num_envs)):.3f},suc={success:d}'
                    pbar.set_description(desc)
                    pbar.update(1)
            if evaluate:
                pbar.close()
            return tot_rew
