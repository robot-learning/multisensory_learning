#!/usr/bin/env python
import rospy
import tf2_ros
from geometry_msgs.msg import PoseStamped, TransformStamped

import numpy as np

from ll4ma_util import ros_util


pose_msg = None


def pose_cb(msg):
    global pose_msg

    ref_T_dope = ros_util.pose_to_homogeneous(msg.pose)
    # I figured this out for bleach in Meshlab and rviz looking at the relative frames
    dope_T_obj = np.array([[-1,  0,  0,  0],
                           [ 0,  0, -1,  0],
                           [ 0, -1,  0,  0],
                           [ 0,  0,  0,  1]])
    ref_T_obj = ref_T_dope @ dope_T_obj
    pose_msg = ros_util.homogeneous_to_pose(ref_T_obj)


if __name__ == '__main__':
    rospy.init_node("dope_relay")

    broadcaster = tf2_ros.StaticTransformBroadcaster()
    rate = rospy.Rate(100)

    sub = rospy.Subscriber("/dope/pose_bleach", PoseStamped, pose_cb)

    tf_stmp = TransformStamped()
    tf_stmp.header.frame_id = "kinect2_rgb_optical_frame"  # TODO
    tf_stmp.child_frame_id = "bleach" # TODO

    rospy.loginfo("Publishing object pose from DOPE...")
    while not rospy.is_shutdown():
        if pose_msg is not None:
            tf_stmp.header.stamp = rospy.Time.now()
            tf_stmp.transform.translation.x = pose_msg.position.x
            tf_stmp.transform.translation.y = pose_msg.position.y
            tf_stmp.transform.translation.z = pose_msg.position.z
            tf_stmp.transform.rotation = pose_msg.orientation
            broadcaster.sendTransform(tf_stmp)
        rate.sleep()
