
# from mdn.utils.process_observations import process_data_in

import sys, os
import pickle
import numpy as np
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset

class LimitArray(object):
    """docstring for LimitArray."""

    def __init__(self, arr):
        super(LimitArray, self).__init__()
        self.arr = arr

    def push(self, item):
        self.arr.insert(0, item)
        del self.arr[-1]
        return True

    def __getitem__(self, idx):
        return self.arr[idx]


# class PickleDataset(Dataset):
#     """docstring for PickleDataset."""
#
#     def __init__( self, filep, modalities=[], device="cpu", batch_size=0 ):
#
#         super(PickleDataset, self).__init__()
#         with open(filep, 'r') as f:
#             self.file_paths = [line.strip() for line in f]
#
#         self._modalities = modalities
#         self.device = device
#         self.batch_size = batch_size
#
#         self.data_ranges = {
#           'rgb': [0, 255.0], 'depth': [-3.0, 0], 'segmentation': [0, 16.0],
#           'joint_position': [-3.051551103591919, 3.739427089691162],
#           'joint_velocity': [-0.765457034111023, 0.774268388748169],
#           'joint_torque': [-13.537010192871094, 4.9902753829956055],
#           'ee_position': [-0.7601864337921143, 1.4886488914489746],
#           'ee_orientation': [-0.992543637752533, 0.9374253749847412],
#           'ee_velocity': [-1.6010644435882568, 1.389662504196167],
#           'ee_state': [-1.6010644435882568, 1.4886488914489746],
#           'objects': [0, 0], 'action': [0, 0]
#         }
#
#         self._filename = None
#         self._data = None
#         self._attrs = None
#
#         self._preprocess()
#
#     def _preprocess(self):
#
#         print("\nPre-processing files to initialize data loader samples...")
#         if self.batch_size > 0:
#             self.sample_opts = self.file_paths
#         else:
#             self.sample_opts = []
#             for filep in tqdm(self.file_paths, file=sys.stdout):
#                 with open(filep, 'rb') as f:
#                     data, attrs = pickle.load(f)
#                     temp = [(filep, idx) for idx in range(len(data[self._modalities[0]]))]
#                     self.sample_opts += temp
#                     # for idx in range(0, len(data[self._modalities[0]])):
#                     #     self.sample_opts.append( (filep, idx) )
#
#                     # # Check min/max for data scaling
#                     # for modality in self._modalities:
#                     #     data_min = np.min(data[modality])
#                     #     data_max = np.max(data[modality])
#                     #     self.data_ranges[modality][0] = min(
#                     #       self.data_ranges[modality][0], float(data_min) )
#                     #     self.data_ranges[modality][1] = max(
#                     #       self.data_ranges[modality][1], float(data_max) )
#
#             # # Post-processing to add pads around data ranges
#             # for modality, (low, high) in self.data_ranges.items():
#             #     pad = self.data_range_buffers[modality]
#             #     self.data_ranges[modality] = [low - pad, high + pad]
#
#         print("Pre-processing complete\n")
#
#     def __len__(self):
#         return len(self.sample_opts)
#
#     def __getitem__(self, index, single=False):
#
#         if self.batch_size > 0:
#             filep = self.sample_opts[index]
#             if self._filename != filep:
#                 with open(filep, 'rb') as f:
#                     self._data, self._attrs = pickle.load(f)
#                 self._filename = filep
#             idx = np.random.choice( np.arange(len(self._data['rgb'])),
#               self.batch_size, replace=False )
#             outputs = {}
#             for m in self._modalities:
#                 if m == 'ee_state':
#                     inst = np.concatenate( [self._data[key][idx]
#                       for key in self._data if 'ee_' in key], 1 )
#                 elif m not in self._data:
#                     if m in self._data['objects']:
#                         inst = self._data['objects'][m]
#                         inst = np.concatenate( [inst[key][idx]
#                           for key in inst], 1 )
#                 else:
#                     inst = self._data[m][idx]
#                 try:
#                     outputs[m] = process_data_in( inst, m, self.data_ranges )
#                 except Exception as e:
#                     outputs[m] = torch.tensor(inst)
#             return outputs
#
#         filep, idx = self.sample_opts[index]
#         # print('num frames:', (end_idx - start_idx))
#         if self._filename != filep:
#             with open(filep, 'rb') as f:
#                 self._data, self._attrs = pickle.load(f)
#             self._filename = filep
#
#         outputs = {}
#         for m in self._modalities:
#             outputs[m] = process_data_in( self._data[m][idx:idx+1], m, self.data_ranges )
#         return outputs
#
#     def collate_fn(self, states):
#         output = {}
#         for key in states[0]:
#             output[key] = torch.cat(
#               [state[key] for state in states], 0 )
#         return output


def batch_dict(states, device):
    output = {}
    for key in states[0]:
        temp = [ state[key]
          for state in states if torch.is_tensor(state[key]) ]
        if len(temp) > 0:
            output[key] = torch.stack(temp).to(device)
    return output


def process_data(xdata, data_ranges):
    outputs = {}
    for m in xdata:
        try:
            # makes it between 0,1
            temp = torch.clamp(xdata[m], data_ranges[m][0], data_ranges[m][1])
            rang = data_ranges[m][1] - data_ranges[m][0]
            temp += data_ranges[m][0]
            temp = temp / rang
            outputs[m] = temp
            # if (outputs[m] > data_ranges[m][1]).any():
            #     print('over data range')
            #     import pdb; pdb.set_trace()
        except Exception as e:
            outputs[m] = xdata[m]
    return outputs


def normalize(arr):
    norm = (arr**2).sum()
    return arr / norm


if __name__ == '__main__':
    trainset = PickleDataset(
      'data/train_pickles.txt', ['ee_state', 'joint_torque', 'box'], batch_size=2
    )
    first = trainset[0]
    import pdb; pdb.set_trace()
