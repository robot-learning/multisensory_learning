#!/usr/bin/env python
import os
import os.path as osp
import sys
import argparse
import trimesh
from trimesh.collision import CollisionManager
import numpy as np
from tqdm import tqdm

from multisensory_learning.learning import SkillLearnerDataset, SKILLS, OBJECTS

from ll4ma_util import file_util, ui_util, math_util, ros_util


REFLEX_MESH = "package://multisensory_learning/src/multisensory_learning/assets/reflex.stl"


def random_ee_pose_traj():
    # TODO need to get these from learner
    minx = 0.2
    maxx = 0.9
    miny = -0.8
    maxy =  0.8
    minz = 0.819
    maxz = 0.955
    start_xyz = np.random.uniform([minx, miny, minz], [maxx, maxy, maxz])
    end_xyz = np.append(np.random.uniform([minx, miny], [maxx, maxy]), start_xyz[-1])

    push = end_xyz - start_xyz
    push_distance = np.linalg.norm(push)
    push_direction = push / push_distance
    w_R_ee = math_util.construct_rotation_matrix(  # This is same as push pose gen
        x=push_direction,
        y=np.array([0., 0., 1.]),
    )

    n_interpolate = max(3, int(push_distance / 0.04))
    
    w_T_ee_start = math_util.pose_to_homogeneous(start_xyz, R=w_R_ee)
    w_T_ee_end = math_util.pose_to_homogeneous(end_xyz, R=w_R_ee)
    Ts = [w_T_ee_start, w_T_ee_end]

    # Interpolate positions
    xs = np.linspace(start_xyz[0], end_xyz[0], n_interpolate)
    ys = np.linspace(start_xyz[1], end_xyz[1], n_interpolate)
    T = w_T_ee_start.copy()    
    for i in range(n_interpolate):
        T = T.copy()
        T[0,-1] = xs[i]
        T[1,-1] = ys[i]
        Ts.append(T)

    return Ts


def get_sample(w_T_obj, w_T_ee_start, w_T_ee_end):
    obj_T_w = math_util.homogeneous_inverse(w_T_obj)
    obj_T_ee_start = obj_T_w @ w_T_ee_start
    obj_T_ee_end = obj_T_w @ w_T_ee_end
    sample = {
        'ee_start_pos_in_obj' : math_util.homogeneous_to_position(obj_T_ee_start),
        'ee_start_rot_in_obj' : math_util.homogeneous_to_rotation(obj_T_ee_start).flatten(),
        'ee_end_pos_in_obj'   : math_util.homogeneous_to_position(obj_T_ee_end),
        'ee_end_rot_in_obj'   : math_util.homogeneous_to_rotation(obj_T_ee_end).flatten(),
        # No change in pos/rot since these are no-ops
        'obj_pos_delta_in_obj': np.zeros(3),
        'obj_rot_delta_in_obj': np.eye(3).flatten()
    }
    return sample


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--skill', type=str, required=True, choices=SKILLS)
    parser.add_argument('-o', '--object', type=str, required=True, choices=OBJECTS)
    parser.add_argument('-d', '--data_root', type=str, default=None)
    parser.add_argument('-n', '--n_noops', type=int, default=5)
    parser.add_argument('--regenerate', action='store_true')
    parser.add_argument('--include_perturbed', action='store_true')
    parser.add_argument('--test', action='store_true')
    args = parser.parse_args()

    if args.data_root is not None:
        data_root = args.data_root
    else:
        data_root = os.environ.get("DATA_ROOT")
        if data_root is None:
            ui_util.print_error_exit(
                "\n  Need to specify data root either as environment variable "
                "DATA_ROOT or as script input with --data_root (-d) flag\n"
            )
    if not osp.exists(data_root):
        ui_util.print_error_exit(f"\n  Data root does not exist: {data_root}\n")

    sample_dir = osp.join(data_root, f"{args.skill}_{args.object}_noop_samples")
    if args.regenerate:
        file_util.force_create_dir(sample_dir)
    else:
        file_util.safe_create_dir(sample_dir)

    data_dirs = [d for d in file_util.list_dir(data_root, exclude_files=True)
                 if f"{args.skill}_{args.object}_data" in d]
    if not args.include_perturbed:
        data_dirs = [d for d in data_dirs if 'perturbed' not in d]
    if len(data_dirs) == 0:
        ui_util.print_error_exit(f"No data found for skill {args.skill.upper()} and "
                                 f"object {args.object.upper()}")
        
    # Copy over metadata
    metadata_fn = osp.join(data_dirs[0], 'metadata.yaml')
    file_util.copy_file(
        metadata_fn,
        osp.join(sample_dir, 'metadata.yaml')
    )
    fns = [f for data_dir in data_dirs for f in file_util.list_dir(data_dir, '.pickle')]
    if args.test:
        fns = fns[:1]

    metadata = file_util.load_yaml(metadata_fn)
    urdf_fn = osp.join(
        metadata['env']['objects'][args.object]['asset_root'],
        metadata['env']['objects'][args.object]['asset_filename']
    )
    obj_mesh_fn = ros_util.get_mesh_filename_from_urdf(urdf_fn, collision=True)
    reflex_mesh_fn = ros_util.resolve_ros_package_path(REFLEX_MESH)

    collision_manager = CollisionManager()
    
    for fn in tqdm(fns, desc=f"{args.skill}-{args.object}"):
        data, attrs = file_util.load_pickle(fn)

        obj_pos = np.array(data['objects'][args.object]['position'][0])
        obj_quat = np.array(data['objects'][args.object]['orientation'][0])
        w_T_obj = math_util.pose_to_homogeneous(obj_pos, obj_quat)

        hand_mesh = trimesh.convex.convex_hull(trimesh.load(reflex_mesh_fn))
        obj_mesh = trimesh.convex.convex_hull(trimesh.load(obj_mesh_fn))

        collision_manager.add_object('hand', hand_mesh)
        collision_manager.add_object('object', obj_mesh)
        collision_manager.set_transform('object', w_T_obj)

        save_fn_prefix = osp.basename(fn).replace('.pickle', '')
        identifier = osp.basename(osp.dirname(fn)).replace(f"{args.skill}_{args.object}_data_", "")
        if identifier:
            save_fn_prefix = f"{identifier}_{save_fn_prefix}"
        
        n_collected = 0
        while n_collected < args.n_noops:
            collided = False
            Ts = random_ee_pose_traj()
            for T in Ts:
                if collision_manager.in_collision_single(hand_mesh, T):
                    collided = True
                    if args.test:
                        print("COLLIDED")
                        obj_mesh.apply_transform(w_T_obj)
                        hand_mesh.apply_transform(T)
                        trimesh.util.concatenate([obj_mesh, hand_mesh]).show()
                        hand_mesh = trimesh.convex.convex_hull(trimesh.load(reflex_mesh_fn))
                        obj_mesh = trimesh.convex.convex_hull(trimesh.load(obj_mesh_fn))
                    break
            if not collided:
                sample = get_sample(w_T_obj, Ts[0], Ts[-1])
                save_fn = f"{save_fn_prefix}_{n_collected+1:03d}.pickle"
                file_util.save_pickle(sample, osp.join(sample_dir, save_fn))
                n_collected += 1
                if args.test:
                    print("NO COLLIDE")
                    obj = trimesh.convex.convex_hull(trimesh.load(obj_mesh_fn))
                    obj.apply_transform(w_T_obj)
                    meshes = [obj]
                    for T in Ts:
                        hand = trimesh.convex.convex_hull(trimesh.load(reflex_mesh_fn))
                        hand.apply_transform(T)
                        meshes.append(hand)
                    trimesh.util.concatenate(meshes).show()
                    
            
