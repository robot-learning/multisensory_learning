
# from isaacRL.experts.trajopt import TrajectoryOptimizer
from ll4ma_isaacgym.behaviors.planners.trajopt import TrajectoryOptimizer

from ll4ma_util import math_util
import moveit_interface.util as moveit_util

import torch

class PickExpert:
    """docstring for PickExpert."""

    def __init__(self, action_size, target_object, same_starts=True, grasp_offset=None, target_prior=None, planner='trajopt', action_repeats=2, max_iterations=100):
        self.target_object = target_object
        self.same_starts = same_starts
        self.planner = planner
        self.action_repeats = action_repeats
        # self._downward_ee = torch.tensor([-0.080, -0.629,  0.084, -0.769]) # torch.tensor([-0.764, -0.013, 0.645, -0.034]) # torch.tensor([0.60909081,  0.36338773, -0.60599518,  0.36017716])
        x_axis, y_axis, z_axis = torch.eye(3)
        rot = torch.stack([-z_axis, -y_axis, torch.cross(-z_axis, -y_axis)])
        self._downward_ee = math_util.rotation_to_quat(rot)
        if grasp_offset is None:
            grasp_offset = torch.cat(
                [torch.tensor([0., 0., 0.1]), self._downward_ee]
            )
        self.grasp_offset = grasp_offset.unsqueeze(0)
        self.lift_offset = torch.tensor([0., 0., 0.2]).unsqueeze(0)
        if target_prior is None:
            target_prior = torch.cat(
                [torch.tensor([0., 0., 0.15]), self._downward_ee]
            )
        self.target_prior = target_prior.unsqueeze(0)

        self.action_size = action_size
        self.num_joints = 7

        if self.planner != 'trajopt':
            print('planner:', self.planner)
            import pdb; pdb.set_trace()
        self.timesteps = 10
        self.total_steps = 120
        self.interp_steps = self.total_steps // self.timesteps
        self.max_iterations = max_iterations
        self.trajopt = TrajectoryOptimizer(timesteps=self.timesteps,FP_PRECISION=1e-8,max_iterations=self.max_iterations)
        self.trajopt.init_noise = 0.03
        self.temp_verbose = False
        # self.trajopt.verbose = True
        # self.error_type = 'position'
        self.error_type = 'pose'
        self.trajopt.error_type = self.error_type
        self.ee_link = 'reflex_palm_link'

        self.grasp_steps = 80
        self.lift_steps = 80 # int(self.total_steps * 3 / 4) + 1

        self.setup_steps = 1
        self.action_steps = self.total_steps + self.lift_steps + 0
        self.setdown_steps = 1
        self.reset_setup()
        self.grasp_value = 1.
        self.ungrasp_value = -1.
        self.grasp_neutral = 0.

    def reset_setup(self):
        pass

    def get_setup_actions(self, state, envis=None):
        state_idx = state['state_idx']
        state = state['fullstate']
        num_envs = state.shape[0]
        if envis is None:
            envis = torch.arange(num_envs)
        start_joints = state[:,state_idx['joint_position']][:,:self.num_joints]

        setup = torch.cat([start_joints.clone(), torch.zeros_like(start_joints[:,0:1])], dim=1)

        return setup[envis].unsqueeze(0)

    def get_actions(self, state, target_pose=None, envis=None):
        state_idx = state['state_idx']
        state = state['fullstate']
        num_envs = state.shape[0]
        if envis is None:
            envis = torch.arange(num_envs)
        start_joints = state[:,state_idx['joint_position']][:,:self.num_joints]
        start_ee = state[:,state_idx['ee_state']][:,:self.num_joints]
        object_state = state[:,state_idx[self.target_object]]

        # move 2 grasping pose
        if target_pose is None:
            if self.grasp_offset.device != object_state.device:
                self.grasp_offset = self.grasp_offset.to(object_state.device)
            target_pose = object_state.clone()
            target_pose[:,0:3] += self.grasp_offset[:,0:3]
            target_pose[:,3:7] = self.grasp_offset[:,3:7]

        move2grasp, _ = self._get_actions(
            start_joints,
            target_pose,
            state,
            num_envs,
            envis
        )

        return move2grasp[:,envis]

    def get_setdown_actions(self, state, envis=None):
        state_idx = state['state_idx']
        state = state['fullstate']
        num_envs = state.shape[0]
        if envis is None:
            envis = torch.arange(num_envs)
        start_joints = state[:,state_idx['joint_position']][:,:self.num_joints]
        start_ee = state[:,state_idx['ee_state']][:,:self.num_joints]
        object_state = state[:,state_idx[self.target_object]]

        # grasp object
        grasp = torch.zeros(
            (self.grasp_steps, num_envs, self.action_size),
            device=state.device, dtype=start_joints.dtype
        )
        grasp[:,:,:self.num_joints] = start_joints[:,:self.num_joints].unsqueeze(0)
        grasp[:,:,-1] = self.grasp_value

        # lift object
        target_pose = start_ee.clone()
        if self.lift_offset.device != object_state.device:
            self.lift_offset = self.lift_offset.to(state.device)
        target_pose[:,0:3] += self.lift_offset[:,0:3]

        lift, _lift_info = self._get_actions(
            start_joints,
            target_pose,
            state,
            num_envs,
            envis,
        )
        lift[:,:,-1] = self.grasp_value

        return torch.cat((grasp, lift), dim=0)[:,envis]

    def _get_actions(self,
        start_joints,
        target_pose,
        obj_state,
        num_envs,
        envis,
        identical=False,
        repeats=1,
        interp=None
    ):
        if interp is None:
            interp = self.interp_steps
        actions = torch.zeros(
            (interp*self.timesteps, num_envs, self.action_size),
            device=start_joints.device, dtype=start_joints.dtype
        )
        actions[:,:,-1] = self.grasp_neutral
        infos = []
        err_types = ['pose' for _ in range(len(target_pose))]
        if self.error_type == 'position':
            ik_resp, success = moveit_util.get_ik(
                target_pose.cpu().numpy(), self.ee_link, 5
            )
            sols = [
                torch.tensor(sol.position, dtype=target_pose.dtype, device=target_pose.device)
                    if ik_resp.solution_found[si] else target_pose[si]
                        for si, sol in enumerate(ik_resp.solutions)
            ]
            err_types = [
                'position'
                    if ik_resp.solution_found[si] else 'pose'
                        for si, sol in enumerate(ik_resp.solutions)
            ]
            target_pose = torch.stack(sols)
        for ei in envis:
            if not identical or ei == 0:
                trajectory, info = self.trajopt.get_trajectory(
                    start_joints[ei].cpu(), target_pose[ei].cpu(),
                    obj_state, interp=interp,
                    repeats=repeats,
                    goal_ee_type=err_types[ei]
                )
                infos.append(info)
            actions[:,ei,:self.num_joints] = trajectory[:,:].to(actions.device)
        if identical:
            return actions, info
        return actions, infos

    def reset_idx(self, envis):
        pass

def l2_dist(vec1, vec2):
    return ((vec1 - vec2)**2).sum(dim=1)

class PickStepExpert(PickExpert):
    """docstring for PickStepExpert."""

    def __init__(self, action_size, target_object, same_starts=True, grasp_offset=None, target_prior=None, planner='trajopt', num_envs=1, action_repeats=1, max_iterations=50):
        super(PickStepExpert, self).__init__(action_size, target_object, same_starts, grasp_offset, target_prior, planner, action_repeats, max_iterations)

        self.num_envs = num_envs
        self.action_idx = torch.zeros((self.num_envs,), dtype=torch.long)
        self.max_length = torch.tensor([self.action_steps-1]*self.num_envs, dtype=torch.long)
        self.actions = torch.zeros((self.num_envs,self.action_steps,self.action_size))
        self.env_idx = torch.arange(self.num_envs)

    def get_actions(self, take_step=True):
        actions = self.actions[self.env_idx,self.action_idx]
        if take_step:
            self.action_idx = torch.minimum(self.action_idx+1, self.max_length)
        return actions

    def reset_idx(self, envis, state, target_pose=None):
        actions = super().get_actions(state, target_pose, envis)
        if actions.device != self.actions.device:
            self.actions = self.actions.to(actions.device)
        self.actions[envis] = actions.transpose(0,1)
        self.action_idx[envis] *= 0
