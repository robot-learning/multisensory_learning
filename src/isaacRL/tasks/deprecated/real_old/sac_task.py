
from isaacgym import gymtorch
from isaacgym.torch_utils import tensor_clamp

from isaacRL.tasks.rl_task import RLTask
from isaacRL.tasks.goalies import  (
  ObjectMove,
  StackPlace,
  StackPlaceShaped
)
from isaacRL.utils.data_utils import batch_dict

from collections import defaultdict
import numpy as np
import torch


class SACTask(RLTask):
    """docstring for SACTask."""

    def __init__(self, config, device):
        super(SACTask, self).__init__(config, device)

        self.goalies = None
        self.reset()

        if config['goal']['type'] == 'ObjectMove':
            if 'maxdist' not in config['goal']:
                config['goal']['maxdist'] = 1.0
            self.goalies = [ ObjectMove( self.state[envi],
              config['goal']['object'],
              offset=config['goal']['offset'],
              threshold=config['goal']['threshold'],
              maxdist=config['goal']['maxdist'],
              sparse=self.sparse_rewards )
                for envi in range(self.num_envs) ]
        elif config['goal']['type'] == 'StackPlace':
            if 'maxdist' not in config['goal']:
                config['goal']['maxdist'] = 1.0
            self.goalies = [ StackPlace( self.state[envi],
              config['goal']['object'],
              config['goal']['base_obj'],
              offset=config['goal']['offset'],
              threshold=config['goal']['threshold'],
              maxdist=config['goal']['maxdist'],
              sparse=self.sparse_rewards )
                for envi in range(self.num_envs) ]
        elif config['goal']['type'] == 'StackPlaceShaped':
            if 'maxdist' not in config['goal']:
                config['goal']['maxdist'] = 1.0
            if 'part_ws' not in config['goal']:
                config['goal']['part_ws'] = [0.7, 1.0]
            self.goalies = [ StackPlaceShaped( self.state[envi],
              config['goal']['object'],
              config['goal']['base_obj'],
              offset=config['goal']['offset'],
              threshold=config['goal']['threshold'],
              maxdist=config['goal']['maxdist'],
              part_ws=config['goal']['part_ws'] )
                for envi in range(self.num_envs) ]
        else:
            print('unkown goal')

    def reset(self):
        self.reset_indices( torch.ones((self.num_envs,)) )

    def reset_indices(self, terminals):
        super().reset_indices(terminals)
        if self.goalies is not None:
            reset_idx = torch.arange(self.num_envs)[terminals>0]
            for envi in reset_idx:
                self.goalies[envi].update_orig_state(self.state[envi])
        return self.torch_state

    def update_rewards(self):
        for envi in range(self.num_envs):
            reward, thrsh = self.goalies[envi].reward(self.state[envi])
            self.rewards[envi] = reward
            self.terminals[envi] = int(thrsh)
        return self.rewards, self.terminals
