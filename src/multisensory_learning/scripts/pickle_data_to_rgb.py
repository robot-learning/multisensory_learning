#!/usr/bin/env python
import os
import sys
import pickle
import argparse
import cv2
from tqdm import tqdm

from multisensory_learning.util import file_util


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--pickle', type=str, required=True)
    parser.add_argument('--rgb_root', type=str, required=True)
    args = parser.parse_args()

    file_util.check_path_exists(args.pickle, "Pickle file")
    base_name = os.path.basename(args.pickle).replace('.pickle', '')
    data_dir = os.path.join(args.rgb_root, base_name)
    file_util.safe_create_dir(data_dir)
    
    with open(args.pickle, 'rb') as f:
        data, attrs = pickle.load(f)

    rgb = data['rgb']
    for i in tqdm(range(len(rgb))):
        filename = os.path.join(data_dir, f"rgb_{i+1:04d}.png")
        cv2.imwrite(filename, cv2.cvtColor(rgb[i], cv2.COLOR_RGB2BGR))
