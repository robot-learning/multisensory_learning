
from isaacgym import gymtorch, gymapi
from ll4ma_util import file_util

import os, sys, argparse, json, yaml
import torch
import numpy as np
from tqdm import tqdm

from isaacRL.utils.grapher import TrainGrapher

def setup(pre_config={}, eval=False, parser=None):
    if parser is None:
        parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str,
        default="configs/autenc.json", help="path to config file")
    parser.add_argument("-v", "--verbose", default=False,
        help="if print all info")
    parser.add_argument("--headless", default=None,
        help="overwrite running headless or not")
    parser.add_argument("--checkpt", type=str, default=None,
        help="if continuing training from checkpoint model")
    parser.add_argument("--no_graph", action='store_true',# default=False,
        help="if want to graph as training")
    parser.add_argument("--use_ros", action='store_true',
        help="if using ros")
    opt = parser.parse_args()
    # print(opt)

    config = file_util.load_yaml(opt.config)
    for key in pre_config:
        if isinstance(pre_config[key], dict):
            for key2 in pre_config[key]:
                config[key][key2] = pre_config[key][key2]
        else:
            config[key] = pre_config[key]
    savname = config['training']['checkpoint_path'].strip('/').split('/')[-1]
    config['environ']['name'] = savname
    if opt.headless is not None:
        config['environ']['headless'] = opt.headless.lower() == 'true'
    if eval and not config['environ']['headless']:
        config['environ']['num_envs'] = 4

    if config['model']['model_type'] == 'a2c':
        from isaacRL.trainers.trainer_rlg import TrainerRLG
        trainer = TrainerRLG(config)
    elif config['model']['model_type'] == 'ppo':
        from isaacRL.trainers.trainer_ppo import TrainerPPO
        trainer = TrainerPPO(config)
    elif config['model']['model_type'] == 'ppop':
        from isaacRL.trainers.trainer_ppop import TrainerPPOP
        trainer = TrainerPPOP(config)
    elif config['model']['model_type'] == 'rpl_ppo':
        from isaacRL.trainers.trainer_rpl_ppo import TrainerRPLPPO
        trainer = TrainerRPLPPO(config)
    elif config['model']['model_type'] == 'bc':
        from isaacRL.trainers.trainer_bc import TrainerBC
        trainer = TrainerBC(config)
    elif config['model']['model_type'] == 'dagger':
        from isaacRL.trainers.trainer_dagger import TrainerDAGGER
        trainer = TrainerDAGGER(config)
    elif config['model']['model_type'] == 'loki':
        from isaacRL.trainers.trainer_loki import TrainerLOKI
        trainer = TrainerLOKI(config)
    elif config['model']['model_type'] == 'sqil':
        from isaacRL.trainers.trainer_sqil import TrainerSQIL
        trainer = TrainerSQIL(config)
    elif config['model']['model_type'] == 'sqilher':
        from isaacRL.trainers.trainer_sqilher import TrainerSQILHER
        trainer = TrainerSQILHER(config)
    elif config['model']['model_type'] == 'ddpg':
        from isaacRL.trainers.trainer_ddpg import TrainerDDPG
        trainer = TrainerDDPG(config)
    elif config['model']['model_type'] == 'td3':
        from isaacRL.trainers.trainer_td3 import TrainerTD3
        trainer = TrainerTD3(config)
    elif config['model']['model_type'] == 'sac':
        from isaacRL.trainers.trainer_sacbase import TrainerSACBase
        trainer = TrainerSACBase(config)
    elif config['model']['model_type'] == 'sacbandit':
        from isaacRL.trainers.trainer_sacbandit import TrainerSACBandit
        trainer = TrainerSACBandit(config)
    elif config['model']['model_type'] == 'perceiver_img':
        from isaacRL.trainers.trainer_perceiver_img import TrainerPerceiverImg
        trainer = TrainerPerceiverImg(config)
    elif config['model']['model_type'] == 'statepred':
        from isaacRL.trainers.trainer_statepred import TrainerStatePred
        trainer = TrainerStatePred(config)
    else:
        print('unkown model type', config['model']['model_type'])
        return

    if config['model'].get('dynamics', None) is not None:
        from isaacRL.trainers.trainer_dynaq import trainer_dynaq
        trainer = trainer_dynaq(trainer)

    return opt, trainer

def main():
    opt, trainer = setup()
    torch.set_printoptions(precision=3,sci_mode=False)
    if 'expert' in trainer.config['environ'] and trainer.config['environ']['expert']['type'].lower() == 'moveit':
        opt.use_ros = True
    if opt.use_ros:
        import rospy
        print('using ros')
        rospy.init_node('train_isaacgym')

    if opt.checkpt is not None:
        trainer.load_model(opt.checkpt)

    if not opt.no_graph:
        # savname = trainer.config['training']['checkpoint_path'].strip('/').split('/')[-1]
        try:
            trainer.add_grapher(TrainGrapher(save_inter=50,savname=trainer.config['environ']['name']))
        except Exception as e:
            pass


    trainer.train()

    if opt.use_ros:
        rospy.is_shutdown()


if __name__ == '__main__':
    main()
