import time
import numpy as np
import torch
from torch import nn


def contrastive_loss(latent_next, latent_pos, batch_size):
    neg_dot_products = torch.mm(latent_next, latent_pos.t()) # b x b
    neg_dists = -((latent_next ** 2).sum(1).unsqueeze(1) -
                  2 * neg_dot_products + (latent_pos ** 2).sum(1).unsqueeze(0))
    idxs = np.arange(batch_size)
    # Set to minus infinity entries when comparing z with z - will be zero when apply softmax
    neg_dists[idxs, idxs] = float('-inf') # b x b+1
    
    pos_dot_products = (latent_pos * latent_next).sum(dim=1) # b
    pos_dists = -((latent_pos ** 2).sum(1) - 2* pos_dot_products + (latent_next ** 2).sum(1))
    pos_dists = pos_dists.unsqueeze(1) # b x 1
    
    dists = torch.cat((neg_dists, pos_dists), dim=1) # b x b+1
    dists = nn.functional.log_softmax(dists, dim=1) # b x b+1
    loss = -dists[:, -1].mean() # Get last column with is the true pos sample
    return loss



def mod_contrastive_loss(latent_next, latent_pos, batch_size):
    neg_dists = -torch.pow(torch.cdist(latent_next, latent_pos), 2)  # bxb
    # Set to minus infinity entries when comparing z with z - will be zero when apply softmax
    neg_dists.fill_diagonal_(float('-inf'))
        
    pos_dists = -torch.pow(torch.norm(latent_pos - latent_next, p=2, dim=1), 2).unsqueeze(1) # bx1
    
    dists = torch.cat((neg_dists, pos_dists), dim=1) # bxb+1
    dists = nn.functional.log_softmax(dists, dim=1) # bxb+1

    loss = -dists[:, -1].mean() # Get last column which is the true pos sample
    return loss


batch_size = 128
latent = torch.rand(batch_size, 32)
latent_next = torch.rand(batch_size, 32)
latent_pos = torch.rand(batch_size, 32)

start = time.time()
cl = contrastive_loss(latent_next, latent_pos, batch_size)
end = time.time()
print("ORIG:", end - start)

start = time.time()
mod_cl = mod_contrastive_loss(latent_next, latent_pos, batch_size)
end = time.time()
print("MOD:", end - start)

assert torch.allclose(cl, mod_cl)
print("SUCCESS")
