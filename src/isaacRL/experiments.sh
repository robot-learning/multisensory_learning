#!/bin/bash
python train.py --config configs/sac/franka16.yaml
python train.py --config configs/sac/franka17.yaml
python train.py --config configs/sac/franka18.yaml
python train.py --config configs/sac/franka19.yaml
python train.py --config configs/place_noisy/bandit_01n_00.yaml
python train.py --config configs/place_noisy/joint_01n_rpl00.yaml

python full_eval.py --config configs/sac/franka16.yaml --use_reward
python full_eval.py --config configs/sac/franka17.yaml --use_reward
python full_eval.py --config configs/sac/franka18.yaml --use_reward
python full_eval.py --config configs/sac/franka19.yaml --use_reward
python full_eval.py --config configs/place_noisy/bandit_01n_00.yaml
python full_eval.py --config configs/place_noisy/joint_01n_rpl00.yaml
