#!/usr/bin/env python
import os
import sys
import rospy
import numpy as np
from tqdm import tqdm

# Trying this library for SE(3) GMM
import riepybdlib.manifold as rm
import riepybdlib.statistics as rs
from riepybdlib.angular_representations import Quaternion
from riepybdlib.statistics import RegularizationType

from geometry_msgs.msg import PoseStamped
from visualization_msgs.msg import MarkerArray

from ll4ma_util import file_util, ros_util, ui_util
from multisensory_learning.models import MDN
from multisensory_learning.models.loss import mdn_loss

import torch
from torch.utils.data import Dataset, DataLoader
from torch.distributions import MultivariateNormal
from torch.optim import Adam


# class PushDataset(Dataset):

#     def __init__(self, data):
#         self.set_samples(data)

#     def __len__(self):
#         return len(self.samples)

#     def __getitem__(self, idx):
#         return self.samples[idx]

#     def set_samples(self, data):
#         self.samples = []
#         for i in range(len(next(iter(data.values())))):
#             # inputs = np.concatenate([data['start_obj_pos'][i], data['start_obj_quat'][i]])
#             push_act = 1 if data['push_height'][i] > 0.15 else 0
#             # inputs = np.append(inputs, push_act)
#             inputs = np.array(push_act)
#             target = np.concatenate([data['end_obj_pos'][i], data['end_obj_quat'][i]])
#             sample = (inputs, target)
#             self.samples.append(sample)

            
if __name__ == '__main__':
    """
    Visualizes the distribution of object poses after dropping from different heights. 
    Note this is a hacked script and not meant to be generally useful to anyone.
    """
    rospy.init_node('visualizer_push_distribution')

    data_dir = rospy.get_param("~data_dir")
    clear_cache = rospy.get_param("~clear_cache", False)
    low_only = rospy.get_param("~low_only", False)
    mid_only = rospy.get_param("~mid_only", False)
    high_only = rospy.get_param("~high_only", False)

    file_util.check_path_exists(data_dir, "Data directory")
    
    metadata_filename = os.path.join(data_dir, "metadata.yaml")
    file_util.check_path_exists(metadata_filename, "Data collection metadata file")
    metadata = file_util.load_yaml(metadata_filename)
    
    mesh_resource = "package://ll4ma_isaacgym/src/ll4ma_isaacgym/assets/cleaner.stl"

    cache_filename = os.path.join(data_dir, '.push_visualization_data.pickle')

    if clear_cache:
        file_util.safe_remove_path(cache_filename)
    
    if os.path.exists(cache_filename) and not clear_cache:
        rospy.logwarn("Using cached data")
        push_data = file_util.load_pickle(cache_filename)
    else:
        push_data = {
            'end_obj_pos': [],
            'end_obj_quat': [],
            'drop_height': []
        }

        for filename in tqdm(file_util.list_dir(data_dir, '.pickle')):
            data, attrs = file_util.load_pickle(filename)
            push_data['end_obj_pos'].append(data['objects']['cleaner']['position'][-1])
            push_data['end_obj_quat'].append(data['objects']['cleaner']['orientation'][-1])
            obj_height = data['objects']['cleaner']['position'][0,2]
            ee_height = data['ee_position'][-1,2]
            push_data['drop_height'].append(ee_height - obj_height)
            
        file_util.save_pickle(push_data, cache_filename)


    actual = MarkerArray()
    learned = MarkerArray()
    env = MarkerArray()
    
    marker_idx = 0
    for i in range(len(next(iter(push_data.values())))):
        print("HEIGHT", push_data['drop_height'][i])
        if push_data['drop_height'][i] < 0.25:
            if mid_only or high_only:
                continue
            color = 'deepskyblue'
        elif push_data['drop_height'][i] < 0.45:
            if low_only or high_only:
                continue
            color = 'orchid'
        else:
            if low_only or mid_only:
                continue
            color = 'chartreuse'
        actual.markers.append(ros_util.get_marker_msg(push_data['end_obj_pos'][i],
                                                      push_data['end_obj_quat'][i],
                                                      shape='mesh', mesh_resource=mesh_resource,
                                                      marker_id=marker_idx, color=color, alpha=0.8))
        marker_idx += 1


    table_config = metadata['env']['objects']['table']
    env.markers.append(ros_util.get_marker_msg(table_config['position'], table_config['orientation'],
                                               shape='cube', scale=table_config['extents'],
                                               marker_id=marker_idx, color=table_config['rgb_color']))
    marker_idx += 1

        
    actual_marker_pub = rospy.Publisher('/actual_obj_poses', MarkerArray, queue_size=1)
    learned_marker_pub = rospy.Publisher('/learned_obj_poses', MarkerArray, queue_size=1)
    env_marker_pub = rospy.Publisher('/environment', MarkerArray, queue_size=1)
    
    # if train_mdn:
    #     n_epochs = 500
    #     batch_size = 32
    #     n_components = 2
        
    #     dataset = PushDataset(push_data)
    #     loader = DataLoader(dataset, batch_size, shuffle=True, num_workers=0)
        
    #     # Input dim=8 for pos/quat and push height, output dim=7 for pos/quat
    #     mdn = MDN(1, 7, [256, 256], n_components, 'relu', 'layer').double()
    #     mdn.train()
    #     optim = Adam(mdn.parameters(), lr=1e-5)

    #     for epoch in range(1, n_epochs + 1):
    #         losses = []
    #         pbar = tqdm(total=len(dataset), file=sys.stdout)
    #         for inputs, target in loader:
    #             inputs = inputs.view(-1, 1)
    #             mdn_out = mdn(inputs.double())
    #             loss = mdn_loss(target, *mdn_out)
                            
    #             optim.zero_grad()
    #             loss.backward()
    #             optim.step()
    #             losses.append(loss.item())
            
    #             pbar.set_description(f"  E{epoch}-loss={np.mean(losses):.4f}")
    #             pbar.update(batch_size)
    #         pbar.close()

    #     checkpoint = {'mdn': mdn.state_dict()}
    #     checkpoint_fn = os.path.join(data_dir, f"mdn.pt")
    #     torch.save(checkpoint, checkpoint_fn)

    #     # Generate model visualizations
    #     mdn.eval()

    #     idx = 0
    #     component = 0
    #     n_samples = 20
    #     push_act = 0

    #     with torch.no_grad():
    #         # inputs = np.concatenate([push_data['start_obj_pos'][idx], push_data['start_obj_quat'][idx]])
    #         # inputs = np.append(inputs, push_act)
    #         inputs = np.array(push_act)
    #         inputs = torch.tensor(inputs).double()
    #         inputs = inputs.view(-1, 1)
    #         alpha, mu, sigma_tril = mdn(inputs)
    #         print("ALPHA", alpha)

    #     colors = ['coral', 'purple']
    #     for component in range(n_components):
    #         mvn = MultivariateNormal(mu[:,:,component], scale_tril=sigma_tril[:,:,:,component])
    #         samples = mvn.sample((n_samples,)).squeeze(1).numpy()

    #         pose = mu[:,:,component].numpy().squeeze()
    #         pos = pose[:3]
    #         quat = pose[3:] / np.linalg.norm(pose[3:])
    #         learned.markers.append(
    #             ros_util.get_marker_msg(pos, orientation=quat, shape='mesh',
    #                                     mesh_resource=mesh_resource, marker_id=marker_idx,
    #                                     color=colors[component], alpha=1.0)
    #         )
    #         marker_idx += 1
        
    #         for sample in samples:
    #             pos = sample[:3]
    #             quat = sample[3:] / np.linalg.norm(sample[3:])
    #             learned.markers.append(
    #                 ros_util.get_marker_msg(pos, orientation=quat, shape='mesh',
    #                                         mesh_resource=mesh_resource, marker_id=marker_idx,
    #                                         color=colors[component], alpha=0.2)
    #             )
    #             marker_idx += 1
    # elif use_gmm:
    #     m_3d = rm.get_euclidean_manifold(3, 'pos')
    #     m_quat = rm.get_quaternion_manifold('quat')
    #     m_pose = m_3d * m_quat
        
    #     upright_data = []
    #     fallen_data = []
    #     for i in range(len(next(iter(push_data.values())))):
    #         pos = push_data['end_obj_pos'][i]
    #         quat = push_data['end_obj_quat'][i]
    #         quat /= np.linalg.norm(quat)
    #         quat = Quaternion(quat[3], quat[:3])
    #         pose = (pos, quat)
    #         if pos[-1] < 0.41:
    #             upright_data.append(pose)
    #         else:
    #             fallen_data.append(pose)
    #     all_data = upright_data + fallen_data

    #     # Try fitting Gaussians for each to get better initialization for GMM fitting
    #     mu = m_pose.id_elem
    #     sigma = np.eye(m_pose.n_dimT)
    #     gauss_upright = rs.Gaussian(m_pose, mu, sigma)
    #     gauss_upright.mle(upright_data)
    #     gauss_fallen = rs.Gaussian(m_pose, mu, sigma)
    #     gauss_fallen.mle(fallen_data)

    #     gmm = rs.GMM(m_pose, 2)
    #     gmm.gaussians = [gauss_upright, gauss_fallen]
        
    #     conv_thresh = 1e-7
    #     reg_lambda = 1e-3
    #     reg_type = RegularizationType.SHRINKAGE
    #     # reg_type = RegularizationType.DIAGONAL
    #     minsteps = 5
            
    #     # gmm.kmeans(m_data, reg_lambda=reg_lambda, reg_type=reg_type)
    #     lik, avglik = gmm.fit(all_data, convthres=conv_thresh, minsteps=minsteps,
    #                           reg_lambda=reg_lambda, reg_type=reg_type)

    #     colors = ['green', 'purple', 'red', 'blue']
    #     print("WEIGHTS", gmm.priors)
    #     for _ in range(30):
    #         for i, gauss in enumerate(gmm.gaussians):
    #             pos, quat = gauss.sample()
    #             quat = quat.to_nparray()
    #             quat = np.append(quat[1:], quat[0]) # They give (w, x, y, z), want (x, y, z, w)
    #             learned.markers.append(
    #                 ros_util.get_marker_msg(pos, orientation=quat, shape='mesh',
    #                                         mesh_resource=mesh_resource, marker_id=marker_idx,
    #                                         color=colors[i], alpha=0.4)
    #             )
    #             marker_idx += 1
        
    rospy.loginfo("Publishing marker msgs...")
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        actual_marker_pub.publish(actual)
        env_marker_pub.publish(env)
        if learned.markers:
            learned_marker_pub.publish(learned)
        rate.sleep()
