
from isaacRL.trainers.trainer import Trainer
from isaacRL.models.actorcritics import ActorBasic, CriticBasic
from isaacRL.tasks import get_task
from isaacRL.utils.buffer import BufferBasic, BufferHER

from isaacgymenvs.utils.utils import set_seed

import torch
import torch.nn as nn
import torch.nn.functional as F

from tqdm import tqdm
import numpy as np
import os, sys, argparse, json
import os.path as osp
import rospy

class TrainerPerceiverImg(Trainer):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        device = "cuda" if torch.cuda.is_available() else "cpu"
        super(TrainerSACBase, self).__init__(config, device)

        self.dataset = TrajectoryDataset('a_rgb_data', data_keys=['rgb'], label_keys=['ee_state'], train=True)
        # self.dataloader = DataLoader(dataset)
        self.validloader = None

        self.model = PerceiverIO(
            *,
            depth,
            dim,
            queries_dim,
            logits_dim = None,
            num_latents = 512,
            latent_dim = 512,
            cross_heads = 1,
            latent_heads = 8,
            cross_dim_head = 64,
            latent_dim_head = 64,
            weight_tie_layers = False,
            decoder_ff = False
        )

        self.optim = torch.optim.Adam(self.model.params(), lr=self.lr)

    def load_model(self):
        raise NotImplementedError

    def save_model(self):
        raise NotImplementedError

    def train(self):
        trainset = self.dataset
        for epoch in range(self.start_epoch, self.end_epoch):

            print('epoch:', epoch)

            lossHistory = []
            self.model.train()
            pbar = tqdm(total=len(trainset), file=sys.stdout)
            iorder = np.arange(len(trainset))
            np.random.shuffle(iorder)
            # for di, xdata in enumerate(trainset):
            for di in range(len(trainset)):
                xdata = trainset[iorder[di]]
                for key in xdata:
                    xdata[key] = xdata[key].to(self.device)

                self.optim.zero_grad()
                preds, autencs = self.model(xdata)
                loss = self.criterion(xdata, preds)
                loss.backward()
                self.optim.step()

                lossHistory.append(loss)
                desc = f'  Iter {di}: loss={np.mean(lossHistory):.4f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            self.tloss.append( np.mean(lossHistory) )

            if epoch % self.config['training']['evaluation_interval'] == 0:
                print('eval:')
                loss = self.evaluate()
                trainloss = self.tloss[-1]
                print('train loss:', trainloss)
                print('eval loss:', loss)
                self.vloss.append( loss )
                if loss[0] < self.bestloss:
                    self.bestloss = loss[0]
                    best_epoch = epoch
                    filen = os.path.join(
                        self.config['training']['checkpoint_path'],
                        'best_epoch.txt'
                    )
                    with open(filen, 'w') as f:
                        f.write(str(epoch))

            if epoch % self.config['training']['checkpoint_interval'] == 0:
                self.save_model(epoch)

    def evaluate(self):
        if self.validloader is None:
            validset = TrajectoryDataset('b_rgb_data', data_keys=['rgb'], train=False)
            self.validloader = validset

        lossHistory = []
        rgb_losses = []
        autenc_losses = []
        self.model.eval()
        with torch.no_grad():
            pbar = tqdm(total=len(self.validloader), file=sys.stdout)
            iorder = np.arange(len(self.validloader))
            np.random.shuffle(iorder)
            for di in range(len(self.validloader)):
                xdata = self.validloader[iorder[di]]
                for key in xdata:
                    xdata[key] = xdata[key].to(self.device)

                preds, autencs = self.model(xdata)
                loss = self.criterion(xdata, preds)

                lossHistory.append(loss)
                desc = f'  Iter {di}: loss={np.mean(lossHistory):.4f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
        return np.mean(lossHistory)
