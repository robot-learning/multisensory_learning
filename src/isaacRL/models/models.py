
import torch
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F

from typing import Type, Any, Callable, Union, List, Optional

class Mish(nn.Module):
    """
    Mish activation function from:
    "Mish: A Self Regularized Non-Monotonic Neural Activation Function"
    https://arxiv.org/abs/1908.08681v1
    x * tanh( softplus( x ) )
    """

    def __init__(self):
        super(Mish, self).__init__()

    def forward(self, x):
        return x * torch.tanh( F.softplus(x) )

activations = {'relu': nn.ReLU, 'mish': Mish, 'selu': nn.SELU, 'elu': nn.ELU}

class ResBlock(nn.Module):
    """
    Based on https://github.com/pytorch/vision/blob/master/torchvision/models/resnet.py#L37
    """
    expansion: int = 1

    def __init__(
        self,
        inchans: int,
        outchans: int,
        stride: int = 1,
        activation: Optional[Callable[..., nn.Module]] = nn.ReLU,
        norm_layer: Optional[Callable[..., nn.Module]] = nn.BatchNorm2d
    ) -> None:
        super(ResBlock, self).__init__()

        self.inchans = inchans
        self.outchans = outchans
        self.stride = stride
        self.activation = activation()
        self.model = nn.Sequential(
          nn.Conv2d(inchans, outchans, kernel_size=3, stride=stride, padding=1),
          norm_layer(outchans),
          self.activation,
          nn.Conv2d(outchans, outchans, kernel_size=3, stride=stride, padding=1),
          norm_layer(outchans),
          self.activation
        )
        self.conv11 = nn.Conv2d(inchans + outchans, outchans, kernel_size=1, stride=1, padding=0)

    def forward(self, x: Tensor) -> Tensor:
        identity = x

        out = self.model(x)

        out = torch.cat((out, identity), 1)
        out = self.activation(self.conv11(out))

        return out

class ConvNet(nn.Module):
    """Convolutional Network"""

    def __init__(self, inchans=3, activation=nn.ReLU, lastivation=None):
        super(ConvNet, self).__init__()

        self.model = nn.Sequential(
          ResBlock(inchans, 32, activation=activation),
          nn.MaxPool2d(2),
          ResBlock(32, 64, activation=activation),
          nn.MaxPool2d(2),
          ResBlock(64, 128, activation=activation),
          ResBlock(128, 64, activation=activation),
          nn.MaxPool2d(2),
          nn.Conv2d(64, 1, kernel_size=1, stride=1, padding=0)
        )
        self.lastivation = lastivation

    def forward(self, xdata):
        ydata = self.model(xdata)
        if self.lastivation is not None:
            ydata = self.lastivation(ydata)
        return ydata


class FCNet(nn.Module):
    """Fully Connected Network"""

    def __init__(self, infeats=10, outfeats=10, layers=[15, 10, 15], activation='relu',
      lastivation=None, batchnorm=False, dropout=0):
        super(FCNet, self).__init__()
        self.infeats = infeats
        self.outfeats = outfeats
        self.layers = layers
        activation = activations[activation]

        model = []
        layers = [infeats] + layers
        for ix, layer_size in enumerate(layers[1:]):
            model.append(nn.Linear(layers[ix], layer_size))
            model.append(activation())
            if batchnorm and ix < (len(layers)-1):
                model.append(nn.BatchNorm1d(layer_size))
            if dropout > 0 and ix < (len(layers)-1):
                model.append(nn.Dropout(dropout))
        model.append(nn.Linear(layers[-1], outfeats))
        self.model = nn.Sequential( *model )
        self.lastivation = lastivation

    def forward(self, xdata):
        ydata = self.model(xdata)
        if self.lastivation is not None:
            ydata = self.lastivation(ydata)
        return ydata


class MVNModel(nn.Module):
    """
    MultivariateNormalModel:
    outpus a mean and stdev for a mutltivariate gaussian distribution
    """
    def __init__( self, infeats=10, outfeats=10, layers=[15, 10, 15],
            activation='relu', batchnorm=False, dropout=0, log_range=[-20, 2] ):
        super(MVNModel, self).__init__()
        activation = activations[activation]
        model = []
        layers = [infeats] + layers
        for ix, layer_size in enumerate(layers[1:]):
            model.append(nn.Linear(layers[ix], layer_size))
            model.append(activation())
            if batchnorm:
                model.append(nn.BatchNorm1d(layer_size))
            if dropout > 0:
                model.append(nn.Dropout(dropout))
        self.model = nn.Sequential( *model )
        last_size = layers[-1]

        self.mu = nn.Linear( last_size, outfeats )
        self.sig = nn.Linear( last_size, outfeats )

        self.LOG_SIG_MAX = log_range[1]
        self.LOG_SIG_MIN = log_range[0]

    def forward(self, x, pause=False):
        z_h = self.model(x)
        mu = self.mu(z_h)
        sig = self.sig(z_h)
        sig = torch.clamp(sig, min=self.LOG_SIG_MIN, max=self.LOG_SIG_MAX)
        sigexp = torch.exp( sig )
        return mu, sigexp

class IdentityNet(nn.Module):
    """docstring for IdentityNet."""

    def __init__(self):
        super(IdentityNet, self).__init__()

    def forward(self, xdata):
        return xdata
