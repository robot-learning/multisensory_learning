
import numpy as np

import torch
import torch.nn as nn
import torch.distributions as D
from torch.distributions.multivariate_normal import MultivariateNormal
from torch.distributions.mixture_same_family import MixtureSameFamily

def get_distribution(alphas, mus, sig, sig_diag):
    bsize = mus.shape[0]
    mixnum = mus.shape[1]
    dimsize = mus.shape[2]
    diag_idx = np.arange(dimsize)
    tril_idx = np.tril_indices(dimsize, -1)
    sigma = torch.zeros(bsize, mixnum, dimsize, dimsize).to(sig.device)
    sigma[:, :, tril_idx[0], tril_idx[1]] = sig
    # sigma += torch.transpose(sigma, -2, -1)
    sigma[:, :, diag_idx, diag_idx] = sig_diag

    mix = D.Categorical( alphas )
    # comp = D.Independent( D.MultivariateNormal( mus, sigma ), 1 )
    comp = D.Independent( D.MultivariateNormal( mus, scale_tril=sigma ), 0 )
    gmm = MixtureSameFamily( mix, comp )
    return gmm
