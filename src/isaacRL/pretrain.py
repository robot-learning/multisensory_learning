
from ll4ma_util import file_util
from isaacRL.trainers.modality_trainer import TrainerModals

import os, sys, argparse, json, yaml
import numpy as np
from tqdm import tqdm


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str,
        default="configs/autenc.json", help="path to config file")
    parser.add_argument("-v", "--verbose", default=False,
        help="if print all info")
    parser.add_argument("--continu", type=str, default=None,
        help="if continuing training from checkpoint model")
    opt = parser.parse_args()
    # print(opt)

    config = file_util.load_yaml(opt.config)

    trainer = TrainerModals(config)

    if opt.continu is not None:
        # checkpt = os.path.join( opt.continu )
        checkpt = opt.continu
        trainer.load_model(checkpt)

    trainer.train()


if __name__ == '__main__':
    main()
