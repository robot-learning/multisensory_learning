#!/usr/bin/env python
import sys
import os.path as osp
import trimesh
import torch
import numpy as np

from ll4ma_util import ros_util, math_util, torch_util
from multisensory_learning.learning import Ortho6dConfig, Ortho6dLearner
from multisensory_learning.util import learn_util
from distribution_planning.distributions import Gaussian


def pos_ortho6d_to_homogeneous(pos_ortho6d):
    pos, ortho6d = np.split(pos_ortho6d, [3])
    rot = torch_util.ortho6d_to_rotation(torch.tensor(ortho6d)).numpy()
    return math_util.pose_to_homogeneous(pos, R=rot)


if __name__ == '__main__':

    np.set_printoptions(precision=4, suppress=True)
        
    asset_dir = osp.join(
        ros_util.get_path('ll4ma_isaacgym'),
        'src',
        'll4ma_isaacgym',
        'assets'
    )
    mesh_fn = osp.join(asset_dir, 'cleaner.stl')

    ortho6d_cp = '/media/data_haro/ortho6d_cp/ortho6d_001.pt'
    ortho6d_config = Ortho6dConfig(checkpoint=ortho6d_cp)
    ortho6d_learner = Ortho6dLearner(ortho6d_config, set_eval=True, device='cpu')

    random_poses = [
        math_util.uniform_random_planar_pose([-0.01, -0.01, -0.01], [ 0.01,  0.01, 0.01])
        for _ in range(100)
    ]

    vecs = []
    ortho6ds = []
    meshes = []
    for p, q in random_poses:
        # T = math_util.pose_to_homogeneous(p, q)
        # mesh = trimesh.load(mesh_fn)
        # mesh.apply_transform(T)
        # meshes.append(mesh)

        ortho6d = learn_util.quat_to_ortho6d(
            torch.tensor(q).unsqueeze(0),
            ortho6d_learner
        ).squeeze().numpy()
        ortho6ds.append(ortho6d)
        vecs.append(np.concatenate([p, ortho6d]))
    vecs = np.array(vecs)
    ortho6ds = np.array(ortho6ds)

    # trimesh.util.concatenate(meshes).show()

    D = vecs.shape[-1]
    mean = np.mean(vecs, axis=0)
    cov = np.cov(vecs.T) + np.eye(D) * 1e-8  # Make PSD
    # cov = np.diag(np.diag(cov))  # TODO trying diagonalize
    gauss = Gaussian(mean, cov)
    sps = gauss.get_sigma_points(beta=2)

    
    # # Visualize samples:
    # meshes = []
    # samples = gauss.sample(100)
    # # samples = vecs  # Nominal samples
    # for pos_ortho6d in samples:
    #     T = pos_ortho6d_to_homogeneous(pos_ortho6d)
    #     mesh = trimesh.load(mesh_fn)
    #     mesh.apply_transform(T)
    #     meshes.append(mesh)
    # trimesh.util.concatenate(meshes).show()
    # sys.exit()

    meshes = []
    T_mean = pos_ortho6d_to_homogeneous(gauss.get_mean())
    mean_mesh = trimesh.load(mesh_fn)
    mean_mesh.apply_transform(T_mean)
    mean_mesh.visual.face_colors = [1., 0., 0., 1.]
    meshes.append(mean_mesh)
    for i in range(D):
        pos_ortho6d_plus = sps[i,:]
        T_plus = pos_ortho6d_to_homogeneous(pos_ortho6d_plus)
        mesh_plus = trimesh.load(mesh_fn)
        mesh_plus.visual.face_colors = [0., 1., 0., 0.6]
        mesh_plus.apply_transform(T_plus)
        meshes.append(mesh_plus)

        pos_ortho6d_minus = sps[D+i,:]
        T_minus = pos_ortho6d_to_homogeneous(pos_ortho6d_minus)
        mesh_minus = trimesh.load(mesh_fn)
        mesh_minus.visual.face_colors = [0., 0., 1., 0.6]
        mesh_minus.apply_transform(T_minus)
        meshes.append(mesh_minus)

    trimesh.util.concatenate(meshes).show()
        
    
