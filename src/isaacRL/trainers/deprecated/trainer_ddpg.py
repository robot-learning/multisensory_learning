
from isaacRL.tasks.sim_task import SimTask
from isaacRL.tasks.gym_task import GymTask

from isaacRL.trainers.trainer_rl import TrainerRL
from isaacRL.models.ddpg import Actor, Critic
from isaacRL.utils.buffer import Buffer
from isaacRL.utils.utils import freeze_net, unfreeze_net, OUNoise

import torch
import torch.nn as nn
import torch.nn.functional as F

import os, sys, argparse, json, random
import time
import os.path as osp
import numpy as np
from tqdm import tqdm
import rospy


class TrainerDDPG(TrainerRL):
    """
    Holder of models and data for training and evaluating DDPF
    Reinforcement Learning method.
    """

    def __init__(self, config):
        super(TrainerDDPG, self).__init__(config)

        self.buffer = Buffer(
          self.config['training']['max_buff_size'], self.device
        )

        obs_shape = (config['environ']['num_obs'],)
        self.action_space = (config['model']['actor']['actions'],)
        try:
            obs_shape = self.envir.obs_space.shape
            self.action_space = self.envir.act_space.shape
        except Exception as e:
            pass

        self.gamma = self.config['training']['gamma']
        self.tau = self.config['training']['tau']
        self.anneal_tau = None if 'anneal_tau' not in self.config['training'] else self.config['training']['anneal_tau']
        self.targ_upd_inter = self.config['training']['target_update_interval']

        self.critic = Critic( self.config['model'] ).to(self.device)
        self.actor = Actor( self.config['model'] ).to(self.device)
        self.target_q = Critic( self.config['model'] ).to(self.device)
        self.target_q.set_weights(self.critic)
        freeze_net(self.target_q)
        self.target_actor = Actor( self.config['model'] ).to(self.device)
        self.target_actor.set_weights(self.actor)
        freeze_net(self.target_actor)
        # print(self.critic)
        print(self.actor)

        self.critic_opt = torch.optim.Adam(
          self.critic.parameters(), lr=self.lr, weight_decay=self.weight_decay
        )
        self.critic_scheduler = None
        self.actor_opt = torch.optim.Adam(
          self.actor.parameters(), lr=self.lr, weight_decay=self.weight_decay
        )
        self.actor_scheduler = None
        if 'anneal_lr' in self.config['training']:
            anneal = self.config['training']['anneal_lr']
            self.critic_scheduler = torch.optim.lr_scheduler.MultiStepLR(
              self.critic_opt, milestones=anneal[1], gamma=anneal[0], verbose=True
            )
            self.actor_scheduler = torch.optim.lr_scheduler.MultiStepLR(
              self.actor_opt, milestones=anneal[1], gamma=anneal[0], verbose=True
            )
        self.closses, self.alosses = [], []

        self.random_start = False if 'random_start' not in self.config['training'] else self.config['training']['random_start']
        self.action_noise = True if 'action_noise' not in self.config['training'] else self.config['training']['action_noise']
        self.episodic = False if 'episodic' not in self.config['training'] else self.config['training']['episodic']
        self.noisifier = OUNoise( (
            (self.num_envs, self.config['model']['actor']['actions']),
            self.config['model']['actor']['act_range']
          ),
          max_sigma=self.config['training']['sigma'][1],
          min_sigma=self.config['training']['sigma'][0],
          episodic=self.episodic, device=self.device
        )
        self.ccriterion = nn.MSELoss()

    def train(self):

        interv = self.config['training']['checkpoint_interval']

        self.critic.train()
        self.actor.train()
        self.envir.reset()
        # print('init_pose:', self.envir.state['ee_state'][0,:7])
        for epoch in range(self.start_epoch, self.end_epoch):
            if rospy.is_shutdown():
                return

            # print('tarq weights', self.target_q.model.model[-1].weight[0,:10])
            rewards = []
            pbar = tqdm(total=interv, file=sys.stdout)
            for di in range(interv):
                if rospy.is_shutdown():
                    return

                start = time.time()
                if len(self.buffer) < self.batch_size:
                    temp = self.acts_per_interval
                    self.acts_per_interval = (
                      (1 + (self.batch_size // self.num_envs))
                      - self.acts_per_interval
                    )
                    rewards.append( self.play( ((epoch-1)*interv)+di, random=self.random_start ) )
                    self.acts_per_interval = temp
                rewards.append( self.play(((epoch-1)*interv)+di) )
                play_time = time.time() - start

                # learn from replays
                start = time.time()
                closs, aloss = self.update_weights(((epoch-1)*interv)+di)
                learn_time = time.time() - start
                self.closses.append( closs )
                self.alosses.append( aloss )
                desc = f'  R {rewards[-1]:.3f}: loss={np.mean(self.closses[-(di+1):]):.3f},{np.mean(self.alosses[-(di+1):]):.3f}'# time={play_time:.2f},{learn_time:.2f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            print('epoch:', epoch)
            # rounds = interv * self.acts_per_interval * epoch
            # print('envsteps {}'.format(self.envir.num_envs * rounds))
            # print('actsteps {}'.format(rounds))
            # rounds = interv * self.updates_per_interval * epoch
            # print('updates {}'.format(rounds))
            print('mean rew per step:', np.mean(rewards))
            print('mean closs:', np.mean(self.closses[-interv:]))
            print('mean aloss:', np.mean(self.alosses[-interv:]))
            self.save_model(epoch)
            if self.critic_scheduler is not None:
                self.critic_scheduler.step()
                self.actor_scheduler.step()
            if self.anneal_tau is not None:
                if epoch in self.anneal_tau[1]:
                    self.tau *= self.anneal_tau[0]
                    print('Adjusting tau to', self.tau)

    def add_noise(self, actions, stepi, done):
        if self.action_noise:
            if self.episodic:
                import pdb; pdb.set_trace()
                self.noisifier.evolve_state(done)
            return self.noisifier.get_action(actions, stepi)
        return actions

    def play(self, playi, random=False):
        with torch.no_grad():
            self.critic.eval()
            self.actor.eval()
            state = self.envir.get_env_states()
            done = self.envir.reset_buf.clone()
            tot_rewards = 0.
            for step in range(self.acts_per_interval):
                if rospy.is_shutdown():
                    return tot_rewards
                if random:
                    actions = self.actor.random(state)
                else:
                    actions = self.actor(state)
                    actions = self.add_noise(actions, playi, done)
                nstate, rewards, done = self.envir.step(actions)
                tot_rewards += rewards.mean().item()
                # if done.sum() > 0:
                #     print('rewards:', rewards)
                self.buffer.add_transitions( {
                  'state': state, 'nstate': nstate, 'actions': actions,
                  'rewards': rewards, 'terminals': done
                } )
                state = nstate
            return tot_rewards/self.acts_per_interval


    def update_weights(self, update_i):
        mean_closs = 0.
        mean_aloss = 0.
        self.critic.train()
        self.actor.train()
        for epoch in range(self.updates_per_interval):
            if rospy.is_shutdown():
                return
            (
              state, statep, action, reward, done
            ) = self.buffer.sample(self.batch_size)
            actionsp = self.target_actor(statep)
            qvals = reward.unsqueeze(-1) + self.gamma * self.target_q(statep, actionsp) * (1.0 - done.unsqueeze(-1))
            cvals = self.critic(state, action)
            closs = self.ccriterion(qvals, cvals)
            # closs = ((qvals - cvals)**2).mean()

            self.critic_opt.zero_grad()
            closs.backward()
            self.critic_opt.step()

            action = self.actor(state)
            aloss = -self.critic(state, action)
            aloss = aloss.mean()

            self.actor_opt.zero_grad()
            aloss.backward()
            self.actor_opt.step()

            mean_closs += closs.item()
            mean_aloss += aloss.item()

        if update_i % self.targ_upd_inter == 0:
            self.target_q.update(self.critic, self.tau)
            self.target_actor.update(self.actor, self.tau)

        mean_closs /= self.updates_per_interval
        mean_aloss /= self.updates_per_interval
        return mean_closs, mean_aloss

    @property
    def num_envs(self):
        return self.envir.num_envs

    def load_model(self, checkpt):
        checkpoint = torch.load(checkpt)
        self.critic.load_state_dict(checkpoint['critic'])
        self.actor.load_state_dict(checkpoint['actor'])
        self.target_q.load_state_dict(checkpoint['target_q'])
        self.target_actor.load_state_dict(checkpoint['target_actor'])
        if self.critic_opt is not None:
            self.critic_opt.load_state_dict(checkpoint['critic_optimizer'])
            self.actor_opt.load_state_dict(checkpoint['actor_optimizer'])
        self.closses = checkpoint['closs']
        self.alosses = checkpoint['aloss']
        # self.bestloss = checkpoint['bestloss']
        self.start_epoch = checkpoint['epochs']+1
        self.end_epoch += self.start_epoch
        self.random_start = False

    def save_model(self, epoch):
        checkpoint = {}
        checkpoint['critic'] = self.critic.state_dict()
        checkpoint['actor'] = self.actor.state_dict()
        checkpoint['target_q'] = self.target_q.state_dict()
        checkpoint['target_actor'] = self.target_actor.state_dict()
        checkpoint['critic_optimizer'] = self.critic_opt.state_dict()
        checkpoint['actor_optimizer'] = self.actor_opt.state_dict()
        checkpoint['closs'] = self.closses
        checkpoint['aloss'] = self.alosses
        # checkpoint['bestloss'] = self.bestloss
        checkpoint['epochs'] = epoch
        filename = osp.join(
          self.config['training']['checkpoint_path'],
          self.config['training']['checkpoint_name'].format(epoch)
        )
        torch.save(checkpoint, filename)

    def evaluate(self, steps=None):
        if steps is None:
            steps = self.config['training']['checkpoint_interval']
        self.critic.eval()
        self.actor.eval()
        action_noise = self.action_noise
        self.action_noise = False
        self.envir.reset()
        rewards = []
        pbar = tqdm(total=steps, file=sys.stdout)
        for di in range(steps):
            if rospy.is_shutdown():
                break

            start = time.time()
            rewards.append( self.play(di) )
            play_time = time.time() - start

            desc = f'  R {rewards[-1]:.3f}'
            pbar.set_description(desc)
            pbar.update(1)
        pbar.close()
        self.action_noise = action_noise
        return rewards
