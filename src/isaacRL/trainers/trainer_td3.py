
from isaacRL.trainers.trainer_ac import TrainerAC
from isaacRL.models.actorcritics import ActorBasic

import torch
import torch.nn.functional as F

import time
import os, sys
import os.path as osp


class TrainerTD3(TrainerAC):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        super(TrainerTD3, self).__init__(config)

        self.policy_tau = self.config['training']['policy_tau']

    def setup_actor(self):
        self.actor = ActorBasic(
            self.observation_size, self.action_size,
            self.config['model']['actor'],
        )
        self.actor.to(self.device)
        self.target_actor = self.actor.copy()
        self.target_actor.noise_clip = True
        self.target_actor.to(self.device)
        self.pretrained_model = self.config['model']['actor'].get('pretrained', None)
        if self.pretrained_model is not None and osp.exists(self.pretrained_model):
            self.load_pretrained(self.pretrained_model)

    def get_actions(self, state, evaluate):
        actions = self.actor(state, not evaluate)
        actions = actions.detach()
        return actions

    def train_setup(self):
        self.actor.forward = self.actor.get_random_action
        super().train_setup()
        self.actor.forward = self.actor.get_action

    def _update_weights(self, state, action, nstate, rewards, terminals):
        with torch.no_grad():
            naction = self.target_actor(nstate)
            q1_tar = self.target_critic1(nstate, naction)
            q2_tar = self.target_critic2(nstate, naction)
            # Two Q-functions to mitigate positive bias in the policy improvement step
            n_qval = torch.min(q1_tar, q2_tar)
            n_qval = rewards + (1-terminals) * self.gamma * n_qval
            n_qval = n_qval.detach()

        qval1 = self.critic1(state, action)
         # JQ = 𝔼(st,at)~D[0.5(Q1(st,at) - r(st,at) - γ(𝔼st+1~p[V(st+1)]))^2]
        q1_loss = F.mse_loss(qval1, n_qval)
        self.critic_optim1.zero_grad()
        q1_loss.backward()
        if self.clip_grad is not None:
            self.clip_grad(self.critic1.parameters())
        self.critic_optim1.step()

        qval2 = self.critic2(state, action)
        q2_loss = F.mse_loss(qval2, n_qval)
        self.critic_optim2.zero_grad()
        q2_loss.backward()
        if self.clip_grad is not None:
            self.clip_grad(self.critic2.parameters())
        self.critic_optim2.step()

        pi_loss = None
        if self.actor_update and self._wupdate_calls % self.policy_update_interval == 0:
            action = self.actor(state)

            # qval1 = self.critic1(state, action)
            # qval2 = self.critic2(state, action)
            # qvals = torch.min(qval1, qval2)
            qvals = self.critic1(state, action)
            pi_loss = -qvals.mean()

            self.actor_optim.zero_grad()
            pi_loss.backward()
            if self.clip_grad is not None:
                self.clip_grad(self.actor.parameters())
            self.actor_optim.step()

        if self._wupdate_calls % self.target_update_interval == 0:
            self.target_critic1 = self.critic1.soft_update(self.tau, self.target_critic1)
            self.target_critic2 = self.critic2.soft_update(self.tau, self.target_critic2)
            self.target_actor = self.actor.soft_update(self.policy_tau, self.target_actor)

        qloss = (q1_loss + q2_loss).item()
        pi_loss = pi_loss.item() if pi_loss is not None else 0.
        return qloss, pi_loss
