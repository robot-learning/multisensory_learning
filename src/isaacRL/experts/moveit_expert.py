
from ll4ma_isaacgym.behaviors import Behavior, behavior_util
from ll4ma_isaacgym.core import SessionConfig
from ll4ma_isaacgym.robots import Robot
from ll4ma_util import ros_util
from std_msgs.msg import ColorRGBA
from copy import deepcopy
import os

import torch

from isaacRL.experts.expert_interface import ExpertInterface
class MoveitExpert(ExpertInterface):
    """docstring for MoveitExpert."""

    def __init__(self, config, envir, open_loop=True):
        super(MoveitExpert, self).__init__(envir, config['max_steps'])
        self.config = config
        env_configf = os.path.expanduser(config['env_config'])
        self.sim_cfg = SessionConfig(config_filename=env_configf)
        self.robot = Robot(
          next( iter( self.sim_cfg.env.robots.values() ) )
        )
        if 'task_config' in self.config:
            from ll4ma_isaacgym.core import TaskConfig
            self.task_cfg = TaskConfig(config_filename=self.config['task_config'])
        else:
            self.task_cfg = self.sim_cfg.task
        self.object_noise = config.get('object_noise', 0.)
        self.save_file = config.get('save_file', None)
        self.open_loop = open_loop

        self.action_skips = config.get('action_skips', 1)

        # We specify a list of potential trajectories
        self.envi_idx = torch.arange(self.envir.num_envs, device=self.device)
        self.similarity_threshold = config.get('similarity_threshold', 0.01)
        self.num_potents = 0
        self.max_trajs = config.get('max_trajs', 1)
        self.recycle = self.max_trajs != 'inf'
        if not self.recycle:
            self.max_trajs = self.envir.num_envs
        self.compstate_sizes = self.config.get('state_sizes', {'ee_state': 3})
        self.state_size = sum([self.compstate_sizes[key] for key in self.compstate_sizes])
        self.start_states = torch.zeros(
          (self.max_trajs, self.state_size),
          dtype=torch.float, device=self.device
        )
        self.potential_trajs = torch.zeros(
          (self.max_trajs, self.max_steps+2, self.envir.act_shape),
          dtype=torch.float, device=self.device
        )
        self.potential_lens = torch.zeros(
          (self.max_trajs,), dtype=torch.long, device=self.device
        )
        self.potential_labels = [[] for _ in range(self.max_trajs)]

        self.plan_idx = torch.zeros(
          (self.envir.num_envs,), dtype=torch.long, device=self.device
        )
        self.plan_len = torch.zeros(
          (self.envir.num_envs,), dtype=torch.long, device=self.device
        )
        self.plans = torch.zeros(
          (self.envir.num_envs, self.max_steps+2, self.envir.act_shape),
          dtype=torch.float, device=self.device
        )
        self.labels = [[] for _ in self.envi_idx]

        if self.recycle and self.save_file is not None:
            self.load_traj(self.save_file)

        self.behavior = self._get_behavior()

        from std_msgs.msg import ColorRGBA
        from ll4ma_isaacgym.core import EnvironmentState
        self.timestep = 0
        self.simstate = EnvironmentState()
        self.simstate.dt = self.sim_cfg.sim.dt
        self.simstate.objects = self.sim_cfg.env.objects
        self.simstate.object_colors = {
          k: ColorRGBA() for k in self.sim_cfg.env.objects.keys()
        }
        self.simstate.joint_names = self.envir.joint_names
        self.simstate.n_arm_joints = self.robot.arm.num_joints()
        self.simstate.n_ee_joints = self.robot.end_effector.num_joints()

    def lengths(self):
        if self.open_loop:
            return self.plan_len
        else:
            return [ self.max_steps for envi in self.envi_idx ]

    def get_actions(self):
        self.actions = self.plans[self.envi_idx,self.plan_idx].clone()
        self.plan_idx = torch.minimum(
          self.plan_idx+1, self.plan_len
        )
        return self.actions, self.plan_idx >= self.plan_len

    def next_actions(self, num_next=1):
        return torch.cat([
        self.plans[self.envi_idx,torch.minimum(self.plan_idx+(i*self.action_skips), self.plan_len)].clone()
            for i in range(num_next)],
        dim=-1)

    @property
    def state(self):
        return self.envir.state

    @property
    def state_idx(self):
        return self.envir.state_idx

    def reset(self):
        """
        Resets the behaviors for each env.
        """
        self.get_plans()
        self.plan_idx *= 0

    def reset_idx(self, resets):
        """
        Resets the behaviors for each env.
        """
        indices = resets.nonzero(as_tuple=False).squeeze(-1)
        if len(indices) > 0:
            self.get_plans( indices )
            self.plan_idx[indices] *= 0

    def _get_behavior(self):
        return Behavior(
          self.task_cfg.behavior, self.robot, self.sim_cfg.env, None,
          device=self.device, open_loop=self.open_loop, rospy_log=False
        )

    def get_plans(self, envis=None):
        if envis is None:
            envis = self.envi_idx
        potids = self.find_closest_potential(envis)
        if potids is None:
            return
        if not (potids == -1).all():
            renvis = envis[potids != -1]
            self.plans[renvis] = self.potential_trajs[potids[potids != -1]]
            self.plan_len[renvis] = self.potential_lens[potids[potids != -1]]
            # self.labels[envis] = self.potential_labels[potids]
        if (potids == -1).any():
            # TODO: fix this for loop to be cheaper timewise
            self.create_plans(envis[potids == -1])

    def create_plans(self, envis):
        for envi in envis:
            potidx = self.create_plan(envi)
            if potidx is None:
                continue
            self.plans[envi] = self.potential_trajs[potidx]
            self.plan_len[envi] = self.potential_lens[potidx]
            self.labels[envi] = self.potential_labels[potidx]
        self.save_traj(self.save_file)

    def create_plan(self, envi):
        behavior = self._get_behavior() # self.behavior
        traj, labels = behavior.get_trajectory(self.get_simstate(envi))
        if traj is None:
            # Something went wrong and the plan failed, so we want to
            # reset the environment to try and fix it
            return None
        plen = min(len(traj), self.max_steps)
        self.potential_trajs[self.num_potents,0,:traj.shape[1]] = traj[0,:]
        self.potential_trajs[self.num_potents,1:plen+1,:traj.shape[1]] = traj[:plen,:]
        self.potential_trajs[self.num_potents,plen+1,:traj.shape[1]] = traj[plen-1,:]
        self.potential_lens[self.num_potents] = plen+1
        self.potential_labels[self.num_potents] = labels
        self.start_states[self.num_potents] = self.get_compstate(envi)
        potidx = self.num_potents
        self.num_potents += 1
        if not self.recycle:
            self.num_potents = self.num_potents % self.max_trajs
        return potidx

    def find_closest_potential(self, envis):
        """
        Searches through the list of potential trajectories to find one with a
        similar enough start state (is greedy, so will choose whatever one appears
        first, not necessarily the best one)
        """
        if not self.recycle:
            return torch.zeros_like(envis) - 1
        if self.num_potents < 1:
            numnew = min(len(envis), self.max_trajs)
            self.create_plans(envis[:numnew])
            if len(envis) < self.max_trajs:
                return None

        states = self.get_compstate(envis)
        comp = torch.stack([self.start_states[:self.num_potents]]*len(envis))
        diff = states.unsqueeze(1) - comp
        # l2 similarity (checking the overall diff)
        simil = torch.norm(diff, p=2, dim=-1)
        # Squared Sum Error
        # simil = torch.sum(torch.pow(diff, 2), dim=-1)
        # l1 similarity
        # simil = torch.sum(diff.abs(),dim=-1)
        # l1 max similarity (checking the largest diff)
        # simil = torch.max(diff.abs(),dim=-1)
        mindx = torch.argmin(simil, dim=1)
        if self.num_potents >= self.max_trajs:
            return mindx
        else:
            idx = torch.arange(len(mindx))
            mins = simil[idx,mindx]
            # goods = idx[mins < self.similarity_threshold]
            bads = idx[mins >= self.similarity_threshold]
            if (self.num_potents+len(bads)) >= self.max_trajs:
                num_allow = self.max_trajs - self.num_potents - 1
                bads = bads[:num_allow]
            mindx[bads] = -1
            return mindx

    def get_compstate(self, envi):
        """
        gets a state for comparison
        """
        return torch.cat(
            [self.state[:,self.state_idx[key][:self.compstate_sizes[key]]][envi]
                for key in self.compstate_sizes],
            dim=-1
        )

    def get_simstate(self, env_idx):
        self.simstate.joint_position = (
          self.state[env_idx,self.state_idx['joint_position']].clone().cpu().unsqueeze(-1)
        )
        self.simstate.joint_velocity = (
          self.state[env_idx,self.state_idx['joint_velocity']].clone().cpu().unsqueeze(-1)
        )
        self.simstate.joint_torque = (
          self.state[env_idx,self.state_idx['joint_force']].clone().cpu().unsqueeze(-1)
        )

        self.simstate.ee_state = self.state[env_idx,self.state_idx['ee_state']].clone().cpu()

        self.simstate.prev_action = self.state[env_idx,self.state_idx['prev_action']].clone().cpu()

        self.simstate.timestep = self.timestep

        for obj_name in self.envir.get_objects():
            self.simstate.object_states[obj_name] = self.state[env_idx,self.state_idx[obj_name]].clone().cpu()

        self.simstate.goal = self.state[env_idx, self.state_idx['goal']].clone().cpu()

        return self.simstate

    def save_traj(self, filename='trajectory.torch'):
        folpath = os.path.expanduser('~/data/isaacgym/trajectories/')
        folpath = os.path.join(folpath, '/'.join(filename.split('/')[:-1]))
        filename = filename.split('/')[-1]
        os.makedirs(folpath, exist_ok=True)
        traj_save = {
          'start_states': self.start_states,
          'potentials': self.potential_trajs,
          'potential_lens': self.potential_lens,
          'labels': self.potential_labels,
          'num_potentials': self.num_potents
        }
        return torch.save(traj_save, os.path.join(folpath, filename))

    def load_traj(self, filename='trajectory.torch'):
        folpath = os.path.expanduser('~/data/isaacgym/trajectories/')
        filpath = os.path.join(folpath, filename)
        if not os.path.exists(filpath):
            return False
        try:
            saves = torch.load(filpath)
            nums = min(saves['potentials'].shape[0], self.potential_trajs.shape[0])
            self.start_states[:nums] = saves['start_states'][:nums]
            self.potential_trajs[:nums] = saves['potentials'][:nums]
            self.potential_lens[:nums] = saves['potential_lens'][:nums]
            self.potential_labels[:nums] = saves['labels']
            self.num_potents = saves['num_potentials']
            return True
        except Exception as e:
            print('loading error:', e)
            return False

def get_trajectory(state, target_pose, ee_name):
    plan, success = behavior_util.get_plan(
      target_pose, state, ee_name,
      "world", [], 0.3, 0.1, 1, 5, False, 1.0
    )
    if success:
        print('plan length:', len(plan.trajectory.joint_trajectory))
        trajectory = ros_util.interpolate_joint_trajectory(plan.trajectory.joint_trajectory, state.dt)
        trajectory = torch.stack(
          [torch.tensor(pt.positions) for pt in trajectory.points]
        )
        return trajectory, ['move']*len(trajectory)
    return None, []
