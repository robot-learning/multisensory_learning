
import torch
import torch.distributions as D
from torch.distributions.multivariate_normal import MultivariateNormal
from torch.distributions.mixture_same_family import MixtureSameFamily
# from utils.distributions import get_distribution
#
# def mdn_loss(alphas, mus, sig, sig_diag, targets):
#     distr = get_distribution(alphas, mus, sig, sig_diag)
#     results = distr.log_prob(targets)
#     return -torch.mean(results)

def logprob_loss(distr, targets):
    mus, sig = distr
    sigma = torch.diag_embed(sig)
    distr = MultivariateNormal(mus, covariance_matrix=sigma)
    results = distr.log_prob(targets)
    return -torch.mean( results )

def logprob_mix_loss(distr, targets):
    # mus, sig, alphas = distr
    # sigma = torch.diag_embed( sig )
    # mix = D.Categorical( alphas.log() )
    # comp = D.Independent( MultivariateNormal(mus, covariance_matrix=sigma), 0 )
    # distr = MixtureSameFamily( mix, comp )
    # results = distr.log_prob( targets )
    # return -torch.mean( results )

    mus, sig, alphas = distr
    n_gaus = alphas.shape[1]
    result = torch.zeros(targets.shape[0], n_gaus).to(mus.device)
    for ai in range(n_gaus):
        sigma = torch.diag_embed( sig[:,ai,:] )
        gaus = MultivariateNormal(mus[:,ai,:], covariance_matrix=sigma)
        result[:,ai] = gaus.log_prob(targets) + alphas[:,ai].log()
    return -torch.mean(torch.logsumexp(result, dim=1))

    # for idx in range(self.n_gaussians):
    #     tmp_mat = torch.zeros(y.shape[0], self.n_outputs, self.n_outputs).to(self.device)
    #     tmp_mat[:, tril_idx[0], tril_idx[1]] = L[:, :, idx]
    #     tmp_mat[:, diag_idx[0], diag_idx[1]] = L_d[:, :, idx]
    #     mvgaussian = MultivariateNormal(loc=mu[:, :, idx], scale_tril=tmp_mat)
    #     result_per_gaussian = mvgaussian.log_prob(y)
    #     result[:, idx] = result_per_gaussian + pi[:, idx].log()
    # return -torch.mean(torch.logsumexp(result, dim=1))

def kldiv_loss(mus, sig, targets):
    sigma = torch.diag_embed(sig)
    distr = MultivariateNormal(mus, covariance_matrix=sigma)
    # distr = D.Independent( D.MultivariateNormal( mus, sigma ), 0 )
    half = targets.shape[1] // 2
    tmus = targets[:,:half]
    tsig = targets[:,half:]
    tsigma = torch.diag_embed(tsig)
    tdistr = MultivariateNormal(tmus, covariance_matrix=tsigma)
    # tdistr = D.Independent( D.MultivariateNormal( tmus, tsigma ), 0 )
    kldiv1 = D.kl.kl_divergence(distr, tdistr)
    kldiv2 = D.kl.kl_divergence(tdistr, distr)
    return torch.mean( kldiv1 + kldiv2 )

def kldiv_mix_loss(mus, sig, targets):
    sigma = torch.diag_embed(sig)
    distr = MultivariateNormal(mus, covariance_matrix=sigma)
    mix = D.Categorical( alphas )
    comp = D.Independent( MultivariateNormal(mus, covariance_matrix=sigma), 0 )
    distr = MixtureSameFamily( mix, comp )

    half = targets.shape[1] // 2
    tmus = targets[:,:half]
    tsig = targets[:,half:]
    tsigma = torch.diag_embed(tsig)
    tdistr = MultivariateNormal(tmus, covariance_matrix=tsigma)
    # tdistr = D.Independent( D.MultivariateNormal( tmus, tsigma ), 0 )

    kldiv1 = D.kl.kl_divergence(distr, tdistr)
    kldiv2 = D.kl.kl_divergence(tdistr, distr)
    return torch.mean( kldiv1 + kldiv2 )


if __name__ == '__main__':

    print('compare logprob loss')
    # mus = torch.rand(2, 5)
    # sig = torch.rand(2, 5)
    mus = torch.zeros(2, 5)
    sig = torch.ones(2, 5)

    tmus = torch.rand(2, 5)
    tsig = torch.rand(2, 5)

    loss = logprob_loss((mus, sig), mus)
    print('random with self:', loss)

    loss = logprob_loss((mus, sig), tmus)
    print('random with random:', loss)

    loss = logprob_loss((mus, sig), mus+0.1)
    print('random with 0.1 add:', loss)

    loss = logprob_loss((mus, sig), mus+1)
    print('random with 1.0 add:', loss)

    loss = logprob_loss((mus, sig), mus-0.1)
    print('random with 0.1 sub:', loss)

    loss = logprob_loss((mus, sig), mus-1)
    print('random with 1.0 sub:', loss)

    loss = logprob_loss((mus, sig), torch.zeros(2, 5))
    print('random with zeros:', loss)

    loss = logprob_loss((mus, sig), torch.ones(2, 5))
    print('random with ones:', loss)


    print('compare logprob mixture loss')
    mus = torch.rand(2, 1, 5)
    sig = torch.rand(2, 1, 5)
    alphas = torch.ones(2, 1)

    loss = logprob_mix_loss((mus, sig, alphas), mus)
    print('random with self:', loss)

    loss = logprob_mix_loss((mus, sig, alphas), tmus)
    print('random with random:', loss)

    loss = logprob_mix_loss((mus, sig, alphas), mus+0.1)
    print('random with 0.1 add:', loss)

    loss = logprob_mix_loss((mus, sig, alphas), mus+1)
    print('random with 1.0 add:', loss)

    loss = logprob_mix_loss((mus, sig, alphas), mus-0.1)
    print('random with 0.1 sub:', loss)

    loss = logprob_mix_loss((mus, sig, alphas), mus-1)
    print('random with 1.0 sub:', loss)

    loss = logprob_mix_loss((mus, sig, alphas), torch.zeros(2, 5))
    print('random with zeros:', loss)

    loss = logprob_mix_loss((mus, sig, alphas), torch.ones(2, 5))
    print('random with ones:', loss)

    # sigma = torch.diag_embed(sig)
    # mdis = D.MultivariateNormal( mus, sigma )
    # mdis2 = MultivariateNormal( mus, sigma )
    # distr = D.Independent( mdis, 0 )
    #
    # tsigma = torch.diag_embed(tsig)
    # tmdis = D.MultivariateNormal( tmus, tsigma )
    # tdistr = D.Independent( tmdis, 0 )
    #
    # mkldiv = D.kl.kl_divergence(mdis, tmdis)
    # mkldiv2 = D.kl.kl_divergence(tmdis, mdis)
    #
    # kldiv = D.kl.kl_divergence(distr, tdistr)
    # kldiv2 = D.kl.kl_divergence(tdistr, distr)
    # import pdb; pdb.set_trace()
