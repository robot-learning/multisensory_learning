
import numpy as np
import os
import torch

from isaacgym import gymutil, gymtorch, gymapi
from isaacgym.torch_utils import *
from isaacgymenvs.tasks.base.vec_task import VecTask

from isaacRL.tasks.gymtasks.gym_rewards import *

from ll4ma_util import ros_util

class FrankaStack(VecTask):

    def __init__(self, cfg, sim_device, graphics_device_id, headless):
        self.cfg = cfg
        self.franka_name = "franka"
        self.hand_name = "panda_link7"
        self.franka_joints = 7
        self.subactions = False
        self.block_size = 0.04

        self.max_episode_length = self.cfg["env"]["episodeLength"]

        self.start_joint_noise = 0.0 # 0.25
        self.action_scale = self.cfg["env"]["actionScale"]
        self.start_position_noise = self.cfg["env"]["startPositionNoise"]
        self.start_rotation_noise = self.cfg["env"]["startRotationNoise"]
        self.aggregate_mode = self.cfg["env"]["aggregateMode"]

        self.dof_vel_scale = self.cfg["env"]["dofVelocityScale"]
        self.dist_reward_scale = self.cfg["env"]["distRewardScale"]
        self.rot_reward_scale = self.cfg["env"]["rotRewardScale"]
        self.around_handle_reward_scale = self.cfg["env"]["aroundHandleRewardScale"]
        self.open_reward_scale = self.cfg["env"]["openRewardScale"]
        self.finger_dist_reward_scale = self.cfg["env"]["fingerDistRewardScale"]
        self.action_penalty_scale = self.cfg["env"]["actionPenaltyScale"]

        self.debug_viz = self.cfg["env"]["enableDebugVis"]

        self.up_axis = "z"
        self.up_axis_idx = 2

        self.dt = 1/60.

        num_obs = 27 # 23
        num_acts = 9

        self.cfg["env"]["numObservations"] = num_obs
        self.cfg["env"]["numActions"] = num_acts

        super().__init__(
          config=self.cfg, sim_device=sim_device,
          graphics_device_id=graphics_device_id, headless=headless
        )

        # get gym GPU state tensors
        actor_root_state_tensor = self.gym.acquire_actor_root_state_tensor(self.sim)
        dof_state_tensor = self.gym.acquire_dof_state_tensor(self.sim)
        _dof_force = self.gym.acquire_dof_force_tensor(self.sim)
        rigid_body_tensor = self.gym.acquire_rigid_body_state_tensor(self.sim)
        jacobians = self.gym.acquire_jacobian_tensor(self.sim, self.franka_name)

        self.refresh()

        # create some wrapper tensors for different slices
        self.franka_default_dof_pos = to_torch(
          # [1.157, -1.066, -0.155, -2.239, -1.841, 1.003, 0.469, 0.035, 0.035],
          # [0., 0.6, 0., -0.8, 0., 1.4, 0.87, 1.0, 1.0], # fingers horizontal
          # [0., 0.6, 0., -0.8, 0., 1.4, -.7, 1.0, 1.0], # fingers vertical
          # [0., 0.6, 0., -0.8, 0., 0., 0.87, 1.0, 1.0], # hand facing inward
          # [0., 0.6, 0., -0.8, 3.14, 3., 1.57, 1.0, 1.0], # hand facing forward
          [0., 0.6, 0., -0.8, 0., 1., 1.57, 1.0, 1.0], # hand facing forward
          device=self.device
        )
        self.dof_state = gymtorch.wrap_tensor(dof_state_tensor)
        self.franka_dof_state = self.dof_state.view(self.num_envs, -1, 2)[:, :self.num_franka_dofs]
        self.franka_dof_pos = self.franka_dof_state[..., 0]
        self.franka_dof_vel = self.franka_dof_state[..., 1]
        self.robot_dof_for = gymtorch.wrap_tensor(
          _dof_force
        ).view(self.num_envs, -1)

        self.rigid_body_states = gymtorch.wrap_tensor(
          rigid_body_tensor
        ).view(self.num_envs, -1, 13)
        self.num_bodies = self.rigid_body_states.shape[1]

        self.root_state_tensor = gymtorch.wrap_tensor(
          actor_root_state_tensor
        ).view(self.num_envs, -1, 13)

        self.block1_states = self.root_state_tensor[:, 1]
        self.block2_states = self.root_state_tensor[:, 2]
        self.objects = {
          'block1': self.block1_states,
          'block2': self.block2_states,
        }

        self.num_dofs = self.gym.get_sim_dof_count(self.sim) // self.num_envs
        self.franka_dof_targets = torch.zeros(
          (self.num_envs, self.num_dofs), dtype=torch.float, device=self.device
        )

        self.jacobian = gymtorch.wrap_tensor( jacobians )
        print("JAC", self.jacobian.shape)
        self.ee_jacobian = self.jacobian[:,self.hand_handle,:,:self.franka_joints]

        # get the indices for all four objects (robot, block1, block2)
        self.global_indices = torch.arange(
          self.num_envs * 3, dtype=torch.int32, device=self.device
        ).view(self.num_envs, -1)
        self.env_state = {
          'joint_position': self.franka_dof_pos,
          'joint_velocity': self.franka_dof_vel,
          'joint_torque': self.robot_dof_for,
        }

        self.reset_idx(torch.arange(self.num_envs, device=self.device))

    def create_sim(self):
        self.sim_params.up_axis = gymapi.UP_AXIS_Z
        self.sim_params.gravity.x = 0
        self.sim_params.gravity.y = 0
        self.sim_params.gravity.z = -9.81
        self.sim = super().create_sim(
            self.device_id, self.graphics_device_id, self.physics_engine, self.sim_params)
        self._create_ground_plane()
        self._create_envs(self.num_envs, self.cfg["env"]['envSpacing'], int(np.sqrt(self.num_envs)))

    def _create_ground_plane(self):
        plane_params = gymapi.PlaneParams()
        plane_params.normal = gymapi.Vec3(0.0, 0.0, 1.0)
        self.gym.add_ground(self.sim, plane_params)

    def _create_envs(self, num_envs, spacing, num_per_row):
        lower = gymapi.Vec3(-spacing, -spacing, 0.0)
        upper = gymapi.Vec3(spacing, spacing, spacing)

        # asset_root = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../assets")
        # franka_asset_file = "urdf/franka_description/robots/franka_panda.urdf"
        asset_root = os.path.dirname(ros_util.get_path('ll4ma_robots_description'))
        franka_asset_file = 'll4ma_robots_description/urdf/panda/static/panda_base_static.urdf'
        # 'll4ma_robots_description/urdf/iiwa/static/iiwa_reflex_static.urdf'

        if "asset" in self.cfg["env"]:
            asset_root = os.path.join(os.path.dirname(os.path.abspath(__file__)), self.cfg["env"]["asset"].get("assetRoot", asset_root))
            franka_asset_file = self.cfg["env"]["asset"].get("assetFileNameFranka", franka_asset_file)

        # load franka asset
        asset_options = gymapi.AssetOptions()
        asset_options.flip_visual_attachments = True
        asset_options.fix_base_link = True
        asset_options.collapse_fixed_joints = True
        asset_options.disable_gravity = True
        asset_options.thickness = 0.001
        asset_options.default_dof_drive_mode = gymapi.DOF_MODE_POS
        asset_options.use_mesh_materials = True
        franka_asset = self.gym.load_asset(self.sim, asset_root, franka_asset_file, asset_options)

        # create table/block assets
        num_blocks = 2
        asset_options = gymapi.AssetOptions()
        asset_options.density = 2000.0
        asset_options.fix_base_link = False
        block1_asset = self.gym.create_box(
          self.sim, self.block_size, self.block_size, self.block_size, asset_options
        )
        block2_asset = self.gym.create_box(
          self.sim, self.block_size, self.block_size, self.block_size, asset_options
        )
        # asset_options.fix_base_link = True
        # table_asset = self.gym.create_box(
        #   self.sim, 0.9, 0.9, 0.4, asset_options
        # )

        franka_dof_stiffness = to_torch(
          [400, 400, 400, 400, 400, 400, 400, 1.0e6, 1.0e6],
          dtype=torch.float, device=self.device
        )
        franka_dof_damping = to_torch(
          [80, 80, 80, 80, 80, 80, 80, 1.0e2, 1.0e2],
          dtype=torch.float, device=self.device
        )

        self.num_franka_bodies = self.gym.get_asset_rigid_body_count(franka_asset)
        self.num_franka_dofs = self.gym.get_asset_dof_count(franka_asset)
        self.body_names = self.gym.get_asset_rigid_body_names(franka_asset)

        print("num franka bodies: ", self.num_franka_bodies)
        print("num franka dofs: ", self.num_franka_dofs)
        print("franka bodies: ", self.body_names)

        # set franka dof properties
        franka_dof_props = self.gym.get_asset_dof_properties(franka_asset)
        self.franka_dof_lower_limits = []
        self.franka_dof_upper_limits = []
        for i in range(self.num_franka_dofs):
            franka_dof_props['driveMode'][i] = gymapi.DOF_MODE_POS
            if self.physics_engine == gymapi.SIM_PHYSX:
                franka_dof_props['stiffness'][i] = franka_dof_stiffness[i]
                franka_dof_props['damping'][i] = franka_dof_damping[i]
            else:
                franka_dof_props['stiffness'][i] = 7000.0
                franka_dof_props['damping'][i] = 50.0

            self.franka_dof_lower_limits.append(franka_dof_props['lower'][i])
            self.franka_dof_upper_limits.append(franka_dof_props['upper'][i])

        self.franka_dof_lower_limits = to_torch(self.franka_dof_lower_limits, device=self.device)
        self.franka_dof_upper_limits = to_torch(self.franka_dof_upper_limits, device=self.device)
        self.franka_dof_speed_scales = torch.ones_like(self.franka_dof_lower_limits)
        self.franka_dof_speed_scales[[7, 8]] = 0.1
        franka_dof_props['effort'][7] = 200
        franka_dof_props['effort'][8] = 200

        franka_start_pose = gymapi.Transform()
        franka_start_pose.p = gymapi.Vec3(0.0, 0.0, 0.0)
        franka_start_pose.r = gymapi.Quat(0.0, 0.0, 0.0, 1.0)
        # franka_start_pose.p = gymapi.Vec3(0.5, 0.0, 0.0)
        # franka_start_pose.r = gymapi.Quat(0.0, 0.0, 1.0, 0.0)
        # [0., 0.6, 0., -0.8, 0., 1., 1.57]
        # franka_start_pose.p = gymapi.Vec3(0.5, 0., 0.)
        # quat = quat_from_angle_axis(torch.tensor(-torch.pi/2), torch.tensor([0.,0.,1.]))
        # print('pose:', quat)
        # franka_start_pose.r = gymapi.Quat(*quat)
        # franka_start_pose.r = gymapi.Quat(0., 0., 1., 0.)
        # import pdb; pdb.set_trace()

        z_pos = self.block_size / 2. + 0.0025
        block1_start_pose = gymapi.Transform()
        block1_start_pose.p = gymapi.Vec3(0.5, -0.15, z_pos)
        block2_start_pose = gymapi.Transform()
        block2_start_pose.p = gymapi.Vec3(0.5, 0.15, z_pos)

        # compute aggregate size
        num_franka_bodies = self.num_franka_bodies
        num_franka_shapes = self.gym.get_asset_rigid_shape_count(franka_asset)
        num_block1_bodies = self.gym.get_asset_rigid_body_count(block1_asset)
        num_block1_shapes = self.gym.get_asset_rigid_shape_count(block1_asset)
        num_block2_bodies = self.gym.get_asset_rigid_body_count(block2_asset)
        num_block2_shapes = self.gym.get_asset_rigid_shape_count(block2_asset)
        max_agg_bodies = num_franka_bodies + num_block1_bodies + num_block2_bodies
        max_agg_shapes = num_franka_shapes + num_block1_shapes + num_block2_shapes

        self.frankas = []
        self.block1s = []
        self.default_block1_states = []
        self.block2s = []
        self.default_block2_states = []
        self.envs = []

        for i in range(self.num_envs):
            # create env instance
            env_ptr = self.gym.create_env(
                self.sim, lower, upper, num_per_row
            )

            if self.aggregate_mode >= 3:
                self.gym.begin_aggregate(env_ptr, max_agg_bodies, max_agg_shapes, True)

            franka_actor = self.gym.create_actor(
              env_ptr, franka_asset, franka_start_pose, self.franka_name, i, 1, 0
            )
            self.gym.set_actor_dof_properties(env_ptr, franka_actor, franka_dof_props)
            if i == 0:
                self.joint_names = self.gym.get_actor_dof_names(
                  env_ptr, franka_actor
                )

            if self.aggregate_mode == 2:
                self.gym.begin_aggregate(env_ptr, max_agg_bodies, max_agg_shapes, True)

            block1_pose = block1_start_pose
            block1_pose.p.x += self.start_position_noise * (np.random.rand() - 0.5)
            dz = 0.5 * np.random.rand()
            dy = np.random.rand() - 0.5
            block1_pose.p.y += self.start_position_noise * dy
            block1_pose.p.z += self.start_position_noise * dz
            block1_actor = self.gym.create_actor(
              env_ptr, block1_asset, block1_pose, "block1", i, 0, 0
            )
            self.default_block1_states.append( [
              block1_pose.p.x, block1_pose.p.y, block1_pose.p.z,
              block1_pose.r.x, block1_pose.r.y, block1_pose.r.z,
              block1_pose.r.w, 0, 0, 0, 0, 0, 0
            ] )

            block2_pose = block2_start_pose
            block2_pose.p.x += self.start_position_noise * (np.random.rand() - 0.5)
            dz = 0.5 * np.random.rand()
            dy = np.random.rand() - 0.5
            block2_pose.p.y += self.start_position_noise * dy
            block2_pose.p.z += self.start_position_noise * dz
            block2_actor = self.gym.create_actor(
              env_ptr, block2_asset, block2_pose, "block2", i, 0, 0
            )
            self.default_block2_states.append( [
              block2_pose.p.x, block2_pose.p.y, block2_pose.p.z,
              block2_pose.r.x, block2_pose.r.y, block2_pose.r.z,
              block2_pose.r.w, 0, 0, 0, 0, 0, 0
            ] )

            if self.aggregate_mode > 0:
                self.gym.end_aggregate(env_ptr)

            self.envs.append(env_ptr)
            self.frankas.append(franka_actor)

        self.hand_handle = self.gym.find_actor_rigid_body_handle(
          env_ptr, franka_actor, self.hand_name
        )
        self.lfinger_handle = self.gym.find_actor_rigid_body_handle(
          env_ptr, franka_actor, "panda_leftfinger"
        )
        self.rfinger_handle = self.gym.find_actor_rigid_body_handle(
          env_ptr, franka_actor, "panda_rightfinger"
        )

        self.default_block1_states = to_torch(
          self.default_block1_states, device=self.device, dtype=torch.float
        ).view( self.num_envs, 13 )
        self.default_block2_states = to_torch(
          self.default_block2_states, device=self.device, dtype=torch.float
        ).view( self.num_envs, 13 )

        self.init_data()

    def init_data(self):
        hand = self.gym.find_actor_rigid_body_handle(
          self.envs[0], self.frankas[0], self.hand_name
        )
        lfinger = self.gym.find_actor_rigid_body_handle(
          self.envs[0], self.frankas[0], "panda_leftfinger"
        )
        rfinger = self.gym.find_actor_rigid_body_handle(
          self.envs[0], self.frankas[0], "panda_rightfinger"
        )

        hand_pose = self.gym.get_rigid_transform(self.envs[0], hand)
        lfinger_pose = self.gym.get_rigid_transform(self.envs[0], lfinger)
        rfinger_pose = self.gym.get_rigid_transform(self.envs[0], rfinger)

        finger_pose = gymapi.Transform()
        finger_pose.p = (lfinger_pose.p + rfinger_pose.p) * 0.5
        finger_pose.r = lfinger_pose.r

        hand_pose_inv = hand_pose.inverse()
        grasp_pose_axis = 2
        franka_local_grasp_pose = hand_pose_inv * finger_pose
        franka_local_grasp_pose.p += gymapi.Vec3(
          *get_axis_params(0.04, grasp_pose_axis)
        )
        self.franka_local_grasp_pos = to_torch( [
          franka_local_grasp_pose.p.x, franka_local_grasp_pose.p.y,
          franka_local_grasp_pose.p.z
        ], device=self.device ).repeat((self.num_envs, 1))
        self.franka_local_grasp_rot = to_torch( [
          franka_local_grasp_pose.r.x, franka_local_grasp_pose.r.y,
          franka_local_grasp_pose.r.z, franka_local_grasp_pose.r.w
        ], device=self.device ).repeat((self.num_envs, 1))

        block_local_grasp_pose = gymapi.Transform()
        block_local_grasp_pose.p = gymapi.Vec3(
          *get_axis_params(0.0, grasp_pose_axis)
        )
        # block_local_grasp_pose.r = gymapi.Quat(0, -0.5, 0, 0.5)
        block_local_grasp_pose.r = gymapi.Quat(
          *quat_from_angle_axis(torch.tensor(-torch.pi/2), torch.tensor([0.,0.,1.]))
        )
        self.block_local_grasp_pos = to_torch( [
          block_local_grasp_pose.p.x, block_local_grasp_pose.p.y,
          block_local_grasp_pose.p.z
        ], device=self.device ).repeat((self.num_envs, 1))
        self.block_local_grasp_rot = to_torch( [
          block_local_grasp_pose.r.x, block_local_grasp_pose.r.y,
          block_local_grasp_pose.r.z, block_local_grasp_pose.r.w
        ], device=self.device ).repeat((self.num_envs, 1))

        self.gripper_forward_axis = to_torch(
          [0, 0, 1], device=self.device
        ).repeat((self.num_envs, 1))
        self.gripper_x_axis = to_torch(
          [1, 0, 0], device=self.device
        ).repeat((self.num_envs, 1))
        self.block_inward_axis = to_torch(
          [0, 0, -1], device=self.device
        ).repeat((self.num_envs, 1))
        self.block_x_axis = to_torch(
          [1, 0, 0], device=self.device
        ).repeat((self.num_envs, 1))

        self.franka_grasp_pos = torch.zeros_like(self.franka_local_grasp_pos)
        self.franka_grasp_rot = torch.zeros_like(self.franka_local_grasp_rot)
        self.franka_grasp_rot[..., -1] = 1  # xyzw
        self.block_grasp_pos = torch.zeros_like(self.block_local_grasp_pos)
        self.block_grasp_rot = torch.zeros_like(self.block_local_grasp_rot)
        self.block_grasp_rot[..., -1] = 1
        self.franka_lfinger_pos = torch.zeros_like(self.franka_local_grasp_pos)
        self.franka_rfinger_pos = torch.zeros_like(self.franka_local_grasp_pos)
        self.franka_lfinger_rot = torch.zeros_like(self.franka_local_grasp_rot)
        self.franka_rfinger_rot = torch.zeros_like(self.franka_local_grasp_rot)

    def compute_reward(self, actions):
        self.rew_buf[:], self.reset_buf[:] = compute_stack_reward(
          self.reset_buf, self.progress_buf, self.actions, self.block1_states,
          self.franka_grasp_pos, self.block_grasp_pos, self.franka_grasp_rot,
          self.block_grasp_rot, self.franka_lfinger_pos, self.franka_rfinger_pos,
          self.gripper_forward_axis, self.block_inward_axis, self.gripper_x_axis,
          self.block_x_axis, self.num_envs, self.dist_reward_scale,
          self.rot_reward_scale, self.around_handle_reward_scale,
          self.open_reward_scale, self.finger_dist_reward_scale,
          self.action_penalty_scale, self.max_episode_length
        )

    def refresh(self):
        self.gym.refresh_actor_root_state_tensor(self.sim)
        self.gym.refresh_dof_state_tensor(self.sim)
        self.gym.refresh_dof_force_tensor(self.sim)
        self.gym.refresh_rigid_body_state_tensor(self.sim)
        self.gym.refresh_jacobian_tensors(self.sim)

    def get_robot_ee(self, simple=False):
        hand_pose = self.rigid_body_states[:, self.hand_handle][:, 0:7]
        if simple:
            return hand_pose
        global_franka_rot, global_franka_pos = tf_combine(
          hand_pose[:,3:7], hand_pose[:,0:3],
          self.franka_local_grasp_rot, self.franka_local_grasp_pos
        )
        return torch.cat((global_franka_pos, global_franka_rot), 1)

    def compute_observations(self):

        self.refresh()

        hand_pos = self.rigid_body_states[:, self.hand_handle][:, 0:3]
        hand_rot = self.rigid_body_states[:, self.hand_handle][:, 3:7]
        block1_pos = self.block1_states[:, 0:3]
        block1_rot = self.block1_states[:, 3:7]

        """
        take the franka_local_grasp_pos and apply the current position and
        rotation of the hand. The franka_local_grasp_pos is the offset from
        the hand designating the point where it can start grasping. Similarly,
        franka_local_grasp_rot designates the rotational offset. By applying
        the current pose of the hand, we find the correct location where we
        would be grasping if the gripper started to close at that moment.
        """
        (
          self.franka_grasp_rot[:], self.franka_grasp_pos[:],
          self.block_grasp_rot[:], self.block_grasp_pos[:]
        ) = compute_grasp_transforms(
          hand_rot, hand_pos, self.franka_local_grasp_rot, self.franka_local_grasp_pos,
          block1_rot, block1_pos, self.block_local_grasp_rot, self.block_local_grasp_pos
        )

        self.franka_lfinger_pos = self.rigid_body_states[:, self.lfinger_handle][:, 0:3]
        self.franka_rfinger_pos = self.rigid_body_states[:, self.rfinger_handle][:, 0:3]
        self.franka_lfinger_rot = self.rigid_body_states[:, self.lfinger_handle][:, 3:7]
        self.franka_rfinger_rot = self.rigid_body_states[:, self.rfinger_handle][:, 3:7]

        dof_pos_scaled = (
          2.0 * (self.franka_dof_pos - self.franka_dof_lower_limits)
          / (self.franka_dof_upper_limits - self.franka_dof_lower_limits) - 1.0
        )
        to_target = self.block_grasp_pos - self.franka_grasp_pos
        self.obs_buf = torch.cat( (
          dof_pos_scaled,
          self.franka_dof_vel * self.dof_vel_scale,
          to_target,
          self.block1_states[:,:3],
          self.block2_states[:,:3]
        ), dim=-1)

        return self.obs_buf

    def reset_idx(self, env_ids):
        env_ids_int32 = env_ids.to(dtype=torch.int32)
        # if len(env_ids) > 0:
        #     import pdb; pdb.set_trace()

        # reset franka
        pos = tensor_clamp(
          self.franka_default_dof_pos.unsqueeze(0) + self.start_joint_noise * (
            torch.rand( ( len(env_ids), self.num_franka_dofs ),
            device=self.device) - (self.start_joint_noise*2)
          ), self.franka_dof_lower_limits, self.franka_dof_upper_limits
        )
        self.franka_dof_pos[env_ids, :] = pos
        self.franka_dof_vel[env_ids, :] = torch.zeros_like(
          self.franka_dof_vel[env_ids]
        )
        self.franka_dof_targets[env_ids, :self.num_franka_dofs] = pos

        # reset blocks
        self.block1_states[env_ids] = self.default_block1_states[env_ids]
        self.block2_states[env_ids] = self.default_block2_states[env_ids]
        block_indices = self.global_indices[env_ids, 1:].flatten()
        self.gym.set_actor_root_state_tensor_indexed(
          self.sim, gymtorch.unwrap_tensor(self.root_state_tensor),
          gymtorch.unwrap_tensor(block_indices), len(block_indices)
        )

        multi_env_ids_int32 = self.global_indices[env_ids, :1].flatten()
        self.gym.set_dof_position_target_tensor_indexed(
          self.sim, gymtorch.unwrap_tensor(self.franka_dof_targets),
          gymtorch.unwrap_tensor(multi_env_ids_int32), len(multi_env_ids_int32)
        )

        self.gym.set_dof_state_tensor_indexed(
          self.sim, gymtorch.unwrap_tensor(self.dof_state),
          gymtorch.unwrap_tensor(multi_env_ids_int32), len(multi_env_ids_int32)
        )

        self.progress_buf[env_ids] = 0
        self.reset_buf[env_ids] = 0

    def pre_physics_step(self, actions):
        self.actions = actions.clone().to(self.device)
        if self.subactions:
            targets = actions
            # targets = (
            #   self.franka_dof_targets[:, :self.num_franka_dofs] + self.actions
            # )
        else:
            targets = (
              self.franka_dof_targets[:, :self.num_franka_dofs] +
              self.franka_dof_speed_scales * self.dt * self.actions * self.action_scale
            )
        self.franka_dof_targets[:, :self.num_franka_dofs] = tensor_clamp(
          targets, self.franka_dof_lower_limits, self.franka_dof_upper_limits
        )
        env_ids_int32 = torch.arange(self.num_envs, dtype=torch.int32, device=self.device)
        self.gym.set_dof_position_target_tensor(
          self.sim, gymtorch.unwrap_tensor(self.franka_dof_targets)
        )

    def post_physics_step(self):
        self.progress_buf += 1

        env_ids = self.reset_buf.nonzero(as_tuple=False).squeeze(-1)
        if len(env_ids) > 0:
            self.reset_idx(env_ids)

        self.compute_observations()
        self.compute_reward(self.actions)

        # debug viz
        if self.viewer and self.debug_viz:
            self.gym.clear_lines(self.viewer)
            self.gym.refresh_rigid_body_state_tensor(self.sim)

            for i in range(self.num_envs):
                px = (self.franka_grasp_pos[i] + quat_apply(self.franka_grasp_rot[i], to_torch([1, 0, 0], device=self.device) * 0.2)).cpu().numpy()
                py = (self.franka_grasp_pos[i] + quat_apply(self.franka_grasp_rot[i], to_torch([0, 1, 0], device=self.device) * 0.2)).cpu().numpy()
                pz = (self.franka_grasp_pos[i] + quat_apply(self.franka_grasp_rot[i], to_torch([0, 0, 1], device=self.device) * 0.2)).cpu().numpy()

                p0 = self.franka_grasp_pos[i].cpu().numpy()
                self.gym.add_lines(self.viewer, self.envs[i], 1, [p0[0], p0[1], p0[2], px[0], px[1], px[2]], [0.85, 0.1, 0.1])
                self.gym.add_lines(self.viewer, self.envs[i], 1, [p0[0], p0[1], p0[2], py[0], py[1], py[2]], [0.1, 0.85, 0.1])
                self.gym.add_lines(self.viewer, self.envs[i], 1, [p0[0], p0[1], p0[2], pz[0], pz[1], pz[2]], [0.1, 0.1, 0.85])

                px = (self.franka_lfinger_pos[i] + quat_apply(self.franka_lfinger_rot[i], to_torch([1, 0, 0], device=self.device) * 0.2)).cpu().numpy()
                py = (self.franka_lfinger_pos[i] + quat_apply(self.franka_lfinger_rot[i], to_torch([0, 1, 0], device=self.device) * 0.2)).cpu().numpy()
                pz = (self.franka_lfinger_pos[i] + quat_apply(self.franka_lfinger_rot[i], to_torch([0, 0, 1], device=self.device) * 0.2)).cpu().numpy()

                p0 = self.franka_lfinger_pos[i].cpu().numpy()
                self.gym.add_lines(self.viewer, self.envs[i], 1, [p0[0], p0[1], p0[2], px[0], px[1], px[2]], [1, 0, 0])
                self.gym.add_lines(self.viewer, self.envs[i], 1, [p0[0], p0[1], p0[2], py[0], py[1], py[2]], [0, 1, 0])
                self.gym.add_lines(self.viewer, self.envs[i], 1, [p0[0], p0[1], p0[2], pz[0], pz[1], pz[2]], [0, 0, 1])

                px = (self.franka_rfinger_pos[i] + quat_apply(self.franka_rfinger_rot[i], to_torch([1, 0, 0], device=self.device) * 0.2)).cpu().numpy()
                py = (self.franka_rfinger_pos[i] + quat_apply(self.franka_rfinger_rot[i], to_torch([0, 1, 0], device=self.device) * 0.2)).cpu().numpy()
                pz = (self.franka_rfinger_pos[i] + quat_apply(self.franka_rfinger_rot[i], to_torch([0, 0, 1], device=self.device) * 0.2)).cpu().numpy()

                p0 = self.franka_rfinger_pos[i].cpu().numpy()
                self.gym.add_lines(self.viewer, self.envs[i], 1, [p0[0], p0[1], p0[2], px[0], px[1], px[2]], [1, 0, 0])
                self.gym.add_lines(self.viewer, self.envs[i], 1, [p0[0], p0[1], p0[2], py[0], py[1], py[2]], [0, 1, 0])
                self.gym.add_lines(self.viewer, self.envs[i], 1, [p0[0], p0[1], p0[2], pz[0], pz[1], pz[2]], [0, 0, 1])

    def set_viewer(self):
        """Create the viewer."""

        # todo: read from config
        self.enable_viewer_sync = True
        self.viewer = None

        # if running with a viewer, set up keyboard shortcuts and camera
        if self.headless == False:
            # subscribe to keyboard shortcuts
            self.viewer = self.gym.create_viewer(
              self.sim, gymapi.CameraProperties()
            )
            self.gym.subscribe_viewer_keyboard_event(
              self.viewer, gymapi.KEY_ESCAPE, "QUIT"
            )
            self.gym.subscribe_viewer_keyboard_event(
              self.viewer, gymapi.KEY_V, "toggle_viewer_sync"
            )

            # set the camera position based on up axis
            sim_params = self.gym.get_sim_params(self.sim)
            if sim_params.up_axis == gymapi.UP_AXIS_Z:
                cam_pos = gymapi.Vec3(4, 3, 2)
                cam_target = gymapi.Vec3(-4, -3, 0)
            else:
                cam_pos = gymapi.Vec3(20.0, 3.0, 25.0)
                cam_target = gymapi.Vec3(10.0, 0.0, 15.0)

            # Point viewer camera at middle env
            n_per_row = int(np.sqrt(self.num_envs))
            look_at_env = self.envs[self.num_envs // 2 + n_per_row // 2]
            self.gym.viewer_camera_look_at(
              self.viewer, look_at_env, cam_pos, cam_target
            )
