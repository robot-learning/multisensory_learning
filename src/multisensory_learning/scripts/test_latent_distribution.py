#!/usr/bin/env python
import os
import sys
import argparse
import torch
import torch.nn.functional as F
import h5py
import numpy as np

import matplotlib.pyplot as plt

from multisensory_learning.learners import LatentPlanningLearner
from multisensory_learning.datasets import LatentPlanningDataset
from multisensory_learning.common.config import default_config
from multisensory_learning.util import file_util


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--checkpoint', type=str, required=True)
    parser.add_argument('--h5', type=str, required=True)
    parser.add_argument('--goal_idx', type=int, default=0)
    parser.add_argument('--n_candidates', type=int, default=1000)
    parser.add_argument('--n_elite', type=int, default=100)
    parser.add_argument('--n_iters', type=int, default=10)
    parser.add_argument('--use_cpu', action='store_true')
    parser.add_argument('--show_cost', action='store_true')
    args = parser.parse_args()
    
    file_util.check_path_exists(args.checkpoint)
    file_util.check_path_exists(args.h5)

    if torch.cuda.is_available() and not args.use_cpu:
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    
    learner = LatentPlanningLearner(checkpoint_filename=args.checkpoint, device=device)
    learner.set_models_to_eval()

    # Get the initial obs/act to get initial state
    with h5py.File(args.h5, 'r') as h5_file:
        obs = {}
        act = {}
        for modality in learner.config.obs_modalities:
            obs[modality] = learner.dataset.process_data_in(h5_file[modality][:1], modality)
        for modality in learner.config.act_modalities:
            act[modality] = learner.dataset.process_data_in(h5_file[modality][:1], modality)

        # Add batch and send to device
        obs = {k: v.unsqueeze(1).to(device) for k, v in obs.items()}
        act = {k: v.unsqueeze(1).to(device) for k, v in act.items()}

        orig_imgs = {'rgb': np.array(h5_file['rgb'][0])}

        goal = h5_file.attrs['task_goals'][args.goal_idx]
        print("GOAL", goal)

    rssm_out = learner.compute_transition(act, obs)
    init_state = rssm_out.posterior_states[-1]
    
    mean, std_dev = learner.find_latent_distribution(args.goal_idx, init_state, args.n_candidates,
                                                     args.n_elite, args.n_iters, args.show_cost)
    
    fig, axes = plt.subplots(4, 5)
    fig.set_size_inches((10, 10))
    for i in range(4):
        for j in range(4):
            noise = torch.randn_like(mean)
            state = mean + std_dev * noise
            # state = mean
            state = state.unsqueeze(0) # Add batch
            state = state.unsqueeze(0) # Add time
            # state = torch.randn_like(state) * 10
            
            decoded = learner.decode_state(state)
            axes[i,j].imshow(decoded['rgb'].squeeze())
            axes[i,j].axis('off')

    # Show the ground truth
    for i in range(4):
        axes[i,-1].axis('off')
    axes[-1,-1].imshow(orig_imgs['rgb'])
    
    plt.suptitle(goal.decode('utf-8'))
    plt.tight_layout()
    plt.show()
