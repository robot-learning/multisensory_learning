
from isaacgym import gymtorch, gymapi

from ll4ma_isaacgym.core import SessionConfig, Simulator
from ll4ma_util import file_util, ui_util, ros_util, vis_util
from isaacRL.utils.data_utils import batch_dict
from isaacRL.utils.utils import get_pinv
from isaacRL.utils.goalies import select_goalie

import os, math
import os.path as osp
from copy import deepcopy
from collections import defaultdict
import numpy as np
import torch
import torch.nn.functional as F
# import scipy.spatial
# from scipy.spatial.transform import Rotation as R
# from pytorch3d import transforms as trans3d

DEFAULT_ASSET_ROOT = os.path.dirname(ros_util.get_path('ll4ma_robots_description'))

class SimTask(object):
    """
    Expects information to extract for Simulator creation
    """

    def __init__(self, config, device, open_loop=True):

        self.config = config
        self.device = device

        self.additive = self.config['add_actions'] if 'add_actions' in self.config else False
        self.cartesian = self.config['cartesian_control'] if 'cartesian_control' in self.config else False
        self.inv_threshold = 0.00001
        self.goalie_resets = True
        self.act_shape = self.config['actions']
        self.act_range = self.config['act_range']
        self.act_range.append( (self.act_range[1] - self.act_range[0]) )
        self.action_scale = 7.5
        self.open_thresh = self.act_range[0]/2
        self.close_thresh = self.act_range[1]/2
        self.num_envs = self.config['num_envs']
        self.max_env_steps = self.config['max_env_steps']
        self.init_wait_steps = max(1, self.config['init_wait_steps'])
        # self.use_rgb = config['sim']['use_rgb']
        self.use_goal_contextualization = False if 'use_goal_contextualization' not in self.config else self.config['use_goal_contextualization']
        self.dof_vel_scale = 0.1 if 'dof_vel_scale' not in self.config else self.config['dof_vel_scale']
        self.dof_vel_lims = torch.ones((self.num_envs, self.act_shape), device=self.device)*0.1

        # self.object_std = {}
        # self.object_noise = {}
        # if 'object_noise' in self.config:
        #     noises = self.config['object_noise']['noise']
        #     names = self.config['object_noise']['objects']
        #     for name, noise in zip(names, noises):
        #         noise = torch.tensor(noise)
        #         self.object_std[name] = noise
        #         noise = self.object_std[name]
        #         noise = torch.randn((self.num_envs, noise.shape[0])) * noise
        #         self.object_noise[name] = noise

        env_config = osp.expanduser(config['env_config'])
        file_util.check_path_exists(env_config, "Config file")
        sim_config = SessionConfig(config_filename=env_config)

        sim_config.sim.physx_gpu = False
        sim_config.sim.use_gpu = False #torch.cuda.is_available()
        sim_config.sim.device = 'cpu'
        # sim_config.sim.graphics_device_id = -1
        # sim_config.sim.num_position_iterations = 4
        # sim_config.sim.num_velocity_iterations = 0
        sim_config.sim.n_threads = self.config['num_threads']
        # sim_config.sim.dt = 1./30.

        # Populate args onto session config
        sim_config.data_root = None
        sim_config.data_prefix = None
        sim_config.n_envs = self.num_envs
        sim_config.open_loop = open_loop
        sim_config.randomize_robot = False
        sim_config.run_forever = False
        sim_config.publish_ros = False
        sim_config.demo = False
        sim_config.device = 'cpu'
        sim_config.sim.asset_root = DEFAULT_ASSET_ROOT
        sim_config.sim.render_graphics = True

        sim_config.state_noise = self.config['state_noise'] if 'state_noise' in self.config else 0.0
        self.simulator = Simulator(sim_config)
        self.sim_device = self.simulator.config.device
        # self.rgb_size = ( self.config['sim']['camera']['width'],
        #   self.config['sim']['camera']['height'] )
        # self.simulator.sensor_config.width = self.config['sim']['camera']['width']
        # self.simulator.sensor_config.height = self.config['sim']['camera']['height']

        self.actions = torch.zeros( (self.num_envs, self.act_shape),
          dtype=torch.float, device=self.device )
        self.goalie = select_goalie(config)
        self.goals = torch.zeros( (self.num_envs, self.goalie.goal_shape),
          dtype=torch.float, device=self.device )
        if self.goals.shape[1] > 6:
            self.goals[:,6] += 1

        self.progress_buf = torch.zeros( (self.num_envs,), device=self.device )
        self.rew_buf = torch.zeros( (self.num_envs,), device=self.device )
        self.reset_buf = torch.zeros( (self.num_envs,), device=self.device )

    def step(self, actions=None):
        """
        Perform one step in the task including applying actions, stepping physics,
        and computing observations.
        """
        # Apply actions
        self.pre_physics_step(actions)

        # Step physics
        self.simulator.step()

        # Compute observations
        rews, terms = self.post_physics_step()


        return self.state, rews, terms

    def cartesian2joints(self, cacts):
        inv_jac = get_pinv(self.simulator.ee_jacobian.to(cacts.device), self.inv_threshold)
        if not torch.isfinite(inv_jac).all():
            print('jacobian has infinite or nan values')
            import pdb; pdb.set_trace()
        jacts = torch.bmm(inv_jac, cacts[:,:6].unsqueeze(-1)).squeeze(-1)
        jacts = torch.cat((jacts, cacts[:,6:]), 1)
        return jacts

    def add_actions(self, base_acts, actions):
        ret_acts = base_acts.clone()
        if self.cartesian:
            actions = self.cartesian2joints( actions )
        ret_acts[:,:-1] += actions[:,:-1]
        ret_acts[actions[:,-1] < self.open_thresh,-1] = self.robot.end_effector.config.discretes.open
        ret_acts[actions[:,-1] > self.close_thresh,-1] = self.robot.end_effector.config.discretes.close
        return ret_acts

    def pre_physics_step(self, actions=None):
        if actions is not None:
            if self.cartesian and self.additive:
                actions = self.cartesian2joints( actions )
            # cont_idx = actions[:,:-1].shape[1]
            # actions[:,:-1] = torch.clamp( actions[:,:-1],
            #   min=self.dof_lower_limits[:cont_idx],
            #   max=self.dof_upper_limits[:cont_idx]
            # )
            self.actions[:] = actions[:]
            # if self.additive:
            #     cidx = actions.shape[1]-1
            #     # actions[:,:cidx] *= (
            #     #   self.dof_speed_scales[:cidx] * self.dt #* self.action_scale
            #     # )
            #     actions[:,:cidx] = torch.clamp( actions[:,:cidx],
            #       min=self.act_range[0], # -self.dof_vel_lims
            #       max=self.act_range[1] # self.dof_vel_lims
            #     )
            self.simulator.apply_actions(actions.to(self.sim_device), self.additive)
            self.progress_buf += 1

    def post_physics_step(self):

        self.update_env_states()
        self.compute_reward()

        rews = self.rew_buf.clone()
        terms = self.reset_buf.clone()

        # envis = self.reset_buf.nonzero(as_tuple=False).squeeze(-1)
        envis = torch.arange(self.num_envs)[
          self.reset_buf.nonzero(as_tuple=False).squeeze(-1) ]
        if len(envis) > 0:
            self.reset(envis)

        return rews, terms

    def compute_reward(self):
        self.rew_buf[:], resets = self.goalie.reward(self.state, self.goals)
        # self.rew_buf[:] *= -1
        if self.goalie_resets:
            self.reset_buf[:] = resets[:]
        self.reset_buf = torch.where(
          self.progress_buf >= self.max_env_steps - 1,
          torch.ones_like(self.reset_buf), self.reset_buf
        )

    def reset(self, envis=None):
        if envis is None:
            envis = torch.arange(self.num_envs, device=self.device)

        for envi in envis:
            self.simulator.reset_objects(envi)
            self.simulator.reset_robots(envi)
        for step in range(self.init_wait_steps):
            self.simulator.step()

        self.progress_buf[envis] *= 0
        self.reset_buf[envis] *= 0

        self.update_env_states()
        self.goals[:] = self.goalie.extract_goal(self.state)
        self.state['goal'] = self.goals.clone()
        return self.state

    def update_env_states(self):
        self.state = self._get_env_state()
        for envi in range(self.num_envs):
            self.simulator.get_env_state(envi)

    def get_env_states(self):
        return self.state

    def get_sim_states(self):
        return self.simulator.env_states

    def _get_env_state(self):
        """
        Get values from the environment that we can use for learning
        """
        envstate = {}
        envstate['joint_position'] = self.dof_pos[
          :,-self.robot_num_dofs:,0 ].clone().to(self.device)
        envstate['joint_velocity'] = self.dof_vel[
          :,-self.robot_num_dofs:,0 ].clone().to(self.device)
        envstate['joint_torque'] = self.dof_force[
          :,-self.robot_num_dofs:,0 ].clone().to(self.device)

        envstate['joint_pos_scaled'] = (
          2.0 * (envstate['joint_position'] - self.dof_lower_limits)
          / (self.dof_upper_limits - self.dof_lower_limits)
          - 1.0
        )
        envstate['joint_vel_scaled'] = (
          envstate['joint_velocity']  * self.dof_vel_scale
        )

        # self.env_states[env_idx].prev_action = self.actions[envi]
        envstate['prev_action'] = self.actions.clone()

        # End effector info
        ee_state = self.rb_states[
          self.robot.end_effector.rb_indices, : ].clone().to(self.device)
        envstate['ee_state'] = ee_state
        # envstate['ee_quat'] = ee_state[:,:7]
        # ee_rot = trans3d.quaternion_to_matrix(ee_state[:,3:7])
        # envstate['ee_mat'] = torch.cat((ee_state[:,:3], ee_rot.flatten(1)), dim=1)
        # ee_state[:,3:6] = get_eulers(ee_state[:,3:7])
        # envstate['ee_eul'] = ee_state[:,:6]

        for obj_name, obj_config in self.objects.items():
            rb_idx = obj_config.rb_indices
            obj_state = self.rb_states[rb_idx, :].clone().to(self.device)
            envstate[obj_name] = obj_state
            # obj_rot = R.from_quat(obj_state[:,3:7].cpu().numpy())
            # envstate[obj_name+'_mat'] = torch.cat(
            #   (obj_state[:,:3], torch.tensor(obj_rot.flatten(1)).to(self.device))
            # )
            # if obj_name in self.object_noise:
            #     # noise = self.object_noise[obj_name][envi]
            #     noise = self.object_std[obj_name]
            #     noise = torch.randn((noise.shape[0],)) * noise
            #     envstate[obj_name][:len(noise)] += noise
            # if self.use_goal_contextualization:
            #     envstate[obj_name] = (
            #       envstate[obj_name][:,:3] - envstate['goal'][:,:3]
            #     )

        envstate['goal'] = self.goals.clone()
        envstate['goal_current'] = self.goalie.extract_current(envstate)

        if self.use_goal_contextualization:
            envstate = self.goalie.goal_reference( envstate, self.goals,
              ['ee_state'] + [obj_name for obj_name in self.objects]
            )
            # envstate['ee_state'][:, :3] = (
            #   envstate['ee_state'][:,:3] - envstate['goal'][:,:3]
            # )
            # for obj_name, obj_config in self.objects.items():
            #     envstate[obj_name] = (
            #       envstate[obj_name][:,:3] - envstate['goal'][:,:3]
            #     )

        # if self.use_rgb:
        #     envstate['rgb'] = torch.tensor(
        #       self.simulator._get_rgb_img(
        #         self.simulator.envs[envi], self.simulator.sensor_config ),
        #       dtype=torch.float ).permute(2, 0, 1).unsqueeze(0)
        #     envstate['rgb'] = F.interpolate( envstate['rgb'], self.rgb_size,
        #       mode='bilinear', align_corners=False ) # (t, 3, 64, 64)
        #     envstate['rgb'] = envstate['rgb'].squeeze(0)
        #
        #     envstate['depth'] = torch.tensor(
        #       self.simulator._get_depth_img(
        #         self.simulator.envs[envi], self.simulator.sensor_config ),
        #       dtype=torch.float ).unsqueeze(0).unsqueeze(0)
        #     envstate['depth'] = F.interpolate( envstate['depth'], self.rgb_size,
        #       mode='bilinear', align_corners=False ) # (t, 1, 64, 64)
        #     envstate['depth'] = envstate['depth'].squeeze(0)
        return envstate

    @property
    def sim_cfg(self):
        return self.simulator.config

    @property
    def objects(self):
        return self.sim_cfg.env.objects

    @property
    def robot_num_dofs(self):
        return self.simulator.robot_num_dofs

    @property
    def dof_speed_scales(self):
        return self.simulator.dof_speed_scales.to(self.device)

    @property
    def dof_lower_limits(self):
        return self.simulator.dof_lower_lims.to(self.device)

    @property
    def dof_upper_limits(self):
        return self.simulator.dof_upper_lims.to(self.device)

    @property
    def dt(self):
        return self.sim_cfg.sim.dt

    @property
    def robot(self):
        return self.simulator.robot

    @property
    def joint_names(self):
        return self.simulator.robot_joint_names

    @property
    def rb_states(self):
        return self.simulator.rb_states

    @property
    def env_obj_data(self):
        return self.simulator.env_obj_data

    @property
    def dof_pos(self):
        return self.simulator.dof_pos

    @property
    def dof_vel(self):
        return self.simulator.dof_vel

    @property
    def dof_force(self):
        return self.simulator.dof_force

    @property
    def prev_actions(self):
        return self.simulator.actions

    def get_ee_pose(self):
        return self.rb_states[
          self.robot.end_effector.rb_indices,:7].clone().to(self.device)

    def get_joint_pose(self):
        return self.dof_pos[:,-self.robot_num_dofs:,0].clone().to(self.device)
