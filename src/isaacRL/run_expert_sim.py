
from tasks.sim_task import SimTask
from isaacRL.utils.expert_behaviors import Expert

import torch
from ll4ma_util import file_util
import os, sys, argparse, json, yaml
import numpy as np
from tqdm import tqdm
import rospy

def setup():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str,
        default="configs/rpl_ppo/ppo_iiwastack1.yaml", help="path to config file")
    parser.add_argument("-v", "--verbose", default=False,
        help="if print all info")
    parser.add_argument("-s", "--use_saved", action='store_true', default=False,
        help="if print all info")
    parser.add_argument("--checkpt", type=str, default=None,
        help="if continuing training from checkpoint model")
    opt = parser.parse_args()
    # print(opt)

    rospy.init_node('run_isaacgym')

    config = file_util.load_yaml(opt.config)

    return opt, config

def main():
    opt, config = setup()
    device = "cpu"
    # device = "cuda" if torch.cuda.is_available() else "cpu"
    open_loop = True

    state_feats = 0
    for mod in config['model']['modalities']:
        if mod == 'rgb':
            state_feats += config['model']['modalities'][mod]['outfeats']
        else:
            state_feats += config['model']['modalities'][mod]
    config['environ']['num_obs'] = state_feats
    # config['environ']['actions'] = config['model']['actor']['actions']
    config['environ']['act_range'] = config['model']['actor']['act_range']

    envir = SimTask(config['environ'], device, open_loop)
    envir.goalie_resets = False
    state = envir.reset()
    for _ in range(10):
        if rospy.is_shutdown():
            return
        state = envir.step(None)

    max_steps = config['environ']['max_env_steps']
    print('max steps:', max_steps)
    expert = Expert(envir, max_steps, device, open_loop=open_loop)

    print('resetting behaviors')
    tot_rewards = 0.
    tot_terms = 0.000001
    state = envir.reset()
    if opt.use_saved:
        print('loaded:', expert.load_traj('scalpel.torch'))
    else:
        expert.reset()
        print('saved:', expert.save_traj('scalpel.torch'))
    pbar = tqdm(total=max_steps, file=sys.stdout)
    desc = f'  Expert'
    pbar.set_description(desc)
    for step in range(max_steps):
        if rospy.is_shutdown():
            return

        base_acts, _ = expert.get_actions()
        nstate, rewards, terminals = envir.step(base_acts)
        tot_rewards += rewards.mean().item()
        tot_terms += terminals.sum().item()
        state = nstate
        desc = f'  Expert R: {(rewards.mean().item()):.1f} {(tot_terms):.0f}'
        pbar.set_description(desc)
        pbar.update(1)
    pbar.close()

    rospy.is_shutdown()


if __name__ == '__main__':
    main()
