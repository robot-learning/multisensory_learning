
from isaacRL.tasks.sac_task import SACTask
from isaacRL.tasks.expert_behaviors import Expert
from isaacRL.models.actorcritics import SoftActorCritic
from isaacRL.trainers.trainer_sac import TrainerSAC
from isaacRL.utils.buffer import BufferExp

import os, sys, argparse, json
import os.path as osp
import numpy as np
from tqdm import tqdm
import rospy

import torch
import torch.nn as nn
import torch.nn.functional as F


class TrainerSQIL(TrainerSAC):
    """
    Holder of models and data for training and evaluating SQIL method.
    """

    def __init__(self, config):
        super(TrainerSQIL, self).__init__(config)
        self.expert = Expert(self.envir)
        self.buffer = BufferExp( self.config['training']['max_buff_size'] )


    def train(self):

        interv = self.config['training']['checkpoint_interval']
        eval_iterv = self.config['training']['evaluation_interval'] // interv

        self.expert_demos(self.config['training']['load_experts'])

        # for epoch in range(self.start_epoch, self.end_epoch):
        #
        #     if self.expert_interval > 0 and (epoch % self.expert_interval) == 0:
        #         # Expert actions
        #         self.expert_demos(False)
        #
        #     tot_rewards = 0.0
        #     for di in range(interv):
        #         if rospy.is_shutdown():
        #             return
        #
        #         # Act in environment
        #         tot_rewards += self.play(epoch)
        #
        #         # Learn from replays
        #         qlosses, pi_losses = 0., 0.
        #         pbar = tqdm(total=self.updates_per_interval, file=sys.stdout)
        #         for ui in range(1,self.updates_per_interval+1):
        #             if rospy.is_shutdown():
        #                 return
        #             qloss, pi_loss = self.update_weights(epoch)
        #             qlosses += qloss
        #             pi_losses += pi_loss
        #             desc = f'  Learn loss=q{(qlosses/ui):.4f},p{(pi_losses/ui):.4f}'
        #             pbar.set_description(desc)
        #             pbar.update(1)
        #         pbar.close()
        #         self.qlosses.append(qlosses/self.updates_per_interval)
        #         self.pi_losses.append(pi_losses/self.updates_per_interval)
        #
        #     print('epoch:', epoch)
        #     self.save_model(epoch)
        #     if di % eval_iterv == 0:
        #         self.evaluate()

        self.cur_state = self.envir.reset()
        for epoch in range(self.start_epoch, self.end_epoch):

            tot_rewards = 0.0
            pbar = tqdm(total=interv, file=sys.stdout)
            for di in range(interv):
                if rospy.is_shutdown():
                    return

                # act in environment
                tot_rewards += self.play(epoch)

                # learn from replays
                qlosses, pi_losses = 0., 0.
                for _ in range(self.updates_per_interval):
                    if rospy.is_shutdown():
                        return
                    qloss, pi_loss = self.update_weights(epoch)
                    qlosses += qloss
                    pi_losses += pi_loss
                self.qlosses.append(qlosses/self.updates_per_interval)
                self.pi_losses.append(pi_losses/self.updates_per_interval)
                desc = f'  R {tot_rewards:.4f}: loss={np.mean(self.qlosses):.4f},{np.mean(self.pi_losses):.4f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()

            print('epoch:', epoch)
            self.save_model(epoch)
            if di % eval_iterv == 0:
                self.evaluate()


    def play(self, playi):
        # with torch.no_grad():
        #     self.model.eval()
        #     tot_rewards = 0.
        #     tot_terms = 0.000001
        #     state = self.envir.reset()
        #     pbar = tqdm(total=self.max_steps, file=sys.stdout)
        #     desc = f'  Play'
        #     pbar.set_description(desc)
        #     for step in range(self.max_steps):
        #         if rospy.is_shutdown():
        #             return tot_rewards
        #         actions, _ = self.model.policy(
        #           self.buffer.prep_state(state, self.device)
        #         )
        #         actions = actions.detach()
        #         nstate, rewards, terminals = self.envir.step_all(actions)
        #         tot_rewards += rewards.sum().item()
        #         tot_terms += terminals.sum().item()
        #         rewards *= 0
        #         terminals *= 0
        #         self.buffer.add_transitions(
        #           state, actions, nstate, rewards, terminals
        #         )
        #         state = nstate
        #         desc = f'  Play R: {(tot_rewards):.1f} {(tot_terms):.0f}'
        #         pbar.set_description(desc)
        #         pbar.update(1)
        #     pbar.close()
        #     return tot_rewards
        with torch.no_grad():
            self.model.eval()
            tot_rewards = 0.000001
            for step in range(self.acts_per_interval):
                if rospy.is_shutdown():
                    return tot_rewards
                actions, _ = self.model.policy(
                  self.buffer.prep_state(self.cur_state, self.device)
                )
                actions = actions.detach()
                nstate, rewards, terminals = self.envir.step_all(actions)
                tot_rewards += rewards.sum().item()
                rewards *= 0
                terminals *= 0
                self.buffer.add_transitions(
                  self.cur_state, actions, nstate, rewards, terminals
                )
                self.cur_state = nstate
            return tot_rewards


    def expert_demos(self, load_experts=True):

        if load_experts:
            self.buffer.load(
              osp.join(osp.expanduser(self.config['environ']['sim']['data_root']),
                self.config['environ']['sim']['data_prefix'])
            )
            print('loaded experts', self.buffer.experts)
            return

        demos = 0
        while demos < self.config['training']['expert_demos']:
            demos_left = self.config['training']['expert_demos'] - demos
            togain = min(demos_left, self.envir.num_envs)
            print('getting {} experts'.format(togain))
            tot_rewards = 0.
            tot_terms = 0.000001
            state = self.envir.reset()
            self.expert.reset()
            lengths = self.expert.lengths()
            toolong = torch.tensor([lengths[envi] >= self.max_steps for envi in range(self.envir.num_envs)])
            while any(toolong):
                print(toolong.sum())
                self.expert.reset_indices( toolong.to(torch.long) )
                lengths = self.expert.lengths()
                toolong = torch.tensor([lengths[envi] >= self.max_steps for envi in range(self.envir.num_envs)])
                self.envir.reset_indices( toolong )
                self.expert.reset_indices( toolong.to(torch.long) )
                lengths = self.expert.lengths()
                toolong = torch.tensor([lengths[envi] >= self.max_steps for envi in range(self.envir.num_envs)])
                import pdb; pdb.set_trace()

            keepers = torch.tensor([True]*self.envir.num_envs)
            if demos_left < self.envir.num_envs:
                keepers[demos_left:] = False
            pbar = tqdm(total=self.max_steps, file=sys.stdout)
            desc = f'  Expert'
            pbar.set_description(desc)
            for step in range(self.max_steps):
                if rospy.is_shutdown():
                    return

                base_acts, exp_resets = self.expert.get_actions()
                # TODO: right now it's getting the actual positions instead of difference of positions and the policy is incapable of learning that given the action space provided (also is probably a more difficult task)
                actions = self.envir.convert_action2tensor(base_acts)
                nstate, rewards, terminals = self.envir.step_all(base_acts)
                tot_rewards += rewards[keepers].sum().item()
                rewards = torch.ones_like(rewards)
                # do or operation on terminals with exp_resets
                terminals[exp_resets] = 1
                # TODO: make whether to ignore terminal condition from
                # goal/rewards optional
                terminals[[not exp for exp in exp_resets]] = 0
                tot_terms += terminals[keepers].sum().item()
                self.buffer.add_transitions(
                  state, actions, nstate, rewards, terminals, keepers
                )
                state = nstate
                keepers[terminals==1] = False
                if not any(keepers):
                    break
                desc = f'  Expert R: {(tot_rewards):.1f} {(tot_terms):.0f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            demos += togain
        self.buffer.experts = len(self.buffer)
        self.buffer.save(
          osp.join(osp.expanduser(self.config['environ']['sim']['data_root']),
            self.config['environ']['sim']['data_prefix'])
        )
        print('buffer:', len(self.buffer))
