
from isaacgym import gymapi
from isaacgym import gymutil

import numpy as np
import os
import random
import torch
from ll4ma_util import file_util

def main():
    config = file_util.load_yaml(
        'configs/franka_tasks/sac01.yaml'
    )
    env, _ = get_env(config['environ'])
    for i in range(10):
        env.step()

def get_env(config, rl_device='cuda'):
    env_configf = os.path.expanduser(config['env_config'])
    env_config = file_util.load_yaml(env_configf)
    env_config["env"]["asset"]["assetRoot"] = os.path.expanduser(
        "~/workspace/isaacgym/assets"  # '/home/iain/isaac_ws/src'
    )
    if 'num_envs' in config:
        env_config['env']['numEnvs'] = config['num_envs']
    if 'steps_per_action' not in config:
        config['steps_per_action'] = 1
    if 'max_env_steps' in config:
        env_config['env']['episodeLength'] = config['max_env_steps'] * config['steps_per_action']
        env_config['max_env_steps'] = config['max_env_steps']
    if 'actionScale' in config:
        env_config['env']['actionScale'] = config['actionScale']
    env_config['randomize'] = config.get('randomize', 0)

    for key in config:
        env_config[key] = config[key]

    env_config['physics_engine'] = 'physx'
    env_config['sim']['physics_engine'] = env_config['physics_engine']
    env_config['sim']['use_gpu_pipeline'] = config['use_gpu']
    env_config['sim']['physx']['use_gpu'] = config['use_gpu']
    env_config['sim']['physx']['num_threads'] = (
        1 if 'num_threads' not in config else config['num_threads']
    )
    env_config['sim']['physx']['num_subscenes'] = config['subscenes']
    env_config['sim']['physx']['solver_type'] = 1
    env_config['rl_device'] = rl_device

    device_type = "cuda" if config['use_gpu'] else "cpu"
    device_id = 0

    physics_engine = gymapi.SIM_PHYSX if env_config['physics_engine'] == "physx" else gymapi.SIM_GLEX
    task = _TASK_MAP[config['type']](
        cfg=env_config,
        sim_params=get_sim_params(env_config['sim']),
        physics_engine=physics_engine,
        device_type=device_type,
        device_id=device_id,
        headless=config['headless']
    )
    from pixmc.tasks.base.vec_task import VecTaskPython
    env = VecTaskPython(task, rl_device)

    return env, env_config

def get_sim_params(cfg):
    # previously args defaults
    args_use_gpu_pipeline = cfg['use_gpu_pipeline']
    args_use_gpu = cfg['physx']['use_gpu']
    args_subscenes = cfg['physx']['num_subscenes']  # 0
    args_slices = args_subscenes
    args_num_threads = 0

    # initialize sim
    sim_params = gymapi.SimParams()
    sim_params.dt = 1 / 60.
    sim_params.num_client_threads = args_slices

    assert cfg['physics_engine'] == "physx"
    sim_params.physx.solver_type = 1
    sim_params.physx.num_position_iterations = 4
    sim_params.physx.num_velocity_iterations = 0
    sim_params.physx.num_threads = 4
    sim_params.physx.use_gpu = args_use_gpu
    sim_params.physx.num_subscenes = args_subscenes
    sim_params.physx.max_gpu_contact_pairs = 8 * 1024 * 1024

    sim_params.use_gpu_pipeline = args_use_gpu_pipeline
    sim_params.physx.use_gpu = args_use_gpu

    # Override num_threads if specified
    if cfg['physics_engine'] == "physx" and args_num_threads > 0:
        sim_params['physx']['num_threads'] = args_num_threads

    return sim_params

if __name__ == '__main__':
    main()
