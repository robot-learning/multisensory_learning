import numpy as np
import trimesh
from trimesh.collision import CollisionManager
from multiprocessing import Process, Manager
import time


def check_collisions(idx, manager, m, n, return_dict):
    collisions = []
    for _ in range(n):
        collisions.append(manager.in_collision_single(
            m,
            np.array([[1, 0, 0, np.random.randn()],
                      [0, 1, 0, np.random.randn()],
                      [0, 0, 1, np.random.randn()],
                      [0, 0, 0, 1]])
        ))
    return_dict[idx] = collisions

if __name__ == '__main__':

    collision_manager = CollisionManager()

    m1 = trimesh.creation.box((0.1, 0.1, 0.1))
    m2 = trimesh.creation.box((0.2, 0.2, 0.2))

    collision_manager.add_object('m1', m1)
    collision_manager.add_object('m2', m2)

    start = time.time()

    proc_manager = Manager()
    return_dict = proc_manager.dict()
    ps = []
    for idx in range(16):
        p = Process(
            target=check_collisions,
            args=(idx, collision_manager, m2, 10000, return_dict)
        )
        p.start()
        ps.append(p)

    for p in ps:
        p.join()
    
        
    print("TIME", time.time() - start)

    n_vals = 0
    for v in return_dict.values():
        n_vals += len(v)
    
    print(f"Processed {n_vals} collisions")
    
