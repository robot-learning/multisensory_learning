#!/usr/bin/env python
import os.path as osp
import sys
import ast
import argparse
import numpy as np
from copy import deepcopy
from dataclasses import dataclass
from typing import List

from multisensory_learning.learning import msp_util
from multisensory_learning.models.loss import mdn_loss
from multisensory_learning.learning import Learner, LearnerConfig

from ll4ma_util import file_util, torch_util, func_util, ui_util

import torch
import torch.nn.functional as F


@dataclass
class GoalLearnerConfig(LearnerConfig):
    msp_checkpoint: str = ''
    train_regressor: bool = False
    train_mdn: bool = True
    train_classifier: bool = False
    regressor_hidden_sizes: List[int] = func_util.lambda_field([128, 128])
    mdn_n_components: int = 1
    mdn_hidden_sizes: List[int] = func_util.lambda_field([128, 128])
    classifier_hidden_sizes: List[int] = func_util.lambda_field([128, 128])
    multisensory_embedding_size: int = 64
    activation: str = 'relu'
    vec_norm: bool = 'layer'
    diag_only: bool = True
    diag_scalar: float = 1e-8


class GoalLearner(Learner):

    def __init__(self, config, filenames=[], set_train=False, set_eval=False):
        super().__init__(config, filenames, set_train, set_eval)

    def get_models(self):
        return msp_util.get_goal_models(self.config)

    def get_batch_size(self, batch):
        return batch['latent_in'].size(0)

    def get_regressor_checkpoint_filename(self, epoch, episode=None):
        prefix = "regressor" if episode is None else f"regressor_episode_{episode:03d}"
        filename = osp.join(self.config.checkpoint_dir, f"{prefix}_epoch_{epoch:03d}.pt")
        return filename
    
    def get_mdn_checkpoint_filename(self, epoch, episode=None):
        prefix = "mdn" if episode is None else f"mdn_episode_{episode:03d}"
        filename = osp.join(self.config.checkpoint_dir, f"{prefix}_epoch_{epoch:03d}.pt")
        return filename

    def get_classifier_checkpoint_filename(self, epoch, episode=None):
        prefix = "classifier" if episode is None else f"classifier_episode_{episode:03d}"
        filename = osp.join(self.config.checkpoint_dir, f"{prefix}_epoch_{epoch:03d}.pt")
        return filename
    
    def apply_models(self, batch):
        return msp_util.apply_goal_models(batch['latent_in'], batch['latent_target'],
                                          self.models, self.config)

    def apply_mdn(self, latent_in):
        """
        TODO integrate with apply_models? This is being used for things other than training
        """
        return self.models['mdn'](latent_in)

    def train_regressor(self):
        self.compute_loss = self.compute_regressor_loss
        self.get_checkpoint_filename = self.get_regressor_checkpoint_filename
        super().train()

    def train_mdn(self):
        self.compute_loss = self.compute_mdn_loss
        self.get_checkpoint_filename = self.get_mdn_checkpoint_filename
        super().train()

    def train_classifier(self):
        self.compute_loss = self.compute_classifier_loss
        self.get_checkpoint_filename = self.get_classifier_checkpoint_filename
        super().train()

    def compute_regressor_loss(self, batch, outputs):
        loss = F.mse_loss(outputs['regressor_out'], batch['latent_target']) 
        loss_dict = {'regressor': loss.item()}
        return loss, loss_dict
        
    def compute_mdn_loss(self, batch, outputs):
        loss = mdn_loss(batch['latent_target'], *outputs['mdn_out'], batch['goal_label'])
        loss_dict = {'mdn': loss.item()}
        return loss, loss_dict

    def compute_classifier_loss(self, batch, outputs):
        loss = F.cross_entropy(outputs['classifier_out'], batch['goal_label'].long())
        loss_dict = {'classifier': loss.item()}
        return loss, loss_dict
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--train_sample_dir', type=str, required=True)
    parser.add_argument('--val_sample_dir', type=str,
                        help=("Optional separate directory containing validation samples. "
                              "If not provided, will make validation samples from training data. "
                              "It's better to use samples from completely different trajs."))
    parser.add_argument('--include', type=str,
                        help="Only include sample filenames containing this string")
    parser.add_argument('--exclude', type=str,
                        help="Exlclude sample filenames containing this string")
    parser.add_argument('--checkpoint_dir', type=str, required=True)
    parser.add_argument('--batch_size', type=int)
    parser.add_argument('--n_epochs', type=int)
    parser.add_argument('--checkpoint_interval', type=int)
    parser.add_argument('--train_mdn', action='store_true')
    parser.add_argument('--no_train_mdn', dest='train_mdn', action='store_false')
    parser.add_argument('--train_classifier', action='store_true')
    parser.add_argument('--no_train_classifier', dest='train_classifier', action='store_false')
    parser.add_argument('--train_regressor', action='store_true')
    parser.add_argument('--no_train_regressor', dest='train_regressor', action='store_false')
    parser.add_argument('--params', type=str, default='{}',
                        help="Catch-all for other options to be set on GoalLearnerConfig")
    parser.set_defaults(train_mdn=True, train_classifier=False, train_regressor=False)
    args = parser.parse_args()

    if not args.train_mdn and not args.train_classifier:
        ui_util.print_error("Must enable at least one of MDN or classifier training")

    # Accumulate training samples from any directory with provided dir as prefix, this is
    # necessary since filesystem limitations prevented too many samples being saved in
    # one dir, so they'll be uniquely indexed versions of train_sample_dir
    train_filenames = []
    sample_dirs = [f for f in file_util.list_dir(osp.dirname(args.train_sample_dir))
                   if args.train_sample_dir in f]
    if len(sample_dirs) > 1:
        ui_util.print_warning("\nUsing multiple train sample dirs satisfying prefix\n")
    for sample_dir in sample_dirs:
        train_filenames += file_util.list_dir(sample_dir, '.pickle')
    if args.include:
        train_filenames = [f for f in train_filenames if args.include in f]
    if args.exclude:
        train_filenames = [f for f in train_filenames if args.exclude not in f]
    pos_train_filenames = [f for f in train_filenames if "not_goal" not in f]
    
    val_filenames = file_util.list_dir(args.val_sample_dir, '.pickle')
    if args.include:
        val_filenames = [f for f in val_filenames if args.include in f]
    if args.exclude:
        val_filenames = [f for f in val_filenames if args.exclude not in f]
    pos_val_filenames = [f for f in val_filenames if "not_goal" not in f]
        
    file_util.check_path_exists(args.train_sample_dir, "Train sample directory")
    metadata_filename = osp.join(args.train_sample_dir, "metadata.yaml")
    file_util.check_path_exists(metadata_filename)
    metadata = file_util.load_yaml(metadata_filename)
    if args.val_sample_dir:
        file_util.check_path_exists(args.val_sample_dir, "Validation sample directory")
    
    config = GoalLearnerConfig()
    config.msp_checkpoint = metadata['msp_checkpoint']
    func_util.override_dataclass_with_args(config, args) # Take anything set on args
    config.from_dict(ast.literal_eval(args.params))  # Take anything else provided in catch-all params

    # TODO this will be in metadata once data is created again
    msp_checkpoint = torch.load(metadata['msp_checkpoint'])
    config.multisensory_embedding_size = msp_checkpoint['args']['multisensory_embedding_size']
    
    file_util.safe_create_dir(config.checkpoint_dir)

    # Print config to catch any mistakes in settings
    def print_config(data, indent=''):
        if not isinstance(data, dict):
            raise ValueError("Something is wrong, was expecting dictionary as input")
        for k, v in data.items():
            if isinstance(v, dict):
                print(indent + k)
                print_config(v, indent + '    ')
            else:
                print(f"{indent + k}: {v}")

    print("\n========================== CONFIG ==========================")
    config_dict = deepcopy(vars(config))
    print_config(config_dict, '  ')
    

    # TODO probably MDN and regressor training can be combined
    
    if args.train_regressor:
        config.train_regressor = True
        config.train_mdn = False
        config.train_classifier = False
        goal_learner = GoalLearner(config, pos_train_filenames, pos_val_filenames)
        print(f"\nTraining regressor for {config.n_epochs} epochs (checkpoint every "
              f"{config.checkpoint_interval})...")
        goal_learner.train_regressor()
    regressor_pt_name = goal_learner.last_saved_checkpoint if args.train_regressor else None
    
    # Train MDN separately since it's only trained on positive samples
    if args.train_mdn:
        config.train_regressor = False
        config.train_mdn = True
        config.train_classifier = False
        goal_learner = GoalLearner(config, pos_train_filenames, pos_val_filenames)
        print(f"\nTraining MDN for {config.n_epochs} epochs (checkpoint every "
              f"{config.checkpoint_interval})...")
        goal_learner.train_mdn()
    mdn_pt_name = goal_learner.last_saved_checkpoint if args.train_mdn else None

    if args.train_classifier:
        config.train_regressor = False
        config.train_mdn = False
        config.train_classifier = True
        goal_learner = GoalLearner(config, train_filenames, val_filenames)
        print(f"\nTraining classifier for {config.n_epochs} epochs (checkpoint "
              f"every {config.checkpoint_interval})...")
        goal_learner.train_classifier()
    class_pt_name = goal_learner.last_saved_checkpoint if args.train_classifier else None

    # Merge the checkpoints into one file
    goal_pt = {}
    if regressor_pt_name:
        regressor_pt = torch.load(regressor_pt_name)
        goal_pt.update(regressor_pt['models'])
    if mdn_pt_name:
        mdn_pt = torch.load(mdn_pt_name)
        goal_pt.update(mdn_pt['models'])
    if class_pt_name:
        class_pt = torch.load(class_pt_name)
        goal_pt.update(class_pt['models'])
    goal_pt_name = osp.join(args.checkpoint_dir, f"goal_models.pt")
    torch.save(goal_pt, goal_pt_name)

