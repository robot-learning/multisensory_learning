#!/usr/bin/env python
import isaacgym

import os
import os.path as osp
import sys
import numpy as np
import trimesh
import random
from tqdm import tqdm
from copy import deepcopy
from scipy.spatial.transform import Rotation

from ll4ma_isaacgym.core import (
    BehaviorConfig,
    MoveToPoseConfig,
    PushObjectConfig,
    SessionConfig,
    Simulator
)
from ll4ma_isaacgym.behaviors import PushObject, Behavior
from ll4ma_robots_description.scene_generators import ShelfGenerator

import rospy
import tf2_ros
from std_srvs.srv import Trigger, TriggerResponse
from std_msgs.msg import Float64MultiArray, MultiArrayDimension
from geometry_msgs.msg import PoseStamped, Point, Pose, TransformStamped, Quaternion
from visualization_msgs.msg import (
    InteractiveMarker,
    InteractiveMarkerControl,
    MarkerArray
)
from moveit_msgs.msg import DisplayRobotState
from interactive_markers.interactive_marker_server import InteractiveMarkerServer

from multisensory_learning.learning import SkillLearner, SkillLearnerConfig
from multisensory_learning.planning import SkillPlanner, GoalDistributionGenerator
from multisensory_learning.util import learn_util
from multisensory_learning.srv import (
    SetVisualizationParams,
    SetVisualizationParamsResponse,
    GetPlan,
    GetPlanRequest
)

from distribution_planning.distributions import DiracDelta, Gaussian
from distribution_planning.distributions.torch_distributions import GaussianMixtureModel

from ll4ma_util import file_util, ros_util, ui_util, torch_util, math_util, vis_util

import torch
from torch.distributions import MultivariateNormal, Categorical


# COLORS = ['darkviolet', 'darkorange', 'dodgerblue', 'red', 'yellow']
# COLORS = ['mediumseagreen', 'darkorange', 'dodgerblue', 'red', 'yellow']
COLORS = ['darkorange', 'dodgerblue', 'mediumseagreen', 'red', 'yellow']
SEQ_FRAME_COLORS = ['mediumseagreen', 'tomato', 'indigo']
SEQ_PRED_COLORS = ['magenta', 'cyan']

# TODO you should be able to get this from a MoveIt service request
IIWA_JOINT_NAMES = [f'iiwa_joint_{i}' for i in range(1, 8)]
REFLEX_JOINT_NAMES = [f'reflex_preshape_joint_{i}' for i in [1,2]] + \
    [f'reflex_proximal_joint_{i}' for i in [1,2,3]]
JOINT_NAMES = IIWA_JOINT_NAMES + REFLEX_JOINT_NAMES

REFLEX_LINK_NAMES = [f"reflex_{n}_link" for n in ['pad', 'shell']]
REFLEX_LINK_NAMES += [f"reflex_distal_link_{i}" for i in [1,2,3]]
REFLEX_LINK_NAMES += [f"reflex_distal_pad_link_{i}" for i in [1,2,3]]
REFLEX_LINK_NAMES += [f"reflex_flex_link_{i}" for i in [1,2,3]]
REFLEX_LINK_NAMES += [f"reflex_proximal_link_{i}" for i in [1,2,3]]
REFLEX_LINK_NAMES += [f"reflex_proximal_pad_link_{i}" for i in [1,2,3]]
REFLEX_LINK_NAMES += [f"reflex_swivel_link_{i}" for i in [1,2]]
IIWA_LINK_NAMES = [f"iiwa_link_{i}" for i in range(8)]
IIWA_LINK_NAMES += ["reflex2_extra_shell", "reflex2_to_iiwa_mount", "robot_table_link"]

REFLEX_MESH = "package://multisensory_learning/src/multisensory_learning/assets/reflex.stl"


class SkillLearnerVisualizer:

    def __init__(self):        
        push_cp = rospy.get_param("~push_cp", "")
        pick_place_cp = rospy.get_param("~pick_place_cp", "")
        if not push_cp and not pick_place_cp:
            rospy.logerr("No model checkpoints specified")
            return
        model_cps = {}
        if push_cp:
            file_util.check_path_exists(push_cp, "Push model checkpoint")
            model_cps['push'] = push_cp
        if pick_place_cp:
            file_util.check_path_exists(pick_place_cp, "Pick-place model checkpoint")
            model_cps['pick_place'] = pick_place_cp
        
        self.n_mdn_samples = rospy.get_param("~n_mdn_samples", 100)
        self.visualize_all_components = rospy.get_param("~visualize_all_components", True)
        self.modulate_push_height = rospy.get_param("~modulate_push_height", True)
        self.debug = rospy.get_param("~debug", False)
        self.mode = rospy.get_param("~mode", "model_pred")
        self.device = rospy.get_param("~device", "cpu")

        seed = rospy.get_param("~seed", 1)
        random.seed(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)
        
        self.pred_skill = list(model_cps.keys())[0]
        self.skill_params = None
        self._goal_mean = None
        self._goal_cov = None
        self._init_mean = None
        self._init_cov = None
        self.p0 = None

        # TODO hacking this
        cfg_path = osp.join(
            ros_util.get_path('multisensory_learning'),
            # "src/multisensory_learning/config/isaacgym/push_cleaner_3shelf.yaml"
            "src/multisensory_learning/config/isaacgym/push_cleaner_shelf.yaml"
        )
        gym_metadata = file_util.load_yaml(cfg_path)
        
        self.planner = SkillPlanner(model_cps, gym_metadata=gym_metadata)

        self.tf_broadcaster = tf2_ros.StaticTransformBroadcaster()
        self.tf_stmps = []

        self.pred = MarkerArray()
        self.samples = MarkerArray()
        self.env = MarkerArray()
        self.labels = MarkerArray()
        self.push = MarkerArray()
        self.init_mean = MarkerArray()
        self.init_samples = MarkerArray()
        self.goal_mean = MarkerArray()
        self.goal_samples = MarkerArray()
        max_seqs = 4  # TODO will be combinatorics n_steps * n_components
        self.seqs = [MarkerArray() for i in range(max_seqs)]
        self.seq_preds = [MarkerArray() for i in range(max_seqs)]
        self.push_samples = MarkerArray()        
        
        self.pred_pub = rospy.Publisher('/pred_obj_poses', MarkerArray, queue_size=1)
        self.samples_pub = rospy.Publisher('/sample_obj_poses', MarkerArray, queue_size=1)
        self.env_pub = rospy.Publisher('/environment', MarkerArray, queue_size=1)
        self.labels_pub = rospy.Publisher('/labels', MarkerArray, queue_size=1)
        self.push_pub = rospy.Publisher('/push', MarkerArray, queue_size=1)
        self.init_mean_pub = rospy.Publisher('/init_mean', MarkerArray, queue_size=1)
        self.init_samples_pub = rospy.Publisher('/init_samples', MarkerArray, queue_size=1)
        self.goal_mean_pub = rospy.Publisher("/goal_mean", MarkerArray, queue_size=1)
        self.goal_samples_pub = rospy.Publisher("/goal_samples", MarkerArray, queue_size=1)
        self.prepush_robot_pub = rospy.Publisher(
            '/prepush_robot_state', DisplayRobotState, queue_size=1)
        self.endpush_robot_pub = rospy.Publisher(
            '/endpush_robot_state', DisplayRobotState, queue_size=1)
        self.obj_pose_pub = rospy.Publisher("/obj_frame", PoseStamped, queue_size=1)
        self.seq_pubs = [rospy.Publisher(f"/skill_seq_{i}", MarkerArray, queue_size=1)
                         for i in range(max_seqs)]
        self.seq_pred_pubs = [rospy.Publisher(f"/skill_seq_preds_{i}", MarkerArray, queue_size=1)
                              for i in range(max_seqs)]
        self.push_samples_pub = rospy.Publisher("/push_samples", MarkerArray, queue_size=1)

        self.text_size = [0.04]*3
        self.metadata = self.planner.gym_metadata
        self.env.markers = self.planner.env_markers
        self.obj_mesh = self.planner.mesh_resource

        self.goal_pos_var_vals = np.linspace(1e-6, 0.01, 20 + 1)
        self.goal_pos_var_idxs = [0]*3
        
        self.goal_mean_pos = np.array([0.6, -0.2, 0.87216])
        self.goal_mean_quat = np.array([0, 0, 0, 1])
        self.goal_mean_pose = ros_util.array_to_pose(
            np.concatenate([self.goal_mean_pos, self.goal_mean_quat])
        )
        self.goal_imarker = ros_util.get_imarker_msg(
            self.goal_mean_pos,
            self.goal_mean_quat,
            shape='mesh',
            mesh_resource=self.obj_mesh,
            color="orange",
            imarker_name="goal_mean"
        )

        self.init_mean_pos = np.array([0.6, 0.2, 0.87216])
        self.init_mean_quat = np.array([0, 0, 0, 1])
        self.init_mean_pose = ros_util.array_to_pose(
            np.concatenate([self.init_mean_pos, self.init_mean_quat])
        )
        self.init_imarker = ros_util.get_imarker_msg(
            self.init_mean_pos,
            self.init_mean_quat,
            shape='mesh',
            mesh_resource=self.obj_mesh,
            color="white",
            imarker_name="init_mean"
        )

        self.push_start_pose = deepcopy(self.init_mean_pose)
        self.push_start_pose.position.y += 0.2
        self.push_end_pose = deepcopy(self.goal_mean_pose)
        self._recompute_push_poses()

        # TODO can mod this pose to be a good default
        self.pick_place_start_pose = deepcopy(self.init_mean_pose)
        self.pick_place_start_pose.position.z += 0.25
        self.pick_place_start_pose.orientation = Quaternion(-0.5, 0.5, 0.5, 0.5)
        self.pick_place_end_pose = deepcopy(self.pick_place_start_pose)
        self.pick_place_end_pose.position.y -= 0.4

        self.push_start_imarker = self._get_push_imarker(
            self.push_start_pose,
            "push_start",
            "gold"
        )
        self.push_end_imarker = self._get_push_imarker(
            self.push_end_pose,
            "push_end",
            "cornflowerblue",
            use_height=False
        )
        self.pick_place_start_imarker = self._get_pick_place_imarker(
            self.pick_place_start_pose,
            "pick_place_start",
            "seagreen",
        )
        self.pick_place_end_imarker = self._get_pick_place_imarker(
            self.pick_place_end_pose,
            "pick_place_end",
            "tomato"
        )

        self.push_imarker_server = InteractiveMarkerServer("push_controls")
        self.pick_place_imarker_server = InteractiveMarkerServer("pick_place_controls")
        self.plan_imarker_server = InteractiveMarkerServer("plan_controls")
        
        self.plan_imarker_server.insert(self.init_imarker, self.process_init_imarker_fb)
        if self.mode == 'model_pred':
            self.push_imarker_server.insert(self.push_start_imarker, self.process_push_start_fb)
            self.push_imarker_server.insert(self.push_end_imarker, self.process_push_end_fb)
            self.pick_place_imarker_server.insert(
                self.pick_place_start_imarker,
                self.process_pick_place_start_fb
            )
            self.pick_place_imarker_server.insert(
                self.pick_place_end_imarker,
                self.process_pick_place_end_fb
            )
        elif self.mode == 'planning':
            self.plan_imarker_server.insert(self.goal_imarker, self.process_goal_imarker_fb)
            pass
        else:
            raise ValueError(f"Unknown visualizer mode: {self.mode}")

        
        self.set_vis_params_srv = rospy.Service(
            "/skill_visualizer/set_visualization_params",
            SetVisualizationParams,
            self.set_vis_params
        )
        self.set_dist_srv = rospy.Service(
            "/skill_visualizer/set_distributions",
            Trigger,
            self.set_distributions
        )
        self.get_plan_srv = rospy.Service(
            "/skill_visualizer/get_plan",
            Trigger,
            self.get_plan
        )
        self.show_action_samples_srv = rospy.Service(
            "/skill_visualizer/show_action_samples",
            Trigger,
            self.show_action_samples
        )
        self.run_sim_srv = rospy.Service(
            "/skill_visualizer/run_simulator",
            Trigger,
            self.run_simulator
        )

    @property
    def nominal_learner(self):
        return self.planner.nominal_learner

    @property
    def ortho6d_learner(self):
        return self.nominal_learner.ortho6d_learner        
        
    def run(self):
        self.plan_imarker_server.applyChanges()
        if self.mode == 'model_pred':
            if self.nominal_learner.skill == 'push':
                self.push_imarker_server.applyChanges()
            if self.nominal_learner.skill == 'pick_place':
                self.pick_place_imarker_server.applyChanges()
        
        rate = rospy.Rate(100)
        rospy.loginfo("Skill learner visualization services are available")
        while not rospy.is_shutdown():
            if self.env.markers:
                self.env_pub.publish(self.env)
            if self.labels.markers:
                self.labels_pub.publish(self.labels)
            if self.pred.markers:
                self.pred_pub.publish(self.pred)
            if self.samples.markers:
                self.samples_pub.publish(self.samples)
            if self.push.markers:
                self.push_pub.publish(self.push)
            if self.init_mean.markers:
                self.init_mean_pub.publish(self.init_mean)
            if self.init_samples.markers:
                self.init_samples_pub.publish(self.init_samples)
            if self.goal_mean.markers:
                self.goal_mean_pub.publish(self.goal_mean)
            if self.goal_samples.markers:
                self.goal_samples_pub.publish(self.goal_samples)
            if self.push_samples.markers:
                self.push_samples_pub.publish(self.push_samples)
            for i, seq in enumerate(self.seqs):
                if seq.markers:
                    self.seq_pubs[i].publish(seq)
            for i, seq_preds in enumerate(self.seq_preds):
                if seq_preds.markers:
                    self.seq_pred_pubs[i].publish(seq_preds)
            for tf_stmp in self.tf_stmps:
                self.tf_broadcaster.sendTransform(tf_stmp)
            rate.sleep()
                
    def set_vis_params(self, req):
        # These are updating the goal distribution variance
        if self.goal_pos_var_idxs[0] != req.goal_posx_var_index:
            self.goal_pos_var_idxs[0] = req.goal_posx_var_index
        if self.goal_pos_var_idxs[1] != req.goal_posy_var_index:
            self.goal_pos_var_idxs[1] = req.goal_posy_var_index
        if self.goal_pos_var_idxs[2] != req.goal_posz_var_index:
            self.goal_pos_var_idxs[2] = req.goal_posz_var_index
        return SetVisualizationParamsResponse(success=True)

    def set_distributions(self, req):

        # # Compute goal mean/cov
        # self._goal_mean = learn_util.pose_to_vec(
        #     self.goal_mean_pos,
        #     self.goal_mean_quat,
        #     self.ortho6d_learner
        # ).squeeze().numpy()
        # self._goal_cov = learn_util.cov_from_planar_samples(
        #     self.goal_mean_quat,
        #     self.ortho6d_learner,
        #     min_angle=0.1,
        #     max_angle=0.1
        # )
        # B = self.planner.config.cem.n_samples
        # self.goal_distribution = GaussianMixtureModel(
        #     torch.ones(B, 1),
        #     torch.tensor(self._goal_mean).unsqueeze(0).unsqueeze(-1).repeat(B, 1, 1),
        #     torch.tensor(self._goal_cov).unsqueeze(0).unsqueeze(-1).repeat(B, 1, 1, 1)
        # )
        

        # Set the GMM goal distribution using the generator
        self._set_goal_gmm()

        # Compute init mean/cov
        self._init_mean = learn_util.pose_to_vec(
            self.init_mean_pos,
            self.init_mean_quat,
            self.ortho6d_learner
        ).squeeze().numpy()
        angle = np.pi
        self._init_cov = learn_util.cov_from_planar_samples(
            self.init_mean_quat,
            self.ortho6d_learner,
            min_angle=-angle,
            max_angle= angle
        )
        self.p0 = Gaussian(self._init_mean, self._init_cov)

        # Update goal distribution visualization
        if self.mode == 'planning':
            self.goal_mean.markers, self.goal_samples.markers, _, _ = self._get_mdn_markers(
                self.goal_distribution.alpha[:1],  # Slice to 1 to have batch dim equal 1
                self.goal_distribution.mu[:1],
                self.goal_distribution.sigma_tril[:1]
                # colors=['white']
            )

        # Update init distribution visualization
        self.init_mean.markers, self.init_samples.markers, _, _ = self._get_mdn_markers(
            torch.ones(1, 1),
            torch.tensor(self._init_mean).unsqueeze(0).unsqueeze(-1),
            torch.linalg.cholesky(torch.tensor(self._init_cov)).unsqueeze(0).unsqueeze(-1),
            colors=['white']
        )
        
        return TriggerResponse(success=True)

    def get_plan(self, req):
        # if self._goal_mean is None or self._goal_cov is None:
        #     rospy.logerr("Cannot get plan, goal distribution has not been set yet")
        #     return TriggerResponse(success=False)

        result = self.planner.get_plan(
            self.p0,
            self.goal_distribution,
            self.planner.problem.config.skeleton,
            self.planner.problem.config.goal_cost_type,
            planner=self.planner.config.solver_type
        )
        self.skill_params = self.planner.result_to_skill_params(result)

        rollout = self.planner.skill_seq.rollout(
            self.p0,
            params_list=self.skill_params,
            # n_samples_to_propagate=80
        )
        self._set_plan_seq_markers(rollout)

        return TriggerResponse(success=True)

    def show_action_samples(self, req):    
        sampler = self.planner.get_sampler()
        samples = sampler.sample(100).numpy()
        marker_id = 1
        self.push_samples.markers = []
        for sample in samples:
            start_xyz = sample[:3]
            end_xy = sample[3:]
            
            start_point = Point(*start_xyz)
            end_point = Point(end_xy[0], end_xy[1], start_point.z)

            self.push_samples.markers.append(
                ros_util.get_marker_msg(
                    scale=[0.025, 0.06, 0.06],
                    shape='arrow',
                    color='mediumseagreen',
                    points=[start_point, end_point],
                    marker_id=marker_id,
                    alpha=0.8
                )
            )
            marker_id += 1

        return TriggerResponse(success=True)

    def run_simulator(self, req):
        if self.skill_params is None:
            rospy.logerr("Cannot run simulator, skill params not set. Do 'Get Plan' first")
            return TriggerResponse(success=False)

        cfg_fn = self.metadata['config_filename']    
        cfg = SessionConfig(config_filename=cfg_fn)
        cfg.n_envs = 4
        cfg.sim.headless = False

        sim = Simulator(cfg)
        target_obj = 'cleaner'  # TODO
        sim.config.env.objects[target_obj].position = self.init_mean_pos
        sim.config.env.objects[target_obj].position_ranges = None
        sim.config.env.objects[target_obj].orientation = self.init_mean_quat
        sim.config.env.objects[target_obj].sample_angle_lower = None
        sim.config.env.objects[target_obj].sample_angle_upper = None
        sim.reset()

        behavior_cfg = BehaviorConfig()
        behavior_cfg.behaviors = [
            MoveToPoseConfig(config_dict={
                'name': 'approach_1',
                'end_effector_frame': 'reflex_palm_link',
            }),
            MoveToPoseConfig(config_dict={
                'name': 'push_1',
                'end_effector_frame': 'reflex_palm_link',
                'cartesian_path': True,
                'disable_collisions': [target_obj]
            })
        ]
        
        # TODO this will be hacked
        start_ee_pose_1, end_ee_pose_1 = self._get_ee_poses_from_params(self.skill_params[0])
        # start_ee_pose_2, end_ee_pose_2 = self._get_ee_poses_from_params(self.skill_params[1])

        trajs = []
        for env_idx in range(cfg.n_envs):
            behavior = Behavior(behavior_cfg, sim.robot, sim.config.env, sim, open_loop=True)
            behavior.behaviors['approach_1'].set_target_pose(start_ee_pose_1)
            behavior.behaviors['push_1'].set_target_pose(end_ee_pose_1)
            traj, labels = behavior.get_trajectory(sim.get_env_state(env_idx))
            trajs.append(traj)

        # TODO need to do this properly to get data
        for i in range(500):
            
            # TODO can test this by applying the actions from the trajs


            
            sim.step()


        sim.destroy_sim()
        
        
        return TriggerResponse(success=True)
    
    
    def process_goal_imarker_fb(self, fb):
        self.goal_mean_pose = fb.pose
        self.goal_mean_pos = np.array([
            fb.pose.position.x,
            fb.pose.position.y,
            fb.pose.position.z
        ])
        self.goal_mean_quat = np.array([
            fb.pose.orientation.x,
            fb.pose.orientation.y,
            fb.pose.orientation.z,
            fb.pose.orientation.w
        ])

    def process_init_imarker_fb(self, fb):
        self.init_mean_pose = fb.pose
        self.init_mean_pos = np.array([
            fb.pose.position.x,
            fb.pose.position.y,
            fb.pose.position.z
        ])
        self.init_mean_quat = np.array([
            fb.pose.orientation.x,
            fb.pose.orientation.y,
            fb.pose.orientation.z,
            fb.pose.orientation.w
        ])
        if self.mode == 'model_pred':
            self._update_rviz_with_current_settings()

    def process_push_start_fb(self, fb):
        self.push_start_pose = fb.pose
        self.push_end_pose.position.z = fb.pose.position.z  # End height varies along with start
        self._recompute_push_poses()
        self.push_imarker_server.setPose('push_start', self.push_start_pose)
        self.push_imarker_server.setPose('push_end', self.push_end_pose)
        self.push_imarker_server.applyChanges()
        if self.mode == 'model_pred':
            self.pred_skill = 'push'
            self._update_rviz_with_current_settings()

    def process_push_end_fb(self, fb):
        self.push_end_pose = fb.pose
        self._recompute_push_poses()
        self.push_imarker_server.setPose('push_start', self.push_start_pose)
        self.push_imarker_server.setPose('push_end', self.push_end_pose)
        self.push_imarker_server.applyChanges()
        if self.mode == 'model_pred':
            self.pred_skill = 'push'
            self._update_rviz_with_current_settings()

    def process_pick_place_start_fb(self, fb):
        self.pick_place_start_pose = fb.pose
        if self.mode == 'model_pred':
            self.pred_skill = 'pick_place'
            self._update_rviz_with_current_settings()

    def process_pick_place_end_fb(self, fb):
        self.pick_place_end_pose = fb.pose
        if self.mode == 'model_pred':
            self.pred_skill = 'pick_place'
            self._update_rviz_with_current_settings()

    def _recompute_push_poses(self):
        """
        Need to recompute orientations each time start/end positions are updated.
        """
        push_start_pos = np.array([
            self.push_start_pose.position.x,
            self.push_start_pose.position.y,
            self.push_start_pose.position.z
        ])
        push_end_pos = np.array([
            self.push_end_pose.position.x,
            self.push_end_pose.position.y,
            self.push_end_pose.position.z
        ])
        push = push_end_pos - push_start_pos
        push_distance = np.linalg.norm(push)
        push_direction = push / push_distance
        w_R_ee = math_util.construct_rotation_matrix(  # This is same as push pose gen
            x=push_direction,
            y=np.array([0, 0, 1]),
            normalize=True
        )
        quat = math_util.rotation_to_quat(w_R_ee)
        self.push_start_pose.orientation = Quaternion(*quat)
        self.push_end_pose.orientation = Quaternion(*quat)

    def _get_ee_poses_from_params(self, params):
        params = params.squeeze()
        ee_z = params[:1]
        ee_start_xy = params[1:3]
        ee_end_xy = params[3:]
        push_start_pos = np.concatenate([ee_start_xy, ee_z])
        push_end_pos = np.concatenate([ee_end_xy, ee_z])
        push = push_end_pos - push_start_pos
        push_distance = np.linalg.norm(push)
        push_direction = push / push_distance
        w_R_ee = math_util.construct_rotation_matrix(  # This is same as push pose gen
            x=push_direction,
            y=np.array([0, 0, 1]),
            normalize=False
        )
        w_T_ee_init = math_util.pose_to_homogeneous(push_start_pos, R=w_R_ee)
        w_T_ee_end = w_T_ee_init.copy()
        w_T_ee_end[:3,3] += push
        ee_start_pos, ee_start_quat = math_util.homogeneous_to_pose(w_T_ee_init)
        ee_start_pose = np.concatenate([ee_start_pos, ee_start_quat])
        ee_end_pos, ee_end_quat = math_util.homogeneous_to_pose(w_T_ee_end)
        ee_end_pose = np.concatenate([ee_end_pos, ee_end_quat])
        return ee_start_pose, ee_end_pose
        
    def _get_model_outputs(self, inputs):
        with torch.no_grad():
            outputs = self.planner.learners[self.pred_skill].apply_models(inputs)
            if 'regressor' in outputs:
                outputs['pred_R'] = torch_util.ortho6d_to_rotation(outputs['regressor'][:,3:])
        return inputs, outputs

    def _update_rviz_with_current_settings(self):
        if self.pred_skill not in ['push', 'pick_place']:
            rospy.logerr(f"Unknown skill type for predictions: {self.pred_skill}")
            return
        if self.p0 is None:
            rospy.logwarn("Initial distribution is not set. Press 'Set Distributions' button")
            return 

        if self.pred_skill == 'push':
            start_pos = ros_util.pose_to_position(self.push_start_pose)
            end_pos = ros_util.pose_to_position(self.push_end_pose)
            params = np.concatenate([start_pos, end_pos[:2]])
            # TODO can tune these values to balance the quality of the distribution returned,
            # where more samples should give a better estimate of how the distribution will
            # look, which will give better PDF estimates, and in turn affect the filtered
            # samples, where lower filter_pct will keep fewer samples but they will be good
            # samples, and higher pct will keep more samples but they will get increasingly
            # lower PDF samples the more you add.
            n_input_samples = 100
            filter_pct = 0.
            samples, mix_idxs, _, _ = self.planner.get_learner('push').propagate_distribution(
                params,
                distribution=self.p0,
                # propagation_type='mean'
                propagation_type='samples',
                n_input_samples=n_input_samples,
                filter_to_topk=int(filter_pct * n_input_samples)
            )            
        elif self.pred_skill == 'pick_place':
            # TODO this is going to be hacked until I figure out the sampling params
            start_pos = ros_util.pose_to_position(self.pick_place_start_pose)
            start_rot = ros_util.pose_to_rotation(self.pick_place_start_pose).flatten()
            end_pos = ros_util.pose_to_position(self.pick_place_end_pose)
            end_rot = ros_util.pose_to_rotation(self.pick_place_end_pose).flatten()
            params = np.concatenate([start_pos, start_rot, end_pos, end_rot])
            samples, mix_idxs = self.planner.get_learner('push').propagate_distribution(
                params,
                distribution=self.p0,
                propagation_type='mean'
            )
        else:
            raise ValueError(f"Unknown skill type for visualization: {pick_place}")
        
        marker_id = 1
        self.samples.markers = []
        for i, sample in enumerate(samples):
            pos, quat = self._get_pos_quat(sample)
            self.samples.markers.append(
                ros_util.get_marker_msg(
                    pos,
                    quat,
                    shape='mesh',
                    color=COLORS[mix_idxs[i]],
                    # color=cmap(math_util.min_max_scale(pdfs[i].item(), min_pdf, max_pdf, 0, 1)),
                    mesh_resource=self.obj_mesh,
                    alpha=0.9,
                    marker_id=marker_id
                )
            )
            marker_id += 1


        
        # pred_markers, sample_markers, _, _ = self._get_mdn_markers(
        #     gmm.alpha,
        #     gmm.mu,
        #     gmm.sigma_tril,
        #     return_components=self.visualize_all_components
        # )
        # self.pred.markers = pred_markers
        # self.samples.markers = sample_markers


        
        
        # w_T_obj = math_util.pose_to_homogeneous(self.init_mean_pos, self.init_mean_quat)
        # w_R_obj = math_util.homogeneous_to_rotation(w_T_obj)
        # obj_T_w = math_util.homogeneous_inverse(w_T_obj)

        # if self.pred_skill == 'push':
        #     w_T_ee_init = ros_util.pose_to_homogeneous(self.push_start_pose)
        #     w_T_ee_end = ros_util.pose_to_homogeneous(self.push_end_pose)
        # elif self.pred_skill == 'pick_place':
        #     w_T_ee_init = ros_util.pose_to_homogeneous(self.pick_place_start_pose)
        #     w_T_ee_end = ros_util.pose_to_homogeneous(self.pick_place_end_pose)

        # obj_T_ee_init = obj_T_w @ w_T_ee_init
        # obj_T_ee_end = obj_T_w @ w_T_ee_end

        # inputs = {
        #     # Model inputs:
        #     'ee_start_pos_in_obj': torch.tensor(
        #         math_util.homogeneous_to_position(obj_T_ee_init)
        #     ).unsqueeze(0),
        #     'ee_start_rot_in_obj': torch.tensor(
        #         math_util.homogeneous_to_rotation(obj_T_ee_init)
        #     ).flatten().unsqueeze(0),
        #     'ee_end_pos_in_obj': torch.tensor(
        #         math_util.homogeneous_to_position(obj_T_ee_end)
        #     ).unsqueeze(0),
        #     'ee_end_rot_in_obj': torch.tensor(
        #         math_util.homogeneous_to_rotation(obj_T_ee_end)
        #     ).flatten().unsqueeze(0),
        #     # Needed for visualization:
        #     'obj_pos_init': torch.tensor(
        #         math_util.homogeneous_to_position(w_T_obj)
        #     ).unsqueeze(0),
        #     'obj_rot_init': torch.tensor(
        #         math_util.homogeneous_to_rotation(w_T_obj)
        #     ).flatten().unsqueeze(0)
        # }
        # _, outputs = self._get_model_outputs(inputs)
        # self._update_markers(inputs, outputs)

    def _update_markers(self, inputs={}, outputs={}):
        self.pred.markers = []
        self.samples.markers = []
        # self.env.markers = []
        self.labels.markers = []
        self.push.markers = []
        self.goal_mean.markers = []
        self.goal_samples.markers = []
        
        sample = {k: v.squeeze() for k, v in inputs.items()}
        self.labels.markers = []

        start_point = Point(
            self.push_start_pose.position.x,
            self.push_start_pose.position.y,
            self.push_start_pose.position.z
        )
        end_point = Point(
            self.push_end_pose.position.x,
            self.push_end_pose.position.y,
            self.push_end_pose.position.z
        )
        
        self.push.markers = [
            ros_util.get_marker_msg(
                scale=[0.025, 0.06, 0.06],
                shape='arrow',
                color='gold',
                points=[start_point, end_point]
            )
        ]
                
        if 'regressor' in outputs:
            pos, quat = self._get_pos_quat(outputs['regressor'], inputs)
            self.pred.markers = [
                ros_util.get_marker_msg(
                    pos,
                    quat,
                    shape='mesh',
                    mesh_resource=self.obj_mesh,
                    color='purple',
                    alpha=0.99
                )
            ]
            # self.labels.markers.append(
            #     ros_util.get_marker_msg(
            #         pos_pred,
            #         scale=self.text_size,
            #         shape='text',
            #         text='pred',
            #         marker_id=3,
            #         color='white'
            #     )
            # )
        elif 'mdn' in outputs:
            alpha, mu, sigma_tril = outputs['mdn']
            pred_markers, sample_markers, mdn_labels, _ = self._get_mdn_markers(
                alpha,
                mu,
                sigma_tril,
                inputs,
                self.visualize_all_components
            )
            self.pred.markers = pred_markers
            self.samples.markers = sample_markers
            self.labels.markers += mdn_labels

    def _get_skill_markers(self, inputs, outputs):
        alpha, mu, sigma_tril = outputs['mdn']
        mean_markers, sample_markers, label_markers, _ = self._get_mdn_markers(
            alpha,
            mu,
            sigma_tril,
            inputs
        )

        # TODO need to set push arrow markers
        markers = mean_markers + sample_markers + label_markers
        return markers
            
    def _get_mdn_markers(
            self,
            alpha,
            mu,
            sigma_tril,
            inputs=None,
            return_components=True,
            colors=COLORS,
            start_marker_id=0
    ):
        mean_markers = []
        sample_markers = []
        label_markers = []

        marker_id = start_marker_id
        if return_components:
            alpha_str = ""
            for component in range(alpha.size(-1)):
                alpha_str += f"C{component+1}: {alpha[0, component].item():.2f} "
            label_markers.append(
                ros_util.get_marker_msg(
                    [0., 0., 1.],
                    scale=[0.09]*3,
                    shape='text',
                    text=alpha_str,
                    marker_id=marker_id,
                    color='white'
                )
            )
            marker_id += 1

            for component in range(mu.size(-1)):
                mvn = MultivariateNormal(
                    mu[:,:,component],
                    scale_tril=sigma_tril[:,:,:,component]
                )
                mean_pos, mean_quat = self._get_pos_quat(mvn.loc, inputs)
                mean_markers.append(
                    ros_util.get_marker_msg(
                        mean_pos,
                        mean_quat,
                        shape='mesh',
                        mesh_resource=self.obj_mesh,
                        color=colors[component % len(colors)],
                        marker_id=marker_id,
                        alpha=0.99
                    )
                )
                marker_id += 1
                label_markers.append(
                    ros_util.get_marker_msg(
                        mean_pos,
                        scale=[0.05]*3,
                        shape='text',
                        text=f"C{component+1}",
                        marker_id=marker_id,
                        color=colors[component % len(colors)]
                    )
                )
                marker_id += 1
                samples = mvn.sample((self.n_mdn_samples,)).squeeze(1)
                for i, s in enumerate(samples):
                    s_pos, s_quat = self._get_pos_quat(s, inputs)
                    sample_markers.append(
                        ros_util.get_marker_msg(
                            s_pos,
                            s_quat,
                            shape='mesh',
                            color=colors[component % len(colors)],
                            mesh_resource=self.obj_mesh,
                            alpha=0.2,
                            marker_id=marker_id
                        )
                    )
                    marker_id += 1
        else:
            mvns = []
            for component in range(mu.size(-1)):
                mvns.append(
                    MultivariateNormal(
                        mu[:,:,component].unsqueeze(0),
                        scale_tril=sigma_tril[:,:,:,component].unsqueeze(0)
                    )
                )
    
            # Generate samples in proportion to mixture weights
            mix = Categorical(alpha)
            samples = []
            for i in range(self.n_mdn_samples):
                idx = mix.sample().item()
                s = mvns[idx].sample().squeeze()
                s_pos, s_quat = self._get_pos_quat(s, inputs)
                sample_markers.append(
                    ros_util.get_marker_msg(
                        s_pos,
                        s_quat,
                        shape='mesh',
                        color=colors[idx],
                        mesh_resource=self.obj_mesh,
                        alpha=0.4,
                        marker_id=marker_id
                    )
                )
                marker_id += 1
                
        return mean_markers, sample_markers, label_markers, marker_id

    def _get_pos_quat(self, pos_ortho6d, inputs=None):
        if inputs is not None:
            # This should be from model pred, so doing delta on top of init
            if 'obj_pos_init' not in inputs or 'obj_rot_init' not in inputs:
                raise ValueError("Couldn't find init obj pose in inputs for delta pred")
            w_T_obj = math_util.pose_to_homogeneous(
                inputs['obj_pos_init'].cpu().numpy().squeeze(),
                R=inputs['obj_rot_init'].cpu().numpy().squeeze().reshape(3,3)
            )
            obj_T_pred = math_util.pose_to_homogeneous(
                pos_ortho6d.squeeze()[:3].cpu().numpy(),
                R=torch_util.ortho6d_to_rotation(
                    pos_ortho6d.squeeze()[3:].unsqueeze(0)
                ).squeeze().numpy()
            )
            w_T_pred = w_T_obj @ obj_T_pred
            pos = math_util.homogeneous_to_position(w_T_pred)
            rot = math_util.homogeneous_to_rotation(w_T_pred)
            quat = math_util.rotation_to_quat(rot, True)  # This is so you get the normalization
        else:
            pos = pos_ortho6d.squeeze()[:3].cpu().numpy()
            R = torch_util.ortho6d_to_rotation(pos_ortho6d.squeeze()[3:].unsqueeze(0))
            quat = math_util.rotation_to_quat(R.squeeze().cpu().numpy(), True)
        return pos, quat

    def _set_goal_gmm(self):
        # TODO copying this from script to get working quickly

        goal_cfg = self.planner.config.goal_distribution
        shelf, shelf_meshes = ShelfGenerator.create_mesh(
            width=goal_cfg.shelf_W,
            height=goal_cfg.shelf_H,
            depth=goal_cfg.shelf_D,
            thickness=0.02,
            n_shelves=goal_cfg.n_shelves,
            exclude_bottom_wall=True,
            return_component_meshes=True
        )
        table = trimesh.creation.box((goal_cfg.table_D, goal_cfg.table_W, goal_cfg.table_H))

        goal_gen = GoalDistributionGenerator(
            self.ortho6d_learner.config.checkpoint,
            obj_mesh_fn=os.path.join(ros_util.get_path('ll4ma_isaacgym'),
                                     "src", "ll4ma_isaacgym", "assets", "cleaner.stl")
        )
        goal_gen.add_env_object(
            "table",
            table,
            transform=np.array([
                [1, 0, 0,  0.7991],
                [0, 1, 0, -0.1892],
                [0, 0, 1, goal_cfg.table_H/2.],
                [0, 0, 0, 1]
            ]),
            color=vis_util.get_color('darkslategrey'),
        )
        shelf_part_names = []
        shelf_tf = np.eye(4)
        shelf_tf[:3,3] = [0.7, -0.5, goal_cfg.table_H + goal_cfg.shelf_H/2.]
        shelf_tf[:3,:3] = Rotation.from_euler('Z', 90, degrees=True).as_matrix()
        for i, m in enumerate(shelf_meshes):
            part_name = f"shelf_part_{i}"
            goal_gen.add_env_object(
                part_name,
                m,
                transform=shelf_tf,
                color=vis_util.get_color('tan', alpha=1)
            )
            shelf_part_names.append(part_name)

        H_buffer = 0.001  # Add mm buffer for collision check, stable poses are touching surface
        diff = goal_cfg.shelf_H / (goal_cfg.n_shelves + 1) + H_buffer
        target_height = goal_cfg.table_H + goal_cfg.target_shelf * diff
        gmm = goal_gen.create_distribution(
            shelf,
            # TODO could get xy pos from YAML config for shelf task
            nominal_T=np.array([
                [1, 0, 0,  0.7],
                [0, 1, 0, -0.5],
                [0, 0, 1, target_height],
                [0, 0, 0, 1]
            ]),
            n_support_surfaces=goal_cfg.n_components,
            variance=0.2,
            n_samples=100,
            reject_collisions=shelf_part_names,
            n_gmm_components=goal_cfg.n_components,
            # show=True
        )

        B = self.planner.config.cem.n_samples
        alpha = torch.tensor(gmm.weights_).unsqueeze(0).repeat(B, 1)  # (B, K)
        mu = torch.tensor(gmm.means_).unsqueeze(0).repeat(B, 1, 1).permute(0, 2, 1)
        sigma = torch.tensor(gmm.covariances_).unsqueeze(0).repeat(B, 1, 1, 1).permute(0, 2, 3, 1)

        # # TODO testing with one component
        # component = 2
        # alpha = alpha[:,component].unsqueeze(-1)
        # mu = mu[:,:,component].unsqueeze(-1)
        # sigma = sigma[:,:,:,component].unsqueeze(-1)
        self.goal_distribution = GaussianMixtureModel(alpha, mu, sigma)
    
    def _set_plan_seq_markers(self, rollout):
        for ma in self.seqs:
            ma.markers = []
        for ma in self.seq_preds:
            ma.markers = []
        self.tf_stmps = [
            ros_util.get_tf_stamped_msg(
                self.init_mean_pos,
                self.init_mean_quat,
                'world',
                'obj0'
            )
        ]

        marker_id = 0
        for step_idx, node in enumerate(rollout.nodes):
            ps, Rs = learn_util.vec_to_pos_rot(node.samples)

            # for sample_idx in range(len(node.samples)):
            #     self.seqs[0].markers.append(
            #         ros_util.get_marker_msg(
            #             ps[sample_idx].numpy(),
            #             math_util.rotation_to_quat(Rs[sample_idx].numpy(), True),
            #             shape='mesh',
            #             mesh_resource=self.obj_mesh,
            #             color=SEQ_FRAME_COLORS[step_idx],
            #             marker_id=marker_id,
            #             alpha=0.2
            #         )
            #     )
            #     marker_id += 1

            # Show push action
            start_xyz = node.params.squeeze()[:3].numpy()
            end_xy = node.params.squeeze()[3:].numpy()
            start_point = Point(*start_xyz)
            end_point = deepcopy(start_point)
            end_point.x = end_xy[0]
            end_point.y = end_xy[1]
            self.seqs[0].markers.append(
                ros_util.get_marker_msg(
                    scale=[0.025, 0.06, 0.06],
                    shape='arrow',
                    color=SEQ_FRAME_COLORS[step_idx],
                    points=[start_point, end_point],
                    marker_id=marker_id
                )
            )
            marker_id += 1
    
    def _get_push_imarker(
            self,
            pose,
            name,
            color='indigo',
            desc='',
            use_height=True
    ):
        imarker = ros_util.get_imarker_msg(
            pose=pose,
            color=color,
            shape='mesh',
            mesh_resource=REFLEX_MESH,
            imarker_scale=0.2,
            imarker_name=name,
            imarker_desc=desc,
            imarker_move_y=use_height, # Note y-axis goes up/down because EE pose is different
            imarker_rotate_x=False,
            imarker_rotate_y=False,
            imarker_rotate_z=False
        )
        return imarker

    def _get_pick_place_imarker(
            self,
            pose,
            name,
            color='indigo',
            desc='',
    ):
        imarker = ros_util.get_imarker_msg(
            pose=pose,
            color=color,
            shape='mesh',
            mesh_resource=REFLEX_MESH,
            imarker_scale=0.2,
            imarker_name=name,
            imarker_desc=desc
        )
        return imarker
        
            
if __name__ == '__main__':
    rospy.init_node('visualize_pose_learner')

    vis = SkillLearnerVisualizer()
    vis.run()
