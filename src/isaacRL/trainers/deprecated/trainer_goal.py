
from isaacRL.tasks.expert_behaviors import Expert
from isaacRL.trainers.trainer_sacbase import TrainerSACBase
from isaacRL.utils.buffer import BufferHER

import os, sys, argparse, json
import os.path as osp
import numpy as np
from tqdm import tqdm
import rospy

import torch

class TrainerGoal(TrainerSACBase):
    """docstring for TrainerGoal."""

    def __init__(self, config):
        super(TrainerGoal, self).__init__(config)

        self.goal_samples = self.config['training']['goal_samples']

        assert self.config['environ']['type'] == 'arm'

        self.buffer = BufferHER( self.config['training']['max_buff_size'],
          self.max_steps, self.config['environ']['prev_states']
        )

    def train(self):

        if self.config['training']['expert_demos'] > 0:
            self.expert_demos(self.config['training']['load_experts'])

        interv = max( self.config['training']['evaluation_interval'],
                        self.config['training']['checkpoint_interval']
        )

        self.cur_state = self.envir.reset()
        for epoch in range(self.start_epoch, self.end_epoch):

            pbar = tqdm(total=interv, file=sys.stdout)
            for di in range(interv):
                if rospy.is_shutdown():
                    return

                if epoch > self.config['training']['expert_epochs']:
                    # act in environment
                    self.play(epoch)
                    while len(self.buffer) < self.config['training']['batch_size']:
                        self.play(epoch)

                # learn from replays
                qlosses, pi_losses = 0., 0.
                for _ in range(self.updates_per_interval):
                    qloss, pi_loss = self.update_weights(epoch)
                    qlosses += qloss
                    pi_losses += pi_loss
                self.qlosses.append(qlosses/self.updates_per_interval)
                self.pi_losses.append(pi_losses/self.updates_per_interval)
                desc = f'  Iter {di}: loss={np.mean(self.qlosses):.4f},{np.mean(self.pi_losses):.4f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()

            print('epoch:', epoch)
            self.save_model(epoch)
            self.evaluate()


    def play(self, playi):
        with torch.no_grad():
            self.model.eval()
            for step in range(self.acts_per_interval):
                if rospy.is_shutdown():
                    return
                actions, _ = self.model.policy(
                  self.buffer.prep_state(self.cur_state, self.device)
                )
                actions = actions.detach()
                nstate, rewards, terminals = self.envir.step_all(actions)
                self.buffer.add_transitions(
                  self.cur_state, actions, nstate, rewards, terminals
                )
                self.cur_state = nstate


    def evaluate(self):
        print('evaluation')
        tot_rewards = 0.0
        qvals = []
        with torch.no_grad():
            self.model.eval()
            state = self.envir.reset()
            pbar = tqdm(total=self.max_steps, file=sys.stdout)
            desc = f'  Evaluate'
            pbar.set_description(desc)
            for step in range(self.max_steps):
                if rospy.is_shutdown():
                    return
                actions, _ = self.model.policy(
                  self.buffer.prep_state(state, self.device) )
                actions = actions.detach()
                qval1, qval2 = self.model.critic(
                  self.buffer.prep_state(state, self.device), actions )
                qval = torch.min(qval1, qval2).cpu()
                qvals.append( qval.squeeze().tolist() )
                nstate, rewards, terminals = self.envir.step_all(actions)
                state = nstate
                tot_rewards += rewards.mean().item()
                desc = f'  Evaluate rewards: {tot_rewards:.4f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
        print('rewards tot:{}, mean:{}'.format(tot_rewards, tot_rewards/self.max_steps))
        self.cur_state = self.envir.reset()
        print('eval done')
        return qvals


    def expert_demos(self, load_experts=True):
        demos = 0

        if load_experts:
            self.buffer.load(
              osp.join(osp.expanduser(self.config['environ']['sim']['data_root']),
                self.config['environ']['sim']['data_prefix'])
            )
            print('loaded experts')
            return

        expert = Expert(self.envir)
        state = self.envir.reset()
        while demos < self.config['training']['expert_demos'] and \
                not rospy.is_shutdown():

            actions = expert.get_actions()
            # if not (actions.abs().sum(1)>0).any():
            #     continue
            nstate, rewards, terminals = self.envir.step_all(actions)
            self.buffer.add_transitions(
              state, actions, nstate, rewards, terminals
            )
            state = nstate
            if terminals.sum() > 0:
                print('completed demo')
                demos += terminals.sum()
        assert self.buffer._buff['terminals'].sum() == demos
        print('got experts')
        self.buffer.save(
          osp.join(osp.expanduser(self.config['environ']['sim']['data_root']),
            self.config['environ']['sim']['data_prefix'])
        )
