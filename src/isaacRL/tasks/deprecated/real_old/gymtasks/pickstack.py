
import numpy as np
import os
import torch

from isaacgymenvs.utils.torch_jit_utils import *
from isaacgymenvs.tasks.base.base_task import BaseTask
from isaacgym import gymtorch
from isaacgym import gymapi


class PickStack(BaseTask):

    def __init__(self, cfg, sim_params, physics_engine, device_type, device_id, headless):
        self.cfg = cfg
        self.sim_params = sim_params
        self.physics_engine = physics_engine

        self.max_episode_length = self.cfg["env"]["episodeLength"]

        self.action_scale = self.cfg["env"]["actionScale"]
        self.start_position_noise = self.cfg["env"]["startPositionNoise"]
        self.start_rotation_noise = self.cfg["env"]["startRotationNoise"]
        self.aggregate_mode = self.cfg["env"]["aggregateMode"]

        self.dof_vel_scale = self.cfg["env"]["dofVelocityScale"]
        self.dist_reward_scale = self.cfg["env"]["distRewardScale"]
        self.rot_reward_scale = self.cfg["env"]["rotRewardScale"]
        self.around_handle_reward_scale = self.cfg["env"]["aroundHandleRewardScale"]
        self.stack_reward_scale = self.cfg["env"]["stackRewardScale"]
        self.finger_dist_reward_scale = self.cfg["env"]["fingerDistRewardScale"]
        self.action_penalty_scale = self.cfg["env"]["actionPenaltyScale"]

        self.debug_viz = self.cfg["env"]["enableDebugVis"]

        self.up_axis = "z"
        self.up_axis_idx = 2

        self.distX_offset = 0.04
        self.dt = 1/60.

        self.cfg["env"]["numObservations"] = 23
        self.cfg["env"]["numActions"] = 9

        self.cfg["device_type"] = device_type
        self.cfg["device_id"] = device_id
        self.cfg["headless"] = headless

        super().__init__(cfg=self.cfg)

        # get gym GPU state tensors
        actor_root_state_tensor = self.gym.acquire_actor_root_state_tensor(self.sim)
        dof_state_tensor = self.gym.acquire_dof_state_tensor(self.sim)
        rigid_body_tensor = self.gym.acquire_rigid_body_state_tensor(self.sim)

        self.gym.refresh_actor_root_state_tensor(self.sim)
        self.gym.refresh_dof_state_tensor(self.sim)
        self.gym.refresh_rigid_body_state_tensor(self.sim)

        # create some wrapper tensors for different slices
        self.franka_default_dof_pos = to_torch([1.157, -1.066, -0.155, -2.239, -1.841, 1.003, 0.469, 0.035, 0.035], device=self.device)
        self.dof_state = gymtorch.wrap_tensor(dof_state_tensor)
        self.franka_dof_state = self.dof_state.view(self.num_envs, -1, 2)[:, :self.num_franka_dofs]
        self.franka_dof_pos = self.franka_dof_state[..., 0]
        self.franka_dof_vel = self.franka_dof_state[..., 1]

        self.rigid_body_states = gymtorch.wrap_tensor(rigid_body_tensor).view(self.num_envs, -1, 13)
        self.num_bodies = self.rigid_body_states.shape[1]

        block1_state = self.num_franka_bodies + self.num_block1_bodies
        self.block1_state = self.rigid_body_states[:, self.num_franka_bodies]
        self.block2_state = self.rigid_body_states[:, block1_state]

        self.root_state_tensor = gymtorch.wrap_tensor(actor_root_state_tensor).view(self.num_envs, -1, 13)

        self.block1_rstates = self.root_state_tensor[:,1]
        self.block2_rstates = self.root_state_tensor[:,2]

        self.num_dofs = self.gym.get_sim_dof_count(self.sim) // self.num_envs
        self.franka_dof_targets = torch.zeros((self.num_envs, self.num_dofs), dtype=torch.float, device=self.device)

        # get the indices for all four objects (robot, table, block1, block2)
        self.global_indices = torch.arange(self.num_envs * (4), dtype=torch.int32, device=self.device).view(self.num_envs, -1)

        self.reset(torch.arange(self.num_envs, device=self.device))

    def create_sim(self):
        self.sim_params.up_axis = gymapi.UP_AXIS_Z
        self.sim_params.gravity.x = 0
        self.sim_params.gravity.y = 0
        self.sim_params.gravity.z = -9.81
        self.sim = self.gym.create_sim(
          self.device_id, self.graphics_device_id,
          self.physics_engine, self.sim_params
        )
        self._create_ground_plane()
        self._create_envs( self.num_envs,
          self.cfg["env"]['envSpacing'], int(np.sqrt(self.num_envs))
        )

    def _create_ground_plane(self):
        plane_params = gymapi.PlaneParams()
        plane_params.normal = gymapi.Vec3(0.0, 0.0, 1.0)
        self.gym.add_ground(self.sim, plane_params)

    def _create_envs(self, num_envs, spacing, num_per_row):
        lower = gymapi.Vec3(-spacing, 0.0, -spacing)
        upper = gymapi.Vec3(spacing, spacing, spacing)

        asset_root = "../../assets"
        franka_asset_file = "urdf/franka_description/robots/franka_panda.urdf"

        if "asset" in self.cfg["env"]:
            asset_root = self.cfg["env"]["asset"].get("assetRoot", asset_root)
            franka_asset_file = self.cfg["env"]["asset"].get("assetFileNameFranka", franka_asset_file)

        block_size = [0.08, 0.08, 0.08]

        # create block assets
        asset_options = gymapi.AssetOptions()
        asset_options.density = 2000.0
        asset_options.fix_base_link = False
        block1_asset = self.gym.create_box(
          self.sim, block_size[0], block_size[1], block_size[2], asset_options
        )
        block2_asset = self.gym.create_box(
          self.sim, block_size[0], block_size[1], block_size[2], asset_options
        )
        asset_options.fix_base_link = True
        table_asset = self.gym.create_box(
          self.sim, 0.9, 0.9, 0.4, asset_options
        )

        # load franka asset
        asset_options = gymapi.AssetOptions()
        asset_options.flip_visual_attachments = True
        asset_options.fix_base_link = True
        asset_options.collapse_fixed_joints = True
        asset_options.disable_gravity = True
        asset_options.thickness = 0.001
        asset_options.default_dof_drive_mode = gymapi.DOF_MODE_POS
        asset_options.use_mesh_materials = True
        franka_asset = self.gym.load_asset(
          self.sim, asset_root, franka_asset_file, asset_options
        )

        franka_dof_stiffness = to_torch(
          [400, 400, 400, 400, 400, 400, 400, 1.0e6, 1.0e6],
          dtype=torch.float, device=self.device
        )
        franka_dof_damping = to_torch(
          [80, 80, 80, 80, 80, 80, 80, 1.0e2, 1.0e2],
          dtype=torch.float, device=self.device
        )

        self.num_franka_bodies = self.gym.get_asset_rigid_body_count(franka_asset)
        self.num_franka_dofs = self.gym.get_asset_dof_count(franka_asset)
        self.num_block1_bodies = self.gym.get_asset_rigid_body_count(block1_asset)
        self.num_block2_bodies = self.gym.get_asset_rigid_body_count(block2_asset)

        # print("num franka bodies: ", self.num_franka_bodies)
        # print("num franka dofs: ", self.num_franka_dofs)
        # print("num block1 bodies: ", self.num_block1_bodies)
        # print("num block2 bodies: ", self.num_block2_bodies)

        # set franka dof properties
        franka_dof_props = self.gym.get_asset_dof_properties(franka_asset)
        self.franka_dof_lower_limits = []
        self.franka_dof_upper_limits = []
        for i in range(self.num_franka_dofs):
            franka_dof_props['driveMode'][i] = gymapi.DOF_MODE_POS
            if self.physics_engine == gymapi.SIM_PHYSX:
                franka_dof_props['stiffness'][i] = franka_dof_stiffness[i]
                franka_dof_props['damping'][i] = franka_dof_damping[i]
            else:
                franka_dof_props['stiffness'][i] = 7000.0
                franka_dof_props['damping'][i] = 50.0

            self.franka_dof_lower_limits.append(franka_dof_props['lower'][i])
            self.franka_dof_upper_limits.append(franka_dof_props['upper'][i])

        self.franka_dof_lower_limits = to_torch(
          self.franka_dof_lower_limits, device=self.device )
        self.franka_dof_upper_limits = to_torch(
          self.franka_dof_upper_limits, device=self.device )
        self.franka_dof_speed_scales = torch.ones_like(
          self.franka_dof_lower_limits )
        self.franka_dof_speed_scales[[7, 8]] = 0.1
        franka_dof_props['effort'][7] = 200
        franka_dof_props['effort'][8] = 200

        franka_start_pose = gymapi.Transform()
        franka_start_pose.p = gymapi.Vec3(0.0, 0.0, 0.4025)
        franka_start_pose.r = gymapi.Quat(0.0, 0.0, 0.0, 1.0)

        table_start_pose = gymapi.Transform()
        table_start_pose.p = gymapi.Vec3(0.3, 0.0, 0.2)
        block1_start_pose = gymapi.Transform()
        block1_start_pose.p = gymapi.Vec3(0.55, 0.25, 0.44)
        block2_start_pose = gymapi.Transform()
        block2_start_pose.p = gymapi.Vec3(0.55, -0.25, 0.44)

        # compute aggregate size
        num_franka_bodies = self.gym.get_asset_rigid_body_count(franka_asset)
        num_franka_shapes = self.gym.get_asset_rigid_shape_count(franka_asset)
        num_table_bodies = self.gym.get_asset_rigid_body_count(table_asset)
        num_table_shapes = self.gym.get_asset_rigid_shape_count(table_asset)
        num_block1_bodies = self.gym.get_asset_rigid_body_count(block1_asset)
        num_block1_shapes = self.gym.get_asset_rigid_shape_count(block1_asset)
        num_block2_bodies = self.gym.get_asset_rigid_body_count(block2_asset)
        num_block2_shapes = self.gym.get_asset_rigid_shape_count(block2_asset)
        max_agg_bodies = num_franka_bodies + num_block1_bodies + num_block2_bodies + num_table_bodies
        max_agg_shapes = num_franka_shapes + num_block1_shapes + num_block2_shapes + num_table_shapes

        self.frankas = []
        self.block1s = []
        self.default_block1_poses = []
        self.block2s = []
        self.default_block2_poses = []
        self.envs = []

        for i in range(self.num_envs):
            # create env instance
            env_ptr = self.gym.create_env(
                self.sim, lower, upper, num_per_row
            )

            if self.aggregate_mode >= 3:
                self.gym.begin_aggregate(
                  env_ptr, max_agg_bodies, max_agg_shapes, True
                )

            franka_actor = self.gym.create_actor(
              env_ptr, franka_asset, franka_start_pose, "franka", i, 2, 0
            )
            self.gym.set_actor_dof_properties(
              env_ptr, franka_actor, franka_dof_props
            )

            if self.aggregate_mode == 2:
                self.gym.begin_aggregate(
                  env_ptr, max_agg_bodies, max_agg_shapes, True
                )

            table_actor = self.gym.create_actor(
              env_ptr, table_asset, table_start_pose, "table", i, 0, 0
            )

            block1_pose = block1_start_pose
            block1_pose.p.x += self.start_position_noise * (np.random.rand() - 0.5)
            dz = 0.5 * np.random.rand()
            dy = np.random.rand() - 0.5
            block1_pose.p.y += self.start_position_noise * dy
            block1_pose.p.z += self.start_position_noise * dz
            block1_actor = self.gym.create_actor(
              env_ptr, block1_asset, block1_pose, "block1", i, 0, 0
            )
            self.default_block1_poses.append( [
              block1_pose.p.x, block1_pose.p.y, block1_pose.p.z,
              block1_pose.r.x, block1_pose.r.y, block1_pose.r.z,
              block1_pose.r.w, 0, 0, 0, 0, 0, 0
            ] )

            block2_pose = block2_start_pose
            block2_pose.p.x += self.start_position_noise * (np.random.rand() - 0.5)
            dz = 0.5 * np.random.rand()
            dy = np.random.rand() - 0.5
            block2_pose.p.y += self.start_position_noise * dy
            block2_pose.p.z += self.start_position_noise * dz
            block2_actor = self.gym.create_actor(
              env_ptr, block2_asset, block2_pose, "block2", i, 0, 0
            )
            self.default_block2_poses.append( [
              block2_pose.p.x, block2_pose.p.y, block2_pose.p.z,
              block2_pose.r.x, block2_pose.r.y, block2_pose.r.z,
              block2_pose.r.w, 0, 0, 0, 0, 0, 0
            ] )

            if self.aggregate_mode > 0:
                self.gym.end_aggregate(env_ptr)

            self.envs.append(env_ptr)
            self.frankas.append(franka_actor)
            self.block1s.append(block1_actor)
            self.block2s.append(block2_actor)

        self.hand_handle = self.gym.find_actor_rigid_body_handle(env_ptr, franka_actor, "panda_link7")
        self.lfinger_handle = self.gym.find_actor_rigid_body_handle(env_ptr, franka_actor, "panda_leftfinger")
        self.rfinger_handle = self.gym.find_actor_rigid_body_handle(env_ptr, franka_actor, "panda_rightfinger")
        self.block1_handle = self.gym.find_actor_rigid_body_handle(env_ptr, block1_actor, "block1")
        self.block2_handle = self.gym.find_actor_rigid_body_handle(env_ptr, block2_actor, "block2")
        # self.block1_handle = 10
        # self.block2_handle = 11
        self.default_block1_poses = to_torch(
          self.default_block1_poses, device=self.device, dtype=torch.float
        ).view( self.num_envs, 13 )
        self.default_block2_poses = to_torch(
          self.default_block2_poses, device=self.device, dtype=torch.float
        ).view( self.num_envs, 13 )

        self.init_data()

    def init_data(self):
        hand = self.gym.find_actor_rigid_body_handle(self.envs[0], self.frankas[0], "panda_link7")
        lfinger = self.gym.find_actor_rigid_body_handle(self.envs[0], self.frankas[0], "panda_leftfinger")
        rfinger = self.gym.find_actor_rigid_body_handle(self.envs[0], self.frankas[0], "panda_rightfinger")

        hand_pose = self.gym.get_rigid_transform(self.envs[0], hand)
        lfinger_pose = self.gym.get_rigid_transform(self.envs[0], lfinger)
        rfinger_pose = self.gym.get_rigid_transform(self.envs[0], rfinger)

        finger_pose = gymapi.Transform()
        finger_pose.p = (lfinger_pose.p + rfinger_pose.p) * 0.5
        finger_pose.r = lfinger_pose.r

        hand_pose_inv = hand_pose.inverse()
        grasp_pose_axis = 1
        franka_local_grasp_pose = hand_pose_inv * finger_pose
        franka_local_grasp_pose.p += gymapi.Vec3(
          *get_axis_params(0.04, grasp_pose_axis) )
        self.franka_local_grasp_pos = to_torch(
          [ franka_local_grasp_pose.p.x, franka_local_grasp_pose.p.y,
            franka_local_grasp_pose.p.z ],
          device=self.device
        ).repeat( (self.num_envs, 1) )
        self.franka_local_grasp_rot = to_torch(
          [ franka_local_grasp_pose.r.x, franka_local_grasp_pose.r.y,
            franka_local_grasp_pose.r.z, franka_local_grasp_pose.r.w ],
          device=self.device
        ).repeat( (self.num_envs, 1) )

        block1_local_grasp_pose = gymapi.Transform()
        block1_local_grasp_pose.p = gymapi.Vec3(0, 0, 1)
        block1_local_grasp_pose.r = gymapi.Quat(0, 0, 0, 1)
        self.block1_local_grasp_pos = to_torch(
          [block1_local_grasp_pose.p.x, block1_local_grasp_pose.p.y,
          block1_local_grasp_pose.p.z],
          device=self.device
        ).repeat((self.num_envs, 1))
        self.block1_local_grasp_rot = to_torch(
          [block1_local_grasp_pose.r.x, block1_local_grasp_pose.r.y,
          block1_local_grasp_pose.r.z, block1_local_grasp_pose.r.w],
          device=self.device
        ).repeat((self.num_envs, 1))

        self.gripper_forward_axis = to_torch( [0, 0, 1], device=self.device
        ).repeat((self.num_envs, 1))
        self.gripper_up_axis = to_torch( [0, 1, 0], device=self.device
        ).repeat((self.num_envs, 1))
        self.block1_inward_axis = to_torch( [0, 0, -1], device=self.device
        ).repeat((self.num_envs, 1))
        self.block1_up_axis = to_torch( [0, 0, 1], device=self.device
        ).repeat((self.num_envs, 1))

        self.franka_grasp_pos = torch.zeros_like(self.franka_local_grasp_pos)
        self.franka_grasp_rot = torch.zeros_like(self.franka_local_grasp_rot)
        self.franka_grasp_rot[..., -1] = 1  # xyzw
        self.block_grasp_pos = torch.zeros_like(self.block1_local_grasp_pos)
        self.block_grasp_rot = torch.zeros_like(self.block1_local_grasp_rot)
        self.block_grasp_rot[..., -1] = 1
        self.franka_lfinger_pos = torch.zeros_like(self.franka_local_grasp_pos)
        self.franka_rfinger_pos = torch.zeros_like(self.franka_local_grasp_pos)
        self.franka_lfinger_rot = torch.zeros_like(self.franka_local_grasp_rot)
        self.franka_rfinger_rot = torch.zeros_like(self.franka_local_grasp_rot)

    def compute_reward(self, actions):
        self.rew_buf[:], self.reset_buf[:] = compute_stack_reward(
            self.reset_buf, self.progress_buf, self.actions,
            self.block1_state, self.block2_state,
            self.franka_grasp_pos, self.block_grasp_pos,
            self.franka_grasp_rot, self.block_grasp_rot,
            self.franka_lfinger_pos, self.franka_rfinger_pos,
            self.gripper_forward_axis, self.block1_inward_axis,
            self.gripper_up_axis, self.block1_up_axis,
            self.num_envs, self.dist_reward_scale, self.rot_reward_scale, self.around_handle_reward_scale, self.stack_reward_scale,
            self.finger_dist_reward_scale, self.action_penalty_scale, self.distX_offset, self.max_episode_length
        )

    def compute_observations(self):

        self.gym.refresh_actor_root_state_tensor(self.sim)
        self.gym.refresh_dof_state_tensor(self.sim)
        self.gym.refresh_rigid_body_state_tensor(self.sim)

        hand_pos = self.rigid_body_states[:, self.hand_handle][:, 0:3]
        hand_rot = self.rigid_body_states[:, self.hand_handle][:, 3:7]
        block1_pos = self.rigid_body_states[:, self.block1_handle][:, 0:3]
        block1_rot = self.rigid_body_states[:, self.block1_handle][:, 3:7]
        block2_pos = self.rigid_body_states[:, self.block2_handle][:, 0:3]
        block2_rot = self.rigid_body_states[:, self.block2_handle][:, 3:7]

        self.franka_grasp_rot[:], self.franka_grasp_pos[:], self.block_grasp_rot[:], self.block_grasp_pos[:] = \
            compute_grasp_transforms(
              hand_rot, hand_pos, self.franka_local_grasp_rot,
              self.franka_local_grasp_pos, block1_rot, block1_pos,
              self.block1_local_grasp_rot, self.block1_local_grasp_pos
            )

        self.franka_lfinger_pos = self.rigid_body_states[:, self.lfinger_handle][:, 0:3]
        self.franka_rfinger_pos = self.rigid_body_states[:, self.rfinger_handle][:, 0:3]
        self.franka_lfinger_rot = self.rigid_body_states[:, self.lfinger_handle][:, 3:7]
        self.franka_rfinger_rot = self.rigid_body_states[:, self.rfinger_handle][:, 3:7]

        dof_pos_scaled = (2.0 * (self.franka_dof_pos - self.franka_dof_lower_limits)
                          / (self.franka_dof_upper_limits - self.franka_dof_lower_limits) - 1.0)
        to_target = self.block_grasp_pos - self.franka_grasp_pos
        self.obs_buf = torch.cat( (
          dof_pos_scaled,
          self.franka_dof_vel * self.dof_vel_scale,
          to_target,
          self.block1_state[:, :3],
          self.block2_state[:, :3]
        ), dim=-1 )
        return self.obs_buf

    def reset(self, env_ids):
        env_ids_int32 = env_ids.to(dtype=torch.int32)

        # reset franka
        pos = tensor_clamp(
          self.franka_default_dof_pos.unsqueeze(0) + 0.25 *
          (
            torch.rand( (len(env_ids), self.num_franka_dofs),
            device=self.device ) - 0.5
          ),
          self.franka_dof_lower_limits, self.franka_dof_upper_limits
        )
        self.franka_dof_pos[env_ids, :] = pos
        self.franka_dof_vel[env_ids, :] = torch.zeros_like(
          self.franka_dof_vel[env_ids] )
        self.franka_dof_targets[env_ids, :self.num_franka_dofs] = pos

        # reset blocks
        self.block1_rstates[env_ids] = self.default_block1_poses[env_ids]
        self.block2_rstates[env_ids] = self.default_block2_poses[env_ids]

        block1_indices = self.global_indices[env_ids, 2].flatten()
        block2_indices = self.global_indices[env_ids, 3].flatten()
        self.gym.set_actor_root_state_tensor_indexed(
          self.sim, gymtorch.unwrap_tensor(self.root_state_tensor), gymtorch.unwrap_tensor(block1_indices), len(block1_indices)
        )
        self.gym.set_actor_root_state_tensor_indexed(
          self.sim, gymtorch.unwrap_tensor(self.root_state_tensor), gymtorch.unwrap_tensor(block2_indices), len(block2_indices)
        )

        self.gym.set_dof_position_target_tensor(
          self.sim, gymtorch.unwrap_tensor(self.franka_dof_targets)
        )

        self.gym.set_dof_state_tensor(
          self.sim, gymtorch.unwrap_tensor(self.dof_state)
        )

        self.progress_buf[env_ids] = 0
        self.reset_buf[env_ids] = 0

    def pre_physics_step(self, actions):
        self.actions = actions.clone().to(self.device)
        targets = ( self.franka_dof_targets[:, :self.num_franka_dofs] +
          self.franka_dof_speed_scales * self.dt *
          self.actions * self.action_scale
        )
        self.franka_dof_targets[:, :self.num_franka_dofs] = tensor_clamp(
          targets, self.franka_dof_lower_limits, self.franka_dof_upper_limits
        )
        self.gym.set_dof_position_target_tensor(
          self.sim, gymtorch.unwrap_tensor(self.franka_dof_targets)
        )

    def post_physics_step(self):
        self.progress_buf += 1

        env_ids = self.reset_buf.nonzero(as_tuple=False).squeeze(-1)
        if len(env_ids) > 0:
            self.reset(env_ids)

        self.compute_observations()
        self.compute_reward(self.actions)


#####################################################################
###=========================jit functions=========================###
#####################################################################

@torch.jit.script
def compute_stack_reward(
    reset_buf, progress_buf, actions, block1_state, block2_state,
    franka_grasp_pos, block_grasp_pos, franka_grasp_rot, block_grasp_rot,
    franka_lfinger_pos, franka_rfinger_pos,
    gripper_forward_axis, block_inward_axis, gripper_up_axis, block_up_axis,
    num_envs, dist_reward_scale, rot_reward_scale,
    around_handle_reward_scale, stack_reward_scale,
    finger_dist_reward_scale, action_penalty_scale,
    distX_offset, max_episode_length
):
    # type: (Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, int, float, float, float, float, float, float, float, float) -> Tuple[Tensor, Tensor]

    # distance from hand to block1
    d = torch.norm(franka_grasp_pos - block_grasp_pos, p=2, dim=-1)
    dist_reward = 1.0 / (1.0 + d ** 2)
    dist_reward *= dist_reward
    dist_reward = torch.where(d <= 0.02, dist_reward * 2, dist_reward)

    # reward for matching the orientation of the hand to block1 (fingers wrapped)
    axis1 = tf_vector(franka_grasp_rot, gripper_forward_axis)
    axis2 = tf_vector(block_grasp_rot, block_inward_axis)
    axis3 = tf_vector(franka_grasp_rot, gripper_up_axis)
    axis4 = tf_vector(block_grasp_rot, block_up_axis)

    dot1 = torch.bmm( axis1.view(num_envs, 1, 3),
      axis2.view(num_envs, 3, 1) ).squeeze(-1).squeeze(-1)  # alignment of forward axis for gripper
    dot2 = torch.bmm( axis3.view(num_envs, 1, 3),
      axis4.view(num_envs, 3, 1) ).squeeze(-1).squeeze(-1)  # alignment of up axis for gripper

    rot_reward = 0.5 * (
      torch.sign(dot1) * dot1 ** 2 + torch.sign(dot2) * dot2 ** 2 )

    # reward for distance of each finger from the block
    finger_dist_reward = torch.zeros_like(rot_reward)
    lfinger_dist = torch.abs(franka_lfinger_pos[:, 2] - block_grasp_pos[:, 2])
    rfinger_dist = torch.abs(franka_rfinger_pos[:, 2] - block_grasp_pos[:, 2])
    finger_dist_reward = torch.where(
        franka_lfinger_pos[:, 2] > block_grasp_pos[:, 2],
        torch.where( franka_rfinger_pos[:, 2] < block_grasp_pos[:, 2],
          (0.04 - lfinger_dist) + (0.04 - rfinger_dist), finger_dist_reward
        ),
        finger_dist_reward
    )

    # regularization on the actions (summed for each environment)
    action_penalty = torch.sum(actions ** 2, dim=-1)

    # distance from block1 to the stack position on block2
    stack_pose = block2_state[:, :3] + torch.tensor(
      [0., 0., 0.09], device=block2_state.device )
    b = torch.norm(block1_state[:, :3] - stack_pose, p=2, dim=-1)
    stack_reward = 1.0 / (1.0 + b ** 2)
    stack_reward *= stack_reward
    stack_reward = torch.where(d <= 0.02, stack_reward * 2, stack_reward)

    rewards = (
      dist_reward_scale * dist_reward
      + rot_reward_scale * rot_reward
      # + around_handle_reward_scale * around_handle_reward
      + stack_reward_scale * stack_reward
      + finger_dist_reward_scale * finger_dist_reward
      - action_penalty_scale * action_penalty
    )

    rewards = torch.where( stack_reward > 0.38, rewards + 1.0,
      torch.where( stack_reward > 0.2, rewards + 0.8,
        torch.where( stack_reward > 0.15, rewards + 0.65,
          torch.where( stack_reward > 0.1, rewards + 0.5,
            torch.where( stack_reward > 0.05, rewards + 0.35,
              torch.where( stack_reward > 0.01, rewards + 0.25,
                torch.where( stack_reward > 0.0, rewards + 0.15, rewards )
              )
            )
          )
        )
      )
    )

    # rewards = torch.where(
    #   franka_lfinger_pos[:, 0] < block_grasp_pos[:, 0] - distX_offset,
    #   torch.ones_like( rewards ) * -1, rewards
    # )
    # rewards = torch.where(
    #   franka_rfinger_pos[:, 0] < block_grasp_pos[:, 0] - distX_offset,
    #   torch.ones_like( rewards ) * -1, rewards
    # )

    reset_buf = torch.where( progress_buf >= max_episode_length - 1,
      torch.ones_like(reset_buf), reset_buf
    )

    return rewards, reset_buf


@torch.jit.script
def compute_grasp_transforms( hand_rot, hand_pos,
        franka_local_grasp_rot, franka_local_grasp_pos,
        block_rot, block_pos, block_local_grasp_rot,
        block_local_grasp_pos ):
    # type: (Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor) -> Tuple[Tensor, Tensor, Tensor, Tensor]

    global_franka_rot, global_franka_pos = tf_combine(
        hand_rot, hand_pos, franka_local_grasp_rot, franka_local_grasp_pos)
    global_block_rot, global_block_pos = tf_combine(
        block_rot, block_pos, block_local_grasp_rot, block_local_grasp_pos)

    return global_franka_rot, global_franka_pos, global_block_rot, global_block_pos
