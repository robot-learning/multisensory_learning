#!/usr/bin/env python
import os
import argparse
import torch

from ll4ma_util import ros_util, file_util
from mdn.autoenc_trainer import AutencTrainer


CONFIG_PATH = os.path.join(ros_util.get_path("multisensory_learning"), "src", "mdn", "configs")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--goal_checkpoint', type=str, required=True)
    parser.add_argument('-c', '--config', type=str, default="latent_focal.json")
    args = parser.parse_args()

    device = 'cuda'

    file_util.check_path_exists(args.goal_checkpoint, "Goal classifier checkpoint")
    config = file_util.load_json(os.path.join(CONFIG_PATH, args.config))
    
    trainer = AutencTrainer(config, device, training=False)
    trainer.load_model(args.goal_checkpoint)
    trainer.model.eval()

    inp = torch.randn(10, 64, device=device)
    out = trainer.model(inp)
    out = torch.softmax(out, dim=1).detach().cpu().numpy().squeeze()
    goal_prob = out[-1]
    print(out)

