
import isaacgym

import os, torch, copy, yaml, time
import numpy as np
import matplotlib.pyplot as plt

from isaacgymenvs.utils.rlgames_utils import RLGPUEnv

from isaacgymenvs.utils.utils import set_seed

from rl_games.common import env_configurations, vecenv

from isaacRL.trainers.trainer import Trainer
from isaacRL.tasks.gym_task import create_env, GymTask
from isaacRL.tasks.expert_task import ExpertTask
from isaacRL.utils.rlgames_util import RLGPUAlgoObserver

from rl_games import envs
from rl_games.common import object_factory
from rl_games.common import env_configurations
from rl_games.common import experiment
from rl_games.common import tr_helpers

from rl_games.algos_torch import torch_ext
from rl_games.algos_torch import model_builder
from rl_games.algos_torch import a2c_continuous
from rl_games.algos_torch import a2c_discrete
from rl_games.algos_torch import players
from rl_games.algos_torch import sac_agent

def get_rlg_config(config, checkpt=None):
    rlg_config = {
      'seed': config['environ']['seed'],
      'algo': {'name': 'a2c_continuous'},
      'model': {'name': 'continuous_a2c_logstd'},
      'network': {
        'name': 'actor_critic', 'separate': False,
        'normalization': 'batchnorm' if config['model']['actor'].get('batchnorm',False) else None,
        'space': { 'continuous': {
          'mu_activation': 'None', 'sigma_activation': 'None',
          'mu_init': {'name': 'const_initializer', 'val': 0},
          'sigma_init': {'name': 'const_initializer', 'val': 0},
          'fixed_sigma': True,
        } },
        'mlp': {
          'units': config['model']['actor']['layers'],
          'activation': config['model']['actor']['activation'],
          'initializer': {'name': 'const_initializer', 'val': 0.}, 'regularizer': {'name': 'None'}
        }
      },
      'load_checkpoint': False if checkpt is None else True,
      'load_path': checkpt,
      'config': {
        'name': config['environ']['type'],
        # 'full_experiment_name': config['training']['checkpoint_path'].strip('/').split('/')[-1],
        'full_experiment_name': config['environ']['name'],
        'env_name': 'rlgpu', 'ppo': True, 'mixed_precision': False,
        'normalize_input': True, 'normalize_value': True,
        'num_actors': config['environ']['num_envs'],
        'reward_shaper': {'scale_value': 0.1}, 'normalize_advantage': True,
        'gamma': config['training']['gamma'], 'tau': config['training']['lam'],
        'learning_rate': config['training']['learning_rate'],
        'lr_schedule': 'adaptive', 'kl_threshold': 0.008, 'score_to_win': 20000,
        'max_epochs': config['training']['epochs'],
        'save_best_after': config['training']['save_best_after'],
        'save_frequency': config['training']['checkpoint_interval'],
        'grad_norm': config['training']['grad_norm'],
        'entropy_coef': config['training']['ent_coef'], 'truncate_grads': True,
        'e_clip': config['environ']['cliprange'],
        'horizon_length': config['training']['acts_per_interval'],
        'minibatch_size': config['training']['batch_size'],
        'mini_epochs': config['training']['updates_per_interval'],
        'critic_coef': 4, 'clip_value': True, 'seq_len': 4,
        'bounds_loss_coef': 0.0001, 'print_stats': False,
        'zero_steps': config['training'].get('zero_steps', 0),
        'eval_play_len': config['environ']['max_env_steps']*2
      }
    }
    return {'params': rlg_config}

class TrainerRLG(Trainer):
    """docstring for TrainerRLG."""

    def __init__(self, config):
        device = "cuda" if torch.cuda.is_available() else "cpu"
        super(TrainerRLG, self).__init__(config, device)

        self.config['environ']['seed'] = 42 # -1 if random
        self.config['environ']['seed'] = set_seed(
          self.config['environ']['seed'], torch_deterministic=False
        )
        self.config['environ']['act_range'] = self.config['model']['actor']['act_range']

        self.algo_factory = object_factory.ObjectFactory()
        self.algo_factory.register_builder('a2c_continuous', lambda **kwargs : A2CAgentAlt(**kwargs))
        self.algo_factory.register_builder('a2c_discrete', lambda **kwargs : a2c_discrete.DiscreteA2CAgent(**kwargs))
        self.algo_factory.register_builder('sac', lambda **kwargs: sac_agent.SACAgent(**kwargs))
        #self.algo_factory.register_builder('dqn', lambda **kwargs : dqnagent.DQNAgent(**kwargs))

        self.player_factory = object_factory.ObjectFactory()
        self.player_factory.register_builder('a2c_continuous', lambda **kwargs : players.PpoPlayerContinuous(**kwargs))
        self.player_factory.register_builder('a2c_discrete', lambda **kwargs : players.PpoPlayerDiscrete(**kwargs))
        self.player_factory.register_builder('sac', lambda **kwargs : players.SACPlayer(**kwargs))
        #self.player_factory.register_builder('dqn', lambda **kwargs : players.DQNPlayer(**kwargs))

        self.algo_observer = RLGPUAlgoObserver()
        torch.backends.cudnn.benchmark = True

        vecenv.register('RLGPU',
          lambda config_name, num_actors, **kwargs: RLGPUEnv(
            config_name, num_actors, **kwargs )
        )
        def create_rlgpu_env(**kwargs):
            # task, _ = create_env(self.config['environ'])
            # return task
            if 'expert' in self.config['environ'] and isinstance(self.config['environ']['expert'], dict):
                return ExpertTask(self.config['environ'])
            return GymTask(self.config['environ'])
        env_configurations.register( 'rlgpu', {
            'vecenv_type': 'RLGPU',
            'env_creator': lambda **kwargs: create_rlgpu_env(**kwargs),
        } )

        self.params = get_rlg_config(self.config)['params']


        self.seed = self.params.get('seed', None)

        self.algo_params = self.params['algo']
        self.algo_name = self.algo_params['name']
        self.exp_config = None

        if self.seed:
            torch.manual_seed(self.seed)
            torch.cuda.manual_seed_all(self.seed)
            np.random.seed(self.seed)

        self.params['config']['reward_shaper'] = tr_helpers.DefaultRewardsShaper(**self.params['config']['reward_shaper'])
        if 'features' not in self.params['config']:
            self.params['config']['features'] = {}
        self.params['config']['features']['observer'] = self.algo_observer

    def train(self):
        # self.config['environ']['zero_steps'] = (
        #   self.config['training']['zero_steps']
        #     if 'zero_steps' in self.config['training'] else 0
        # )
        print('Started to train')
        agent = self.algo_factory.create(self.algo_name, base_name='run', params=self.params)
        if self.params['load_path'] is not None:
            agent.restore(self.params['load_path'])
        agent.train()

    def evaluate(self, steps=None):
        # self.config['environ']['zero_steps'] = 0
        print('Started to play')
        player = self.player_factory.create(self.algo_name, params=self.params)
        if self.params['load_path'] is not None:
            player.restore(self.params['load_path'])
        player.run()

    def load_model(self, checkpt):
        # rlg_config_dict = get_rlg_config(self.config, checkpt)
        self.params['load_checkpoint'] = False if checkpt is None else True
        self.params['load_path'] = checkpt
        # self.config['training']['zero_steps'] = 10

    def add_grapher(self, grapher):
        self.config['environ']['grapher'] = grapher

class A2CAgentAlt(a2c_continuous.A2CAgent):
    """docstring for A2CAgentAlt."""

    def __init__(self, base_name, params):
        print(params)
        import pdb; pdb.set_trace()
        super(A2CAgentAlt, self).__init__(base_name, params)
        self._eval_epochs = [0.]
        self._goal_perc = [0.]
        self._eval_means = [0.]
        self.graph_dir = 'graphs/train_comps'
        os.makedirs(self.graph_dir, exist_ok=True)
        self.graph_fil = os.path.join(self.graph_dir, self.experiment_name+'.png')
        self._play_len = self.config['eval_play_len']

    def play_test(self, epoch):

        self.set_eval()
        play_time_start = time.time()

        reward_sum = 0.
        terminals = 0
        goal_nums = 0
        obs = self.vec_env.reset()
        for pi in range(self._play_len):

            if self.use_action_masks:
                masks = self.vec_env.get_action_masks()
                res_dict = self.get_masked_action_values(obs, masks)
            else:
                res_dict = self.get_action_values(obs)

            obs, rewards, dones, infos = self.env_step(res_dict['actions'])

            reward_sum += rewards.sum().item()
            terminals += dones.sum().item()
            goal_nums += self.vec_env.env.goal_resets.sum().item()
        play_time = time.time() - play_time_start

        goal_perc = 100. * goal_nums / terminals
        reward_mean = reward_sum / (self._play_len*self.num_actors)
        print('epoch:{}, goal%:{:.2f}, rew:{:.2f}, play time:{:.2f}'.format(epoch, goal_perc, reward_mean, play_time))
        self._eval_epochs.append(epoch)
        self._goal_perc.append(goal_perc)
        self._eval_means.append(reward_mean)
        self.writer.add_scalar('eval/goal_reached', goal_perc, epoch)
        self.writer.add_scalar('eval/mean_reward', reward_mean, epoch)

        fig, axes = plt.subplots(2,1,figsize=(15,15),sharex=True)
        axes[0].plot(self._eval_epochs,self._goal_perc)
        axes[0].set_ylabel('Goal%')
        # axes[0].set_title('Goal%')
        axes[1].plot(self._eval_epochs,self._eval_means)
        axes[1].set_ylabel('Mean Rewards')
        # axes[1].set_title('Mean Rewards')
        # self.axes[0,1].legend()
        plt.tight_layout()
        plt.savefig(self.graph_fil)
        plt.clf()
        plt.close(fig)

    def train(self):
        if self.zero_steps > 0:
            self.model.a2c_network.freeze_actor()

        self.init_tensors()
        self.last_mean_rewards = -100500
        start_time = time.time()
        total_time = 0
        rep_count = 0
        self.obs = self.env_reset()
        self.curr_frames = self.batch_size_envs

        if self.multi_gpu:
            self.hvd.setup_algo(self)

        while True:
            epoch_num = self.update_epoch()
            step_time, play_time, update_time, sum_time, a_losses, c_losses, b_losses, entropies, kls, last_lr, lr_mul = self.train_epoch()
            total_time += sum_time
            frame = self.frame // self.num_agents

            # cleaning memory to optimize space
            self.dataset.update_values_dict(None)
            # every n epochs we pause and count the percentage of good epochs
            # if (self.save_freq > 0) and (epoch_num % self.save_freq == 0) and (epoch_num >= self.save_best_after):
            if (self.save_freq > 0) and (epoch_num % self.save_freq == 0):
                self.play_test(epoch_num)

            if self.multi_gpu:
                self.hvd.sync_stats(self)
            should_exit = False
            if self.rank == 0:
                self.diagnostics.epoch(self, current_epoch=epoch_num)
                # do we need scaled_time?
                scaled_time = self.num_agents * sum_time
                scaled_play_time = self.num_agents * play_time
                curr_frames = self.curr_frames
                self.frame += curr_frames
                if self.print_stats:
                    fps_step = curr_frames / step_time
                    fps_step_inference = curr_frames / scaled_play_time
                    fps_total = curr_frames / scaled_time
                    print(f'fps step: {fps_step:.1f} fps step and policy inference: {fps_step_inference:.1f}  fps total: {fps_total:.1f}')

                self.write_stats(total_time, epoch_num, step_time, play_time, update_time, a_losses, c_losses, entropies, kls, last_lr, lr_mul, frame, scaled_time, scaled_play_time, curr_frames)
                if len(b_losses) > 0:
                    self.writer.add_scalar('losses/bounds_loss', torch_ext.mean_list(b_losses).item(), frame)

                if self.has_soft_aug:
                    self.writer.add_scalar('losses/aug_loss', np.mean(aug_losses), frame)

                if self.game_rewards.current_size > 0:
                    mean_rewards = self.game_rewards.get_mean()
                    mean_lengths = self.game_lengths.get_mean()
                    self.mean_rewards = mean_rewards[0]

                    for i in range(self.value_size):
                        rewards_name = 'rewards' if i == 0 else 'rewards{0}'.format(i)
                        self.writer.add_scalar(rewards_name + '/step'.format(i), mean_rewards[i], frame)
                        self.writer.add_scalar(rewards_name + '/iter'.format(i), mean_rewards[i], epoch_num)
                        self.writer.add_scalar(rewards_name + '/time'.format(i), mean_rewards[i], total_time)

                    self.writer.add_scalar('episode_lengths/step', mean_lengths, frame)
                    self.writer.add_scalar('episode_lengths/iter', mean_lengths, epoch_num)
                    self.writer.add_scalar('episode_lengths/time', mean_lengths, total_time)

                    if self.has_self_play_config:
                        self.self_play_manager.update(self)

                    checkpoint_name = self.config['name'] + '_ep_' + str(epoch_num) + '_rew_' + str(mean_rewards[0])

                    if self.save_freq > 0:
                        if (epoch_num % self.save_freq == 0) and (mean_rewards[0] <= self.last_mean_rewards):
                            self.save(os.path.join(self.nn_dir, 'last_' + checkpoint_name))

                    if mean_rewards[0] > self.last_mean_rewards and epoch_num >= self.save_best_after:
                        print('saving next best rewards: ', mean_rewards)
                        self.last_mean_rewards = mean_rewards[0]
                        self.save(os.path.join(self.nn_dir, self.config['name']))
                        if self.last_mean_rewards > self.config['score_to_win']:
                            print('Network won!')
                            self.save(os.path.join(self.nn_dir, checkpoint_name))
                            should_exit = True

                if epoch_num > self.max_epochs:
                    self.save(os.path.join(self.nn_dir, 'last_' + self.config['name'] + 'ep' + str(epoch_num) + 'rew' + str(mean_rewards)))
                    print('MAX EPOCHS NUM!')
                    should_exit = True

                update_time = 0
            if self.multi_gpu:
                should_exit_t = torch.tensor(should_exit).float()
                self.hvd.broadcast_value(should_exit_t, 'should_exit')
                should_exit = should_exit_t.float().item()
            if should_exit:
                return self.last_mean_rewards, epoch_num
