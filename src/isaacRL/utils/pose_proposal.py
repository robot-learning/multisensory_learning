
import torch

def get_proposer(config):
    if 'proposer' not in config:
        proposer = UniformProposer(config['offset'])
    else:
        if config['proposer'] == 'surgeon':
            proposer = SurgeonProposer()
        elif config['proposer'] == 'gaussian':
            proposer = GaussianProposer()
        else:
            raise NotImplementedError('Unknown proposer:', config['proposer'])
    return proposer

class Proposer(object):
    """docstring for Proposer."""

    def __init__(self):
        super(Proposer, self).__init__()

    def propose(self):
        raise NotImplementedError('propose for base object not defined!!!')


class UniformProposer(Proposer):
    """docstring for UniformProposer."""

    def __init__(self, pose_range):
        super(UniformProposer, self).__init__()
        self.pose_range = pose_range

    def propose(self, bsize, device='cpu'):
        pose_scale = self.pose_range[:,1] - self.pose_range[:,0]
        pose_low = self.pose_range[:,0]
        pose = torch.rand((bsize, pose_scale.shape[0]))
        pose = pose * pose_scale + pose_low
        return pose.to(device)

class GaussianProposer(Proposer):
    """docstring for GaussianProposer."""

    def __init__(self, pose_range):
        super(GaussianProposer, self).__init__()
        self.pose_range = pose_range

    def propose(self, bsize, device='cpu'):
        pose_scale = self.pose_range[:,1] - self.pose_range[:,0]
        pose_low = self.pose_range[:,0]
        pose = torch.randn((bsize, pose_scale.shape[0]))
        pose = pose * pose_scale + pose_low
        return pose.to(device)

class SurgeonProposer(Proposer):
    """docstring for SurgeonProposer."""

    def __init__(self, offset=[0.,0.,0.09]):
        super(SurgeonProposer, self).__init__()
        self.offset = offset if torch.is_tensor(offset) else torch.tensor(offset)

    def propose(self, state):
        pose = state['surgeon_hand'][:,:7].clone()
        pose[:,:3] += self.offset[:3]
        return pose
        # ee_pose = state['ee_state'][:,:self.goal_shape]
        # self.goal_ranges = self.goal_ranges.to(ee_pose.device)
        # goal_range = self.goal_ranges[:,1] - self.goal_ranges[:,0]
        # goal_low = self.goal_ranges[:,0]
        # goal = torch.rand((*ee_pose.shape), device=ee_pose.device)
        # goal = goal * goal_range + goal_low
