#!/bin/bash
#==========================================================================#
# This script deletes all contents in scratch folder.                      #
#==========================================================================#

#SBATCH --job-name=clear_scratch
#SBATCH -o log-%j.out-%N
#SBATCH -e log-%j.err-%N
#SBATCH --nodes=1
#SBATCH --time=3:00:00
#SBATCH --mem=100M

export SLURM_USER=adam.conkey
export SCRATCH_DIR=/scratch/$SLURM_USER


source $HOME/.bashrc
conda activate isaacgym

python -c "import slurm_helper; slurm_helper.print_timed_msg('Job Started.')"

python -c "import slurm_helper; slurm_helper.clear_dir('$SCRATCH_DIR')"

python -c "import slurm_helper; slurm_helper.print_timed_msg('Job Ended.')"
