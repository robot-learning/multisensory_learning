
from .gymtasks.ll4ma_rl_tasks import Ll4maRLTask
from .gym_task import GymTask
from .expert_task import ExpertTask
from .gymtasks.pickplace_bandit_task import PickplaceBanditTask
from .gymtasks.pick_task import PickBanditTask

def get_task(config, rl_device='cuda'):
    task = config.get('task', None)
    env_class = Ll4maRLTask if config.get('type') == 'll4ma_rl' else GymTask
    # return Ll4maRLTask(config, rl_device)
    if task is None:
        if isinstance(config.get('expert', None), dict):
            envir = ExpertTask(config)
        else:
            envir = env_class(config)
    else:
        if task['name'] == 'picknplace':
            envir = PickplaceBanditTask(config)
        elif task['name'] == 'pick':
            envir = PickBanditTask(config)
        else:
            raise Error('Uknown task {}'.format(task['name']))
    return envir
