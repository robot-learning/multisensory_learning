
from isaacgym import gymtorch, gymapi  # noqa: F401
from isaacRL.train import *
from isaacRL.utils.buffer import BufferFullstate
from isaacRL.tasks.get_task import get_task
from isaacRL.experts.dagger_expert import DaggerPicknplaceStepExpert, DaggerMoveitPlacer

from ll4ma_util import file_util, pytorch3d, math_util

import os
import sys
import argparse
from tqdm import tqdm
import torch
import rospy


def main():
    torch.set_printoptions(precision=3, sci_mode=False)
    device = "cuda" if torch.cuda.is_available() else "cpu"

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str,
        default="configs/place_noisy/behclone_01n_00.yaml", help="path to config file")
    parser.add_argument("--headless", default=None,
        help="overwrite running headless or not")
    opt = parser.parse_args()

    config = file_util.load_yaml(opt.config)
    if True:
        config['environ']['headless'] = True
        config['environ']['num_envs'] = 512
    else:
        config['environ']['headless'] = False
        config['environ']['num_envs'] = 1
    env = get_task(config['environ'])
    num_envs = env.num_envs
    action_size = config['model']['actor']['actions']
    print(env)

    expert = DaggerPicknplaceStepExpert(
        config['environ']['num_acts'],
        config['environ']['goal']['object'],
        same_starts=config['environ']['task'].get('same_starts', True),
        planner=config['environ']['task'].get('planner', 'trajopt'),
        num_envs=num_envs,
        cartesian_path=config['environ']['jacobian_control'],
        changing=False
    )
    # expert = DaggerMoveitPlacer(
    #     config['environ']['num_acts'],
    #     config['environ']['goal']['object'],
    #     env.env_states,
    #     same_starts=config['environ']['task'].get('same_starts', True),
    #     planner=config['environ']['task'].get('planner', 'trajopt'),
    #     num_envs=num_envs,
    #     changing=False
    # )

    buf_size = config['training'].get( 'max_buff_size',
        config['training'].get('max_env_steps', 1)*num_envs
    )
    print('buf size:', buf_size)
    buffer = BufferFullstate(
        state_size=env.fullstate.shape[1],
        action_size=action_size,
        full2obs=env.full2obs,
        device=device,
        storage_device=device,
        max_size=buf_size,
    )

    pretrain_datafile = config['training']['checkpoint_path'].replace('checkpoints', 'data')
    pretrain_datafile = pretrain_datafile.strip('/')
    print('pretrain data folder:', pretrain_datafile.rsplit('/', 1)[0])
    os.makedirs(pretrain_datafile.rsplit('/', 1)[0], exist_ok=True)
    pretrain_datafile = pretrain_datafile + '.torch'
    print('pretrain data file:', pretrain_datafile)

    act_num = (buffer.max_size // num_envs) + 1
    state = env.reset()
    expert.reset_idx(torch.arange(num_envs), state)
    pbar = tqdm(total=act_num, file=sys.stdout)
    desc = f'Filling Buffer'
    pbar.set_description(desc)
    for ai in range(act_num):
        if rospy.is_shutdown():
            sys.exit(1)
        actions = expert.get_actions(state)
        actions = actions.detach()
        nstate, rewards, terminals, _ = env.step(actions)
        buffer.add(
            state, actions, nstate, rewards, terminals
        )
        state = nstate
        pbar.update(1)
        if len(buffer) >= buffer.max_size:
            break
        if (ai + 1) % 5000 == 0:
            buffer.save(pretrain_datafile)
    pbar.close()

    buffer.save(pretrain_datafile)
    print('mean rewards:', buffer.mean_rewards())
    success = env.successes()
    print('successes:', success)
    success = success


if __name__ == '__main__':
    main()
