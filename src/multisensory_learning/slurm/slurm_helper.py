import os
import time
import shutil
import subprocess
from glob import glob
from datetime import datetime
# from tensorflow.python.client import device_lib


def print_file(filename):
    with open(filename, 'r') as f:
        print(f.read())


def create_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def list_dir(dir):
    files = glob(os.path.join(dir, '*')) if os.path.exists else []
    return files


def print_dir(dir):
    print(f"\nDirectory: {dir}")
    files = list_dir(dir)
    for file in files:
        print(f"  {file}")

        
def copy_dir(src, dest):
    shutil.copytree(src, dest)


def copy_file(src, dest):
    shutil.copyfile(src, dest)


def clear_dir(dir):
    print(f"\nClearing directory: {dir}")
    print("\nBefore clear:")
    print_dir(dir)
    for path in list_dir(dir):
        if os.path.isfile(path):
            os.remove(path)
        elif os.path.isdir(path):
            shutil.rmtree(path)
    print("\nAfter clear:")
    print_dir(dir)


def rm_dir(dir):
    shutil.rmtree(dir)


def clear_venv():
    venv_dir = os.environ['DGX_VENV']
    clear_dir(venv_dir)
 

def print_timed_msg(msg):
    print(f"\n[{time.strftime('%Y-%m-%d %H:%M')}] {msg}\n")
    

def list_scratch():
    print(f"\nContents of scratch space {os.environ['SCRATCH_DIR']}:")
    print(list_dir(scratch_dir))


def create_scratch():
    print("Creating scratch space...")
    scratch_dir = os.environ['SCRATCH_DIR']
    if os.path.exists(scratch_dir):
        print(f"Scratch space already exists: {scratch_dir}")
    else:
        create_dir(scratch_dir)
        print("Scratch space created.")

    
def copy_local_to_scratch(local_dir, scratch_dir):
    local_paths = list_dir(local_dir)
    for local_path in local_paths:
        scratch_path = os.path.join(scratch_dir, os.path.basename(local_path))
        if not os.path.exists(scratch_path):
            copy_dir(local_path, scratch_path)


def copy_scratch_to_local(scratch_dir, local_dir):
    create_dir(local_dir)
    for scratch_path in list_dir(scratch_dir):
        local_path = os.path.join(local_dir, os.path.basename(scratch_path))
        if os.path.exists(local_path):
            # Make it a unique path so nothing gets overwritten unintentionally
            local_path = f"{local_path}.UPDATED_{datetime.now().strftime('%Y%m%d-%H%M%S')}"
        copy_dir(scratch_path, local_path)


def run_script(filename, *args):
    subprocess.check_call([filename] + list(args))
