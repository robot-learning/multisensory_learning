
import torch
import torch.nn as nn
import torch.nn.functional as F

class Mish(nn.Module):
    """
    Mish activation function from:
    "Mish: A Self Regularized Non-Monotonic Neural Activation Function"
    https://arxiv.org/abs/1908.08681v1
    x * tanh( softplus( x ) )
    """

    def __init__(self):
        super(Mish, self).__init__()

    def forward(self, x):
        return x * torch.tanh( F.softplus(x) )

class Swish(nn.Module):
    """
    Swish activation function from:
    "Searching for Activation Functions"
    https://arxiv.org/abs/1710.05941
    x * sigmoid( x )
    """

    def __init__(self, arg):
        super(Swish, self).__init__()

    def forward(self, x):
        return x * F.sigmoid(x)
