import h5py
import numpy as np


class Oracle:
    """
    Base clase for oracles, which operate on streams of data recorded from the robot
    doing a task, and it will label each timestep as satisfying a property or not.
    """

    def generate_labels(*args, **kwargs):
        """
        Generates labels for the data at each timestep by determining if 
        it satisfies the oracle's defined conditions at each timestep.
        """
        raise NotImplementedError()
   
    def get_h5_data(self, h5_filename):
        """
        Loads an H5 file and converts it to a dictionary of numpy arrays.
        """
        # TODO this doesn't read out any attrs yet, just data
        with h5py.File(h5_filename, 'r') as h5_file:
            data = self._get_dict_from_h5(h5_file)
        return data
            
    def _get_dict_from_h5(self, h5_item):
        """
        Recursive function to convert all H5 data to a dictionary of numpy arrays.
        
        Args:
            h5_item: H5 File or Group
        """
        data = {}
        for k, v in h5_item.items():
            if isinstance(v, h5py.Dataset):
                data[k] = np.array(v)
            elif isinstance(v, h5py.File) or isinstance(v, h5py.Group):
                data[k] = self._get_dict_from_h5(v)
            else:
                raise ValueError(f"Unknown H5 type for key '{k}'")
        return data


if __name__ == '__main__':
    # Check to see if it read things from H5

    def print_data(data, indent=''):
        for k, v in data.items():
            if isinstance(v, np.ndarray):
                print(indent + k, v.shape)
            elif isinstance(v, dict):
                print(indent + k)
                print_data(v, indent + '  ')

    filename = "/home/adam/pick_object_data/demo_0001.h5"
    oracle = Oracle()
    data = oracle.get_h5_data(filename)
    print_data(data)

    
                
            
