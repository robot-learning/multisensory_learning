import os
import importlib  # Dynamic class creation
from dataclasses import dataclass, field
from typing import List, Dict

from ll4ma_util import ros_util, func_util

from ll4ma_isaacgym.core.config import (
    RobotConfig,
    RandomObjectsConfig,
    SimulatorConfig,
    SensorConfig
)

@dataclass
class CameraConfig(SensorConfig):
    """
    Configuration dataclass for cameras.
    """

    origin: List[float] = None
    target: List[float] = None
    width: int = 512
    height: int = 512
    horizontal_fov: int = 120
    sim_handle: int = None
    depth_min: float = -3.0
    use_rgb: bool = True
    use_depth: bool = True

@dataclass
class ObjectConfig(func_util.Config):
    """
    Configuration dataclass for objects in the simulator.
    """

    object_type: str = None
    asset_root: str = None
    asset_filename: str = None
    name: str = None
    extents: List[float] = func_util.lambda_field([0.0, 0.0, 0.0])
    extents_ranges: List[List[float]] = None
    density: float = None  # g/m^3
    friction: float = None  # Range [0,1]: 0 slippery, 1 sticky, defaults 1
    restitution: float = None  # Range [0,1]: 0 no-bounce, 1 bounces away on drop, defaults 0
    mass: float = None
    rgb_color: List[float] = None
    set_color: bool = True
    position: List[float] = func_util.lambda_field([None] * 3)
    position_ranges: List[List[float]] = None  # [(lo_x, hi_x), (lo_y, hi_y), (lo_z, hi_z)]
    position_gaussian: List[List[float]] = None  # [(mu_x, std_x), (mu_y, std_y), (mu_z, std_z)]
    orientation: List[float] = None
    orientation_choices: List[List[float]] = None
    sample_axes: List[List[float]] = None
    sample_angle_bounds: List[List[float]] = None
    disturbance_ranges: List[List[float]] = None
    fix_base_link: bool = False
    rb_indices: List[int] = field(default_factory=list)  # For retrieving pose in sim
    frame_id: str = "world"
    # The vhacd option enables convex decomposition instead of approximating
    # objects as a convex hull. This may have slightly slower sim, but it more
    # closely matches the object geometry which is important for manipulation.
    vhacd_enabled: bool = True

    noise: float = 0.       # measures how much noise to add
    noise_type: str = None  # episodic or stepwise

@dataclass
class EnvironmentConfig(func_util.Config):
    """
    Configuration dataclass for the simulation environment, which encapsulates
    objects, robots (arms + end-effectors), sensors, and general attributes to
    be applied to the simulation environment.
    """

    sim: SimulatorConfig = field(default_factory=SimulatorConfig)

    num_envs: int = 1
    control_freq_inv: int = 1
    reset_wait_steps: int = 1
    # int for number of states to reset to or str 'inf' for infinite reset states
    randomize = 0

    # action info
    control_size: int = 0
    action_scale: float = 7.5
    dof_vel_scale: float = 0.1
    additive_actions: bool = False
    cartesian_control: bool = False
    vecolity_control: bool = False

    img_size: int = None
    spacing: float = 2.0
    objects: Dict[str, ObjectConfig] = field(default_factory=dict)
    robots: Dict[str, RobotConfig] = field(default_factory=dict)
    sensors: Dict[str, SensorConfig] = field(default_factory=dict)
    random_objects: RandomObjectsConfig = field(default_factory=RandomObjectsConfig)

    def __post_init__(self):
        super().__post_init__()
        for rob in self.robots.values():
            self.control_size += rob.arm.n_joints
            if rob.end_effector is not None:
                self.control_size += 1

    def from_dict(self, dict_):
        for k, v in dict_["objects"].items():
            self.objects[k] = ObjectConfig(config_dict=v)
        del dict_["objects"]

        for k, v in dict_["robots"].items():
            arm_config_name = f"{v['arm']['arm_type']}Config"
            ArmConfigClass = func_util.get_class(arm_config_name, ["ll4ma_isaacgym.core.config"])
            if ArmConfigClass is None:
                raise ValueError(f"No config known for arm type: {v['arm']['arm_type']}")
            arm_config = ArmConfigClass(config_dict=v["arm"])

            ee_config_name = f"{v['end_effector']['ee_type']}Config"
            EEConfigClass = func_util.get_class(ee_config_name, ["ll4ma_isaacgym.core.config"])
            if EEConfigClass is None:
                raise ValueError(f"No config known for EE type: {v['end_effector']['ee_type']}")
            ee_config = EEConfigClass(config_dict=v["end_effector"])

            del v["arm"]
            del v["end_effector"]
            robot_config = RobotConfig(config_dict=v)
            robot_config.arm = arm_config
            robot_config.end_effector = ee_config
            robot_config.name = k
            self.robots[k] = robot_config

        del dict_["robots"]

        for k, v in dict_["sensors"].items():
            if v["sensor_type"] == "camera":
                self.sensors[k] = CameraConfig(config_dict=v)
            else:
                raise ValueError(f"Unknown sensor type: {v['sensor_type']}")
        del dict_["sensors"]

        super().from_dict(dict_)
