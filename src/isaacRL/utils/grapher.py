
import os
import torch
import numpy as np
import matplotlib.pyplot as plt

class EvalGrapher(object):
    """docstring for EvalGrapher."""

    def __init__(self, max_graphs=4, savname='test'):
        super(EvalGrapher, self).__init__()
        self.max_graphs = max_graphs
        self.num_graphs = 0
        self.data = {
          'rewards': [], 'goal_dist': [], 'ee_state': [], 'goal_pos': [], 'joint_position': []
        }
        self.graphs = []
        self.savfol = os.path.join('graphs', savname, 'eval')
        os.makedirs(self.savfol, exist_ok=True)

    def update(self, state, rewards, done, info):
        # self.data['joint_position'] = state['joint_position']
        self.data['rewards'].append(rewards.clone())
        self.data['goal_dist'].append(info['goal_dist'].clone())
        self.data['ee_state'].append(state['ee_state'].clone())
        self.data['goal_pos'].append(state['goal'].clone())
        self.data['joint_position'].append(state['joint_position'].clone())
        if (done > 0).any():
            for idx in torch.arange(done.shape[0])[done>0]:
                fig, axes = plt.subplots(2,2,figsize=(15,15))
                data = [data[idx].item() for data in self.data['rewards']]
                axes[0,0].plot(data)

                data = [data[idx].item() for data in self.data['goal_dist']]
                axes[0,1].plot(data)

                data = np.stack([data[idx].cpu().numpy() for data in self.data['joint_position']])
                for ai in range(min(data.shape[1], 7)):
                    axes[1,0].plot(data[:,ai], label='joint{}'.format(ai))
                axes[1,0].legend()

                data = np.stack([data[idx].cpu().numpy() for data in self.data['ee_state']])
                data2 = np.stack([data[idx].cpu().numpy() for data in self.data['goal_pos']])
                labels = ['x', 'y', 'z']
                labels2 = ['gx', 'gy', 'gz']
                for ai in range(min(data.shape[1],3)):
                    axes[1,1].plot(data[:,ai], label=labels[ai])
                    axes[1,1].plot(data2[1:,ai], label=labels2[ai])
                axes[1,1].legend()
                plt.savefig(
                  self.savfol + '/{}_{}.png'.format('data', self.num_graphs)
                )
                plt.clf()
                self.num_graphs += 1
                if self.num_graphs >= self.max_graphs:
                    break
            plt.close('all')
        return self.num_graphs >= self.max_graphs


class TrainGrapher(object):
    """docstring for TrainGrapher."""

    def __init__(self, save_inter=50, savname='test'):
        super(TrainGrapher, self).__init__()
        self.save_inter = save_inter
        self.inter = 0
        self.reset()
        self.graphs = []
        self.savfol = os.path.join('graphs', savname, 'train')
        os.makedirs(self.savfol, exist_ok=True)

    def reset(self):
        self.data = {
          'rewards': [], 'goal_dist': [], 'ee_state': [], 'goal_pos': [], 'joint_position': []
        }

    def update(self, state, rewards, done, info):
        idx = 0
        self.data['rewards'].append(rewards[idx].item())
        self.data['goal_dist'].append(info['goal_dist'][idx].item())
        self.data['ee_state'].append(state['ee_state'][idx].cpu().numpy())
        self.data['goal_pos'].append(state['goal'][idx].cpu().numpy())
        self.data['joint_position'].append(state['joint_position'][idx].cpu().numpy())
        if done[idx] > idx:
            if self.inter % self.save_inter == 0 and len(self.data['rewards']) > 2:
                fig, axes = plt.subplots(2,2,figsize=(15,15))
                axes[0,0].plot(self.data['rewards'][1:], label='rewards')
                axes[0,0].set_title('rewards')
                axes[0,1].plot(self.data['goal_dist'][1:], label='goal_dist')
                axes[0,1].set_title('goal_dist')

                data = np.stack(self.data['joint_position'])
                for ai in range(min(data.shape[1], 7)):
                    axes[1,0].plot(data[1:,ai], label='joint{}'.format(ai))
                axes[1,0].set_title('joint_position')
                axes[1,0].legend()

                data = np.stack(self.data['ee_state'])
                data2 = np.stack(self.data['goal_pos'])
                labels = ['x', 'y', 'z']
                labels2 = ['gx', 'gy', 'gz']
                for ai in range(min(data.shape[1],3)):
                    axes[1,1].plot(data[1:,ai], label=labels[ai])
                    axes[1,1].plot(data2[1:,ai], label=labels2[ai])
                axes[1,1].set_title('ee_state')
                axes[1,1].legend()

                savfil = self.savfol + '/{}_{}.png'.format('data', self.inter)
                plt.savefig(savfil)
                plt.clf()
                print('saved graph: ', savfil)
                print('steps:', len(data))
                plt.close(fig)
            self.inter += 1
            self.reset()
        return False
