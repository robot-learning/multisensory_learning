
from isaacgym import gymtorch, gymapi
from ll4ma_util import file_util
from isaacRL.tasks.gym_task import create_env

import torch

# config = file_util.load_yaml('configs/a2c/iiwastack1.yaml')
config = file_util.load_yaml('configs/a2c/move2object01.yaml')
config['environ']['act_range'] = config['model']['actor']['act_range']
task, env_config = create_env(config['environ'])
# print('env_config:\n', env_config)
zero_act = task.zero_actions()
for _ in range(5):
    task.step(zero_act)

task.reset_buf = torch.ones_like(task.reset_buf)
task.step(zero_act)
task.reset_buf = torch.ones_like(task.reset_buf)

for _ in range(10):
    task.step(zero_act)
