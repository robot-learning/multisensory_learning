#!/usr/bin/env python
from isaacgym import gymapi  # Just to avoid gym complaining about torch imports

import sys
import os.path as osp
import argparse
import rospy
import numpy as np
from tqdm import tqdm

from ll4ma_isaacgym.core import SessionConfig, Simulator, MoveToPoseConfig
from ll4ma_isaacgym.behaviors import MoveToPose

from ll4ma_util import ros_util, file_util, ui_util

import torch


def get_target_obj(attrs):
    return next(iter(attrs['behavior_params'].values()))['target_object']


def get_behavior_params(attrs):
    return next(iter(attrs['behavior_params'].values()))


def set_obj_pose_params(sim, attrs, pos_xy_std=0.01):
    target_obj = get_target_obj(attrs)
    behavior_params = get_behavior_params(attrs)
    obj_pose_init = behavior_params['init_object_pose']
    px, py, pz = obj_pose_init[:3]
    qx, qy, qz, qw = obj_pose_init[3:]
    sim.config.env.objects[target_obj].position = [px, py, pz]
    sim.config.env.objects[target_obj].position_ranges = None
    sim.config.env.objects[target_obj].position_gaussian = [
        [px, pos_xy_std], [py, pos_xy_std], None
    ]
    sim.config.env.objects[target_obj].orientation = [qx, qy, qz, qw]
    sim.config.env.objects[target_obj].sample_angle_lower = -np.pi
    sim.config.env.objects[target_obj].sample_angle_upper =  np.pi

    
def set_robot_state(sim, data):
    init_idx = list(data['behavior']).index(args.behavior_init)
    sim.robot.set_init_joint_position(data['joint_position'][init_idx])


def get_target_ee_pose(attrs):
    behavior_params = get_behavior_params(attrs)
    return behavior_params['ee_end_pose']

    
def get_behavior(sim, attrs):
    target_obj = get_target_obj(attrs)
    orig_behavior_cfg = sim.config.task.behavior.behaviors[0]
    behavior_cfg = MoveToPoseConfig(config_dict={
        'name': 'push',
        'end_effector_frame': sim.robot.end_effector.get_link(),
        'max_plan_attempts': orig_behavior_cfg.max_plan_attempts,
        'planning_time': orig_behavior_cfg.planning_time,
        'max_vel_factor': orig_behavior_cfg.max_vel_factor,
        'max_acc_factor': orig_behavior_cfg.max_acc_factor,
        'ignore_error': orig_behavior_cfg.ignore_error,
        'cartesian_path': True,
        'min_cartesian_pct': orig_behavior_cfg.min_cartesian_pct,
        'disable_collisions': [target_obj]
    })
    behavior = MoveToPose(behavior_cfg, sim.robot, sim.config.env, sim)
    return behavior

    
if __name__ == '__main__':
    rospy.init_node('collect_distribution_data')
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--nominal_data_dir', type=str, required=True)
    parser.add_argument('--behavior_init', type=str, default='push_object:push',
                        help="Behavior name at which scene should be init to")
    parser.add_argument('-n', '--n_perturbations', type=int, default=20)
    parser.add_argument('--headless', action='store_true')
    args = parser.parse_args()

    metadata_fn = osp.join(args.nominal_data_dir, "metadata.yaml")
    metadata = file_util.load_yaml(metadata_fn)

    cfg_fn = metadata['config_filename']    
    cfg = SessionConfig(config_filename=cfg_fn)
    cfg.n_envs = args.n_perturbations
    cfg.sim.headless = args.headless

    fns = file_util.list_dir(args.nominal_data_dir, '.pickle')

    sim = Simulator(cfg)
    sim.collect_data = False  # I'm going to manually manage this

    save_dir = f"{args.nominal_data_dir}_perturbed"
    file_util.create_dir(save_dir)
    file_util.save_yaml(metadata, osp.join(save_dir, "metadata.yaml"))
    already_saved = file_util.list_dir(save_dir, '.pickle')

    
    for fn in tqdm(fns):
        save_fn_prefix = osp.basename(fn.replace('.pickle', ''))

        # Check if already saved perturbed data for this demo, if so no need to do it again
        perturbed_fns = [f for f in already_saved if save_fn_prefix in f]
        if len(perturbed_fns) == args.n_perturbations:
            continue
        
        data, attrs = file_util.load_pickle(fn)

        set_obj_pose_params(sim, attrs)
        set_robot_state(sim, data)
        sim.reset()
        sim.reset_dataset()
        
        target_ee_pose = get_target_ee_pose(attrs)
        behavior = get_behavior(sim, attrs)
        behavior.set_target_pose(target_ee_pose)
        traj, labels = behavior.get_trajectory(sim.get_env_state(0))

        if traj:
            aux_data = {'behavior': ['push_object:push' for _ in range(cfg.n_envs)]}
            sim._cache_step_data(aux_data)  # Get initial timestep
        
            for t, pt in enumerate(traj.points):
                action = torch.tensor(pt.positions).unsqueeze(0).repeat(cfg.n_envs, 1)
                action = torch.cat([action, torch.zeros(cfg.n_envs, 1)], dim=-1)
                sim.apply_actions(action)
                sim.step()

            for t in range(sim.config.task.extra_steps):
                sim.step()

            aux_data = {'behavior': ['push_object' for _ in range(cfg.n_envs)]}
            sim._cache_step_data(aux_data)  # Get final timestep

            # Saving original behavior params, init/target obj poses won't
            # be correct, but that's okay as long as you use data instead
            # of attrs for setting skill model data
            sim.log_env_attr('behavior_params',
                             [attrs['behavior_params'] for _ in range(cfg.n_envs)])
            
            for env_idx in range(cfg.n_envs):
                save_fn = osp.join(save_dir, f"{save_fn_prefix}_{env_idx+1:03d}.pickle")
                sim._save_pickle_data(env_idx, save_fn)
        else:
             rospy.logwarn(f"Couldn't get plan for {fn}")   

    ui_util.print_happy("\n  Distribution data collection complete.\n")
