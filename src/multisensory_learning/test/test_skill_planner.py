#!/usr/bin/env python

"""
This script is just a lightweight way to run the planner on the skill problem
to incrementally test the outputs of different parts in the pipeline
"""
import numpy as np
from multisensory_learning.learning import SkillLearnerConfig, SkillLearner
from multisensory_learning.planning import SkillProblem, SkillSequence
from ll4ma_util import file_util
from ll4ma_opt.solvers import CEM, MPPI
from ll4ma_opt.solvers.samplers import GaussianSampler


if __name__ == '__main__':
    learner_cp_fn = '/media/data_haro/push_cleaner_cp_mdn_comp-2_256-256/checkpoint_100.pt'
    file_util.check_path_exists(learner_cp_fn)
    
    learner_config = SkillLearnerConfig(checkpoint=learner_cp_fn)
    learners = {'push': SkillLearner(learner_config, set_eval=True, device='cpu')}
    skill_seq = SkillSequence(learners)
    problem = SkillProblem(skill_seq)
    problem.set_skeleton(['push', 'push'])
    problem.set_init_pose(np.array([0.,0.,0.]), np.array([0.,0.,0.,1.]))
    problem.set_goal_pose(np.array([1.,0.,0.]), np.array([0.,0.,0.,1.]))
    sampler = GaussianSampler(problem, start_iso_var=0.1)
    planner = CEM(problem, sampler)

    
    result = planner.optimize(verbose=False)
    soln = result.solution.squeeze()

    push1 = soln[:11]
    ee_pos1 = push1[:3]
    ee_ortho1 = push1[3:9]
    push_xy1 = push1[9:]
    
    push2 = soln[11:]
    ee_pos2 = push2[:3]
    ee_ortho2 = push2[3:9]
    push_xy2 = push2[9:]

    print("EE POS1", ee_pos1)
    print("PUSH XY1", push_xy1)

    print("EE POS2", ee_pos2)
    print("PUSH XY2", push_xy2)
    


    
    print("\nGOOD\n")
