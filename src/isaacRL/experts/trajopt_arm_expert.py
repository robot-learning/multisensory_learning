

class TrajoptArmExpert:
    """docstring for TrajoptArmExpert."""

    def __init__(
        self,
        action_size,
        planner='trajopt',
        action_repeats=2,
        max_iterations=150,
        num_joints=7,
        joint_names=[],
        ee_link='reflex_palm_link',
        grasp_value=1,
        ungrasp_value=-1,
        grasp_neutral=0,
        cartesian_path=False
    ):

        self.planner = planner
        self.action_repeats = action_repeats
        self.cartesian_path = cartesian_path

        self.num_joints = num_joints
        self.ee_link = ee_link
        self.action_size = self.num_joints + 1

        self.grasp_value = grasp_value
        self.ungrasp_value = ungrasp_value
        self.grasp_neutral = grasp_neutral

        self.timesteps = 10
        if self.cartesian_path:
            self.total_steps = self.timesteps
            self.interp_steps = 1
        else:
            self.total_steps = 140
            self.interp_steps = self.total_steps // self.timesteps
        self.max_iterations = max_iterations
        self.trajopt = TrajectoryOptimizer(
            timesteps=self.timesteps,
            alpha=0.5,
            rho=0.5,
            FP_PRECISION=1e-8,
            max_iterations=self.max_iterations,
            ee_link=self.ee_link,
            ee_trajectory=self.cartesian_path
        )
        self.trajopt.init_noise = 0.03
        self.temp_verbose = False
        # self.trajopt.verbose = True
        # self.error_type = 'position'
        self.error_type = 'pose'
        self.trajopt.error_type = self.error_type

        self._downward_ee = torch.tensor([0.07469838112592697, -0.6329322457313538, -0.042158689349889755, -0.7694410681724548])
        if grasp_offset is None:
            grasp_offset = torch.cat(
                [torch.tensor([0., 0., 0.1]), self._downward_ee]
            )
        self.grasp_offset = grasp_offset.unsqueeze(0)
        self.lift_offset = torch.tensor([0., 0., 0.2]).unsqueeze(0)
        self.set_offset = torch.tensor([0., 0., 0.1]).unsqueeze(0)

        self.grasp_steps = 80
        self.lift_steps = 80 # int(self.total_steps * 3 / 4) + 1

        if self.same_starts:
            self.setup_steps = 1 + self.grasp_steps + self.lift_steps
        else:
            self.setup_steps = self.total_steps + self.grasp_steps + self.lift_steps
        self.action_steps = self.total_steps + 30 + 0
        self.setdown_steps = self.grasp_steps + self.lift_steps

        self.jac_move_steps = 60
        self.jac_lift_steps = 50

        self._goal_key = 'goal_noisy'
        self._ee_key = 'ee_state'
        self._joint_key = 'joint_position'
        self._jac_key = 'jacobian'

    def _get_moveto_actions(self, state, target_pose, envis=None, interp=None, offset=None, start_joints=None):
        state = state['full_state']
        num_envs = state.shape[0]
        if envis is None:
            envis = torch.arange(num_envs)
        start_joints = state[self._joint_key][:,:self.num_joints].clone()

        if offset is not None:
            target_pose = target_pose.clone()
            target_pose[:,2] += offset[:,2].to(state.device)

        actions, info = self._get_trajectory(
            start_joints,
            target_pose,
            state,
            num_envs,
            envis,
            repeats=self.action_repeats,
            interp=interp
        )
        actions[:,:,-1] = self.grasp_neutral
        return actions

    def _get_moveabove_actions(self, state, target_pose, envis=None, interp=None):
        return self._get_moveto_actions(state, target_pose, envis, interp, self.set_offset)

    def _get_raise_actions(self, state, target_pose=None, envis=None, interp=None, start_joints=None):
        return self._get_moveto_actions(state, target_pose, envis, interp, self.lift_offset, start_joints)

    def _get_lower_actions(self, state, target_pose=None, envis=None, interp=None, start_joints=None):
        return self._get_moveto_actions(state, target_pose, envis, interp, -self.set_offset, start_joints)

    def _get_grasp_actions(self, last_action, envis=None, grasp_value=self.grasp_value):
        actions = torch.zeros(
            (self.grasp_steps, num_envs, self.action_size),
            device=state.device, dtype=start_joints.dtype
        )
        actions[:,:,:self.num_joints] = last_action[-1:,:,:self.num_joints].clone()
        actions[:,:,-1] = grasp_value
        if envis is None:
            envis = torch.arange(num_envs)
        return actions

    def _get_ungrasp_actions(self, last_action, envis=None):
        return _get_grasp_actions(last_action, envis, self.ungrasp_value)

    def _get_trajectory(
        self,
        start_joints,
        target_pose,
        obj_state,
        num_envs,
        envis,
        identical=False,
        repeats=1,
        interp=None
    ):
        if interp is None:
            interp = self.interp_steps
        if self.cartesian_path:
            interp = 1
        actions = torch.zeros(
            (interp*self.timesteps, num_envs, self.action_size),
            device=start_joints.device, dtype=start_joints.dtype
        )
        actions[:,:,-1] = self.grasp_neutral
        infos = []
        err_types = ['pose' for _ in range(len(target_pose))]
        if self.error_type == 'position':
            ik_resp, success = moveit_util.get_ik(
                target_pose.cpu().numpy(), self.ee_link, 5
            )
            sols = [
                torch.tensor(sol.position, dtype=target_pose.dtype, device=target_pose.device)
                    if ik_resp.solution_found[si] else target_pose[si]
                        for si, sol in enumerate(ik_resp.solutions)
            ]
            err_types = [
                'position'
                    if ik_resp.solution_found[si] else 'pose'
                        for si, sol in enumerate(ik_resp.solutions)
            ]
            target_pose = torch.stack(sols)
        for ei in envis:
            if not identical or ei == 0:
                trajectory, info = self.trajopt.get_trajectory(
                    start_joints[ei].cpu(),
                    target_pose[ei].cpu(),
                    obj_state,
                    interp=interp,
                    repeats=repeats,
                    goal_ee_type=err_types[ei]
                )
                infos.append(info)
            actions[:,ei,:self.num_joints] = trajectory[:,:].to(actions.device)
        if identical:
            return actions, info
        return actions, infos

    def _get_controls(
        self,
        start_pose,
        target_pose,
        jacobian,
        obj_state,
        envis,
        identical=False,
    ):
        return jacobian_controller.get_action(start_pose, target_pose, jacobian), {}
