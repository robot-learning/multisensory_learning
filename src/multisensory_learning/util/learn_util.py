import torch
import numpy as np

from ll4ma_util import math_util, torch_util


def rot_to_ortho6d(R, ortho6d_learner):
    if R.dim() == 3:
        R = R.flatten(1)  # Turn 3x3 into a 9-vector (obeying batch dim)
    with torch.no_grad():
        ortho6d = ortho6d_learner.apply_models({'rotation': R.float()}, True)['ortho6d']
    return ortho6d


def quat_to_ortho6d(q, ortho6d_learner):
    R = torch_util.quat_to_rotation(q)
    ortho6d = rot_to_ortho6d(R, ortho6d_learner)
    return ortho6d


def vec_to_pos_ortho6d(v):
    if v.dim() == 1:
        unbatch = True
        v = v.unsqueeze(0)
    else:
        unbatch = False
    if v.size(-1) != 9:
        raise ValueError("Expected vector to have size 9 (3D pos and 6D ortho) "
                         f"but got {v.size(-1)}")
    pos = v[:,:3]
    ortho6d = v[:,3:]
    return (pos.squeeze(0), ortho6d.squeeze(0)) if unbatch else (pos, ortho6d)


def vec_to_pos_rot(v):
    pos, ortho6d = vec_to_pos_ortho6d(v)
    if ortho6d.dim() == 1:
        rot = torch_util.ortho6d_to_rotation(ortho6d.unsqueeze(0)).squeeze(0)
    else:
        rot = torch_util.ortho6d_to_rotation(ortho6d)
    return pos, rot


def vec_to_homogeneous(v):
    pos, rot = vec_to_pos_rot(v)
    if pos.dim() == 1:
        T = torch.eye(4)
        T[:3,:3] = rot
        T[:3,3] = pos
    else:
        T = torch.eye(4).unsqueeze(0).repeat(pos.size(0), 1, 1)
        T[:,:3,:3] = rot
        T[:,:3,3] = pos
    return T


def homogeneous_to_vec(T, ortho6d_learner):
    if T.dim() == 2:
        T = T.unsqueeze(0)  # Add batch dim
    pos = math_util.homogeneous_to_position(T)
    rot = math_util.homogeneous_to_rotation(T)
    ortho6d = rot_to_ortho6d(rot, ortho6d_learner)
    return torch.cat([pos, ortho6d], dim=-1)


def pose_to_vec(p, q, ortho6d_learner):
    if not torch.is_tensor(p):
        p = torch.tensor(p)
    if not torch.is_tensor(q):
        q = torch.tensor(q)
    T = math_util.pose_to_homogeneous(p, q)
    return homogeneous_to_vec(T, ortho6d_learner)


def compose_vecs(ref_vec, delta_vec, ortho6d_learner):
    w_T_ref = vec_to_homogeneous(ref_vec)
    ref_T_obj = vec_to_homogeneous(delta_vec)
    w_T_obj = torch.bmm(w_T_ref, ref_T_obj)
    return homogeneous_to_vec(w_T_obj, ortho6d_learner)


def cov_from_planar_samples(
        ref_quat,
        ortho6d_learner,
        min_pos=[-0.01, -0.01, 0.],
        max_pos=[ 0.01,  0.01, 0.],
        sample_axis=[0, 0, 1],
        min_angle=-np.pi,
        max_angle= np.pi,
        n_samples=200
):
    random_poses = [
        math_util.uniform_random_planar_pose(
            min_pos,
            max_pos,
            sample_axis,
            min_angle,
            max_angle
        )
        for _ in range(n_samples)
    ]

    vecs = []
    for p, rand_q in random_poses:
        q = math_util.quat_mul(rand_q, ref_quat)
        ortho6d = quat_to_ortho6d(
            torch.tensor(q).unsqueeze(0),
            ortho6d_learner
        ).squeeze().numpy()
        vecs.append(np.concatenate([p, ortho6d]))
    vecs = np.array(vecs)

    cov = np.cov(vecs.T)
    cov += np.eye(cov.shape[0]) * 1e-8  # Make PSD
    return cov
