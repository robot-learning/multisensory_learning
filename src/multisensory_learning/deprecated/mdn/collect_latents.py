
import sys, os
import os.path as osp
import pickle
import numpy as np
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset

from utils.process_observations import process_data_in
from ll4ma_isaacgym.oracles.above_object_oracle import AboveObjectOracle
# from oracles.above_object_oracle import AboveObjectOracle
from multisensory_learning.util import data_util, torch_util, msp_util, func_util
from utils.latent_models import LatentModel

# def process_data_in(data, modality, data_ranges):
#     if modality == 'rgb':
#         data = data.transpose(0, 3, 1, 2) # (t, 3, h, w)
#         data = torch.tensor(data, dtype=torch.float32)
#         data = F.interpolate(data, (64, 64), mode='bilinear', align_corners=False)
#         data.div_(255) # Normalize to range [0, 1]
#     elif modality == 'depth':
#         low, high = data_ranges[modality]
#         data = data_util.scale_min_max(data, low, high, 0, 1)
#         data = torch.tensor(data, dtype=torch.float32).unsqueeze(1) # Add channel: (t, 1, h, w)
#         data = F.interpolate(data, (64, 64), mode='bilinear', align_corners=False) # (t, 1, 64, 64)
#     elif modality in data_ranges:
#         low, high = data_ranges[modality]
#         data = data_util.scale_min_max(data, low, high, 0, 1)
#         data = torch.tensor(data, dtype=torch.float32)
#     else:
#         raise ValueError(f"Unknown modality for processing data into network: {modality}")
#     return data


latent_checkpt = '../multisensory_learning/checkpoints/msp_epoch_200.pt'
root = '/home/iain/data/isaac_sims/pickles/'
savedir = '/home/iain/data/isaac_sims/latent_states/'
batch_size = 1
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
oracle = AboveObjectOracle()
# obs_mods = [ 'rgb', 'joint_position', 'joint_velocity', 'joint_torque' ]
# obs_mods = [ 'rgb', 'joint_position' ]

latent_model = LatentModel(latent_checkpt, device)
obs_modalities = latent_model.config['enc_obs_modalities']
# obs_modalities.extend( m
#   for m in latent_model.config['dec_obs_modalities']
#     if m not in obs_modalities )
act_modalities = latent_model.config['act_modalities']


data_ranges = {
    'joint_position': [-2*np.pi, 2*np.pi],
    'joint_velocity': [-3.5, 3.5],
    'joint_torque': [-35., 35.],
    'delta_joint_position': [-0.4, 0.4],
    'depth': [-3., 0.],
    'ee_position': [-3.5, 3.5],
    'ee_velocity': [-4.5, 4.5],
    'obj_position': [-3.5, 3.5],
    'obj_velocity': [-4.5, 4.5]
}
# Buffers to put on data ranges, accounts for test data that might be slightly out of
# data range seen in training, but generally we assume training data is accurate
# representation of true data distribution ranges
data_range_buffers = {
    'joint_position': 0.0,
    'joint_velocity': 0.5,
    'joint_torque': 5.0,
    'delta_joint_position': 0.1,
    'depth': 0.1,
    'ee_position': 0.5,
    'ee_velocity': 0.5,
    'obj_position': 0.5,
    'obj_velocity': 0.5
}
# Some modalities can be symmetrically positive and negative
data_range_symmetrical = {
    'joint_position': True,
    'joint_velocity': True,
    'joint_torque': True,
    'delta_joint_position': True,
    'depth': False,
    'ee_position': True,
    'ee_velocity': True,
    'obj_position': True,
    'obj_velocity': True
}

with torch.no_grad():
    fileps = os.listdir(root)
    for filep in tqdm(fileps):
        filepath = osp.join(root, filep)
        with open(filepath, 'rb') as f:
            data, attrs = pickle.load(f)
            goals = oracle.generate_labels(data, 'box')

            for modality in data_ranges:
                if modality == 'delta_joint_position':
                    joint_pos = data['joint_position']
                    deltas = joint_pos[1:] - joint_pos[:-1]
                    data_min = np.min(deltas)
                    data_max = np.max(deltas)
                elif modality == 'obj_position':
                    data_min = float("inf")
                    data_max = -float("inf")
                    for obj_name, obj_data in data['objects'].items():
                        data_min = min(data_min, np.min(obj_data['position']).item())
                        data_max = max(data_max, np.max(obj_data['position']).item())
                elif modality == 'obj_velocity':
                    data_min = float("inf")
                    data_max = -float("inf")
                    for obj_name, obj_data in data['objects'].items():
                        data_min = min(data_min, np.min(obj_data['velocity']).item())
                        data_max = max(data_max, np.max(obj_data['velocity']).item())
                else:
                    data_min = np.min(data[modality]).item()
                    data_max = np.max(data[modality]).item()
                data_ranges[modality][0] = min(data_ranges[modality][0], float(data_min))
                data_ranges[modality][1] = max(data_ranges[modality][1], float(data_max))

            for modality, (low, high) in data_ranges.items():
                pad = data_range_buffers[modality]
                if data_range_symmetrical[modality]:
                    high = max([abs(v) for v in data_ranges[modality]])
                    low = -high
                data_ranges[modality] = [low - pad, high + pad]

            obs = {}
            act = {}
            for m in obs_modalities:
                if m == 'obj_position':
                    for obj_name, obj_data in data['objects'].items():
                        if attrs['objects'][obj_name]['fix_base_link']:
                            continue
                        obs_data = obj_data['position']
                        obs[f'{obj_name}_obj_position'] = msp_util.process_data_in(obs_data, m, data_ranges)
                elif m == 'obj_velocity':
                    for obj_name, obj_data in data['objects'].items():
                        if attrs['objects'][obj_name]['fix_base_link']:
                            continue
                        obs_data = obj_data['velocity']
                        obs[f'{obj_name}_obj_velocity'] = msp_util.process_data_in(obs_data, m, data_ranges)
                else:
                    obs_data = data[m]
                    obs[m] = msp_util.process_data_in(obs_data, m, data_ranges)
            for m in act_modalities:
                if m == 'delta_joint_position':
                    joint_pos = data['action']['joint_position']
                    first = joint_pos[0]
                    joint_pos = np.concatenate([np.expand_dims(first, 0), joint_pos], axis=0)
                    act[m] = msp_util.process_data_in(
                      joint_pos[1:] - joint_pos[:-1], m, data_ranges )
                else:
                    act_data = data['action'][m]
                    act[m] = msp_util.process_data_in(act_data, m, data_ranges)

            xdata = {'obs': obs, 'act': act}
            # for xdata in
            # torch_util.move_batch_to_device(xdata, device)
            for key in xdata['obs']:
                # print(key, ':', xdata['obs'][key].shape)
                xdata['obs'][key] = xdata['obs'][key].unsqueeze(0)
                # print(key, ':', xdata['obs'][key].shape)
            for key in xdata['act']:
                # print(key, ':', xdata['act'][key].shape)
                xdata['act'][key] = xdata['act'][key].unsqueeze(0)
                # print(key, ':', xdata['act'][key].shape)
            ldata = latent_model(xdata)
        savedat = {
          'state': ldata['encoded_obs'].squeeze(1).cpu().numpy(),
          'next_state': ldata['next_latent'].squeeze(1).cpu().numpy(),
          'targets': goals
        }
        savepath = osp.join(savedir, filep)
        np.save(savepath, savedat)
