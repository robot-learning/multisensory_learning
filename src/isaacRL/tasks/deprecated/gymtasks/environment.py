
from isaacgym import gymtorch, gymapi

import os
import math
import random
import torch
import torch.nn.functional as F
import numpy as np
from copy import deepcopy

from geometry_msgs.msg import Pose, TransformStamped
from tf2_msgs.msg import TFMessage
from sensor_msgs.msg import JointState
from std_msgs.msg import ColorRGBA, String
from ll4ma_isaacgym.msg import IsaacGymState

from isaacRL.tasks.gymtasks.env_state import EnvState
from ll4ma_isaacgym.core import EnvironmentState
from ll4ma_isaacgym.core import util as gym_util
from ll4ma_isaacgym.robots import Robot
from ll4ma_util import ui_util, vis_util, ros_util, file_util, math_util


class Environment:
    """
    Base Environment Class for all Isaacgym Simulators, RL tasks, etc.
    """

    def __init__(self, config, headless=False, device='cpu', rl_device='cpu'):
        self.config = config
        self.headless = headless
        self.device = device
        self.rl_device = rl_device

        self.gym = gymapi.acquire_gym()

        self.force_render = True
        self.control_freq_inv = self.config.control_freq_inv
        self.reset_wait_steps = self.config.reset_wait_steps
        self.num_envs = self.config.num_envs
        self.timestep = 0
        self.allow_resets = True
        self.dt = self.config.sim.dt

        self.randomize = self.config.randomize
        self.randomize_robot = False

        self.num_objects = len(self.config.objects)
        self.num_robots = 1 # TODO change for multiple robots

        # Track additional data that is env-specific (e.g. object colors) for logging
        self.env_obj_data = [
            {obj_name: {} for obj_name in self.config.objects.keys()}
                for _ in range(self.num_envs)
        ]

        # TODO for now only one robot
        robot_config = next(iter(self.config.robots.values()))
        self.robot = Robot(robot_config, self.num_envs)
        self.ee_name = self.robot.end_effector.get_link()

        self.num_acts = self.config.control_size
        self.additive = self.config.additive_actions
        # TODO: add options for cartesian actions
        self.cartesian_control = self.config.cartesian_control
        self.vecolity_control = self.config.vecolity_control

        self.action_scale = self.config.action_scale
        self.dof_vel_scale = self.config.dof_vel_scale

        self.allow_state_updates = True

        # TODO for now just assume one camera. This is easy to change just need to make
        # rgb/depth dicts in dataset with sensor_name keys, but it will change data loading
        # so holding off for a moment
        if len(self.config.sensors) > 1:
            raise ValueError("Only one camera sensor currently supported")
        self.sensor_config = next(iter(self.config.sensors.values()))
        self.rgb_size = self.config.img_size

        self.create_sim()
        self.gym.prepare_sim(self.sim)
        self.set_viewer()

        # self.full_preset_states = None
        # if isinstance(self.randomize, str) and self.randomize != 'inf':
        #     self.add_preset_state(self.randomize)

        self.reset_num = 0
        self._setup_values()

    def refresh(self):
        self.gym.refresh_rigid_body_state_tensor(self.sim)
        self.gym.refresh_dof_state_tensor(self.sim)
        self.gym.refresh_dof_force_tensor(self.sim)
        self.gym.refresh_force_sensor_tensor(self.sim)
        self.gym.refresh_jacobian_tensors(self.sim)

    def render(self, mode="rgb_array"):
        """Draw the frame to the viewer, and check for keyboard events."""
        if self.viewer:
            # check for window closed
            if self.gym.query_viewer_has_closed(self.viewer):
                sys.exit()

            # check for keyboard events
            for evt in self.gym.query_viewer_action_events(self.viewer):
                if evt.action == "QUIT" and evt.value > 0:
                    sys.exit()
                elif evt.action == "toggle_viewer_sync" and evt.value > 0:
                    self.enable_viewer_sync = not self.enable_viewer_sync

            # fetch results
            if self.device != 'cpu':
                self.gym.fetch_results(self.sim, True)

            # step graphics
            if self.enable_viewer_sync:
                self.gym.step_graphics(self.sim)
                self.gym.draw_viewer(self.viewer, self.sim, True)

                # Wait for dt to elapse in real time.
                # This synchronizes the physics simulation with the rendering rate.
                self.gym.sync_frame_time(self.sim)

            else:
                self.gym.poll_viewer_events(self.viewer)

            # if self.virtual_display and mode == "rgb_array":
            #     img = self.virtual_display.grab()
            #     return np.array(img)

    def step(self, actions=None):
        """
        Apply an action to the simulator and take a step, updating all variables,
        data, and returning the current state. If actions is None, will simply
        update the state.
        Args:
            actions: (n,a) tensor of actions to apply - default=None
                     (n = number of envs, a = size of the action space)
        Returns:
            The current state as defined by the self.get_state function
        """
        # apply actions
        self.pre_physics_step(actions)

        # physics step
        for i in range(self.control_freq_inv):
            if self.force_render:
                self.render()
            self.gym.simulate(self.sim)

        if self.device == "cpu":
            self.gym.fetch_results(self.sim, True)

        self.refresh()

        # post physics
        self.post_physics_step()

        return self.get_state()

    def pre_physics_step(self, actions):
        if actions is None:
            actions = self.stationary_actions()
        ajnts = self.n_arm_joints
        self.actions[:] = actions[:]
        self.robot_dof_targets[:,ajnts:] = \
        self.robot.end_effector.update_action_joint_pos(
            self.actions[:,ajnts:], self.robot_dof_pos[:,ajnts:]
        )
        if self.vecolity_control:
            self.robot_dof_targets[:,:ajnts] = (
                self.actions[:,:ajnts] * self.robot_dof_max_vel[:ajnts] * 0.5
            )
            self.gym.set_dof_velocity_target_tensor(
                self.sim, gymtorch.unwrap_tensor(self.robot_dof_targets)
            )
        else:
            if self.additive:
                scaled_actions = self.scale_action(self.actions)[:,:ajnts]
                self.robot_dof_targets[:,:ajnts] = (
                    self.robot_dof_pos[:,:ajnts] + scaled_actions
                )
            else:
                self.robot_dof_targets[:,:ajnts] = actions[:,:ajnts]
            self.robot_dof_targets[:] = torch.clamp(
                self.robot_dof_targets[:],
                self.robot_dof_lower_limits, self.robot_dof_upper_limits
            )
            self.gym.set_dof_position_target_tensor(
                self.sim, gymtorch.unwrap_tensor(self.robot_dof_targets)
            )

    def post_physics_step(self):
        self.progress_buf += 1
        self.timesteps += 1
        envis = self.reset_buf.nonzero(as_tuple=False).squeeze(-1)
        if len(envis) > 0 and self.allow_resets:
            self.reset_idx(envis)
        self.refresh()
        if self.allow_state_updates:
            self.update_states()

    def update_states(self):
        """
        Update the fullstate values to reflect the isaacgym simulated values
        """

        self.fullstate['joint_position'] = self.robot_dof_pos.clone().to(self.rl_device)
        self.fullstate['joint_velocity'] = self.robot_dof_vel.clone().to(self.rl_device)
        self.fullstate['joint_force'] = self.robot_dof_for.clone().to(self.rl_device)
        self.fullstate['joint_pos_scaled'] = \
            self.scale_joint_position(self.robot_dof_pos).to(self.rl_device)
        self.fullstate['joint_vel_scaled'] = (
            self.robot_dof_vel  * self.dof_vel_scale
        ).to(self.rl_device)
        self.fullstate['prev_action'] = self.actions.clone().to(self.rl_device)
        self.fullstate['ee_state'] = self.get_robot_ee().to(self.rl_device)
        self.fullstate['arm_joints'] = self.n_arm_joints_torch.unsqueeze(-1).to(self.rl_device)
        self.fullstate['jacobian'] = \
            self.ee_jacobian.clone().flatten(1).to(self.rl_device)

        noise_i = 0
        for obji, (obj_name, obj_cfg) in enumerate(self.config.objects.items()):
            self.fullstate[obj_name] = \
                self.object_roots[:,obji].clone().flatten(1)[:,:7].to(self.rl_device)
            self.fullstate[obj_name+'_init'] = \
                self.object_resets.clone().flatten(1)[:,:7].to(self.rl_device)

            if obj_cfg.noise_type is not None:
                if obj_cfg.noise_type == 'episodic':
                    self.fullstate[obj_name+'_noisy'][:,:7] = (
                        self.fullstate[obj_name][:,:7]
                        + self.object_noises[:,noise_i,:]
                    )
                    self.fullstate[obj_name+'_init_noisy'][:,:7] = (
                        self.fullstate[obj_name+'_init'][:,:7]
                        + self.object_noises[:,noise_i,:]
                    )
                else: # stepwise
                    object_noise = torch.randn_like(
                        self.fullstate[obj_name+'_noisy'][:,:7]
                    ) * obj_cfg.noise
                    self.fullstate[obj_name+'_noisy'][:,:7] = (
                        self.fullstate[obj_name][:,:7]
                        + object_noise
                    )
                    self.fullstate[obj_name+'_init_noisy'][:,:7] = (
                        self.fullstate[obj_name+'_init'][:,:7]
                        + object_noise
                    )
                noise_i += 1

    def update_resets(self, reset):
        self.reset_buf[:] = torch.where(
            reset,
            torch.ones_like(self.reset_buf),
            self.reset_buf
        )

    def get_state(self):
        return self.fullstate

    def get_robot_ee(self, simple=False):
        return self.rigid_body_states[:, self.ee_handle][:, 0:7]

    def stationary_actions(self):
        return self.zero_actions() if self.additive else self.same_actions()

    def zero_actions(self):
        return torch.zeros_like(self.actions)

    def same_actions(self):
        actions = self.robot_dof_targets[:,:self.num_acts].clone()
        actions[:,self.n_arm_joints:] *= 0.
        return actions

    def scale_action(self, actions):
        actions[:,:self.n_arm_joints] = actions[:,:self.n_arm_joints] * self.dt * self.action_scale
        return actions

    def scale_joint_position(self, joint_pos):
        if joint_pos.device == self.robot_dof_lower_limits_rl.device:
            return (
                2.0 * (joint_pos - self.robot_dof_lower_limits_rl[:joint_pos.shape[1]])
                / (self.robot_dof_upper_limits_rl[:joint_pos.shape[1]] - self.robot_dof_lower_limits_rl[:joint_pos.shape[1]])
                - 1.0
            )
        return (
            2.0 * (joint_pos - self.robot_dof_lower_limits[:joint_pos.shape[1]])
            / (self.robot_dof_upper_limits[:joint_pos.shape[1]] - self.robot_dof_lower_limits[:joint_pos.shape[1]])
            - 1.0
        )

    def reset(self):
        """
        Reset the simulator envs and dataset.
        """
        self.reset_idx(torch.arange(self.num_envs))
        # Make sure env is setup by stepping a handful of timesteps
        for _ in range(self.reset_wait_steps):
            self.step()
        return self.get_state()

    def reset_idx(self, env_ids):

        self.reset_robots(env_ids)
        self.reset_objects(env_ids)

        multi_env_ids_int32 = self.global_indices[env_ids, 2:].flatten()
        # self.gym.set_actor_root_state_tensor(
        #   self.sim, gymtorch.unwrap_tensor(roots.clone()),
        # )
        self.gym.set_actor_root_state_tensor_indexed(
            self.sim, gymtorch.unwrap_tensor(self.root_state_tensor),
            gymtorch.unwrap_tensor(multi_env_ids_int32), len(multi_env_ids_int32)
        )

        multi_env_ids_int32 = self.global_indices[env_ids, :1].flatten()
        self.gym.set_dof_position_target_tensor_indexed(
            self.sim, gymtorch.unwrap_tensor(self.robot_dof_targets),
            gymtorch.unwrap_tensor(multi_env_ids_int32), len(multi_env_ids_int32)
        )
        self.gym.set_dof_state_tensor_indexed(
            self.sim, gymtorch.unwrap_tensor(self.dof_state),
            gymtorch.unwrap_tensor(multi_env_ids_int32), len(multi_env_ids_int32)
        )

        self.gym.simulate(self.sim)

        self.reset_num += self.reset_buf.sum().item()
        self.progress_buf[env_ids] *= 0
        self.reset_buf[env_ids] *= 0

    def reset_robots(self, env_ids):
        """
        Updates the following tensors which will then be sent to isaacgym
        in the reset_idx function.
        self.root_state_tensor
        self.robot_dof_targets
        self.dof_state
        """
        pos = self.robot_default_dof_pos.unsqueeze(0).clone()
        if self.randomize != 0 and self.randomize_robot:
            # TODO: define what should happen if the robot needs to be initialized to a random position each reset
            pass
        else:
            self.robot_dof_pos[env_ids, :] = pos
            self.robot_dof_vel[env_ids, :] = torch.zeros_like(
                self.robot_dof_vel[env_ids]
            )
            self.robot_dof_targets[env_ids, :] = pos

    def reset_objects(self, env_ids):
        """
        Updates the following tensors which will then be sent to isaacgym
        in the reset_idx function.
        self.root_state_tensor
        self.robot_dof_targets
        self.dof_state
        """
        object_resets = self.root_state_tensor[env_ids]
        obj_ids = self.robj_ids+self.num_robots
        if self.randomize == 0:
            object_resets[:,obj_ids] = self.object_defaults[env_ids][:,self.robj_ids]
        elif self.randomize == 'inf':
            object_resets[:,obj_ids] = torch.zeros_like(
                self.object_defaults[env_ids][:,self.robj_ids]
            )
            object_resets[:,obj_ids] += self.object_bias[self.robj_ids]
            object_resets[:,obj_ids] += \
                self.object_ranges[self.robj_ids] * torch.rand_like(object_resets)
        elif self.randomize == 'preset':
            if not self._use_preset:
                self._preset_idx = np.random.choice(
                    np.arange(self._num_presets), size=env_ids.shape, replace=False
                )
            object_resets[:,obj_ids] = self._preset_states[self._preset_idx]
            self._use_preset = False
        elif self.randomize > 0:
            object_resets[:,obj_ids] = torch.zeros_like(
                self.object_defaults[env_ids][:,self.robj_ids]
            )
            object_resets[:,obj_ids] += self.object_bias[self.robj_ids]
            num_secs = self.randomize
            rangvals = torch.linspace(0,1,num_secs).to(self.device)
            idx = torch.randint(high=num_secs,size=(len(env_ids)*13,))
            rands = rangvals[idx].reshape(-1,1,13)
            object_resets[:,obj_ids] += self.object_ranges[self.robj_ids] * rands
        else:
            print('Unknown randomization value:', self.randomize)
        self.object_resets[env_ids] = object_resets[:,obj_ids].clone()
        self.root_state_tensor[env_ids] = object_resets

        # # reset the object colors and noise if the noise is episodic
        # noi = 0
        # for obj_name, obj_cfg in self.config.objects.items():
        #     if obj_cfg.noise_type == 'episodic':
        #         self.object_noises[env_ids, noi, :] = torch.randn_like(
        #             self.object_noises[env_ids, noi, :]
        #         )[:] * obj_cfg.noise
        #         noi += 1

        # # reset the object colors
        # for obj_name, cfg in self.config.env.objects.items():
        #     asdf

    def set_viewer(self):
        """Create the viewer."""

        self.enable_viewer_sync = True
        self.viewer = None

        # if running with a viewer, set up keyboard shortcuts and camera
        if self.headless == False:
            # subscribe to keyboard shortcuts
            self.viewer = self.gym.create_viewer(
                self.sim, gymapi.CameraProperties())
            self.gym.subscribe_viewer_keyboard_event(
                self.viewer, gymapi.KEY_ESCAPE, "QUIT")
            self.gym.subscribe_viewer_keyboard_event(
                self.viewer, gymapi.KEY_V, "toggle_viewer_sync")

            # set the camera position based on up axis
            sim_params = self.gym.get_sim_params(self.sim)
            if sim_params.up_axis == gymapi.UP_AXIS_Z:
                cam_pos = gymapi.Vec3(4, 3, 2)
                cam_target = gymapi.Vec3(-4, -3, 0)
            else:
                cam_pos = gymapi.Vec3(20.0, 3.0, 25.0)
                cam_target = gymapi.Vec3(10.0, 0.0, 15.0)

            # Point viewer camera at middle env
            look_at_env = self.envs[0]
            if self.num_envs > 10:
                n_per_row = int(np.sqrt(self.num_envs))
                look_at_env = self.envs[self.num_envs // 2 + n_per_row // 2]
            self.gym.viewer_camera_look_at(
              self.viewer, look_at_env, cam_pos, cam_target
            )

    def create_sim(self):
        """
        Creates simulation (including all envs) and prepares tensors.
        """
        self.seg_id_dict = {}
        self.current_seg_id = 1
        self.sim = gym_util.get_default_sim(self.config.sim)
        self._create_ground_plane()
        self._create_envs()

    def _create_ground_plane(self):
        plane_params = gymapi.PlaneParams()
        plane_params.normal = gymapi.Vec3(0.0, 0.0, 1.0)
        self.gym.add_ground(self.sim, plane_params)

    def _get_random_object(self):
        random_object = random.choice(self.random_objects)
        self.env_states[env_idx].object_mesh_filenames[obj_name] = random_object[
            "filename"
        ]
        self.env_states[env_idx].object_mesh_roots[obj_name] = random_object["root"]
        return random_object["asset"]

    def random_obj_color(self, rgb_color=None, form='tensor'):
        cr = np.random.uniform(0, 1) if rgb_color is None else rgb_color[0]
        cg = np.random.uniform(0, 1) if rgb_color is None else rgb_color[1]
        cb = np.random.uniform(0, 1) if rgb_color is None else rgb_color[2]
        return [cr, cg, cb]

    def _create_envs(self):
        """
        Create simulation assets and assign things to each environment.
        """
        spacing = self.config.spacing # self.cfg["env"]['envSpacing']
        num_per_row = int(np.sqrt(self.num_envs))
        lower = gymapi.Vec3(-spacing, 0.0, -spacing)
        upper = gymapi.Vec3(spacing, spacing, spacing)

        self._create_object_assets()
        robot_asset, robot_dof_props, robot_start_pose = self._create_robot_assets()

        # compute aggregate size
        max_agg_bodies = self.num_robot_bodies + self.num_object_bodies
        max_agg_shapes = self.num_robot_shapes + self.num_object_shapes

        self.robots = []
        self.envs = []
        self.object_defaults = torch.zeros(
            (self.num_envs, self.num_objects, 13), device=self.device
        )
        self.object_defaults[:,:] += self.object_bias[:]
        self.object_defaults[:] += self.object_ranges[:] * 0.5

        # Reset rigid-body indices in case they were loaded previously from file
        for obj_cfg in self.config.objects.values():
            obj_cfg.rb_indices = []

        for env_idx in range(self.num_envs):
            env = self.gym.create_env(self.sim, lower, upper, num_per_row)
            self.envs.append(env)

            self.gym.begin_aggregate(
                env, max_agg_bodies, max_agg_shapes, True
            )

            robot_actor = self.gym.create_actor(
                env,
                robot_asset,
                robot_start_pose,
                self.robot.get_name(),
                env_idx,
                2,
                0
            )
            self.gym.enable_actor_dof_force_sensors(env, robot_actor)
            self.gym.set_actor_dof_properties(
                env, robot_actor, robot_dof_props
            )
            if env_idx == 0:
                self.robot.joint_names = self.gym.get_actor_dof_names(
                    env, robot_actor
                )

            for oi, (obj_name, obj_cfg) in enumerate(self.config.objects.items()):
                if obj_name not in self.seg_id_dict:
                    self.seg_id_dict[obj_name] = self.current_seg_id
                    self.current_seg_id += 1

                obj_pose = gymapi.Transform()
                obj_pose.p = gymapi.Vec3(*self.object_defaults[env_idx,oi,:3])
                obj_pose.r = gymapi.Quat(*self.object_defaults[env_idx,oi,3:7])
                obj_asset = self._get_random_object() \
                    if obj_cfg.object_type == "random" \
                    else self.obj_assets[obj_name]
                obj_actor = self.gym.create_actor(
                    env,
                    obj_asset,
                    obj_pose,
                    obj_name,
                    env_idx,
                    0,
                    self.seg_id_dict[obj_name],
                )
                if obj_cfg.dofs > 0:
                    self.gym.set_actor_dof_properties(
                      env, obj_actor, obj_cfg.dof_props
                    )
                if obj_cfg.set_color:
                    color = self.random_obj_color(obj_cfg.rgb_color)
                    self.gym.set_rigid_body_color(
                        env, obj_actor, 0, gymapi.MESH_VISUAL_AND_COLLISION,
                        gymapi.Vec3(*color)
                    )
                    self.env_obj_data[env_idx][obj_name]['color'] = color

                # Get global index of object in rigid body state tensor
                rb_idx = self.gym.get_actor_rigid_body_index(
                    env, obj_actor, 0, gymapi.DOMAIN_SIM
                )
                obj_cfg.rb_indices.append(rb_idx)

            self.robot_link_names = []
            self.robot_rb_dict = self.gym.get_actor_rigid_body_dict(env, robot_actor)
            for link_name, rb_idx in self.robot_rb_dict.items():
                # Semantic segmentation IDs
                if link_name not in self.seg_id_dict:
                    self.seg_id_dict[link_name] = self.current_seg_id
                    self.current_seg_id += 1
                self.gym.set_rigid_body_segmentation_id(
                    env, robot_actor, rb_idx, self.seg_id_dict[link_name]
                )
                self.robot.add_rb_index(link_name, rb_idx)  # Track RB indices (e.g. for contacts)
                self.robot_link_names.append(link_name)

                self.robot.joint_names = self.gym.get_actor_dof_names(env, robot_actor)

            if self.robot.has_end_effector():
                ee_link = self.robot.end_effector.get_link()
                ee_idx = self.gym.find_actor_rigid_body_index(
                    env, robot_actor, ee_link, gymapi.DOMAIN_SIM
                )
                self.robot.end_effector.add_rb_index(ee_idx)

            self.gym.end_aggregate(env)

            self._add_sensors(env)

            self.seg_ids = list(self.seg_id_dict.values())
            self.seg_colors = vis_util.random_colors(len(self.seg_ids))

        self.ee_handle = self.gym.find_actor_rigid_body_handle(
            env, robot_actor, self.ee_name
        )

    def _add_sensors(self, env_ptr):
        if not hasattr(self, 'sensor_tensors'):
            self.sensor_tensors = {}
        for sensor_name, sensor_cfg in self.config.sensors.items():
            if sensor_cfg.sensor_type == 'camera':
                sensor_props = gymapi.CameraProperties()
                sensor_props.enable_tensors = True
                sensor_props.width = self.sensor_config.width
                sensor_props.height = self.sensor_config.height
                sensor_props.horizontal_fov = self.sensor_config.horizontal_fov
                sensor_handle = self.gym.create_camera_sensor(env_ptr, sensor_props)
                # front camera
                # sensor_origin = gymapi.Vec3(*[1.1, 0.0, 1.3])
                # sensor_target = gymapi.Vec3(*[0.0, 0.0, 0.4])
                # back camera
                # sensor_origin = gymapi.Vec3(*[-.01, 0.6, 1.15])
                # sensor_target = gymapi.Vec3(*[1.0, -.5, 0.45])
                sensor_origin = gymapi.Vec3(*self.sensor_config.origin)
                sensor_target = gymapi.Vec3(*self.sensor_config.target)
                self.gym.set_camera_location(
                    sensor_handle, env_ptr, sensor_origin, sensor_target
                )
                self.sensor_handle = sensor_handle

                if sensor_cfg.use_rgb:
                    rgb_tensor = self.gym.get_camera_image_gpu_tensor(
                        self.sim, env_ptr, sensor_handle, gymapi.IMAGE_COLOR
                    )
                    rgb_tensor_th = gymtorch.wrap_tensor(rgb_tensor)
                    if sensor_name+'_rgb' not in self.sensor_tensors:
                        self.sensor_tensors[sensor_name+'_rgb'] = []
                    self.sensor_tensors[sensor_name+'_rgb'].append(rgb_tensor_th)
                if sensor_cfg.use_depth and False: # TODO test that depth works correctly
                    depth_tensor = self.gym.get_camera_image_gpu_tensor(
                        self.sim, env_ptr, sensor_handle, gymapi.IMAGE_DEPTH
                    )
                    depth_tensor_th = gymtorch.wrap_tensor(depth_tensor)
                    self.sensor_tensors[sensor_name+'_depth'].append(depth_tensor_th)
            else:
                raise NotImplementedError(
                    'No other senser types are supported [yours:{}]'.\
                    format(sensor_cfg.sensor_type)
                )

    def _create_object_assets(self):
        """
        Creates object assets specified in config.
        """
        self.num_object_bodies = 0
        self.num_object_shapes = 0
        self.num_object_dofs = 0
        self.obj_assets = {}
        self.obj_rb_dict = {}
        # stores idx of objects that will be reset (allows to ignore the table)
        self.robj_ids = []
        self.object_bias = torch.zeros(
            (self.num_objects, 13), device=self.device
        )
        self.object_ranges = torch.zeros(
            (self.num_objects, 13), device=self.device
        )
        self.object_noises = []
        for oi, (obj_name, obj_cfg) in enumerate(self.config.objects.items()):
            if not obj_cfg.fix_base_link:
                self.robj_ids.append(oi)
            if obj_cfg.noise_type is not None:
                self.object_noises.append(torch.zeros(7, device=self.rl_device))
            if obj_cfg.extents_ranges is not None:
                for i, extents_range in enumerate(obj_cfg.extents_ranges):
                    if extents_range is not None:
                        obj_cfg.extents[i] = np.random.uniform(*extents_range)

            for i in range(len(obj_cfg.position)):
                self.object_bias[oi,i] = obj_cfg.position[i]
            if self.randomize != 0:
                if obj_cfg.position_ranges is not None:
                    pranges = obj_cfg.position_ranges
                    for i in range(len(pranges)):
                        self.object_ranges[oi,i] = (pranges[i][1] - pranges[i][0])
                        self.object_bias[oi,i] = pranges[i][0]
                elif obj_name != 'table':
                    self.object_bias[oi,:3] -= self.object_noise
                    self.object_ranges[oi,:3] += self.object_noise*2
            if obj_cfg.orientation is None:
                self.object_bias[oi,6] = 1
            else:
                for i in range(4):
                    self.object_bias[oi,i+3] = obj_cfg.orientation[i]

            if obj_cfg.object_type == "random":
                continue

            options = gymapi.AssetOptions()
            if obj_cfg.density is not None:
                options.density = obj_cfg.density
            options.fix_base_link = obj_cfg.fix_base_link
            if obj_cfg.object_type == "box":
                asset = self.gym.create_box(
                    self.sim,
                    obj_cfg.extents[0],
                    obj_cfg.extents[1],
                    obj_cfg.extents[2],
                    options
                )
            elif obj_cfg.object_type == "urdf":
                options.override_com = True
                options.override_inertia = True
                options.vhacd_enabled = obj_cfg.vhacd_enabled
                asset_root = ros_util.resolve_ros_package_path(obj_cfg.asset_root)
                asset = self.gym.load_asset(
                    self.sim, asset_root, obj_cfg.asset_filename, options
                )
            else:
                raise ValueError(
                    f"Unknown object type for simulation: {obj_cfg.object_type}"
                )

            self.obj_assets[obj_name] = asset
            self.obj_rb_dict[obj_name] = self.gym.get_asset_rigid_body_dict(asset)
            obj_cfg.bodies = self.gym.get_asset_rigid_body_count(asset)
            obj_cfg.shapes = self.gym.get_asset_rigid_shape_count(asset)
            obj_cfg.dofs = self.gym.get_asset_dof_count(asset)
            obj_cfg.dof_props = self.gym.get_asset_dof_properties(asset)
            for i in range(obj_cfg.dofs):
                obj_cfg.dof_props['damping'][i] = 10.0

            self.num_object_bodies += obj_cfg.bodies
            self.num_object_shapes += obj_cfg.shapes
            self.num_object_dofs += obj_cfg.dofs

        self.robj_ids = torch.tensor(self.robj_ids, dtype=torch.long)
        if len(self.object_noises) > 0:
            self.object_noises = torch.stack(
                self.object_noises*self.num_envs
            ).reshape(self.num_envs,-1,7)
        else:
            self.object_noises = torch.zeros(
                (self.num_envs, 0, 7), device=self.rl_device
            )

        if self.config.random_objects.assets_root is not None:
            cfg = self.config.random_objects
            path = ros_util.resolve_ros_package_path(cfg.assets_root)
            asset_list = [os.path.basename(f) for f in file_util.list_dir(path, ".urdf")]
            if len(asset_list) == 0:
                raise ValueError(f"Random objects asset directory: {path} has no URDF objects.")

            if cfg.loaded_subset_size:
                for i in range(cfg.loaded_subset_size):
                    if len(asset_list) == 0:
                        raise ValueError(
                            f"Random objects asset directory: {path} is "
                            "not large enough to create a unique subset "
                            f"of size: {cfg.loaded_subset_size}"
                        )

                    randasset = asset_list[random.randint(0, len(asset_list) - 1)]
                    asset_list.remove(randasset)
                    options = gymapi.AssetOptions()
                    options.fix_base_link = True
                    options.override_com = True
                    options.override_inertia = True
                    options.vhacd_enabled = True
                    asset = self.gym.load_asset(self.sim, path, randasset, options)

                    self.random_objects.append(
                        {"asset": asset, "root": cfg.assets_root, "filename": randasset}
                    )
            else:
                for randasset in asset_list:
                    options = gymapi.AssetOptions()
                    options.fix_base_link = False
                    options.override_com = True
                    options.override_inertia = True
                    options.vhacd_enabled = True
                    asset = self.gym.load_asset(self.sim, path, randasset, options)

                    self.random_objects.append(
                        {"asset": asset, "root": cfg.assets_root, "filename": randasset}
                    )

    def _create_robot_assets(self):
        """
        Creates robot assets specified in config.

        TODO Assuming there is only one robot for now, need to modify for bimanual
        """
        asset_options = gymapi.AssetOptions()
        asset_options.armature = self.robot.config.armature
        asset_options.fix_base_link = self.robot.config.fix_base_link
        asset_options.disable_gravity = self.robot.config.disable_gravity
        asset_options.flip_visual_attachments = self.robot.config.flip_visual_attachments
        robot_asset = self.gym.load_asset(
            self.sim,
            self.config.sim.asset_root,
            self.robot.config.asset_filename,
            asset_options
        )

        self.robot_rb_dict = self.gym.get_asset_rigid_body_dict(robot_asset)
        self.num_robot_bodies = self.gym.get_asset_rigid_body_count(robot_asset)
        self.num_robot_shapes = self.gym.get_asset_rigid_shape_count(robot_asset)
        self.num_robot_dofs = self.gym.get_asset_dof_count(robot_asset)

        # set robot dof properties
        robot_dof_props = self.gym.get_asset_dof_properties(robot_asset)
        robot_dof_props["driveMode"].fill(gymapi.DOF_MODE_POS)

        self.robot_dof_lower_limits = torch.tensor(
            robot_dof_props['lower'][:self.num_robot_dofs], device=self.device
        )
        self.robot_dof_lower_limits_rl = self.robot_dof_lower_limits.to(self.rl_device)
        self.robot_dof_upper_limits = torch.tensor(
            robot_dof_props['upper'][:self.num_robot_dofs], device=self.device
        )
        self.robot_dof_upper_limits_rl = self.robot_dof_upper_limits.to(self.rl_device)
        self.robot_dof_max_vel = torch.tensor(
            robot_dof_props['velocity'][:self.num_robot_dofs], device=self.device
        ) # radians per second

        self.n_arm_joints = self.robot.arm.num_joints()
        self.n_arm_joints_torch = torch.tensor([self.n_arm_joints]*self.num_envs, dtype=torch.float)
        n_ee_joints = self.robot.end_effector.num_joints()
        robot_dof_props["stiffness"][:self.n_arm_joints] = self.robot.arm.config.stiffness
        robot_dof_props["damping"][:self.n_arm_joints] = self.robot.arm.config.damping
        if self.robot.config.end_effector:
            robot_dof_props["stiffness"][self.n_arm_joints:] = \
                self.robot.end_effector.config.stiffness
            robot_dof_props["damping"][self.n_arm_joints:] = \
                self.robot.end_effector.config.damping

        self.robot_dof_speed_scales = torch.ones_like(
            self.robot_dof_lower_limits
        ) * self.action_scale
        self.robot_dof_speed_scales[self.n_arm_joints:] = 0.1
        robot_dof_props['effort'][self.n_arm_joints:] = 200

        robot_start_pose = gymapi.Transform()
        robot_start_pose.p = gymapi.Vec3(*self.robot.config.position)
        robot_start_pose.r = gymapi.Quat(*self.robot.config.orientation)

        return robot_asset, robot_dof_props, robot_start_pose

    def _setup_values(self):
        # get gym GPU state tensors
        actor_root_state_tensor = self.gym.acquire_actor_root_state_tensor(self.sim)
        dof_state_tensor = self.gym.acquire_dof_state_tensor(self.sim)
        _dof_force = self.gym.acquire_dof_force_tensor(self.sim)
        rigid_body_tensor = self.gym.acquire_rigid_body_state_tensor(self.sim)
        jacobians = self.gym.acquire_jacobian_tensor(self.sim, self.robot.get_name())

        self.refresh()

        # create some wrapper tensors for different slices
        self.robot_default_dof_pos = torch.zeros(
            self.num_robot_dofs, device=self.device
        )
        self.robot_default_dof_pos[:self.n_arm_joints] = \
            torch.tensor(self.robot.arm.get_default_joint_position())
        self.robot_default_dof_pos[self.n_arm_joints:] = \
            torch.tensor(self.robot.end_effector.get_default_joint_position())

        self.dof_state = gymtorch.wrap_tensor(dof_state_tensor)
        self.robot_dof_state = \
            self.dof_state.view(self.num_envs, -1, 2)[:, :self.num_robot_dofs]
        self.robot_dof_pos = self.robot_dof_state[..., 0]
        self.robot_dof_vel = self.robot_dof_state[..., 1]
        self.robot_dof_for = gymtorch.wrap_tensor(
            _dof_force
        ).view(self.num_envs, -1)

        self.rigid_body_states = gymtorch.wrap_tensor( rigid_body_tensor )
        self.root_state_tensor = gymtorch.wrap_tensor(
            actor_root_state_tensor
        ).view(self.num_envs, -1, 13)

        self.rigid_body_states = self.rigid_body_states.view(self.num_envs, -1, 13)

        self.object_state = self.rigid_body_states[:, self.num_robot_bodies:]
        self.object_roots = self.root_state_tensor[:,1:]

        self.num_dofs = self.gym.get_sim_dof_count(self.sim) // self.num_envs
        self.robot_dof_targets = torch.zeros(
            (self.num_envs, self.num_robot_dofs),
            dtype=torch.float, device=self.device
        )

        jacobians = gymtorch.wrap_tensor( jacobians )
        self.ee_jacobian = jacobians[:,self.ee_handle-1,:,:self.n_arm_joints]
        print("JAC", self.ee_jacobian.shape)

        self.actions = torch.zeros(
            (self.num_envs, self.num_acts), device=self.device
        )

        self._setup_fullstate()

        self.object_resets = self.root_state_tensor[:,self.robj_ids].clone()

        # get the indices for all objects
        self.global_indices = torch.arange(
            self.num_envs * (self.num_robots + self.num_objects),
            dtype=torch.int32, device=self.device
        ).view(self.num_envs, -1)

        self.timesteps = 0

        self.reset_buf = torch.zeros(
            self.num_envs, device=self.device, dtype=torch.long
        )
        self.progress_buf = torch.zeros(
            self.num_envs, device=self.device, dtype=torch.long
        )

    def _setup_fullstate(self):
        self.state_idx = {}
        min_idx = 0
        max_idx = min_idx + self.robot_dof_pos.shape[1]
        self.state_idx['joint_position'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + self.robot_dof_vel.shape[1]
        self.state_idx['joint_velocity'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + self.robot_dof_for.shape[1]
        self.state_idx['joint_force'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + self.robot_dof_pos.shape[1]
        self.state_idx['joint_pos_scaled'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + self.robot_dof_vel.shape[1]
        self.state_idx['joint_vel_scaled'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + self.get_robot_ee().shape[1]
        self.state_idx['ee_state'] = torch.arange(min_idx, max_idx)
        min_idx = max_idx
        max_idx = min_idx + 1
        self.state_idx['arm_joints'] = torch.arange(min_idx, max_idx)

        min_idx = max_idx
        # 6 * 7 = 42
        max_idx = min_idx + self.ee_jacobian.shape[1] * self.n_arm_joints
        self.state_idx['jacobian'] = torch.arange(min_idx, max_idx)

        space_dof = 7
        for obj_name in self.config.objects:
            min_idx = max_idx
            max_idx = min_idx + space_dof
            self.state_idx[obj_name+'_init'] = torch.arange(min_idx, max_idx)
            min_idx = max_idx
            max_idx = min_idx + space_dof
            self.state_idx[obj_name+'_noisy'] = torch.arange(min_idx, max_idx)
            min_idx = max_idx
            max_idx = min_idx + space_dof
            self.state_idx[obj_name+'_init_noisy'] = torch.arange(min_idx, max_idx)
            min_idx = max_idx
            max_idx = min_idx + space_dof
            self.state_idx[obj_name] = torch.arange(min_idx, max_idx)

        min_idx = max_idx
        max_idx = min_idx + self.actions.shape[1]
        self.state_idx['prev_action'] = torch.arange(min_idx, max_idx)
        self.fullstate = EnvState(
            self.state_idx, max_idx, self.num_envs, device=self.rl_device
        )
        # self.fullstate = torch.zeros((self.num_envs, max_idx), device=self.rl_device)

    def destroy_sim(self):
        """
        Destroys the active simulator.
        """
        if self.viewer is not None:
            self.gym.destroy_viewer(self.viewer)
        self.gym.destroy_sim(self.sim)
