
from isaacgym import gymtorch, gymapi

import numpy as np
import os
import torch
import rospy

from isaacgymenvs.utils.torch_jit_utils import *
from isaacgymenvs.tasks.base.vec_task import VecTask

from isaacRL.utils.goalies import select_goalie

def test():
    config = {
      'name': 'TestTask',
      'env': {
        'numEnvs': 128, 'envSpacing': 1.5, 'episodeLength': 50, 'enableDebugVis': False, 'startPositionNoise': 0.0, 'startRotationNoise': 0.0, 'aggregateMode': 3, 'actionScale': 7.5, 'dofVelocityScale': 0.1, 'distRewardScale': 1.5, 'rotRewardScale': 0.5, 'aroundHandleRewardScale': 1.0, 'stackRewardScale': 4.0, 'fingerDistRewardScale': 10.0, 'actionPenaltyScale': 0.01, 'asset': {'assetRoot': '/home/iain/workspace/isaacgym/assets', 'assetFileNameFranka': 'urdf/franka_description/robots/franka_panda.urdf', 'assetFileNameCabinet': 'urdf/sektion_cabinet_model/urdf/sektion_cabinet_2.urdf'}, 'spacing': 1.0, 'objects': {'block': {'object_type': 'box', 'extents': [0.08, 0.08, 0.08], 'friction': 0.4, 'restitution': 0.0, 'density': 2000.0, 'fix_base_link': False, 'position': [0.65, 0.0, 0.65], 'orientation': [0, 0, 0, 1]}, 'table': {'object_type': 'box', 'rgb_color': [0.52, 0.49, 0.41], 'extents': [0.5, 1.0, 0.6], 'friction': 0.2, 'density': 1000.0, 'fix_base_link': True, 'position': [0.7, 0.0, 0.3025], 'orientation': [0, 0, 0, 1]}}, 'sensors': {'camera': {'sensor_type': 'camera', 'origin': [1.5, 0, 1.5], 'target': [-1.5, 0, 0]}}, 'numObservations': 41, 'numActions': 8
      },
      'sim': {
        'dt': 0.0166, 'substeps': 1, 'up_axis': 'z', 'gravity': [0.0, 0.0, -9.81], 'physx': {'num_threads': 4, 'solver_type': 1, 'num_position_iterations': 12, 'num_velocity_iterations': 1, 'contact_offset': 0.005, 'rest_offset': 0.0, 'bounce_threshold_velocity': 0.2, 'max_depenetration_velocity': 1000.0, 'default_buffer_size_multiplier': 5.0, 'max_gpu_contact_pairs': 1048576, 'always_use_articulations': False, 'contact_collection': 0, 'use_gpu': True, 'num_subscenes': 0}, 'use_gpu_pipeline': True
      },
      'max_env_steps': 50,
      'physics_engine': 'physx',
      'type': 'GeneralIiwa', 'num_envs': 128, 'cartesian_control': False, 'add_actions': True, 'vecolity_control': False, 'headless': False, 'num_threads': 4, 'subscenes': 0, 'init_wait_steps': 0, 'prev_states': 1, 'steps_per_action': 1, 'num_obs': 41, 'num_acts': 8, 'act_scale': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0], 'action_scale': 7.5, 'cliprange': 0.2, 'act_range': [-1.0, 1.0], 'use_gpu': True,
      'goal': {
        'type': 'Move2ObjectShaped',
        'object': 'block',
        'offset': [0., 0., 0.08],
        'part_ws': [5.0,0.0,0.01],
        'threshold': 0.05,
        'include_rot': True,
      }
    }

    task = TestTask(
      cfg=config,
      sim_device='cuda:0',
      graphics_device_id=0,
      headless=False
    )
    zero_act = task.zero_actions()
    for _ in range(20):
        task.step(zero_act)

    task.reset_buf = torch.ones_like(task.reset_buf)
    task.step(zero_act)
    task.reset_buf = torch.ones_like(task.reset_buf)

    for _ in range(20):
        task.step(zero_act)

class TestTask(VecTask):

    def __init__(self, cfg, sim_device, graphics_device_id, headless):
        self.checker = False
        self.num_steps = 0
        self.cfg = cfg
        self.num_robots = 1
        self.num_objects = 2
        self.robot_name = 'robot'

        self.cfg['device'] = sim_device
        self.goalie = select_goalie(self.cfg)

        self.additive = self.cfg['add_actions'] if 'add_actions' in self.cfg else False
        self.cartesian = self.cfg['cartesian_control'] if 'cartesian_control' in self.cfg else False
        self.vecolity_control = self.cfg['vecolity_control'] if 'vecolity_control' in self.cfg else False
        self.inv_threshold = 0.00001

        self.max_episode_length = self.cfg['max_env_steps']
        self.debug_viz = self.cfg["env"]["enableDebugVis"]
        self.dof_vel_scale = self.cfg["env"]["dofVelocityScale"]

        self.ee_name = "reflex_pad_link" # "iiwa_link_7"

        self.up_axis = "z"
        self.up_axis_idx = 2

        self.distX_offset = 0.04
        self.dt = 1/60.

        super().__init__(
          config=self.cfg, sim_device=sim_device,
          graphics_device_id=graphics_device_id, headless=headless
        )

        self.setup_values()
        self.reset_idx(torch.arange(self.num_envs, device=self.device))

    def refresh(self, refresh_state=True):
        self.gym.refresh_actor_root_state_tensor(self.sim)
        self.gym.refresh_dof_state_tensor(self.sim)
        self.gym.refresh_dof_force_tensor(self.sim)
        self.gym.refresh_rigid_body_state_tensor(self.sim)
        self.gym.refresh_jacobian_tensors(self.sim)
        if refresh_state:
            self.update_env_states()

    def reset_idx(self, env_ids):
        env_ids_int32 = env_ids.to(dtype=torch.int32)

        pos = self.robot_default_dof_pos.unsqueeze(0).clone()
        pos[:,self.n_arm_joints:] *= 0.
        self.robot_dof_pos[env_ids, :] = pos
        self.robot_dof_vel[env_ids, :] = torch.zeros_like(
          self.robot_dof_vel[env_ids] )
        self.robot_dof_targets[env_ids, :] = pos

        # reset objects
        roots = self.root_state_tensor.clone()
        roots[env_ids, 1] = self.default_table_states[env_ids]
        roots[env_ids, 2] = self.default_block_states[env_ids]

        self.gym.set_actor_root_state_tensor(
          self.sim, gymtorch.unwrap_tensor(roots.clone()),
        )

        self.gym.set_dof_position_target_tensor(
          self.sim, gymtorch.unwrap_tensor(self.robot_dof_targets)
        )

        self.gym.set_dof_state_tensor(
          self.sim, gymtorch.unwrap_tensor(self.dof_state)
        )

        self.progress_buf[env_ids] *= 0
        self.reset_buf[env_ids] *= 0

        # self.gym.simulate(self.sim)

    def compute_observations(self):

        self.refresh()

        self.obs_buf = torch.cat( (
          self.env_state['joint_pos_scaled'],
          self.env_state['joint_vel_scaled'],
          self.env_state['ee_state'],
          self.env_state['goal_dist'],
          self.object_state[:,[1],:3].flatten(1)
        ), dim=-1)
        return self.obs_buf

    def compute_reward(self):
        ee_pose = self.env_state['ee_state'][:,:7]
        ee_dist = torch.norm(ee_pose[:,:3] - self.goals[:,:3], p=2, dim=-1)
        ee_reward = 1.0 / torch.exp(ee_dist)
        action_penalty = torch.sum(self.actions ** 2, dim=-1)
        rewards = 5.0 * ee_reward - 0.01 * action_penalty
        resets = torch.zeros_like(self.reset_buf)
        # rewards, resets, ee_dist = self.goalie.reward(self.env_state, self.goals)

        self.rew_buf[:] = rewards
        self.goal_dist[:] = ee_dist
        self.goal_resets[:] = resets[:]

        self.update_resets(resets > 0)
        self.update_resets(self.progress_buf >= self.max_episode_length - 1)

        self.extras['goal_dist'] = self.goal_dist[:].clone()
        self.extras['rewards'] = self.rew_buf[:].clone()
        self.extras['gdist_mean'] = self.goal_dist[:].mean()
        self.extras['reward_mean'] = self.rew_buf[:].mean()

    def update_resets(self, reset):
        self.reset_buf[:] = torch.where(
          reset,
          torch.ones_like(self.reset_buf),
          self.reset_buf
        )

    def scale_action(self, action):
        scale_action = action.clone()
        scale_action[:,:self.n_arm_joints] = (
          scale_action[:,:self.n_arm_joints] # the joint actions
          * self.robot_dof_max_vel[:self.n_arm_joints] # radians per second
          # * self.dt # scale by the framerate
          # * 10.
        )
        return scale_action

    def pre_physics_step(self, actions):
        if actions.shape[1] < self.num_acts:
            temp = torch.zeros_like(self.actions)
            temp[:,:actions.shape[1]] = actions[:,:]
            actions = temp
        self.actions[:] = actions[:] * self.action_speed_scales # scale for cartesian controls to limit distance traveled per timestep
        # self.actions[self.reset_buf>0] *= 0.
        if self.cartesian and self.additive:
            actions = self.cartesian2joints( actions )
        ajnts = self.n_arm_joints
        # self.robot_dof_targets[:,ajnts:] = self.robot.end_effector.update_action_joint_pos(
        #   actions[:,ajnts:], self.robot_dof_pos[:,ajnts:]
        # )
        if self.vecolity_control:
            # actions = self.dt * actions
            self.robot_dof_targets[:,:ajnts] = (
              actions[:,:ajnts] * self.robot_dof_max_vel[:ajnts]
            )
            self.gym.set_dof_velocity_target_tensor(
              self.sim, gymtorch.unwrap_tensor(self.robot_dof_targets)
            )
        else:
            if self.additive:
                scale_action = self.scale_action(self.actions)
                self.robot_dof_targets[:,:ajnts] = (
                  self.robot_dof_pos[:,:ajnts] + scale_action[:,:ajnts]
                )
            else:
                self.robot_dof_targets[:,:ajnts] = actions[:,:ajnts]
                # self.robot_dof_targets[self.reset_buf==0,:ajnts] = actions[self.reset_buf==0,:ajnts]
            self.robot_dof_targets[:] = torch.clamp(
              self.robot_dof_targets[:],
              self.robot_dof_lower_limits, self.robot_dof_upper_limits
            )
            self.gym.set_dof_position_target_tensor(
              self.sim, gymtorch.unwrap_tensor(self.robot_dof_targets)
            )

    def post_physics_step(self):
        self.progress_buf += 1
        self.num_steps += 1
        envis = torch.arange(self.num_envs)[
          self.reset_buf.nonzero(as_tuple=False).squeeze(-1) ]
        if len(envis) > 0:
            print('envis:', envis)
            self.reset_idx(envis)
        self.compute_observations()
        self.compute_reward()

    def get_robot_ee(self, simple=False):
        return self.rigid_body_states[:, self.ee_handle][:, 0:7]

    def update_env_states(self, extract_goal=False):
        self.env_state = self._get_env_state(extract_goal)

    def get_env_states(self):
        return self.env_state

    def _get_env_state(self, extract_goal=False):
        """
        Get values from the environment that we can use for learning
        """
        envstate = {}
        envstate['joint_position'] = self.robot_dof_pos.clone()
        envstate['joint_velocity'] = self.robot_dof_vel.clone()
        envstate['joint_force'] = self.robot_dof_for.clone()
        envstate['joint_pos_scaled'] = (
          2.0 * (envstate['joint_position'] - self.robot_dof_lower_limits)
          / (self.robot_dof_upper_limits - self.robot_dof_lower_limits)
          - 1.0
        )
        envstate['joint_vel_scaled'] = (
          envstate['joint_velocity']  * self.dof_vel_scale
        )
        envstate['prev_action'] = self.actions.clone()
        envstate['ee_state'] = self.get_robot_ee()

        # obji = 0
        # for obji, (obj_name, obj_cfg) in enumerate(self.objects.items()):
        #     envstate[obj_name] = self.object_roots[:,obji].clone().flatten(1)
        envstate['table'] = self.object_roots[:,0].clone().flatten(1)
        envstate['block'] = self.object_roots[:,1].clone().flatten(1)

        # self.goals[:] = self.goalie.extract_goal(envstate)
        self.goals[:] = envstate['block'][:,:7] + torch.tensor([0.,0.,0.1,0.,0.,0.,0.],device=self.device)
        envstate['goal'] = self.goals.clone()
        envstate['goal_dist'] = torch.zeros_like(self.goals)
        envstate['goal_current'] = torch.zeros_like(self.goals)
        # envstate['goal_dist'] = self.goalie.goal_dist(envstate, self.goals)
        # envstate['goal_current'] = self.goalie.extract_current(envstate)

        return envstate

    def create_sim(self):
        self.seg_id_dict = {}
        self.current_seg_id = 1
        self.sim_params.up_axis = gymapi.UP_AXIS_Z
        self.sim_params.gravity.x = 0
        self.sim_params.gravity.y = 0
        self.sim_params.gravity.z = -9.81
        self.sim = super().create_sim(
          self.device_id, self.graphics_device_id,
          self.physics_engine, self.sim_params
        )
        self._create_ground_plane()
        self._create_envs( self.num_envs,
          self.cfg["env"]['envSpacing'], int(np.sqrt(self.num_envs))
        )

    def _create_ground_plane(self):
        plane_params = gymapi.PlaneParams()
        plane_params.normal = gymapi.Vec3(0.0, 0.0, 1.0)
        self.gym.add_ground(self.sim, plane_params)

    def _create_envs(self, num_envs, spacing, num_per_row):
        lower = gymapi.Vec3(-spacing, -spacing, 0.0)
        upper = gymapi.Vec3(spacing, spacing, spacing)

        asset_root = '/home/iain/isaac_ws/src'
        # asset_root = os.path.dirname(ros_util.get_path('ll4ma_robots_description'))
        # print(asset_root)
        # import pdb; pdb.set_trace()
        # robot_asset_file = 'll4ma_robots_description/urdf/panda/static/panda_base_static.urdf'
        robot_asset_file = 'll4ma_robots_description/urdf/iiwa/static/iiwa_reflex_static.urdf'

        # if "asset" in self.cfg["env"]:
        #     asset_root = os.path.join(os.path.dirname(os.path.abspath(__file__)), self.cfg["env"]["asset"].get("assetRoot", asset_root))
        #     robot_asset_file = self.cfg["env"]["asset"].get("assetFileNameFranka", robot_asset_file)

        # load robot asset
        asset_options = gymapi.AssetOptions()
        asset_options.armature = 0.01
        asset_options.flip_visual_attachments = False
        asset_options.fix_base_link = True
        # asset_options.collapse_fixed_joints = True
        asset_options.disable_gravity = True
        asset_options.thickness = 0.001
        asset_options.default_dof_drive_mode = gymapi.DOF_MODE_POS
        robot_asset = self.gym.load_asset(self.sim, asset_root, robot_asset_file, asset_options)

        # create table/block assets
        num_blocks = 2
        block_height = 0.08
        table_height = 0.6
        asset_options = gymapi.AssetOptions()
        asset_options.density = 2000.0
        asset_options.fix_base_link = False
        block_asset = self.gym.create_box(
          self.sim, block_height, block_height, block_height, asset_options
        )
        asset_options.fix_base_link = True
        table_asset = self.gym.create_box(
          self.sim, 0.5, 1.0, table_height, asset_options
        )

        robot_dof_stiffness = to_torch(
          [400, 400, 400, 400, 400, 400, 400, 1.0e6, 1.0e6],
          dtype=torch.float, device=self.device
        )
        robot_dof_damping = to_torch(
          [80, 80, 80, 80, 80, 80, 80, 1.0e2, 1.0e2],
          dtype=torch.float, device=self.device
        )

        self.robot_rb_dict = self.gym.get_asset_rigid_body_dict(robot_asset)
        self.num_robot_bodies = self.gym.get_asset_rigid_body_count(robot_asset)
        self.num_robot_shapes = self.gym.get_asset_rigid_shape_count(robot_asset)
        self.num_robot_dofs = self.gym.get_asset_dof_count(robot_asset)
        self.body_names = self.gym.get_asset_rigid_body_names(robot_asset)

        print("num robot bodies: ", self.num_robot_bodies)
        print("num robot dofs: ", self.num_robot_dofs)
        print("robot bodies: ", self.body_names)

        # set robot dof properties
        robot_dof_props = self.gym.get_asset_dof_properties(robot_asset)
        robot_dof_props["driveMode"].fill(gymapi.DOF_MODE_POS)

        self.robot_dof_lower_limits = torch.tensor(
          robot_dof_props['lower'][:self.num_robot_dofs], device=self.device
        )
        self.robot_dof_upper_limits = torch.tensor(
          robot_dof_props['upper'][:self.num_robot_dofs], device=self.device
        )
        self.robot_dof_max_vel = torch.tensor(
          robot_dof_props['velocity'][:self.num_robot_dofs], device=self.device
        ) # radians per second

        self.n_arm_joints = 7
        robot_dof_props["stiffness"][:self.n_arm_joints] = [400.0, 400.0, 400.0, 400.0, 400.0, 400.0, 400.0]
        robot_dof_props["damping"][:self.n_arm_joints] = [80.0, 80.0, 80.0, 80.0, 80.0, 80.0, 80.0]
        robot_dof_props["stiffness"][self.n_arm_joints:] = [400.0, 400.0, 400.0, 400.0, 400.0]
        robot_dof_props["damping"][self.n_arm_joints:] = [80.0, 80.0, 80.0, 80.0, 80.0]
        self.robot_dof_speed_scales = torch.ones_like(
          self.robot_dof_lower_limits
        ) # max velocity value
        self.robot_dof_speed_scales[self.n_arm_joints:] = 0.1
        robot_dof_props['effort'][self.n_arm_joints:] = 200

        robot_start_pose = gymapi.Transform()
        robot_start_pose.p = gymapi.Vec3(0.0, 0.0, 0.0025)
        robot_start_pose.r = gymapi.Quat(0.0, 0.0, 0.0, 1.0)

        z_pos = table_height / 2. + 0.0025
        table_start_pose = gymapi.Transform()
        table_start_pose.p = gymapi.Vec3(0.7, 0., z_pos)
        z_pos = table_height + (block_height / 2. + 0.0025) + 0.025
        block_start_pose = gymapi.Transform()
        block_start_pose.p = gymapi.Vec3(0.7, 0., z_pos)

        # compute aggregate size
        num_table_bodies = self.gym.get_asset_rigid_body_count(table_asset)
        num_table_shapes = self.gym.get_asset_rigid_shape_count(table_asset)
        num_block_bodies = self.gym.get_asset_rigid_body_count(block_asset)
        num_block_shapes = self.gym.get_asset_rigid_shape_count(block_asset)
        max_agg_bodies = self.num_robot_bodies + num_table_bodies + num_block_bodies
        max_agg_shapes = self.num_robot_shapes + num_table_shapes + num_block_shapes

        self.robots = []
        self.tables = []
        self.default_table_states = []
        self.blocks = []
        self.default_block_states = []
        self.envs = []

        for i in range(self.num_envs):
            # create env instance
            env_ptr = self.gym.create_env(
                self.sim, lower, upper, num_per_row
            )
            self.gym.begin_aggregate(env_ptr, max_agg_bodies, max_agg_shapes, True)

            robot_actor = self.gym.create_actor(
              env_ptr, robot_asset, robot_start_pose, self.robot_name, i, 1, 0
            )
            self.gym.set_actor_dof_properties(env_ptr, robot_actor, robot_dof_props)
            if i == 0:
                self.joint_names = self.gym.get_actor_dof_names(
                  env_ptr, robot_actor
                )

            block_pose = block_start_pose
            block_actor = self.gym.create_actor(
              env_ptr, block_asset, block_pose, "block", i, 0, 0
            )
            self.default_block_states.append( [
              block_pose.p.x, block_pose.p.y, block_pose.p.z,
              block_pose.r.x, block_pose.r.y, block_pose.r.z,
              block_pose.r.w, 0, 0, 0, 0, 0, 0
            ] )
            self.gym.set_rigid_body_color(
              env_ptr, block_actor, 0, gymapi.MESH_VISUAL_AND_COLLISION,
              gymapi.Vec3(0.,0.,1.)
            )

            table_pose = table_start_pose
            table_actor = self.gym.create_actor(
              env_ptr, table_asset, table_pose, "table", i, 0, 0
            )
            self.default_table_states.append( [
              table_pose.p.x, table_pose.p.y, table_pose.p.z,
              table_pose.r.x, table_pose.r.y, table_pose.r.z,
              table_pose.r.w, 0, 0, 0, 0, 0, 0
            ] )
            self.gym.set_rigid_body_color(
              env_ptr, table_actor, 0, gymapi.MESH_VISUAL_AND_COLLISION,
              gymapi.Vec3(0.52, 0.49, 0.41)
            )

            self.gym.end_aggregate(env_ptr)

            self.envs.append(env_ptr)
            self.robots.append(robot_actor)

        self.default_table_states = to_torch(
          self.default_table_states, device=self.device, dtype=torch.float
        ).view( self.num_envs, 13 )
        self.default_block_states = to_torch(
          self.default_block_states, device=self.device, dtype=torch.float
        ).view( self.num_envs, 13 )

        self.ee_handle = self.gym.find_actor_rigid_body_handle(
          env_ptr, robot_actor, self.ee_name
        )

    def set_viewer(self):
        """Create the viewer."""

        # todo: read from config
        self.enable_viewer_sync = True
        self.viewer = None

        # if running with a viewer, set up keyboard shortcuts and camera
        if self.headless == False:
            # subscribe to keyboard shortcuts
            self.viewer = self.gym.create_viewer(
                self.sim, gymapi.CameraProperties())
            self.gym.subscribe_viewer_keyboard_event(
                self.viewer, gymapi.KEY_ESCAPE, "QUIT")
            self.gym.subscribe_viewer_keyboard_event(
                self.viewer, gymapi.KEY_V, "toggle_viewer_sync")

            # set the camera position based on up axis
            sim_params = self.gym.get_sim_params(self.sim)
            if sim_params.up_axis == gymapi.UP_AXIS_Z:
                cam_pos = gymapi.Vec3(4, 3, 2)
                cam_target = gymapi.Vec3(-4, -3, 0)
            else:
                cam_pos = gymapi.Vec3(20.0, 3.0, 25.0)
                cam_target = gymapi.Vec3(10.0, 0.0, 15.0)

            # Point viewer camera at middle env
            n_per_row = int(np.sqrt(self.num_envs))
            look_at_env = self.envs[self.num_envs // 2 + n_per_row // 2]
            self.gym.viewer_camera_look_at(
              self.viewer, look_at_env, cam_pos, cam_target
            )


    def setup_values(self):
        self.goal_resets = torch.zeros_like(self.reset_buf)
        self.goal_dist = torch.zeros_like(self.rew_buf)

        # get gym GPU state tensors
        actor_root_state_tensor = self.gym.acquire_actor_root_state_tensor(self.sim)
        dof_state_tensor = self.gym.acquire_dof_state_tensor(self.sim)
        _dof_force = self.gym.acquire_dof_force_tensor(self.sim)
        rigid_body_tensor = self.gym.acquire_rigid_body_state_tensor(self.sim)
        jacobians = self.gym.acquire_jacobian_tensor(self.sim, self.robot_name)

        self.refresh(False)

        # create some wrapper tensors for different slices
        self.robot_default_dof_pos = torch.zeros(self.num_robot_dofs, device=self.device)
        self.robot_default_dof_pos[:self.n_arm_joints] = torch.tensor([0., 0.6, 0., -0.8, 0., 1., 1.57])
        self.dof_state = gymtorch.wrap_tensor(dof_state_tensor)
        self.robot_dof_state = self.dof_state.view(self.num_envs, -1, 2)[:, :self.num_robot_dofs]
        self.robot_dof_pos = self.robot_dof_state[..., 0]
        self.robot_dof_vel = self.robot_dof_state[..., 1]
        self.robot_dof_for = gymtorch.wrap_tensor(
          _dof_force
        ).view(self.num_envs, -1, 1)

        self.rigid_body_states = gymtorch.wrap_tensor( rigid_body_tensor )
        self.root_state_tensor = gymtorch.wrap_tensor(
          actor_root_state_tensor
        ).view(self.num_envs, -1, 13)

        self.rigid_body_states = self.rigid_body_states.view(self.num_envs, -1, 13)
        self.robot_ee = self.rigid_body_states[:,self.ee_handle,:]

        self.object_state = self.rigid_body_states[:, self.num_robot_bodies:]
        self.object_roots = self.root_state_tensor[:,1:]

        self.num_dofs = self.gym.get_sim_dof_count(self.sim) // self.num_envs
        self.robot_dof_targets = torch.zeros(
          (self.num_envs, self.num_robot_dofs),
          dtype=torch.float, device=self.device
        )

        jacobians = gymtorch.wrap_tensor( jacobians )
        print("JAC", jacobians.shape)
        self.ee_jacobian = jacobians[:,self.ee_handle,:,:self.n_arm_joints]

        self.actions = torch.zeros(
          (self.num_envs, self.num_acts), device=self.device
        )
        self.action_speed_scales = torch.ones_like(self.actions)
        if 'act_scale' in self.cfg:
            self.action_speed_scales *= torch.tensor(self.cfg['act_scale'], device=self.device)

        self.goals = torch.zeros( (self.num_envs, 7),
          dtype=torch.float, device=self.device
        )

        # get the indices for all objects
        self.global_indices = torch.arange(
          self.num_envs * (self.num_robots + self.num_objects),
          dtype=torch.int32, device=self.device
        ).view(self.num_envs, -1)

if __name__ == '__main__':
    test()
