#!/usr/bin/env python
import argparse
import matplotlib.pyplot as plt

from ll4ma_util import file_util


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--filename', required=True)
    args = parser.parse_args()

    file_util.check_path_exists(args.filename)

    log = file_util.load_pickle(args.filename)

    print("SAMPLES", log['samples'].shape)
    print("COST", log['cost'].shape)
    print("HORIZON COST", log['horizon_cost'].shape)
    
    
    # plt.plot(log['cost'])

    
    horizon_cost = log['horizon_cost']
    n_steps, n_horizon, n_samples = horizon_cost.shape
    for step in range(n_steps):
        for sample_idx in range(n_samples):
            plt.plot(horizon_cost[step, :, sample_idx])
        plt.show()
