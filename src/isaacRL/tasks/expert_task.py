
from isaacgym import gymtorch, gymapi

from isaacgymenvs.tasks import isaacgym_task_map
from isaacgymenvs.tasks.base.vec_task import VecTask
from isaacRL.tasks.gym_task import *
from isaacRL.utils.utils import get_pinv

import os
import numpy as np
import torch

class ExpertTask(GymTask):
    """docstring for ExpertTask."""

    def __init__(self, config):
        config['additive_actions'] = False
        super(ExpertTask, self).__init__(config)

        self.env_resets = torch.zeros_like(self.reset_buf)
        self.steps_i = 0
        self.zeros_steps = self.config.get('zero_steps', 0)
        self.config['expert']['env_config'] = self.config['env_config']
        self.config['expert']['max_steps'] = self.config['max_env_steps']
        # self.config['expert']['object_noise'] = 0.0
        # self.config['expert']['save_file'] = '{}.torch'.format(self.config['name'])
        self.next_action = None
        self.scale_expert = self.config.get('scale_actions', False)

        from isaacRL.experts import MoveitExpert
        self.expert = MoveitExpert(self.config['expert'], self, open_loop=True)

    def add_actions(self, bactions, actions):
        ajnts = self.robot.arm.num_joints()
        bactions[:,:ajnts] += actions[:,:ajnts]
        # TODO: Maybe need to do something special when adding the end effector actions
        # bactions[:,ajnts:] += actions[:,ajnts:]
        return bactions

    def step(self, actions=None):
        """
        adds the given action to the expert action
        """

        if actions is None:
            actions = self.zero_actions()
        actions = self.scale_action(actions)
        actions = self.add_actions(self.next_action, actions)

        # TODO: this function is really time costly
        expert_resets = self.gymtask.get_resets()

        nstate, rewards, dones, info = self.gymtask.step(actions)

        self.expert.reset_idx(expert_resets)

        self.next_action, edones = self.expert.get_actions()
        self.gymtask.update_resets((edones > 0).to(self.sim_device))

        dones = self.gymtask.get_resets()

        if self.grapher is not None and self.grapher.update(self.state, rewards, dones, info):
            return None

        self.steps_i += 1
        state = nstate
        state = self.update_state(state)
        return state, rewards, dones, info

    def reset(self):
        """
        resets the expert too
        """
        state = super().reset()
        self.expert.reset()
        self.next_action, _ = self.expert.get_actions()
        return self.update_state(state)

    def reset_idx(self, env_ids):
        """
        resets the expert at each env too
        """
        state = self.gymtask.reset_idx(env_ids)
        self.expert.reset_idx(env_ids)
        self.next_action, _ = self.expert.get_actions()
        return self.update_state(state)

    def update_state(self, state):
        if self.add_expert > 0:
            # self.add_expert*self.action_size
            next_actions = self.next_action
            if self.add_expert > 1:
                next_actions = torch.cat(
                    (next_actions, self.expert.next_actions(self.add_expert-1)),
                dim=-1)
            if self.scale_expert:
                next_actions[:,:self.gymtask.n_arm_joints] = (
                  2.0 * (next_actions[:,:self.gymtask.n_arm_joints].to(self.sim_device) - self.gymtask.robot_dof_lower_limits[:self.gymtask.n_arm_joints])
                  / (self.gymtask.robot_dof_upper_limits[:self.gymtask.n_arm_joints] - self.gymtask.robot_dof_lower_limits[:self.gymtask.n_arm_joints])
                  - 1.0
                ).to(self.rl_device)
            #     import pdb; pdb.set_trace()
            state['obs'] = torch.cat([state['obs'], next_actions], dim=-1)
            state['fullstate'] = torch.cat([state['fullstate'], next_actions], dim=-1)
        return state

    def cartesian2joints(self, cacts):
        inv_jac = get_pinv(self.gymtask.ee_jacobian, 0.00001)
        jacts = torch.bmm(inv_jac, cacts[:,:6].unsqueeze(-1)).squeeze(-1)
        jacts = torch.cat((jacts, cacts[:,6:]), 1)
        # jacts /= (self.gymtask.dt * self.gymtask.action_scale)
        return jacts

    # @property
    # def sim_cfg(self):
    #     return self.gymtask.cfg['sim_cfg']

    def get_env_states(self):
        return self.latest_state

    def set_additive(self, setval):
        self.gymtask.additive = setval

    def set_cartesian(self, setval):
        self.gymtask.cartesian = setval
