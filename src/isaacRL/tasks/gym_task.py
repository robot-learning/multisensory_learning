
import gym
from isaacgym import gymtorch, gymapi

from isaacgymenvs.tasks import isaacgym_task_map
from isaacRL.tasks.mvp_tasks import _TASK_MAP
from isaacgymenvs.tasks.base.vec_task import VecTask
# from isaacRL.tasks.pickstack import PickStack
from ll4ma_util import file_util
from std_msgs.msg import ColorRGBA
from ll4ma_isaacgym.core import SessionConfig, EnvironmentState
from ll4ma_isaacgym.robots import Robot
# from isaacRL.tasks.gymtasks.general_iiwa import GeneralIiwa
# isaacgym_task_map['GeneralIiwa'] = GeneralIiwa
isaacgym_task_map['FrankaCabinetOG'] = isaacgym_task_map['FrankaCabinet']
from isaacRL.utils.goalie import select_goalie
from isaacRL.models.actorcritics import get_rgb_network

import os
import numpy as np
import torch

class GymTask:
    """
    GymTask is a wrapper function for VecTask objects that sets the archetype
    for other wrapper functions. This allows outside functions to use GymTask
    just as they would VecTask but with additional functions.
    """

    def __init__(self, config):
        self.config = config

        self.init_wait_steps = config['init_wait_steps']

        task, env_config = create_env(self.config)
        self.env_config = env_config
        if config['type'] == 'GeneralFranka' or config['type'] == 'GeneralIiwa':
            self.sim_cfg = self.env_config['sim_cfg']

        self.steps_per_action = self.config.get('steps_per_action', 1)

        self.gymtask = task

        self.add_expert = self.config.get('add_expert', 0)

        self.grapher = None if 'grapher' not in self.config else self.config['grapher']

        if self.use_rgb:
            self.rgb_net, self.rgb_emb_size = get_rgb_network()
            self.rgb_net.to(self.rl_device)
            self.rgb_net.eval()

    def step(self, actions=None):
        if actions is None:
            actions = self.zero_actions() if self.additive else self.same_actions()
        if self.steps_per_action > 1:
            for _ in range(self.steps_per_action-1):
                self.gymtask.step(
                    self.zero_actions() if self.additive else self.same_actions()
                )
        nstate, rewards, done, info = self.gymtask.step(actions)
        if self.grapher is not None and self.grapher.update(self.state, rewards, done, info):
            return None
        if isinstance(nstate, dict):
            state = {key: nstate[key] for key in nstate} #{'obs':nstate['obs']}
        else:
            state = self.state2dict(nstate)
        return state, rewards, done, info

    def pre_physics_step(self, **kwargs):
        return self.gymtask.pre_physics_step(**kwargs)

    def post_physics_step(self, **kwargs):
        return self.gymtask.post_physics_step(**kwargs)

    def state2dict(self, state):
        if self.use_rgb:
            with torch.no_grad():
                obs = self.rgb_net(state)
                return {
                    'obs': obs
                }
            # return {
            #     'obs': torch.zeros((self.num_envs,0),device=state.device,dtype=state.dtype),
            #     'rgb': state.flatten(1)
            # }
        return {'obs': state}

    def reset(self):
        if not hasattr(self.gymtask, 'reset_idx'):
            state = self.gymtask.reset()
            return self.state2dict(state)
        self.gymtask.reset_idx(torch.arange(self.num_envs, device=self.sim_device))
        action = self.zero_actions() if self.additive else self.same_actions()
        obs, _, _, _ = self.gymtask.step(action)
        state = {key: obs[key] for key in obs}
        return state

    def reset_idx(self, env_ids):
        return self.gymtask.reset_idx(env_ids)

    def get_robot_ee(self, simple=False):
        return self.gymtask.get_robot_ee(simple)

    def scale_action(self, actions):
        return self.gymtask.scale_action(actions)

    @property
    def device(self):
        return self.gymtask.rl_device

    @property
    def rl_device(self):
        return self.gymtask.rl_device

    @property
    def sim_device(self):
        return self.gymtask.device

    @property
    def max_steps(self):
        if not hasattr(self.gymtask, 'max_episode_length') and hasattr(self.gymtask, 'task'):
            return self.gymtask.task.max_episode_length
        return self.gymtask.max_episode_length

    @property
    def actions(self):
        return self.gymtask.actions

    @property
    def additive(self):
        return True if not hasattr(self.gymtask, 'additive') else self.gymtask.additive

    @property
    def obs_buf(self):
        return self.gymtask.obs_buf

    @property
    def state(self):
        return self.gymtask.fullstate

    @property
    def state_idx(self):
        return self.gymtask.state_idx

    @property
    def objects(self):
        return self.gymtask.objects

    def full2obs(self, fullstate):
        return self.gymtask.full2obs(fullstate)

    @property
    def goalie(self):
        return self.gymtask.goalie

    @property
    def goal_size(self):
        return self.goalie.goal_size

    def get_rewards(self, goals):
        return self.goalie.reward(self.gymtask.get_env_states(), goals)

    def zero_actions(self):
        return self.gymtask.zero_actions()

    def same_actions(self):
        return self.gymtask.same_actions()

    def successes(self):
        return 0 if not hasattr(self.gymtask, 'successes') else self.gymtask.successes()

    def runs(self):
        return 1 if not hasattr(self.gymtask, 'runs') else self.gymtask.runs()

    def get_objects(self):
        return self.gymtask.get_objects()

    @property
    def robot(self):
        return self.gymtask.robot

    @property
    def num_joints(self):
        return self.gymtask.n_arm_joints

    @property
    def joint_names(self):
        return self.gymtask.joint_names

    @property
    def ee_name(self):
        return self.gymtask.ee_name

    @property
    def observation_space(self) -> gym.Space:
        """Get the environment's observation space."""
        return self.obs_space

    @property
    def obs_space(self) -> gym.Space:
        """Get the environment's observation space."""
        if self.use_rgb:
            return self.rgb_emb_size
        return self.gymtask.obs_space

    @property
    def action_space(self) -> gym.Space:
        """Get the environment's action space."""
        return self.act_space

    @property
    def act_space(self) -> gym.Space:
        """Get the environment's action space."""
        return self.gymtask.act_space

    @property
    def num_envs(self) -> int:
        """Get the number of environments."""
        return self.gymtask.num_environments

    @property
    def num_acts(self) -> int:
        """Get the number of actions in the environment."""
        return self.gymtask.num_actions

    @property
    def action_size(self) -> int:
        """Get the number of actions in the environment."""
        return self.num_acts

    @property
    def num_obs(self) -> int:
        """Get the number of observations in the environment."""
        return self.gymtask.num_observations

    def set_viewer(self):
        return self.gymtask.set_viewer()

    def allocate_buffers(self, **kwargs):
        return self.gymtask.allocate_buffers(**kwargs)

    def set_sim_params_up_axis(self, **kwargs):
        return self.gymtask.set_sim_params_up_axis(**kwargs)

    def create_sim(self, **kwargs):
        return self.gymtask.create_sim(**kwargs)

    def render(self):
        return self.gymtask.render()

    def get_actor_params_info(self, **kwargs):
        return self.gymtask.get_actor_params_info(**kwargs)

    def apply_randomizations(self, **kwargs):
        return self.gymtask.apply_randomizations(**kwargs)

    @property
    def state_space(self):
        return self.gymtask.state_space

    @property
    def reset_buf(self):
        return self.gymtask.reset_buf

    @property
    def rew_buf(self):
        return self.gymtask.rew_buf

    @property
    def act_shape(self):
        return self.gymtask.cfg['env']['numActions']

    @property
    def num_states(self):
        return self.gymtask.num_states

    @property
    def obs_batch(self):
        return self.gymtask.obs_batch

    @property
    def max_vel(self):
        return self.gymtask.robot_dof_max_vel

    @property
    def robot_dof_max_vel(self):
        return self.gymtask.robot_dof_max_vel

    @property
    def full_state_size(self):
        return self.gymtask.fullstate.shape[1]

    @property
    def img_width(self):
        if hasattr(self.gymtask, 'img_width'):
            return self.gymtask.img_width
        elif hasattr(self.gymtask, 'task') and hasattr(self.gymtask.task, 'im_size'):
            return self.gymtask.task.im_size
        return None

    @property
    def img_height(self):
        if hasattr(self.gymtask, 'img_height'):
            return self.gymtask.img_height
        elif hasattr(self.gymtask, 'task') and hasattr(self.gymtask.task, 'im_size'):
            return self.gymtask.task.im_size
        return None

    @property
    def use_rgb(self):
        if hasattr(self.gymtask, 'use_rgb'):
            return self.gymtask.use_rgb
        elif hasattr(self.gymtask, 'task') and hasattr(self.gymtask.task, 'obs_type'):
            return self.gymtask.task.obs_type == "pixels"
        return False

    @property
    def randomize(self):
        return False


def create_env(config, rl_device='cuda'):
    config['use_gpu'] = config.get('use_gpu', torch.cuda.is_available())
    sim_device = 'cuda:0' if config['use_gpu'] else 'cpu'

    env_configf = os.path.expanduser(config['env_config'])
    env_config = file_util.load_yaml(env_configf)
    env_config["env"]["asset"]["assetRoot"] = os.path.expanduser(
        "~/workspace/isaacgym/assets"  # '/home/iain/isaac_ws/src'
    )
    if 'num_envs' in config:
        env_config['env']['numEnvs'] = config['num_envs']
    if 'steps_per_action' not in config:
        config['steps_per_action'] = 1
    if 'max_env_steps' in config:
        env_config['env']['episodeLength'] = config['max_env_steps'] * config['steps_per_action']
        env_config['max_env_steps'] = config['max_env_steps']
    if 'actionScale' in config:
        env_config['env']['actionScale'] = config['actionScale']
    env_config['randomize'] = config.get('randomize', 0)

    for key in config:
        env_config[key] = config[key]

    if config['type'] == 'GeneralFranka' or config['type'] == 'GeneralIiwa':
        sim_cfg = SessionConfig(config_filename=env_configf)
        env_config['sim_cfg'] = sim_cfg
        env_config['robot'] = Robot(
          next( iter( sim_cfg.env.robots.values() ) )
        )
        act_range = torch.tensor(config['act_range'])
        if len(act_range.shape) == 1:
            open = act_range[0].item()
            close = act_range[1].item()
        else:
            assert act_range.shape[0] == config['num_acts']
            open = act_range[-1,0].item()
            close = act_range[-1,1].item()
        env_config['robot'].config.end_effector.discretes.open = close
        env_config['robot'].config.end_effector.discretes.open = open
        env_config['objects'] = sim_cfg.env.objects
        config['device'] = sim_device
        env_config['goalie'] = select_goalie(config['goal'],rl_device)
        env_config['env']['numObservations'] = config['num_obs']
        env_config['env']['numActions'] = config['num_acts']

    env_config['physics_engine'] = 'physx'
    env_config['sim']['physics_engine'] = env_config['physics_engine']
    env_config['sim']['use_gpu_pipeline'] = config['use_gpu']
    env_config['sim']['physx']['use_gpu'] = config['use_gpu']
    env_config['sim']['physx']['num_threads'] = (
        1 if 'num_threads' not in config else config['num_threads']
    )
    env_config['sim']['physx']['num_subscenes'] = config['subscenes']
    env_config['sim']['physx']['solver_type'] = 1
    env_config['rl_device'] = rl_device

    if config['type'] in _TASK_MAP:
        device_type = "cuda" if config['use_gpu'] else "cpu"
        device_id = 0
        physics_engine = gymapi.SIM_PHYSX \
            if env_config['physics_engine'] == "physx" else gymapi.SIM_GLEX
        task = _TASK_MAP[config['type']](
            cfg=env_config,
            sim_params=get_sim_params(env_config['sim']),
            physics_engine=physics_engine,
            device_type=device_type,
            device_id=device_id,
            headless=config['headless']
        )
        from pixmc.tasks.base.vec_task import VecTaskPython
        env = VecTaskPython(task, rl_device)
    elif config['type'] in isaacgym_task_map:
        env = isaacgym_task_map[config['type']](
            cfg=env_config,
            rl_device=rl_device,
            sim_device=sim_device,
            graphics_device_id=0,
            headless=config['headless'],
            virtual_screen_capture=False,
            force_render=True  # config['headless'],
        )
    else:
        raise("Uknown task "+config['type'])
    return env, env_config

def get_sim_params(cfg):
    # previously args defaults
    args_use_gpu_pipeline = cfg['use_gpu_pipeline']
    args_use_gpu = cfg['physx']['use_gpu']
    args_subscenes = cfg['physx']['num_subscenes']  # 0
    args_slices = args_subscenes
    args_num_threads = 0

    # initialize sim
    sim_params = gymapi.SimParams()
    sim_params.dt = 1 / 60.
    sim_params.num_client_threads = args_slices

    assert cfg['physics_engine'] == "physx"
    sim_params.physx.solver_type = 1
    sim_params.physx.num_position_iterations = 4
    sim_params.physx.num_velocity_iterations = 0
    sim_params.physx.num_threads = 4
    sim_params.physx.use_gpu = args_use_gpu
    sim_params.physx.num_subscenes = args_subscenes
    sim_params.physx.max_gpu_contact_pairs = 8 * 1024 * 1024

    sim_params.use_gpu_pipeline = args_use_gpu_pipeline
    sim_params.physx.use_gpu = args_use_gpu

    # Override num_threads if specified
    if cfg['physics_engine'] == "physx" and args_num_threads > 0:
        sim_params['physx']['num_threads'] = args_num_threads

    return sim_params
