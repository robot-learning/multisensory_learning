#!/usr/bin/env python
import os
import sys
import rospy
import numpy as np
import matplotlib

from moveit_msgs.msg import DisplayRobotState
from visualization_msgs.msg import Marker, MarkerArray
from sensor_msgs.msg import Image
from ll4ma_isaacgym.msg import IsaacGymState

from multisensory_learning.learning import (
    MSPLearner, GoalLearner, MSPConfig, GoalLearnerConfig, msp_util
)
from ll4ma_util import ros_util, func_util, torch_util, file_util

import torch


class TeleopVisualizer:

    def __init__(self, checkpoint_filename, data_filename, device='cuda', rate=100):
        self.device = device
        self.data_filename = data_filename
        self.rate = rospy.Rate(rate)

        # Create goal learner
        file_util.check_path_exists(checkpoint_filename)
        goal_config = GoalLearnerConfig(checkpoint=checkpoint_filename, device=device)
        self.goal_learner = GoalLearner(goal_config, set_eval=True)
    
        # Create MSP learner for encoding contextual observations
        file_util.check_path_exists(goal_config.msp_checkpoint, "MSP checkpoint file")
        msp_config = MSPConfig(checkpoint=goal_config.msp_checkpoint, device=device)
        self.msp_learner = MSPLearner(msp_config, set_eval=True)
        self.enc_obs_modalities = self.msp_learner.config.enc_obs_modalities
        self.dec_obs_modalities = self.msp_learner.config.dec_obs_modalities
        
        
        rospy.Subscriber("/isaacgym_state", IsaacGymState, self.gym_state_cb)
        self.marker_pub = rospy.Publisher("/markers", MarkerArray, queue_size=1)
        self.goal_rgb_pub = rospy.Publisher("/goal/rgb", Image, queue_size=1)
        self.goal_joint_pos_pub = rospy.Publisher("/goal/joint_position", DisplayRobotState,
                                                  queue_size=1)
        self.robot_state_pub = rospy.Publisher("/predictions/joint_position", DisplayRobotState,
                                               queue_size=1)
        self.ee_pos_pub = rospy.Publisher("/predictions/ee_position", Marker, queue_size=1)
        self.obj_pos_pub = rospy.Publisher("/predictions/obj_position", MarkerArray, queue_size=1)
        self.cost_pub = rospy.Publisher("/cost", MarkerArray, queue_size=1)
        self.prob_pub = rospy.Publisher("/prob", MarkerArray, queue_size=1)

        self.gym_state = None
        self.robot_msg = None



    def run(self):
        rospy.loginfo("Waiting for Isaac Gym state...")
        while not rospy.is_shutdown() and self.gym_state is None:
            self.rate.sleep()
        rospy.loginfo("State received!")
        
        # Get markers for the objects
        data, attrs = file_util.load_pickle(self.data_filename)
        # TODO these will be in attrs going forward but hacking for now so I don't have
        # to record the data again
        # joint_names = attrs['joint_names']
        # link_names = attrs['link_names']
        joint_names = ['iiwa_joint_1', 'iiwa_joint_2', 'iiwa_joint_3', 'iiwa_joint_4',
                       'iiwa_joint_5', 'iiwa_joint_6', 'iiwa_joint_7', 'reflex_preshape_joint_1',
                       'reflex_proximal_joint_1', 'reflex_preshape_joint_2',
                       'reflex_proximal_joint_2', 'reflex_proximal_joint_3']
        link_names = ['iiwa_base', 'iiwa_link_0', 'iiwa_link_1', 'iiwa_link_2', 'iiwa_link_3',
                      'iiwa_link_4', 'iiwa_link_5', 'iiwa_link_6', 'iiwa_link_7', 'iiwa_link_ee',
                      'reflex_distal_link_1', 'reflex_distal_link_2', 'reflex_distal_link_3',
                      'reflex_distal_pad_link_1', 'reflex_distal_pad_link_2',
                      'reflex_distal_pad_link_3', 'reflex_flex_link_1', 'reflex_flex_link_2',
                      'reflex_flex_link_3', 'reflex_pad_link', 'reflex_palm_link',
                      'reflex_proximal_link_1', 'reflex_proximal_link_2', 'reflex_proximal_link_3',
                      'reflex_proximal_pad_link_1', 'reflex_proximal_pad_link_2',
                      'reflex_proximal_pad_link_3', 'reflex_shell_link', 'reflex_swivel_link_1',
                      'reflex_swivel_link_2']

        marker_msgs = self.get_marker_msgs(attrs['objects'])

        # For displaying cost
        cmap = matplotlib.cm.get_cmap('plasma')
        cost_normalizer = matplotlib.colors.Normalize(vmin=0.0, vmax=2.0)
        
        
        rospy.loginfo("Showing predictions in rviz...")
        while not rospy.is_shutdown():
            # Show the actual object poses (as markers in rviz)
            for tf in self.gym_state.tf[0].transforms:
                if tf.child_frame_id in marker_msgs:
                    marker_msgs[tf.child_frame_id].header.frame_id = tf.header.frame_id
                    marker_msgs[tf.child_frame_id].pose.position.x = tf.transform.translation.x
                    marker_msgs[tf.child_frame_id].pose.position.y = tf.transform.translation.y
                    marker_msgs[tf.child_frame_id].pose.position.z = tf.transform.translation.z
                    marker_msgs[tf.child_frame_id].pose.orientation = tf.transform.rotation
            marker_array = MarkerArray([m for m in marker_msgs.values()])
            self.marker_pub.publish(marker_array)

            
            # Read out the observation from the gym state
            obs = {}
            if 'rgb' in self.enc_obs_modalities:
                obs['rgb'] = ros_util.msg_to_rgb(self.gym_state.rgb[0])
            if 'depth' in self.enc_obs_modalities:
                obs['depth'] = ros_util.msg_to_depth(self.gym_state.depth[0])
            if 'joint_position' in self.enc_obs_modalities:
                obs['joint_position'] = np.array(self.gym_state.joint_state[0].position)
            if 'joint_velocity' in self.enc_obs_modalities:
                obs['joint_velocity'] = np.array(self.gym_state.joint_state[0].velocity)
            if 'joint_torque' in self.enc_obs_modalities:
                obs['joint_torque'] = np.array(self.gym_state.joint_state[0].effort)
            if 'ee_position' in self.enc_obs_modalities:
                obs['ee_position'] = np.array([self.gym_state.ee_pose[0].position.x,
                                               self.gym_state.ee_pose[0].position.y,
                                               self.gym_state.ee_pose[0].position.z])
            if 'arm_joint_position' in self.enc_obs_modalities:
                # TODO hacked
                obs['arm_joint_position'] = np.array(self.gym_state.joint_state[0].position)[:7]
                
            # Add time dim to everything
            for k, v in obs.items():
                obs[k] = np.expand_dims(v, 0)

            # Apply models
            with torch.no_grad():
                latent_in = self.msp_learner.encode_raw_data(obs)
                alpha, mu, sigma_tril = self.goal_learner.apply_mdn(latent_in)

                # TODO add ability to sample and show multiple components
                component = 0  # TODO for now just 1 component
                # mvn = MultivariateNormal(mu[:,:,component], scale_tril=sigma_tril[:,:,:,component])
                mean = mu[:,:,component].unsqueeze(0)

                decoded = self.msp_learner.decode_obs(mean, process_out=True)
                
                    
            # # Show cost
            # cost_msg = MarkerArray()
            # cost_color = cmap(cost_normalizer(cost))
            # cost_msg.markers.append(ros_util.get_marker_msg((-1, -0.5, 1), (0.5, 0.5, 0.5), None,
            #                                                 'sphere', cost_color, 0.99, 123))
            # cost_msg.markers.append(ros_util.get_marker_msg((-1, -0.5, 1), (0.1, 0.1, 0.1), None,
            #                                                 'text', 'white', marker_id=124,
            #                                                 text=f"Cost: {cost:.4f}"))
            # self.cost_pub.publish(cost_msg)

            # # Show prob
            # if self.trainer:
            #     prob_msg = MarkerArray()
            #     prob_color = cmap(cost_normalizer(prob))
            #     prob_msg.markers.append(ros_util.get_marker_msg((-1, -0.5, 0.5), (0.5, 0.5, 0.5), None,
            #                                                     'sphere', prob_color, 0.99, 123))
            #     prob_msg.markers.append(ros_util.get_marker_msg((-1, -0.5, 0.5), (0.1, 0.1, 0.1), None,
            #                                                     'text', 'white', marker_id=124,
            #                                                     text=f"Prob: {prob:.4f}"))
            #     self.prob_pub.publish(prob_msg)
                
                            
            # Show decodes
            if 'arm_joint_position' in self.dec_obs_modalities:
                joint_pos = decoded['arm_joint_position']
                joint_pos = np.concatenate([joint_pos, np.zeros(5)])
                if self.robot_msg is None:
                    self.robot_msg = ros_util.get_display_robot_msg(joint_pos, joint_names,
                                                                    link_names, "gold")
                else:
                    self.robot_msg.state.joint_state.position = joint_pos.tolist()
                self.robot_state_pub.publish(self.robot_msg)
            if 'ee_position' in self.dec_obs_modalities:
                ee_pos_msg = ros_util.get_marker_msg(decoded['ee_position'], (0.1, 0.1, 0.1),
                                                     None, 'sphere', 'red')
                self.ee_pos_pub.publish(ee_pos_msg)
            if 'obj_position' in self.dec_obs_modalities:
                obj_pos_msg = MarkerArray()
                # TODO need to generalize to other objs and just pull info from metadata
                obj_pos_msg.markers.append(ros_util.get_marker_msg(decoded['block_obj_position'],
                                                                   (0.08, 0.08, 0.08), None,
                                                                   'cube', 'gold'))
                self.obj_pos_pub.publish(obj_pos_msg)
                        
        
    def gym_state_cb(self, msg):
        self.gym_state = msg

    def get_marker_msgs(self, objects):
        marker_msgs = {}
        for i, (obj_id, obj_data) in enumerate(objects.items()):
            marker = Marker()
            marker.id = i
            marker.type = Marker.CUBE
            marker.action = Marker.MODIFY
            marker.scale.x = obj_data['x_extent']
            marker.scale.y = obj_data['y_extent']
            marker.scale.z = obj_data['z_extent']
            marker.color.a = 1.0
            marker.color.r = obj_data['rgb_color'][0]
            marker.color.g = obj_data['rgb_color'][1]
            marker.color.b = obj_data['rgb_color'][2]
            marker_msgs[obj_id] = marker
        return marker_msgs


if __name__ == '__main__':
    rospy.init_node('teleop_visualizer')
    gym_config = rospy.get_param("~gym_config")
    data_filename = rospy.get_param("~data_filename")
    goal_checkpoint = rospy.get_param("~goal_checkpoint")
    visualizer = TeleopVisualizer(goal_checkpoint, data_filename)
    visualizer.run()
