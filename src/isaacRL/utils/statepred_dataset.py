
import torch
from torch.utils.data import Dataset

from isaacRL.utils.pytorch3d.rotation_conversions import quaternion_to_axis_angle
from ll4ma_util.torch_util import quat_to_axisangle

class StatePredDataset(Dataset):
    """docstring for StatePredDataset."""

    def __init__(self, datafile, xkeys, ykeys, device='cpu'):
        super(StatePredDataset, self).__init__()
        self._datafile = datafile
        self._data = torch.load(datafile)
        self._datakeys = self._data['indices']
        self._data = self._data['states']#.to(device)
        self.xkeys = xkeys
        self.ykeys = ykeys

    def __len__(self):
        return self._data.shape[0]

    def __getitem__(self, idx):
        data = self._data[idx]
        xdata = data[self._datakeys[self.xkeys]]
        ydata = data[self._datakeys[self.ykeys]]
        # axis = quaternion_to_axis_angle(ydata[3:7])
        # angle = (axis**2).sum().sqrt()
        # ydata = torch.cat((
        #     ydata[0:3], axis #/angle, angle.unsqueeze(0)
        # ))
        return xdata, ydata
