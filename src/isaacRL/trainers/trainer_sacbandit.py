
from isaacRL.trainers.trainer_sacbase import TrainerSACBase
from isaacRL.models.actorcritics import ActorMVN

import torch
import torch.nn.functional as F

import time
from tqdm import tqdm
import os, sys
import os.path as osp
import rospy


class TrainerSACBandit(TrainerSACBase):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        super(TrainerSACBandit, self).__init__(config)

    def print_info(self):
        super().print_info()
        print('state:', self.buffer._states[0])
        print('action:', self.buffer._actions[0])
        print('nstate:', self.buffer._nstates[0])
        print('reward:', self.buffer._rewards[0])
        print('terminal:', self.buffer._terminals[0])
        eaction = self.buffer._actions[0:1]
        astate = {'obs': self.buffer._states[0:1]}
        paction, plog = self.actor.get_actions(astate, True)
        print('pred action:', paction)
        qval1 = self.critic1(astate, eaction)
        qval2 = self.critic2(astate, eaction)
        print('eaction q val1:', qval1)
        print('eaction q val2:', qval2)
        qval1 = self.critic1(astate, paction)
        qval2 = self.critic2(astate, paction)
        print('paction q val1:', qval1)
        print('paction q val2:', qval2)
        qvals = torch.min(qval1, qval2)
        print('pi loss:', ((self.alpha.detach() * plog) - qvals).mean())

    def train_setup(self):
        # fill minimal replay buffer
        minsize = (self.batch_size//self.num_envs)+1
        minsize = max(minsize, min(self.max_steps, self.buffer.max_size//self.num_envs)+1)
        minsize = max(minsize, self.config['training'].get('init_buf_steps', 10))
        if self.pretrain_datafile is not None:
            print('pretrain data file:', self.pretrain_datafile)
            self.buffer.load(self.pretrain_datafile, self.envir.full2obs)
            print('buffer idx:', self.buffer.load_idx)
            print('buffer len:', len(self.buffer))
        else:
            if self.state is None:
                self.state = self.envir.reset()
            pbar = tqdm(total=minsize, file=sys.stdout)
            desc = f'Filling buffer'
            pbar.set_description(desc)
            tot_rew = 0.
            with torch.no_grad():
                for iter in range(minsize):
                    if rospy.is_shutdown():
                        return
                    actions = self.state['full_state']['stand_noisy'].clone()
                    actions[:, 2] += 0.225
                    actions[:, 3:7] = self.state['full_state']['ee_state'][:,3:7].clone()
                    nstate, rewards, terminals, _ = self.envir.step(actions)
                    self.buffer.add(
                        self.state, actions, nstate, rewards, terminals
                    )
                    self.state = nstate
                    tot_rew += rewards.sum().item()
                    pbar.update(1)
                pbar.close()
                for iter in range(minsize):
                    if rospy.is_shutdown():
                        return
                    actions, _ = self.actor.get_actions(self.state, False)
                    nstate, rewards, terminals, _ = self.envir.step(actions)
                    self.buffer.add(
                        self.state, actions, nstate, rewards, terminals
                    )
                    self.state = nstate
                    tot_rew += rewards.sum().item()
                    pbar.update(1)
                pbar.close()
            print('tot rew:', tot_rew)
        actions, _ = self.actor.get_actions(self.state, True)
        # print(actions)
        # import pdb; pdb.set_trace()
