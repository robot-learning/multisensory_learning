#!/usr/bin/env python
import argparse
import seaborn as sns
import matplotlib.pyplot as plt

from ll4ma_util import file_util
from multisensory_learning.learning import GoalLearnerConfig, GoalLearner, MSPConfig, MSPLearner

import torch
import torch.distributions as D


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--goal_checkpoint', type=str, required=True)
    parser.add_argument('--data_filename', type=str, required=True)
    parser.add_argument('--input_idx', type=int, default=0)
    args = parser.parse_args()

    file_util.check_path_exists(args.goal_checkpoint)
    file_util.check_path_exists(args.data_filename)

    data, attrs = file_util.load_pickle(args.data_filename)
    
    goal_config = GoalLearnerConfig(checkpoint=args.goal_checkpoint)
    goal_learner = GoalLearner(goal_config, set_eval=True)

    file_util.check_path_exists(goal_config.msp_checkpoint)
    
    msp_config = MSPConfig(checkpoint=goal_config.msp_checkpoint)
    msp_learner = MSPLearner(msp_config, set_eval=True)

    with torch.no_grad():
        latents = msp_learner.encode_raw_data(data)
        latent_in = latents[args.input_idx]

        idx = 0  # TODO only 1 component right now
        alpha, mu, sigma_tril = goal_learner.apply_mdn(latent_in)
        mvn = D.MultivariateNormal(loc=mu[:,:,idx], scale_tril=sigma_tril[:,:,:,idx])
        
        log_pdfs = mvn.log_prob(latents).unsqueeze(0).cpu().numpy()
        
        sns.heatmap(log_pdfs)
        plt.show()
