
from isaacRL.tasks.sac_task import SACTask
from isaacRL.tasks.gym_tasks import GymTask
from isaacRL.tasks.expert_behaviors import Expert
from isaacRL.trainers.trainer_sacbase import TrainerSACBase
from isaacRL.utils.buffer import Buffer

import os, sys, argparse, json, random
import os.path as osp
import numpy as np
from tqdm import tqdm
import rospy

import torch


class TrainerSAC(TrainerSACBase):
    """
    Holder of models and data for training and evaluating Soft Actor-Critic
    Reinforcement Learning method.
    """

    def __init__(self, config):
        super(TrainerSAC, self).__init__(config)

        self.buffer = Buffer( self.config['training']['max_buff_size'],
          self.max_steps, self.config['environ']['prev_states']
        )
        print('buffer size:', self.buffer.max_size)

        if self.config['environ']['type'] == 'arm':
            self.envir = SACTask(self.config['environ'], self.device)
        else:
            self.envir = GymTask(self.config['environ'], self.device)

    def train(self):

        if self.config['training']['expert_demos'] > 0:
            self.expert_demos(self.config['training']['load_experts'])

        tot_rewards = 0.
        print('resetting')
        self.model.train()
        self.state = self.envir.reset()
        # torch.autograd.detect_anomaly(True)
        for epoch in range(self.start_epoch, self.end_epoch):
            if rospy.is_shutdown():
                return

            if (epoch-1) >= self.config['training']['expert_epochs']:
                tot_rewards = self.play(epoch)

            # learn from replays
            qlosses, pi_losses = 0., 0.
            pbar = tqdm(total=self.updates_per_interval, file=sys.stdout)
            desc = f'  Learn Epoch {epoch}'
            pbar.set_description(desc)
            for ui in range(self.updates_per_interval):
                if rospy.is_shutdown():
                    return
                qloss, pi_loss = self.update_weights(ui)
                self.qlosses.append(qloss)
                self.pi_losses.append(pi_loss)
                desc = f'  Learn Epoch {epoch}: loss={np.mean(self.qlosses):.4f},{np.mean(self.pi_losses):.4f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()

            if epoch%self.ckpt_interval == 0:
                rounds = self.envir.num_envs * self.max_steps * epoch
                print('saving after {} timesteps'.format(rounds))
                mean_rew = tot_rewards / (self.envir.num_envs * self.max_steps)
                print('mean rew per step:', mean_rew)
                print('mean rewards:', self.buffer.mean_rewards())
                self.save_model(epoch)


    def play(self, playi):
        with torch.no_grad():
            if self.config['environ']['type'] == 'arm':
                state = self.envir.reset()
            else:
                state = self.state
            tot_rewards = 0.
            tot_terms = 0.000001
            pbar = tqdm(total=self.max_steps, file=sys.stdout)
            desc = f'  Play Epoch {playi}'
            pbar.set_description(desc)
            for step in range(self.max_steps):
                if rospy.is_shutdown():
                    return tot_rewards
                if random.uniform(0,1) < self.randact_prob:
                    actions = self.model.policy.random_action(
                      self.envir.num_envs, self.device )
                else:
                    actions, _ = self.model.policy(
                      self.buffer.prep_state(state, self.device)
                    )
                actions = actions.detach()
                nstate, rewards, terminals = self.envir.step_all(actions)
                tot_rewards += rewards.sum().item()
                tot_terms += terminals.sum().item()
                self.buffer.add_transitions(
                  state, actions, nstate, rewards, terminals
                )
                state = nstate
                if self.config['environ']['type'] == 'arm' and terminals.sum() > 0:
                    state = self.envir.reset_indices(terminals)
                desc = f'  Play R: {(tot_rewards/self.envir.num_envs):.1f} {(tot_terms):.0f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            self.state = state
            return tot_rewards

    def expert_demos(self, load_experts=True):
        demos = 0
        if load_experts:
            self.buffer.load(
              osp.join(osp.expanduser(self.config['environ']['sim']['data_root']),
                self.config['environ']['sim']['data_prefix'])
            )
            print('loaded experts')
            return

        expert = Expert(self.envir)
        while demos < self.config['training']['expert_demos']:
            print('resetting behaviors')
            tot_rewards = 0.
            tot_terms = 0.000001
            state = self.envir.reset()
            expert.reset()
            pbar = tqdm(total=self.max_steps, file=sys.stdout)
            desc = f'  Expert'
            pbar.set_description(desc)
            for step in range(self.max_steps):
                if rospy.is_shutdown():
                    return

                base_acts, exp_resets = expert.get_actions()
                actions = torch.zeros((len(base_acts), self.action_space))
                for ai, action in enumerate(base_acts):
                    if action is not None:
                        actions[ai] = action.get_state_tensor()
                actions[:, -1] = actions[:,-1] * 2 - 1
                nstate, rewards, terminals = self.envir.step_all(base_acts)
                # do or operation on terminals with exp_resets
                terminals[exp_resets] = 1
                # TODO: make whether to ignore terminal condition from
                # goal/rewards optional
                terminals[[not exp for exp in exp_resets]] = 0
                tot_rewards += rewards.sum().item()
                tot_terms += terminals.sum().item()
                self.buffer.add_transitions(
                  state, actions, nstate, rewards, terminals
                )
                state = nstate
                if all(exp_resets):
                    break
                desc = f'  Expert R: {(tot_rewards):.1f} {(tot_terms):.0f}'
                pbar.set_description(desc)
                pbar.update(1)
            pbar.close()
            demos += self.envir.num_envs

    def evaluate(self):
        print('evaluation')
        tot_rewards = 0.0
        qvals = []
        self.model.eval()
        with torch.no_grad():
            state = self.envir.reset()
            pbar = tqdm(total=self.max_steps, file=sys.stdout)
            desc = f'  Evaluate'
            pbar.set_description(desc)
            for step in range(self.max_steps):
                if rospy.is_shutdown():
                    return
                actions, _ = self.model.policy(
                  self.buffer.prep_state(state, self.device) )
                actions = actions.detach()
                qval = torch.min(
                  self.model.critic1(
                    self.buffer.prep_state(state, self.device), actions ),
                  self.model.critic2(
                    self.buffer.prep_state(state, self.device), actions )
                ).cpu()
                qvals.append( qval.squeeze().tolist() )
                nstate, rewards, terminals = self.envir.step_all(actions)
                state = nstate
                tot_rewards += rewards.mean().item()
                desc = f'  Evaluate rewards: {tot_rewards:.4f}'
                pbar.set_description(desc)
                pbar.update(1)
                if (terminals>0).all():
                    break
            pbar.close()
        print('rewards tot:{}, mean:{}'.format(tot_rewards, tot_rewards/self.max_steps))
        self.state = self.envir.reset()
        print('eval done')
        return qvals
