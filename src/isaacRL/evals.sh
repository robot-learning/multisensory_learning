
echo "configs/place_noise_presets/joint_01n_rpl02.yaml"
python full_eval.py --config configs/place_noise_presets/joint_01n_rpl02.yaml

echo "configs/place_noise_presets/joint_01n_rpl03.yaml"
python full_eval.py --config configs/place_noise_presets/joint_01n_rpl03.yaml

echo "configs/place_noise_presets/joint_01n_rpl04.yaml"
python full_eval.py --config configs/place_noise_presets/joint_01n_rpl04.yaml

echo "configs/place_noise_presets/joint_01n_rpl05.yaml"
python full_eval.py --config configs/place_noise_presets/joint_01n_rpl05.yaml

echo "configs/place_noise_presets/joint_02n_rpl01.yaml"
python full_eval.py --config configs/place_noise_presets/joint_02n_rpl01.yaml

echo "configs/place_noise_presets/joint_03n_rpl01.yaml"
python full_eval.py --config configs/place_noise_presets/joint_03n_rpl01.yaml

echo "configs/place_noise_presets/joint_05n_rpl01.yaml"
python full_eval.py --config configs/place_noise_presets/joint_05n_rpl01.yaml

echo "configs/place_noise_presets/joint_07n_rpl01.yaml"
python full_eval.py --config configs/place_noise_presets/joint_07n_rpl01.yaml

echo "configs/place_noise_presets/joint_09n_rpl01.yaml"
python full_eval.py --config configs/place_noise_presets/joint_09n_rpl01.yaml

# python full_eval.py --config configs/picknplace/joint_rpl01.yaml
# python full_eval.py --config configs/picknplace/joint_rpla01.yaml
# python full_eval.py --config configs/picknplace/joint_sac01.yaml
# # python full_eval.py --config configs/picknplace/joint_her01.yaml
#
# python full_eval.py --config configs/picknplace/joint_rpl02.yaml
# python full_eval.py --config configs/picknplace/joint_rpla02.yaml
# python full_eval.py --config configs/picknplace/joint_sac02.yaml

# for i in 93 80 79 63 180 178
# do
#   echo "python evaluate.py --config configs/picknplace/sac01.yaml --checkpt checkpoints/picknplace/sac01/model_$i.pt"
#   python evaluate.py --config configs/picknplace/sac01.yaml --checkpt checkpoints/picknplace/sac01/model_$i.pt
# done

# for i in {11..50..5}
# do
#   echo "python evaluate.py --config configs/bottle/rpl01s.yaml --checkpt checkpoints/bottle/rpl01s/model_$i.pt"
#   python evaluate.py --config configs/bottle/rpl01s.yaml --checkpt checkpoints/bottle/rpl01s/model_$i.pt
# done
# for i in {11..50..5}
# do
#   echo "python evaluate.py --config configs/bottle/herpla02s.yaml --checkpt checkpoints/bottle/herpla02s/model_$i.pt"
#   python evaluate.py --config configs/bottle/herpla02s.yaml --checkpt checkpoints/bottle/herpla02s/model_$i.pt
# done

# CHECKS = "checkpoints/bottle/herpla02s/model_5.pt checkpoints/bottle/herpla02s/model_11.pt checkpoints/bottle/herpla02s/model_18.pt checkpoints/bottle/herpla02s/model_25.pt checkpoints/bottle/herpla02s/model_33.pt checkpoints/bottle/herpla02s/model_42.pt checkpoints/bottle/herpla02s/model_50.pt"
# for checkpt in $CHECKS
# do
#   echo "python evaluate.py --config configs/bottle/herpla02s.yaml --checkpt checkpt"
#   # python evaluate.py --config configs/bottle/herpla02s.yaml --checkpt checkpt
# done
# CHECKS = "checkpoints/bottle/herpl01s/model_5.pt checkpoints/bottle/herpl01s/model_11.pt checkpoints/bottle/herpl01s/model_18.pt checkpoints/bottle/herpl01s/model_25.pt checkpoints/bottle/herpl01s/model_33.pt checkpoints/bottle/herpl01s/model_42.pt checkpoints/bottle/herpl01s/model_50.pt"
# for checkpt in $CHECKS
# do
#   echo "python evaluate.py --config configs/bottle/herpl01s.yaml --checkpt checkpt"
#   # python evaluate.py --config configs/bottle/herpl01s.yaml --checkpt checkpt
# done
# CHECKS = "checkpoints/bottle/rpla01s/model_5.pt checkpoints/bottle/rpla01s/model_11.pt checkpoints/bottle/rpla01s/model_18.pt checkpoints/bottle/rpla01s/model_25.pt checkpoints/bottle/rpla01s/model_33.pt checkpoints/bottle/rpla01s/model_42.pt checkpoints/bottle/rpla01s/model_50.pt"
# for checkpt in $CHECKS
# do
#   echo "python evaluate.py --config configs/bottle/rpla01s.yaml --checkpt checkpt"
#   # python evaluate.py --config configs/bottle/rpla01s.yaml --checkpt checkpt
# done
