
from isaacRL.tasks.gym_task import GymTask
from isaacgymenvs.utils.utils import set_seed

from ll4ma_util import file_util

import os, sys, argparse, json, yaml
import rospy
import torch
import numpy as np
from tqdm import tqdm

from isaacRL.experts.trajopt import IKOptimizer
from isaacRL.testers.utils import *

def main():

    device = "cpu"

    config = file_util.load_yaml('configs/tests/stack_traj.yaml')
    config['environ']['num_envs'] = 1
    config['environ']['headless'] = False
    config['environ']['device'] = device # -1 if random
    config['environ']['use_gpu'] = device == 'cuda'
    config['environ']['seed'] = -1 # if random
    config['environ']['seed'] = set_seed(
      config['environ']['seed'], torch_deterministic=False
    )
    config['environ']['act_range'] = config['model']['actor']['act_range']
    config['environ']['additive_actions'] = False
    config['environ']['allow_resets'] = False
    config['environ']['max_env_steps'] = 500
    max_env_steps = config['environ']['max_env_steps']

    print('using ros')
    rospy.init_node('test_moveit_ik')

    envir = GymTask(config['environ'])
    state = envir.reset()

    ikoptim = IKOptimizer()

    np.set_printoptions(precision=3)

    for _ in range(20):
        if rospy.is_shutdown():
            return
        # rand_joints = np.random.uniform(-1., 1., 7)
        rand_joints = torch.rand((7,))*2. - 1.
        # rand_joints = np.array([-0.767,  0.488, -0.417, -0.266, -0.167, -0.768,  0.253])
        state = run_steps(envir, joint2actions(envir, rand_joints), 100)
        start_joints, start_ee_pose = get_joints_and_ee_pose(envir, state)
        start_joints = start_joints[0].cpu().numpy()
        start_ee_pose = start_ee_pose[0].cpu().numpy()
        # print('start joints:', start_joints)
        # print('start ee:', start_ee_pose)
        if rospy.is_shutdown():
            return

        start_ee_pose = np.array([0.6500, -0.3000,  0.8800, -0.7071,  0.0000,  0.7071,  0.0000])
        # soln_joints = moveit_ik(envir, start_ee_pose)
        # soln_joints = optim_ik(envir, start_ee_pose)
        soln_joints = ikoptim.get_ik(start_ee_pose)

        rospy.sleep(1)  # Sleep to display in viewer
        state = run_steps(envir, joint2actions(envir, soln_joints), 100)
        new_joints, new_ee_pose = get_joints_and_ee_pose(envir, state)
        new_joints = new_joints[0].cpu().numpy()
        new_ee_pose = new_ee_pose[0].cpu().numpy()

        print("\nSTART JOINTS", start_joints)
        print("NEW JOINTS  ", new_joints)
        print('solution joints:',soln_joints)
        print("START EE POSE", start_ee_pose)
        print("NEW EE POSE  ", new_ee_pose)
        rospy.sleep(1)
-0.1886 0.8276 -0.4887 -1.2996 0.5089 1.0134 2.4399
if __name__ == '__main__':
    main()
